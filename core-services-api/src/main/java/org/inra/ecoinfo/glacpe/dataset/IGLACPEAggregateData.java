package org.inra.ecoinfo.glacpe.dataset;

import java.util.Date;

import org.inra.ecoinfo.glacpe.refdata.outilsmesures.OutilsMesure;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.refdata.site.Site;


/**
 * Cette interface permet de marquer les classes qui seront sujettent à des calculs de données aggrégées
 * 
 * @author antoine schellenberger
 * 
 */
public interface IGLACPEAggregateData {

    /**
     *
     * @return
     */
    Float getValue();

    /**
     *
     * @return
     */
    Float getDepth();

    /**
     *
     * @return
     */
    Date getDate();

    /**
     *
     * @return
     */
    VariableGLACPE getVariable();

    /**
     *
     * @return
     */
    Plateforme getPlateforme();

    /**
     *
     * @return
     */
    Site getSite();

    /**
     *
     * @return
     */
    Projet getProjet();

    /**
     *
     * @return
     */
    OutilsMesure getOutilsMesure();

    /**
     *
     * @return
     */
    OutilsMesure getOutilsPrelevement();

    /**
     *
     * @return
     */
    Date getHeure();
}
