package org.inra.ecoinfo.glacpe.dataset.chimie;

import java.util.Date;
import java.util.List;

import org.inra.ecoinfo.glacpe.extraction.vo.GroupeVariableVO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.projetsite.ProjetSite;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public interface IChimieDatatypeManager {

    /**
     *
     */
    public static final String CODE_DATATYPE_CHIMIE = "physico_chimie";

    /**
     *
     * @return
     * @throws BusinessException
     */
    List<Projet> retrieveChimieAvailablesProjets() throws BusinessException;

    /**
     *
     * @param projetSiteId
     * @return
     * @throws BusinessException
     */
    List<Plateforme> retrieveChimieAvailablesPlateformesByProjetSiteId(long projetSiteId) throws BusinessException;

    /**
     *
     * @param plateformesIds
     * @param firstDate
     * @param endDate
     * @return
     * @throws BusinessException
     */
    List<GroupeVariableVO> retrieveGroupesVariables(List<Long> plateformesIds, Date firstDate, Date endDate) throws BusinessException;

    /**
     *
     * @param projetId
     * @param siteId
     * @return
     * @throws BusinessException
     */
    ProjetSite retrieveChimieAvailablesProjetsSiteByProjetAndSite(Long projetId, Long siteId) throws BusinessException;

}
