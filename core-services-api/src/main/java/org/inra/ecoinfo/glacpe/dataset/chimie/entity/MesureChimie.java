/**
 * OREILacs project - see LICENCE.txt for use created: 31 mars 2009 15:16:22
 */
package org.inra.ecoinfo.glacpe.dataset.chimie.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Index;
import org.inra.ecoinfo.glacpe.refdata.projetsitethemedatatype.ProjetSiteThemeDatatype;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.security.entity.ISecurityPath;
import org.inra.ecoinfo.security.entity.ISecurityPathWithVariable;


/**
 * @author "Antoine Schellenberger"
 * 
 */
@Entity
@Table(name = MesureChimie.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {SousSequenceChimie.ID_JPA, "profondeurMax"}))
@org.hibernate.annotations.Table(appliesTo = MesureChimie.TABLE_NAME, indexes = {@Index(name = "profondeurMax", columnNames = {"profondeurMax"})})
public class MesureChimie implements Serializable, ISecurityPath {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    static public final String TABLE_NAME = "mesure_chimie_mchimie";//

    /**
     *
     */
    static public final String ID_JPA = "mchimie_id";

    /**
     *
     */
    @Id
    @Column(name = ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = SousSequenceChimie.ID_JPA, referencedColumnName = SousSequenceChimie.ID_JPA, nullable = false)
    private SousSequenceChimie sousSequence;

    @OneToMany(mappedBy = "mesure", cascade = {CascadeType.ALL})
    @OrderBy("variable")
    private List<ValeurMesureChimie> valeurs = new LinkedList<ValeurMesureChimie>();

    /**
     * Classe raccourcis
     * 
     * @return
     */
    public Site getSite() {
        return this.getSousSequence().getPlateforme().getSite();
    }

    /**
     *
     */
    public MesureChimie() {
        super();
    }

    @Column
    private Float profondeurMin;

    @Column
    private Float profondeurMax;

    @Column
    private Float profondeurReelle;

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public List<ValeurMesureChimie> getValeurs() {
        return valeurs;
    }

    /**
     *
     * @param valeurs
     */
    public void setValeurs(List<ValeurMesureChimie> valeurs) {
        this.valeurs = valeurs;
    }

    /**
     *
     * @return
     */
    public Float getProfondeurMin() {
        return profondeurMin;
    }

    /**
     *
     * @param profondeur
     */
    public void setProfondeurMin(Float profondeur) {
        this.profondeurMin = profondeur;
    }

    /**
     *
     * @return
     */
    public Float getProfondeurMax() {
        return profondeurMax;
    }

    /**
     *
     * @param profondeur
     */
    public void setProfondeurMax(Float profondeur) {
        this.profondeurMax = profondeur;
    }

    /**
     *
     * @return
     */
    public SousSequenceChimie getSousSequence() {
        return sousSequence;
    }

    /**
     *
     * @param sousSequence
     */
    public void setSousSequence(SousSequenceChimie sousSequence) {
        this.sousSequence = sousSequence;
    }

    @Override
    public String getSecurityPath() {
        return String.format("%s/%s/%s/%s", ((ProjetSiteThemeDatatype) getSousSequence().getSequence().getVersionFile().getDataset().getLeafNode()).getProjetSiteTheme().getProjetSite().getProjet().getCode(), getSite().getPath(),
                ((ProjetSiteThemeDatatype) getSousSequence().getSequence().getVersionFile().getDataset().getLeafNode()).getProjetSiteTheme().getTheme().getCode(), getSousSequence().getSequence().getVersionFile().getDataset().getLeafNode()
                        .getDatatype().getCode());
    }

    @Override
    public List<ISecurityPathWithVariable> getChildren() {
        List<ISecurityPathWithVariable> children = new LinkedList<ISecurityPathWithVariable>();
        for (ValeurMesureChimie valeur : valeurs) {
            children.add(valeur);
        }
        return children;
    }

    @Override
    public void setChildren(List<ISecurityPathWithVariable> children) {
        this.valeurs.clear();
        for (ISecurityPathWithVariable child : children) {
            this.valeurs.add((ValeurMesureChimie) child);
        }
    }

    @Override
    public Date getDatePrelevement() {
        return this.getSousSequence().getSequence().getDatePrelevement();
    }

    /**
     *
     * @return
     */
    public Float getProfondeurReelle() {
        return profondeurReelle;
    }

    /**
     *
     * @param profondeurReelle
     */
    public void setProfondeurReelle(Float profondeurReelle) {
        this.profondeurReelle = profondeurReelle;
    }

}
