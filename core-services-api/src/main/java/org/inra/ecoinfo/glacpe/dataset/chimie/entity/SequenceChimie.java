/**
 * OREILacs project - see LICENCE.txt for use created: 31 mars 2009 13:30:55
 */
package org.inra.ecoinfo.glacpe.dataset.chimie.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Index;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.glacpe.refdata.projetsite.ProjetSite;


/**
 * @author "Antoine Schellenberger"
 * 
 */
@Entity
@Table(name = SequenceChimie.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {ProjetSite.ID_JPA, "date_prelevement"}))
@org.hibernate.annotations.Table(appliesTo = SequenceChimie.TABLE_NAME, indexes = {@Index(name = SequenceChimie.INDEX_ATTRIBUTS_DATE, columnNames = {"date_prelevement"}),
        @Index(name = SequenceChimie.INDEX_ATTRIBUTS_PSITE, columnNames = {ProjetSite.ID_JPA}), @Index(name = SequenceChimie.INDEX_ATTRIBUTS_IVF, columnNames = {VersionFile.ID_JPA})})
public class SequenceChimie implements Serializable {

    /**
     *
     */
    public SequenceChimie() {
        super();

    }

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    static public final String TABLE_NAME = "sequence_chimie_schimie";

    /**
     *
     */
    static public final String ID_JPA = "schimie_id";

    /**
     *
     */
    public static final String INDEX_ATTRIBUTS_DATE = "date_chimie_ikey";

    /**
     *
     */
    public static final String INDEX_ATTRIBUTS_PSITE = "psi_chimie_ikey";

    /**
     *
     */
    public static final String INDEX_ATTRIBUTS_IVF = "ivf_chimie_ikey";

    /**
     *
     */
    @Id
    @Column(name = ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column(name = "date_prelevement", nullable = true)
    private Date datePrelevement;

    @Column(name = "date_reception", nullable = false)
    private Date dateReception;

    @Column(name = "date_debut_campagne", nullable = true)
    private Date dateDebutCampagne;

    @Column(name = "date_fin_campagne", nullable = true)
    private Date dateFinCampagne;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = ProjetSite.ID_JPA, referencedColumnName = ProjetSite.ID_JPA, nullable = false)
    private ProjetSite projetSite;

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = VersionFile.ID_JPA, referencedColumnName = VersionFile.ID_JPA, nullable = false)
    private VersionFile versionFile;

    @OneToMany(mappedBy = "sequence", cascade = {CascadeType.ALL})
    private List<SousSequenceChimie> sousSequences = new LinkedList<SousSequenceChimie>();

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public Date getDatePrelevement() {
        return datePrelevement;
    }

    /**
     *
     * @param datePrelevement
     */
    public void setDatePrelevement(Date datePrelevement) {
        this.datePrelevement = datePrelevement;
    }

    /**
     *
     * @return
     */
    public Date getDateReception() {
        return dateReception;
    }

    /**
     *
     * @param dateReception
     */
    public void setDateReception(Date dateReception) {
        this.dateReception = dateReception;
    }

    /**
     *
     * @return
     */
    public Date getDateDebutCampagne() {
        return dateDebutCampagne;
    }

    /**
     *
     * @param dateDebutCampagne
     */
    public void setDateDebutCampagne(Date dateDebutCampagne) {
        this.dateDebutCampagne = dateDebutCampagne;
    }

    /**
     *
     * @return
     */
    public Date getDateFinCampagne() {
        return dateFinCampagne;
    }

    /**
     *
     * @param dateFinCampagne
     */
    public void setDateFinCampagne(Date dateFinCampagne) {
        this.dateFinCampagne = dateFinCampagne;
    }

    /**
     *
     * @return
     */
    public ProjetSite getProjetSite() {
        return projetSite;
    }

    /**
     *
     * @param projetSite
     */
    public void setProjetSite(ProjetSite projetSite) {
        this.projetSite = projetSite;
    }

    /**
     *
     * @return
     */
    public List<SousSequenceChimie> getSousSequences() {
        return sousSequences;
    }

    /**
     *
     * @param sousSequences
     */
    public void setSousSequences(List<SousSequenceChimie> sousSequences) {
        this.sousSequences = sousSequences;
    }

    /**
     *
     * @param versionFile
     */
    public void setVersionFile(VersionFile versionFile) {
        this.versionFile = versionFile;
    }

    /**
     *
     * @return
     */
    public VersionFile getVersionFile() {
        return versionFile;
    }
}
