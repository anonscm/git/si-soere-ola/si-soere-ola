/**
 * OREILacs project - see LICENCE.txt for use created: 31 mars 2009 15:18:53
 */
package org.inra.ecoinfo.glacpe.dataset.chimie.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Index;
import org.inra.ecoinfo.glacpe.dataset.IGLACPEAggregateData;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.OutilsMesure;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.projetsitethemedatatype.ProjetSiteThemeDatatype;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.refdata.valeurqualitative.ValeurQualitative;
import org.inra.ecoinfo.security.entity.ISecurityPathWithVariable;


/**
 * @author "Antoine Schellenberger"
 */
@Entity
@Table(name = ValeurMesureChimie.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {VariableGLACPE.ID_JPA, MesureChimie.ID_JPA}))
@org.hibernate.annotations.Table(appliesTo = ValeurMesureChimie.TABLE_NAME, indexes = {@Index(name = ValeurMesureChimie.INDEX_ATTRIBUTS_VAR, columnNames = {VariableGLACPE.ID_JPA})})
public class ValeurMesureChimie implements Serializable, IGLACPEAggregateData, ISecurityPathWithVariable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    static public final String TABLE_NAME = "valeur_mesure_chimie_vmchimie";// valeur_mesureChimie

    /**
     *
     */
    static public final String ID_JPA = "vmchimie_id";

    /**
     *
     */
    public static final String INDEX_ATTRIBUTS_VAR = "var_chimie_ikey";

    /**
     *
     */
    @Id
    @Column(name = "vmchimie_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = MesureChimie.ID_JPA, referencedColumnName = MesureChimie.ID_JPA, nullable = false)
    private MesureChimie mesure;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = VariableGLACPE.ID_JPA, referencedColumnName = VariableGLACPE.PERSISTENT_NAME_ID, nullable = false)
    private VariableGLACPE variable;

    @Column(nullable = true)
    private Float valeur;

    @Column(nullable = true)
    private ValeurQualitative flag;

    @Column(name = "ligne_fichier_echange")
    private Long ligneFichierEchange;

    /**
     *
     * @param mesure
     */
    public void setMesure(MesureChimie mesure) {
        this.mesure = mesure;
    }

    /**
     *
     * @return
     */
    public MesureChimie getMesure() {
        return mesure;
    }

    /**
     *
     * @return
     */
    public VariableGLACPE getVariable() {
        return variable;
    }

    /**
     *
     * @param variable
     */
    public void setVariable(VariableGLACPE variable) {
        this.variable = variable;
    }

    /**
     *
     * @return
     */
    public Float getValeur() {
        return valeur;
    }

    /**
     *
     * @param valeur
     */
    public void setValeur(Float valeur) {
        this.valeur = valeur;
    }

    /**
     *
     */
    public ValeurMesureChimie() {
        super();
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public Long getLigneFichierEchange() {
        return ligneFichierEchange;
    }

    /**
     *
     * @param ligneFichierEchange
     */
    public void setLigneFichierEchange(Long ligneFichierEchange) {
        this.ligneFichierEchange = ligneFichierEchange;
    }

    /**
     *
     * @return
     */
    @Override
    public Float getValue() {
        return getValeur();
    }

    /**
     *
     * @return
     */
    @Override
    public Float getDepth() {
        return this.getMesure().getProfondeurReelle() != null ? this.getMesure().getProfondeurReelle() : this.getMesure().getProfondeurMax();
    }

    /**
     *
     * @return
     */
    @Override
    public Date getDate() {
        return this.getMesure().getSousSequence().getSequence().getDatePrelevement();
    }

    /**
     *
     * @return
     */
    @Override
    public Plateforme getPlateforme() {
        return this.getMesure().getSousSequence().getPlateforme();
    }

    /**
     *
     * @return
     */
    @Override
    public Site getSite() {
        return this.getMesure().getSousSequence().getPlateforme().getSite();
    }

    /**
     *
     * @return
     */
    @Override
    public Projet getProjet() {
        return this.getMesure().getSousSequence().getSequence().getProjetSite().getProjet();
    }

    /**
     *
     * @return
     */
    @Override
    public OutilsMesure getOutilsMesure() {
        return this.getMesure().getSousSequence().getOutilsMesure().getTypeOutilsMesure().getType().getValeur().equals("mesure") ? this.getMesure().getSousSequence().getOutilsMesure() : null;
    }

    /**
     *
     * @return
     */
    @Override
    public Date getHeure() {
        return null;
    }

    @Override
    public String getSecurityPathWithVariable() {
        return String.format("%s/%s/%s/%s/%s", ((ProjetSiteThemeDatatype) getMesure().getSousSequence().getSequence().getVersionFile().getDataset().getLeafNode()).getProjetSiteTheme().getProjetSite().getProjet().getCode(), getMesure().getSite()
                .getPath(), ((ProjetSiteThemeDatatype) getMesure().getSousSequence().getSequence().getVersionFile().getDataset().getLeafNode()).getProjetSiteTheme().getTheme().getCode(), getMesure().getSousSequence().getSequence().getVersionFile()
                .getDataset().getLeafNode().getDatatype().getCode(), getVariable().getCode());
    }

    @Override
    public Date getDatePrelevement() {
        return this.getMesure().getSousSequence().getSequence().getDatePrelevement();
    }

    /**
     *
     * @return
     */
    @Override
    public OutilsMesure getOutilsPrelevement() {
        return this.getMesure().getSousSequence().getOutilsMesure().getTypeOutilsMesure().getType().getValeur().equals("mesure") ? null : this.getMesure().getSousSequence().getOutilsMesure();
    }

    /**
     *
     * @return
     */
    public ValeurQualitative getFlag() {
        return flag;
    }

    /**
     *
     * @param flag
     */
    public void setFlag(ValeurQualitative flag) {
        this.flag = flag;
    }
}
