package org.inra.ecoinfo.glacpe.dataset.chloro;

import java.util.Date;
import java.util.List;

import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public interface IChlorophylleDatatypeManager {

    /**
     *
     */
    String CODE_DATATYPE_CHLORO = "";

    /**
     *
     * @return
     * @throws BusinessException
     */
    public List<Site> retrieveChlorophylleAvailablesSites() throws BusinessException;

    /**
     *
     * @param plateformesIds
     * @param firstDate
     * @param endDate
     * @return
     * @throws BusinessException
     */
    public List<VariableGLACPE> retrieveChlorophylleAvailablesVariables(List<Long> plateformesIds, Date firstDate, Date endDate) throws BusinessException;

    /**
     *
     * @return
     * @throws BusinessException
     */
    public List<Projet> retrieveChloroAvailablesProjets() throws BusinessException;

    /**
     *
     * @param id
     * @return
     * @throws BusinessException
     */
    public List<Plateforme> retrieveChloroAvailablesPlateformesByProjetSiteId(Long id) throws BusinessException;

}
