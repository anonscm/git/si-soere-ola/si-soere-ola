/**
 * OREILacs project - see LICENCE.txt for use created: 24 septembre 2010 09:14:20
 */
package org.inra.ecoinfo.glacpe.dataset.chloro.entity;

import static javax.persistence.CascadeType.ALL;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Index;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.glacpe.refdata.projetsite.ProjetSite;


/**
 * @author "Cédric Anache"
 * 
 */

@Entity
@Table(name = SequenceChloro.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {ProjetSite.ID_JPA, SequenceChloro.NAME_ATTRIBUTS_DATE}))
@org.hibernate.annotations.Table(appliesTo = SequenceChloro.TABLE_NAME, indexes = {@Index(name = SequenceChloro.INDEX_ATTRIBUTS_DATE, columnNames = {SequenceChloro.NAME_ATTRIBUTS_DATE}),
        @Index(name = SequenceChloro.INDEX_ATTRIBUTS_PSI, columnNames = {ProjetSite.ID_JPA}), @Index(name = SequenceChloro.INDEX_ATTRIBUTS_IVF, columnNames = {VersionFile.ID_JPA})})
public class SequenceChloro implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = "schloro_id";

    /**
     *
     */
    public static final String TABLE_NAME = "sequence_chloro_schloro";

    /**
     *
     */
    public static final String NAME_ATTRIBUTS_DATE = "date";

    /**
     *
     */
    public static final String INDEX_ATTRIBUTS_DATE = "date_chloro_ikey";

    /**
     *
     */
    public static final String INDEX_ATTRIBUTS_PSI = "psi_chloro_ikey";

    /**
     *
     */
    public static final String INDEX_ATTRIBUTS_IVF = "ivf_chloro_ikey";

    /**
     *
     */
    @Id
    @Column(name = ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column(name = NAME_ATTRIBUTS_DATE, nullable = false)
    private Date date;

    // Association des Sequences à une projetSite
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = ProjetSite.ID_JPA, referencedColumnName = ProjetSite.ID_JPA, nullable = false)
    private ProjetSite projetSite;

    // Association des Sequences à un fichier d'échange
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = VersionFile.ID_JPA, referencedColumnName = VersionFile.ID_JPA, nullable = false)
    private VersionFile versionFile;

    // Une séquence peut avoir plusieurs Mesures
    @OneToMany(mappedBy = "sequenceChloro", cascade = ALL)
    private List<SousSequenceChloro> sousSequences = new LinkedList<SousSequenceChloro>();

    /**
     *
     */
    public SequenceChloro() {
        super();
    }

    /**
     *
     * @param date
     * @param projetSite
     * @param versionFile
     */
    public SequenceChloro(Date date, ProjetSite projetSite, VersionFile versionFile) {
        super();
        this.date = date;
        this.projetSite = projetSite;
        this.versionFile = versionFile;
    }

    // Id

    /**
     *
     * @return
     */
        public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    // Date

    /**
     *
     * @return
     */
        public Date getDate() {
        return date;
    }

    /**
     *
     * @param date
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     *
     * @return
     */
    public ProjetSite getProjetSite() {
        return projetSite;
    }

    /**
     *
     * @param projetSite
     */
    public void setProjetSite(ProjetSite projetSite) {
        this.projetSite = projetSite;
    }

    /**
     *
     * @return
     */
    public List<SousSequenceChloro> getSousSequences() {
        return sousSequences;
    }

    /**
     *
     * @param sousSequences
     */
    public void setSousSequences(List<SousSequenceChloro> sousSequences) {
        this.sousSequences = sousSequences;
    }

    /**
     *
     * @param versionFile
     */
    public void setVersionFile(VersionFile versionFile) {
        this.versionFile = versionFile;
    }

    /**
     *
     * @return
     */
    public VersionFile getVersionFile() {
        return versionFile;
    }

}// end of class
