/**
 * OREILacs project - see LICENCE.txt for use created: 24 septembre 2010 10:07:20
 */
package org.inra.ecoinfo.glacpe.dataset.chloro.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Index;
import org.inra.ecoinfo.glacpe.dataset.IGLACPEAggregateData;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.OutilsMesure;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.projetsitethemedatatype.ProjetSiteThemeDatatype;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.security.entity.ISecurityPathWithVariable;


/**
 * @author "Cédric Anache"
 * 
 */
@Entity
@Table(name = ValeurMesureChloro.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {VariableGLACPE.ID_JPA, MesureChloro.ID_JPA}))
@org.hibernate.annotations.Table(appliesTo = ValeurMesureChloro.TABLE_NAME, indexes = {@Index(name = ValeurMesureChloro.INDEX_ATTRIBUTS_VAR, columnNames = {VariableGLACPE.ID_JPA})})
public class ValeurMesureChloro implements Serializable, ISecurityPathWithVariable, IGLACPEAggregateData {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = "vmchloro_id";

    /**
     *
     */
    public static final String TABLE_NAME = "valeur_mesure_chloro_vmchloro";

    /**
     *
     */
    public static final String INDEX_ATTRIBUTS_VAR = "var_chloro_ikey";

    /**
     *
     */
    @Id
    @Column(name = ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    // Associations des valeurs à une mesure
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = MesureChloro.ID_JPA, referencedColumnName = MesureChloro.ID_JPA, nullable = false)
    private MesureChloro mesure;

    // Variable associé à la valeur mesuré
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = VariableGLACPE.ID_JPA, referencedColumnName = VariableGLACPE.VARIABLE_NAME_ID, nullable = false)
    private VariableGLACPE variable;

    @Column(nullable = true)
    private Float valeur;

    /**
     *
     */
    public ValeurMesureChloro() {
        super();
    }

    /**
     *
     * @param mesure
     * @param variable
     * @param valeur
     */
    public ValeurMesureChloro(MesureChloro mesure, VariableGLACPE variable, Float valeur) {
        super();
        this.mesure = mesure;
        this.variable = variable;
        this.valeur = valeur;
        if (!mesure.getValeurs().contains(this))
            mesure.getValeurs().add(this);
    }

    // Id

    /**
     *
     * @return
     */
        public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    // MesureChloro

    /**
     *
     * @param mesure
     */
        public void setMesure(MesureChloro mesure) {
        this.mesure = mesure;
    }

    /**
     *
     * @return
     */
    public MesureChloro getMesure() {
        return mesure;
    }

    // Variable

    /**
     *
     * @return
     */
        public VariableGLACPE getVariable() {
        return variable;
    }

    /**
     *
     * @param variable
     */
    public void setVariable(VariableGLACPE variable) {
        this.variable = variable;
    }

    // Valeur de la variable

    /**
     *
     * @return
     */
        public Float getValeur() {
        return valeur;
    }

    /**
     *
     * @param valeur
     */
    public void setValeur(Float valeur) {
        this.valeur = valeur;
    }

    @Override
    public String getSecurityPathWithVariable() {
        return String.format("%s/%s/%s/%s/%s", ((ProjetSiteThemeDatatype) getMesure().getSousSequenceChloro().getSequenceChloro().getVersionFile().getDataset().getLeafNode()).getProjetSiteTheme().getProjetSite().getProjet().getCode(),
                ((ProjetSiteThemeDatatype) getMesure().getSousSequenceChloro().getSequenceChloro().getVersionFile().getDataset().getLeafNode()).getProjetSiteTheme().getProjetSite().getSite().getPath(), ((ProjetSiteThemeDatatype) getMesure()
                        .getSousSequenceChloro().getSequenceChloro().getVersionFile().getDataset().getLeafNode()).getProjetSiteTheme().getTheme().getCode(), getMesure().getSousSequenceChloro().getSequenceChloro().getVersionFile().getDataset()
                        .getLeafNode().getDatatype().getCode(), getVariable().getCode());
    }

    @Override
    public Date getDatePrelevement() {
        return this.getMesure().getSousSequenceChloro().getSequenceChloro().getDate();
    }

    /**
     *
     * @return
     */
    @Override
    public Float getValue() {
        return getValeur();
    }

    /**
     *
     * @return
     */
    @Override
    public Float getDepth() {
        return mesure.getProfondeur_max();
    }

    /**
     *
     * @return
     */
    @Override
    public Date getDate() {
        return mesure.getSousSequenceChloro().getSequenceChloro().getDate();
    }

    /**
     *
     * @return
     */
    @Override
    public Plateforme getPlateforme() {
        return mesure.getSousSequenceChloro().getPlateforme();
    }

    /**
     *
     * @return
     */
    @Override
    public Site getSite() {
        return getMesure().getSousSequenceChloro().getSequenceChloro().getProjetSite().getSite();
    }

    /**
     *
     * @return
     */
    @Override
    public Projet getProjet() {
        return getMesure().getSousSequenceChloro().getSequenceChloro().getProjetSite().getProjet();
    }

    /**
     *
     * @return
     */
    @Override
    public OutilsMesure getOutilsMesure() {
        return null;
    }

    /**
     *
     * @return
     */
    @Override
    public Date getHeure() {
        return null;
    }

    /**
     *
     * @return
     */
    @Override
    public OutilsMesure getOutilsPrelevement() {
        return null;
    }

}// End of class
