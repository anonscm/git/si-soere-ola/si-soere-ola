package org.inra.ecoinfo.glacpe.dataset.conditionprelevement;

import java.util.Date;
import java.util.List;

import org.inra.ecoinfo.glacpe.extraction.vo.VariableVO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.projetsite.ProjetSite;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public interface IConditionGeneraleDatatypeManager {

    /**
     *
     */
    public static final String CODE_DATATYPE_CONDITION_PRELEVEMENT = "conditions_prelevements";

    /**
     *
     * @return
     * @throws BusinessException
     */
    List<Projet> retrieveConditionGeneraleAvailablesProjets() throws BusinessException;

    /**
     *
     * @param projetSiteId
     * @return
     * @throws BusinessException
     */
    List<Plateforme> retrieveConditionGeneraleAvailablesPlateformesByProjetSiteId(long projetSiteId) throws BusinessException;

    /**
     *
     * @param projetId
     * @param siteId
     * @return
     * @throws BusinessException
     */
    ProjetSite retrieveConditionGeneraleAvailablesProjetsSiteByProjetAndSite(Long projetId, Long siteId) throws BusinessException;

    /**
     *
     * @return
     * @throws BusinessException
     */
    List<VariableVO> retrieveListVariables() throws BusinessException;

    /**
     *
     * @param plateformesIds
     * @param firstDate
     * @param endDate
     * @return
     * @throws BusinessException
     */
    List<VariableVO> retrieveListsVariables(List<Long> plateformesIds, Date firstDate, Date endDate) throws BusinessException;

    /**
     *
     * @return
     * @throws BusinessException
     */
    List<Projet> retrieveTransparenceAvailablesProjets() throws BusinessException;

    /**
     *
     * @param plateformesIds
     * @param firstDate
     * @param endDate
     * @return
     * @throws BusinessException
     */
    List<VariableGLACPE> retrieveTransparenceAvailablesVariables(List<Long> plateformesIds, Date firstDate, Date endDate) throws BusinessException;
}
