package org.inra.ecoinfo.glacpe.dataset.conditionprelevement.entity;

import static javax.persistence.CascadeType.ALL;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Index;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projetsite.ProjetSite;
import org.inra.ecoinfo.glacpe.refdata.projetsitethemedatatype.ProjetSiteThemeDatatype;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.security.entity.ISecurityPath;
import org.inra.ecoinfo.security.entity.ISecurityPathWithVariable;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = MesureConditionGenerale.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {Plateforme.ID_JPA, "date_prelevement", ProjetSite.ID_JPA}))
@org.hibernate.annotations.Table(appliesTo = MesureConditionGenerale.TABLE_NAME, indexes = {@Index(name = MesureConditionGenerale.INDEX_ATTRIBUTS_DATE, columnNames = {"date_prelevement"}),
        @Index(name = MesureConditionGenerale.INDEX_ATTRIBUTS_PLATEFORME, columnNames = {Plateforme.ID_JPA}), @Index(name = MesureConditionGenerale.INDEX_ATTRIBUTS_IVF, columnNames = {VersionFile.ID_JPA}),
        @Index(name = MesureConditionGenerale.INDEX_ATTRIBUTS_PSI, columnNames = {ProjetSite.ID_JPA})})
public class MesureConditionGenerale implements Serializable, ISecurityPath {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = "mconditionprelevement_id";

    /**
     *
     */
    public static final String TABLE_NAME = "mesure_condition_prelevement_mconditionprelevement";

    /**
     *
     */
    public static final String INDEX_ATTRIBUTS_DATE = "date_condition_ikey";

    /**
     *
     */
    public static final String INDEX_ATTRIBUTS_PLATEFORME = "plateforme_condition_ikey";

    /**
     *
     */
    public static final String INDEX_ATTRIBUTS_IVF = "ivf_condition_ikey";

    /**
     *
     */
    public static final String INDEX_ATTRIBUTS_PSI = "psi_condition_ikey";

    /**
     *
     */
    public MesureConditionGenerale() {
        super();
    }

    /**
     *
     */
    @Id
    @Column(name = ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column(name = "date_prelevement", nullable = true)
    private Date datePrelevement;

    @ManyToOne(cascade = {CascadeType.PERSIST})
    @JoinColumn(name = Plateforme.ID_JPA, referencedColumnName = Plateforme.ID_JPA, nullable = false)
    private Plateforme plateforme;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = ProjetSite.ID_JPA, referencedColumnName = ProjetSite.ID_JPA, nullable = false)
    private ProjetSite projetSite;

    @ManyToOne(cascade = {CascadeType.PERSIST})
    @JoinColumn(name = VersionFile.ID_JPA, referencedColumnName = VersionFile.ID_JPA, nullable = false)
    private VersionFile versionFile;

    @Column(name = "ligne_fichier_echange")
    private Long ligneFichierEchange;

    @OneToMany(mappedBy = "mesure", cascade = ALL)
    @OrderBy("variable")
    private List<ValeurConditionGenerale> valeurs = new LinkedList<ValeurConditionGenerale>();

    @Temporal(TemporalType.TIME)
    @Column(name = "heure", nullable = true)
    private Date heure;

    @Column(name = "commentaire")
    private String commentaire;

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Date getDatePrelevement() {
        return datePrelevement;
    }

    /**
     *
     * @param datePrelevement
     */
    public void setDatePrelevement(Date datePrelevement) {
        this.datePrelevement = datePrelevement;
    }

    /**
     *
     * @return
     */
    public Plateforme getPlateforme() {
        return plateforme;
    }

    /**
     *
     * @param plateforme
     */
    public void setPlateforme(Plateforme plateforme) {
        this.plateforme = plateforme;
    }

    /**
     *
     * @return
     */
    public ProjetSite getProjetSite() {
        return projetSite;
    }

    /**
     *
     * @param projetSite
     */
    public void setProjetSite(ProjetSite projetSite) {
        this.projetSite = projetSite;
    }

    /**
     *
     * @param versionFile
     */
    public void setVersionFile(VersionFile versionFile) {
        this.versionFile = versionFile;
    }

    /**
     *
     * @return
     */
    public VersionFile getVersionFile() {
        return versionFile;
    }

    /**
     *
     * @return
     */
    public Site getSite() {
        return this.getPlateforme().getSite();
    }

    @Override
    public String getSecurityPath() {
        return String.format("%s/%s/%s/%s", ((ProjetSiteThemeDatatype) getVersionFile().getDataset().getLeafNode()).getProjetSiteTheme().getProjetSite().getProjet().getCode(), getSite().getPath(), ((ProjetSiteThemeDatatype) getVersionFile()
                .getDataset().getLeafNode()).getProjetSiteTheme().getTheme().getCode(), getVersionFile().getDataset().getLeafNode().getDatatype().getCode());
    }

    @Override
    public List<ISecurityPathWithVariable> getChildren() {
        List<ISecurityPathWithVariable> children = new LinkedList<ISecurityPathWithVariable>();
        for (ValeurConditionGenerale valeur : valeurs) {
            children.add(valeur);
        }
        return children;
    }

    @Override
    public void setChildren(List<ISecurityPathWithVariable> children) {
        this.valeurs.clear();
        for (ISecurityPathWithVariable child : children) {
            this.valeurs.add((ValeurConditionGenerale) child);
        }
    }

    /**
     *
     * @return
     */
    public Long getLigneFichierEchange() {
        return ligneFichierEchange;
    }

    /**
     *
     * @param ligneFichierEchange
     */
    public void setLigneFichierEchange(Long ligneFichierEchange) {
        this.ligneFichierEchange = ligneFichierEchange;
    }

    /**
     *
     * @return
     */
    public List<ValeurConditionGenerale> getValeurs() {
        return valeurs;
    }

    /**
     *
     * @param valeurs
     */
    public void setValeurs(List<ValeurConditionGenerale> valeurs) {
        this.valeurs = valeurs;
    }

    /**
     *
     * @return
     */
    public Date getHeure() {
        return heure;
    }

    /**
     *
     * @param heure
     */
    public void setHeure(Date heure) {
        this.heure = heure;
    }

    /**
     *
     * @return
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     *
     * @param commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }
}
