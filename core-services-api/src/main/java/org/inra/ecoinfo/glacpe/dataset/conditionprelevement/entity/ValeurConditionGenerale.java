package org.inra.ecoinfo.glacpe.dataset.conditionprelevement.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Index;
import org.inra.ecoinfo.glacpe.refdata.projetsitethemedatatype.ProjetSiteThemeDatatype;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.refdata.valeurqualitative.ValeurQualitative;
import org.inra.ecoinfo.security.entity.ISecurityPathWithVariable;


/**
 * @author "Guillaume Enrico"
 * 
 */
@Entity
@Table(name = ValeurConditionGenerale.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {VariableGLACPE.ID_JPA, MesureConditionGenerale.ID_JPA}))
@org.hibernate.annotations.Table(appliesTo = ValeurConditionGenerale.TABLE_NAME, indexes = {@Index(name = ValeurConditionGenerale.INDEX_ATTRIBUTS_VAR, columnNames = {VariableGLACPE.ID_JPA})})
public class ValeurConditionGenerale implements Serializable, ISecurityPathWithVariable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = "vconditionprelevement_id";

    /**
     *
     */
    public static final String TABLE_NAME = "valeur_condition_prelevement_vmconditionprelevement";

    /**
     *
     */
    public static final String INDEX_ATTRIBUTS_VAR = "var_conditions_ikey";

    /**
     *
     */
    @Id
    @Column(name = ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = MesureConditionGenerale.ID_JPA, referencedColumnName = MesureConditionGenerale.ID_JPA, nullable = false)
    private MesureConditionGenerale mesure;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = VariableGLACPE.ID_JPA, referencedColumnName = VariableGLACPE.VARIABLE_NAME_ID, nullable = false)
    private VariableGLACPE variable;

    @Column(nullable = true)
    private Float valeur;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = ValeurQualitative.ID_JPA, referencedColumnName = ValeurQualitative.ID_JPA, nullable = true)
    private ValeurQualitative valeurQualitative;

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public VariableGLACPE getVariable() {
        return variable;
    }

    /**
     *
     * @param variable
     */
    public void setVariable(VariableGLACPE variable) {
        this.variable = variable;
    }

    /**
     *
     * @return
     */
    public Float getValeur() {
        return valeur;
    }

    /**
     *
     * @param valeur
     */
    public void setValeur(Float valeur) {
        this.valeur = valeur;
    }

    /**
     *
     * @return
     */
    public MesureConditionGenerale getMesure() {
        return mesure;
    }

    /**
     *
     * @param mesure
     */
    public void setMesure(MesureConditionGenerale mesure) {
        this.mesure = mesure;
    }

    /**
     *
     */
    public ValeurConditionGenerale() {
        super();
    }

    /**
     *
     * @return
     */
    public ValeurQualitative getValeurQualitative() {
        return valeurQualitative;
    }

    /**
     *
     * @param valeurQualitative
     */
    public void setValeurQualitative(ValeurQualitative valeurQualitative) {
        this.valeurQualitative = valeurQualitative;
    }

    @Override
    public String getSecurityPathWithVariable() {
        return String.format("%s/%s/%s/%s/%s", ((ProjetSiteThemeDatatype) getMesure().getVersionFile().getDataset().getLeafNode()).getProjetSiteTheme().getProjetSite().getProjet().getCode(), getMesure().getSite().getPath(),
                ((ProjetSiteThemeDatatype) getMesure().getVersionFile().getDataset().getLeafNode()).getProjetSiteTheme().getTheme().getCode(), getMesure().getVersionFile().getDataset().getLeafNode().getDatatype().getCode(), getVariable().getCode());
    }

    @Override
    public Date getDatePrelevement() {
        return this.getMesure().getDatePrelevement();
    }
}
