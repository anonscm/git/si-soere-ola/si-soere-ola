package org.inra.ecoinfo.glacpe.dataset.contenustomacaux;

import java.util.Date;
import java.util.List;

import org.inra.ecoinfo.glacpe.extraction.vo.VariableVO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.projetsite.ProjetSite;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public interface IContenuStomacauxDatatypeManager {

    /**
     *
     */
    public static final String CODE_DATATYPE_CONTENU_STOMACAUX = "contenus_stomacaux";

    /**
     *
     * @return
     * @throws BusinessException
     */
    List<Projet> retrieveContenuStomacauxAvailablesProjets() throws BusinessException;

    /**
     *
     * @param projetSiteId
     * @return
     * @throws BusinessException
     */
    List<Plateforme> retrieveContenuStomacauxAvailablesPlateformesByProjetSiteId(long projetSiteId) throws BusinessException;

    /**
     *
     * @param projetId
     * @param siteId
     * @return
     * @throws BusinessException
     */
    ProjetSite retrieveContenuStomacauxAvailablesProjetsSiteByProjetAndSite(Long projetId, Long siteId) throws BusinessException;

    /**
     *
     * @param plateformesIds
     * @param firstDate
     * @param endDate
     * @return
     * @throws BusinessException
     */
    List<VariableVO> retrieveListsVariables(List<Long> plateformesIds, Date firstDate, Date endDate) throws BusinessException;

}
