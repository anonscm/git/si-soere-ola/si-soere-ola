package org.inra.ecoinfo.glacpe.dataset.contenustomacaux.entity;

import static javax.persistence.CascadeType.ALL;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Index;
import org.inra.ecoinfo.glacpe.refdata.projetsitethemedatatype.ProjetSiteThemeDatatype;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.refdata.valeurqualitative.ValeurQualitative;
import org.inra.ecoinfo.security.entity.ISecurityPath;
import org.inra.ecoinfo.security.entity.ISecurityPathWithVariable;


/**
 * @author "Guillaume Enrico"
 * 
 */

@Entity
@Table(name = MesureContenuStomacaux.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {SousSequenceContenuStomacaux.ID_JPA, MesureContenuStomacaux.NUMERO_POISSON}))
@org.hibernate.annotations.Table(appliesTo = MesureContenuStomacaux.TABLE_NAME, indexes = {@Index(name = MesureContenuStomacaux.NUMERO_POISSON, columnNames = {MesureContenuStomacaux.NUMERO_POISSON})})
public class MesureContenuStomacaux implements Serializable, ISecurityPath {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    static public final String TABLE_NAME = "mesure_contenus_stomacaux_mcontenustomacaux";

    /**
     *
     */
    static public final String ID_JPA = "mcontenustomacaux_id";

    /**
     *
     */
    static public final String SEXE_POISSON = "sexe";

    /**
     *
     */
    static public final String SEXUALITE_POISSON = "maturite";

    /**
     *
     */
    static public final String MALFORMATION_POISSON = "malformation";

    /**
     *
     */
    static public final String ETAT_ESTOMAC_POISSON = "etat_estomac";

    /**
     *
     */
    static public final String NUMERO_POISSON = "numero_poisson";

    /**
     *
     */
    public MesureContenuStomacaux() {
        super();
    }

    /**
     *
     */
    @Id
    @Column(name = ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column(name = "ligne_fichier_echange")
    private Long ligneFichierEchange;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = SousSequenceContenuStomacaux.ID_JPA, referencedColumnName = SousSequenceContenuStomacaux.ID_JPA, nullable = false)
    private SousSequenceContenuStomacaux sousSequence;

    @OneToMany(mappedBy = "mesure", cascade = ALL)
    @OrderBy(ValeurMesureContenuStomacaux.PROIE)
    private List<ValeurMesureContenuStomacaux> valeurs = new LinkedList<ValeurMesureContenuStomacaux>();

    @Column(name = MesureContenuStomacaux.NUMERO_POISSON, nullable = false)
    private Integer numeroPoisson;

    @Column
    private Float longueurPoisson;

    @Column
    private Float poids;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = SEXE_POISSON, referencedColumnName = ValeurQualitative.ID_JPA, nullable = true)
    private ValeurQualitative sexePoisson;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = SEXUALITE_POISSON, referencedColumnName = ValeurQualitative.ID_JPA, nullable = true)
    private ValeurQualitative maturitePoisson;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = MALFORMATION_POISSON, referencedColumnName = ValeurQualitative.ID_JPA, nullable = true)
    private ValeurQualitative malformation;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = ETAT_ESTOMAC_POISSON, referencedColumnName = ValeurQualitative.ID_JPA, nullable = true)
    private ValeurQualitative etatEstomac;

    @Column(nullable = false)
    private Boolean estomacVide;

    @Column(nullable = false)
    private Integer numeroPilulier;

    @Column(nullable = false)
    private Integer numeroEcaille;

    @Column(nullable = false)
    private Boolean utilisationComptage;

    @Column(nullable = false)
    private Boolean poissonCompte;

    @Column
    private String commentaire;

    /**
     * Classe raccourcis
     * 
     * @return
     */

    public Site getSite() {
        return this.getSousSequence().getPlateforme().getSite();
    }

    @Override
    public String getSecurityPath() {
        return String.format("%s/%s/%s/%s", ((ProjetSiteThemeDatatype) getSousSequence().getSequence().getVersionFile().getDataset().getLeafNode()).getProjetSiteTheme().getProjetSite().getProjet().getCode(), getSite().getPath(),
                ((ProjetSiteThemeDatatype) getSousSequence().getSequence().getVersionFile().getDataset().getLeafNode()).getProjetSiteTheme().getTheme().getCode(), getSousSequence().getSequence().getVersionFile().getDataset().getLeafNode()
                        .getDatatype().getCode());
    }

    @Override
    public List<ISecurityPathWithVariable> getChildren() {
        List<ISecurityPathWithVariable> children = new LinkedList<ISecurityPathWithVariable>();
        for (ValeurMesureContenuStomacaux valeur : valeurs) {
            children.add(valeur);
        }
        return children;
    }

    @Override
    public void setChildren(List<ISecurityPathWithVariable> children) {
        this.valeurs.clear();
        for (ISecurityPathWithVariable child : children) {
            this.valeurs.add((ValeurMesureContenuStomacaux) child);
        }
    }

    @Override
    public Date getDatePrelevement() {
        return this.getSousSequence().getSequence().getDatePrelevement();
    }

    /**
     *
     * @return
     */
    public Long getLigneFichierEchange() {
        return ligneFichierEchange;
    }

    /**
     *
     * @param ligneFichierEchange
     */
    public void setLigneFichierEchange(Long ligneFichierEchange) {
        this.ligneFichierEchange = ligneFichierEchange;
    }

    /**
     *
     * @return
     */
    public SousSequenceContenuStomacaux getSousSequence() {
        return sousSequence;
    }

    /**
     *
     * @param sousSequence
     */
    public void setSousSequence(SousSequenceContenuStomacaux sousSequence) {
        this.sousSequence = sousSequence;
    }

    /**
     *
     * @return
     */
    public List<ValeurMesureContenuStomacaux> getValeurs() {
        return valeurs;
    }

    /**
     *
     * @param valeurs
     */
    public void setValeurs(List<ValeurMesureContenuStomacaux> valeurs) {
        this.valeurs = valeurs;
    }

    /**
     *
     * @return
     */
    public Integer getNumeroPoisson() {
        return numeroPoisson;
    }

    /**
     *
     * @param numeroPoisson
     */
    public void setNumeroPoisson(Integer numeroPoisson) {
        this.numeroPoisson = numeroPoisson;
    }

    /**
     *
     * @return
     */
    public Float getLongueurPoisson() {
        return longueurPoisson;
    }

    /**
     *
     * @param longueurPoisson
     */
    public void setLongueurPoisson(Float longueurPoisson) {
        this.longueurPoisson = longueurPoisson;
    }

    /**
     *
     * @return
     */
    public Float getPoids() {
        return poids;
    }

    /**
     *
     * @param poids
     */
    public void setPoids(Float poids) {
        this.poids = poids;
    }

    /**
     *
     * @return
     */
    public ValeurQualitative getSexePoisson() {
        return sexePoisson;
    }

    /**
     *
     * @param sexePoisson
     */
    public void setSexePoisson(ValeurQualitative sexePoisson) {
        this.sexePoisson = sexePoisson;
    }

    /**
     *
     * @return
     */
    public ValeurQualitative getMaturitePoisson() {
        return maturitePoisson;
    }

    /**
     *
     * @param maturitePoisson
     */
    public void setMaturitePoisson(ValeurQualitative maturitePoisson) {
        this.maturitePoisson = maturitePoisson;
    }

    /**
     *
     * @return
     */
    public ValeurQualitative getMalformation() {
        return malformation;
    }

    /**
     *
     * @param malformation
     */
    public void setMalformation(ValeurQualitative malformation) {
        this.malformation = malformation;
    }

    /**
     *
     * @return
     */
    public ValeurQualitative getEtatEstomac() {
        return etatEstomac;
    }

    /**
     *
     * @param etatEstomac
     */
    public void setEtatEstomac(ValeurQualitative etatEstomac) {
        this.etatEstomac = etatEstomac;
    }

    /**
     *
     * @return
     */
    public Boolean getEstomacVide() {
        return estomacVide;
    }

    /**
     *
     * @param estomacVide
     */
    public void setEstomacVide(Boolean estomacVide) {
        this.estomacVide = estomacVide;
    }

    /**
     *
     * @return
     */
    public Integer getNumeroPilulier() {
        return numeroPilulier;
    }

    /**
     *
     * @param numeroPilulier
     */
    public void setNumeroPilulier(Integer numeroPilulier) {
        this.numeroPilulier = numeroPilulier;
    }

    /**
     *
     * @return
     */
    public Integer getNumeroEcaille() {
        return numeroEcaille;
    }

    /**
     *
     * @param numeroEcaille
     */
    public void setNumeroEcaille(Integer numeroEcaille) {
        this.numeroEcaille = numeroEcaille;
    }

    /**
     *
     * @return
     */
    public Boolean getUtilisationComptage() {
        return utilisationComptage;
    }

    /**
     *
     * @param utilisationComptage
     */
    public void setUtilisationComptage(Boolean utilisationComptage) {
        this.utilisationComptage = utilisationComptage;
    }

    /**
     *
     * @return
     */
    public Boolean getPoissonCompte() {
        return poissonCompte;
    }

    /**
     *
     * @param poissonCompte
     */
    public void setPoissonCompte(Boolean poissonCompte) {
        this.poissonCompte = poissonCompte;
    }

    /**
     *
     * @return
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     *
     * @param commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }
}
