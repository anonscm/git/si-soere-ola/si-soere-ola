package org.inra.ecoinfo.glacpe.dataset.contenustomacaux.entity;

import static javax.persistence.CascadeType.ALL;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Index;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.glacpe.refdata.projetsite.ProjetSite;


/**
 * @author "Guillaume Enrico"
 * 
 */

@Entity
@Table(name = SequenceContenuStomacaux.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {ProjetSite.ID_JPA, "date_prelevement"}))
@org.hibernate.annotations.Table(appliesTo = SequenceContenuStomacaux.TABLE_NAME, indexes = {@Index(name = SequenceContenuStomacaux.INDEX_ATTRIBUTS_DATE, columnNames = {"date_prelevement"}),
        @Index(name = SequenceContenuStomacaux.INDEX_ATTRIBUTS_PSI, columnNames = {ProjetSite.ID_JPA}), @Index(name = SequenceContenuStomacaux.INDEX_ATTRIBUTS_IVF, columnNames = {VersionFile.ID_JPA})})
public class SequenceContenuStomacaux implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    static public final String TABLE_NAME = "sequence_contenus_stomacaux_scontenustomacaux";

    /**
     *
     */
    static public final String ID_JPA = "scontenustomacaux_id";

    /**
     *
     */
    public static final String INDEX_ATTRIBUTS_DATE = "date_contenus_ikey";

    /**
     *
     */
    public static final String INDEX_ATTRIBUTS_PSI = "psi_contenus_ikey";

    /**
     *
     */
    public static final String INDEX_ATTRIBUTS_IVF = "ivf_contenus_ikey";

    /**
     *
     */
    public SequenceContenuStomacaux() {
        super();
    }

    /**
     *
     */
    @Id
    @Column(name = ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column(name = "date_prelevement", nullable = true)
    private Date datePrelevement;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = ProjetSite.ID_JPA, referencedColumnName = ProjetSite.ID_JPA, nullable = false)
    private ProjetSite projetSite;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = VersionFile.ID_JPA, referencedColumnName = VersionFile.ID_JPA, nullable = false)
    private VersionFile versionFile;

    @Column
    private String nomPecheur;

    @Column
    private Float profondeur;

    @Column
    private Float duree;

    @OneToMany(mappedBy = "sequence", cascade = ALL)
    private List<SousSequenceContenuStomacaux> sousSequence = new LinkedList<SousSequenceContenuStomacaux>();

    /**
     *
     * @return
     */
    public Date getDatePrelevement() {
        return datePrelevement;
    }

    /**
     *
     * @param datePrelevement
     */
    public void setDatePrelevement(Date datePrelevement) {
        this.datePrelevement = datePrelevement;
    }

    /**
     *
     * @return
     */
    public ProjetSite getProjetSite() {
        return projetSite;
    }

    /**
     *
     * @param projetSite
     */
    public void setProjetSite(ProjetSite projetSite) {
        this.projetSite = projetSite;
    }

    /**
     *
     * @return
     */
    public VersionFile getVersionFile() {
        return versionFile;
    }

    /**
     *
     * @param versionFile
     */
    public void setVersionFile(VersionFile versionFile) {
        this.versionFile = versionFile;
    }

    /**
     *
     * @return
     */
    public String getNomPecheur() {
        return nomPecheur;
    }

    /**
     *
     * @param nomPecheur
     */
    public void setNomPecheur(String nomPecheur) {
        this.nomPecheur = nomPecheur;
    }

    /**
     *
     * @return
     */
    public Float getProfondeur() {
        return profondeur;
    }

    /**
     *
     * @param profondeur
     */
    public void setProfondeur(Float profondeur) {
        this.profondeur = profondeur;
    }

    /**
     *
     * @return
     */
    public Float getDuree() {
        return duree;
    }

    /**
     *
     * @param duree
     */
    public void setDuree(Float duree) {
        this.duree = duree;
    }

    /**
     *
     * @return
     */
    public List<SousSequenceContenuStomacaux> getSousSequence() {
        return sousSequence;
    }

    /**
     *
     * @param sousSequence
     */
    public void setSousSequence(List<SousSequenceContenuStomacaux> sousSequence) {
        this.sousSequence = sousSequence;
    }
}
