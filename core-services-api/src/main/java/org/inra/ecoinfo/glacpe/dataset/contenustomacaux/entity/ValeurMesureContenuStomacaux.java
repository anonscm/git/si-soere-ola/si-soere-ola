package org.inra.ecoinfo.glacpe.dataset.contenustomacaux.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Index;
import org.inra.ecoinfo.glacpe.refdata.projetsitethemedatatype.ProjetSiteThemeDatatype;
import org.inra.ecoinfo.refdata.valeurqualitative.ValeurQualitative;
import org.inra.ecoinfo.security.entity.ISecurityPathWithVariable;


/**
 * @author "Guillaume Enrico"
 * 
 */

@Entity
@Table(name = ValeurMesureContenuStomacaux.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {ValeurMesureContenuStomacaux.PROIE, MesureContenuStomacaux.ID_JPA}))
@org.hibernate.annotations.Table(appliesTo = ValeurMesureContenuStomacaux.TABLE_NAME, indexes = {@Index(name = ValeurMesureContenuStomacaux.INDEX_ATTRIBUTS_VAR, columnNames = {ValeurMesureContenuStomacaux.PROIE})})
public class ValeurMesureContenuStomacaux implements Serializable, ISecurityPathWithVariable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    static public final String TABLE_NAME = "valeur_mesure_contenus_stomacaux_vmcontenustomacaux";

    /**
     *
     */
    static public final String ID_JPA = "vmcontenustomacaux_id";

    /**
     *
     */
    public static final String INDEX_ATTRIBUTS_VAR = "vq_contenus_ikey";

    /**
     *
     */
    static public final String PROIE = "proie";

    /**
     *
     */
    public ValeurMesureContenuStomacaux() {
        super();
    }

    /**
     *
     */
    @Id
    @Column(name = ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = MesureContenuStomacaux.ID_JPA, referencedColumnName = MesureContenuStomacaux.ID_JPA, nullable = false)
    private MesureContenuStomacaux mesure;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = PROIE, referencedColumnName = ValeurQualitative.ID_JPA, nullable = true)
    private ValeurQualitative proie;

    @Column(nullable = true)
    private Float valeur;

    /**
     *
     * @return
     */
    public MesureContenuStomacaux getMesure() {
        return mesure;
    }

    /**
     *
     * @param mesure
     */
    public void setMesure(MesureContenuStomacaux mesure) {
        this.mesure = mesure;
    }

    /**
     *
     * @return
     */
    public Float getValeur() {
        return valeur;
    }

    /**
     *
     * @param valeur
     */
    public void setValeur(Float valeur) {
        this.valeur = valeur;
    }

    /**
     * Classe raccourcis
     * 
     * @return
     */

    @Override
    public String getSecurityPathWithVariable() {
        return String.format("%s/%s/%s/%s/%s", ((ProjetSiteThemeDatatype) getMesure().getSousSequence().getSequence().getVersionFile().getDataset().getLeafNode()).getProjetSiteTheme().getProjetSite().getProjet().getCode(), getMesure().getSite()
                .getPath(), ((ProjetSiteThemeDatatype) getMesure().getSousSequence().getSequence().getVersionFile().getDataset().getLeafNode()).getProjetSiteTheme().getTheme().getCode(), getMesure().getSousSequence().getSequence().getVersionFile()
                .getDataset().getLeafNode().getDatatype().getCode(), getProie().getCode());
    }

    @Override
    public Date getDatePrelevement() {
        return this.getMesure().getSousSequence().getSequence().getDatePrelevement();
    }

    /**
     *
     * @return
     */
    public ValeurQualitative getProie() {
        return proie;
    }

    /**
     *
     * @param proie
     */
    public void setProie(ValeurQualitative proie) {
        this.proie = proie;
    }
}
