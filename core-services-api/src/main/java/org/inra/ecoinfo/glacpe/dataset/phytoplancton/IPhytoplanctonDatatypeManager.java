package org.inra.ecoinfo.glacpe.dataset.phytoplancton;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.inra.ecoinfo.glacpe.extraction.vo.VariableVO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.projetsite.ProjetSite;
import org.inra.ecoinfo.glacpe.refdata.taxon.Taxon;
import org.inra.ecoinfo.glacpe.refdata.taxon.TaxonProprieteTaxon;
import org.inra.ecoinfo.glacpe.refdata.taxon.ValeurProprieteTaxon;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public interface IPhytoplanctonDatatypeManager {

    /**
     *
     */
    public static final String CODE_DATATYPE_PHYTOPLANCTON = "phytoplancton";

    /**
     *
     * @param plateformesIds
     * @param firstDate
     * @param endDate
     * @return
     * @throws BusinessException
     */
    List<VariableVO> retrieveListsVariables(List<Long> plateformesIds, Date firstDate, Date endDate) throws BusinessException;

    /**
     *
     * @return
     * @throws BusinessException
     */
    List<Projet> retrievePhytoplanctonAvailablesProjets() throws BusinessException;

    /**
     *
     * @return
     * @throws BusinessException
     */
    Map<Taxon, Map<String, ValeurProprieteTaxon>> retrievePhytoplanctonAvailablesTaxons() throws BusinessException;
    /**
     *
     * @return
     * @throws BusinessException
     */
    List<Taxon> retrievePhytoplanctonRootTaxon() throws BusinessException;

    /**
     *
     * @param projetSiteId
     * @return
     * @throws BusinessException
     */
    List<Plateforme> retrievePhytoplanctonAvailablesPlateformesByProjetSiteId(long projetSiteId) throws BusinessException;

    /**
     *
     * @param projetId
     * @param siteId
     * @return
     * @throws BusinessException
     */
    ProjetSite retrievePhytoplanctonAvailablesProjetsSiteByProjetAndSite(long projetId, long siteId) throws BusinessException;

    /**
     *
     * @param taxonId
     * @return
     * @throws BusinessException
     */
    List<TaxonProprieteTaxon> retrieveTaxonPropertyByTaxonId(long taxonId) throws BusinessException;

    /**
     *
     * @param taxonCode
     * @return
     * @throws BusinessException
     */
    Taxon retrieveTaxonByCode(String taxonCode) throws BusinessException;
}
