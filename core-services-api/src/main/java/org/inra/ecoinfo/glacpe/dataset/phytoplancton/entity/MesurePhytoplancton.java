package org.inra.ecoinfo.glacpe.dataset.phytoplancton.entity;

import static javax.persistence.CascadeType.ALL;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Index;
import org.inra.ecoinfo.glacpe.refdata.projetsitethemedatatype.ProjetSiteThemeDatatype;
import org.inra.ecoinfo.glacpe.refdata.taxon.Taxon;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.security.entity.ISecurityPath;
import org.inra.ecoinfo.security.entity.ISecurityPathWithVariable;


/**
 * @author "Guillaume Enrico"
 * 
 */

@Entity
@Table(name = MesurePhytoplancton.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {SousSequencePhytoplancton.ID_JPA, Taxon.ID_JPA}))
@org.hibernate.annotations.Table(appliesTo = MesurePhytoplancton.TABLE_NAME, indexes = {@Index(name = Taxon.ID_JPA, columnNames = {Taxon.ID_JPA})})
public class MesurePhytoplancton implements Serializable, ISecurityPath {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    static public final String TABLE_NAME = "mesure_phytoplancton_mphytoplancton";

    /**
     *
     */
    static public final String ID_JPA = "mphytoplancton_id";

    /**
     *
     */
    public MesurePhytoplancton() {
        super();
    }

    /**
     *
     */
    @Id
    @Column(name = "mphytoplancton_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column(name = "ligne_fichier_echange")
    private Long ligneFichierEchange;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = SousSequencePhytoplancton.ID_JPA, referencedColumnName = SousSequencePhytoplancton.ID_JPA, nullable = false)
    private SousSequencePhytoplancton sousSequence;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = Taxon.ID_JPA, referencedColumnName = Taxon.ID_JPA, nullable = false)
    private Taxon taxon;

    @OneToMany(mappedBy = "mesure", cascade = ALL)
    @OrderBy("variable")
    private List<ValeurMesurePhytoplancton> valeurs = new LinkedList<ValeurMesurePhytoplancton>();

    /**
     * Classe raccourcis
     * 
     * @return
     */
    public Site getSite() {
        return this.getSousSequence().getPlateforme().getSite();
    }

    /**
     *
     * @return
     */
    public Long getLigneFichierEchange() {
        return ligneFichierEchange;
    }

    /**
     *
     * @param ligneFichierEchange
     */
    public void setLigneFichierEchange(Long ligneFichierEchange) {
        this.ligneFichierEchange = ligneFichierEchange;
    }

    /**
     *
     * @return
     */
    public SousSequencePhytoplancton getSousSequence() {
        return sousSequence;
    }

    /**
     *
     * @param sousSequence
     */
    public void setSousSequence(SousSequencePhytoplancton sousSequence) {
        this.sousSequence = sousSequence;
    }

    /**
     *
     * @return
     */
    public List<ValeurMesurePhytoplancton> getValeurs() {
        return valeurs;
    }

    /**
     *
     * @param valeurs
     */
    public void setValeurs(List<ValeurMesurePhytoplancton> valeurs) {
        this.valeurs = valeurs;
    }

    /**
     *
     * @return
     */
    public Taxon getTaxon() {
        return taxon;
    }

    /**
     *
     * @param taxon
     */
    public void setTaxon(Taxon taxon) {
        this.taxon = taxon;
    }

    @Override
    public String getSecurityPath() {
        return String.format("%s/%s/%s/%s", ((ProjetSiteThemeDatatype) getSousSequence().getSequence().getVersionFile().getDataset().getLeafNode()).getProjetSiteTheme().getProjetSite().getProjet().getCode(), getSite().getPath(),
                ((ProjetSiteThemeDatatype) getSousSequence().getSequence().getVersionFile().getDataset().getLeafNode()).getProjetSiteTheme().getTheme().getCode(), getSousSequence().getSequence().getVersionFile().getDataset().getLeafNode()
                        .getDatatype().getCode());
    }

    @Override
    public List<ISecurityPathWithVariable> getChildren() {
        List<ISecurityPathWithVariable> children = new LinkedList<ISecurityPathWithVariable>();
        for (ValeurMesurePhytoplancton valeur : valeurs) {
            children.add(valeur);
        }
        return children;
    }

    @Override
    public void setChildren(List<ISecurityPathWithVariable> children) {
        this.valeurs.clear();
        for (ISecurityPathWithVariable child : children) {
            this.valeurs.add((ValeurMesurePhytoplancton) child);
        }
    }

    @Override
    public Date getDatePrelevement() {
        return this.getSousSequence().getSequence().getDatePrelevement();
    }
}