package org.inra.ecoinfo.glacpe.dataset.phytoplancton.entity;

import static javax.persistence.CascadeType.ALL;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Index;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.OutilsMesure;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;


/**
 * @author "Guillaume Enrico"
 * 
 */
@Entity
@Table(name = SousSequencePhytoplancton.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {SequencePhytoplancton.ID_JPA, Plateforme.ID_JPA}))
@org.hibernate.annotations.Table(appliesTo = SousSequencePhytoplancton.TABLE_NAME, indexes = {@Index(name = SousSequencePhytoplancton.INDEX_ATTRIBUTS_PLATEFORME, columnNames = {Plateforme.ID_JPA})})
public class SousSequencePhytoplancton implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    static public final String TABLE_NAME = "sous_sequence_phytoplancton_ssphytoplancton";

    /**
     *
     */
    static public final String ID_JPA = "ssphytoplancton_id";

    /**
     *
     */
    public static final String INDEX_ATTRIBUTS_PLATEFORME = "plateforme_phyto_ikey";

    /**
     *
     */
    @Id
    @Column(name = ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = SequencePhytoplancton.ID_JPA, referencedColumnName = SequencePhytoplancton.ID_JPA, nullable = false)
    private SequencePhytoplancton sequence;

    @OneToMany(mappedBy = "sousSequence", cascade = ALL)
    private List<MesurePhytoplancton> mesures = new LinkedList<MesurePhytoplancton>();

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = Plateforme.ID_JPA, referencedColumnName = Plateforme.ID_JPA, nullable = false)
    private Plateforme plateforme;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "mesure_id", referencedColumnName = OutilsMesure.ID_JPA, nullable = true)
    private OutilsMesure outilsMesure;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "prelevement_id", referencedColumnName = OutilsMesure.ID_JPA, nullable = true)
    private OutilsMesure outilsPrelevement;

    @Column(name = "surface_comptage", nullable = true)
    private Float surfaceComptage;

    @Column(name = "profondeur_min", nullable = true)
    private Float profondeurMin;

    @Column(name = "profondeur_max", nullable = true)
    private Float profondeurMax;

    /**
     *
     * @return
     */
    public SequencePhytoplancton getSequence() {
        return sequence;
    }

    /**
     *
     * @param sequence
     */
    public void setSequence(SequencePhytoplancton sequence) {
        this.sequence = sequence;
    }

    /**
     *
     * @return
     */
    public List<MesurePhytoplancton> getMesures() {
        return mesures;
    }

    /**
     *
     * @param mesures
     */
    public void setMesures(List<MesurePhytoplancton> mesures) {
        this.mesures = mesures;
    }

    /**
     *
     * @return
     */
    public Plateforme getPlateforme() {
        return plateforme;
    }

    /**
     *
     * @param plateforme
     */
    public void setPlateforme(Plateforme plateforme) {
        this.plateforme = plateforme;
    }

    /**
     *
     * @return
     */
    public Float getProfondeurMin() {
        return profondeurMin;
    }

    /**
     *
     * @param profondeurMin
     */
    public void setProfondeurMin(Float profondeurMin) {
        this.profondeurMin = profondeurMin;
    }

    /**
     *
     * @return
     */
    public Float getProfondeurMax() {
        return profondeurMax;
    }

    /**
     *
     * @param profondeurMax
     */
    public void setProfondeurMax(Float profondeurMax) {
        this.profondeurMax = profondeurMax;
    }

    /**
     *
     * @return
     */
    public Float getSurfaceComptage() {
        return surfaceComptage;
    }

    /**
     *
     * @param surfaceComptage
     */
    public void setSurfaceComptage(Float surfaceComptage) {
        this.surfaceComptage = surfaceComptage;
    }

    /**
     *
     * @return
     */
    public OutilsMesure getOutilsMesure() {
        return outilsMesure;
    }

    /**
     *
     * @param outilsMesure
     */
    public void setOutilsMesure(OutilsMesure outilsMesure) {
        this.outilsMesure = outilsMesure;
    }

    /**
     *
     * @return
     */
    public OutilsMesure getOutilsPrelevement() {
        return outilsPrelevement;
    }

    /**
     *
     * @param outilsPrelevement
     */
    public void setOutilsPrelevement(OutilsMesure outilsPrelevement) {
        this.outilsPrelevement = outilsPrelevement;
    }
}