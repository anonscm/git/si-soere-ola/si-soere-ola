package org.inra.ecoinfo.glacpe.dataset.phytoplancton.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Index;
import org.inra.ecoinfo.glacpe.dataset.IGLACPEAggregateData;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.OutilsMesure;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.projetsitethemedatatype.ProjetSiteThemeDatatype;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.security.entity.ISecurityPathWithVariable;


/**
 * @author "Guillaume Enrico"
 * 
 */
@Entity
@Table(name = ValeurMesurePhytoplancton.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {VariableGLACPE.ID_JPA, MesurePhytoplancton.ID_JPA}))
@org.hibernate.annotations.Table(appliesTo = ValeurMesurePhytoplancton.TABLE_NAME, indexes = {@Index(name = ValeurMesurePhytoplancton.INDEX_ATTRIBUTS_VAR, columnNames = {VariableGLACPE.ID_JPA})})
public class ValeurMesurePhytoplancton implements Serializable, IGLACPEAggregateData, ISecurityPathWithVariable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    static public final String TABLE_NAME = "valeur_mesure_phytoplancton_vmphytoplancton";

    /**
     *
     */
    static public final String ID_JPA = "vmphytoplancton_id";

    /**
     *
     */
    public static final String INDEX_ATTRIBUTS_VAR = "var_phyto_ikey";

    /**
     *
     */
    public ValeurMesurePhytoplancton() {
        super();
    }

    /**
     *
     */
    @Id
    @Column(name = ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = MesurePhytoplancton.ID_JPA, referencedColumnName = MesurePhytoplancton.ID_JPA, nullable = false)
    private MesurePhytoplancton mesure;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = VariableGLACPE.ID_JPA, referencedColumnName = VariableGLACPE.PERSISTENT_NAME_ID, nullable = false)
    private VariableGLACPE variable;

    @Column(nullable = true)
    private Float valeur;

    /**
     *
     * @param mesure
     */
    public void setMesure(MesurePhytoplancton mesure) {
        this.mesure = mesure;
    }

    /**
     *
     * @return
     */
    public MesurePhytoplancton getMesure() {
        return mesure;
    }

    /**
     *
     * @return
     */
    public VariableGLACPE getVariable() {
        return variable;
    }

    /**
     *
     * @param variable
     */
    public void setVariable(VariableGLACPE variable) {
        this.variable = variable;
    }

    /**
     *
     * @return
     */
    public Float getValeur() {
        return valeur;
    }

    /**
     *
     * @param valeur
     */
    public void setValeur(Float valeur) {
        this.valeur = valeur;
    }

    @Override
    public String getSecurityPathWithVariable() {
        return String.format("%s/%s/%s/%s/%s", ((ProjetSiteThemeDatatype) getMesure().getSousSequence().getSequence().getVersionFile().getDataset().getLeafNode()).getProjetSiteTheme().getProjetSite().getProjet().getCode(), getMesure().getSite()
                .getPath(), ((ProjetSiteThemeDatatype) getMesure().getSousSequence().getSequence().getVersionFile().getDataset().getLeafNode()).getProjetSiteTheme().getTheme().getCode(), getMesure().getSousSequence().getSequence().getVersionFile()
                .getDataset().getLeafNode().getDatatype().getCode(), getVariable().getCode());
    }

    @Override
    public Date getDatePrelevement() {
        return this.getMesure().getSousSequence().getSequence().getDatePrelevement();
    }

    /**
     *
     * @return
     */
    @Override
    public Float getValue() {
        return getValeur();
    }

    /**
     *
     * @return
     */
    @Override
    public Float getDepth() {
        return null;
    }

    /**
     *
     * @return
     */
    @Override
    public Date getDate() {
        return this.getMesure().getSousSequence().getSequence().getDatePrelevement();
    }

    /**
     *
     * @return
     */
    @Override
    public Plateforme getPlateforme() {
        return this.getMesure().getSousSequence().getPlateforme();
    }

    /**
     *
     * @return
     */
    @Override
    public Site getSite() {
        return this.getMesure().getSousSequence().getPlateforme().getSite();
    }

    /**
     *
     * @return
     */
    @Override
    public Projet getProjet() {
        return this.getMesure().getSousSequence().getSequence().getProjetSite().getProjet();
    }

    /**
     *
     * @return
     */
    @Override
    public OutilsMesure getOutilsMesure() {
        return this.getMesure().getSousSequence().getOutilsMesure();
    }

    /**
     *
     * @return
     */
    @Override
    public Date getHeure() {
        return null;
    }

    /**
     *
     * @return
     */
    @Override
    public OutilsMesure getOutilsPrelevement() {
        return this.getMesure().getSousSequence().getOutilsPrelevement();
    }
}
