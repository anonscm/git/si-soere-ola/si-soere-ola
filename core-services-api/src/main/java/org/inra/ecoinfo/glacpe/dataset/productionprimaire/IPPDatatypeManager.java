package org.inra.ecoinfo.glacpe.dataset.productionprimaire;

import java.util.Date;
import java.util.List;

import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public interface IPPDatatypeManager {

    /**
     *
     */
    String CODE_DATATYPE_PP = "";

    /**
     *
     * @return
     * @throws BusinessException
     */
    public List<Site> retrievePPAvailablesSites() throws BusinessException;

    /**
     *
     * @param plateformesIds
     * @param firstDate
     * @param endDate
     * @return
     * @throws BusinessException
     */
    public List<VariableGLACPE> retrievePPAvailablesVariables(List<Long> plateformesIds, Date firstDate, Date endDate) throws BusinessException;

    /**
     *
     * @param id
     * @return
     * @throws BusinessException
     */
    public List<Plateforme> retrievePPAvailablesPlateformesByProjetSiteId(Long id) throws BusinessException;

    /**
     *
     * @return
     * @throws BusinessException
     */
    public List<Projet> retrievePPAvailablesProjets() throws BusinessException;

}
