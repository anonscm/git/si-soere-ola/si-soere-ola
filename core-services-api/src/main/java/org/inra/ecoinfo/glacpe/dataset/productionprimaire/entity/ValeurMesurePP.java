/**
 * OREILacs project - see LICENCE.txt for use created: 31 mars 2009 15:18:53
 */
package org.inra.ecoinfo.glacpe.dataset.productionprimaire.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Index;
import org.inra.ecoinfo.glacpe.dataset.IGLACPEAggregateData;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.OutilsMesure;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.projetsitethemedatatype.ProjetSiteThemeDatatype;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.security.entity.ISecurityPathWithVariable;


/**
 * @author "Antoine Schellenberger"
 * 
 */
@Entity
@Table(name = ValeurMesurePP.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {VariableGLACPE.ID_JPA, MesurePP.ID_JPA}))
@org.hibernate.annotations.Table(appliesTo = ValeurMesurePP.TABLE_NAME, indexes = {@Index(name = ValeurMesurePP.INDEX_ATTRIBUTS_VAR, columnNames = {VariableGLACPE.ID_JPA})})
public class ValeurMesurePP implements Serializable, ISecurityPathWithVariable, IGLACPEAggregateData {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = "vmpp_id";

    /**
     *
     */
    public static final String TABLE_NAME = "valeur_mesure_production_primaire_vmpp";

    /**
     *
     */
    public static final String INDEX_ATTRIBUTS_VAR = "var_pp_ikey";

    /**
     *
     */
    @Id
    @Column(name = ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = MesurePP.ID_JPA, referencedColumnName = MesurePP.ID_JPA, nullable = false)
    private MesurePP mesure;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = VariableGLACPE.ID_JPA, referencedColumnName = VariableGLACPE.VARIABLE_NAME_ID, nullable = false)
    private VariableGLACPE variable;

    @Column(nullable = true)
    private Float valeur;

    /**
     *
     */
    public ValeurMesurePP() {
        super();
    }

    /**
     *
     * @param mesure
     * @param variable
     * @param valeur
     */
    public ValeurMesurePP(MesurePP mesure, VariableGLACPE variable, Float valeur) {
        super();
        this.mesure = mesure;
        this.variable = variable;
        this.valeur = valeur;
        if (!mesure.getValeurs().contains(this))
            mesure.getValeurs().add(this);
    }

    /**
     *
     * @param mesure
     */
    public void setMesure(MesurePP mesure) {
        this.mesure = mesure;
    }

    /**
     *
     * @return
     */
    public MesurePP getMesure() {
        return mesure;
    }

    /**
     *
     * @return
     */
    public VariableGLACPE getVariable() {
        return variable;
    }

    /**
     *
     * @param variable
     */
    public void setVariable(VariableGLACPE variable) {
        this.variable = variable;
    }

    /**
     *
     * @return
     */
    public Float getValeur() {
        return valeur;
    }

    /**
     *
     * @param valeur
     */
    public void setValeur(Float valeur) {
        this.valeur = valeur;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getSecurityPathWithVariable() {
        return String.format("%s/%s/%s/%s/%s", ((ProjetSiteThemeDatatype) getMesure().getSousSequencePP().getSequencePP().getVersionFile().getDataset().getLeafNode()).getProjetSiteTheme().getProjetSite().getProjet().getCode(),
                ((ProjetSiteThemeDatatype) getMesure().getSousSequencePP().getSequencePP().getVersionFile().getDataset().getLeafNode()).getProjetSiteTheme().getProjetSite().getSite().getPath(), ((ProjetSiteThemeDatatype) getMesure()
                        .getSousSequencePP().getSequencePP().getVersionFile().getDataset().getLeafNode()).getProjetSiteTheme().getTheme().getCode(), getMesure().getSousSequencePP().getSequencePP().getVersionFile().getDataset().getLeafNode()
                        .getDatatype().getCode(), getVariable().getCode());
    }

    @Override
    public Date getDatePrelevement() {
        return this.getMesure().getSousSequencePP().getSequencePP().getDate();
    }

    /**
     *
     * @return
     */
    @Override
    public Float getValue() {
        return getValeur();
    }

    /**
     *
     * @return
     */
    @Override
    public Float getDepth() {
        return mesure.getProfondeur();
    }

    /**
     *
     * @return
     */
    @Override
    public Date getDate() {
        return mesure.getSousSequencePP().getSequencePP().getDate();
    }

    /**
     *
     * @return
     */
    @Override
    public Plateforme getPlateforme() {
        return mesure.getSousSequencePP().getPlateforme();
    }

    /**
     *
     * @return
     */
    @Override
    public Site getSite() {
        return getMesure().getSousSequencePP().getSequencePP().getProjetSite().getSite();
    }

    /**
     *
     * @return
     */
    @Override
    public Projet getProjet() {
        return getMesure().getSousSequencePP().getSequencePP().getProjetSite().getProjet();
    }

    /**
     *
     * @return
     */
    @Override
    public OutilsMesure getOutilsMesure() {
        return null;
    }

    /**
     *
     * @return
     */
    @Override
    public Date getHeure() {
        return null;
    }

    /**
     *
     * @return
     */
    @Override
    public OutilsMesure getOutilsPrelevement() {
        // TODO Auto-generated method stub
        return null;
    }
}
