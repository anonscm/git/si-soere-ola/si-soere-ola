package org.inra.ecoinfo.glacpe.dataset.sondemulti;

import java.util.Date;
import java.util.List;

import org.inra.ecoinfo.glacpe.extraction.vo.VariableVO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.projetsite.ProjetSite;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public interface ISondeMultiDatatypeManager {

    /**
     *
     */
    public static final String CODE_DATATYPE_SONDE_MULTI = "sonde_multiparametres";

    /**
     *
     * @param code
     * @return
     * @throws BusinessException
     */
    Unite retrieveVariableUnitByCode(String code) throws BusinessException;

    /**
     *
     * @return
     * @throws BusinessException
     */
    List<Projet> retrieveSondeMultiAvailablesProjets() throws BusinessException;

    /**
     *
     * @param projetSiteId
     * @return
     * @throws BusinessException
     */
    List<Plateforme> retrieveSondeMultiAvailablesPlateformesByProjetSiteId(long projetSiteId) throws BusinessException;

    /**
     *
     * @param plateformesIds
     * @param firstDate
     * @param endDate
     * @return
     * @throws BusinessException
     */
    List<VariableVO> retrieveListsVariables(List<Long> plateformesIds, Date firstDate, Date endDate) throws BusinessException;

    /**
     *
     * @param projetId
     * @param siteId
     * @return
     * @throws BusinessException
     */
    ProjetSite retrieveSondeMultiAvailablesProjetsSiteByProjetAndSite(Long projetId, Long siteId) throws BusinessException;
}
