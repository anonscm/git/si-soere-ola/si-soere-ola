package org.inra.ecoinfo.glacpe.dataset.sondemulti.entity;

import static javax.persistence.CascadeType.ALL;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Index;
import org.inra.ecoinfo.glacpe.refdata.projetsitethemedatatype.ProjetSiteThemeDatatype;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.security.entity.ISecurityPath;
import org.inra.ecoinfo.security.entity.ISecurityPathWithVariable;


/**
 * @author "Guillaume Enrico"
 * 
 */

@Entity
@Table(name = MesureSondeMulti.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {SousSequenceSondeMulti.ID_JPA, "profondeur"}))
@org.hibernate.annotations.Table(appliesTo = MesureSondeMulti.TABLE_NAME, indexes = {@Index(name = "profondeur_sonde_ikey", columnNames = {"profondeur"})})
public class MesureSondeMulti implements Serializable, ISecurityPath {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    static public final String TABLE_NAME = "mesure_sonde_multi_msondemulti";

    /**
     *
     */
    static public final String ID_JPA = "msondemulti_id";

    /**
     *
     */
    @Id
    @Column(name = ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column(name = "ligne_fichier_echange")
    private Long ligneFichierEchange;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = SousSequenceSondeMulti.ID_JPA, referencedColumnName = SousSequenceSondeMulti.ID_JPA, nullable = false)
    private SousSequenceSondeMulti sousSequence;

    @OneToMany(mappedBy = "mesure", cascade = ALL)
    @OrderBy("variable")
    private List<ValeurMesureSondeMulti> valeurs = new LinkedList<ValeurMesureSondeMulti>();

    /**
     * Classe raccourcis
     * 
     * @return
     */
    public Site getSite() {
        return this.getSousSequence().getPlateforme().getSite();
    }

    /**
     *
     */
    public MesureSondeMulti() {
        super();
    }

    @Column(nullable = true)
    private Float profondeur;

    @Temporal(TemporalType.TIME)
    @Column(name = "heure", nullable = true)
    private Date heure;

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public List<ValeurMesureSondeMulti> getValeurs() {
        return valeurs;
    }

    /**
     *
     * @param valeurs
     */
    public void setValeurs(List<ValeurMesureSondeMulti> valeurs) {
        this.valeurs = valeurs;
    }

    /**
     *
     * @return
     */
    public Float getProfondeur() {
        return profondeur;
    }

    /**
     *
     * @param profondeur
     */
    public void setProfondeur(Float profondeur) {
        this.profondeur = profondeur;
    }

    /**
     *
     * @return
     */
    public Date getHeure() {
        return heure;
    }

    /**
     *
     * @param heure
     */
    public void setHeure(Date heure) {
        this.heure = heure;
    }

    /**
     *
     * @return
     */
    public SousSequenceSondeMulti getSousSequence() {
        return sousSequence;
    }

    /**
     *
     * @param sousSequence
     */
    public void setSousSequence(SousSequenceSondeMulti sousSequence) {
        this.sousSequence = sousSequence;
    }

    /**
     *
     * @return
     */
    public Long getLigneFichierEchange() {
        return ligneFichierEchange;
    }

    /**
     *
     * @param ligneFichierEchange
     */
    public void setLigneFichierEchange(Long ligneFichierEchange) {
        this.ligneFichierEchange = ligneFichierEchange;
    }

    @Override
    public String getSecurityPath() {
        return String.format("%s/%s/%s/%s", ((ProjetSiteThemeDatatype) getSousSequence().getSequence().getVersionFile().getDataset().getLeafNode()).getProjetSiteTheme().getProjetSite().getProjet().getCode(), getSite().getPath(),
                ((ProjetSiteThemeDatatype) getSousSequence().getSequence().getVersionFile().getDataset().getLeafNode()).getProjetSiteTheme().getTheme().getCode(), getSousSequence().getSequence().getVersionFile().getDataset().getLeafNode()
                        .getDatatype().getCode());
    }

    @Override
    public List<ISecurityPathWithVariable> getChildren() {
        List<ISecurityPathWithVariable> children = new LinkedList<ISecurityPathWithVariable>();
        for (ValeurMesureSondeMulti valeur : valeurs) {
            children.add(valeur);
        }
        return children;
    }

    @Override
    public void setChildren(List<ISecurityPathWithVariable> children) {
        this.valeurs.clear();
        for (ISecurityPathWithVariable child : children) {
            this.valeurs.add((ValeurMesureSondeMulti) child);
        }
    }

    @Override
    public Date getDatePrelevement() {
        return this.getSousSequence().getSequence().getDatePrelevement();
    }
}
