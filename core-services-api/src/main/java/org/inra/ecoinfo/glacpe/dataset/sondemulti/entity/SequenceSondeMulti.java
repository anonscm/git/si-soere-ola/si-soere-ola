package org.inra.ecoinfo.glacpe.dataset.sondemulti.entity;

import static javax.persistence.CascadeType.ALL;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Index;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.glacpe.refdata.projetsite.ProjetSite;


/**
 * @author "Guillaume Enrico"
 * 
 */
@Entity
@Table(name = SequenceSondeMulti.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {ProjetSite.ID_JPA, "date_prelevement"}))
@org.hibernate.annotations.Table(appliesTo = SequenceSondeMulti.TABLE_NAME, indexes = {@Index(name = SequenceSondeMulti.INDEX_ATTRIBUTS_DATE, columnNames = {"date_prelevement"}),
        @Index(name = SequenceSondeMulti.INDEX_ATTRIBUTS_PSITE, columnNames = {ProjetSite.ID_JPA}), @Index(name = SequenceSondeMulti.INDEX_ATTRIBUTS_IVF, columnNames = {VersionFile.ID_JPA})})
public class SequenceSondeMulti implements Serializable {

    /**
     *
     */
    public SequenceSondeMulti() {
        super();
    }

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    static public final String TABLE_NAME = "sequence_sonde_multi_ssondemulti";

    /**
     *
     */
    static public final String ID_JPA = "ssondemulti_id";

    /**
     *
     */
    public static final String INDEX_ATTRIBUTS_DATE = "date_sonde_ikey";

    /**
     *
     */
    public static final String INDEX_ATTRIBUTS_PSITE = "psi_sonde_ikey";

    /**
     *
     */
    public static final String INDEX_ATTRIBUTS_IVF = "ivf_sonde_ikey";

    /**
     *
     */
    @Id
    @Column(name = ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column(name = "date_prelevement", nullable = true)
    private Date datePrelevement;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = ProjetSite.ID_JPA, referencedColumnName = ProjetSite.ID_JPA, nullable = false)
    private ProjetSite projetSite;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = VersionFile.ID_JPA, referencedColumnName = VersionFile.ID_JPA, nullable = false)
    private VersionFile versionFile;

    @OneToMany(mappedBy = "sequence", cascade = ALL)
    private List<SousSequenceSondeMulti> sousSequences = new LinkedList<SousSequenceSondeMulti>();

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public Date getDatePrelevement() {
        return datePrelevement;
    }

    /**
     *
     * @param datePrelevement
     */
    public void setDatePrelevement(Date datePrelevement) {
        this.datePrelevement = datePrelevement;
    }

    /**
     *
     * @return
     */
    public ProjetSite getProjetSite() {
        return projetSite;
    }

    /**
     *
     * @param projetSite
     */
    public void setProjetSite(ProjetSite projetSite) {
        this.projetSite = projetSite;
    }

    /**
     *
     * @return
     */
    public List<SousSequenceSondeMulti> getSousSequences() {
        return sousSequences;
    }

    /**
     *
     * @param sousSequences
     */
    public void setSousSequences(List<SousSequenceSondeMulti> sousSequences) {
        this.sousSequences = sousSequences;
    }

    /**
     *
     * @return
     */
    public VersionFile getVersionFile() {
        return versionFile;
    }

    /**
     *
     * @param versionFile
     */
    public void setVersionFile(VersionFile versionFile) {
        this.versionFile = versionFile;
    }

}
