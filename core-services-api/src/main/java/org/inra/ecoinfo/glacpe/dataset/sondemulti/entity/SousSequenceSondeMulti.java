package org.inra.ecoinfo.glacpe.dataset.sondemulti.entity;

import static javax.persistence.CascadeType.ALL;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Index;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.OutilsMesure;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;


/**
 * @author "Guillaume Enrico"
 * 
 */
@Entity
@Table(name = SousSequenceSondeMulti.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {SequenceSondeMulti.ID_JPA, Plateforme.ID_JPA, OutilsMesure.ID_JPA}))
@org.hibernate.annotations.Table(appliesTo = SousSequenceSondeMulti.TABLE_NAME, indexes = {@Index(name = SousSequenceSondeMulti.INDEX_ATTRIBUTS_PLATEFORME, columnNames = {Plateforme.ID_JPA})})
public class SousSequenceSondeMulti implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    static public final String TABLE_NAME = "sous_sequence_sonde_multi_sssondemulti";

    /**
     *
     */
    static public final String ID_JPA = "sssondemulti_id";

    /**
     *
     */
    public static final String INDEX_ATTRIBUTS_PLATEFORME = "plateforme_sonde_ikey";

    /**
     *
     */
    @Id
    @Column(name = ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = SequenceSondeMulti.ID_JPA, referencedColumnName = SequenceSondeMulti.ID_JPA, nullable = false)
    private SequenceSondeMulti sequence;

    @OneToMany(mappedBy = "sousSequence", cascade = ALL)
    private List<MesureSondeMulti> mesures = new LinkedList<MesureSondeMulti>();

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = Plateforme.ID_JPA, referencedColumnName = Plateforme.ID_JPA, nullable = false)
    private Plateforme plateforme;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = OutilsMesure.ID_JPA, referencedColumnName = OutilsMesure.ID_JPA, nullable = false)
    private OutilsMesure outilsMesure;

    @Column(name = "commentaire_sonde", nullable = false)
    private String commentaireSonde;

    /**
     *
     */
    public SousSequenceSondeMulti() {
        super();
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public SequenceSondeMulti getSequence() {
        return sequence;
    }

    /**
     *
     * @param sequence
     */
    public void setSequence(SequenceSondeMulti sequence) {
        this.sequence = sequence;
    }

    /**
     *
     * @return
     */
    public List<MesureSondeMulti> getMesures() {
        return mesures;
    }

    /**
     *
     * @param mesures
     */
    public void setMesures(List<MesureSondeMulti> mesures) {
        this.mesures = mesures;
    }

    /**
     *
     * @return
     */
    public Plateforme getPlateforme() {
        return plateforme;
    }

    /**
     *
     * @param plateforme
     */
    public void setPlateforme(Plateforme plateforme) {
        this.plateforme = plateforme;
    }

    /**
     *
     * @return
     */
    public OutilsMesure getOutilsMesure() {
        return outilsMesure;
    }

    /**
     *
     * @param outilsMesure
     */
    public void setOutilsMesure(OutilsMesure outilsMesure) {
        this.outilsMesure = outilsMesure;
    }

    /**
     *
     * @return
     */
    public String getCommentaires() {
        return commentaireSonde;
    }

    /**
     *
     * @param commentaires
     */
    public void setCommentaires(String commentaires) {
        this.commentaireSonde = commentaires;
    }
}
