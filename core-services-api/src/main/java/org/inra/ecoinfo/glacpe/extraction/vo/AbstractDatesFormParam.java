package org.inra.ecoinfo.glacpe.extraction.vo;

import java.io.PrintStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.inra.ecoinfo.localization.ILocalizationManager;

/**
 *
 * @author ptcherniati
 */
public abstract class AbstractDatesFormParam implements IDateFormParameter {

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.glacpe.extraction.vo.messages";

    private static final String MSG_END_DATE_INVALID = "PROPERTY_MSG_END_DATE_INVALID";
    private static final String MSG_START_DATE_INVALID = "PROPERTY_MSG_START_DATE_INVALID";
    private static final String MSG_END_DATE_EMPTY = "PROPERTY_MSG_END_DATE_EMPTY";
    private static final String MSG_START_DATE_EMPTY = "PROPERTY_MSG_START_DATE_EMPTY";
    private static final String MSG_BAD_PERIODE = "PROPERTY_MSG_BAD_PERIODE";

    /**
     *
     */
    protected static final String MSG_FROM = "PROPERTY_MSG_FROM";

    /**
     *
     */
    protected static final String MSG_TO = "PROPERTY_MSG_TO";

    private static final String OR_CONDITION = "or";

    /**
     *
     */
    protected static final String DATE_FORMAT = "dd/MM/yyyy";

    /**
     *
     */
    protected static final String DATE_FORMAT_WT_YEAR = "dd/MM";

    /**
     *
     * @return
     */
    public abstract boolean isEmpty();

    /**
     *
     */
    protected static final String INITIAL_SQL_CONDITION = "and (";

    /**
     *
     */
    protected static final String SQL_CONDITION = "%s between :%s and :%s or ";

    /**
     *
     */
    protected static final String END_PREFIX = "end";

    /**
     *
     */
    protected static final String START_PREFIX = "start";

    /**
     *
     */
    protected static final DateFormat dateFormatter = new SimpleDateFormat(DATE_FORMAT);

    /**
     *
     */
    protected List<Map<String, String>> periods;

    /**
     *
     */
    public static final String START_INDEX = "start";

    /**
     *
     */
    public static final String END_INDEX = "end";

    /**
     *
     */
    protected ILocalizationManager localizationManager;

    /**
     *
     */
    public abstract void customValidate();

    /**
     *
     */
    protected String SQLCondition;

    /**
     *
     */
    protected Map<String, Date> parametersMap = new HashMap<String, Date>();

    /**
     *
     */
    protected List<String> validityMessages = new LinkedList<String>();

    /**
     *
     * @return
     * @throws ParseException
     */
    public abstract String getSummaryHTML() throws ParseException;

    /**
     *
     * @param printStream
     * @throws ParseException
     */
    public abstract void buildSummary(PrintStream printStream) throws ParseException;

    /**
     *
     * @param HQLAliasDate
     * @throws ParseException
     */
    protected abstract void buildParameterMapAndSQLCondition(String HQLAliasDate) throws ParseException;

    /**
     *
     * @return
     */
    public abstract String getPatternDate();

    /**
     *
     * @param HQLAliasDate
     * @return
     * @throws ParseException
     */
    public String buildSQLCondition(String HQLAliasDate) throws ParseException {

        retrieveParametersMap(HQLAliasDate);
        return SQLCondition;
    }

    /**
     *
     * @param localizationManager
     */
    public AbstractDatesFormParam(ILocalizationManager localizationManager) {
        super();
        this.localizationManager = localizationManager;
    }

    /**
     *
     */
    public AbstractDatesFormParam() {
        super();
    }

    /**
     *
     * @param HQLAliasDate
     * @return
     * @throws ParseException
     */
    public Map<String, Date> retrieveParametersMapAndBuildSQLCondition(String HQLAliasDate) throws ParseException {
        return retrieveParametersMap(HQLAliasDate);
    }

    private Map<String, Date> retrieveParametersMap(String HQLAliasDate) throws ParseException {

        SQLCondition = INITIAL_SQL_CONDITION;
        buildParameterMapAndSQLCondition(HQLAliasDate);
        if (SQLCondition.lastIndexOf(OR_CONDITION) != -1) {
            SQLCondition = String.format("%s%s", SQLCondition.substring(0, SQLCondition.lastIndexOf(OR_CONDITION)), ")");
        } else {
            SQLCondition = String.format("%s%s", SQLCondition, ")");
        }

        return parametersMap;
    }

    /**
     *
     * @param period
     * @return
     */
    protected boolean testPeriodEmpty(String period) {
        if (period == null || period.trim().length() == 0) {
            return true;
        }
        return false;
    }

    /**
     *
     * @param currentYear
     * @param periodStart
     * @param periodEnd
     * @return
     * @throws ParseException
     */
    protected String buildPeriodString(int currentYear, String periodStart, String periodEnd) throws ParseException {
        return String.format("%s %s/%d %s %s/%d", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_FROM), periodStart, currentYear, getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_TO), periodEnd, currentYear);

    }

    /**
     *
     * @param currentYear
     * @param periodIndex
     * @param periodStart
     * @param periodEnd
     * @param HQLAliasDate
     * @throws ParseException
     */
    protected void buildPeriod(int currentYear, int periodIndex, String periodStart, String periodEnd, String HQLAliasDate) throws ParseException {
        if (periodStart != null && periodEnd != null && periodStart.trim().length() > 0 && periodEnd.trim().length() > 0) {
            String startParameterName = processDate(START_PREFIX, currentYear, periodIndex, periodStart);
            String endParameterName = processDate(END_PREFIX, currentYear, periodIndex, periodEnd);

            SQLCondition = SQLCondition.concat(String.format(SQL_CONDITION, HQLAliasDate, startParameterName, endParameterName));
        }

    }

    /**
     *
     * @param prefix
     * @param currentYear
     * @param periodIndex
     * @param date
     * @return
     * @throws ParseException
     */
    protected String processDate(String prefix, int currentYear, int periodIndex, String date) throws ParseException {
        String parameterName = String.format("%s%d%d", prefix, periodIndex, currentYear);
        Date formattedDate = dateFormatter.parse(String.format("%s/%d", date, currentYear));
        parametersMap.put(parameterName, formattedDate);
        return parameterName;
    }

    /**
     *
     * @param printStream
     */
    protected void printValidityMessages(PrintStream printStream) {
        printStream.println("<ul>");
        for (String validityMessage : validityMessages) {
            printStream.println(String.format("<li>%s</li>", validityMessage));
        }
        printStream.println("</ul>");
    }

    /**
     *
     */
    protected void testValidityPeriods() {
        Integer index = 0;
        DateFormat dateFormat = new SimpleDateFormat(getPatternDate());
        for (Map<String, String> periodsMap : periods) {
            Date startDate = null;
            Date endDate = null;
            if (periodsMap.get(START_INDEX) == null || periodsMap.get(START_INDEX).trim().isEmpty()) {
                validityMessages.add(String.format(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_START_DATE_EMPTY), index + 1, getPatternDate()));
            } else {
                try {
                    if (!periodsMap.get(START_INDEX).matches(getPatternDate().replaceAll("[^/]", ".")))
                        throw new ParseException("", 0);
                    startDate = dateFormat.parse(periodsMap.get(START_INDEX));
                    if (periodsMap.get(START_INDEX).compareTo(dateFormat.format(startDate)) != 0) {
                        validityMessages.add(String.format(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_START_DATE_INVALID), index + 1, getPatternDate(), periodsMap.get(START_INDEX)));
                    }
                } catch (ParseException e) {
                    validityMessages.add(String.format(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_START_DATE_INVALID), index + 1, getPatternDate(), periodsMap.get(START_INDEX)));
                }
            }

            if (periodsMap.get(END_INDEX) == null || periodsMap.get(END_INDEX).trim().isEmpty()) {
                validityMessages.add(String.format(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_END_DATE_EMPTY), index + 1, getPatternDate()));
            } else {
                try {
                    if (!periodsMap.get(END_INDEX).matches(getPatternDate().replaceAll("[^/]", ".")))
                        throw new ParseException("", 0);
                    endDate = dateFormat.parse(periodsMap.get(END_INDEX));
                    if (periodsMap.get(END_INDEX).compareTo(dateFormat.format(endDate)) != 0) {
                        validityMessages.add(String.format(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_END_DATE_INVALID), index + 1, getPatternDate(), periodsMap.get(END_INDEX)));
                    }
                } catch (ParseException e) {
                    validityMessages.add(String.format(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_END_DATE_INVALID), index + 1, getPatternDate(), periodsMap.get(END_INDEX)));
                }
            }
            if (startDate != null && endDate != null && startDate.compareTo(endDate) > 0) {
                validityMessages.add(String.format(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_BAD_PERIODE), index + 1));
            }

            index++;
        }
    }

    /**
     *
     * @return
     */
    public Boolean getIsValid() {
        validityMessages = new LinkedList<String>();

        customValidate();

        testValidityPeriods();

        if (validityMessages.isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @return
     */
    public List<Map<String, String>> getPeriods() {
        return periods;
    }

    /**
     *
     * @param periods
     */
    public void setPeriods(List<Map<String, String>> periods) {
        this.periods = periods;
    }

    /**
     *
     * @return
     */
    public List<String> getValidityMessages() {
        return validityMessages;
    }

    /**
     *
     * @return
     * @throws ParseException
     */
    public Map<String, Date> retrieveParametersMap() throws ParseException {
        retrieveParametersMap("");
        return parametersMap;
    }

    /**
     *
     */
    public class Periode {

        private String dateStart;
        private String dateEnd;

        /**
         *
         * @param dateStart
         * @param dateEnd
         */
        public Periode(String dateStart, String dateEnd) {
            super();
            this.dateStart = dateStart;
            this.dateEnd = dateEnd;
        }

        /**
         *
         * @param dateStart
         */
        public void setDateStart(String dateStart) {
            this.dateStart = dateStart;
        }

        /**
         *
         * @return
         */
        public String getDateStart() {
            return dateStart;
        }

        /**
         *
         * @param dateEnd
         */
        public void setDateEnd(String dateEnd) {
            this.dateEnd = dateEnd;
        }

        /**
         *
         * @return
         */
        public String getDateEnd() {
            return dateEnd;
        }
    }

    /**
     *
     * @return
     */
    public ILocalizationManager getLocalizationManager() {
        return localizationManager;
    }

    /**
     *
     * @param localizationManager
     */
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

}
