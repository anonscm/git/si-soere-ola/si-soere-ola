/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.glacpe.extraction.vo;

import java.util.List;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.variable.Variable;

/**
 *
 * @author Antoine Schellenberger
 */
public class DatatypeGenericVO {

    private final Boolean isDataAvailable;
    private final Dataset dataset;
    private List<Variable> variables;
    final private DataType dataType;

    public Boolean getIsDataAvailable() {
        return isDataAvailable;
    }

    public DataType getDatatype() {
        return dataType;
    }

    public Dataset getDataset() {
        return dataset;
    }

    public DatatypeGenericVO(DataType dataType, Boolean hasValue, Dataset dataset) {
        this.dataType = dataType;
        this.isDataAvailable = hasValue;
        this.dataset = dataset;
    }

    public void setVariables(List<Variable> variables) {
        this.variables = variables;
    }

    public List<Variable> getVariables() {
        return variables;
    }


  

}
