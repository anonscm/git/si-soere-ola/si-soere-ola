package org.inra.ecoinfo.glacpe.extraction.vo;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.inra.ecoinfo.localization.ILocalizationManager;

/**
 *
 * @author ptcherniati
 */
public class DatesYearsContinuousFormParamVO extends AbstractDatesFormParam implements Cloneable {

    /**
     *
     */
    public static final String LABEL = "DatesYearsContinuousFormParam";

    /**
     *
     * @param localizationManager
     */
    public DatesYearsContinuousFormParamVO(ILocalizationManager localizationManager) {
        super(localizationManager);
    }

    /**
     *
     */
    public DatesYearsContinuousFormParamVO() {
        super();
    }

    /**
     *
     * @return
     */
    @Override
    public boolean isEmpty() {
        for (Map<String, String> periodsMap : periods) {
            for (String key : periodsMap.keySet()) {
                if (testPeriodEmpty(periodsMap.get(key))) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     *
     * @param HQLAliasDate
     * @throws ParseException
     */
    @Override
    protected void buildParameterMapAndSQLCondition(String HQLAliasDate) throws ParseException {
        int periodIndex = 0;
        for (Map<String, String> periodsMap : periods) {
            buildContinuousPeriod(periodIndex++, periodsMap.get(START_INDEX), periodsMap.get(END_INDEX), HQLAliasDate);
        }
    }

    /**
     *
     * @param printStream
     * @throws ParseException
     */
    @Override
    public void buildSummary(PrintStream printStream) throws ParseException {

        for (Map<String, String> periodsMap : periods) {

            printStream.println(String.format("    %s", buildContinuousPeriodString(periodsMap.get(START_INDEX), periodsMap.get(END_INDEX))));
        }
    }

    /**
     *
     * @return
     * @throws ParseException
     */
    @Override
    public String getSummaryHTML() throws ParseException {

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(bos);

        if (!getIsValid()) {
            printValidityMessages(printStream);
            return bos.toString();
        }

        for (Map<String, String> periodsMap : periods) {

            printStream.println(String.format("    %s<br/>", buildContinuousPeriodString(periodsMap.get(START_INDEX), periodsMap.get(END_INDEX))));
        }
        return bos.toString();
    }

    /**
     *
     * @param periodStart
     * @param periodEnd
     * @return
     * @throws ParseException
     */
    protected String buildContinuousPeriodString(String periodStart, String periodEnd) throws ParseException {
        return String.format("%s %s %s %s", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_FROM), periodStart, getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_TO), periodEnd);
    }

    /**
     *
     * @param periodIndex
     * @param periodStart
     * @param periodEnd
     * @param HQLAliasDate
     * @throws ParseException
     */
    protected void buildContinuousPeriod(int periodIndex, String periodStart, String periodEnd, String HQLAliasDate) throws ParseException {
        if (periodStart != null && periodEnd != null && periodStart.trim().length() > 0 && periodEnd.trim().length() > 0) {
            Date formattedStartDate = dateFormatter.parse(periodStart);

            String startParameterName = String.format("%s%d", START_PREFIX, periodIndex);
            String endParameterName = String.format("%s%d", END_PREFIX, periodIndex);

            parametersMap.put(startParameterName, formattedStartDate);

            Date formattedEndDate = dateFormatter.parse(periodEnd);
            parametersMap.put(endParameterName, formattedEndDate);

            SQLCondition = SQLCondition.concat(String.format(SQL_CONDITION, HQLAliasDate, startParameterName, endParameterName));
        }

    }

    /**
     *
     */
    @Override
    public void customValidate() {

    }

    /**
     *
     * @return
     */
    @Override
    public String getPatternDate() {
        return DATE_FORMAT;
    }

    /**
     *
     * @return
     */
    @Override
    public List<Periode> getPeriodsFromDateFormParameter() {

        List<Periode> periodes = new LinkedList<AbstractDatesFormParam.Periode>();
        for (Map<String, String> period : getPeriods()) {
            periodes.add(new Periode(period.get(AbstractDatesFormParam.START_INDEX), period.get(AbstractDatesFormParam.END_INDEX)));
        }
        return periodes;
    }

    public DatesYearsContinuousFormParamVO clone() {
        DatesYearsContinuousFormParamVO dateForm = new DatesYearsContinuousFormParamVO(this.localizationManager);
        List<Map<String, String>> periodMap = new LinkedList<Map<String, String>>();
        for (Map<String, String> period : this.periods) {
            Map<String, String> periodIndex = new HashMap<String, String>();
            for (String key : period.keySet()) {
                periodIndex.put(key, period.get(key));
            }
            periodMap.add(periodIndex);
        }
        dateForm.setPeriods(periodMap);
        return dateForm;
    }
}
