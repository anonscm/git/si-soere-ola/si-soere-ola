package org.inra.ecoinfo.glacpe.extraction.vo;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.xml.bind.ValidationException;

import org.inra.ecoinfo.localization.ILocalizationManager;

/**
 *
 * @author ptcherniati
 */
public class DepthRequestParamVO implements Serializable, Cloneable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.glacpe.ui.messages";

    private static final String MSG_RANGE_DEPTHS_SELECTED = "PROPERTY_MSG_RANGE_DEPTHS_SELECTED";
    private static final String MSG_ALL_DEPTHS_SELECTED = "PROPERTY_MSG_ALL_DEPTHS_SELECTED";
    private static final String PATTERN_STRING_DEPTH_SUMMARY = "   %s: %f";
    private static final String MSG_MAX = "PROPERTY_MSG_MAX";
    private static final String MSG_MIN = "PROPERTY_MSG_MIN";

    private static final String PROPERTY_MSG_BAD_DEPTH = "PROPERTY_MSG_BAD_DEPTH";

    private static final String PROPERTY_MSG_BAD_RATE = "PROPERTY_MSG_BAD_RATE";

    /**
     *
     */
    protected ILocalizationManager localizationManager;

    private Float depthMin = 0f;
    private Float depthMax = 0f;
    private Boolean allDepth = true;
    private Boolean validMinMAxDepht = false;

    /**
     *
     * @param localizationManager
     */
    public DepthRequestParamVO(ILocalizationManager localizationManager) {
        super();
        this.localizationManager = localizationManager;
    }

    /**
     *
     */
    public DepthRequestParamVO() {
        super();
    }

    /**
     *
     * @return
     */
    public Float getDepthMin() {
        return depthMin;
    }

    /**
     *
     * @param depthMin
     */
    public void setDepthMin(Float depthMin) {
        this.depthMin = depthMin;
    }

    /**
     *
     * @return
     */
    public Float getDepthMax() {
        return depthMax;
    }

    /**
     *
     * @param depthMax
     */
    public void setDepthMax(Float depthMax) {
        this.depthMax = depthMax;
    }

    /**
     *
     * @return
     */
    public Boolean getAllDepth() {
        return allDepth;
    }

    /**
     *
     * @param allDepth
     */
    public void setAllDepth(Boolean allDepth) {
        this.allDepth = allDepth;
    }

    /**
     *
     * @return
     */
    public String getSummaryHTML() {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(bos);
        if (getAllDepth()) {
            printStream.println(String.format("<div style='display:block'>%s</div>", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_ALL_DEPTHS_SELECTED)));
        } else {
            printStream.println(String.format("<div style='display:block'>%s:</div>", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_RANGE_DEPTHS_SELECTED)));
            printStream.println(String.format("<ul><li>".concat(PATTERN_STRING_DEPTH_SUMMARY).concat("</li>"), getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_MIN), getDepthMin()));
            printStream.println(String.format("<li>".concat(PATTERN_STRING_DEPTH_SUMMARY).concat("</li></ul>"), getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_MAX), getDepthMax()));
        }
        printStream.println();
        try {
            bos.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bos.toString();
    }

    /**
     *
     * @param printStream
     */
    public void buildSummary(PrintStream printStream) {

        if (getAllDepth()) {
            printStream.println(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_ALL_DEPTHS_SELECTED));
        } else {
            printStream.println(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_RANGE_DEPTHS_SELECTED));
            printStream.println(String.format(PATTERN_STRING_DEPTH_SUMMARY, getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_MIN), getDepthMin()));
            printStream.println(String.format(PATTERN_STRING_DEPTH_SUMMARY, getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_MAX), getDepthMax()));
        }
        printStream.println();

    }

    /**
     *
     * @param context
     * @param component
     * @param value
     * @throws ValidationException
     */
    public void validateDepth(FacesContext context, UIComponent component, Object value) throws ValidationException {
        int depthMin = 0;
        int depthMax = 0;
        String depthMinString = ((UIInput) ((UIInput) component).getParent().getChildren().get(1)).getValue().toString();
        String depthMaxString = ((UIInput) ((UIInput) component).getParent().getChildren().get(2)).getValue().toString();

        try {
            depthMin = Integer.parseInt(depthMinString);
            depthMax = Integer.parseInt(depthMaxString);
        } catch (NumberFormatException e) {
            ((UIInput) component).setValid(false);
            validMinMAxDepht = false;
            context.addMessage(component.getClientId(context), new FacesMessage(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_BAD_DEPTH)));
        }
        if (depthMin > depthMax) {
            ((UIInput) component).setValid(false);
            validMinMAxDepht = false;
            context.addMessage(component.getClientId(context), new FacesMessage(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_BAD_RATE)));
        } else {
            validMinMAxDepht = true;
            ((UIInput) component).setValid(true);
        }

    }

    /**
     *
     * @return
     */
    public Boolean isValidDepht() {
        return allDepth || validMinMAxDepht;
    }

    /**
     *
     * @param validMinMAxDepht
     */
    public void setValidMinMAxDepht(Boolean validMinMAxDepht) {
        this.validMinMAxDepht = validMinMAxDepht;
    }

    /**
     *
     * @return
     */
    public Boolean getValidMinMAxDepht() {
        return validMinMAxDepht;
    }

    /**
     *
     * @return
     */
    public ILocalizationManager getLocalizationManager() {
        return localizationManager;
    }

    /**
     *
     * @param localizationManager
     */
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    public DepthRequestParamVO clone() {
        DepthRequestParamVO depthVO = new DepthRequestParamVO(this.localizationManager);
        depthVO.setAllDepth(new Boolean(this.allDepth));
        depthVO.setDepthMin(new Float(this.depthMin));
        depthVO.setDepthMax(new Float(this.depthMax));
        depthVO.setValidMinMAxDepht(new Boolean(this.validMinMAxDepht));
        return depthVO;
    }
}
