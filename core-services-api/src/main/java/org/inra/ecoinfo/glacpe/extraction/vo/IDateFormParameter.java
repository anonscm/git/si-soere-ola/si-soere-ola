package org.inra.ecoinfo.glacpe.extraction.vo;

import java.util.List;

import org.inra.ecoinfo.glacpe.extraction.vo.AbstractDatesFormParam.Periode;

/**
 *
 * @author ptcherniati
 */
public interface IDateFormParameter {

    /**
     *
     * @return
     */
    List<Periode> getPeriodsFromDateFormParameter();
}