package org.inra.ecoinfo.glacpe.extraction.vo;

import java.io.Serializable;

import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;

/**
 *
 * @author ptcherniati
 */
public class PlateformeVO implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private String code;
    private String nom;
    private String nomSite;
    private String typeSite;
    private ProjetSiteVO projet;

    /**
     *
     * @return
     */
    public ProjetSiteVO getProjet() {
        return projet;
    }

    /**
     *
     */
    public PlateformeVO() {
        super();
    }

    /**
     *
     * @param plateforme
     */
    public PlateformeVO(Plateforme plateforme) {
        this.id = plateforme.getId();
        this.code = plateforme.getCode();
        this.nom = plateforme.getNom();
        this.nomSite = plateforme.getSite().getNom();
    }

    /**
     *
     * @return
     */
    public String getCode() {
        return code;
    }

    /**
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     *
     * @param projet
     */
    public void setProjet(ProjetSiteVO projet) {
        this.projet = projet;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getNom() {
        return nom;
    }

    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     *
     * @return
     */
    public String getNomSite() {
        return nomSite;
    }

    /**
     *
     * @param nomSite
     */
    public void setNomSite(String nomSite) {
        this.nomSite = nomSite;
    }

    /**
     *
     * @return
     */
    public String getTypeSite() {
        return typeSite;
    }

    /**
     *
     * @param typeSite
     */
    public void setTypeSite(String typeSite) {
        this.typeSite = typeSite;
    }

}
