package org.inra.ecoinfo.glacpe.extraction.vo;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import org.inra.ecoinfo.tree.TreeNodeItem;

/**
 *
 * @author ptcherniati
 */
public class PluginDataTypesTreeDescriptionVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String pluginId;

    private List<TreeNodeItem> datatypes = new LinkedList<TreeNodeItem>();

    /**
     *
     * @return
     */
    public String getPluginId() {
        return pluginId;
    }

    /**
     *
     * @param pluginId
     */
    public void setPluginId(String pluginId) {
        this.pluginId = pluginId;
    }

    /**
     *
     * @return
     */
    public List<TreeNodeItem> getDatatypes() {
        return datatypes;
    }

    /**
     *
     * @param datatypes
     */
    public void setDatatypes(List<TreeNodeItem> datatypes) {
        this.datatypes = datatypes;
    }
}
