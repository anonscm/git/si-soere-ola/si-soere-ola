package org.inra.ecoinfo.glacpe.extraction.vo;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import org.inra.ecoinfo.glacpe.refdata.projetsite.ProjetSite;

/**
 *
 * @author ptcherniati
 */
public class ProjetSiteVO implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private Long projetId;
    private String nom;
    private String code;
    private String type;

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     */
    public ProjetSiteVO() {
        super();
    }

    /**
     *
     * @param projetSite
     */
    public ProjetSiteVO(ProjetSite projetSite) {
        this.nom = projetSite.getProjet().getNom();
        this.code = projetSite.getProjet().getCode();
        this.projetId = projetSite.getProjet().getId();
        this.id = projetSite.getId();
        this.type = projetSite.getSite().getTypeSite().getCode();

    }

    /**
     *
     * @return
     */
    public String getNom() {
        return nom;
    }

    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     *
     * @return
     */
    public String getCode() {
        return code;
    }

    /**
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public List<PlateformeVO> getPlateformes() {
        return plateformes;
    }

    /**
     *
     * @param plateformes
     */
    public void setPlateformes(List<PlateformeVO> plateformes) {
        this.plateformes = plateformes;
    }

    private List<PlateformeVO> plateformes = new LinkedList<PlateformeVO>();

    /**
     *
     * @param plateforme
     */
    public void addPlateforme(PlateformeVO plateforme) {
        plateformes.add(plateforme);
        plateforme.setProjet(this);
    }

    /**
     *
     * @param projetId
     */
    public void setProjetId(Long projetId) {
        this.projetId = projetId;
    }

    /**
     *
     * @return
     */
    public Long getProjetId() {
        return projetId;
    }

    /**
     *
     * @return
     */
    public String getType() {
        return type;
    }

    /**
     *
     * @param type
     */
    public void setType(String type) {
        this.type = type;
    }

}
