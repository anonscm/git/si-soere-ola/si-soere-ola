package org.inra.ecoinfo.glacpe.extraction.vo;

import java.io.Serializable;

import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;

/**
 *
 * @author ptcherniati
 */
public class VariableVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private String nom;
    private String code;
    private String group;
    private String affichage;
    private GroupeVariableVO groupeVariable;
    private Integer ordreAffichageGroupe;
    private Boolean isDataAvailable = false;

    /**
     *
     * @return
     */
    public Boolean getIsDataAvailable() {
        return isDataAvailable;
    }

    /**
     *
     * @param isDataAvailable
     */
    public void setIsDataAvailable(Boolean isDataAvailable) {
        this.isDataAvailable = isDataAvailable;
    }

    /**
     *
     * @param variable
     */
    public VariableVO(VariableGLACPE variable) {
        this.id = variable.getId();
        this.nom = variable.getNom();
        this.code = variable.getCode();
        this.affichage = variable.getAffichage();
        this.ordreAffichageGroupe = variable.getOrdreAffichageGroupe();

    }

    /**
     *
     */
    public VariableVO() {

    }

    /**
     *
     * @param variable
     * @param group
     */
    public VariableVO(VariableGLACPE variable, String group) {
        this.id = variable.getId();
        this.nom = variable.getNom();
        this.code = variable.getCode();
        this.affichage = variable.getAffichage();
        this.ordreAffichageGroupe = variable.getOrdreAffichageGroupe();
        this.group = group;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getNom() {
        return nom;
    }

    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     *
     * @return
     */
    public String getCode() {
        return code;
    }

    /**
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     *
     * @return
     */
    public GroupeVariableVO getGroupeVariable() {
        return groupeVariable;
    }

    /**
     *
     * @param groupeVariable
     */
    public void setGroupeVariable(GroupeVariableVO groupeVariable) {
        this.groupeVariable = groupeVariable;
    }

    /**
     *
     * @param affichage
     */
    public void setAffichage(String affichage) {
        this.affichage = affichage;
    }

    /**
     *
     * @return
     */
    public String getAffichage() {
        return affichage;
    }

    /**
     *
     * @return
     */
    public Integer getOrdreAffichageGroupe() {
        return ordreAffichageGroupe;
    }

    /**
     *
     * @param ordreAffichageGroupe
     */
    public void setOrdreAffichageGroupe(Integer ordreAffichageGroupe) {
        this.ordreAffichageGroupe = ordreAffichageGroupe;
    }

    /**
     *
     * @return
     */
    public String getGroup() {
        return group;
    }

    /**
     *
     * @param group
     */
    public void setGroup(String group) {
        this.group = group;
    }

}
