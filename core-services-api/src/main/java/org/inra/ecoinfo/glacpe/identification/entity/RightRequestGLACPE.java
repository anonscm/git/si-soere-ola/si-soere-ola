package org.inra.ecoinfo.glacpe.identification.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.inra.ecoinfo.identification.entity.RightRequest;

/**
 *
 * @author ptcherniati
 */
@Entity
@DiscriminatorValue("rightRequest_glacpe")
@Table(name = RightRequestGLACPE.NAME_ENTITY_JPA)
public class RightRequestGLACPE extends RightRequest {

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "rightrequest_glacpe_rrg";

    @Column
    private String telephone;

    @Column(nullable = false)
    private String variables;

    @Column(nullable = false)
    private String sites;

    @Column(nullable = false)
    private String dates;

    @Column
    private Date dateRequest;

    /**
     *
     */
    public RightRequestGLACPE() {
        super();
    }

    /**
     *
     * @return
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     *
     * @param telephone
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     *
     * @return
     */
    public String getVariables() {
        return variables;
    }

    /**
     *
     * @param variables
     */
    public void setVariables(String variables) {
        this.variables = variables;
    }

    /**
     *
     * @return
     */
    public String getSites() {
        return sites;
    }

    /**
     *
     * @param sites
     */
    public void setSites(String sites) {
        this.sites = sites;
    }

    /**
     *
     * @return
     */
    public String getDates() {
        return dates;
    }

    /**
     *
     * @param dates
     */
    public void setDates(String dates) {
        this.dates = dates;
    }

    /**
     *
     * @return
     */
    public Date getDateRequest() {
        return dateRequest;
    }

    /**
     *
     * @param dateRequest
     */
    public void setDateRequest(Date dateRequest) {
        this.dateRequest = dateRequest;
    }
}
