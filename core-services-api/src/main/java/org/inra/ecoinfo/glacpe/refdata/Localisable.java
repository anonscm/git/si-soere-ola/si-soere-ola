package org.inra.ecoinfo.glacpe.refdata;

/**
 * OREILacs project - see LICENCE.txt for use created: 31 mars 2009 15:27:51
 */

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;


/**
 * @author "Antoine Schellenberger"
 */
@MappedSuperclass
public class Localisable implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String LOCALISABLE_NAME_ID = "loc_id";

    /**
     *
     */
    @Id
    @Column(name = LOCALISABLE_NAME_ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    /**
     *
     */
    @Column(nullable = true)
    protected Float latitude;

    /**
     *
     */
    @Column(nullable = true)
    protected Float longitude;

    /**
     *
     */
    @Column(nullable = true)
    protected Float altitude;

    /**
     *
     */
    public Localisable() {}

    /**
     * @param latitude2
     *            c'est la latitude
     * @param longitude
     * @param altitude
     */
    public Localisable(final Float latitude2, Float longitude, Float altitude) {
        super();
        this.latitude = latitude2;
        this.longitude = longitude;
        this.altitude = altitude;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public Float getLatitude() {
        return latitude;
    }

    /**
     *
     * @param latitude
     */
    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    /**
     *
     * @return
     */
    public Float getLongitude() {
        return longitude;
    }

    /**
     *
     * @param longitude
     */
    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    /**
     *
     * @return
     */
    public Float getAltitude() {
        return altitude;
    }

    /**
     *
     * @param altitude
     */
    public void setAltitude(Float altitude) {
        this.altitude = altitude;
    }

}
