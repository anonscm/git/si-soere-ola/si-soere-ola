package org.inra.ecoinfo.glacpe.refdata.datatypevariableunite;

import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;

import java.util.HashMap;
import java.util.Map;
import javax.persistence.Column;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.MapKey;
import javax.persistence.OneToMany;

import org.inra.ecoinfo.glacpe.refdata.controlecoherence.ControleCoherence;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.datatypevariableunite.DatatypeVariableUnite;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.refdata.variable.Variable;

/**
 *
 * @author ptcherniati
 */
@Entity
@DiscriminatorValue("dvuGLACPE")
@Inheritance(strategy = InheritanceType.JOINED)
public class DatatypeVariableUniteGLACPE extends DatatypeVariableUnite {

    private static final long serialVersionUID = 1L;
    
    //afiocca
    public static final String CODE_SANDRE = "code_sandre";
    public static final String CONTEXTE = "contexte";

    
    public static final String TABLE_NAME = "datatype_variable_unite_glacpe_dvug";


    @OneToMany(mappedBy = "datatypeVariableUnite", cascade = {MERGE, PERSIST, REFRESH})
    @MapKey(name = "site")
    private Map<SiteGLACPE, ControleCoherence> controleCoherence = new HashMap<SiteGLACPE, ControleCoherence>();
   
    @Column(nullable = true,name = CODE_SANDRE, unique = false)
    private String codeSandre;
    
    @Column(nullable = true,name = CONTEXTE, unique = false)
    private String contexte;

    
    
    

    public String getCodeSandre() {
        return codeSandre;
    }

    public void setCodeSandre(String codeSandre) {
        this.codeSandre = codeSandre;
    }
    


    public String getContexte() {
        return contexte;
    }

    public void setContexte(String contexte) {
        this.contexte = contexte;
    }

    
    
    /**
     *
     */
    public DatatypeVariableUniteGLACPE() {
        super();
    }

    /**
     *
     * @param dbDatatype
     * @param dbUnite
     * @param dbVariable
     */
    public DatatypeVariableUniteGLACPE(DataType dbDatatype, Unite dbUnite, Variable dbVariable) {
        super(dbDatatype, dbUnite, dbVariable);
    }

    /**
     *
     * @return
     */
    public Map<SiteGLACPE, ControleCoherence> getControleCoherence() {
        return controleCoherence;
    }

    /**
     *
     * @param controleCoherence
     */
    public void setControleCoherence(Map<SiteGLACPE, ControleCoherence> controleCoherence) {
        this.controleCoherence = controleCoherence;
    }

}
