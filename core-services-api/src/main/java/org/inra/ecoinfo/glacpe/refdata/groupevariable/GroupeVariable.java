package org.inra.ecoinfo.glacpe.refdata.groupevariable;

import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.utils.Utils;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = GroupeVariable.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {GroupeVariable.ATTRIBUTE_CODE_NAME, GroupeVariable.ATTRIBUTE_PARENT_NAME}))
public class GroupeVariable implements Serializable {
    
    //afiocca
    public static final String CODE_SANDRE = "code_sandre";
    public static final String CONTEXTE = "contexte";

    /**
     *
     */
    public static final String ID_JPA = "gro_id";

    /**
     *
     */
    public static final String ATTRIBUTE_NAME_NAME = "nom_groupe";

    /**
     *
     */
    public static final String ATTRIBUTE_CODE_NAME = "code";

    /**
     *
     */
    public static final String ATTRIBUTE_PARENT_NAME = "gro_gro_id";

    /**
     *
     */
    public static final String TABLE_NAME = "groupe_gro";

    /**
     *
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = ID_JPA)
    protected Long id;

    private static final long serialVersionUID = 1L;
    @Column(nullable = false, name = ATTRIBUTE_NAME_NAME)
    private String nom;

    @Column(nullable = false, name = ATTRIBUTE_CODE_NAME)
    private String code;

    @ManyToOne(cascade = {MERGE, PERSIST, REFRESH})
    @JoinColumn(name = ATTRIBUTE_PARENT_NAME, referencedColumnName = ID_JPA, nullable = true)
    private GroupeVariable parent;

    @OneToMany(mappedBy = "parent", cascade = {MERGE, PERSIST, REFRESH})
    @OrderBy("nom")
    private List<GroupeVariable> enfants = new LinkedList<GroupeVariable>();

    @OneToMany(mappedBy = "groupe", cascade = {MERGE, PERSIST, REFRESH})
    @OrderBy("nom")
    private List<VariableGLACPE> variables = new LinkedList<VariableGLACPE>();
    
    
    @Column(nullable = true,name = CODE_SANDRE, unique = false)
    private String codeSandre;
    @Column(nullable = true,name = CONTEXTE , unique = false)
    private String contexte;


    public String getCodeSandre() {
        return codeSandre;
    }

    public void setCodeSandre(String codeSandre) {
        this.codeSandre = codeSandre;
    }

    public String getContexte() {
        return contexte;
    }

    public void setContexte(String contexte) {
        this.contexte = contexte;
    }

    /**
     *
     */
    public GroupeVariable() {
        super();
    }

    /**
     *
     * @param nom
     */
    public GroupeVariable(String nom) {
        super();
        setNom(nom);

    }

    /**
     *
     * @return
     */
    public String getCode() {
        return code;
    }

    /**
     *
     * @return
     */
    public List<GroupeVariable> getEnfants() {
        return enfants;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @return
     */
    public String getNom() {
        return nom;
    }

    /**
     *
     * @return
     */
    public GroupeVariable getParent() {
        return parent;
    }

    /**
     *
     * @return
     */
    public List<VariableGLACPE> getVariables() {
        return variables;
    }

    /**
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     *
     * @param enfants
     */
    public void setEnfants(List<GroupeVariable> enfants) {
        this.enfants = enfants;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
        this.code = Utils.createCodeFromString(nom);
    }

    /**
     *
     * @param parent
     */
    public void setParent(GroupeVariable parent) {
        this.parent = parent;
    }

    /**
     *
     * @param variables
     */
    public void setVariables(List<VariableGLACPE> variables) {
        this.variables = variables;
    }

}
