/**
 * OREILacs project - see LICENCE.txt for use created: 31 mars 2009 13:40:26
 */
package org.inra.ecoinfo.glacpe.refdata.outilsmesures;

import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.inra.ecoinfo.glacpe.refdata.typeoutilsmesure.TypeOutilsMesure;
import org.inra.ecoinfo.utils.Utils;

/**
 * @author "Antoine Schellenberger"
 *
 */
@Entity
@Table(name = OutilsMesure.TABLE_NAME)
public class OutilsMesure implements Serializable {

    //afiocca
    public static final String CODE_SANDRE = "code_sandre";
    public static final String CONTEXTE = "contexte";

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = "ome_id";

    /**
     *
     */
    public static final String TABLE_NAME = "outils_mesure_ome";

    /**
     *
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = ID_JPA)
    protected Long id;

    @ManyToOne(cascade = {MERGE, PERSIST, REFRESH}, optional = false)
    @JoinColumn(name = TypeOutilsMesure.ID_JPA, referencedColumnName = TypeOutilsMesure.ID_JPA, nullable = false)
    private TypeOutilsMesure typeOutilsMesure;

    @Column(nullable = false, unique = true)
    private String nom;

    @Column(columnDefinition = "TEXT")
    private String description;
    private String modele;

    private String fabriquant;

    private String numeroSerie;
    private String etalonnage;
    @Column(nullable = false, unique = true)
    private String code;

    @Column(nullable = true, name = CODE_SANDRE, unique = false)
    private String codeSandre;
    
    @Column(nullable = true, name = CONTEXTE, unique = false)
    private String contexte;

    public String getCodeSandre() {
        return codeSandre;
    }

    public void setCodeSandre(String codeSandre) {
        this.codeSandre = codeSandre;
    }

    public String getContexte() {
        return contexte;
    }

    public void setContexte(String contexte) {
        this.contexte = contexte;
    }

    /**
     *
     */
    public OutilsMesure() {
        super();

    }

    /**
     *
     * @param nom
     * @param description
     * @param modele
     * @param fabriquant
     * @param numeroSerie
     * @param etalonnage
     */
    public OutilsMesure(String nom, String description, String modele, String fabriquant, String numeroSerie, String etalonnage) {
        super();
        setNom(nom);
        this.description = description;
        this.modele = modele;
        this.fabriquant = fabriquant;
        this.numeroSerie = numeroSerie;
        this.etalonnage = etalonnage;
    }

    /**
     *
     * @return
     */
    public String getCode() {
        return code;
    }

    /**
     *
     * @return
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @return
     */
    public String getEtalonnage() {
        return etalonnage;
    }

    /**
     *
     * @return
     */
    public String getFabriquant() {
        return fabriquant;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @return
     */
    public String getModele() {
        return modele;
    }

    /**
     *
     * @return
     */
    public String getNom() {
        return nom;
    }

    /**
     *
     * @return
     */
    public String getNumeroSerie() {
        return numeroSerie;
    }

    /**
     *
     * @return
     */
    public TypeOutilsMesure getTypeOutilsMesure() {
        return typeOutilsMesure;
    }

    /**
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     *
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @param etalonnage
     */
    public void setEtalonnage(String etalonnage) {
        this.etalonnage = etalonnage;
    }

    /**
     *
     * @param fabriquant
     */
    public void setFabriquant(String fabriquant) {
        this.fabriquant = fabriquant;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @param modele
     */
    public void setModele(String modele) {
        this.modele = modele;
    }

    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
        this.code = Utils.createCodeFromString(nom);
    }

    /**
     *
     * @param numeroSerie
     */
    public void setNumeroSerie(String numeroSerie) {
        this.numeroSerie = numeroSerie;
    }

    /**
     *
     * @param typeOutilsMesure
     */
    public void setTypeOutilsMesure(TypeOutilsMesure typeOutilsMesure) {
        this.typeOutilsMesure = typeOutilsMesure;
    }
}
