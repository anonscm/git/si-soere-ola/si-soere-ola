/**
 * OREILacs project - see LICENCE.txt for use created: 31 mars 2009 11:23:02
 */
package org.inra.ecoinfo.glacpe.refdata.plateforme;

import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.inra.ecoinfo.glacpe.refdata.Localisable;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.typeplateforme.TypePlateforme;
import org.inra.ecoinfo.utils.Utils;


/**
 * @author "Antoine Schellenberger"
 * 
 */
@Entity
@Table(name = Plateforme.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {SiteGLACPE.ID_JPA, Plateforme.ATTR_JPA_NOM}))
@AttributeOverrides({@AttributeOverride(name = Localisable.LOCALISABLE_NAME_ID, column = @Column(name = Plateforme.ID_JPA))})
public class Plateforme extends Localisable {
    
    
    //afiocca
    public static final String CODE_SANDRE = "code_sandre";
    public static final String CONTEXTE = "contexte";

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = "loc_id";

    /**
     *
     */
    public static final String TABLE_NAME = "plateforme_pla";

    /**
     *
     */
    public static final String ATTR_JPA_NOM = "nom";

    @Column(nullable = false)
    private String nom;
     
    @Column(nullable = true,name = CODE_SANDRE, unique = false)
    private String codeSandre;
    
    @Column(nullable = true,name = CONTEXTE , unique = false)
    private String contexte;


    public String getCodeSandre() {
        return codeSandre;
    }

    public void setCodeSandre(String codeSandre) {
        this.codeSandre = codeSandre;
    }

    public String getContexte() {
        return contexte;
    }

    public void setContexte(String contexte) {
        this.contexte = contexte;
    }

    /**
     *
     * @param nom
     * @param latitude
     * @param longitude
     * @param altitude
     */
    public Plateforme(String nom, Float latitude, Float longitude, Float altitude) {
        super(latitude, longitude, altitude);
        setNom(nom);
    }

    /**
     *
     */
    public Plateforme() {
        super();
    }

    @Column(nullable = false)
    private String code;

    /**
     *
     * @return
     */
    public String getCode() {
        return code;
    }

    /**
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    @ManyToOne(cascade = {MERGE, PERSIST, REFRESH})
    @JoinColumn(name = TypePlateforme.ID_JPA, referencedColumnName = TypePlateforme.ID_JPA, nullable = false)
    private TypePlateforme typePlateforme;

    @ManyToOne(cascade = {MERGE, PERSIST, REFRESH})
    @JoinColumn(name = SiteGLACPE.ID_JPA, referencedColumnName = SiteGLACPE.ID_JPA, nullable = false)
    private SiteGLACPE site;

    /**
     *
     * @return
     */
    public String getNom() {
        return nom;
    }

    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
        this.code = Utils.createCodeFromString(nom);
    }

    /**
     *
     * @return
     */
    public SiteGLACPE getSite() {
        return site;
    }

    /**
     *
     * @param site
     */
    public void setSite(SiteGLACPE site) {
        this.site = site;
    }

    /**
     *
     * @return
     */
    public TypePlateforme getTypePlateforme() {
        return typePlateforme;
    }

    /**
     *
     * @param typePlateforme
     */
    public void setTypePlateforme(TypePlateforme typePlateforme) {
        this.typePlateforme = typePlateforme;
    }
}
