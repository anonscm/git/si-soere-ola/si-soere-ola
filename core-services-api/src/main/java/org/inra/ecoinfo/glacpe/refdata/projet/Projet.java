package org.inra.ecoinfo.glacpe.refdata.projet;

import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projetsite.ProjetSite;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.utils.Utils;


/**
 * Représente un theme scientifique
 * 
 * @author "Antoine Schellenberger"
 * 
 */
@Entity
@Table(name = Projet.NAME_TABLE)
public class Projet implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = "pro_id";

    /**
     *
     */
    public static final String NAME_TABLE = "projet_pro";

    /**
     *
     */
    public static final String FIELD_ENTITY_NAME = "nom";

    /**
     *
     */
    static public final String FIELD_ENTITY_DESCRIPTION = "description_projet";

    /**
     *
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = ID_JPA)
    protected Long id;

    /**
     *
     */
    public Projet() {}

    /**
     *
     * @param nom
     */
    public Projet(String nom) {
        setNom(nom);
    }

    @OneToMany(mappedBy = "projet", cascade = {MERGE, PERSIST, REFRESH})
    @Fetch(FetchMode.JOIN)
    private List<ProjetSite> projetsSites = new LinkedList<ProjetSite>();

    @Column(name = FIELD_ENTITY_DESCRIPTION, columnDefinition = "TEXT")
    private String descriptionProjet;

    @Column(name = FIELD_ENTITY_NAME, nullable = false, unique = true)
    private String nom;

    @Column(nullable = false, unique = true)
    private String code;

    /**
     *
     * @return
     */
    public List<ProjetSite> getProjetsSites() {
        return projetsSites;
    }

    /**
     *
     * @param projetsSites
     */
    public void setProjetsSites(List<ProjetSite> projetsSites) {
        this.projetsSites = projetsSites;
    }

    /**
     *
     * @return
     */
    public String getDescriptionProjet() {
        return descriptionProjet;
    }

    /**
     *
     * @param descriptionProjet
     */
    public void setDescriptionProjet(String descriptionProjet) {
        this.descriptionProjet = descriptionProjet;
    }

    /**
     *
     * @return
     */
    public String getNom() {
        return nom;
    }

    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
        this.code = Utils.createCodeFromString(nom);
    }

    /**
     *
     * @return
     */
    public String getCode() {
        return code;
    }

    /**
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public List<Plateforme> retrievePlateformes() {
        List<Plateforme> plateformes = new LinkedList<Plateforme>();
        for (ProjetSite projetSite : projetsSites) {
            for (Plateforme plateforme : projetSite.getSite().getPlateformes()) {
                if (!plateformes.contains(plateforme)) {
                    plateformes.add((plateforme));
                }
            }
        }
        return plateformes;
    }

    /**
     *
     * @return
     */
    public List<Site> getSites() {
        List<Site> sites = new LinkedList<Site>();
        for (ProjetSite projetSite : projetsSites) {
            sites.add(projetSite.getSite());
        }
        return sites;
    }

}
