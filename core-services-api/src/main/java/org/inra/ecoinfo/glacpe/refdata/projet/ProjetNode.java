package org.inra.ecoinfo.glacpe.refdata.projet;

import org.inra.ecoinfo.refdata.node.INode;
import org.inra.ecoinfo.refdata.node.ITreeNode;

/**
 *
 * @author ptcherniati
 */
public class ProjetNode implements ITreeNode {

    private final Projet projet;

    /**
     *
     * @param projet
     */
    public ProjetNode(final Projet projet) {
        this.projet = projet;
    }

    @Override
    public String getName() {
        return projet.getNom();
    }

    @Override
    public String getDescription() {
        return projet.getDescriptionProjet();
    }

    @Override
    public String nameEntityJPA() {
        return Projet.NAME_TABLE;
    }

    @Override
    public String entityFieldName() {
        return Projet.FIELD_ENTITY_NAME;
    }

    @Override
    public String entityFieldDescription() {
        return Projet.FIELD_ENTITY_DESCRIPTION;
    }

    @Override
    public String getPath() {
        return projet.getCode();
    }

    @Override
    public String getNodeType() {
        return "projet";
    }

    @Override
    public int compareTo(final INode o) {
        int compare = -1;
        if (!(o instanceof ProjetNode)) {
            compare = projet.getCode().compareTo(((ProjetNode) o).projet.getCode());
        }
        return compare;
    }

    @Override
    public ITreeNode getParentNode() {
        return null;
    }

    /**
     *
     * @return
     */
    public Projet getProjet() {
        return projet;
    }

}
