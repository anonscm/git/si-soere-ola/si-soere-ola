package org.inra.ecoinfo.glacpe.refdata.projetsite;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.projet.ProjetNode;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.refdata.AbstractRecurentObject;
import org.inra.ecoinfo.refdata.node.INode;
import org.inra.ecoinfo.refdata.node.ITreeNode;
import org.inra.ecoinfo.utils.Utils;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = ProjetSite.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {SiteGLACPE.ID_JPA, Projet.ID_JPA}))
public class ProjetSite implements Serializable, ITreeNode {

    /**
     *
     */
    public static final String ID_JPA = "psi_id";

    /**
     *
     */
    public static final String TABLE_NAME = "projet_site_psi";

    /**
     *
     */
    public ProjetSite() {
        super();
    }

    /**
     *
     * @param site
     * @param projet
     */
    public ProjetSite(SiteGLACPE site, Projet projet) {
        this.site = site;
        this.projet = projet;
    }

    @Column(name = "commanditaire_projet")
    private String commanditaire;

    @Column(name = "commentaire_projet")
    private String commentaire;

    @Column(name = "date_debut")
    private Date debut;

    @Column(name = "date_fin")
    private Date fin;

    /**
     *
     */
    @Id
    @Column(name = ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    private static final long serialVersionUID = 1L;

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH}, optional = false)
    @JoinColumn(name = SiteGLACPE.ID_JPA, referencedColumnName = SiteGLACPE.ID_JPA, nullable = false)
    private SiteGLACPE site;

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH}, optional = false)
    @JoinColumn(name = Projet.ID_JPA, referencedColumnName = Projet.ID_JPA, nullable = false)
    private Projet projet;

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public SiteGLACPE getSite() {
        return site;
    }

    /**
     *
     * @param site
     */
    public void setSite(SiteGLACPE site) {
        this.site = site;
    }

    /**
     *
     * @return
     */
    public Projet getProjet() {
        return projet;
    }

    /**
     *
     * @param projet
     */
    public void setProjet(Projet projet) {
        this.projet = projet;
    }

    /**
     *
     * @return
     */
    public String getCommanditaire() {
        return commanditaire;
    }

    /**
     *
     * @param commanditaire
     */
    public void setCommanditaire(String commanditaire) {
        this.commanditaire = commanditaire;
    }

    /**
     *
     * @return
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     *
     * @param commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    /**
     *
     * @return
     */
    public Date getDebut() {
        return debut;
    }

    /**
     *
     * @param debut
     */
    public void setDebut(Date debut) {
        this.debut = debut;
    }

    /**
     *
     * @return
     */
    public Date getFin() {
        return fin;
    }

    /**
     *
     * @param fin
     */
    public void setFin(Date fin) {
        this.fin = fin;
    }

    @Override
    public String getName() {
        return site.getNom();
    }

    @Override
    public String getDescription() {
        return site.getDescription();
    }

    @Override
    public String nameEntityJPA() {
        return SiteGLACPE.NAME_ENTITY_JPA;
    }

    @Override
    public String entityFieldName() {
        return SiteGLACPE.ENTITY_FIELD_NAME;
    }

    @Override
    public String entityFieldDescription() {
        return SiteGLACPE.ENTITY_FIELD_DESCRIPTION;
    }

    @Override
    public String getPath() {
        String path;
        if (getParentNode() != null) {
            final StringBuffer pathBuffer = new StringBuffer(getParentNode().getPath()).append(AbstractRecurentObject.CST_PROPERTY_RECURENT_SEPARATOR).append(Utils.createCodeFromString(getName()));
            path = pathBuffer.toString();
        } else {
            path = getName();
        }
        return path;
    }

    @Override
    public String getNodeType() {
        return "localisation";
    }

    @Override
    public int compareTo(final INode o) {
        int i;
        if (o instanceof ProjetSite) {
            i = -1;
        } else {
            i = site.compareTo(((ProjetSite) o).getSite());
        }
        return i;
    }

    @Override
    public ITreeNode getParentNode() {
        ITreeNode treeNode = null;
        if (site.getParent() != null) {
            treeNode = new ProjetSiteTop((SiteGLACPE) site.getParent(), projet);
        } else {
            treeNode = new ProjetNode(projet);
        }
        return treeNode;
    }
}
