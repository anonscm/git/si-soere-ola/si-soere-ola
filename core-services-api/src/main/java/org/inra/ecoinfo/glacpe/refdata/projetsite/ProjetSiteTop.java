package org.inra.ecoinfo.glacpe.refdata.projetsite;

import java.io.Serializable;
import java.util.Date;

import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.projet.ProjetNode;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.refdata.AbstractRecurentObject;
import org.inra.ecoinfo.refdata.node.INode;
import org.inra.ecoinfo.refdata.node.ITreeNode;
import org.inra.ecoinfo.utils.Utils;

/**
 *
 * @author ptcherniati
 */
public class ProjetSiteTop implements Serializable, ITreeNode {

    /**
     *
     */
    public static final String ID_JPA = "psit_id";

    /**
     *
     */
    public static final String TABLE_NAME = "projet_site";
    private static final long serialVersionUID = 1L;

    private String commanditaire;

    private String commentaire;

    private Date debut;

    private Date fin;

    /**
     *
     */
    protected Long id;

    private SiteGLACPE site;

    private Projet projet;

    /**
     *
     */
    public ProjetSiteTop() {
        super();
    }

    /**
     *
     * @param site
     * @param projet
     */
    public ProjetSiteTop(final SiteGLACPE site, final Projet projet) {
        this.site = site;
        this.projet = projet;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public SiteGLACPE getSite() {
        return site;
    }

    /**
     *
     * @param site
     */
    public void setSite(final SiteGLACPE site) {
        this.site = site;
    }

    /**
     *
     * @return
     */
    public Projet getProjet() {
        return projet;
    }

    /**
     *
     * @param projet
     */
    public void setProjet(final Projet projet) {
        this.projet = projet;
    }

    /**
     *
     * @return
     */
    public String getCommanditaire() {
        return commanditaire;
    }

    /**
     *
     * @param commanditaire
     */
    public void setCommanditaire(final String commanditaire) {
        this.commanditaire = commanditaire;
    }

    /**
     *
     * @return
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     *
     * @param commentaire
     */
    public void setCommentaire(final String commentaire) {
        this.commentaire = commentaire;
    }

    /**
     *
     * @return
     */
    public Date getDebut() {
        return debut;
    }

    /**
     *
     * @param debut
     */
    public void setDebut(final Date debut) {
        this.debut = debut;
    }

    /**
     *
     * @return
     */
    public Date getFin() {
        return fin;
    }

    /**
     *
     * @param fin
     */
    public void setFin(final Date fin) {
        this.fin = fin;
    }

    @Override
    public String getName() {
        return site.getNom();
    }

    @Override
    public String getDescription() {
        return site.getDescription();
    }

    @Override
    public String nameEntityJPA() {
        return SiteGLACPE.NAME_ENTITY_JPA;
    }

    @Override
    public String entityFieldName() {
        return SiteGLACPE.ENTITY_FIELD_NAME;
    }

    @Override
    public String entityFieldDescription() {
        return SiteGLACPE.ENTITY_FIELD_DESCRIPTION;
    }

    @Override
    public String getPath() {
        return getParentNode() != null ? new StringBuffer(getParentNode().getPath()).append(AbstractRecurentObject.CST_PROPERTY_RECURENT_SEPARATOR).append(Utils.createCodeFromString(getName())).toString() : getName();
    }

    @Override
    public String getNodeType() {
        return "localisation";
    }

    @Override
    public int compareTo(final INode o) {
        int compare = -1;
        if (!(o instanceof ProjetSiteTop)) {
            compare = site.compareTo(((ProjetSiteTop) o).getSite());
        }
        return compare;
    }

    @Override
    public ITreeNode getParentNode() {
        return new ProjetNode(projet);
    }

}
