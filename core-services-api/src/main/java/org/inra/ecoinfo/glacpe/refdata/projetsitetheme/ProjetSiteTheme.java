package org.inra.ecoinfo.glacpe.refdata.projetsitetheme;

import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.inra.ecoinfo.glacpe.refdata.projetsite.ProjetSite;
import org.inra.ecoinfo.glacpe.refdata.projetsitethemedatatype.ProjetSiteThemeDatatype;
import org.inra.ecoinfo.refdata.AbstractRecurentObject;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.node.INode;
import org.inra.ecoinfo.refdata.node.ITreeNode;
import org.inra.ecoinfo.refdata.theme.Theme;
import org.inra.ecoinfo.utils.Utils;

/**
 *
 * @author ptcherniati
 */
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Table(name = ProjetSiteTheme.NAME_ENTITY_JPA, uniqueConstraints = @UniqueConstraint(columnNames = {ProjetSiteTheme.PARAM_NAME_JPA_PROJET_SITE, ProjetSiteTheme.PARAM_NAME_JPA_THEME}))
public class ProjetSiteTheme implements Serializable, ITreeNode {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = "psth_id";

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "projet_site_theme_psth";

    /**
     *
     */
    public static final String PARAM_NAME_JPA_PROJET_SITE = "psit_id";

    /**
     *
     */
    public static final String PARAM_NAME_JPA_THEME = "the_id";

    /**
     *
     */
    public static final String THEME = "theme";

    /**
     *
     */
    @Id
    @Column(name = ID_JPA)
    @GeneratedValue(strategy = GenerationType.TABLE)
    protected Long id;

    @ManyToOne(cascade = {MERGE, PERSIST, REFRESH}, optional = false)
    @JoinColumn(name = PARAM_NAME_JPA_PROJET_SITE, referencedColumnName = ProjetSite.ID_JPA, nullable = false)
    private ProjetSite projetSite;

    @OneToMany(mappedBy = "projetSiteTheme", cascade = {MERGE, PERSIST, REFRESH})
    private List<ProjetSiteThemeDatatype> projetSiteThemeDatatypes = new LinkedList<ProjetSiteThemeDatatype>();

    @ManyToOne(cascade = {MERGE, PERSIST, REFRESH}, optional = false)
    @JoinColumn(name = PARAM_NAME_JPA_THEME, referencedColumnName = Theme.PERSISTENT_NAME_ID, nullable = false)
    private Theme theme;

    /**
     *
     */
    public ProjetSiteTheme() {
        super();
    }

    /**
     *
     * @param projetSite
     * @param theme
     */
    public ProjetSiteTheme(final ProjetSite projetSite, final Theme theme) {
        super();
        this.projetSite = projetSite;
        this.theme = theme;
    }

    /**
     *
     * @return
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    public List<DataType> getDatatypes() {
        List<DataType> datatypes = new LinkedList<DataType>();
        for (ProjetSiteThemeDatatype projetSiteThemeDatatype : projetSiteThemeDatatypes) {
            datatypes.add((DataType) projetSiteThemeDatatype.getDatatype());
        }

        return (List) datatypes;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @return
     */
    public ProjetSite getProjetSite() {
        return projetSite;
    }

    /**
     *
     * @return
     */
    public List<ProjetSiteThemeDatatype> getProjetSitesThemesDatatypes() {
        return projetSiteThemeDatatypes;
    }

    /**
     *
     * @return
     */
    public Theme getTheme() {
        return theme;
    }

    /**
     *
     * @param id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     *
     * @param projetSite
     */
    public void setProjetSite(final ProjetSite projetSite) {
        this.projetSite = projetSite;
    }

    /**
     *
     * @param projetSiteThemeDatatypes
     */
    public void setProjetSitesThemesDatatypes(final List<ProjetSiteThemeDatatype> projetSiteThemeDatatypes) {
        this.projetSiteThemeDatatypes = projetSiteThemeDatatypes;
    }

    /**
     *
     * @param theme
     */
    public void setTheme(final Theme theme) {
        this.theme = theme;
    }

    @Override
    public String getName() {
        return theme.getName();
    }

    @Override
    public String getDescription() {
        return theme.getDescription();
    }

    @Override
    public String nameEntityJPA() {
        return Theme.NAME_ENTITY_JPA;
    }

    @Override
    public String entityFieldName() {
        return Theme.NAME_ATTRIBUTS_NAME;
    }

    @Override
    public String entityFieldDescription() {
        return Theme.NAME_ATTRIBUTS_DESCRIPTION;
    }

    @Override
    public ITreeNode getParentNode() {
        return projetSite;
    }

    @Override
    public String getPath() {
        return getParentNode() != null ? new StringBuffer(getParentNode().getPath()).append(AbstractRecurentObject.CST_PROPERTY_RECURENT_SEPARATOR).append(Utils.createCodeFromString(getName())).toString() : getName();
    }

    @Override
    public int compareTo(final INode o) {
        int compare = -1;
        if (o instanceof ProjetSiteTheme) {
            compare = theme.compareTo(((ProjetSiteTheme) o).getTheme());
        }
        return compare;
    }

    @Override
    public String getNodeType() {
        return THEME;
    }

    /**
     *
     * @return
     */
    public List<ProjetSiteThemeDatatype> getProjetSiteThemeDatatypes() {
        return projetSiteThemeDatatypes;
    }

    /**
     *
     * @param projetSiteThemeDatatypes
     */
    public void setProjetSiteThemeDatatypes(final List<ProjetSiteThemeDatatype> projetSiteThemeDatatypes) {
        this.projetSiteThemeDatatypes = projetSiteThemeDatatypes;
    }

}
