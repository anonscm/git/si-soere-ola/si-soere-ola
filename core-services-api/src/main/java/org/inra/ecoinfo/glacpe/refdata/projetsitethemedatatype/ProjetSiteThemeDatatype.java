package org.inra.ecoinfo.glacpe.refdata.projetsitethemedatatype;

import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.inra.ecoinfo.glacpe.refdata.projetsitetheme.ProjetSiteTheme;
import org.inra.ecoinfo.refdata.AbstractRecurentObject;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.node.ILeafTreeNode;
import org.inra.ecoinfo.refdata.node.INode;
import org.inra.ecoinfo.refdata.node.ITreeNode;
import org.inra.ecoinfo.refdata.node.entity.LeafNode;
import org.inra.ecoinfo.utils.Utils;

/**
 *
 * @author ptcherniati
 */
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Table(name = ProjetSiteThemeDatatype.NAME_ENTITY_JPA, uniqueConstraints = @UniqueConstraint(columnNames = {ProjetSiteThemeDatatype.PARAM_NAME_JPA_DATATYPE, ProjetSiteThemeDatatype.PARAM_NAME_JPA_PROJET_SITE_THEME}))
public class ProjetSiteThemeDatatype extends LeafNode implements ILeafTreeNode {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = "pstdt_id";

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "projet_site_theme_datatype_pstdt";

    /**
     *
     */
    public static final String PARAM_NAME_JPA_DATATYPE = "dty_id";

    /**
     *
     */
    public static final String PARAM_NAME_JPA_PROJET_SITE_THEME = "psth_id";

    /**
     *
     */
    public static final String DATATYPE = "datatype";

    @ManyToOne(cascade = {MERGE, PERSIST, REFRESH}, optional = false)
    @JoinColumn(name = ProjetSiteTheme.ID_JPA, referencedColumnName = ProjetSiteTheme.ID_JPA, nullable = false)
    private ProjetSiteTheme projetSiteTheme;

    /**
     *
     */
    @ManyToOne(cascade = {MERGE, PERSIST, REFRESH}, optional = false)
    @JoinColumn(name = PARAM_NAME_JPA_DATATYPE, referencedColumnName = DataType.DATATYPE_NAME_ID, nullable = false)
    protected DataType datatype;

    /**
     *
     */
    public ProjetSiteThemeDatatype() {
        super();
    }

    /**
     *
     * @param datatype
     * @param projetSiteTheme
     */
    public ProjetSiteThemeDatatype(final DataType datatype, final ProjetSiteTheme projetSiteTheme) {
        super();
        this.datatype = datatype;
        this.projetSiteTheme = projetSiteTheme;
        projetSiteTheme.getProjetSitesThemesDatatypes().add(this);
    }

    /**
     *
     * @param id
     * @param datatype
     * @param projetSiteTheme
     */
    public ProjetSiteThemeDatatype(final Long id, final DataType datatype, final ProjetSiteTheme projetSiteTheme) {
        super();
        this.id = id;
        this.datatype = datatype;
        this.projetSiteTheme = projetSiteTheme;
    }

    /**
     *
     * @return
     */
    public ProjetSiteTheme getProjetSiteTheme() {
        return projetSiteTheme;
    }

    /**
     *
     * @param projetSiteTheme
     */
    public void setProjetSiteTheme(final ProjetSiteTheme projetSiteTheme) {
        this.projetSiteTheme = projetSiteTheme;
    }

    @Override
    public ITreeNode getParentNode() {
        return projetSiteTheme;
    }

    @Override
    public String getName() {
        return datatype.getName();
    }

    @Override
    public String getDescription() {
        return datatype.getDescription();
    }

    @Override
    public String nameEntityJPA() {
        return DataType.NAME_ENTITY_JPA;
    }

    @Override
    public String entityFieldName() {
        return DataType.NAME_ATTRIBUTS_NAME;
    }

    @Override
    public String entityFieldDescription() {
        return DataType.NAME_ATTRIBUTS_DESCRIPTION;
    }

    @Override
    public String getPath() {
        return getParentNode() != null ? new StringBuffer(getParentNode().getPath()).append(AbstractRecurentObject.CST_PROPERTY_RECURENT_SEPARATOR).append(Utils.createCodeFromString(getName())).toString() : getName();
    }

    @Override
    public int compareTo(final INode o) {
        int compare = -1;
        if (o instanceof ProjetSiteThemeDatatype) {
            compare = datatype.compareTo(((ProjetSiteThemeDatatype) o).getDatatype());
        }
        return compare;
    }

    @Override
    public String getNodeType() {
        return DATATYPE;
    }

    /**
     *
     * @param datatype
     */
    public void setDatatype(final DataType datatype) {
        this.datatype = datatype;
    }

    public DataType getDatatype() {
        return datatype;
    }

    @Override
    public LeafNode getLeafNode() {
        return this;
    }

    @Override
    public String getDepositPlacePrefixForFileName() {
        return new StringBuffer(getProjetSiteTheme().getProjetSite().getProjet().getCode()).append(AbstractRecurentObject.CST_PROPERTY_RECURENT_SEPARATOR_IN_FILE_NAME).append(getProjetSiteTheme().getProjetSite().getSite().getPath())
                .append(AbstractRecurentObject.CST_PROPERTY_RECURENT_SEPARATOR_IN_FILE_NAME).append(Utils.createCodeFromString(getName())).toString();
    }
}
