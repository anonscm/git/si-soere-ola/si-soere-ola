package org.inra.ecoinfo.glacpe.refdata.site;

import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projetsite.ProjetSite;
import org.inra.ecoinfo.glacpe.refdata.typesite.TypeSite;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.utils.Utils;


/**
 *
 * @author ptcherniati
 */
@Entity
@DiscriminatorValue("site_glacpe")
@Table(name = SiteGLACPE.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {SiteGLACPE.ATTRIBUTE_CODE_JPA, SiteGLACPE.ID_JPA}))
public class SiteGLACPE extends Site {
    
    //afiocca
    public static final String CODE_SANDRE_ME = "code_sandre_me"; //code sandre Masse d'eau plan d'eau
    public static final String CODE_SANDRE_PE = "code_sandre_pe"; // code sandre Plans d'eau
    
    
    static public final String ID_JPA = "id";// modification noyau à réaliser

    
    public static final String TABLE_NAME = "site_glacpe_sit";


    public static final String ATTRIBUTE_CODE_JPA = "codeFromName";

    private static final long serialVersionUID = 1L;

    @OneToMany(mappedBy = "site", cascade = {MERGE, PERSIST, REFRESH})
    private List<Plateforme> plateformes = new LinkedList<Plateforme>();

    @OneToMany(mappedBy = "site", cascade = {MERGE, PERSIST, REFRESH})
    private List<ProjetSite> projetsSites = new LinkedList<ProjetSite>();

    @Column(nullable = false, name = ATTRIBUTE_CODE_JPA, unique = true)
    private String codeFromName;

    @ManyToOne(cascade = {MERGE, PERSIST, REFRESH})
    @JoinColumn(name = TypeSite.ID_JPA, referencedColumnName = TypeSite.ID_JPA, nullable = false)
    private TypeSite typeSite;
    
    
    @Column(nullable = true,name = CODE_SANDRE_ME, unique = false)
    private String codeSandreMe;
    @Column(nullable = true,name = CODE_SANDRE_PE, unique = false)
    private String codeSandrePe;


    public String getCodeSandrePe() {
        return codeSandrePe;
    }

    public void setCodeSandrePe(String codeSandrePe) {
        this.codeSandrePe = codeSandrePe;
    }
    
        public String getCodeSandreMe() {
        return codeSandreMe;
    }

    public void setCodeSandreMe(String codeSandreMe) {
        this.codeSandreMe = codeSandreMe;
    }
    



    /**
     *
     */
    public SiteGLACPE() {
        super();
    }

    /**
     *
     * @param nom
     * @param description
     */
    public SiteGLACPE(String nom, String description) {
        super(nom, description);
    }

    
    
    /**
     *
     * @return
     */
    public List<Plateforme> getPlateformes() {
        return plateformes;
    }

    /**
     *
     * @param plateformes
     */
    public void setPlateformes(List<Plateforme> plateformes) {
        this.plateformes = plateformes;
    }

    /**
     *
     * @return
     */
    public List<ProjetSite> getProjetsSites() {
        return projetsSites;
    }

    /**
     *
     * @param projetsSites
     */
    public void setProjetsSites(List<ProjetSite> projetsSites) {
        this.projetsSites = projetsSites;
    }

    /**
     *
     * @param codeFromName
     */
    public void setCodeFromName(String codeFromName) {
        this.codeFromName = codeFromName;
    }

    @Override
    public void setNom(String nom) {
        super.setNom(nom);
        this.setCodeFromName(Utils.createCodeFromString(nom));
    }

    /**
     *
     * @return
     */
    public String getCodeFromName() {
        return codeFromName;
    }

    /**
     *
     * @return
     */
    public TypeSite getTypeSite() {
        return typeSite;
    }

    /**
     *
     * @param typeSite
     */
    public void setTypeSite(TypeSite typeSite) {
        this.typeSite = typeSite;
    }
}
