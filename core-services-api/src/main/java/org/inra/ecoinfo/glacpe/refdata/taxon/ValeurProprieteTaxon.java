package org.inra.ecoinfo.glacpe.refdata.taxon;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

import org.inra.ecoinfo.refdata.valeurqualitative.ValeurQualitative;

/**
 *
 * @author ptcherniati
 */
@Entity(name = ValeurProprieteTaxon.TABLE_NAME)
public class ValeurProprieteTaxon implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public final static String ID_JPA = "vprota_id";

    /**
     *
     */
    public static final String TABLE_NAME = "valeur_propriete_taxon_vprota";// ValeurProprieteTaxon_vprota

    /**
     *
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = ID_JPA)
    protected Long id;

    
    
    @Column(name = "vprota_stringvalue", nullable = true,columnDefinition = "TEXT")
    private String stringValue;

    @Column(name = "vprota_floatvalue", nullable = true)
    private Float floatValue;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = ValeurQualitative.ID_JPA, referencedColumnName = ValeurQualitative.ID_JPA, nullable = true)
    private ValeurQualitative qualitativeValue;

    /**
     *
     */
    public ValeurProprieteTaxon() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     *
     * @return
     */
    public String getStringValue() {
        return stringValue;
    }

    /**
     *
     * @param stringValue
     */
    public void setStringValue(String stringValue) {
        this.stringValue = stringValue;
    }

    /**
     *
     * @return
     */
    public Float getFloatValue() {
        return floatValue;
    }

    /**
     *
     * @param floatValue
     */
    public void setFloatValue(Float floatValue) {
        this.floatValue = floatValue;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public ValeurQualitative getQualitativeValue() {
        return qualitativeValue;
    }

    /**
     *
     * @param qualitativeValue
     */
    public void setQualitativeValue(ValeurQualitative qualitativeValue) {
        this.qualitativeValue = qualitativeValue;
    }

}
