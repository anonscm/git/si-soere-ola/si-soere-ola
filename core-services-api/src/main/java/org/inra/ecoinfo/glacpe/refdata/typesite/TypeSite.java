package org.inra.ecoinfo.glacpe.refdata.typesite;

import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;

import org.inra.ecoinfo.utils.Utils;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = TypeSite.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {TypeSite.CODE}))
public class TypeSite implements Serializable {
    
    //afiocca
    public static final String CODE_SANDRE = "code_sandre";
    public static final String CONTEXTE = "contexte";
    

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    static public final String TABLE_NAME = "type_site_tsit";

    /**
     *
     */
    static public final String ID_JPA = "tsit_id";

    /**
     *
     */
    public static final String ENTITY_FIELD_NAME = "nom";

    /**
     *
     */
    public static final String CODE = "code";

    /**
     *
     */
    static public final String ENTITY_FIELD_DESCRIPTION = "description";

    /**
     *
     */
    public TypeSite() {
        super();
    }

    /**
     *
     * @param nom
     * @param description
     */
    public TypeSite(String nom, String description) {
        super();
        setNom(nom);
        this.description = description;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = ID_JPA)
    private Long id;

    @Column(name = CODE, nullable = false, unique = true)
    private String code;

    @Column(name = ENTITY_FIELD_NAME, nullable = false, unique = false)
    private String nom = "";

    @Column(name = ENTITY_FIELD_DESCRIPTION, nullable = true, columnDefinition = "TEXT")
    private String description;

    @OneToMany(mappedBy = "typeSite", cascade = {MERGE, PERSIST, REFRESH})
    private List<SiteGLACPE> sites = new LinkedList<SiteGLACPE>();
    
    @Column(nullable = true,name = CODE_SANDRE, unique = false)
    private String codeSandre;
    @Column(nullable = true,name = CONTEXTE , unique = false)
    private String contexte;


    public String getCodeSandre() {
        return codeSandre;
    }

    public void setCodeSandre(String codeSandre) {
        this.codeSandre = codeSandre;
    }

    public String getContexte() {
        return contexte;
    }

    public void setContexte(String contexte) {
        this.contexte = contexte;
    }


    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getNom() {
        return nom;
    }

    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
        this.code = Utils.createCodeFromString(nom);
    }

    /**
     *
     * @return
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return
     */
    public String getCode() {
        return code;
    }

    /**
     *
     * @return
     */
    public List<SiteGLACPE> getSites() {
        return sites;
    }

}
