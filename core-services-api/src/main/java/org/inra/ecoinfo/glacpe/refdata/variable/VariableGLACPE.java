package org.inra.ecoinfo.glacpe.refdata.variable;

import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;

import org.inra.ecoinfo.glacpe.refdata.groupevariable.GroupeVariable;
import org.inra.ecoinfo.glacpe.refdata.variablenorme.VariableNorme;
import org.inra.ecoinfo.refdata.variable.Variable;

/**
 *
 * @author ptcherniati
 */
@Entity
@DiscriminatorValue("variable_glacpe")
@SqlResultSetMapping(name = Variable.RESULTSET_MAPPING_JPA, entities = {@EntityResult(entityClass = VariableGLACPE.class)})
@Table(name = VariableGLACPE.NAME_ENTITY_JPA)
public class VariableGLACPE extends Variable {

    private static final long serialVersionUID = 1L;
    
    //afiocca
    public static final String CODE_SANDRE = "code_sandre";
    public static final String CONTEXTE = "contexte";


    static public final String NAME_ENTITY_JPA = "variable_glacpe_varg";

    @ManyToOne(cascade = {MERGE, PERSIST, REFRESH})
    @JoinColumn(name = GroupeVariable.ID_JPA, referencedColumnName = GroupeVariable.ID_JPA, nullable = true)
    private GroupeVariable groupe;

    @Column(name = "ordre_affichage_groupe")
    private Integer ordreAffichageGroupe;

    @ManyToOne(cascade = {MERGE, PERSIST, REFRESH}, optional = false)
    @JoinColumn(name = VariableNorme.ID_JPA, referencedColumnName = VariableNorme.ID_JPA, nullable = true)
    private VariableNorme variableNorme;
    

    @Column(nullable = true,name = CODE_SANDRE, unique = false)
    private String codeSandre;
    
    @Column(nullable = true,name = CONTEXTE , unique = false)
    private String contexte;


    public String getCodeSandre() {
        return codeSandre;
    }

    public void setCodeSandre(String codeSandre) {
        this.codeSandre = codeSandre;
    }
    
    public String getContexte() {
        return contexte;
    }

    public void setContexte(String contexte) {
        this.contexte = contexte;
    }



    /**
     *
     */
    public VariableGLACPE() {
        super();
    }

    /**
     *
     * @param id
     */
    public VariableGLACPE(Long id) {
        this.id = id;
    }

    /**
     *
     * @param nom
     * @param definition
     * @param affichage
     * @param ordreAffichageGroupe
     * @param isQualitative
     */
    public VariableGLACPE(String nom, String definition, String affichage, Integer ordreAffichageGroupe, Boolean isQualitative) {
        super();
        setNom(nom);
        setDefinition(definition);
        setAffichage(affichage);
        setOrdreAffichageGroupe(ordreAffichageGroupe);
        setIsQualitative(isQualitative);
    }

    public Variable cloneOnlyAttributes() {
        Variable variable = new Variable();
        variable.setNom(getNom());
        variable.setDefinition(getDefinition());
        variable.setId(getId());
        return variable;

    }
    


    /**
     *
     * @return
     */
    public GroupeVariable getGroupe() {
        return groupe;
    }

    /**
     *
     * @return
     */
    public Integer getOrdreAffichageGroupe() {
        return ordreAffichageGroupe;
    }

    /**
     *
     * @return
     */
    public VariableNorme getVariableNorme() {
        return variableNorme;
    }

    /**
     *
     * @param groupe
     */
    public void setGroupe(GroupeVariable groupe) {
        this.groupe = groupe;
    }

    /**
     *
     * @param ordreAffichageGroupe
     */
    public void setOrdreAffichageGroupe(Integer ordreAffichageGroupe) {
        this.ordreAffichageGroupe = ordreAffichageGroupe;
    }

    /**
     *
     * @param variableNorme
     */
    public void setVariableNorme(VariableNorme variableNorme) {
        this.variableNorme = variableNorme;
    }

}
