/**
 * OREILacs project - see LICENCE.txt for use created: 31 mars 2009 13:30:55
 */
package org.inra.ecoinfo.glacpe.synthesis.chimie;

import java.util.Date;

import javax.persistence.Entity;

import org.hibernate.annotations.Index;
import org.hibernate.annotations.Table;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue;


/**
 * @author "Antoine Schellenberger"
 * 
 */
@Entity(name = "ChimieSynthesisValue")
@Table(appliesTo = "ChimieSynthesisValue", indexes = {@Index(name = "ChimieSynthesisValue_site_variable_idx", columnNames = {"site", "variable"})})
public class SynthesisValue extends GenericSynthesisValue {

    private static final long serialVersionUID = 1L;

    /**
     *
     * @param date
     * @param site
     * @param variable
     * @param value
     */
    public SynthesisValue(Date date, String site, String variable, Float value) {
        super();
        this.date = date;
        this.site = site;
        this.variable = variable;
        this.valueFloat = value;
    }

    /**
     *
     */
    public SynthesisValue() {
        super();
    }

}
