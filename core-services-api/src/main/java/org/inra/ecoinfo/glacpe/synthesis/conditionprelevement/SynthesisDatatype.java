package org.inra.ecoinfo.glacpe.synthesis.conditionprelevement;

import java.util.Date;

import javax.persistence.Entity;

import org.inra.ecoinfo.synthesis.entity.GenericSynthesisDatatype;

/**
 *
 * @author ptcherniati
 */
@Entity(name = "ConditionprelevementSynthesisDatatype")
public class SynthesisDatatype extends GenericSynthesisDatatype {

    private static final long serialVersionUID = 1L;

    /**
     *
     * @param site
     * @param minDate
     * @param maxDate
     */
    public SynthesisDatatype(String site, Date minDate, Date maxDate) {
        super();
        this.site = site;
        this.minDate = minDate;
        this.maxDate = maxDate;
    }

    /**
     *
     */
    public SynthesisDatatype() {
        super();
    }
}
