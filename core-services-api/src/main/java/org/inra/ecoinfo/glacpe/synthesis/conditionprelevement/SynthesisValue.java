package org.inra.ecoinfo.glacpe.synthesis.conditionprelevement;

import java.util.Date;

import javax.persistence.Entity;

import org.hibernate.annotations.Index;
import org.hibernate.annotations.Table;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue;

/**
 *
 * @author ptcherniati
 */
@Entity(name = "ConditionprelevementSynthesisValue")
@Table(appliesTo = "ConditionprelevementSynthesisValue", indexes = {@Index(name = "ConditionprelevementSynthesisValue_site_variable_idx", columnNames = {"site", "variable"})})
public class SynthesisValue extends GenericSynthesisValue {

    private static final long serialVersionUID = 1L;

    /**
     *
     * @param date
     * @param site
     * @param variable
     * @param valueFloat
     * @param valueString
     */
    public SynthesisValue(Date date, String site, String variable, Float valueFloat, String valueString) {
        super();
        this.date = date;
        this.site = site;
        this.variable = variable;
        this.valueFloat = valueFloat;
        this.valueString = valueString;
    }

    /**
     *
     */
    public SynthesisValue() {
        super();
    }
}
