package org.inra.ecoinfo.glacpe;

import static org.inra.ecoinfo.glacpe.LacsTransactionalTestExecutionListener.ADMIN_TESTEUR;
import static org.inra.ecoinfo.glacpe.LacsTransactionalTestExecutionListener.getConfiguration;
import static org.inra.ecoinfo.glacpe.LacsTransactionalTestExecutionListener.getConfigurationTest;
import static org.inra.ecoinfo.glacpe.LacsTransactionalTestExecutionListener.getDatasetManager;
import static org.inra.ecoinfo.glacpe.LacsTransactionalTestExecutionListener.getDatatypeDAO;
import static org.inra.ecoinfo.glacpe.LacsTransactionalTestExecutionListener.getExtractionManager;
import static org.inra.ecoinfo.glacpe.LacsTransactionalTestExecutionListener.getProjetSiteThemeDatatypeDAO;
import static org.inra.ecoinfo.glacpe.LacsTransactionalTestExecutionListener.getSecurityContext;
import static org.inra.ecoinfo.glacpe.LacsTransactionalTestExecutionListener.getSecurityDatasetManager;
import static org.inra.ecoinfo.glacpe.LacsTransactionalTestExecutionListener.getUsersManager;
import static org.inra.ecoinfo.glacpe.LacsTransactionalTestExecutionListener.logger;
import static org.inra.ecoinfo.glacpe.LacsTransactionalTestExecutionListener.plateformeDAO;
import static org.inra.ecoinfo.glacpe.LacsTransactionalTestExecutionListener.projetSiteDAO;
import static org.inra.ecoinfo.glacpe.LacsTransactionalTestExecutionListener.siteDAO;
import static org.inra.ecoinfo.glacpe.LacsTransactionalTestExecutionListener.taxonDAO;
import static org.inra.ecoinfo.glacpe.LacsTransactionalTestExecutionListener.variableDAO;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.inra.ecoinfo.dataset.security.ISecurityDatasetManager;
import org.inra.ecoinfo.dataset.security.PermissionDataset;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.glacpe.extraction.vo.AbstractDatesFormParam;
import org.inra.ecoinfo.glacpe.extraction.vo.DatasRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesYearsContinuousFormParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesYearsDiscretsFormParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesYearsRangeFormParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO;
import org.inra.ecoinfo.glacpe.extraction.vo.ProjetSiteVO;
import org.inra.ecoinfo.glacpe.extraction.vo.VariableVO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projetsite.ProjetSite;
import org.inra.ecoinfo.glacpe.refdata.taxon.Taxon;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.notifications.entity.Notification;
import org.inra.ecoinfo.security.entity.Privilege;
import org.inra.ecoinfo.security.entity.PrivilegeProperty;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.LoggerObject;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.configuration.PlateformeTest;
import org.inra.ecoinfo.utils.configuration.PrivilegeTest;
import org.inra.ecoinfo.utils.configuration.TaxonTest;
import org.inra.ecoinfo.utils.configuration.Test;
import org.inra.ecoinfo.utils.configuration.UploadTest;
import org.inra.ecoinfo.utils.configuration.VariableTest;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.ErrorNotificationException;
import org.inra.ecoinfo.utils.exceptions.NotificationException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.runner.RunWith;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.Ostermiller.util.CSVParser;

/**
 * @author philippe
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/META-INF/spring/applicationContextTest.xml" })
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@Transactional(rollbackFor = Exception.class)
@TestExecutionListeners(listeners = { LacsTransactionalTestExecutionListener.class })
public abstract class AbstractTest {

	protected static final String CST_STRING_EMPTY = "";
	protected static final String ERREUR_DANS_LE_REQUESTREMINDER = "Erreur dans le requestreminder";
	protected static final String DEPHTS = "dephts";
	protected static final String DATES = "dates";
	protected static final String MIN = "minimum";
	protected static final String MAX = "maximum";
	protected static final String AVERAGE = "moyenne";
	// private static final String SUM = "somme";
	protected static final String COUNT = "count";
	public static final String INFO = "info";
	public static final String ERROR = "error";
	public static final String WARN = "warn";
	static final int BUFFER = 2048;
	protected static final char SEPARATOR = ';';
	private static final String ADD_TO_PATH = "%s/%s";
	public static int instanceNumber = 5;
	protected static Map<String, Map<Integer, Notification>> notifications = new TreeMap<String, Map<Integer, Notification>>();
	private static List<String> attachements = new LinkedList<String>();
	protected LinkedList<VariableVO> variablesVO;
	static {
		notifications.put(INFO, new TreeMap<Integer, Notification>());
		notifications.put(ERROR, new TreeMap<Integer, Notification>());
		notifications.put(WARN, new TreeMap<Integer, Notification>());
	}

	protected void test(String testName) throws BusinessException, IOException, PersistenceException, NotificationException {

		log(testName, getTest(testName).getExtract().getComment());
		// login
		startSession();
		startRequest();
		logWithUserName(testName,
				"Connexion de l'utilisateur %s et mise à jour de ses droits");
		connectUserAndSetRoles(getTest(testName));
		endRequest();
		// depôt
		startRequest();
		loadData(getTest(testName));
		endRequest();
		// publication
		startRequest();
		publishData(getTest(testName));
		endRequest();
		// extraction
		log(testName, "Extraction du fichier %s");
		startRequest();
		extractData(getTest(testName));
		endRequest();
		// extraction
		log(testName,
				"Verification de la conformité du fichier d'extraction du fichier %s");
		startRequest();
		checkExtractData(getTest(testName));
		endRequest();
		// dé-publication
		startRequest();
		unPublishData(getTest(testName));
		endRequest();
		getDatatypeDAO().flush();
		// suppression
		startRequest();
		unLoadData(getTest(testName));
		endRequest();
		// logOut
		logWithUserName(testName,
				"Déconnexion de l'utilisateur %s et mise à jour de ses droits");
		startRequest();
		deConnectUserAndSetRoles(getTest(testName));
		endRequest();
		endSession();
	}

	protected void connect(Utilisateur utilisateurByLogin) throws BusinessException {
		startRequest();
		getSecurityContext().initContext(utilisateurByLogin);
		startRequest();
	}

	protected void connectUserAndSetRoles(Test test) throws BusinessException, PersistenceException {
		connect(test.getUtilisateur());
		for (PrivilegeTest privilegeTest : test.getPrivilegesTest()) {
			if (privilegeTest == null || privilegeTest.getPrivilegePath() == null || AbstractTest.CST_STRING_EMPTY.equals(privilegeTest.getPrivilegePath())) {
				continue;
			}
			if(privilegeTest.isDepot()){
				Privilege privilegeDepot = getNewPrivilege(test, privilegeTest, org.inra.ecoinfo.security.entity.Role.ROLE_DEPOT);
				LacsTransactionalTestExecutionListener.getPrivilegeDAO().saveOrUpdate(privilegeDepot);
			}
			if(privilegeTest.getExtractionPrivilege()!=null && privilegeTest.getExtractionPrivilege().isExtraction()){
				Privilege privilegeExtraction = getNewPrivilege(test, privilegeTest, org.inra.ecoinfo.security.entity.Role.ROLE_EXTRACTION);
				List<PrivilegeProperty> privilegeProperties = new LinkedList<PrivilegeProperty>();
				if(privilegeTest.getExtractionPrivilege().getBeginDate() != null){
					PrivilegeProperty privilegePropertyDateDebut = new PrivilegeProperty(PrivilegeProperty.PROPERTY_START, DateUtil.getSimpleDateFormatDateLocale().format(privilegeTest.getExtractionPrivilege().getBeginDate()));
					privilegePropertyDateDebut.setPrivilege(privilegeExtraction);
					privilegeProperties.add(privilegePropertyDateDebut);
				}
				if(privilegeTest.getExtractionPrivilege().getEndDate() != null){
					PrivilegeProperty privilegePropertyDateFin = new PrivilegeProperty(PrivilegeProperty.PROPERTY_END, DateUtil.getSimpleDateFormatDateLocale().format(privilegeTest.getExtractionPrivilege().getEndDate()));
					privilegePropertyDateFin.setPrivilege(privilegeExtraction);
					privilegeProperties.add(privilegePropertyDateFin);
				}
				privilegeExtraction.setProperties(privilegeProperties);
				LacsTransactionalTestExecutionListener.getPrivilegeDAO().saveOrUpdate(privilegeExtraction);
			}
		}
		LacsTransactionalTestExecutionListener.getUtilisateurDAO().saveOrUpdate(test.getUtilisateur());
		getSecurityContext().releaseContext();
		getSecurityContext().initContext(getUsersManager().getUtilisateurByLogin(test.getUtilisateurLogin()));
		getSecurityContext().reloadPrivileges();
	}

	private Privilege getNewPrivilege(final Test test, PrivilegeTest privilegeTest, String role)
			throws PersistenceException, BusinessException {
		final String privilegepath = privilegeTest.getPrivilegePath();
		Privilege privilege = new Privilege();
		privilege.setResourcePath(privilegepath);
		privilege.setPrivilegeType(LacsTransactionalTestExecutionListener.getPrivilegeTypeDAO().getByNaturalKey(ISecurityDatasetManager.PRIVILEGE_TYPE_DATASET));
		Utilisateur utilisateur = LacsTransactionalTestExecutionListener.getUtilisateurDAO().merge(test.getUtilisateur());
		test.setUtilisateur(utilisateur);
		privilege.setUtilisateur(utilisateur);
		privilege.setRole(retrieveRoleByName(role));
		utilisateur.getPrivileges().add(privilege);
		return privilege;
	}
	
	protected org.inra.ecoinfo.security.entity.Role retrieveRoleByName(final String name) throws BusinessException, PersistenceException {
        final org.inra.ecoinfo.security.entity.Role role = LacsTransactionalTestExecutionListener.getRoleDAO().getByName(name);
        if (role == null) {

            throw new BusinessException("pas de rôle");
        }
        return role;
    }

	protected void deConnectUserAndSetRoles(Test test) throws BusinessException {
		List<PermissionDataset> treeItemsPermissions = getSecurityDatasetManager().getAllPermissionsByUserId(getSecurityContext().getUtilisateur().getId());
		for (PrivilegeTest privilegeTest : test.getPrivilegesTest()) {
			if (privilegeTest == null || privilegeTest.getPrivilegePath() == null || CST_STRING_EMPTY.equals(privilegeTest.getPrivilegePath()))
				continue;
			String privilege = privilegeTest.getPrivilegePath();
			String[] nodesString = privilege.split("/");
			List<PermissionDataset> nodes = treeItemsPermissions;
			if (nodes.size() == 0)
				continue;
			PermissionDataset node = getNode(nodes, nodesString[0]);
			unsetPropertyForNode(node, privilegeTest);
			if (nodes.size() == 1) {
				node.fillAllEmptyCheckPerm();
				continue;
			}
			if (node == null)
				continue;
			for (int i = 1; i < nodesString.length; i++) {
				String nodeString = nodesString[i];
				node = getNode(node.getChildren(), nodeString);
				unsetPropertyForNode(node, privilegeTest);
				if (i == nodesString.length - 1) {
					node.fillAllEmptyCheckPerm();
					break;
				}
				if (node == null)
					break;
			}
		}
		getSecurityDatasetManager().updateUserPermissions(test.getUtilisateurLogin(), treeItemsPermissions);
		getSecurityContext().releaseContext();
		getSecurityContext().initContext(getUsersManager().getUtilisateurByLogin(ADMIN_TESTEUR));
		getSecurityContext().reloadPrivileges();
	}

	private void unsetPropertyForNode(PermissionDataset node, PrivilegeTest privilegeTest) {
		node.setDepot(false);
		node.setPublication(false);
		node.setAdministration(false);
		node.setSynthese(false);
		node.setExtraction(false);
		node.setDebut(null);
		node.setFin(null);
	}

	private PermissionDataset getNode(List<PermissionDataset> nodes, String code) {
		for (PermissionDataset permissionDataset : nodes) {
			if (permissionDataset.getNom() == null)
				continue;
			if (Utils.createCodeFromString(permissionDataset.getNom()).endsWith(code))
				return permissionDataset;
		}
		return null;
	}

	protected Test getTest(String testCode) {
		return getConfigurationTest().getTests().get(testCode);
	}

	protected void loadData(Test test) throws BusinessException, IOException,
			PersistenceException, ErrorNotificationException {
		int warnSize;
		for (UploadTest uploadTest : test.getUploads()) {
			logWithFileName(test.getName(), "Dépôt du fichier %s", uploadTest);
			warnSize = notifications.get(ERROR) == null ? 0 : notifications.get(ERROR).size();
			VersionFile version = getNewVersionTest(uploadTest);
			if ((notifications.get(ERROR) == null ? 0 : notifications.get(ERROR).size()) > warnSize)
				throw new ErrorNotificationException(notifications.get(ERROR).get(new Integer(notifications.get(ERROR).size() - 1)));
			version.setData(getBytesFromFile(uploadTest.getInFile()));
			getDatasetManager().uploadVersion(version);
		}
	}

	protected void unLoadData(Test test) throws BusinessException, IOException,
			PersistenceException {
		for (UploadTest uploadTest : test.getUploads()) {
			logWithFileName(test.getName(), "Suppression du fichier %s", uploadTest);
			VersionFile version = getVersionTest(uploadTest, test);
			getDatasetManager().deleteVersion(version);
			version = getVersionTest(uploadTest, test);
			assertNull("la version n'a pas été supprimée", version);
		}
	}

	protected void publishData(Test test) throws BusinessException,
			PersistenceException {
		for (UploadTest uploadTest : test.getUploads()) {
			logWithFileName(test.getName(), "Publication du fichier %s", uploadTest);
			VersionFile version = getVersionTest(uploadTest, test);
			getDatasetManager().publishVersion(version);
			version = getVersionTest(uploadTest, test);
			assertEquals("la version publiée n'est pas la bonne", version, version
					.getDataset().getPublishVersion());
			assertEquals("l'utilisateur de dépôt n'est pas le bon", test
					.getUtilisateur().getLogin(), version.getUploadUser()
					.getLogin());
		}
	}

	protected void unPublishData(Test test) throws BusinessException,
			PersistenceException {
		for (UploadTest uploadTest : test.getUploads()) {
			logWithFileName(test.getName(), "Dé-publication du fichier %s", uploadTest);
			VersionFile version = getVersionTest(uploadTest, test);
			getDatasetManager().unPublishVersion(version);
			version = getVersionTest(uploadTest, test);
			assertNull("la version n'a pas été dépubliée", version.getDataset()
					.getPublishVersion());
		}
	}

	protected void extractData(Test test) throws PersistenceException, BusinessException {
	        getExtractionManager().extract(getParameter(test), test.getExtract().getAffichage());
	}

	abstract protected IParameter getParameter(Test test) throws PersistenceException;

	@SuppressWarnings("unused")
	protected void checkExtractData(Test test) throws BusinessException, IOException, PersistenceException {
		ZipFile zipFile = getExtractZip();
		ZipEntry entry;
		Enumeration<? extends ZipEntry> entries = zipFile.entries();
		Long orderOfentry = new Long(0);
		List<ErrorsReport> errorsReports = new LinkedList<AbstractTest.ErrorsReport>();
		while (entries.hasMoreElements()) {
			entry = entries.nextElement();
			if (entry.getName().startsWith("recapitulatif_extraction_")) {
				continue;
			}
			orderOfentry++;
			log(test.getName(), String.format("vérification du fichier : %s", entry.getName()));
			String fileEncoding = Utils.detectStreamEncoding(new BufferedInputStream(zipFile.getInputStream(entry)));
			CSVParser parser = new CSVParser(new InputStreamReader(zipFile.getInputStream(entry), fileEncoding), SEPARATOR);
			ErrorsReport errorsReport;
			if (test.getDownloads().size() == 0 || test.getDownloads().get(orderOfentry) == null) {
				errorsReport = new ErrorsReport();
				errorsReport.addErrorMessage(String.format("Le fichier d'extraction %s n'est pas attendu", entry.getName()));
				errorsReports.add(errorsReport);
				continue;
			} else {
				errorsReport = new ErrorsReport(entry.getName(), test.getDownloads().get(orderOfentry).getOutFile().getFileName());
			}
			CSVParser parserControl = new CSVParser(new InputStreamReader(new FileInputStream(test.getDownloads().get(orderOfentry).getOutFile().getPath()), fileEncoding), SEPARATOR);
			String[] values = null;
			String[] expectedValues = null;
			int lineNumber = 0;
			String[] enTetes = null;
			while ((values = parser.getLine()) != null) {
				lineNumber++;
				expectedValues = parserControl.getLine();
				if (expectedValues == null) {
					errorsReport.addErrorMessage(String.format("Le fichier d'extraction contient plus de lignes (%d) que le fichier de contrôle (%d)", parser.getLastLineNumber(),
							parserControl.getLastLineNumber()));
					break;
				}
				String value;
				String expectedValue;
				int i;
				for (i = 0; i < values.length; i++) {
					if (i >= expectedValues.length) {
						errorsReport.addErrorMessage(String.format("Ligne %d, le fichier d'extraction contient plus de valeurs (%d) que le fichier de contrôle (%d)", lineNumber, values.length,
								expectedValues.length));
						break;
					}
					value = values[i];
					expectedValue = expectedValues[i];
					if (!value.equals(expectedValue)) {
						errorsReport.addErrorMessage(String.format("ligne %d colonne %d, la valeur attendue %s est différente de la valeur retournée %s", lineNumber, i + 1, expectedValue, value));
					}
				}
				if (i < values.length) {
					errorsReport.addErrorMessage(String.format("Ligne %d, le fichier d'extraction contient moins de valeurs (%d) que le fichier de contrôle (%d)", lineNumber, values.length,
							expectedValues.length));
				}
			}
			if (errorsReport.hasErrors()) {
				errorsReports.add(errorsReport);
			}
		}
		zipFile.close();
		if (!errorsReports.isEmpty()) {
			String error = new String();
			for (ErrorsReport errorsReport : errorsReports) {
				error = error.concat(errorsReport.getErrorsMessages()).concat(ErrorsReport.NEW_LINE);
			}
			fail(error);
		}
	}

	private ZipFile getExtractZip() throws IOException {
		String path = attachements.get(attachements.size() - 1);
		return new ZipFile(getConfiguration().getRepositoryURI().concat(System.getProperty("file.separator")).concat(path));
	}

	protected DatesRequestParamVO getDatesRequestParam(Test test) {
		DatesRequestParamVO datesRequestParamVO = new DatesRequestParamVO();
		AbstractDatesFormParam datesRequest = test.getExtract().getExtractDatesTest();
		if (DatesYearsContinuousFormParamVO.class.equals(datesRequest.getClass())) {
			datesRequestParamVO.setDatesYearsContinuousFormParam((DatesYearsContinuousFormParamVO) datesRequest);
			datesRequestParamVO.setSelectedFormSelection(DatesYearsContinuousFormParamVO.LABEL);
		} else if (DatesYearsDiscretsFormParamVO.class.equals(datesRequest.getClass())) {
			datesRequestParamVO.setDatesYearsDiscretsFormParam((DatesYearsDiscretsFormParamVO) datesRequest);
			datesRequestParamVO.setSelectedFormSelection(DatesYearsDiscretsFormParamVO.LABEL);
		} else if (DatesYearsRangeFormParamVO.class.equals(datesRequest.getClass())) {
			datesRequestParamVO.setDatesYearsRangeFormParam((DatesYearsRangeFormParamVO) datesRequest);
			datesRequestParamVO.setSelectedFormSelection(DatesYearsRangeFormParamVO.LABEL);
		}
		return datesRequestParamVO;
	}

	protected Object getDepthRequestParamVO(Test test) {
		return test.getExtract().getExtractDatasTest().getDepthRequestParamVO();
	}

	protected DatasRequestParamVO getDatasRequestParam(Test test) {
		DatasRequestParamVO datasRequestParamVO = new DatasRequestParamVO();
		datasRequestParamVO.setLocalizationManager(test.getLocalizationManager());
		datasRequestParamVO.setDataBalancedByDepth(test.getExtract().getExtractDatasTest().getDatasBalancedByDepht());
		datasRequestParamVO.setMaxValueAndAssociatedDepth(test.getExtract().getExtractDatasTest().getMaxValueAndAssociatedDepht());
		datasRequestParamVO.setMinValueAndAssociatedDepth(test.getExtract().getExtractDatasTest().getMinValueAndAssociatedDepht());
		datasRequestParamVO.setRawData(test.getExtract().getExtractDatasTest().getRawDatas());
		datasRequestParamVO.setExtractionParameters(test.getExtract().getExtractDatasTest().getExtractionParameters());
		return datasRequestParamVO;
	}

	protected List<VariableVO> getVariablesTests(Test test) throws PersistenceException {
		variablesVO = new LinkedList<VariableVO>();
		for (VariableTest variableTest : test.getExtract().getVariablesTests()) {
			variablesVO.add(new VariableVO((VariableGLACPE) variableDAO.getByCode(variableTest.getCode())));
		}
		return variablesVO;
	}

	protected List<PlateformeVO> getPlateformesTests(Test test) throws PersistenceException {
		List<PlateformeVO> plateformesVO = new LinkedList<PlateformeVO>();
		for (UploadTest uploadTest : test.getUploads()){
			for (PlateformeTest plateforme : test.getExtract().getPlateformesTests()) {
				ProjetSite projetSite = projetSiteDAO.getBySiteCodeAndProjetCode(uploadTest.getSiteCode(), test.getExtract().getProjectCode());
				Plateforme plateformeDB = plateformeDAO.getByNKey(plateforme.getCode(),uploadTest.getSiteCode());
				if(plateformeDB != null && projetSite != null)
				{
					PlateformeVO plateformeVO = new PlateformeVO(plateformeDB);
					ProjetSiteVO projetSiteVO = new ProjetSiteVO(projetSite);
					plateformeVO.setProjet(projetSiteVO);
					plateformesVO.add(plateformeVO);
				}
			}			
		}
		return plateformesVO;
	}
	
	protected List<Taxon> getTaxonTests(Test test) throws PersistenceException {
		List<Taxon> taxonList = new LinkedList<Taxon>();
		for(TaxonTest taxon : test.getExtract().getTaxonsTest())
		{
			taxonList.add(taxonDAO.getByCode(taxon.getCode()));
		}
		return taxonList;
	}

	private VersionFile getVersionTest(UploadTest uploadTest, Test test) throws BusinessException,
			PersistenceException {
		List<Dataset> datasets = getDatasetManager().retrieveDatasets(
				getProjetSiteThemeDatatypeDAO().getByPathProjetSiteThemeCodeAndDatatypeCode(Utils.createCodeFromString(uploadTest.getProjetCode()),
						siteDAO.getByCode(Utils.createCodeFromString(uploadTest.getSiteCode())).getPath(),
						Utils.createCodeFromString(uploadTest.getThemeCode()),
						Utils.createCodeFromString(uploadTest.getDatatypeCode())
					));
		for (Dataset dataset : datasets) {
			return dataset.getLastVersion();
		}
		return null;
	}

	private VersionFile getNewVersionTest(UploadTest uploadTest)
			throws BusinessException, PersistenceException {
		return getDatasetManager().getVersionFromFileName(
				uploadTest.getInfileName(),
				getProjetSiteThemeDatatypeDAO().getByPathProjetSiteThemeCodeAndDatatypeCode(Utils.createCodeFromString(uploadTest.getProjetCode()),
						siteDAO.getByCode(Utils.createCodeFromString(uploadTest.getSiteCode())).getPath(),
						Utils.createCodeFromString(uploadTest.getThemeCode()),
						Utils.createCodeFromString(uploadTest.getDatatypeCode())
					)
				);
	}

	private static byte[] getBytesFromFile(File file) throws IOException {
		InputStream is = new FileInputStream(file);
		long length = file.length();
		byte[] bytes = new byte[(int) length];
		int offset = 0;
		int numRead = 0;
		while (offset < bytes.length
				&& (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
			offset += numRead;
		}
		is.close();
		if (offset < bytes.length) {
			throw new IOException("Could not completely read file "+ file.getName());
		}	
		return bytes;
	}

	protected MockHttpSession session;
    protected MockHttpServletRequest request;

    protected void startSession() {
        session = new MockHttpSession();
    }

    protected void endSession() {
        session.clearAttributes();
        session = null;
    }

    protected void startRequest() {
        request = new MockHttpServletRequest();
        request.setSession(session);
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
    }

    protected void endRequest() {
        ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).requestCompleted();
        RequestContextHolder.resetRequestAttributes();
        request = null;
    }

	protected void log(String testName, String message) {
		logger.info(new LoggerObject(getTest(testName).getUtilisateur().getLogin(), new Date(), message));
	}

	protected void logWithFileName(String testName, String message, UploadTest uploadTest)
			throws BusinessException {
		logger.info(new LoggerObject(getTest(testName).getUtilisateur()
				.getLogin(), new Date(), String.format(message,
				uploadTest.getInfileName())));
	}

	protected void logWithUserName(String testName, String message) throws BusinessException {
		logger.info(new LoggerObject(getTest(testName).getUtilisateur().getLogin(), new Date(), String.format(message, getTest(testName).getUtilisateurLogin())));
	}

	public static void addNotification(Notification notification) {
		notifications.get(notification.getLevel()).put(notifications.get(notification.getLevel()).size(), notification);
		if (notification.getAttachment() != null)
			attachements.add(notification.getAttachment());
	}

	public class TokenizerValues {

		private String[] values;
		private int valueIndex = 0;

		public TokenizerValues(String[] values) {
			this.values = values;
		}

		private Float convertToFloat(String value) throws NumberFormatException {
			value = value.replaceAll(",", ".");
			if (value.isEmpty()) {
				return null;
			} else {
				return Float.parseFloat(value);
			}
		}

		public Float currentTokenFloat() throws NumberFormatException {
			return convertToFloat(currentToken());
		}

		public Float nextTokenFloat() throws NumberFormatException {
			return convertToFloat(values[valueIndex++].trim());
		}

		public String currentToken() {
			String value = values[valueIndex].trim();
			if (value.isEmpty()) {
				return null;
			} else {
				return value;
			}
		}

		public int currentTokenIndex() {
			return valueIndex;
		}

		public String nextToken() {
			String result = currentToken();
			valueIndex++;
			return result;
		}
	}

	protected static String getMethodeName() {
		Throwable t = new Throwable();
		t.fillInStackTrace();
		StackTraceElement e = t.getStackTrace()[1];
		String functionName = e.getMethodName();
		return functionName;
	}

	protected void addToPath(String path, String toAdd) {
		path = String.format(ADD_TO_PATH, path, toAdd);
	}

	class ErrorsReport {

		private static final String NEW_LINE = "\n";
		private static final String TIRET = "-";
		private static final String ERROR_MESSAGE = "Le fichier d'extraction %s n'est pas conforme au fichier de contrôle %S";
		private String errorsMessages;
		int errors = 0;

		public ErrorsReport(String extractionFileName, String controlefileName) {
			super();
			errorsMessages = new String(String.format(ERROR_MESSAGE, extractionFileName, controlefileName));
		}

		public ErrorsReport() {
			super();
			errorsMessages = "";
		}

		public void addErrorMessage(String errorMessage) {
			errorsMessages = errorsMessages.concat(TIRET).concat(errorMessage).concat(NEW_LINE);
			errors++;
		}

		public int getCountErrors() {
			return errors;
		}

		public String getErrorsMessages() {
			return errorsMessages;
		}

		public boolean hasErrors() {
			return errors > 0;
		}
	}
}