package org.inra.ecoinfo.glacpe;

import org.inra.ecoinfo.glacpe.chimie.ChimieTest;
import org.inra.ecoinfo.glacpe.conditionprelevement.ConditionGeneraleTest;
import org.inra.ecoinfo.glacpe.phytoplancton.PhytoTest;
import org.inra.ecoinfo.glacpe.ppchlorotransp.PPChloroTranspTest;
import org.inra.ecoinfo.glacpe.sonde.SondeTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses(value = { ChimieTest.class, SondeTest.class, PPChloroTranspTest.class, ConditionGeneraleTest.class, PhytoTest.class })
public class LacSuite {
	
}
