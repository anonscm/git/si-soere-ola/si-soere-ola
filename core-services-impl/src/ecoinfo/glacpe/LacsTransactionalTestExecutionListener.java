package org.inra.ecoinfo.glacpe;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.Map.Entry;
import java.util.zip.CRC32;
import java.util.zip.CheckedInputStream;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseDataSourceConnection;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.inra.ecoinfo.config.impl.CoreConfiguration;
import org.inra.ecoinfo.dataset.IDatasetManager;
import org.inra.ecoinfo.dataset.security.ISecurityDatasetManager;
import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.extraction.IRequeteDAO;
import org.inra.ecoinfo.glacpe.refdata.groupevariable.IGroupeVariableBuilder;
import org.inra.ecoinfo.glacpe.refdata.plateforme.IPlateformeDAO;
import org.inra.ecoinfo.glacpe.refdata.projetsite.IProjetSiteDAO;
import org.inra.ecoinfo.glacpe.refdata.projetsitethemedatatype.IProjetSiteThemeDatatypeDAO;
import org.inra.ecoinfo.glacpe.refdata.taxon.ITaxonDAO;
import org.inra.ecoinfo.identification.IUsersManager;
import org.inra.ecoinfo.identification.IUtilisateurDAO;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.notifications.exceptions.UpdateNotificationException;
import org.inra.ecoinfo.refdata.IMetadataManager;
import org.inra.ecoinfo.refdata.datatype.IDatatypeDAO;
import org.inra.ecoinfo.refdata.node.INodeDAO;
import org.inra.ecoinfo.refdata.site.ISiteDAO;
import org.inra.ecoinfo.refdata.theme.IThemeDAO;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.security.IPrivilegeDAO;
import org.inra.ecoinfo.security.IPrivilegeTypeDAO;
import org.inra.ecoinfo.security.IRoleDAO;
import org.inra.ecoinfo.security.ISecurityContext;
import org.inra.ecoinfo.utils.FileWithFolderCreator;
import org.inra.ecoinfo.utils.LoggerObject;
import org.inra.ecoinfo.utils.configuration.AbstractConfigurationTest;
import org.inra.ecoinfo.utils.configuration.FileTest;
import org.inra.ecoinfo.utils.configuration.RefdataTest;
import org.inra.ecoinfo.utils.exceptions.BadFormatException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.springframework.context.ApplicationContext;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.test.context.TestContext;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

public class LacsTransactionalTestExecutionListener extends TransactionalTestExecutionListener {

	private static final String					LOG4J_PATH		= "../src/test/resources/log4jTest.properties";
	public static final String					ADMIN_TESTEUR	= "adminTest";
	public static final Logger					logger			= Logger.getLogger("tests");
	private final static String					CLEAN_ABSTRACT_	= "cleanAbstract";
	private final static String					INPUT_ABSTRACT	= "inputAbstract";
	public static final String					UNDERSCORE		= "_";
	public static final String					CSV				= ".csv";
	protected static ApplicationContext			applicationContext;
	protected static IUsersManager				usersManager;
	protected static AbstractConfigurationTest			configurationTest;
	protected static CoreConfiguration				configuration;
	protected static JpaTransactionManager		transactionManager;
	protected static IMetadataManager			metadataManager;
	protected static ISecurityDatasetManager 	securityDatasetManager;
	protected static IPlateformeDAO plateformeDAO;
	protected static IVariableDAO variableDAO;
	protected static INodeDAO nodeDAO;
	protected static IProjetSiteThemeDatatypeDAO projetSiteThemeDatatypeDAO;
	protected static ITaxonDAO taxonDAO;
	protected static ISiteDAO siteDAO;
	protected static IThemeDAO themeDAO;
	protected static IDatatypeDAO datatypeDAO;
	protected static IProjetSiteDAO projetSiteDAO;
	protected static IGroupeVariableBuilder groupeVariableBuilder;
	protected static IExtractionManager extractionManager;
	protected static IRequeteDAO requeteDAO;
	protected static IPrivilegeDAO privilegeDAO;
	protected static IPrivilegeTypeDAO privilegeTypeDAO;
	protected static IRoleDAO roleDAO;
	protected static IUtilisateurDAO utilisateurDAO;
	private static boolean						isContextLoaded	= false;
	private static DataSource					dataSource;
	private static DatabaseDataSourceConnection	datasourceConnection;
	private static IDatasetManager				datasetManager;
	private static String repositoryUriTest;

    protected MockHttpSession session;
    protected MockHttpServletRequest request;

    protected void startSession() {
        session = new MockHttpSession();
    }

    protected void endSession() {
        session.clearAttributes();
        session = null;
    }

    protected void startRequest() {
        request = new MockHttpServletRequest();
        request.setSession(session);
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
    }

    protected void endRequest() {
        ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).requestCompleted();
        RequestContextHolder.resetRequestAttributes();
        request = null;
    }
    
	@Override
	public void beforeTestClass(TestContext testContext) throws Exception {
		PropertyConfigurator.configure(LOG4J_PATH);
		try {
			applicationContext = testContext.getApplicationContext();
		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.info(new LoggerObject(ADMIN_TESTEUR, new Date(), "initialisation du context"));
		usersManager = (IUsersManager) applicationContext.getBean("usersManager");
		configurationTest = (AbstractConfigurationTest) applicationContext.getBean("configurationTest");
		configuration = (CoreConfiguration) applicationContext.getBean("configuration");
		transactionManager = (JpaTransactionManager) applicationContext.getBean("transactionManager");
		metadataManager = (IMetadataManager) applicationContext.getBean("metadataManager");
		dataSource = (DataSource) applicationContext.getBean("dataSource");
		securityDatasetManager = (ISecurityDatasetManager) applicationContext.getBean("securityDatasetManager");
		datasetManager = (IDatasetManager) applicationContext.getBean("datasetManager");
		plateformeDAO = ((IPlateformeDAO) applicationContext.getBean("plateformeDAO"));
		nodeDAO = ((INodeDAO) applicationContext.getBean("siteThemeDatatypeDAO"));
		projetSiteThemeDatatypeDAO = ((IProjetSiteThemeDatatypeDAO) applicationContext.getBean("projetSiteThemeDatatypeDAO"));
		siteDAO = ((ISiteDAO) applicationContext.getBean("siteDAO"));
		themeDAO = ((IThemeDAO) applicationContext.getBean("themeDAO"));
		datatypeDAO = ((IDatatypeDAO) applicationContext.getBean("datatypeDAO"));
		variableDAO = ((IVariableDAO) applicationContext.getBean("variableDAO"));
		projetSiteDAO = ((IProjetSiteDAO) applicationContext.getBean("projetSiteDAO"));
		taxonDAO = ((ITaxonDAO) applicationContext.getBean("taxonDAO"));
		groupeVariableBuilder = ((IGroupeVariableBuilder) applicationContext.getBean("groupeVariableBuilder"));
		extractionManager = ((IExtractionManager)applicationContext.getBean("extractionManager"));
		privilegeDAO = ((IPrivilegeDAO)applicationContext.getBean("privilegeDAO"));
		privilegeTypeDAO = ((IPrivilegeTypeDAO)applicationContext.getBean("privilegeTypeDAO"));
		roleDAO = ((IRoleDAO)applicationContext.getBean("roleDAO"));
		utilisateurDAO = ((IUtilisateurDAO)applicationContext.getBean("utilisateurDAO"));
		requeteDAO = ((IRequeteDAO)applicationContext.getBean("requeteDAO"));
		super.beforeTestClass(testContext);
		if (!isContextLoaded) {
			repositoryUriTest = configuration.getRepositoryURI().replaceAll("(-test)?\057$","-test/");
			configuration.setRepositoryURI(repositoryUriTest);
			File repositoryTestFile = new File(repositoryUriTest);
			if(repositoryTestFile.exists()){
				FileWithFolderCreator.recursifDelete(repositoryTestFile);
			}
			repositoryTestFile.mkdirs();
			File[] contenu = repositoryTestFile.listFiles();
			for (int i = 0; i < contenu.length; i++) {
				File file = contenu[i];
				file.deleteOnExit();
			}
			new File(repositoryUriTest.concat("metadata")).mkdirs();
			new File(repositoryUriTest.concat("extraction")).mkdirs();
			new File(repositoryUriTest.concat("repository")).mkdirs();
			startSession();
			createTestConfig();
			loadMetadata();
			startSession();
			isContextLoaded = true;
		}
	}

	@Override
	public void afterTestClass(TestContext testContext) throws Exception {
		if (--AbstractTest.instanceNumber == 0)afterSuperClass(testContext);
	}

	private void afterSuperClass(TestContext testContext) throws IOException, DataSetException, DatabaseUnitException, SQLException, Exception {
		logger.info(new LoggerObject(ADMIN_TESTEUR, new Date(), "Nettoyage du contexte"));
		IDataSet dataSet = new FlatXmlDataSet(configurationTest.getFileByCode(CLEAN_ABSTRACT_));
		DatabaseOperation.CLEAN_INSERT.execute(getDatasourceConnexion(), dataSet);
		new File(repositoryUriTest).delete();
		super.afterTestClass(testContext);
	}

	protected void createTestConfig() throws BusinessException, IOException, SQLException, DatabaseUnitException {
		logger.info(new LoggerObject(ADMIN_TESTEUR, new Date(), "Initialisation de la base"));
		logger.info(new LoggerObject(ADMIN_TESTEUR, new Date(), "Initialisation des utilisateurs"));
		IDataSet dataSet = new FlatXmlDataSet(configurationTest.getFileByCode(CLEAN_ABSTRACT_));
		DatabaseOperation.CLEAN_INSERT.execute(getDatasourceConnexion(), dataSet);
		dataSet = new FlatXmlDataSet(configurationTest.getFileByCode(INPUT_ABSTRACT));
		DatabaseOperation.CLEAN_INSERT.execute(getDatasourceConnexion(), dataSet);
		for (Entry<String, Utilisateur> utilisateurTestEntry : configurationTest.getUtilisateurs().entrySet()) {
			usersManager.addUserProfile(utilisateurTestEntry.getValue(),"");
			logger.info(new LoggerObject(ADMIN_TESTEUR, new Date(), "Initialisation de l'utilisateur "
					.concat(usersManager.getUtilisateurByLogin(utilisateurTestEntry.getValue().getLogin()).getLogin())));
		}
		connect(usersManager.getUtilisateurByLogin(ADMIN_TESTEUR));
	}

	private DatabaseDataSourceConnection getDatasourceConnexion() throws SQLException {
		datasourceConnection = new DatabaseDataSourceConnection(dataSource, "public");
		datasourceConnection.getConfig().setFeature(DatabaseConfig.FEATURE_CASE_SENSITIVE_TABLE_NAMES, true);
		return datasourceConnection;
	}

	private void connect(Utilisateur utilisateurByLogin) throws BusinessException {
		logger.info(new LoggerObject(ADMIN_TESTEUR, new Date(), "Connection de l'utilisateur admin"));
		startRequest();
		getSecurityContext().initContext(utilisateurByLogin);
		startRequest();
	}

	private void loadMetadata() throws FileNotFoundException, IOException, UpdateNotificationException, BusinessException, BadFormatException {
		logger.info(new LoggerObject(ADMIN_TESTEUR, new Date(), "Initialisation des données de référence"));
		for (Entry<Integer, RefdataTest> refDataEntry : configurationTest
				.getRefsData().entrySet()) {
			startRequest();
			FileTest inFile = refDataEntry.getValue().getInFile();
			String dataTypecode = refDataEntry.getValue().getCode();
			if (inFile == null) continue;
			logger.info(new LoggerObject(ADMIN_TESTEUR, new Date(), "Initialisation de la donnée de référence " + dataTypecode));
			String repositoryURI = configuration.getRepositoryURI();
			String metadatasURI = repositoryURI.concat(System.getProperty("file.separator")).concat("metadata");
			String sourcePath = inFile.getPath();
			File sourceFile = FileWithFolderCreator.createFile(sourcePath);
			String resultPath = metadatasURI.concat(System.getProperty("file.separator").concat(sourceFile.getName()));
			File resultFile = FileWithFolderCreator.createFile(resultPath);
			FileWithFolderCreator.createOutputStream(resultPath);
			metadataManager.uploadMetadatas(sourceFile, dataTypecode, inFile.getFileName(), ADMIN_TESTEUR);
			logger.info(new LoggerObject(ADMIN_TESTEUR, new Date(), "fait ...! : ".concat(compare(sourceFile, resultFile) ? "fichier conforme" : "fichier non conforme")));
			endRequest();
		}
	}

	private boolean compare(File metadataFile, File metadataFileResult) throws IOException {
		CheckedInputStream originalInputStram = new CheckedInputStream(new FileInputStream(metadataFile.getPath()), new CRC32());
		CheckedInputStream resultInputStram = new CheckedInputStream(new FileInputStream(metadataFileResult.getPath()), new CRC32());
		byte[] sourceBuf = new byte[128];
		byte[] resultBuf = new byte[128];
		while (originalInputStram.read(sourceBuf) >= 0) {
		}
		while (resultInputStram.read(resultBuf) >= 0) {
		}
		long sourceChecksum = originalInputStram.getChecksum().getValue();
		long resultChecksum = resultInputStram.getChecksum().getValue();
		return sourceChecksum == resultChecksum;
	}

	public static String getAdminTesteur() {
		return ADMIN_TESTEUR;
	}

	public static Logger getLogger() {
		return logger;
	}

	public static ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	public static IUsersManager getUsersManager() {
		return usersManager;
	}

	public static void setDataSource(DataSource dataSource) {
		LacsTransactionalTestExecutionListener.dataSource = dataSource;
	}

	public static ISecurityContext getSecurityContext() {
		return (ISecurityContext) applicationContext.getBean("securityContext");
	}

	public static AbstractConfigurationTest getConfigurationTest() {
		return configurationTest;
	}

	public static CoreConfiguration getConfiguration() {
		return configuration;
	}

	public static JpaTransactionManager getTransactionManager() {
		return transactionManager;
	}

	public static IMetadataManager getMetadataManager() {
		return metadataManager;
	}
	
	public static ISecurityDatasetManager getSecurityDatasetManager() {
		return securityDatasetManager;
	}

	public static IDatasetManager getDatasetManager() {
		return datasetManager;
	}
	
	
	public static String getCleanAbstract() {
		return CLEAN_ABSTRACT_;
	}

	
	public static String getCsv() {
		return CSV;
	}

	
	public static IPlateformeDAO getPlateformeDAO() {
		return plateformeDAO;
	}

	
	public static IVariableDAO getVariableDAO() {
		return variableDAO;
	}

	
	public static IProjetSiteDAO getProjetSiteDAO() {
		return projetSiteDAO;
	}

	
	public static IGroupeVariableBuilder getGroupeVariableBuilder() {
		return groupeVariableBuilder;
	}

	
	public static DataSource getDataSource() {
		return dataSource;
	}

	
	public static DatabaseDataSourceConnection getDatasourceConnection() {
		return datasourceConnection;
	}

	
	public static String getRepositoryUriTest() {
		return repositoryUriTest;
	}

	
	public static IExtractionManager getExtractionManager() {
		return extractionManager;
	}

	
	public static INodeDAO getNodeDAO() {
		return nodeDAO;
	}

	
	public static ISiteDAO getSiteDAO() {
		return siteDAO;
	}

	
	public static IThemeDAO getThemeDAO() {
		return themeDAO;
	}

	
	public static IDatatypeDAO getDatatypeDAO() {
		return datatypeDAO;
	}

	
	public static IRequeteDAO getRequeteDAO() {
		return requeteDAO;
	}

	public static void setDatasetManager(IDatasetManager datasetManager) {
		LacsTransactionalTestExecutionListener.datasetManager = datasetManager;
	}

	public static void setSecurityDatasetManager(ISecurityDatasetManager securityDatasetManager) {
		LacsTransactionalTestExecutionListener.securityDatasetManager = securityDatasetManager;
	}
	
	public static ITaxonDAO getTaxonDAO() {
		return taxonDAO;
	}
	
	public static IPrivilegeDAO getPrivilegeDAO() {
		return privilegeDAO;
	}
	
	public static IProjetSiteThemeDatatypeDAO getProjetSiteThemeDatatypeDAO() {
		return projetSiteThemeDatatypeDAO;
	}

	public static IPrivilegeTypeDAO getPrivilegeTypeDAO() {
		return privilegeTypeDAO;
	}

	public static IRoleDAO getRoleDAO() {
		return roleDAO;
	}

	public static IUtilisateurDAO getUtilisateurDAO() {
		return utilisateurDAO;
	}
}
