package org.inra.ecoinfo.glacpe.chimie;

import static org.inra.ecoinfo.glacpe.LacsTransactionalTestExecutionListener.getGroupeVariableBuilder;
import static org.inra.ecoinfo.glacpe.LacsTransactionalTestExecutionListener.getVariableDAO;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.glacpe.AbstractTest;
import org.inra.ecoinfo.glacpe.dataset.chimie.impl.ChimieParameters;
import org.inra.ecoinfo.glacpe.extraction.vo.DatasRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.DepthRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.GroupeVariableVO;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO;
import org.inra.ecoinfo.glacpe.extraction.vo.VariableVO;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.configuration.VariableTest;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.ErrorNotificationException;
import org.inra.ecoinfo.utils.exceptions.NotificationException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;

import com.Ostermiller.util.CSVParser;

public class ChimieTest  extends AbstractTest{

	public ChimieTest() {
		super();
	}
	private static final String PATH_TO_MESURE = "%s/%s/%s";

	@Test
	@Transactional
	public void chimie1() throws BusinessException, IOException, PersistenceException, NotificationException{
		test(getMethodeName());
	}

	@Test
	@Transactional
	public void chimie2() throws BusinessException, IOException, PersistenceException, NotificationException{
		test(getMethodeName());
	}
	
	@Test
	@Transactional
	public void chimie3() throws IOException, PersistenceException, NoExtractionResultException, NotificationException{
		try {
			test(getMethodeName());
		} catch (Exception e) {
			if(e.getCause().getClass().equals(NoExtractionResultException.class)){
				throw (NoExtractionResultException)e.getCause();
			}
			throw new UnknownError();
		}
	}

	@Test(expected=ErrorNotificationException.class)
	@Transactional
	public void chimie4() throws BusinessException, IOException, PersistenceException, NotificationException{
		test(getMethodeName());
	}

	@Test(expected=ErrorNotificationException.class)
	@Transactional
	public void chimie5() throws BusinessException, IOException, PersistenceException, NotificationException{
		test(getMethodeName());
	}

	@Test
	@Transactional
	public void chimie6() throws BusinessException, IOException, PersistenceException, NotificationException{
		test(getMethodeName());
	}

	@Test
	@Transactional
	public void chimie7() throws BusinessException, IOException, PersistenceException, NotificationException{
		test(getMethodeName());
	}

	@Override
	protected IParameter getParameter(org.inra.ecoinfo.utils.configuration.Test test) throws PersistenceException {
		Map<String, Object> metadatasMap = new HashMap<String, Object>();
		metadatasMap.put(PlateformeVO.class.getSimpleName(), getPlateformesTests(test));
		metadatasMap.put(VariableVO.class.getSimpleName(), getVariablesTests(test));
		metadatasMap.put(DatasRequestParamVO.class.getSimpleName(), getDatasRequestParam(test));
		metadatasMap.put(DepthRequestParamVO.class.getSimpleName(), getDepthRequestParamVO(test));
		metadatasMap.put(DatesRequestParamVO.class.getSimpleName(), getDatesRequestParam(test));
		metadatasMap.put(IExtractionManager.KEYMAP_COMMENTS, test.getExtract().getComment());
		IParameter parameters = new ChimieParameters(metadatasMap);
		parameters.setCommentaire(test.getExtract().getComment());
		return parameters;
	}
	
	protected void checkExtractData(Map<String, Map<String, Map<String, Map<String, Map<String, Float>>>>> buildMap, ZipEntry entry, ZipFile zipFile, org.inra.ecoinfo.utils.configuration.Test test)
			throws IOException {
		log(test.getName(), String.format("vérification du fichier brut : %s", entry.getName()));
		String fileEncoding = Utils.detectStreamEncoding(new BufferedInputStream(zipFile.getInputStream(entry)));
		CSVParser parser = new CSVParser(new InputStreamReader(zipFile.getInputStream(entry), fileEncoding), SEPARATOR);
		String[] values = null;
		int lineNumber = 0;
		String[] enTetes = null;
		while ((values = parser.getLine()) != null) {
			TokenizerValues tokenizerValues = new TokenizerValues(values);
			if (lineNumber++ == 0) {
				enTetes = new String[values.length];
				for (int i = 0; i < values.length; i++) {
					enTetes[i] = Utils.createCodeFromString(tokenizerValues.nextToken());
				}
			} else {
				String projectCode = Utils.createCodeFromString(tokenizerValues.nextToken());
				String siteCode = Utils.createCodeFromString(tokenizerValues.nextToken());
				String plateformeCode = Utils.createCodeFromString(tokenizerValues.nextToken());
				String date = Utils.createCodeFromString(tokenizerValues.nextToken());
				String profondeur = tokenizerValues.nextTokenFloat().toString();
				String pathToMesure = String.format(PATH_TO_MESURE, projectCode, siteCode, plateformeCode);
				for (int i = 5; i < values.length; i++) {
					String variableCode = enTetes[i];
					Float expectedValue = null;
					try {
						expectedValue = buildMap.get(pathToMesure).get(DATES).get(date).get(profondeur).get(variableCode);
					} catch (NullPointerException e) {
						fail("la valeur n'est pas dans le fichier d'entrée");
					}
					Float value = tokenizerValues.nextTokenFloat();
					assertEquals(String.format("Dans le fichier de données brutes ligne %d colonne %d la valeur attendue est incorrecte", lineNumber, i), expectedValue, value);
				}
			}
		}
		log(test.getName(), String.format("vérification du fichier brut : %s OK", entry.getName()));
	}

	protected void checkExtractAggregatedData(Map<String, Map<String, Map<String, Map<String, Map<String, Float>>>>> buildMap, ZipFile zipFile, ZipEntry entry, org.inra.ecoinfo.utils.configuration.Test test)
			throws IOException {
		log(test.getName(), String.format("vérification du fichier de données aggrégées : %s", entry.getName()));
		String fileEncoding = Utils.detectStreamEncoding(new BufferedInputStream(zipFile.getInputStream(entry)));
		CSVParser parser = new CSVParser(new InputStreamReader(zipFile.getInputStream(entry), fileEncoding), SEPARATOR);
		String[] values = null;
		int lineNumber = 0;
		String[] variables = null;
		String[] types = null;
		while ((values = parser.getLine()) != null) {
			TokenizerValues tokenizerValues = new TokenizerValues(values);
			if (lineNumber++ == 0) {
				variables = new String[values.length];
				types = new String[values.length];
				for (int i = 0; i < values.length; i++) {
					String value = Utils.createCodeFromString(tokenizerValues.nextToken());
					String[] split = value.split("[()]");
					if (split.length <= 1) {
						variables[i] = value;
						continue;
					}
					variables[i] = split[1];
					types[i] = split[0];
				}
			} else {
				String projectCode = Utils.createCodeFromString(tokenizerValues.nextToken());
				String siteCode = Utils.createCodeFromString(tokenizerValues.nextToken());
				String plateformeCode = Utils.createCodeFromString(tokenizerValues.nextToken());
				String date = Utils.createCodeFromString(tokenizerValues.nextToken());
				/* String profondeurMin = */tokenizerValues.nextToken();
				/* String profondeurMax = */tokenizerValues.nextToken();
				String profondeurString = tokenizerValues.nextToken();
				for (int i = 7; i < values.length; i++) {
					if (i >= variables.length) break;
					Float value = null;
					try {
						value = tokenizerValues.nextTokenFloat();
					} catch (ArrayIndexOutOfBoundsException e) {
						continue;
					}
					if (value == null) {
						continue;
					}
					String variableCode = variables[i];
					String type = types[i];
					Float expectedValue = null;
					try {				
						String pathToMesure = String.format(PATH_TO_MESURE, projectCode, siteCode, plateformeCode);
						if (MIN.equals(type)) {
							expectedValue = profondeurString == null ? null : buildMap.get(pathToMesure).get(DATES).get(date).get(MIN).get(variableCode);
						} else if (MAX.equals(type)) {
							expectedValue = profondeurString == null ? null : buildMap.get(pathToMesure).get(DATES).get(date).get(MAX).get(variableCode);
						} else if (AVERAGE.equals(type)) {
							expectedValue = buildMap.get(pathToMesure).get(DATES).get(date).get(AVERAGE).get(variableCode);
							continue;// en attendant les specif sur la moyenne
						}
					} catch (NullPointerException e) {
						fail("la valeur n'est pas dans le fichier d'entrée");
					}
					assertEquals(String.format("Dans le fichier de données brutes ligne %d colonne %d la valeur attendue est incorrecte pour %s : %s : %s", lineNumber, i, type, variableCode, date),
							expectedValue, value);
				}
			}
		}
		log(test.getName(), String.format("vérification du fichier brut : %s OK", entry.getName()));
	}

	/*@Override
	protected void checkZipFile(ZipFile zipFile, org.inra.ecoinfo.utils.configuration.Test test) throws BusinessException, IOException, PersistenceException {
		ZipEntry entry;
		Enumeration<? extends ZipEntry> entries = zipFile.entries();
		Map<String, Map<String, Map<String, Map<String, Map<String, Float>>>>> buildMap = buildMap(test,zipFile);
		while (entries.hasMoreElements()) {
			entry = entries.nextElement();
			if (entry.getName().startsWith(Utils.createCodeFromString(test.getUpload().getSiteCode()).concat(UNDERSCORE).concat("aggregated"))) {
				checkExtractAggregatedData(buildMap, zipFile, entry, test);
			} else if (entry.getName().startsWith("recapitulatif_extraction__")) {
				checkExtractRecapitulatifData(entry, zipFile, test);
			} else {
				checkExtractData(buildMap, entry, zipFile, test);
			}
		}
		zipFile.close();
	}@SuppressWarnings("unused")
	
	private Map<String, Map<String, Map<String, Map<String, Map<String, Float>>>>> buildMap(org.inra.ecoinfo.utils.configuration.Test test, ZipFile zipFile) throws BusinessException, BadDelimiterException, IOException {
		String extractSiteCode = test.getUpload().getSiteCode();
		String extractThemeCode = test.getUpload().getThemeCode();
		String extractDatatypeCode = test.getUpload().getDatatypeCode();
		String path = String.format(SecurityDatasetManager.RESOURCE_PATH, extractSiteCode, extractThemeCode, extractDatatypeCode);
		List<String> availableVariables = new LinkedList<String>();
		Map<String, Map<String, Map<String, Map<String, Map<String, Float>>>>> fileParsing = new TreeMap<String, Map<String, Map<String, Map<String, Map<String, Float>>>>>();
		File entryFile = test.getInFile();
		if (!getSecurityContext().hasPrivilege(path, Role.ROLE_EXTRACTION, ISecurityDatasetManager.PRIVILEGE_TYPE_DATASET)) {
			assertTrue("Les droits ne permettent pas d'extraire ce type de données", zipFile.size()<=1);
			return fileParsing;
		}
		String fileEncoding = Utils.detectStreamEncoding(new BufferedInputStream(new FileInputStream(entryFile)));
		CSVParser parser = new CSVParser(new InputStreamReader(new FileInputStream(entryFile), fileEncoding), SEPARATOR);
		String[] values = null;
		int lineNumber = 0;
		while ((values = parser.getLine()) != null) {
			if (lineNumber++ == 0) {
				continue;
			}
			TokenizerValues tokenizerValues = new TokenizerValues(values);
			String projectCode = Utils.createCodeFromString(tokenizerValues.nextToken());
			String siteCode = Utils.createCodeFromString(tokenizerValues.nextToken());
			String plateformeCode = Utils.createCodeFromString(tokenizerValues.nextToken());
			String date = Utils.createCodeFromString(tokenizerValues.nextToken());
			String dateDebutCampagne = Utils.createCodeFromString(tokenizerValues.nextToken());
			String dateFinCampagne = Utils.createCodeFromString(tokenizerValues.nextToken());
			String dateReception = Utils.createCodeFromString(tokenizerValues.nextToken());
			String profondeur = tokenizerValues.nextTokenFloat().toString();
			String variableCode = Utils.createCodeFromString(tokenizerValues.nextToken());
			Float value = tokenizerValues.nextTokenFloat();
			path = String.format(SecurityDatasetManager.RESOURCE_PATH_WITH_VARIABLE, siteCode, extractThemeCode, extractDatatypeCode, variableCode);
			if (getSecurityContext().hasPrivilege(path, Role.ROLE_EXTRACTION, ISecurityDatasetManager.PRIVILEGE_TYPE_DATASET)) {
				String pathToMesure = String.format(PATH_TO_MESURE, projectCode, siteCode, plateformeCode);
				if (fileParsing.get(pathToMesure) == null) {
					fileParsing.put(pathToMesure, new TreeMap<String, Map<String, Map<String,Map<String,Float>>>>());
					fileParsing.get(pathToMesure).put(DATES, new TreeMap<String, Map<String, Map<String,Float>>>());
				}
				if (fileParsing.get(pathToMesure).get(DATES).get(date) == null) {
					fileParsing.get(pathToMesure).get(DATES).put(date, new TreeMap<String, Map<String, Float>>());
				}
				if (fileParsing.get(pathToMesure).get(DATES).get(date).get(profondeur) == null) {
					fileParsing.get(pathToMesure).get(DATES).get(date).put(profondeur, new TreeMap<String, Float>());
				}
				if (fileParsing.get(pathToMesure).get(DATES).get(date).get(MIN) == null) {
					fileParsing.get(pathToMesure).get(DATES).get(date).put(MIN, new TreeMap<String, Float>());
				}
				if (fileParsing.get(pathToMesure).get(DATES).get(date).get(MAX) == null) {
					fileParsing.get(pathToMesure).get(DATES).get(date).put(MAX, new TreeMap<String, Float>());
				}
				if (fileParsing.get(pathToMesure).get(DATES).get(date).get(AVERAGE) == null) {
					fileParsing.get(pathToMesure).get(DATES).get(date).put(AVERAGE, new TreeMap<String, Float>());
				}
				if (fileParsing.get(pathToMesure).get(DATES).get(date).get(COUNT) == null) {
					fileParsing.get(pathToMesure).get(DATES).get(date).put(COUNT, new TreeMap<String, Float>());
				}
				fileParsing.get(pathToMesure).get(DATES).get(date).get(profondeur).put(variableCode, value);
				if (fileParsing.get(pathToMesure).get(DATES).get(date).get(COUNT).isEmpty()
						|| fileParsing.get(pathToMesure).get(DATES).get(date).get(COUNT).get(variableCode) == null) {
					fileParsing.get(pathToMesure).get(DATES).get(date).get(COUNT).put(variableCode, new Float(1));
				} else {
					fileParsing.get(pathToMesure).get(DATES).get(date).get(COUNT).put(variableCode,
							fileParsing.get(pathToMesure).get(DATES).get(date).get(COUNT).get(variableCode) + 1);
				}
				if ((fileParsing.get(pathToMesure).get(DATES).get(date).get(MIN).isEmpty() || fileParsing.get(pathToMesure)
						.get(DATES).get(date).get(MIN).get(variableCode) == null)
						|| value < fileParsing.get(pathToMesure).get(DATES).get(date).get(MIN).get(variableCode)) {
					fileParsing.get(pathToMesure).get(DATES).get(date).get(MIN).put(variableCode, value);
				}
				if (fileParsing.get(pathToMesure).get(DATES).get(date).get(MAX).isEmpty()
						|| fileParsing.get(pathToMesure).get(DATES).get(date).get(MAX).get(variableCode) == null
						|| value > fileParsing.get(pathToMesure).get(DATES).get(date).get(MAX).get(variableCode)) {
					fileParsing.get(pathToMesure).get(DATES).get(date).get(MAX).put(variableCode, value);
				}
				if (fileParsing.get(pathToMesure).get(DATES).get(date).get(AVERAGE).isEmpty()
						|| fileParsing.get(pathToMesure).get(DATES).get(date).get(AVERAGE).get(variableCode) == null) {Test
					fileParsing.get(pathToMesure).get(DATES).get(date).get(AVERAGE).put(variableCode, value);
				} else {
					Float average = fileParsing.get(pathToMesure).get(DATES).get(date).get(AVERAGE).get(variableCode);
					Float count = fileParsing.get(pathToMesure).get(DATES).get(date).get(COUNT).get(variableCode);
					average = (average * (count - 1) + value) / count;
					fileParsing.get(pathToMesure).get(DATES).get(date).get(AVERAGE).put(variableCode, average);
				}
			}
		}
		return fileParsing;
	}*/
	
	@Override
	protected List<VariableVO> getVariablesTests(org.inra.ecoinfo.utils.configuration.Test test) throws PersistenceException {
		if (variablesVO == null) {
			variablesVO = new LinkedList<VariableVO>();
			List<VariableGLACPE> variablesDB = new LinkedList<VariableGLACPE>();
			List<String> variablesCodes = new LinkedList<String>();
			for (VariableTest variableTest : test.getExtract().getVariablesTests()) {
				variablesDB.add((VariableGLACPE) getVariableDAO().getByCode(variableTest.getCode()));
				if (!variablesCodes.contains(variableTest.getCode())) variablesCodes.add(variableTest.getCode());
			}
			List<GroupeVariableVO> groupesVariables = getGroupeVariableBuilder().build(variablesDB);
			for (GroupeVariableVO groupeVariableVO : groupesVariables) {
				List<Long> variableIds = new LinkedList<Long>();
				for (VariableVO variableVO : groupeVariableVO.getVariables()) {
					if (variablesCodes.contains(variableVO.getCode()) && !variablesVO.contains(variableVO) && !variableIds.contains(variableVO.getId())) {
						variablesVO.add(variableVO);
						variableIds.add(variableVO.getId());
					}
				}
			}
		}
		return variablesVO;
	}
	
}
