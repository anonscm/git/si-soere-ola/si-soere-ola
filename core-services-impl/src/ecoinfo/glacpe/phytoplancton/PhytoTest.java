package org.inra.ecoinfo.glacpe.phytoplancton;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.glacpe.AbstractTest;
import org.inra.ecoinfo.glacpe.dataset.phytoplancton.impl.PhytoplanctonParameters;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO;
import org.inra.ecoinfo.glacpe.extraction.vo.VariableVO;
import org.inra.ecoinfo.glacpe.refdata.taxon.Taxon;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.NotificationException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;

public class PhytoTest extends AbstractTest{

	public PhytoTest() {
		super();
	}
	
	@Test
	@Transactional
	public void phyto1() throws BusinessException, IOException, PersistenceException, NotificationException{
		test(getMethodeName());
	}

	@Override
	protected IParameter getParameter(org.inra.ecoinfo.utils.configuration.Test test) throws PersistenceException {
		Map<String, Object> metadatasMap = new HashMap<String, Object>();
		metadatasMap.put(PlateformeVO.class.getSimpleName(), getPlateformesTests(test));
		metadatasMap.put(VariableVO.class.getSimpleName(), getVariablesTests(test));
		metadatasMap.put(Taxon.class.getSimpleName(), getTaxonTests(test));
		metadatasMap.put("sommation", true);
		metadatasMap.put("propriete", false);
		metadatasMap.put("aggregated", true);
		metadatasMap.put(DatesRequestParamVO.class.getSimpleName(), getDatesRequestParam(test));
		metadatasMap.put(IExtractionManager.KEYMAP_COMMENTS, test.getExtract().getComment());
		IParameter parameters = new PhytoplanctonParameters(metadatasMap);
		parameters.setCommentaire(test.getExtract().getComment());
		return parameters;
	}
}
