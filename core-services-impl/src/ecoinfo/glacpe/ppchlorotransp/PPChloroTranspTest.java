package org.inra.ecoinfo.glacpe.ppchlorotransp;

import static org.inra.ecoinfo.glacpe.LacsTransactionalTestExecutionListener.getApplicationContext;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.glacpe.AbstractTest;
import org.inra.ecoinfo.glacpe.dataset.chloro.IChlorophylleDAO;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.IConditionGeneraleDAO;
import org.inra.ecoinfo.glacpe.dataset.productionprimaire.IPPDAO;
import org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.impl.PPChloroTranspParameter;
import org.inra.ecoinfo.glacpe.extraction.vo.DatasRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.DepthRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO;
import org.inra.ecoinfo.glacpe.extraction.vo.VariableVO;
import org.inra.ecoinfo.glacpe.refdata.projetsite.ProjetSite;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.configuration.VariableTest;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.NotificationException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;

import com.Ostermiller.util.CSVParser;

public class PPChloroTranspTest  extends AbstractTest{
	IPPDAO ppDAO;
	IChlorophylleDAO chloroDAO;
	IConditionGeneraleDAO conditionGeneraleDAO;

	public PPChloroTranspTest() {
		super();
		
	}
	private static final String PATH_TO_MESURE = "%s/%s/%s";

	@Test
	@Transactional
	public void ppchlorotransp1() throws BusinessException, IOException, PersistenceException, NotificationException{
		test(getMethodeName());
	}

	@SuppressWarnings("unchecked")
	@Override
	protected IParameter getParameter(org.inra.ecoinfo.utils.configuration.Test test) throws PersistenceException {
		if(ppDAO==null) ppDAO=(IPPDAO) getApplicationContext().getBean("productionPrimaireDAO");
		if(chloroDAO==null) chloroDAO=(IChlorophylleDAO) getApplicationContext().getBean("chlorophylleDAO");
		if(conditionGeneraleDAO==null) conditionGeneraleDAO=(IConditionGeneraleDAO) getApplicationContext().getBean("conditionGeneraleDAO");
		Map<String, Object> metadatasMap = new HashMap<String, Object>();
		metadatasMap.put(PlateformeVO.class.getSimpleName(), getPlateformesTests(test));
		List<Long> projectSiteIds = new LinkedList<Long>();
		for (PlateformeVO plateformeVO : ((List<PlateformeVO>)metadatasMap.get(PlateformeVO.class.getSimpleName()))) {
			projectSiteIds.add(plateformeVO.getProjet().getId());
		}
		metadatasMap.put(ProjetSite.class.getSimpleName(), projectSiteIds);
		metadatasMap.put(VariableVO.class.getSimpleName(), getMapVariablesTests(test));
		metadatasMap.put(DatasRequestParamVO.class.getSimpleName(), getDatasRequestParam(test));
		metadatasMap.put(DepthRequestParamVO.class.getSimpleName(), getDepthRequestParamVO(test));
		metadatasMap.put(DatesRequestParamVO.class.getSimpleName(), getDatesRequestParam(test));
		metadatasMap.put(IExtractionManager.KEYMAP_COMMENTS, test.getExtract().getComment());
		IParameter parameters = new PPChloroTranspParameter(metadatasMap);
		parameters.setCommentaire(test.getExtract().getComment());
		return parameters;
	}
	
	protected void checkExtractData(Map<String, Map<String, Map<String, Map<String, Map<String, Float>>>>> buildMap, ZipEntry entry, ZipFile zipFile, org.inra.ecoinfo.utils.configuration.Test test)
			throws IOException {
		log(test.getName(), String.format("vérification du fichier brut : %s", entry.getName()));
		String fileEncoding = Utils.detectStreamEncoding(new BufferedInputStream(zipFile.getInputStream(entry)));
		CSVParser parser = new CSVParser(new InputStreamReader(zipFile.getInputStream(entry), fileEncoding), SEPARATOR);
		String[] values = null;
		int lineNumber = 0;
		String[] enTetes = null;
		while ((values = parser.getLine()) != null) {
			TokenizerValues tokenizerValues = new TokenizerValues(values);
			if (lineNumber++ == 0) {
				enTetes = new String[values.length];
				for (int i = 0; i < values.length; i++) {
					enTetes[i] = Utils.createCodeFromString(tokenizerValues.nextToken());
				}
			} else {
				String projectCode = Utils.createCodeFromString(tokenizerValues.nextToken());
				String siteCode = Utils.createCodeFromString(tokenizerValues.nextToken());
				String plateformeCode = Utils.createCodeFromString(tokenizerValues.nextToken());
				String date = Utils.createCodeFromString(tokenizerValues.nextToken());
				String profondeur = tokenizerValues.nextTokenFloat().toString();
				String pathToMesure = String.format(PATH_TO_MESURE, projectCode, siteCode, plateformeCode);
				for (int i = 5; i < values.length; i++) {
					String variableCode = enTetes[i];
					Float expectedValue = null;
					try {
						expectedValue = buildMap.get(pathToMesure).get(DATES).get(date).get(profondeur).get(variableCode);
					} catch (NullPointerException e) {
						fail("la valeur n'est pas dans le fichier d'entrée");
					}
					Float value = tokenizerValues.nextTokenFloat();
					assertEquals(String.format("Dans le fichier de données brutes ligne %d colonne %d la valeur attendue est incorrecte", lineNumber, i), expectedValue, value);
				}
			}
		}
		log(test.getName(), String.format("vérification du fichier brut : %s OK", entry.getName()));
	}

	protected void checkExtractAggregatedData(Map<String, Map<String, Map<String, Map<String, Map<String, Float>>>>> buildMap, ZipFile zipFile, ZipEntry entry, org.inra.ecoinfo.utils.configuration.Test test)
			throws IOException {
		log(test.getName(), String.format("vérification du fichier de données aggrégées : %s", entry.getName()));
		String fileEncoding = Utils.detectStreamEncoding(new BufferedInputStream(zipFile.getInputStream(entry)));
		CSVParser parser = new CSVParser(new InputStreamReader(zipFile.getInputStream(entry), fileEncoding), SEPARATOR);
		String[] values = null;
		int lineNumber = 0;
		String[] variables = null;
		String[] types = null;
		while ((values = parser.getLine()) != null) {
			TokenizerValues tokenizerValues = new TokenizerValues(values);
			if (lineNumber++ == 0) {
				variables = new String[values.length];
				types = new String[values.length];
				for (int i = 0; i < values.length; i++) {
					String value = Utils.createCodeFromString(tokenizerValues.nextToken());
					String[] split = value.split("[()]");
					if (split.length <= 1) {
						variables[i] = value;
						continue;
					}
					variables[i] = split[1];
					types[i] = split[0];
				}
			} else {
				String projectCode = Utils.createCodeFromString(tokenizerValues.nextToken());
				String siteCode = Utils.createCodeFromString(tokenizerValues.nextToken());
				String plateformeCode = Utils.createCodeFromString(tokenizerValues.nextToken());
				String date = Utils.createCodeFromString(tokenizerValues.nextToken());
				/* String profondeurMin = */tokenizerValues.nextToken();
				/* String profondeurMax = */tokenizerValues.nextToken();
				String profondeurString = tokenizerValues.nextToken();
				for (int i = 7; i < values.length; i++) {
					if (i >= variables.length) break;
					Float value = null;
					try {
						value = tokenizerValues.nextTokenFloat();
					} catch (ArrayIndexOutOfBoundsException e) {
						continue;
					}
					if (value == null) {
						continue;
					}
					String variableCode = variables[i];
					String type = types[i];
					Float expectedValue = null;
					try {				
						String pathToMesure = String.format(PATH_TO_MESURE, projectCode, siteCode, plateformeCode);
						if (MIN.equals(type)) {
							expectedValue = profondeurString == null ? null : buildMap.get(pathToMesure).get(DATES).get(date).get(MIN).get(variableCode);
						} else if (MAX.equals(type)) {
							expectedValue = profondeurString == null ? null : buildMap.get(pathToMesure).get(DATES).get(date).get(MAX).get(variableCode);
						} else if (AVERAGE.equals(type)) {
							expectedValue = buildMap.get(pathToMesure).get(DATES).get(date).get(AVERAGE).get(variableCode);
							continue;// en attendant les specif sur la moyenne
						}
					} catch (NullPointerException e) {
						fail("la valeur n'est pas dans le fichier d'entrée");
					}
					assertEquals(String.format("Dans le fichier de données brutes ligne %d colonne %d la valeur attendue est incorrecte pour %s : %s : %s", lineNumber, i, type, variableCode, date),
							expectedValue, value);
				}
			}
		}
		log(test.getName(), String.format("vérification du fichier brut : %s OK", entry.getName()));
	} 
	
	protected Map<String, List<VariableVO>> getMapVariablesTests(org.inra.ecoinfo.utils.configuration.Test test) throws PersistenceException {
		Map<String, List<VariableVO>> mapVariablesSelected = new HashMap<String, List<VariableVO>>();
		DatesRequestParamVO dates =  getDatesRequestParam(test);
		List<PlateformeVO> plateformes = getPlateformesTests(test);
		List<Long> plateformesIds = new LinkedList<Long>();
		Date firstDate = null;
		Date lastDate = null;
		
		try{
		for(Date mapDate : dates.getCurrentDatesFormParam().retrieveParametersMap().values())
		{
			if(firstDate == null)
			{
				firstDate = mapDate;
			}
			else if(firstDate.after(mapDate))
			{
					firstDate = mapDate;
			}
		}
		
		for(Date mapDate : dates.getCurrentDatesFormParam().retrieveParametersMap().values())
		{
			if(lastDate == null)
			{
				lastDate = mapDate;
			}
			else if(lastDate.before(mapDate))
			{
				lastDate = mapDate;
			}
		}
		}
		catch(ParseException e)
		{
			throw new PersistenceException(e);
		}
		
		for(PlateformeVO plateforme : plateformes)
		{
			plateformesIds.add(plateforme.getId());
		}
		
		List<VariableGLACPE> ppVariables = ppDAO.retrieveAvailablesVariables(plateformesIds, firstDate, lastDate);
		List<VariableGLACPE> chloroVariables = chloroDAO.retrieveAvailablesVariables(plateformesIds, firstDate, lastDate);
		List<VariableGLACPE> transpVariables = conditionGeneraleDAO.retrieveAvailablesTransparenceVariables(plateformesIds, firstDate, lastDate);
		if (variablesVO == null) {
			variablesVO = new LinkedList<VariableVO>();
			for (VariableTest variableTest : test.getExtract().getVariablesTests()) {
				boolean done=false;
				for (VariableGLACPE ppVariable : ppVariables) {
					if(ppVariable.getCode().equals(variableTest.getCode())){
						if(!mapVariablesSelected.containsKey(PPChloroTranspParameter.PRODUCTION_PRIMAIRE))mapVariablesSelected.put(PPChloroTranspParameter.PRODUCTION_PRIMAIRE, new LinkedList<VariableVO>());
						mapVariablesSelected.get(PPChloroTranspParameter.PRODUCTION_PRIMAIRE).add(new VariableVO(ppVariable));
						done=true;
						break;
					}
				}
				if(done)continue;
				for (VariableGLACPE chloroVariable : chloroVariables) {
					if(chloroVariable.getCode().equals(variableTest.getCode())){
						if(!mapVariablesSelected.containsKey(PPChloroTranspParameter.CHLOROPHYLLE))mapVariablesSelected.put(PPChloroTranspParameter.CHLOROPHYLLE, new LinkedList<VariableVO>());
						mapVariablesSelected.get(PPChloroTranspParameter.CHLOROPHYLLE).add(new VariableVO(chloroVariable));
						done=true;
						break;
					}
				}
				if(done)continue;
				for (VariableGLACPE transpVariable : transpVariables) {
					if(transpVariable.getCode().equals(variableTest.getCode())){
						if(!mapVariablesSelected.containsKey(PPChloroTranspParameter.TRANSPARENCE))mapVariablesSelected.put(PPChloroTranspParameter.TRANSPARENCE, new LinkedList<VariableVO>());
						mapVariablesSelected.get(PPChloroTranspParameter.TRANSPARENCE).add(new VariableVO(transpVariable));
						done=true;
						break;
					}
				}
			}
		}
		return mapVariablesSelected;
	}
	
}
