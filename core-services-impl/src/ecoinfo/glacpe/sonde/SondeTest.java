package org.inra.ecoinfo.glacpe.sonde;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.glacpe.AbstractTest;
import org.inra.ecoinfo.glacpe.dataset.sondemulti.impl.SondeMultiParameters;
import org.inra.ecoinfo.glacpe.extraction.vo.DatasRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.DepthRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO;
import org.inra.ecoinfo.glacpe.extraction.vo.VariableVO;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.NotificationException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;

public class SondeTest extends AbstractTest{

	public SondeTest() {
		super();
	}

	@Test
	@Transactional
	public void sonde1() throws BusinessException, IOException, PersistenceException, NotificationException{
		test(getMethodeName());
	}

	@Override
	protected IParameter getParameter(org.inra.ecoinfo.utils.configuration.Test test) throws PersistenceException {
		Map<String, Object> metadatasMap = new HashMap<String, Object>();

		metadatasMap.put(PlateformeVO.class.getSimpleName(), getPlateformesTests(test));
		metadatasMap.put(VariableVO.class.getSimpleName(), getVariablesTests(test));
		metadatasMap.put(DatasRequestParamVO.class.getSimpleName(), getDatasRequestParam(test));
		metadatasMap.put(DepthRequestParamVO.class.getSimpleName(), getDepthRequestParamVO(test));

		metadatasMap.put(DatesRequestParamVO.class.getSimpleName(), getDatesRequestParam(test));
		metadatasMap.put(IExtractionManager.KEYMAP_COMMENTS, test.getExtract().getComment());
		IParameter parameters = new SondeMultiParameters(metadatasMap);
		return parameters;
	}
}
