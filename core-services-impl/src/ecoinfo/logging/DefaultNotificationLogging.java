package org.inra.ecoinfo.logging;

import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.inra.ecoinfo.glacpe.AbstractTest;
import org.inra.ecoinfo.notifications.entity.Notification;
import org.inra.ecoinfo.security.ISecurityContext;
import org.inra.ecoinfo.utils.LoggerObject;
import org.springframework.beans.factory.annotation.Autowired;

public class DefaultNotificationLogging{
	@Autowired
	ISecurityContext securityContext;

	public void logAddNotificationArround(ProceedingJoinPoint joinPoint) {
		Notification notification =(Notification) (joinPoint.getArgs()[0]);
		notification.setUtilisateur(securityContext.getUtilisateur());
		Logger logger = Logger.getLogger("tests");
		logger.info(new LoggerObject(notification));
		AbstractTest.addNotification(notification);
		return;
	}
}