package org.inra.ecoinfo.utils;

import static junit.framework.Assert.assertEquals;

import java.text.ParseException;

import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.glacpe.AbstractTest;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.junit.Test;


public class IntervalDateTest extends AbstractTest{
	private static final String PROPERTY_MSG_BAD_DATES_FOR_PERIOD = "Une date de d\u00e9but est ant\u00e9rieure \u00e0 une date de fin";
	@Test()
	public void getIntervalDateddMMyyyy() throws BadExpectedValueException, ParseException{
		String beginDateddMMyyyy="12-01-2011";
		String endDateddMMyyyy="12-05-2015";
		IntervalDate interval = IntervalDate.getIntervalDateddMMyyyy(beginDateddMMyyyy, endDateddMMyyyy);
		assertEquals("la date de début est mauvaise",beginDateddMMyyyy, interval.getBeginDateToString());
		assertEquals("la date de fin est mauvaise",endDateddMMyyyy, interval.getEndDateToString());
	}

	@Test(expected=BadExpectedValueException.class)
	public void getIntervalDateddMMyyyyWithBadPattern() throws BadExpectedValueException, ParseException{
		String beginDateddMMyyyy="01-2011";
		String endDateddMMyyyy="12-05-2005";
		IntervalDate.getIntervalDateddMMyyyy(beginDateddMMyyyy, endDateddMMyyyy);
	}
	
	@Test()
	public void getIntervalDateddMMyyyyDateInversees() throws ParseException{
		String beginDateddMMyyyy="12-01-2015";
		String endDateddMMyyyy="12-05-2012";
		try {
			IntervalDate.getIntervalDateddMMyyyy(beginDateddMMyyyy, endDateddMMyyyy);
		} catch (BadExpectedValueException e) {
			assertEquals("pas de message pour date inversées", PROPERTY_MSG_BAD_DATES_FOR_PERIOD, e.getMessage());
		} 
	}

    @Override
    protected IParameter getParameter(org.inra.ecoinfo.utils.configuration.Test test) {
        // TODO Auto-generated method stub
        return null;
    }
}