package org.inra.ecoinfo.utils;

import org.apache.log4j.Logger;
import org.inra.ecoinfo.glacpe.AbstractTest;
import org.inra.ecoinfo.notifications.IMessageProducer;
import org.inra.ecoinfo.notifications.INotificationDAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.richfaces.application.push.MessageException;

public class RichFacesMessageProducerTest implements IMessageProducer{
	Logger logger = Logger.getLogger(RichFacesMessageProducerTest.class);
	private INotificationDAO notificationDAO;
	@Override
	public void push(String login, Long id) throws MessageException {
		logger.info(String.format("message %d pour %s", id, login));
		try {
			AbstractTest.addNotification(notificationDAO.getById(id));
		} catch (PersistenceException e) {
			e.printStackTrace();
		}
	}
	public void setNotificationDAO(INotificationDAO notificationDAO) {
		this.notificationDAO = notificationDAO;
	}
}
