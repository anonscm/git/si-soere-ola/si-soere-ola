package org.inra.ecoinfo.utils.configuration;

public class AbstractData {

	private int	order;
	private String	code;
	private FileTest	inFile;
	private String	inFileCode;
	private FileTest	outFile;
	private String	outFileCode;

	public AbstractData() {
		super();
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public FileTest getInFile() {
		return inFile;
	}

	public void setInFile(FileTest inFile) {
		this.inFile = inFile;
	}

	public String getInFileCode() {
		return inFileCode;
	}

	public void setInFileCode(String inFileCode) {
		this.inFileCode = inFileCode;
	}

	public FileTest getOutFile() {
		return outFile;
	}

	public void setOutFile(FileTest outFile) {
		this.outFile = outFile;
	}

	public String getOutFileCode() {
		return outFileCode;
	}

	public void setOutFileCode(String outFileCode) {
		this.outFileCode = outFileCode;
	}
}