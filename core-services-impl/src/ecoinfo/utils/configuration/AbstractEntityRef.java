package org.inra.ecoinfo.utils.configuration;

public class AbstractEntityRef {

	protected String	code;

	public AbstractEntityRef() {
		super();
	}

	public AbstractEntityRef(String code) {
		super();
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public String toString() {
		return code;
	}
}