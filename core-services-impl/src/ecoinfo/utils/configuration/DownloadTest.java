package org.inra.ecoinfo.utils.configuration;


public class DownloadTest {
	private Long order;
	private String outFileRef;
	private FileTest outFile;
	
	public Long getOrder() {
		return order;
	}
	
	public String getOutFileRef() {
		return outFileRef;
	}
	
	public FileTest getOutFile() {
		return outFile;
	}
	
	public void setOrder(Long order) {
		this.order = order;
	}
	
	public void setOutFileRef(String outFileRef) {
		this.outFileRef = outFileRef;
	}
	
	public void setOutFile(FileTest outFile) {
		this.outFile = outFile;
	}
	
}
