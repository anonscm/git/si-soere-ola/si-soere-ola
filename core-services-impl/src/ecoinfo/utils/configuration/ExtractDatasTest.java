package org.inra.ecoinfo.utils.configuration;

import java.util.LinkedList;
import java.util.List;

import org.inra.ecoinfo.glacpe.extraction.vo.DepthRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.ExtractionParametersVO;
import org.inra.ecoinfo.localization.ILocalizationManager;


public class ExtractDatasTest {
	private ILocalizationManager localizationManager;
	private String name;
	private boolean rawDatas;
	private boolean datasBalancedByDepht;
	private boolean minValueAndAssociatedDepht;
	private boolean maxValueAndAssociatedDepht;
	private DepthRequestParamVO depthRequestParamVO = new DepthRequestParamVO();
	private List<ExtractionParametersVO> extractionParameters = new LinkedList<ExtractionParametersVO>();
	private List<String> extractionParametersRef = new LinkedList<String>();
	
	public String getName() {
		return name;
	}
	
	public boolean getRawDatas() {
		return rawDatas;
	}
	
	public boolean getDatasBalancedByDepht() {
		return datasBalancedByDepht;
	}
	
	public boolean getMinValueAndAssociatedDepht() {
		return minValueAndAssociatedDepht;
	}
	
	public boolean getMaxValueAndAssociatedDepht() {
		return maxValueAndAssociatedDepht;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setRawDatas(boolean rawDatas) {
		this.rawDatas = rawDatas;
	}
	
	public void setDatasBalancedByDepht(boolean datasBalancedByDepht) {
		this.datasBalancedByDepht = datasBalancedByDepht;
	}
	
	public void setMinValueAndAssociatedDepht(boolean minValueAndAssociatedDepht) {
		this.minValueAndAssociatedDepht = minValueAndAssociatedDepht;
	}
	
	public void setMaxValueAndAssociatedDepht(boolean maxValueAndAssociatedDepht) {
		this.maxValueAndAssociatedDepht = maxValueAndAssociatedDepht;
	}

	public void setDepthMin(Float depthMin) {
		depthRequestParamVO.setDepthMin(depthMin);
	}

	public DepthRequestParamVO getDepthRequestParamVO() {
		return depthRequestParamVO;
	}

	
	public List<ExtractionParametersVO> getExtractionParameters() {
		return extractionParameters;
	}

	
	public void addExtractionParameter(ExtractionParametersVO extractionParameter) {
		if(extractionParameter==null)return;
		this.datasBalancedByDepht=true;
		this.extractionParameters.add(extractionParameter);
	}

	public void setDepthMax(Float depthMax) {
		depthRequestParamVO.setAllDepth(false);
		depthRequestParamVO.setDepthMax(depthMax);
	}

	
	public List<String> getExtractionParametersRef() {
		return extractionParametersRef;
	}

	
	public void addExtractionParameterRef(String extractionParameterRef) {
		this.extractionParametersRef.add(extractionParameterRef);
	}

	public ILocalizationManager getLocalizationManager() {
		return localizationManager;
	}

	public void setLocalizationManager(ILocalizationManager localizationManager) {
		this.localizationManager = localizationManager;
		depthRequestParamVO.setLocalizationManager(localizationManager);
	}
}
