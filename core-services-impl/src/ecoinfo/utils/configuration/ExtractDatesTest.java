package org.inra.ecoinfo.utils.configuration;

import java.text.ParseException;

import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;


public class ExtractDatesTest {
	private IntervalDate periode;
	private String name;
	
	public IntervalDate getPeriode() {
		return periode;
	}
	
	public String getName() {
		return name;
	}
	
	public void setPeriode(IntervalDateTest periode) throws BadExpectedValueException, ParseException {
		this.periode = IntervalDate.getIntervalDateyyyyMMdd(periode.getBeginDate(), periode.getEndDate());
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
}
