package org.inra.ecoinfo.utils.configuration;

import java.util.LinkedList;
import java.util.List;

import org.inra.ecoinfo.glacpe.extraction.vo.AbstractDatesFormParam;
import org.inra.ecoinfo.localization.ILocalizationManager;


public class ExtractTest {
	private List<PlateformeTest> plateformesTests = new LinkedList<PlateformeTest>();
	private String variablesRef;
	private List<VariableTest> variablesTest = new LinkedList<VariableTest>();
	private String taxonsRef;
	private List<TaxonTest> taxonsTest = new LinkedList<TaxonTest>();
	private ExtractDatasTest extractDatasTest;
	private String extractDatasRef;
	private AbstractDatesFormParam extractDatesTest;
	private String extractDatesRef;
	private String comment;
	private String projectCode;
	private int affichage = 1;
	private ILocalizationManager localizationManager;
	
	public List<PlateformeTest> getPlateformesTests() {
		return plateformesTests;
	}
	
	public List<VariableTest> getVariablesTests() {
		return variablesTest;
	}
	
	public ExtractDatasTest getExtractDatasTest() {
		return extractDatasTest;
	}
	
	public AbstractDatesFormParam getExtractDatesTest() {
		return extractDatesTest;
	}
	
	public void setPlateformesTests(List<PlateformeTest> plateformetests) {
		this.plateformesTests = plateformetests;
	}
	
	public void setVariablesTests(List<VariableTest> variabletests) {
		this.variablesTest = variabletests;
	}
	
	public void setExtractDatasTest(ExtractDatasTest extractDatasTest) {
		extractDatasTest.setLocalizationManager(localizationManager);
		this.extractDatasTest = extractDatasTest;
	}
	
	public void setExtractDatesTest(AbstractDatesFormParam extractDatesTest) {
		this.extractDatesTest = extractDatesTest;
	}
	
	public void addPlateforme(PlateformeTest plateforme){
		this.plateformesTests.add(plateforme);
	}
	
	public void setVariablesRef(RefTest ref){
		this.variablesRef=ref.getRef();
	}
	
	public String getVariablesRef(){
		return variablesRef;
	}
	
	public String getExtractDatasRef() {
		return extractDatasRef;
	}
	
	public String getExtractDatesRef() {
		return extractDatesRef;
	}
	
	public void setExtractDatasRef(RefTest ref) {
		this.extractDatasRef = ref.getRef();
	}
	
	public void setExtractDatesRef(RefTest ref) {
		this.extractDatesRef = ref.getRef();
	}

	public void setVariables(List<RefTest> variables){
		variablesTest.clear();
		for (RefTest refTest : variables) {
			variablesTest.add(new VariableTest(refTest.getRef()));
		}
	}

	
	public String getComment() {
		return comment;
	}
	
	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getProjectCode() {
		return projectCode;
	}

	public void setProjectCode(String project) {
		this.projectCode = project;
	}

	public ILocalizationManager getLocalizationManager() {
		return localizationManager;
	}

	public void setLocalizationManager(ILocalizationManager localizationManager) {
		this.localizationManager = localizationManager;
	}

	public List<TaxonTest> getTaxonsTest() {
		return taxonsTest;
	}
	
	public void setTaxonsTest(List<TaxonTest> taxonsTest) {
		this.taxonsTest = taxonsTest;
	}

	public void setTaxons(List<RefTest> taxonTest) {
		taxonsTest.clear();
		for(RefTest ref : taxonTest)
		{
			taxonsTest.add(new TaxonTest(ref.getRef()));
		}
	}
	
	public String getTaxonsRef() {
		return taxonsRef;
	}

	public void setTaxonsRef(RefTest ref) {
		this.taxonsRef = ref.getRef();
	}

    public int getAffichage() {
        return affichage;
    }

    public void setAffichage(int affichage) {
        this.affichage = affichage;
    }
    
    public void setAffichage(String affichage) {
        this.affichage = Integer.parseInt(affichage);
    }
}
