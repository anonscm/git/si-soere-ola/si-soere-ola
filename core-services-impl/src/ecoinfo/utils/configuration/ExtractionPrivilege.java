package org.inra.ecoinfo.utils.configuration;

import java.util.Date;
	public class ExtractionPrivilege{
		private Date beginDate;
		private Date EndDate;
		private boolean extraction;
		public ExtractionPrivilege() {
			super();
		}
		
		public Date getBeginDate() {
			return beginDate;
		}
		
		public Date getEndDate() {
			return EndDate;
		}
		
		public boolean isExtraction() {
			return extraction;
		}
		
		public void setBeginDate(Date beginDate) {
			this.beginDate = beginDate;
		}
		
		public void setEndDate(Date endDate) {
			EndDate = endDate;
		}
		
		public void setExtraction(boolean extraction) {
			this.extraction = extraction;
		}
	}