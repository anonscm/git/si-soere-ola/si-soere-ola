package org.inra.ecoinfo.utils.configuration;


public class FileTest {
	private String path;
	private String name;
	public FileTest() {
		super();
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFileName(){
		if(path==null)return "";
		String[] nodes = path.split("/");
		return nodes[nodes.length-1];
	}
	
}
