package org.inra.ecoinfo.utils.configuration;



public class IntervalDateTest{
	private String beginDate;
	private String EndDate;
	
	public String getBeginDate() {
		return beginDate;
	}
	
	public String getEndDate() {
		return EndDate;
	}
	
	public void setBeginDate(String beginDate) {
		this.beginDate = beginDate;
	}
	
	public void setEndDate(String endDate) {
		EndDate = endDate;
	}
}