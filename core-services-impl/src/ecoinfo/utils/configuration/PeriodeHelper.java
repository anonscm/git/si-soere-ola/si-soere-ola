package org.inra.ecoinfo.utils.configuration;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class PeriodeHelper {
	private List<Map<String, String>> period = new LinkedList<Map<String,String>>();
	private List<Map<String, String>> periodYear = new LinkedList<Map<String,String>>();
	private List<Map<String, String>> datesPeriode = new LinkedList<Map<String,String>>();
	private String years;
	public void addPeriod(Map<String, String> period){
		this.period.add(period);
	}
	public void addPeriodYear(Map<String, String> periodYear){
		this.periodYear.add(periodYear);
	}
	public void addDatesPeriode(Map<String, String> datesPeriode){
		this.datesPeriode.add(datesPeriode);
	}
	public List<Map<String, String>> getPeriod() {
		return period;
	}
	public List<Map<String, String>> getPeriodYear() {
		return periodYear;
	}
	public List<Map<String, String>> getDatesPeriode() {
		return datesPeriode;
	}
	public String getYears() {
		return years;
	}
	public void setYears(String years) {
		this.years = years;
	}
}
