package org.inra.ecoinfo.utils.configuration;

import java.sql.Date;

public class PrivilegeTest {
	private String code;
	private String privilegePath;
	private boolean administration = false;
	private boolean depot = false;
	private boolean publication = false;
	private boolean suppression = false;
	private boolean synthese = false;
	private ExtractionPrivilege extractionPrivilege;
	private Date beginDate;
	private Date endDate;
	
	public String getCode() {
		return code;
	}
	
	public String getPrivilegePath() {
		return privilegePath;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	public void setPrivilegePath(String privilegePath) {
		this.privilegePath = privilegePath;
	}

	
	public boolean isAdministration() {
		return administration;
	}

	
	public boolean isDepot() {
		return depot;
	}

	
	public boolean isPublication() {
		return publication;
	}

	
	public boolean isSuppression() {
		return suppression;
	}

	
	public boolean isSynthese() {
		return synthese;
	}

	
	public void setAdministration(boolean administration) {
		this.administration = administration;
	}

	
	public void setDepot(boolean depot) {
		this.depot = depot;
	}

	
	public void setPublication(boolean publication) {
		this.publication = publication;
	}

	
	public void setSuppression(boolean suppression) {
		this.suppression = suppression;
	}

	
	public void setSynthese(boolean synthese) {
		this.synthese = synthese;
	}

	
	public Date getBeginDate() {
		return beginDate;
	}

	
	public Date getEndDate() {
		return endDate;
	}

	
	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}

	
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	public void addExtraction(ExtractionPrivilege extraction){
		this.extractionPrivilege = extraction;
	}

	
	public ExtractionPrivilege getExtractionPrivilege() {
		return extractionPrivilege;
	}

	
	public void setExtractionPrivilege(ExtractionPrivilege extractionPrivilege) {
		this.extractionPrivilege = extractionPrivilege;
	}
	
}
