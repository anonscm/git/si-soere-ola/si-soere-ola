package org.inra.ecoinfo.utils.configuration;

import java.io.File;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.localization.ILocalizationManager;

public class Test {
	private ILocalizationManager localizationManager;
	private String name;
	private Utilisateur utilisateur;
	private String utilisateurLogin;
	private List<PrivilegeTest> privilegesTest = new LinkedList<PrivilegeTest>();
	private String privilegeCode;
	private String extractType;
	private Date beginDate;
	private Date endDate;
	private List<UploadTest> uploads = new LinkedList<UploadTest>();
	private ExtractTest extract;
	private Map<Long, DownloadTest> downloads = new TreeMap<Long, DownloadTest>();
	private String outFileCode;
	private List<StringBuffer>	privilegesString;
	private File	inFile;
	
	public String getName() {
		return name;
	}
	
	public Utilisateur getUtilisateur() {
		return utilisateur;
	}
	
	public List<PrivilegeTest> getPrivilegesTest() {
		return privilegesTest;
	}
	
	public Date getBeginDate() {
		return beginDate;
	}
	
	public Date getEndDate() {
		return endDate;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}
	
	public void setPrivilegesTest(List<PrivilegeTest> privilegesTest) {
		this.privilegesTest = privilegesTest;
	}
	
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	
	public String getOutFileCode() {
		return outFileCode;
	}

	
	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}

	
	public void setOutFileCode(String outFileCode) {
		this.outFileCode = outFileCode;
	}

	
	public String getUtilisateurLogin() {
		return utilisateurLogin;
	}

	
	public String getPrivilegeCode() {
		return privilegeCode;
	}

	
	public String getExtractType() {
		return extractType;
	}

	
	public void setUtilisateurLogin(String utilisateurLogin) {
		this.utilisateurLogin = utilisateurLogin;
	}

	
	public void setPrivilegeCode(String privilegeCode) {
		this.privilegeCode = privilegeCode;
	}
	
	
	public List<UploadTest> getUploads() {
		return uploads;
	}

	
	public Map<Long, DownloadTest> getDownloads() {
		return downloads;
	}
	
	public void addUpload(UploadTest uploadTest){
		uploads.add(uploadTest);
	}

	
	public void addDownload(DownloadTest download) {
		this.downloads.put(download.getOrder(), download);
	}

	public void setExtractType(String extractType) {
		this.extractType = extractType;
	}
	
	public ExtractTest getExtract() {
		return extract;
	}

	
	public void setExtract(ExtractTest extract) {
		extract.setLocalizationManager(localizationManager);
		this.extract = extract;
	}
	
	public void addPrivileges(List<StringBuffer> privileges){
		this.privilegesString = privileges;
	}

	public void updatePrivileges(Map<String, PrivilegeTest> privileges) {
		for (StringBuffer ref : privilegesString) {
			this.privilegesTest.add(privileges.get(ref.toString()));
		}
	}

	public File getInFile() {
		return inFile;
	}

	public ILocalizationManager getLocalizationManager() {
		return localizationManager;
	}

	public void setLocalizationManager(ILocalizationManager localizationManager) {
		this.localizationManager = localizationManager;
	}
}
