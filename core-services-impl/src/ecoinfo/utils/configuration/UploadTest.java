package org.inra.ecoinfo.utils.configuration;

import java.io.File;


public class UploadTest {
	private FileTest inFileTest;
	private String inFileRef;
	private String projetCode;
	private String siteCode;
	private String themeCode;
	private String datatypeCode;
	
	public void setInFileTest(FileTest inFile) {
		this.inFileTest = inFile;
	}

	public File getInFile(){
		return new File(inFileTest.getPath());
	}
	
	public String getSiteCode() {
		return siteCode;
	}
	
	public String getThemeCode() {
		return themeCode;
	}
	
	public String getDatatypeCode() {
		return datatypeCode;
	}
	
	public void setInFile(FileTest inFile) {
		this.inFileTest = inFile;
	}
	
	public void setSiteCode(String siteCode) {
		this.siteCode = siteCode;
	}
	
	public void setThemeCode(String themeCode) {
		this.themeCode = themeCode;
	}
	
	public void setDatatypeCode(String datatypeCode) {
		this.datatypeCode = datatypeCode;
	}
	
	public String getInFileRef() {
		return inFileRef;
	}

	
	public void setInFileRef(String inFileRef) {
		this.inFileRef = inFileRef;
	}

	public String getInfileName() {
		return getInFile().getName();
	}
	
	public String getProjetCode() {
		return projetCode;
	}

	public void setProjetCode(String projetCode) {
		this.projetCode = projetCode;
	}
}
