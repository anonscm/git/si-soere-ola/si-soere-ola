package org.inra.ecoinfo.utils.exceptions;

import org.inra.ecoinfo.notifications.entity.Notification;

public class NotificationException extends Exception {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;
	private Notification	notification;

	public NotificationException() {
		super();
	}

	public NotificationException(String message) {
		super(message);
	}

	public NotificationException(Throwable cause) {
		super(cause);
	}

	public NotificationException(String message, Throwable cause) {
		super(message, cause);
	}

	public NotificationException(Notification notification) {
		super(notification.getMessage());
		this.notification = notification;
	}

	public Notification getNotification() {
		return notification;
	}
}