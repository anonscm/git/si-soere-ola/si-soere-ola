package org.inra.ecoinfo.glacpe.dataset;

import java.io.IOException;
import java.io.Serializable;

import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.xml.sax.SAXException;

import com.Ostermiller.util.CSVParser;

/**
 *
 * @author ptcherniati
 */
public interface IProcessRecord extends Serializable {

    /**
     * <p>
     * Process record of the file.
     * 
     * @param parser
     *            the parser
     * @param versionFile
     * @param datasetDescriptor
     * @link(VersionFile) the version file
     * @param sessionProperties
     * @link(ISessionPropertiesGLACPE) the session properties
     * @param fileEncoding
     *            the file encoding
     * @param datasetDescriptorGLACPE
     * @link(DatasetDescriptorGLACPE) the dataset descriptor acbb
     * @throws BusinessException
     *             the business exception {@link VersionFile} the version file {@link ISessionPropertiesGLACPE} the session properties {@link DatasetDescriptorGLACPE} the {@link DatasetDescriptorGLACPE}
     */
    void processRecord(CSVParser parser, VersionFile versionFile, String fileEncoding, DatasetDescriptor datasetDescriptor) throws BusinessException;

    /**
     *
     * @return
     * @throws IOException
     * @throws SAXException
     */
    DatasetDescriptor holdDatasetDescriptor() throws IOException, SAXException;

    /** The Constant DATASET_DESCRIPTOR_XML. */
    static final String DATASET_DESCRIPTOR_XML = "dataset-descriptor.xml";

}
