package org.inra.ecoinfo.glacpe.dataset;

import java.io.Serializable;

import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

import com.Ostermiller.util.CSVParser;

/**
 *
 * @author ptcherniati
 */
public interface ITestHeaders extends Serializable {

    /**
     * Test headers.
     * 
     * @param parser
     *            the parser
     * @param versionFile
     * @link(VersionFile)
     * @link(ISessionPropertiesGLACPE) the session properties
     * @param encoding
     *            the encoding
     * @param badsFormatsReport
     * @link(BadsFormatsReport) the bads formats report
     * @param datasetDescriptor
     * @link(DatasetDescriptorACBB) the dataset descriptor
     * @return the last line number of the header
     * @throws BusinessException
     *             test the frame header of a file {@link VersionFile} the version file {@link ISessionPropertiesGLACPE} the session properties {@link BadsFormatsReport} the bads formats report {@link DatasetDescriptorGLACPE} the dataset descriptor
     */
    long testHeaders(CSVParser parser, VersionFile versionFile, String encoding, BadsFormatsReport badsFormatsReport, DatasetDescriptor datasetDescriptor) throws BusinessException;

}