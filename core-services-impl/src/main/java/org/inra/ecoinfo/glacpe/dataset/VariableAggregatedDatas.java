package org.inra.ecoinfo.glacpe.dataset;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.inra.ecoinfo.glacpe.refdata.outilsmesures.OutilsMesure;


/**
 * Cette classe aggrège quelques statistiques liées à une variable donnée dans un contexte de type de données particulier
 * 
 * @author antoine schellenberger *
 */
public class VariableAggregatedDatas {

    private String variableName;
    private Integer displayOrder;
    private String siteName;
    private String plateformeName;
    private OutilsMesure outilsMesure;
    private OutilsMesure outilsPrelevement;
    private Date heure;

    private AggregatedData minAggregatedData = new AggregatedData();
    private AggregatedData maxAggregatedData = new AggregatedData();
    private AggregatedData averageAggregatedData = new AggregatedData();
    private List<AggregatedData> targetAggregatedDatas = new LinkedList<AggregatedData>();

    /**
     *
     * @param siteName
     * @param plateformeName
     * @param variableName
     * @param displayOrder
     */
    public VariableAggregatedDatas(String siteName, String plateformeName, String variableName, Integer displayOrder) {
        super();
        this.variableName = variableName;
        this.displayOrder = displayOrder;
        this.siteName = siteName;
        this.plateformeName = plateformeName;
    }

    /**
     *
     * @param siteName
     * @param plateformeName
     * @param variableName
     */
    public VariableAggregatedDatas(String siteName, String plateformeName, String variableName) {
        super();
        this.variableName = variableName;
        this.siteName = siteName;
        this.plateformeName = plateformeName;
    }

    /**
     *
     * @return
     */
    public String getVariableName() {
        return variableName;
    }

    /**
     *
     * @return
     */
    public Integer getDisplayOrder() {
        return displayOrder;
    }

    /**
     *
     */
    public class ValueAggregatedData {

        private Float depth;
        private Float value;
        private Date heure;

        /**
         *
         * @param depth
         * @param value
         */
        public ValueAggregatedData(Float depth, Float value) {
            super();
            this.depth = depth;
            this.value = value;
        }

        /**
         *
         * @return
         */
        public Float getDepth() {
            return depth;
        }

        /**
         *
         * @return
         */
        public Float getValue() {
            return value;
        }

        /**
         *
         * @param depth
         */
        public void setDepth(Float depth) {
            this.depth = depth;
        }

        /**
         *
         * @param value
         */
        public void setValue(Float value) {
            this.value = value;
        }

        /**
         *
         * @return
         */
        public Date getHeure() {
            return heure;
        }

        /**
         *
         * @param heure
         */
        public void setHeure(Date heure) {
            this.heure = heure;
        }
    }

    /**
     *
     */
    public class AggregatedData {

        private Map<Date, List<ValueAggregatedData>> valuesAggregatedDatasByDatesMap = new HashMap<Date, List<ValueAggregatedData>>();
        private Map<Date, Float> valuesByDatesMap = new HashMap<Date, Float>();

        /**
         *
         */
        public AggregatedData() {
            super();
        }

        /**
         *
         * @return
         */
        public Map<Date, Float> getValuesByDatesMap() {
            return valuesByDatesMap;
        }

        /**
         *
         * @return
         */
        public Map<Date, List<ValueAggregatedData>> getValuesAggregatedDatasByDatesMap() {
            return valuesAggregatedDatasByDatesMap;
        }

    }

    /**
     *
     * @return
     */
    public AggregatedData getMinAggregatedData() {
        return minAggregatedData;
    }

    /**
     *
     * @param minAggregatedData
     */
    public void setMinAggregatedData(AggregatedData minAggregatedData) {
        this.minAggregatedData = minAggregatedData;
    }

    /**
     *
     * @return
     */
    public AggregatedData getMaxAggregatedData() {
        return maxAggregatedData;
    }

    /**
     *
     * @param maxAggregatedData
     */
    public void setMaxAggregatedData(AggregatedData maxAggregatedData) {
        this.maxAggregatedData = maxAggregatedData;
    }

    /**
     *
     * @return
     */
    public AggregatedData getAverageAggregatedData() {
        return averageAggregatedData;
    }

    /**
     *
     * @param averageAggregatedData
     */
    public void setAverageAggregatedData(AggregatedData averageAggregatedData) {
        this.averageAggregatedData = averageAggregatedData;
    }

    /**
     *
     * @return
     */
    public String getSiteName() {
        return siteName;
    }

    /**
     *
     * @return
     */
    public String getPlateformeName() {
        return plateformeName;
    }

    /**
     *
     * @return
     */
    public OutilsMesure getOutilsMesure() {
        return outilsMesure;
    }

    /**
     *
     * @param outilsMesure
     */
    public void setOutilsMesure(OutilsMesure outilsMesure) {
        this.outilsMesure = outilsMesure;
    }

    /**
     *
     * @return
     */
    public Date getHeure() {
        return heure;
    }

    /**
     *
     * @param heure
     */
    public void setHeure(Date heure) {
        this.heure = heure;
    }

    /**
     *
     * @return
     */
    public List<AggregatedData> getTargetAggregatedDatas() {
        return targetAggregatedDatas;
    }

    /**
     *
     * @param targetAggregatedDatas
     */
    public void setTargetAggregatedDatas(List<AggregatedData> targetAggregatedDatas) {
        this.targetAggregatedDatas = targetAggregatedDatas;
    }

    /**
     *
     * @return
     */
    public OutilsMesure getOutilsPrelevement() {
        return outilsPrelevement;
    }

    /**
     *
     * @param outilsPrelevement
     */
    public void setOutilsPrelevement(OutilsMesure outilsPrelevement) {
        this.outilsPrelevement = outilsPrelevement;
    }
}
