package org.inra.ecoinfo.glacpe.dataset.chimie;

import java.util.Date;
import java.util.List;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.glacpe.dataset.chimie.entity.MesureChimie;
import org.inra.ecoinfo.glacpe.dataset.chimie.entity.ValeurMesureChimie;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesRequestParamVO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.projetsite.ProjetSite;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public interface IChimieDAO extends IDAO<Object> {

    /**
     *
     * @return
     * @throws PersistenceException
     */
    List<Projet> retrieveChimieAvailablesProjets() throws PersistenceException;

    /**
     *
     * @param projetSiteId
     * @return
     * @throws PersistenceException
     */
    List<Plateforme> retrieveChimieAvailablesPlateformesByProjetSiteId(long projetSiteId) throws PersistenceException;

    /**
     *
     * @param plateformesIds
     * @param firstDate
     * @param lastDate
     * @return
     * @throws PersistenceException
     */
    List<VariableGLACPE> retrieveChimieAvailablesVariables(List<Long> plateformesIds, Date firstDate, Date lastDate) throws PersistenceException;

    /**
     *
     * @param selectedPlateformesIds
     * @param selectedVariablesIds
     * @param projetId
     * @param datesRequestParamVO
     * @return
     * @throws PersistenceException
     */
    List<ValeurMesureChimie> extractAggregatedDatasForAllDepth(List<Long> selectedPlateformesIds, List<Long> selectedVariablesIds, List<Long> projetId, DatesRequestParamVO datesRequestParamVO) throws PersistenceException;

    /**
     *
     * @param selectedPlateformesIds
     * @param selectedVariablesIds
     * @param projetId
     * @param datesRequestParamVO
     * @param depthMin
     * @param depthMax
     * @return
     * @throws PersistenceException
     */
    List<ValeurMesureChimie> extractAggregatedDatasForRangeDepth(List<Long> selectedPlateformesIds, List<Long> selectedVariablesIds, List<Long> projetId, DatesRequestParamVO datesRequestParamVO, Float depthMin, Float depthMax)
            throws PersistenceException;

    /**
     *
     * @param selectedPlateformesIds
     * @param projetId
     * @param datesRequestParamVO
     * @param depthMin
     * @param depthMax
     * @return
     * @throws PersistenceException
     */
    List<MesureChimie> extractDatasForRangeDepth(List<Long> selectedPlateformesIds, List<Long> projetId, DatesRequestParamVO datesRequestParamVO, Float depthMin, Float depthMax) throws PersistenceException;

    /**
     *
     * @param selectedPlateformesIds
     * @param projetId
     * @param datesRequestParamVO
     * @return
     * @throws PersistenceException
     */
    List<MesureChimie> extractDatasForAllDepth(List<Long> selectedPlateformesIds, List<Long> projetId, DatesRequestParamVO datesRequestParamVO) throws PersistenceException;

    /**
     *
     * @param projetId
     * @param siteId
     * @return
     * @throws PersistenceException
     */
    ProjetSite retrieveChimieAvailablesProjetsSiteByProjetAndSite(Long projetId, Long siteId) throws PersistenceException;

}
