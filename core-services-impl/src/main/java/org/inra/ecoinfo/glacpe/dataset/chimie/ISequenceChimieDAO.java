package org.inra.ecoinfo.glacpe.dataset.chimie;

import java.util.Date;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.glacpe.dataset.chimie.entity.SequenceChimie;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public interface ISequenceChimieDAO extends IDAO<SequenceChimie> {

    /**
     *
     * @param datePrelevement
     * @param projetSiteId
     * @return
     * @throws PersistenceException
     */
    SequenceChimie getByDatePrelevementAndProjetSiteId(Date datePrelevement, Long projetSiteId) throws PersistenceException;

    void flush();

}
