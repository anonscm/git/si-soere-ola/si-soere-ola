package org.inra.ecoinfo.glacpe.dataset.chimie;

import java.util.Date;

import org.inra.ecoinfo.glacpe.dataset.chimie.entity.ValeurMesureChimie;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public interface IValeurMesureChimieDAO {

    /**
     *
     * @param variableCode
     * @param profondeurMin
     * @param profondeurMax
     * @param projetCode
     * @param date
     * @param plateformeCode
     * @return
     * @throws PersistenceException
     */
    ValeurMesureChimie getByVariableCodeAndProfondeurAndProjetCodeAndDateAndPlateformeCode(String variableCode, Float profondeurMin, Float profondeurMax, String projetCode, Date date, String plateformeCode) throws PersistenceException;

}
