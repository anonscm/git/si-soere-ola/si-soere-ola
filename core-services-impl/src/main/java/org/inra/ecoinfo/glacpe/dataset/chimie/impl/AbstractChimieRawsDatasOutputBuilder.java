package org.inra.ecoinfo.glacpe.dataset.chimie.impl;

import java.text.DateFormat;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.glacpe.dataset.chimie.jpa.JPAValeurMesureChimieDAO;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.IDatatypeVariableUniteGLACPEDAO;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;

/**
 *
 * @author ptcherniati
 */
public abstract class AbstractChimieRawsDatasOutputBuilder extends AbstractOutputBuilder {

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH_CHIMIE = "org.inra.ecoinfo.glacpe.extraction.chimie.messages";

    /**
     *
     */
    protected static final String MAP_INDEX_0 = "0";

    /** The date formatter @link(DateFormat). */
    protected static DateFormat dateFormatter = DateUtil.getDateFormatLocale("dd/MM/yyyy");

    /**
     *
     */
    protected IVariableDAO variableDAO;

    /**
     *
     */
    protected JPAValeurMesureChimieDAO valeurMesureChimieDAO;

    /**
     *
     */
    protected IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO;

    /**
     *
     * @param variableDAO
     */
    public void setVariableDAO(IVariableDAO variableDAO) {
        this.variableDAO = variableDAO;
    }

    /**
     *
     * @param valeurMesureChimieDAO
     */
    public void setValeurMesureChimieDAO(JPAValeurMesureChimieDAO valeurMesureChimieDAO) {
        this.valeurMesureChimieDAO = valeurMesureChimieDAO;
    }

    /**
     *
     * @param selectedPlateformes
     * @return
     */
    protected Set<String> retrieveSitesCodes(List<PlateformeVO> selectedPlateformes) {
        Set<String> sitesNames = new HashSet<String>();
        for (PlateformeVO plateforme : selectedPlateformes) {
            sitesNames.add(Utils.createCodeFromString(plateforme.getNomSite()));
        }
        return sitesNames;
    }

    /**
     *
     * @param datatypeVariableUniteDAO
     */
    public void setDatatypeVariableUniteDAO(IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO) {
        this.datatypeVariableUniteDAO = datatypeVariableUniteDAO;
    }
}
