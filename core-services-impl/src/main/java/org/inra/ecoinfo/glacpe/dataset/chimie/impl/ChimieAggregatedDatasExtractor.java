package org.inra.ecoinfo.glacpe.dataset.chimie.impl;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.AbstractExtractor;
import org.inra.ecoinfo.glacpe.dataset.chimie.IChimieDAO;
import org.inra.ecoinfo.glacpe.dataset.chimie.entity.ValeurMesureChimie;
import org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEAggregatedDatasExtractor;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.DepthRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO;
import org.inra.ecoinfo.glacpe.extraction.vo.VariableVO;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class ChimieAggregatedDatasExtractor extends AbstractGLACPEAggregatedDatasExtractor {

    private static final String MAP_INDEX_0 = "0";

    /**
     *
     */
    public static String CST_RESULT_EXTRACTION_CODE = "chimieAggregatedDatas";

    /**
     *
     */
    protected static final String PROPERTY_MSG_NO_EXTRACTION_RESULT = "PROPERTY_MSG_NO_EXTRACTION_RESULT";

    /**
     *
     */
    protected IChimieDAO chimieDAO;

    private List<Long> buildPlateformesIds(List<PlateformeVO> selectedPlateformes) {
        List<Long> plateformesIds = new LinkedList<Long>();
        for (PlateformeVO plateforme : selectedPlateformes) {
            plateformesIds.add(plateforme.getId());
        }
        return plateformesIds;
    }

    private List<Long> buildProjetSiteIds(List<PlateformeVO> selectedPlateformes) {
        List<Long> projetSiteIds = new LinkedList<Long>();
        for (PlateformeVO plateforme : selectedPlateformes) {
            projetSiteIds.add(plateforme.getProjet().getId());
        }
        return projetSiteIds;
    }

    private List<Long> buildVariablesIds(List<VariableVO> selectedVariables) {
        List<Long> variablesIds = new LinkedList<Long>();
        for (VariableVO variable : selectedVariables) {
            variablesIds.add(variable.getId());
        }
        return variablesIds;
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, List> extractDatas(Map<String, Object> requestMetadatas) throws NoExtractionResultException, BusinessException {

        Map<String, List> extractedDatasMap = new HashMap<String, List>();
        DepthRequestParamVO depthRequestParamVO = (DepthRequestParamVO) requestMetadatas.get(DepthRequestParamVO.class.getSimpleName());
        List<PlateformeVO> selectedPlateformes = (List<PlateformeVO>) requestMetadatas.get(PlateformeVO.class.getSimpleName());
        List<VariableVO> selectedVariables = (List<VariableVO>) requestMetadatas.get(VariableVO.class.getSimpleName());
        List<Long> plateformesIds = buildPlateformesIds(selectedPlateformes);
        List<Long> projetSiteIds = buildProjetSiteIds(selectedPlateformes);
        List<Long> variablesIds = buildVariablesIds(selectedVariables);
        DatesRequestParamVO datesRequestParamVO = (DatesRequestParamVO) requestMetadatas.get(DatesRequestParamVO.class.getSimpleName());

        List<ValeurMesureChimie> valeursMesuresChimies = null;
        try {
            if (depthRequestParamVO.getAllDepth()) {
                valeursMesuresChimies = chimieDAO.extractAggregatedDatasForAllDepth(plateformesIds, variablesIds, projetSiteIds, datesRequestParamVO);
            } else {
                valeursMesuresChimies = chimieDAO.extractAggregatedDatasForRangeDepth(plateformesIds, variablesIds, projetSiteIds, datesRequestParamVO, depthRequestParamVO.getDepthMin(), depthRequestParamVO.getDepthMax());
            }
            extractedDatasMap.put(MAP_INDEX_0, valeursMesuresChimies);

            if (valeursMesuresChimies == null || valeursMesuresChimies.size() == 0) {
                throw new NoExtractionResultException(getLocalizationManager().getMessage(NoExtractionResultException.BUNDLE_SOURCE_PATH, PROPERTY_MSG_NO_EXTRACTION_RESULT));
            }

        } catch (PersistenceException e) {
            AbstractExtractor.LOGGER.error(e.getMessage());
            throw new BusinessException(e);
        }
        return extractedDatasMap;
    }

    @SuppressWarnings("rawtypes")
    @Override
    protected Map<String, List> filterExtractedDatas(Map<String, List> resultsDatasMap) throws BusinessException {
        filterExtractedValues(resultsDatasMap, MAP_INDEX_0);
        return resultsDatasMap;
    }

    @Override
    protected void prepareRequestMetadatas(Map<String, Object> requestMetadatasMap) throws BusinessException {
        super.prepareRequestMetadatas(requestMetadatasMap);
    }

    /**
     *
     * @param chimieDAO
     */
    public void setChimieDAO(IChimieDAO chimieDAO) {
        this.chimieDAO = chimieDAO;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public void extract(IParameter parameters) throws BusinessException {
        prepareRequestMetadatas(parameters.getParameters());
        Map<String, List> resultsDatasMap = extractDatas(parameters.getParameters());
        Map<String, List> filteredResultsDatasMap = filterExtractedDatas(resultsDatasMap);
        ((ChimieParameters) parameters).getResults().put(CST_RESULT_EXTRACTION_CODE, filteredResultsDatasMap);
    }
}
