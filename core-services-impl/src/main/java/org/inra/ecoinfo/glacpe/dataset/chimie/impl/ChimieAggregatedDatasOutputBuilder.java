package org.inra.ecoinfo.glacpe.dataset.chimie.impl;

import java.io.File;
import java.io.PrintStream;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.glacpe.dataset.IGLACPEAggregateData;
import org.inra.ecoinfo.glacpe.dataset.VariableAggregatedDatas;
import org.inra.ecoinfo.glacpe.dataset.VariableAggregatedDatas.ValueAggregatedData;
import org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEAggregatedDatasOutputBuilder;
import org.inra.ecoinfo.glacpe.extraction.vo.DatasRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.DepthRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO;
import org.inra.ecoinfo.glacpe.extraction.vo.ProjetSiteVO;
import org.inra.ecoinfo.glacpe.extraction.vo.VariableVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.IDatatypeVariableUniteGLACPEDAO;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.OutilsMesure;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import com.google.common.base.Strings;

/**
 *
 * @author ptcherniati
 */
public class ChimieAggregatedDatasOutputBuilder extends AbstractGLACPEAggregatedDatasOutputBuilder {

    private static final String BUNDLE_SOURCE_PATH_CHIMIE = "org.inra.ecoinfo.glacpe.extraction.chimie.messages";

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException, NoExtractionResultException {
        ((DefaultParameter) parameters).getFilesMaps().add(super.buildOutput(parameters, ChimieAggregatedDatasExtractor.CST_RESULT_EXTRACTION_CODE));
        return null;
    }

    /**
     *
     */
    protected static final String SUFFIX_FILENAME_AGGREGATED = "aggregated";
    private static final String HEADER_RAW_DATA = "PROPERTY_MSG_HEADER_AGGREGATED";
    private static final String MAP_INDEX_0 = "0";

    private static final String CODE_DATATYPE_CHIMIE = "physico_chimie";

    /**
     *
     */
    protected IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO;

    @SuppressWarnings("unchecked")
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatas) throws BusinessException {

        Properties propertiesVariableName = localizationManager.newProperties(Variable.NAME_ENTITY_JPA, "nom");
        List<VariableVO> selectedVariables = (List<VariableVO>) requestMetadatas.get(VariableVO.class.getSimpleName());
        DatasRequestParamVO datasRequestParamVO = (DatasRequestParamVO) requestMetadatas.get(DatasRequestParamVO.class.getSimpleName());

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format("%s", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_CHIMIE, HEADER_RAW_DATA)));
        for (VariableVO variableVO : selectedVariables) {
            String uniteNom = "nounit";
            try {
                Unite unite = datatypeVariableUniteDAO.getUnite(CODE_DATATYPE_CHIMIE, variableVO.getCode());

                if (unite != null) {
                    uniteNom = unite.getCode();
                }
            } catch (PersistenceException e) {
                throw new BusinessException(e);
            }

            String localizedVariableName = propertiesVariableName.getProperty(variableVO.getNom());
            if (Strings.isNullOrEmpty(localizedVariableName))
                localizedVariableName = variableVO.getNom();

            if (datasRequestParamVO.getMinValueAndAssociatedDepth()) {
                stringBuilder.append(String.format(";\"minimum(%s)%n%s\"", localizedVariableName, uniteNom));
            }
            if (datasRequestParamVO.getMaxValueAndAssociatedDepth()) {
                stringBuilder.append(String.format(";\"maximum(%s)%n%s\"", localizedVariableName, uniteNom));
            }
        }
        return stringBuilder.toString();

    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {

        List<VariableVO> selectedVariables = (List<VariableVO>) requestMetadatasMap.get(VariableVO.class.getSimpleName());
        DatasRequestParamVO datasRequestParamVO = (DatasRequestParamVO) requestMetadatasMap.get(DatasRequestParamVO.class.getSimpleName());
        DepthRequestParamVO depthRequestParamVO = (DepthRequestParamVO) requestMetadatasMap.get(DepthRequestParamVO.class.getSimpleName());

        String depthMin = depthRequestParamVO.getAllDepth() ? "" : depthRequestParamVO.getDepthMin().toString();
        String depthMax = depthRequestParamVO.getAllDepth() ? "" : depthRequestParamVO.getDepthMax().toString();

        List<IGLACPEAggregateData> valeursMesures = resultsDatasMap.get(MAP_INDEX_0);

        Properties propertiesProjetName = localizationManager.newProperties(Projet.NAME_TABLE, "nom");
        Properties propertiesPlateformeName = localizationManager.newProperties(Plateforme.TABLE_NAME, "nom");
        Properties propertiesSiteName = localizationManager.newProperties(Site.NAME_ENTITY_JPA, "nom");
        Properties propertiesOutilName = localizationManager.newProperties(OutilsMesure.TABLE_NAME, "nom");

        String minSeparator = buildCSVSeparator(datasRequestParamVO.getMinValueAndAssociatedDepth());
        String maxSeparator = buildCSVSeparator(datasRequestParamVO.getMaxValueAndAssociatedDepth());

        List<PlateformeVO> selectedPlateformes = (List<PlateformeVO>) requestMetadatasMap.get(PlateformeVO.class.getSimpleName());
        ProjetSiteVO selectedProjet = selectedPlateformes.get(0).getProjet();
        Set<String> sitesNames = retrieveSitesCodes(selectedPlateformes);
        Map<String, File> filesMap = buildOutputsFiles(sitesNames, SUFFIX_FILENAME_AGGREGATED);
        Map<String, PrintStream> outputPrintStreamMap = buildOutputPrintStreamMap(filesMap);
        for (String siteName : outputPrintStreamMap.keySet()) {
            outputPrintStreamMap.get(siteName).println(headers);
        }

        Map<String, Map<Date, Map<String, VariableAggregatedDatas>>> variablesAggregatedDatas = buildVariablesAggregatedDatas(valeursMesures);

        for (String siteCode : variablesAggregatedDatas.keySet()) {
            List<Date> dateSet = asSortedList(variablesAggregatedDatas.get(siteCode).keySet());

            for (VariableVO variable : selectedVariables) {
                if (datasRequestParamVO.getMinValueAndAssociatedDepth()) {
                    for (Date dateKey : dateSet) {
                        VariableAggregatedDatas variableAggregatedDatas = null;
                        String localizedOutilPrelevementName = "";
                        String localizedOutilMesureName = "";

                        if (variablesAggregatedDatas.get(siteCode).get(dateKey).get(variable.getCode()) != null) {
                            variableAggregatedDatas = variablesAggregatedDatas.get(siteCode).get(dateKey).get(variable.getCode());
                        }
                        if (localizedOutilPrelevementName.isEmpty() && variablesAggregatedDatas.get(siteCode).get(dateKey).get(variable.getCode()) != null
                                && variablesAggregatedDatas.get(siteCode).get(dateKey).get(variable.getCode()).getOutilsPrelevement() != null)
                        {
                            localizedOutilPrelevementName = propertiesOutilName.getProperty(variablesAggregatedDatas.get(siteCode).get(dateKey).get(variable.getCode()).getOutilsPrelevement().getNom());
                            if (Strings.isNullOrEmpty(localizedOutilPrelevementName))
                                localizedOutilPrelevementName = variablesAggregatedDatas.get(siteCode).get(dateKey).get(variable.getCode()).getOutilsPrelevement().getNom();
                        }
                        if (localizedOutilMesureName.isEmpty() && variablesAggregatedDatas.get(siteCode).get(dateKey).get(variable.getCode()) != null
                                && variablesAggregatedDatas.get(siteCode).get(dateKey).get(variable.getCode()).getOutilsMesure() != null)
                        {
                            localizedOutilMesureName = propertiesOutilName.getProperty(variablesAggregatedDatas.get(siteCode).get(dateKey).get(variable.getCode()).getOutilsMesure().getNom());
                            if (Strings.isNullOrEmpty(localizedOutilMesureName))
                                localizedOutilMesureName = variablesAggregatedDatas.get(siteCode).get(dateKey).get(variable.getCode()).getOutilsMesure().getNom();
                        }
                        if (variableAggregatedDatas != null) {
                            PrintStream rawDataPrintStream = outputPrintStreamMap.get(Utils.createCodeFromString(variableAggregatedDatas.getSiteName()));

                            String localizedProjetName = propertiesProjetName.getProperty(selectedProjet.getNom());
                            if (Strings.isNullOrEmpty(localizedProjetName))
                                localizedProjetName = selectedProjet.getNom();

                            String localizedSiteName = propertiesSiteName.getProperty(variableAggregatedDatas.getSiteName());
                            if (Strings.isNullOrEmpty(localizedSiteName))
                                localizedSiteName = variableAggregatedDatas.getSiteName();

                            String localizedPlateformeName = propertiesPlateformeName.getProperty(variableAggregatedDatas.getPlateformeName());
                            if (Strings.isNullOrEmpty(localizedPlateformeName))
                                localizedPlateformeName = variableAggregatedDatas.getPlateformeName();

                            for (ValueAggregatedData value : variableAggregatedDatas.getMinAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey)) {
                                rawDataPrintStream.print(String.format("%s;%s;%s;", localizedProjetName, localizedSiteName, localizedPlateformeName));
                                rawDataPrintStream.print(String.format("%s;%s;%s;%s;%s;%s;", DateUtil.getSimpleDateFormatDateLocale().format(dateKey), localizedOutilPrelevementName, localizedOutilMesureName, depthMin, depthMax,
                                        value.getDepth() == null ? "" : value.getDepth()));

                                for (VariableVO variableVO : selectedVariables) {
                                    if (Utils.createCodeFromString(variableAggregatedDatas.getVariableName()).equals(variableVO.getCode())) {
                                        rawDataPrintStream.print(String.format("%s%s%s", value.getValue(), minSeparator, maxSeparator));
                                        break;
                                    } else {
                                        rawDataPrintStream.print(String.format("%s%s", minSeparator, maxSeparator));
                                    }
                                }
                                rawDataPrintStream.println();
                            }
                        }
                    }
                }
                if (datasRequestParamVO.getMaxValueAndAssociatedDepth()) {
                    for (Date dateKey : dateSet) {
                        VariableAggregatedDatas variableAggregatedDatas = null;
                        String localizedOutilPrelevementName = "";
                        String localizedOutilMesureName = "";

                        if (variablesAggregatedDatas.get(siteCode).get(dateKey).get(variable.getCode()) != null) {
                            variableAggregatedDatas = variablesAggregatedDatas.get(siteCode).get(dateKey).get(variable.getCode());
                        }
                        if (localizedOutilPrelevementName.isEmpty() && variablesAggregatedDatas.get(siteCode).get(dateKey).get(variable.getCode()) != null
                                && variablesAggregatedDatas.get(siteCode).get(dateKey).get(variable.getCode()).getOutilsPrelevement() != null)
                        {
                            localizedOutilPrelevementName = propertiesOutilName.getProperty(variablesAggregatedDatas.get(siteCode).get(dateKey).get(variable.getCode()).getOutilsPrelevement().getNom());
                            if (Strings.isNullOrEmpty(localizedOutilPrelevementName))
                                localizedOutilPrelevementName = variablesAggregatedDatas.get(siteCode).get(dateKey).get(variable.getCode()).getOutilsPrelevement().getNom();
                        }
                        if (localizedOutilMesureName.isEmpty() && variablesAggregatedDatas.get(siteCode).get(dateKey).get(variable.getCode()) != null
                                && variablesAggregatedDatas.get(siteCode).get(dateKey).get(variable.getCode()).getOutilsMesure() != null)
                        {
                            localizedOutilMesureName = propertiesOutilName.getProperty(variablesAggregatedDatas.get(siteCode).get(dateKey).get(variable.getCode()).getOutilsMesure().getNom());
                            if (Strings.isNullOrEmpty(localizedOutilMesureName))
                                localizedOutilMesureName = variablesAggregatedDatas.get(siteCode).get(dateKey).get(variable.getCode()).getOutilsMesure().getNom();
                        }
                        if (variableAggregatedDatas != null) {
                            PrintStream rawDataPrintStream = outputPrintStreamMap.get(Utils.createCodeFromString(variableAggregatedDatas.getSiteName()));

                            String localizedProjetName = propertiesProjetName.getProperty(selectedProjet.getNom());
                            if (Strings.isNullOrEmpty(localizedProjetName))
                                localizedProjetName = selectedProjet.getNom();

                            String localizedSiteName = propertiesSiteName.getProperty(variableAggregatedDatas.getSiteName());
                            if (Strings.isNullOrEmpty(localizedSiteName))
                                localizedSiteName = variableAggregatedDatas.getSiteName();

                            String localizedPlateformeName = propertiesPlateformeName.getProperty(variableAggregatedDatas.getPlateformeName());
                            if (Strings.isNullOrEmpty(localizedPlateformeName))
                                localizedPlateformeName = variableAggregatedDatas.getPlateformeName();

                            for (ValueAggregatedData value : variableAggregatedDatas.getMinAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey)) {
                                rawDataPrintStream.print(String.format("%s;%s;%s;", localizedProjetName, localizedSiteName, localizedPlateformeName));
                                rawDataPrintStream.print(String.format("%s;%s;%s;%s;%s;%s;", DateUtil.getSimpleDateFormatDateLocale().format(dateKey), localizedOutilPrelevementName, localizedOutilMesureName, depthMin, depthMax,
                                        value.getDepth() == null ? "" : value.getDepth()));

                                for (VariableVO variableVO : selectedVariables) {
                                    if (Utils.createCodeFromString(variableAggregatedDatas.getVariableName()).equals(variableVO.getCode())) {
                                        rawDataPrintStream.print(String.format("%s%s%s", minSeparator, value.getValue(), maxSeparator));
                                        break;
                                    } else {
                                        rawDataPrintStream.print(String.format("%s%s", minSeparator, maxSeparator));
                                    }
                                }
                                rawDataPrintStream.println();
                            }
                        }
                    }
                }
            }
        }
        closeStreams(outputPrintStreamMap);
        return filesMap;
    }

    /**
     *
     * @param <T>
     * @param c
     * @return
     */
    public static <T extends Comparable<? super T>> List<T> asSortedList(Collection<T> c) {
        List<T> list = new LinkedList<T>(c);
        Collections.sort(list);
        return list;
    }

    /**
     *
     * @param datatypeVariableUniteDAO
     */
    public void setDatatypeVariableUniteDAO(IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO) {
        this.datatypeVariableUniteDAO = datatypeVariableUniteDAO;
    }
}
