package org.inra.ecoinfo.glacpe.dataset.chimie.impl;

import java.io.File;
import java.io.PrintStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.glacpe.dataset.chimie.ConstantsChimie;
import org.inra.ecoinfo.glacpe.dataset.chimie.entity.MesureChimie;
import org.inra.ecoinfo.glacpe.dataset.chimie.entity.ValeurMesureChimie;
import org.inra.ecoinfo.glacpe.extraction.vo.DepthRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO;
import org.inra.ecoinfo.glacpe.extraction.vo.VariableVO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import com.google.common.base.Strings;

/**
 *
 * @author ptcherniati
 */
public class ChimieRawsDatasOutputBuilderByLine extends AbstractChimieRawsDatasOutputBuilder {

    private static final String CODE_DATATYPE_PHYSICO_CHIMIE = "physico_chimie";

    private static final String HEADER_RAW_DATA_ALLDEPTH = "PROPERTY_MSG_HEADER_RAW_ALLDEPTH_ROW";
    private static final String HEADER_RAW_DATA_RIVER = "PROPERTY_MSG_HEADER_RAW_RIVER_ROW";
    private static final String HEADER_RAW_DATA = "PROPERTY_MSG_HEADER_RAW_ROW";

    private static String KEY_VARIABLE_BALANCE_IONIQUE = ConstantsChimie.KEY_VARIABLE_BALANCE_IONIQUE;

    private static final String CODE_TYPE_SITE_RIVIERE = "riviere";
    private static final String CODE_TYPE_OUTIL_MESURE = "mesure";
    private Map<String, Float> variablesMap = new HashMap<>();
    private Map<String, String> variablesHeaderMap = new HashMap<>();
    private Map<String, String> variablesUnitsMap = new HashMap<>();    

    @SuppressWarnings("unchecked")
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatas) throws BusinessException {
        DepthRequestParamVO depthRequestParamVO = (DepthRequestParamVO) requestMetadatas.get(DepthRequestParamVO.class.getSimpleName());
        String typeSite = ((List<PlateformeVO>) requestMetadatas.get(PlateformeVO.class.getSimpleName())).get(0).getProjet().getType();
        StringBuilder stringBuilder = new StringBuilder();

        if (!typeSite.equals(CODE_TYPE_SITE_RIVIERE)) {
            if (depthRequestParamVO.getAllDepth()) {
                stringBuilder.append(String.format("%s", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_CHIMIE, HEADER_RAW_DATA_ALLDEPTH)));
            } else {
                stringBuilder.append(String.format("%s", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_CHIMIE, HEADER_RAW_DATA)));
            }
        } else {
            stringBuilder.append(String.format("%s", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_CHIMIE, HEADER_RAW_DATA_RIVER)));
        }

        return stringBuilder.toString();

    }

    private void initVariablesMap() {
        if (variablesMap.isEmpty()) {

            variablesMap.put(ConstantsChimie.KEY_VARIABLE_NH4, 1.2878f);
            variablesMap.put(ConstantsChimie.KEY_VARIABLE_NO3, 4.4268f);
            variablesMap.put(ConstantsChimie.KEY_VARIABLE_NO2, 3.28443f);
            variablesMap.put(ConstantsChimie.KEY_VARIABLE_PO4, 3.065f);
        }

        if (variablesHeaderMap.isEmpty()) {
            variablesHeaderMap.put(ConstantsChimie.KEY_VARIABLE_NO3, "Nitrates(Azote nitrique)");
            variablesHeaderMap.put(ConstantsChimie.KEY_VARIABLE_NH4, "Ammonium(Azote ammoniacal)");
            variablesHeaderMap.put(ConstantsChimie.KEY_VARIABLE_NO2, "Nitrites(Azote Nitreux)");
            variablesHeaderMap.put(ConstantsChimie.KEY_VARIABLE_PO4, "Phosphore");
        }
        if (variablesUnitsMap.isEmpty()) {
            variablesUnitsMap.put(ConstantsChimie.KEY_VARIABLE_NO3, "mg(NO3)/l");
            variablesUnitsMap.put(ConstantsChimie.KEY_VARIABLE_NH4, "mg(NH4)/l");
            variablesUnitsMap.put(ConstantsChimie.KEY_VARIABLE_NO2, "mg(NO2)/l");
            variablesUnitsMap.put(ConstantsChimie.KEY_VARIABLE_PO4, "mg(P)/l");
        }
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {

        try {
            initVariablesMap();
            List<VariableVO> selectedVariables = (List<VariableVO>) requestMetadatasMap.get(VariableVO.class.getSimpleName());
            DepthRequestParamVO depthRequestParamVO = (DepthRequestParamVO) requestMetadatasMap.get(DepthRequestParamVO.class.getSimpleName());

            Iterator<MesureChimie> mesuresChimies = resultsDatasMap.get(MAP_INDEX_0).iterator();

            VariableGLACPE balanceIoniqueVariable = (VariableGLACPE) variableDAO.getByCode(KEY_VARIABLE_BALANCE_IONIQUE);

            List<PlateformeVO> selectedPlateformes = (List<PlateformeVO>) requestMetadatasMap.get(PlateformeVO.class.getSimpleName());
            String typeSite = selectedPlateformes.get(0).getProjet().getType();
            Set<String> sitesNames = retrieveSitesCodes(selectedPlateformes);
            Map<String, File> filesMap = buildOutputsFiles(sitesNames, SUFFIX_FILENAME_DEFAULT);
            Map<String, PrintStream> outputPrintStreamMap = buildOutputPrintStreamMap(filesMap);
            for (String siteName : outputPrintStreamMap.keySet()) {
                outputPrintStreamMap.get(siteName).println(headers);
            }

            DataType datatypeChimie = datatypeDAO.getByCode(CODE_DATATYPE_PHYSICO_CHIMIE);
            Properties propertiesVariableName = localizationManager.newProperties(Variable.NAME_ENTITY_JPA, "nom");
            Properties propertiesProjetName = localizationManager.newProperties(Projet.NAME_TABLE, "nom");
            Properties propertiesPlateformeName = localizationManager.newProperties(Plateforme.TABLE_NAME, "nom");
            Properties propertiesSiteName = localizationManager.newProperties(Site.NAME_ENTITY_JPA, "nom");
            while (mesuresChimies.hasNext()) {
                final MesureChimie mesureChimie = mesuresChimies.next();
                PrintStream rawDataPrintStream = outputPrintStreamMap.get(((SiteGLACPE) mesureChimie.getSite()).getCodeFromName());

                String localizedProjetName = propertiesProjetName.getProperty(mesureChimie.getSousSequence().getSequence().getProjetSite().getProjet().getNom());
                if (Strings.isNullOrEmpty(localizedProjetName)) {
                    localizedProjetName = mesureChimie.getSousSequence().getSequence().getProjetSite().getProjet().getNom();
                }

                String localizedSiteName = propertiesSiteName.getProperty(mesureChimie.getSousSequence().getSequence().getProjetSite().getSite().getNom());
                if (Strings.isNullOrEmpty(localizedSiteName)) {
                    localizedSiteName = mesureChimie.getSousSequence().getSequence().getProjetSite().getSite().getNom();
                }

                String localizedPlateformeName = propertiesPlateformeName.getProperty(mesureChimie.getSousSequence().getPlateforme().getNom());
                if (Strings.isNullOrEmpty(localizedPlateformeName)) {
                    localizedPlateformeName = mesureChimie.getSousSequence().getPlateforme().getNom();
                }

                Map<Long, ValeurMesureChimie> valeursMesuresChimie = buildValeurs(mesureChimie.getValeurs());
                for (VariableVO variableVO : selectedVariables) {
                    if (!variableVO.getCode().equals(KEY_VARIABLE_BALANCE_IONIQUE)) {
                        ValeurMesureChimie valeurMesureChimie = valeursMesuresChimie.get(variableVO.getId());
                        if (valeurMesureChimie != null && valeurMesureChimie.getValue() != null) {
                            String localizedVariableName = propertiesVariableName.getProperty(variableVO.getNom());
                            if (Strings.isNullOrEmpty(localizedVariableName)) {
                                localizedVariableName = variableVO.getNom();
                            }
                            if (!typeSite.equals(CODE_TYPE_SITE_RIVIERE)) {
                                if (depthRequestParamVO.getAllDepth()) {
                                    rawDataPrintStream.println(String.format("%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s", localizedProjetName, localizedSiteName, localizedPlateformeName,
                                            dateFormatter.format(mesureChimie.getSousSequence().getSequence().getDatePrelevement()),
                                            mesureChimie.getSousSequence().getOutilsMesure().getTypeOutilsMesure().getType().getValeur().equals(CODE_TYPE_OUTIL_MESURE) ? "" : mesureChimie.getSousSequence().getOutilsMesure().getNom(), mesureChimie
                                            .getSousSequence().getOutilsMesure().getTypeOutilsMesure().getType().getValeur().equals(CODE_TYPE_OUTIL_MESURE) ? mesureChimie.getSousSequence().getOutilsMesure().getNom() : "",
                                            mesureChimie.getProfondeurMin(), mesureChimie.getProfondeurMax(), mesureChimie.getProfondeurReelle() == null ? "" : mesureChimie.getProfondeurReelle(), localizedVariableName,
                                            valeurMesureChimie.getValeur(), valeurMesureChimie.getVariable().getUnite(datatypeChimie).getNom()));

                                    if (variablesMap.containsKey(variableVO.getCode())) {
                                        rawDataPrintStream.println(String.format("%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s", localizedProjetName, localizedSiteName, localizedPlateformeName,
                                                dateFormatter.format(mesureChimie.getSousSequence().getSequence().getDatePrelevement()),
                                                mesureChimie.getSousSequence().getOutilsMesure().getTypeOutilsMesure().getType().getValeur().equals(CODE_TYPE_OUTIL_MESURE) ? "" : mesureChimie.getSousSequence().getOutilsMesure().getNom(), mesureChimie
                                                .getSousSequence().getOutilsMesure().getTypeOutilsMesure().getType().getValeur().equals(CODE_TYPE_OUTIL_MESURE) ? mesureChimie.getSousSequence().getOutilsMesure().getNom() : "",
                                                mesureChimie.getProfondeurMin(), mesureChimie.getProfondeurMax(), mesureChimie.getProfondeurReelle() == null ? "" : mesureChimie.getProfondeurReelle(), variablesHeaderMap.get(variableVO.getCode()),
                                                valeurMesureChimie.getValeur() * variablesMap.get(variableVO.getCode()), variablesUnitsMap.get(variableVO.getCode())));
                                    }

                                } else {
                                    rawDataPrintStream.println(String.format("%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s", localizedProjetName, localizedSiteName, localizedPlateformeName, depthRequestParamVO.getDepthMin(),
                                            depthRequestParamVO.getDepthMax(), dateFormatter.format(mesureChimie.getSousSequence().getSequence().getDatePrelevement()), mesureChimie.getSousSequence().getOutilsMesure().getTypeOutilsMesure().getType()
                                            .getValeur().equals(CODE_TYPE_OUTIL_MESURE) ? "" : mesureChimie.getSousSequence().getOutilsMesure().getNom(), mesureChimie.getSousSequence().getOutilsMesure().getTypeOutilsMesure().getType()
                                            .getValeur().equals(CODE_TYPE_OUTIL_MESURE) ? mesureChimie.getSousSequence().getOutilsMesure().getNom() : "", mesureChimie.getProfondeurMin(), mesureChimie.getProfondeurMax(),
                                            mesureChimie.getProfondeurReelle() == null ? "" : mesureChimie.getProfondeurReelle(), localizedVariableName, valeurMesureChimie.getValeur(), valeurMesureChimie.getVariable().getUnite(datatypeChimie)
                                            .getNom()));

                                    if (variablesMap.containsKey(variableVO.getCode())) {
                                        rawDataPrintStream.println(String.format("%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s", localizedProjetName, localizedSiteName, localizedPlateformeName, depthRequestParamVO.getDepthMin(),
                                                depthRequestParamVO.getDepthMax(), dateFormatter.format(mesureChimie.getSousSequence().getSequence().getDatePrelevement()), mesureChimie.getSousSequence().getOutilsMesure().getTypeOutilsMesure().getType()
                                                .getValeur().equals(CODE_TYPE_OUTIL_MESURE) ? "" : mesureChimie.getSousSequence().getOutilsMesure().getNom(), mesureChimie.getSousSequence().getOutilsMesure().getTypeOutilsMesure().getType()
                                                .getValeur().equals(CODE_TYPE_OUTIL_MESURE) ? mesureChimie.getSousSequence().getOutilsMesure().getNom() : "", mesureChimie.getProfondeurMin(), mesureChimie.getProfondeurMax(),
                                                mesureChimie.getProfondeurReelle() == null ? "" : mesureChimie.getProfondeurReelle(), variablesHeaderMap.get(variableVO.getCode()), valeurMesureChimie.getValeur() * variablesMap.get(variableVO.getCode()), 
                                                variablesUnitsMap.get(variableVO.getCode())));
                                    }

                                }
                            } else {
                                rawDataPrintStream.println(String.format("%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s", localizedProjetName, localizedSiteName, localizedPlateformeName, retrieveValidDateString(mesureChimie.getSousSequence().getSequence()
                                        .getDateDebutCampagne()), retrieveValidDateString(mesureChimie.getSousSequence().getSequence().getDateFinCampagne()), retrieveValidDateString(mesureChimie.getSousSequence().getSequence().getDateReception()),
                                        mesureChimie.getSousSequence().getOutilsMesure().getTypeOutilsMesure().getType().getValeur().equals(CODE_TYPE_OUTIL_MESURE) ? "" : mesureChimie.getSousSequence().getOutilsMesure().getNom(), mesureChimie
                                        .getSousSequence().getOutilsMesure().getTypeOutilsMesure().getType().getValeur().equals(CODE_TYPE_OUTIL_MESURE) ? mesureChimie.getSousSequence().getOutilsMesure().getNom() : "", localizedVariableName,
                                        valeurMesureChimie.getValeur(), valeurMesureChimie.getVariable().getUnite(datatypeChimie).getNom()));

                                if (variablesMap.containsKey(variableVO.getCode())) {
                                    rawDataPrintStream.println(String.format("%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s", localizedProjetName, localizedSiteName, localizedPlateformeName, retrieveValidDateString(mesureChimie.getSousSequence().getSequence()
                                            .getDateDebutCampagne()), retrieveValidDateString(mesureChimie.getSousSequence().getSequence().getDateFinCampagne()), retrieveValidDateString(mesureChimie.getSousSequence().getSequence().getDateReception()),
                                            mesureChimie.getSousSequence().getOutilsMesure().getTypeOutilsMesure().getType().getValeur().equals(CODE_TYPE_OUTIL_MESURE) ? "" : mesureChimie.getSousSequence().getOutilsMesure().getNom(), mesureChimie
                                            .getSousSequence().getOutilsMesure().getTypeOutilsMesure().getType().getValeur().equals(CODE_TYPE_OUTIL_MESURE) ? mesureChimie.getSousSequence().getOutilsMesure().getNom() : "", variablesHeaderMap.get(variableVO.getCode()),
                                            valeurMesureChimie.getValeur() * variablesMap.get(variableVO.getCode()), 
                                            variablesUnitsMap.get(variableVO.getCode())));

                                }

                            }
                        }
                    } else {
                        String localizedVariableName = propertiesVariableName.getProperty(variableVO.getNom());
                        if (Strings.isNullOrEmpty(localizedVariableName)) {
                            localizedVariableName = variableVO.getNom();
                        }

                        String balanceIonique = processBalanceIonique(mesureChimie);

                        if (!typeSite.equals(CODE_TYPE_SITE_RIVIERE)) {
                            if (depthRequestParamVO.getAllDepth()) {
                                rawDataPrintStream.println(String.format("%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s", localizedProjetName, localizedSiteName, localizedPlateformeName,
                                        dateFormatter.format(mesureChimie.getSousSequence().getSequence().getDatePrelevement()),
                                        mesureChimie.getSousSequence().getOutilsMesure().getTypeOutilsMesure().getType().getValeur().equals(CODE_TYPE_OUTIL_MESURE) ? "" : mesureChimie.getSousSequence().getOutilsMesure().getNom(), mesureChimie
                                        .getSousSequence().getOutilsMesure().getTypeOutilsMesure().getType().getValeur().equals(CODE_TYPE_OUTIL_MESURE) ? mesureChimie.getSousSequence().getOutilsMesure().getNom() : "",
                                        mesureChimie.getProfondeurMin(), mesureChimie.getProfondeurMax(), mesureChimie.getProfondeurReelle() == null ? "" : mesureChimie.getProfondeurReelle(), localizedVariableName, balanceIonique,
                                        balanceIoniqueVariable.getUnite(datatypeChimie).getNom()));
                            } else {
                                rawDataPrintStream.println(String.format("%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s", localizedProjetName, localizedSiteName, localizedPlateformeName, depthRequestParamVO.getDepthMin(),
                                        depthRequestParamVO.getDepthMax(), dateFormatter.format(mesureChimie.getSousSequence().getSequence().getDatePrelevement()), mesureChimie.getSousSequence().getOutilsMesure().getTypeOutilsMesure().getType()
                                        .getValeur().equals(CODE_TYPE_OUTIL_MESURE) ? "" : mesureChimie.getSousSequence().getOutilsMesure().getNom(), mesureChimie.getSousSequence().getOutilsMesure().getTypeOutilsMesure().getType()
                                        .getValeur().equals(CODE_TYPE_OUTIL_MESURE) ? mesureChimie.getSousSequence().getOutilsMesure().getNom() : "", mesureChimie.getProfondeurMin(), mesureChimie.getProfondeurMax(),
                                        mesureChimie.getProfondeurReelle() == null ? "" : mesureChimie.getProfondeurReelle(), localizedVariableName, balanceIonique, balanceIoniqueVariable.getUnite(datatypeChimie).getNom()));
                            }
                        } else {
                            rawDataPrintStream.println(String.format("%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s", localizedProjetName, localizedSiteName, localizedPlateformeName, retrieveValidDateString(mesureChimie.getSousSequence().getSequence()
                                    .getDateDebutCampagne()), retrieveValidDateString(mesureChimie.getSousSequence().getSequence().getDateFinCampagne()), retrieveValidDateString(mesureChimie.getSousSequence().getSequence().getDateReception()),
                                    mesureChimie.getSousSequence().getOutilsMesure().getTypeOutilsMesure().getType().getValeur().equals(CODE_TYPE_OUTIL_MESURE) ? "" : mesureChimie.getSousSequence().getOutilsMesure().getNom(), mesureChimie
                                    .getSousSequence().getOutilsMesure().getTypeOutilsMesure().getType().getValeur().equals(CODE_TYPE_OUTIL_MESURE) ? mesureChimie.getSousSequence().getOutilsMesure().getNom() : "", localizedVariableName,
                                    balanceIonique, balanceIoniqueVariable.getUnite(datatypeChimie).getNom()));
                        }
                    }
                }
//                mesuresChimies.remove();
            }

            closeStreams(outputPrintStreamMap);
            return filesMap;
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    private String processBalanceIonique(MesureChimie mesureChimie) throws PersistenceException {

        Float balanceIonique = null;
        String balanceIoniqueString = "";

        ValeurMesureChimie vmcCA = valeurMesureChimieDAO.getByVariableCodeAndProfondeurAndProjetCodeAndDateAndPlateformeCode(ConstantsChimie.KEY_VARIABLE_CA, mesureChimie.getProfondeurMin(), mesureChimie.getProfondeurMax(), mesureChimie
                .getSousSequence().getSequence().getProjetSite().getProjet().getCode(), mesureChimie.getSousSequence().getSequence().getDatePrelevement(), mesureChimie.getSousSequence().getPlateforme().getCode());
        ValeurMesureChimie vmcK = valeurMesureChimieDAO.getByVariableCodeAndProfondeurAndProjetCodeAndDateAndPlateformeCode(ConstantsChimie.KEY_VARIABLE_K, mesureChimie.getProfondeurMin(), mesureChimie.getProfondeurMax(), mesureChimie
                .getSousSequence().getSequence().getProjetSite().getProjet().getCode(), mesureChimie.getSousSequence().getSequence().getDatePrelevement(), mesureChimie.getSousSequence().getPlateforme().getCode());
        ValeurMesureChimie vmcCL = valeurMesureChimieDAO.getByVariableCodeAndProfondeurAndProjetCodeAndDateAndPlateformeCode(ConstantsChimie.KEY_VARIABLE_CL, mesureChimie.getProfondeurMin(), mesureChimie.getProfondeurMax(), mesureChimie
                .getSousSequence().getSequence().getProjetSite().getProjet().getCode(), mesureChimie.getSousSequence().getSequence().getDatePrelevement(), mesureChimie.getSousSequence().getPlateforme().getCode());
        ValeurMesureChimie vmcMG = valeurMesureChimieDAO.getByVariableCodeAndProfondeurAndProjetCodeAndDateAndPlateformeCode(ConstantsChimie.KEY_VARIABLE_MG, mesureChimie.getProfondeurMin(), mesureChimie.getProfondeurMax(), mesureChimie
                .getSousSequence().getSequence().getProjetSite().getProjet().getCode(), mesureChimie.getSousSequence().getSequence().getDatePrelevement(), mesureChimie.getSousSequence().getPlateforme().getCode());
        ValeurMesureChimie vmcNA = valeurMesureChimieDAO.getByVariableCodeAndProfondeurAndProjetCodeAndDateAndPlateformeCode(ConstantsChimie.KEY_VARIABLE_NA, mesureChimie.getProfondeurMin(), mesureChimie.getProfondeurMax(), mesureChimie
                .getSousSequence().getSequence().getProjetSite().getProjet().getCode(), mesureChimie.getSousSequence().getSequence().getDatePrelevement(), mesureChimie.getSousSequence().getPlateforme().getCode());
        ValeurMesureChimie vmcNH4 = valeurMesureChimieDAO.getByVariableCodeAndProfondeurAndProjetCodeAndDateAndPlateformeCode(ConstantsChimie.KEY_VARIABLE_NH4, mesureChimie.getProfondeurMin(), mesureChimie.getProfondeurMax(), mesureChimie
                .getSousSequence().getSequence().getProjetSite().getProjet().getCode(), mesureChimie.getSousSequence().getSequence().getDatePrelevement(), mesureChimie.getSousSequence().getPlateforme().getCode());
        ValeurMesureChimie vmcNO3 = valeurMesureChimieDAO.getByVariableCodeAndProfondeurAndProjetCodeAndDateAndPlateformeCode(ConstantsChimie.KEY_VARIABLE_NO3, mesureChimie.getProfondeurMin(), mesureChimie.getProfondeurMax(), mesureChimie
                .getSousSequence().getSequence().getProjetSite().getProjet().getCode(), mesureChimie.getSousSequence().getSequence().getDatePrelevement(), mesureChimie.getSousSequence().getPlateforme().getCode());
        ValeurMesureChimie vmcNO2 = valeurMesureChimieDAO.getByVariableCodeAndProfondeurAndProjetCodeAndDateAndPlateformeCode(ConstantsChimie.KEY_VARIABLE_NO2, mesureChimie.getProfondeurMin(), mesureChimie.getProfondeurMax(), mesureChimie
                .getSousSequence().getSequence().getProjetSite().getProjet().getCode(), mesureChimie.getSousSequence().getSequence().getDatePrelevement(), mesureChimie.getSousSequence().getPlateforme().getCode());
        ValeurMesureChimie vmcPO4 = valeurMesureChimieDAO.getByVariableCodeAndProfondeurAndProjetCodeAndDateAndPlateformeCode(ConstantsChimie.KEY_VARIABLE_PO4, mesureChimie.getProfondeurMin(), mesureChimie.getProfondeurMax(), mesureChimie
                .getSousSequence().getSequence().getProjetSite().getProjet().getCode(), mesureChimie.getSousSequence().getSequence().getDatePrelevement(), mesureChimie.getSousSequence().getPlateforme().getCode());
        ValeurMesureChimie vmcSO4 = valeurMesureChimieDAO.getByVariableCodeAndProfondeurAndProjetCodeAndDateAndPlateformeCode(ConstantsChimie.KEY_VARIABLE_SO4, mesureChimie.getProfondeurMin(), mesureChimie.getProfondeurMax(), mesureChimie
                .getSousSequence().getSequence().getProjetSite().getProjet().getCode(), mesureChimie.getSousSequence().getSequence().getDatePrelevement(), mesureChimie.getSousSequence().getPlateforme().getCode());
        ValeurMesureChimie vmcTAC = valeurMesureChimieDAO.getByVariableCodeAndProfondeurAndProjetCodeAndDateAndPlateformeCode(ConstantsChimie.KEY_VARIABLE_TAC, mesureChimie.getProfondeurMin(), mesureChimie.getProfondeurMax(), mesureChimie
                .getSousSequence().getSequence().getProjetSite().getProjet().getCode(), mesureChimie.getSousSequence().getSequence().getDatePrelevement(), mesureChimie.getSousSequence().getPlateforme().getCode());

        if (vmcCA != null && vmcK != null && vmcCL != null && vmcMG != null && vmcNA != null && vmcNH4 != null && vmcNO3 != null && vmcNO2 != null && vmcPO4 != null && vmcSO4 != null && vmcTAC != null) {
            Float sumCation = ((2 * vmcCA.getValue() / 40.1f) + (2 * vmcMG.getValue() / 24.3f) + (vmcK.getValue() / 39.1f) + (vmcNA.getValue() / 23f) + (vmcNH4.getValue() / 14f));
            Float sumAnion = ((vmcCL.getValue() / 35.5f) + (2 * vmcSO4.getValue() / 96.1f) + (vmcNO2.getValue() / 14.0f) + (vmcNO3.getValeur() / 14) + vmcTAC.getValue() + (3 * vmcPO4.getValue() / 31.0f));

            balanceIonique = 100f * (sumCation - sumAnion) / (sumCation + sumAnion);
        }

        if (balanceIonique != null) {
            balanceIoniqueString = String.format("%.3f%%", balanceIonique);
        }

        return balanceIoniqueString;
    }

    private Map<Long, ValeurMesureChimie> buildValeurs(List<ValeurMesureChimie> valeurs) {
        Map<Long, ValeurMesureChimie> mapValue = new HashMap<Long, ValeurMesureChimie>();
        for (ValeurMesureChimie valeur : valeurs) {
            mapValue.put(valeur.getVariable().getId(), valeur);
        }
        return mapValue;
    }

    private String retrieveValidDateString(Date dateCampagne) {
        if (dateCampagne == null) {
            return "";
        } else {
            return dateFormatter.format(dateCampagne);
        }
    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException, NoExtractionResultException {
        ((DefaultParameter) parameters).getFilesMaps().add(super.buildOutput(parameters, ChimieRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE));
        return null;
    }

}
