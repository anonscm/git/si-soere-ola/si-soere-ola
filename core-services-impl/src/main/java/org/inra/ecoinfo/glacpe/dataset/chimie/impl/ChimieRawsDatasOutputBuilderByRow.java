package org.inra.ecoinfo.glacpe.dataset.chimie.impl;

import java.io.File;
import java.io.PrintStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.glacpe.dataset.chimie.ConstantsChimie;
import org.inra.ecoinfo.glacpe.dataset.chimie.entity.MesureChimie;
import org.inra.ecoinfo.glacpe.dataset.chimie.entity.ValeurMesureChimie;
import org.inra.ecoinfo.glacpe.extraction.vo.DepthRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO;
import org.inra.ecoinfo.glacpe.extraction.vo.VariableVO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.ErrorsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import com.google.common.base.Strings;

/**
 *
 * @author ptcherniati
 */
public class ChimieRawsDatasOutputBuilderByRow extends AbstractChimieRawsDatasOutputBuilder {

    private static final String MSG_BALANCE_IONIQUE_KEY_NOT_FOUND_IN_DB = "PROPERTY_MSG_BALANCE_IONIQUE_KEY_NOT_FOUND_IN_DB";
    private static final String HEADER_RAW_DATA_ALLDEPTH = "PROPERTY_MSG_HEADER_RAW_ALLDEPTH_LINE";
    private static final String HEADER_RAW_DATA = "PROPERTY_MSG_HEADER_RAW_LINE";
    private static final String HEADER_RAW_DATA_RIVER = "PROPERTY_MSG_HEADER_RAW_RIVER_LINE";
    private static final String MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS = "PROPERTY_MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS";
    private static final String PROPERTY_MSG_ERROR = "PROPERTY_MSG_ERROR";

    private static final String CODE_DATATYPE_CHIMIE = "physico_chimie";
    private static final String CODE_TYPE_SITE_RIVIERE = "riviere";
    private static final String CODE_TYPE_OUTIL_MESURE = "mesure";

    private static String KEY_VARIABLE_BALANCE_IONIQUE = ConstantsChimie.KEY_VARIABLE_BALANCE_IONIQUE;
    private Map<String, Float> variablesMap = new HashMap();
    private Map<String, String> variablesHeaderMap = new HashMap();

    @SuppressWarnings("unchecked")
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatas) throws BusinessException {
        initVariablesMap();
        Properties propertiesVariableName = localizationManager.newProperties(Variable.NAME_ENTITY_JPA, "nom");
        String typeSite = ((List<PlateformeVO>) requestMetadatas.get(PlateformeVO.class.getSimpleName())).get(0).getProjet().getType();
        ErrorsReport errorsReport = new ErrorsReport(localizationManager.getMessage(BUNDLE_SOURCE_PATH_CHIMIE, PROPERTY_MSG_ERROR));
        List<VariableVO> selectedVariables = (List<VariableVO>) requestMetadatas.get(VariableVO.class.getSimpleName());
        DepthRequestParamVO depthRequestParamVO = (DepthRequestParamVO) requestMetadatas.get(DepthRequestParamVO.class.getSimpleName());

        StringBuilder stringBuilder = new StringBuilder();

        if (!typeSite.equals(CODE_TYPE_SITE_RIVIERE)) {
            if (depthRequestParamVO.getAllDepth()) {
                stringBuilder.append(String.format("%s", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_CHIMIE, HEADER_RAW_DATA_ALLDEPTH)));
            } else {
                stringBuilder.append(String.format("%s", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_CHIMIE, HEADER_RAW_DATA)));
            }
        } else {
            stringBuilder.append(String.format("%s", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_CHIMIE, HEADER_RAW_DATA_RIVER)));
        }
        for (VariableVO variableVO : selectedVariables) {
            VariableGLACPE variable;
            String uniteNom;
            try {
                variable = (VariableGLACPE) variableDAO.getByCode(variableVO.getCode());
                if (variable == null) {
                    errorsReport.addException(new BusinessException(String.format(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_CHIMIE, MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS), selectedVariables)));
                }
                String localizedVariableName = propertiesVariableName.getProperty(variable.getNom());
                if (Strings.isNullOrEmpty(localizedVariableName)) {
                    localizedVariableName = variable.getNom();
                }

                Unite unite = datatypeVariableUniteDAO.getUnite(CODE_DATATYPE_CHIMIE, variable.getCode());
                if (unite == null) {
                    uniteNom = "nounit";
                } else {
                    uniteNom = unite.getCode();
                }

                if (!variablesMap.containsKey(variable.getCode())) {
                    stringBuilder.append(String.format(";%s (%s)", localizedVariableName, uniteNom));
                } else {
                    stringBuilder.append(String.format(";%s (%s);%s", localizedVariableName, uniteNom, variablesHeaderMap.get(variable.getCode())));
                }
                if (errorsReport.hasErrors()) {
                    throw new PersistenceException(errorsReport.buildHTMLMessages());
                }
            } catch (PersistenceException e) {
                throw new BusinessException(e);
            }
        }
        return stringBuilder.toString();

    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {
        try {

            List<VariableVO> selectedVariables = (List<VariableVO>) requestMetadatasMap.get(VariableVO.class.getSimpleName());
            DepthRequestParamVO depthRequestParamVO = (DepthRequestParamVO) requestMetadatasMap.get(DepthRequestParamVO.class.getSimpleName());

            Iterator<MesureChimie> mesuresChimies = resultsDatasMap.get(MAP_INDEX_0).iterator();

            Properties propertiesProjetName = localizationManager.newProperties(Projet.NAME_TABLE, "nom");
            Properties propertiesPlateformeName = localizationManager.newProperties(Plateforme.TABLE_NAME, "nom");
            Properties propertiesSiteName = localizationManager.newProperties(Site.NAME_ENTITY_JPA, "nom");

            VariableGLACPE balanceIoniqueVariable = (VariableGLACPE) variableDAO.getByCode(KEY_VARIABLE_BALANCE_IONIQUE);

            if (balanceIoniqueVariable == null) {
                throw new BusinessException(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_CHIMIE, MSG_BALANCE_IONIQUE_KEY_NOT_FOUND_IN_DB));
            }

            List<PlateformeVO> selectedPlateformes = (List<PlateformeVO>) requestMetadatasMap.get(PlateformeVO.class.getSimpleName());
            String typeSite = selectedPlateformes.get(0).getProjet().getType();
            Set<String> sitesNames = retrieveSitesCodes(selectedPlateformes);
            Map<String, File> filesMap = buildOutputsFiles(sitesNames, SUFFIX_FILENAME_DEFAULT);
            Map<String, PrintStream> outputPrintStreamMap = buildOutputPrintStreamMap(filesMap);
            for (String siteName : outputPrintStreamMap.keySet()) {
                outputPrintStreamMap.get(siteName).println(headers);
            }
            while (mesuresChimies.hasNext()) {
                final MesureChimie mesureChimie = mesuresChimies.next();

                PrintStream rawDataPrintStream = outputPrintStreamMap.get(((SiteGLACPE) mesureChimie.getSite()).getCodeFromName());
                String line;

                String localizedProjetName = propertiesProjetName.getProperty(mesureChimie.getSousSequence().getSequence().getProjetSite().getProjet().getNom());
                if (Strings.isNullOrEmpty(localizedProjetName)) {
                    localizedProjetName = mesureChimie.getSousSequence().getSequence().getProjetSite().getProjet().getNom();
                }

                String localizedSiteName = propertiesSiteName.getProperty(mesureChimie.getSousSequence().getSequence().getProjetSite().getSite().getNom());
                if (Strings.isNullOrEmpty(localizedSiteName)) {
                    localizedSiteName = mesureChimie.getSousSequence().getSequence().getProjetSite().getSite().getNom();
                }

                String localizedPlateformeName = propertiesPlateformeName.getProperty(mesureChimie.getSousSequence().getPlateforme().getNom());
                if (Strings.isNullOrEmpty(localizedPlateformeName)) {
                    localizedPlateformeName = mesureChimie.getSousSequence().getPlateforme().getNom();
                }

                if (!typeSite.equals(CODE_TYPE_SITE_RIVIERE)) {
                    if (depthRequestParamVO.getAllDepth()) {
                        line = String.format("%s;%s;%s;%s;%s;%s;%s;%s;%s", localizedProjetName, localizedSiteName, localizedPlateformeName, dateFormatter.format(mesureChimie.getSousSequence().getSequence().getDatePrelevement()), mesureChimie
                                .getSousSequence().getOutilsMesure().getTypeOutilsMesure().getType().getValeur().equals(CODE_TYPE_OUTIL_MESURE) ? "" : mesureChimie.getSousSequence().getOutilsMesure().getNom(), mesureChimie.getSousSequence()
                                .getOutilsMesure().getTypeOutilsMesure().getType().getValeur().equals(CODE_TYPE_OUTIL_MESURE) ? mesureChimie.getSousSequence().getOutilsMesure().getNom() : "", mesureChimie.getProfondeurMin(),
                                mesureChimie.getProfondeurMax(), mesureChimie.getProfondeurReelle() == null ? "" : mesureChimie.getProfondeurReelle());
                    } else {
                        line = String.format("%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s", localizedProjetName, localizedSiteName, localizedPlateformeName, depthRequestParamVO.getDepthMin(), depthRequestParamVO.getDepthMax(), dateFormatter.format(mesureChimie
                                .getSousSequence().getSequence().getDatePrelevement()), mesureChimie.getSousSequence().getOutilsMesure().getTypeOutilsMesure().getType().getValeur().equals(CODE_TYPE_OUTIL_MESURE) ? "" : mesureChimie.getSousSequence()
                                .getOutilsMesure().getNom(),
                                mesureChimie.getSousSequence().getOutilsMesure().getTypeOutilsMesure().getType().getValeur().equals(CODE_TYPE_OUTIL_MESURE) ? mesureChimie.getSousSequence().getOutilsMesure().getNom() : "", mesureChimie
                                .getProfondeurMin(), mesureChimie.getProfondeurMax(), mesureChimie.getProfondeurReelle() == null ? "" : mesureChimie.getProfondeurReelle());
                    }
                } else {
                    line = String.format("%s;%s;%s;%s;%s;%s;%s;%s", localizedProjetName, localizedSiteName, localizedPlateformeName, retrieveValidDateString(mesureChimie.getSousSequence().getSequence().getDateDebutCampagne()),
                            retrieveValidDateString(mesureChimie.getSousSequence().getSequence().getDateFinCampagne()), retrieveValidDateString(mesureChimie.getSousSequence().getSequence().getDateReception()), mesureChimie.getSousSequence()
                            .getOutilsMesure().getTypeOutilsMesure().getType().getValeur().equals(CODE_TYPE_OUTIL_MESURE) ? "" : mesureChimie.getSousSequence().getOutilsMesure().getNom(), mesureChimie.getSousSequence().getOutilsMesure()
                            .getTypeOutilsMesure().getType().getValeur().equals(CODE_TYPE_OUTIL_MESURE) ? mesureChimie.getSousSequence().getOutilsMesure().getNom() : "");
                }

                // rawDataPrintStream.print(line);
                Map<Long, Float> valeursMesuresChimie = buildValeurs(mesureChimie.getValeurs());
                Boolean isLineEmpty = true;
                for (VariableVO variableVO : selectedVariables) {
                    // rawDataPrintStream.print(";");
                    line = line.concat(";");
                    if (!variableVO.getCode().equals(KEY_VARIABLE_BALANCE_IONIQUE)) {
                        Float valeur = valeursMesuresChimie.get(variableVO.getId());
                        if (valeur != null) {
                            if (!variablesMap.containsKey(variableVO.getCode())) {
                                line = line.concat(String.format("%s", valeur));
                            } else {
                                line = line.concat(String.format("%s;%.5f", valeur, valeur * variablesMap.get(variableVO.getCode())));
                            }
                            isLineEmpty = false;
                        } else {
                            if (variablesMap.containsKey(variableVO.getCode())) {
                                line = line.concat(";");
                            }
                            // rawDataPrintStream.print("");
                        }

                    } else {
                        line = line.concat(String.format("%s", processBalanceIonique(mesureChimie)));
                    }
                }

                if (!isLineEmpty) {
                    rawDataPrintStream.print(line);
                    rawDataPrintStream.println();
                }
//                mesuresC himies.remove();
            }

            closeStreams(outputPrintStreamMap);
            return filesMap;
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    private void initVariablesMap() {
        if (variablesMap.isEmpty()) {

            variablesMap.put(ConstantsChimie.KEY_VARIABLE_NH4, 1.2878f);
            variablesMap.put(ConstantsChimie.KEY_VARIABLE_NO3, 4.4268f);
            variablesMap.put(ConstantsChimie.KEY_VARIABLE_NO2, 3.28443f);
            variablesMap.put(ConstantsChimie.KEY_VARIABLE_PO4, 3.065f);
        }

        if (variablesHeaderMap.isEmpty()) {
            variablesHeaderMap.put(ConstantsChimie.KEY_VARIABLE_NO3, "Nitrates(Azote nitrique) [mg(NO3)/l]");
            variablesHeaderMap.put(ConstantsChimie.KEY_VARIABLE_NH4, "Ammonium(Azote ammoniacal) [mg(NH4)/l]");
            variablesHeaderMap.put(ConstantsChimie.KEY_VARIABLE_NO2, "Nitrites(Azote Nitreux) [mg(NO2)/l]");
            variablesHeaderMap.put(ConstantsChimie.KEY_VARIABLE_PO4, "Phosphore[mg(P)/l]");
        }
    }

    private Map<Long, Float> buildValeurs(List<ValeurMesureChimie> valeurs) {
        Map<Long, Float> mapValue = new HashMap<Long, Float>();
        for (ValeurMesureChimie valeur : valeurs) {
            mapValue.put(valeur.getVariable().getId(), valeur.getValeur());
        }
        return mapValue;
    }

    private String processBalanceIonique(MesureChimie mesureChimie) throws PersistenceException {

        Float balanceIonique = null;
        String balanceIoniqueString = "";

        ValeurMesureChimie vmcCA = valeurMesureChimieDAO.getByVariableCodeAndProfondeurAndProjetCodeAndDateAndPlateformeCode(ConstantsChimie.KEY_VARIABLE_CA, mesureChimie.getProfondeurMin(), mesureChimie.getProfondeurMax(), mesureChimie
                .getSousSequence().getSequence().getProjetSite().getProjet().getCode(), mesureChimie.getSousSequence().getSequence().getDatePrelevement(), mesureChimie.getSousSequence().getPlateforme().getCode());
        ValeurMesureChimie vmcK = valeurMesureChimieDAO.getByVariableCodeAndProfondeurAndProjetCodeAndDateAndPlateformeCode(ConstantsChimie.KEY_VARIABLE_K, mesureChimie.getProfondeurMin(), mesureChimie.getProfondeurMax(), mesureChimie
                .getSousSequence().getSequence().getProjetSite().getProjet().getCode(), mesureChimie.getSousSequence().getSequence().getDatePrelevement(), mesureChimie.getSousSequence().getPlateforme().getCode());
        ValeurMesureChimie vmcCL = valeurMesureChimieDAO.getByVariableCodeAndProfondeurAndProjetCodeAndDateAndPlateformeCode(ConstantsChimie.KEY_VARIABLE_CL, mesureChimie.getProfondeurMin(), mesureChimie.getProfondeurMax(), mesureChimie
                .getSousSequence().getSequence().getProjetSite().getProjet().getCode(), mesureChimie.getSousSequence().getSequence().getDatePrelevement(), mesureChimie.getSousSequence().getPlateforme().getCode());
        ValeurMesureChimie vmcMG = valeurMesureChimieDAO.getByVariableCodeAndProfondeurAndProjetCodeAndDateAndPlateformeCode(ConstantsChimie.KEY_VARIABLE_MG, mesureChimie.getProfondeurMin(), mesureChimie.getProfondeurMax(), mesureChimie
                .getSousSequence().getSequence().getProjetSite().getProjet().getCode(), mesureChimie.getSousSequence().getSequence().getDatePrelevement(), mesureChimie.getSousSequence().getPlateforme().getCode());
        ValeurMesureChimie vmcNA = valeurMesureChimieDAO.getByVariableCodeAndProfondeurAndProjetCodeAndDateAndPlateformeCode(ConstantsChimie.KEY_VARIABLE_NA, mesureChimie.getProfondeurMin(), mesureChimie.getProfondeurMax(), mesureChimie
                .getSousSequence().getSequence().getProjetSite().getProjet().getCode(), mesureChimie.getSousSequence().getSequence().getDatePrelevement(), mesureChimie.getSousSequence().getPlateforme().getCode());
        ValeurMesureChimie vmcNH4 = valeurMesureChimieDAO.getByVariableCodeAndProfondeurAndProjetCodeAndDateAndPlateformeCode(ConstantsChimie.KEY_VARIABLE_NH4, mesureChimie.getProfondeurMin(), mesureChimie.getProfondeurMax(), mesureChimie
                .getSousSequence().getSequence().getProjetSite().getProjet().getCode(), mesureChimie.getSousSequence().getSequence().getDatePrelevement(), mesureChimie.getSousSequence().getPlateforme().getCode());
        ValeurMesureChimie vmcNO3 = valeurMesureChimieDAO.getByVariableCodeAndProfondeurAndProjetCodeAndDateAndPlateformeCode(ConstantsChimie.KEY_VARIABLE_NO3, mesureChimie.getProfondeurMin(), mesureChimie.getProfondeurMax(), mesureChimie
                .getSousSequence().getSequence().getProjetSite().getProjet().getCode(), mesureChimie.getSousSequence().getSequence().getDatePrelevement(), mesureChimie.getSousSequence().getPlateforme().getCode());
        ValeurMesureChimie vmcNO2 = valeurMesureChimieDAO.getByVariableCodeAndProfondeurAndProjetCodeAndDateAndPlateformeCode(ConstantsChimie.KEY_VARIABLE_NO2, mesureChimie.getProfondeurMin(), mesureChimie.getProfondeurMax(), mesureChimie
                .getSousSequence().getSequence().getProjetSite().getProjet().getCode(), mesureChimie.getSousSequence().getSequence().getDatePrelevement(), mesureChimie.getSousSequence().getPlateforme().getCode());
        ValeurMesureChimie vmcPO4 = valeurMesureChimieDAO.getByVariableCodeAndProfondeurAndProjetCodeAndDateAndPlateformeCode(ConstantsChimie.KEY_VARIABLE_PO4, mesureChimie.getProfondeurMin(), mesureChimie.getProfondeurMax(), mesureChimie
                .getSousSequence().getSequence().getProjetSite().getProjet().getCode(), mesureChimie.getSousSequence().getSequence().getDatePrelevement(), mesureChimie.getSousSequence().getPlateforme().getCode());
        ValeurMesureChimie vmcSO4 = valeurMesureChimieDAO.getByVariableCodeAndProfondeurAndProjetCodeAndDateAndPlateformeCode(ConstantsChimie.KEY_VARIABLE_SO4, mesureChimie.getProfondeurMin(), mesureChimie.getProfondeurMax(), mesureChimie
                .getSousSequence().getSequence().getProjetSite().getProjet().getCode(), mesureChimie.getSousSequence().getSequence().getDatePrelevement(), mesureChimie.getSousSequence().getPlateforme().getCode());
        ValeurMesureChimie vmcTAC = valeurMesureChimieDAO.getByVariableCodeAndProfondeurAndProjetCodeAndDateAndPlateformeCode(ConstantsChimie.KEY_VARIABLE_TAC, mesureChimie.getProfondeurMin(), mesureChimie.getProfondeurMax(), mesureChimie
                .getSousSequence().getSequence().getProjetSite().getProjet().getCode(), mesureChimie.getSousSequence().getSequence().getDatePrelevement(), mesureChimie.getSousSequence().getPlateforme().getCode());

        if (vmcCA != null && vmcK != null && vmcCL != null && vmcMG != null && vmcNA != null && vmcNH4 != null && vmcNO3 != null && vmcNO2 != null && vmcPO4 != null && vmcSO4 != null && vmcTAC != null) {
            Float sumCation = ((2 * vmcCA.getValue() / 40.1f) + (2 * vmcMG.getValue() / 24.3f) + (vmcK.getValue() / 39.1f) + (vmcNA.getValue() / 23f) + (vmcNH4.getValue() / 14f));
            Float sumAnion = ((vmcCL.getValue() / 35.5f) + (2 * vmcSO4.getValue() / 96.1f) + (vmcNO2.getValue() / 14.0f) + (vmcNO3.getValeur() / 14) + vmcTAC.getValue() + (3 * vmcPO4.getValue() / 31.0f));

            balanceIonique = 100f * (sumCation - sumAnion) / (sumCation + sumAnion);
        }

        if (balanceIonique != null) {
            balanceIoniqueString = String.format("%.3f%%", balanceIonique);
        }

        return balanceIoniqueString;
    }

    private String retrieveValidDateString(Date dateCampagne) {
        if (dateCampagne == null) {
            return "";
        } else {
            return dateFormatter.format(dateCampagne);
        }
    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException, NoExtractionResultException {
        ((DefaultParameter) parameters).getFilesMaps().add(super.buildOutput(parameters, ChimieRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE));
        return null;

    }

}
