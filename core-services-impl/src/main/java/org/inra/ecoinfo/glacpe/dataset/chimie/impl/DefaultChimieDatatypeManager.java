package org.inra.ecoinfo.glacpe.dataset.chimie.impl;

import java.util.Date;
import java.util.List;

import org.inra.ecoinfo.MO;
import org.inra.ecoinfo.glacpe.dataset.chimie.ConstantsChimie;
import org.inra.ecoinfo.glacpe.dataset.chimie.IChimieDAO;
import org.inra.ecoinfo.glacpe.dataset.chimie.IChimieDatatypeManager;
import org.inra.ecoinfo.glacpe.extraction.vo.GroupeVariableVO;
import org.inra.ecoinfo.glacpe.extraction.vo.VariableVO;
import org.inra.ecoinfo.glacpe.refdata.groupevariable.IGroupeVariableBuilder;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.projetsite.ProjetSite;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ptcherniati
 */
public class DefaultChimieDatatypeManager extends MO implements IChimieDatatypeManager, BeanFactoryAware {

    /**
     *
     */
    protected IChimieDAO chimieDAO;

    /**
     *
     */
    protected BeanFactory beanFactory;

    /**
     *
     */
    protected IVariableDAO variableDAO;

    /**
     *
     * @param projetId
     * @param siteId
     * @return
     * @throws BusinessException
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public ProjetSite retrieveChimieAvailablesProjetsSiteByProjetAndSite(Long projetId, Long siteId) throws BusinessException {
        try {
            return chimieDAO.retrieveChimieAvailablesProjetsSiteByProjetAndSite(projetId, siteId);
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    /**
     *
     * @return
     * @throws BusinessException
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public List<Projet> retrieveChimieAvailablesProjets() throws BusinessException {
        try {
            return chimieDAO.retrieveChimieAvailablesProjets();
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }

    }

    /**
     *
     * @param projetSiteId
     * @return
     * @throws BusinessException
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public List<Plateforme> retrieveChimieAvailablesPlateformesByProjetSiteId(long projetSiteId) throws BusinessException {
        try {
            return chimieDAO.retrieveChimieAvailablesPlateformesByProjetSiteId(projetSiteId);
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }

    }

    /**
     *
     * @param plateformesIds
     * @param firstDate
     * @param lastDate
     * @return
     * @throws BusinessException
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public List<GroupeVariableVO> retrieveGroupesVariables(List<Long> plateformesIds, Date firstDate, Date lastDate) throws BusinessException {
        try {
            List<VariableGLACPE> variables = chimieDAO.retrieveChimieAvailablesVariables(plateformesIds, firstDate, lastDate);
            if (!variables.isEmpty()) {
                VariableGLACPE variableBalanceIonique = (VariableGLACPE) variableDAO.getByCode(ConstantsChimie.KEY_VARIABLE_BALANCE_IONIQUE);
                if (!variables.contains(variableBalanceIonique)) {
                    variables.add(variableBalanceIonique);
                }
            }
            List<GroupeVariableVO> groupesVariables = getGroupeVariableBuilder().build(variables);
            return groupesVariables;
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    /**
     *
     * @param groupesVariables
     * @param tabs
     */
    protected void print(List<GroupeVariableVO> groupesVariables, String tabs) {
        tabs = tabs + "   ";
        for (GroupeVariableVO groupeVariable : groupesVariables) {
            System.out.println(tabs + groupeVariable.getCode());

            if (groupeVariable.getChildren().size() > 0) {
                print(groupeVariable.getChildren(), tabs);
            } else {
                for (VariableVO variable : groupeVariable.getVariables()) {
                    System.out.println(tabs + "   " + "variable:" + variable.getCode());
                }
            }
        }
    }

    /**
     * Cette méthode récupère le bean groupeVariableBuilder en scope prototype car l'injection d'un bean de scope prototype dans un bean de scope singleton ne fonctionne pas;
     * 
     * @return
     */
    private IGroupeVariableBuilder getGroupeVariableBuilder() {
        return (IGroupeVariableBuilder) beanFactory.getBean(IGroupeVariableBuilder.BEAN_ID);

    }

    /**
     *
     * @param chimieDAO
     */
    public void setChimieDAO(IChimieDAO chimieDAO) {
        this.chimieDAO = chimieDAO;
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        this.beanFactory = beanFactory;

    }

    /**
     *
     * @param variableDAO
     */
    public void setVariableDAO(IVariableDAO variableDAO) {
        this.variableDAO = variableDAO;
    }

}
