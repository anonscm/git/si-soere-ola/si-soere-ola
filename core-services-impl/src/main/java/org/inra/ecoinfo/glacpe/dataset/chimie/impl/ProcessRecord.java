package org.inra.ecoinfo.glacpe.dataset.chimie.impl;

import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_DOUBLON_LINE;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_DUPLICATE_SEQUENCE;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_ERROR;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_ERROR_PLATEFORME_INVALID;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_ERROR_PLATEFORME_SITE_MISMATCH;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_FLOAT_VALUE_EXPECTED;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_MISSING_OUTILS_MESURE_IN_REFERENCES_DATAS;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_MISSING_PROJET_SITE_IN_REFERENCES_DATAS;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.getGLACPEMessage;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.getGLACPEMessageWithBundle;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.hibernate.exception.ConstraintViolationException;
import org.inra.ecoinfo.dataset.exception.InsertionDatabaseException;
import org.inra.ecoinfo.dataset.exception.PersistenceConstraintException;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.glacpe.dataset.chimie.ISequenceChimieDAO;
import org.inra.ecoinfo.glacpe.dataset.chimie.entity.MesureChimie;
import org.inra.ecoinfo.glacpe.dataset.chimie.entity.SequenceChimie;
import org.inra.ecoinfo.glacpe.dataset.chimie.entity.SousSequenceChimie;
import org.inra.ecoinfo.glacpe.dataset.chimie.entity.ValeurMesureChimie;
import org.inra.ecoinfo.glacpe.dataset.impl.AbstractProcessRecord;
import org.inra.ecoinfo.glacpe.dataset.impl.CleanerValues;
import org.inra.ecoinfo.glacpe.refdata.controlecoherence.ControleCoherence;
import org.inra.ecoinfo.glacpe.refdata.controlecoherence.IControleCoherenceDAO;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.IOutilsMesureDAO;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.OutilsMesure;
import org.inra.ecoinfo.glacpe.refdata.plateforme.IPlateformeDAO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projetsite.IProjetSiteDAO;
import org.inra.ecoinfo.glacpe.refdata.projetsite.ProjetSite;
import org.inra.ecoinfo.glacpe.refdata.projetsitethemedatatype.ProjetSiteThemeDatatype;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.system.Allocator;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.DatasetDescriptorBuilder;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.ErrorsReport;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.springframework.transaction.TransactionStatus;
import org.xml.sax.SAXException;

import com.Ostermiller.util.CSVParser;
import com.google.common.collect.Lists;

/**
 *
 * @author ptcherniati
 */
public class ProcessRecord extends AbstractProcessRecord {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private static final String BUNDLE_SOURCE_PATH_CHIMIE = "org.inra.ecoinfo.glacpe.dataset.chimie.messages";

    private static final String MSG_CONFLICT_BD_SEQUENCE_CHIMIE = "PROPERTY_MSG_CONFLICT_BD_SEQUENCE_CHIMIE";

    private static final String PROPERTY_MSG_MISSING_DATE_PRELEVEMENT = "PROPERTY_MSG_MISSING_DATE_PRELEVEMENT";

    private static final String CODE_PROJET_SUIVI_RIVIERE = "suivi_des_rivieres";

    /**
     *
     */
    protected IControleCoherenceDAO controleCoherenceDAO;

    /**
     *
     */
    protected ISequenceChimieDAO sequenceChimieDAO;

    /**
     *
     */
    protected IProjetSiteDAO projetSiteDAO;

    /**
     *
     */
    protected IPlateformeDAO plateformeDAO;

    /**
     *
     */
    protected IOutilsMesureDAO outilsMesureDAO;
    
    /**
     *
     * @return
     * @throws IOException
     * @throws SAXException
     */
    @Override
    public DatasetDescriptor holdDatasetDescriptor() throws IOException, SAXException {
        // Here we use the ISequenceChimieDAO class in order to access the dataset-descriptor.xml because they are in the same package and above all the this.getClass().getResource("../dataset-descriptor.xml") dont work!!
        if (datasetDescriptor == null) {
            datasetDescriptor = DatasetDescriptorBuilder.buildDescriptor(ISequenceChimieDAO.class.getResource(DATASET_DESCRIPTOR_XML).openStream());
        }
        return datasetDescriptor;
    }

    @Override
    public void processRecord(CSVParser parser, VersionFile iVersionFile, String fileEncoding, DatasetDescriptor datasetDescriptor) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport(getGLACPEMessage(PROPERTY_MSG_ERROR));
        Utils.testNotNullArguments(getLocalizationManager(), iVersionFile, fileEncoding);                        
        Utils.testCastedArguments(iVersionFile, VersionFile.class, getLocalizationManager());
        VersionFile versionFile = (VersionFile) iVersionFile;

        try {
            Map<String, ControleCoherence> controlesCoherenceMap = controleCoherenceDAO.getControleCoherenceBySitecodeAndDatatypecode(((ProjetSiteThemeDatatype) versionFile.getDataset().getLeafNode()).getProjetSiteTheme().getProjetSite().getSite()
                    .getCode(), versionFile.getDataset().getLeafNode().getDatatype().getCode());
            Map<String, VariableGLACPE> measuredVariablesMap = new HashMap<String, VariableGLACPE>();

            String[] values = null;
            long lineCount = 1;
            // Saute la 1ere ligne
            parser.getLine();
            Map<String, List<LineRecord>> sequencesMapLines = new HashMap<String, List<LineRecord>>();

            String projetCode;
            String nomSite;
            String plateformeCode;
            Date datePrelevement;
            Date dateDebutCampagne;
            Date dateFinCampagne;
            Date dateReception;
            Float profondeurMin;
            Float profondeurMax;
            Float profondeurReelle;
            String outilPrelevementCode;
            String variableCode;
            VariableGLACPE variable;
            String value = null;
            // String flag = null;
            // On parcourt chaque ligne du fichier
            while ((values = parser.getLine()) != null) {

                CleanerValues cleanerValues = new CleanerValues(values);
                lineCount++;

                // On parcourt chaque colonne d'une ligne
                projetCode = Utils.createCodeFromString(cleanerValues.nextToken());
                nomSite = cleanerValues.nextToken();
                plateformeCode = Utils.createCodeFromString(cleanerValues.nextToken());
                datePrelevement = readDate(cleanerValues, datasetDescriptor);
                dateDebutCampagne = readDate(cleanerValues, datasetDescriptor);
                dateFinCampagne = readDate(cleanerValues, datasetDescriptor);
                dateReception = readDate(cleanerValues, datasetDescriptor);
                outilPrelevementCode = Utils.createCodeFromString(cleanerValues.nextToken());
                profondeurMin = readDepth(cleanerValues);
                profondeurMax = readDepth(cleanerValues);
                profondeurReelle = readDepth(cleanerValues);
                variableCode = Utils.createCodeFromString(cleanerValues.nextToken());
                // flag = cleanerValues.nextToken();

                variable = retrieveVariable(measuredVariablesMap, variableCode, errorsReport);
                value = readValue(errorsReport, controlesCoherenceMap, lineCount, variableCode, value, cleanerValues);
                datePrelevement = retrieveSpecificDatePrelevement(projetCode, datePrelevement, dateFinCampagne, lineCount, errorsReport);

                LineRecord line = new LineRecord(nomSite, plateformeCode, datePrelevement, dateDebutCampagne, dateFinCampagne, dateReception, outilPrelevementCode, profondeurMin, profondeurMax, profondeurReelle, variableCode, value, lineCount,
                        projetCode, variable);
                if (datePrelevement != null) {
                    String sequenceKey = projetCode.concat(Utils.createCodeFromString(nomSite)).concat(datePrelevement.toString());
                    fillLinesMap(sequencesMapLines, line, sequenceKey);
                }

            }
            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.buildHTMLMessages());
            }
            persistLines(errorsReport, versionFile, sequencesMapLines);

            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.buildHTMLMessages());
            }
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        } catch (ParseException e) {
            // d'où vient cette erreur
            throw new BusinessException(e);
        } catch (IOException e) {
            // d'où vient cette erreur
            throw new BusinessException(e);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            throw new BusinessException(e);
        }

    }

    private Float readDepth(CleanerValues cleanerValues) {
        String profondeurString = cleanerValues.nextToken();
        return profondeurString.length() > 0 ? Float.parseFloat(profondeurString) : null;
    }

    private String readValue(ErrorsReport errorsReport, Map<String, ControleCoherence> controlesCoherenceMap, long lineCount, String variableCode, String value, CleanerValues cleanerValues) {
        try {
            value = cleanerValues.nextToken();
            if (controlesCoherenceMap.get(variableCode) != null && value != null)
                testValueCoherence(Float.valueOf(value), controlesCoherenceMap.get(variableCode).getValeurMin(), controlesCoherenceMap.get(variableCode).getValeurMax(), lineCount, 10);
        } catch (NumberFormatException e) {
            errorsReport.addException(new BadExpectedValueException(String.format(getGLACPEMessage(PROPERTY_MSG_FLOAT_VALUE_EXPECTED), lineCount, 13, value)));
        } catch (BadExpectedValueException e) {
            errorsReport.addException(e);
        }
        return value;
    }

    private Date readDate(CleanerValues cleanerValues, DatasetDescriptor datasetDescriptor) throws ParseException {
        String dateString;
        Date date;
        dateString = cleanerValues.nextToken();
        date = dateString.length() > 0 ? new SimpleDateFormat(datasetDescriptor.getColumns().get(cleanerValues.currentTokenIndex() - 1).getFormatType()).parse(dateString) : null;
        return date;
    }

    private VariableGLACPE retrieveVariable(Map<String, VariableGLACPE> measuredVariablesMap, String variableCode, org.inra.ecoinfo.utils.ErrorsReport errorReport) throws PersistenceException {
        VariableGLACPE variable = null;

        if (!measuredVariablesMap.containsKey(variableCode)) {
            variable = (VariableGLACPE) variableDAO.getByCode(variableCode);
            if (variable == null) {
                errorReport.addException(new BusinessException(String.format(getGLACPEMessage(PROPERTY_MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS), variableCode)));
            }
            measuredVariablesMap.put(variableCode, variable);
        } else {
            variable = measuredVariablesMap.get(variableCode);
        }

        return variable;
    }

    private Date retrieveSpecificDatePrelevement(String projetCode, Date datePrelevement, Date dateFinCampagne, long lineCount, ErrorsReport errorsReport) {
        if (datePrelevement == null) {
            if (projetCode.equals(CODE_PROJET_SUIVI_RIVIERE)) {
                datePrelevement = dateFinCampagne;
            } else {
                errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(PROPERTY_MSG_MISSING_DATE_PRELEVEMENT), lineCount)));
            }
        }
        return datePrelevement;
    }

    private void testConflictsSequenceChimie(Date datePrelevement, Long projetSiteId, VersionFile versionFile, boolean debugMode) throws PersistenceException, InsertionDatabaseException {
        SequenceChimie sequenceChimie;
        if (debugMode) {
            sequenceChimie = sequenceChimieDAO.getByDatePrelevementAndProjetSiteId(datePrelevement, projetSiteId);
            if (sequenceChimie != null) {
                throw new PersistenceConstraintException(String.format(getGLACPEMessageWithBundle(BUNDLE_SOURCE_PATH_CHIMIE, MSG_CONFLICT_BD_SEQUENCE_CHIMIE), datePrelevement.toString(), versionFile.getDataset().getDateDebutPeriode().toString()
                        .concat("-").concat(versionFile.getDataset().getDateFinPeriode().toString())));
            }
        }
    }

    private void fillLinesMap(Map<String, List<LineRecord>> linesMap, LineRecord line, String keyLine) {
        List<LineRecord> sousSequenceLines = null;
        if (keyLine != null && keyLine.trim().length() > 0) {
            if (linesMap.containsKey(keyLine)) {
                linesMap.get(keyLine).add(line);
            } else {
                sousSequenceLines = new LinkedList<LineRecord>();
                sousSequenceLines.add(line);
                linesMap.put(keyLine, sousSequenceLines);
            }
        }
    }

    private void persistLines(ErrorsReport errorsReport, VersionFile versionFile, Map<String, List<LineRecord>> sequencesMapLines) throws PersistenceException, InterruptedException {
        Allocator allocator = Allocator.getInstance();
        String projetCode;
        String siteCode;
        Date datePrelevement;
        int count = 0;
        allocator.allocate("publish", versionFile.getFileSize());
        Boolean debugMode = false;
        Iterator<Entry<String, List<LineRecord>>> iterator = sequencesMapLines.entrySet().iterator();
        while (iterator.hasNext()) {
            final Entry<String, List<LineRecord>> entry = iterator.next();
            List<LineRecord> sequenceLines = sequencesMapLines.get(entry.getKey());
            if (!sequenceLines.isEmpty()) {
                datePrelevement = sequenceLines.get(0).getDatePrelevement();
                projetCode = sequenceLines.get(0).getProjetCode();
                siteCode = Utils.createCodeFromString(sequenceLines.get(0).getNomSite());

                debugMode = processSequence(errorsReport, versionFile, debugMode, sequenceLines, datePrelevement, projetCode, siteCode);
                logger.debug(String.format("%d - %s", count++, datePrelevement));
                sequenceChimieDAO.flush();
                versionFile = versionFileDAO.merge(versionFile);
            }
            // Très important à cause de problèmes de performances
            iterator.remove();
        }
    }

    private Boolean processSequence(ErrorsReport errorsReport, VersionFile versionFile, Boolean debugMode, List<LineRecord> sequenceLines, Date datePrelevement, String projetCode, String siteCode) throws PersistenceException {
        try {
            processSequenceInNormalMode(errorsReport, versionFile, debugMode, sequenceLines, datePrelevement, projetCode, siteCode);
        } catch (InsertionDatabaseException e) {
            errorsReport.addException(e);

        } catch (Exception e) {
            debugMode = processSequenceInDebugMode(errorsReport, versionFile, debugMode, sequenceLines, datePrelevement, projetCode, siteCode, e);
        } finally {
            if (!debugMode) {
                versionFileDAO.flush();
                versionFile = versionFileDAO.merge(versionFile);
            }
        }
        return debugMode;

    }

    private void processSequenceInNormalMode(ErrorsReport errorsReport, VersionFile versionFile, Boolean debugMode, List<LineRecord> sequenceLines, Date datePrelevement, String projetCode, String siteCode) throws PersistenceException,
            InsertionDatabaseException
    {
        if (debugMode) {
            TransactionStatus transactionStatus = versionFileDAO.beginNewTransaction();
            buildSequence(datePrelevement, projetCode, siteCode, sequenceLines, versionFile, errorsReport, false);
            versionFileDAO.rollbackTransaction(transactionStatus);
        } else {
            buildSequence(datePrelevement, projetCode, siteCode, sequenceLines, versionFile, errorsReport, false);
        }
    }

    private Boolean processSequenceInDebugMode(ErrorsReport errorsReport, VersionFile versionFile, Boolean debugMode, List<LineRecord> sequenceLines, Date datePrelevement, String projetCode, String siteCode, Exception e) throws PersistenceException {
        if (Utils.matchExceptionCause(e, ConstraintViolationException.class)) {
            debugMode = new Boolean(true);
            TransactionStatus transactionStatus = versionFileDAO.beginNewTransaction();
            try {
                buildSequence(datePrelevement, projetCode, siteCode, sequenceLines, versionFile, errorsReport, true);
            } catch (PersistenceConstraintException e1) {
                errorsReport.addException(e1);
            } catch (InsertionDatabaseException e1) {
                // déjà catché plus haut
            }
            versionFileDAO.rollbackTransaction(transactionStatus);
        } else {
            throw new PersistenceException(e);
        }
        return debugMode;
    }

    private void buildSequence(Date datePrelevement, String projetCode, String siteCode, List<LineRecord> sequenceLines, VersionFile versionFile, ErrorsReport errorsReport, boolean debugMode) throws PersistenceException, InsertionDatabaseException {

        LineRecord firstLine = sequenceLines.get(0);

        SequenceChimie sequenceChimie = new SequenceChimie();

        ProjetSite projetSite = projetSiteDAO.getBySiteCodeAndProjetCode(siteCode, projetCode);
        if (projetSite == null) {
            throw new InsertionDatabaseException(String.format(getGLACPEMessage(PROPERTY_MSG_MISSING_PROJET_SITE_IN_REFERENCES_DATAS), projetCode, siteCode));
        }

        testConflictsSequenceChimie(datePrelevement, projetSite.getId(), versionFile, debugMode);

        sequenceChimie.setProjetSite(projetSite);
        sequenceChimie.setDatePrelevement(datePrelevement);
        sequenceChimie.setDateDebutCampagne(firstLine.getDateDebutCampagne());
        sequenceChimie.setDateFinCampagne(firstLine.getDateFinCampagne());
        sequenceChimie.setDateReception(firstLine.getDateReception());

        sequenceChimie.setVersionFile(versionFile);

        Map<String, List<LineRecord>> sousSequencesLinesMap = new HashMap<String, List<LineRecord>>();

        for (LineRecord line : sequenceLines) {
            fillLinesMap(sousSequencesLinesMap, line, line.getPlateformeCode().concat(line.getOutilPrelevementCode()));
        }

        for (String plateformeCode : sousSequencesLinesMap.keySet()) {
            try {
                List<LineRecord> mesureLines = sousSequencesLinesMap.get(plateformeCode);
                buildSousSequence(mesureLines.get(0).getPlateformeCode(), mesureLines, mesureLines.get(0).getOutilPrelevementCode(), sequenceChimie, errorsReport);
            } catch (InsertionDatabaseException e) {
                errorsReport.addException(e);
            }
        }
        if (!errorsReport.hasErrors()) {
            try {
                sequenceChimieDAO.saveOrUpdate(sequenceChimie);
            } catch (ConstraintViolationException e) {
                String message = String.format(getGLACPEMessage(PROPERTY_MSG_DUPLICATE_SEQUENCE), DateUtil.getSimpleDateFormatDateyyyyMMddUTC().format(datePrelevement));
                errorsReport.addException(new BusinessException(message));
            }
        }
    }

    private void buildSousSequence(String plateformeCode, List<LineRecord> sousSequenceLines, String outilPrelevementCode, SequenceChimie sequenceChimie, ErrorsReport errorsReport) throws PersistenceException, InsertionDatabaseException {

        SousSequenceChimie sousSequenceChimie = new SousSequenceChimie();

        LineRecord firstLine = sousSequenceLines.get(0);

        Plateforme plateforme = null;
        plateforme = plateformeDAO.getByNKey(plateformeCode, Utils.createCodeFromString(firstLine.getNomSite()));
        if (plateforme == null) {
            InsertionDatabaseException insertionDatabaseException = new InsertionDatabaseException(String.format(getGLACPEMessage(PROPERTY_MSG_ERROR_PLATEFORME_INVALID), plateformeCode, firstLine.getOriginalLineNumber()));
            throw insertionDatabaseException;
        }

        if (!plateforme.getSite().getCodeFromName().equals(Utils.createCodeFromString(firstLine.getNomSite()))) {
            InsertionDatabaseException insertionDatabaseException = new InsertionDatabaseException(String.format(getGLACPEMessage(PROPERTY_MSG_ERROR_PLATEFORME_SITE_MISMATCH), plateforme.getNom(), firstLine.getOriginalLineNumber(),
                    firstLine.getNomSite()));
            throw insertionDatabaseException;
        }

        OutilsMesure outilPrelevement = outilsMesureDAO.getByCode(outilPrelevementCode);
        if (outilPrelevement == null) {
            throw new InsertionDatabaseException(String.format(getGLACPEMessage(PROPERTY_MSG_MISSING_OUTILS_MESURE_IN_REFERENCES_DATAS), outilPrelevementCode));
        }

        sousSequenceChimie.setPlateforme(plateforme);
        sousSequenceChimie.setOutilsMesure(outilPrelevement);
        sousSequenceChimie.setSequence(sequenceChimie);
        sequenceChimie.getSousSequences().add(sousSequenceChimie);

        Map<String, List<LineRecord>> mesuresMap = new HashMap<String, List<LineRecord>>();

        for (LineRecord line : sousSequenceLines) {
            String key = line.getProjetCode().equals(CODE_PROJET_SUIVI_RIVIERE) ? "0" : line.getProfondeurMax().toString();
            fillLinesMap(mesuresMap, line, key);
        }

        for (String profondeur : mesuresMap.keySet()) {
            try {
                List<LineRecord> mesureLines = mesuresMap.get(profondeur);
                buildMesure(Float.parseFloat(profondeur), mesureLines.get(0).getProfondeurMin(), mesureLines.get(0).getProfondeurReelle(), mesureLines, sousSequenceChimie, errorsReport);

            } catch (InsertionDatabaseException e) {
                errorsReport.addException(e);
            }
        }
    }

    private void buildMesure(Float profondeurMax, Float profondeurMin, Float profondeurReelle, List<LineRecord> profondeurLines, SousSequenceChimie sousSequenceChimie, ErrorsReport errorsReport) throws PersistenceException,
            InsertionDatabaseException
    {

        List<String> variableName = Lists.newArrayList();
        for (LineRecord profondeurLine : profondeurLines) {
            if (!variableName.contains(profondeurLine.getVariableCode())) {
                variableName.add(profondeurLine.getVariableCode());
            } else {
                errorsReport.addException(new PersistenceException(String.format(getGLACPEMessage(PROPERTY_MSG_DOUBLON_LINE), profondeurLine.getOriginalLineNumber())));
            }
        }

        MesureChimie mesureChimie = new MesureChimie();
        mesureChimie.setSousSequence(sousSequenceChimie);
        sousSequenceChimie.getMesures().add(mesureChimie);
        mesureChimie.setProfondeurMin(profondeurMin);
        mesureChimie.setProfondeurMax(profondeurMax);
        mesureChimie.setProfondeurReelle(profondeurReelle);

        for (LineRecord profondeurLine : profondeurLines) {

            ValeurMesureChimie valeurMesureChimie = new ValeurMesureChimie();

            VariableGLACPE variable = (VariableGLACPE) variableDAO.merge(profondeurLine.getVariable());

            if (variable == null) {
                String errorMessage = String.format(getGLACPEMessage(PROPERTY_MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS), profondeurLine.getVariableCode());
                throw new InsertionDatabaseException(errorMessage);
            }

            valeurMesureChimie.setVariable(variable);

            if (profondeurLine.getValue() == null || profondeurLine.getValue().length() == 0) {
                valeurMesureChimie.setValeur(null);
            } else {
                Float value = Float.parseFloat(profondeurLine.getValue());
                valeurMesureChimie.setValeur(value);
            }

            valeurMesureChimie.setMesure(mesureChimie);
            mesureChimie.getValeurs().add(valeurMesureChimie);
        }

    }

    /**
     *
     * @param controleCoherenceDAO
     */
    public void setControleCoherenceDAO(IControleCoherenceDAO controleCoherenceDAO) {
        this.controleCoherenceDAO = controleCoherenceDAO;
    }

    /**
     *
     * @param sequenceChimieDAO
     */
    public void setSequenceChimieDAO(ISequenceChimieDAO sequenceChimieDAO) {
        this.sequenceChimieDAO = sequenceChimieDAO;
    }

    /**
     *
     * @param projetSiteDAO
     */
    public void setProjetSiteDAO(IProjetSiteDAO projetSiteDAO) {
        this.projetSiteDAO = projetSiteDAO;
    }

    /**
     *
     * @param plateformeDAO
     */
    public void setPlateformeDAO(IPlateformeDAO plateformeDAO) {
        this.plateformeDAO = plateformeDAO;
    }

    /**
     *
     * @param outilsMesureDAO
     */
    public void setOutilsMesureDAO(IOutilsMesureDAO outilsMesureDAO) {
        this.outilsMesureDAO = outilsMesureDAO;
    }
}
