/**
 * OREILacs project - see LICENCE.txt for use created: 13 août 2009 14:33:43
 */
package org.inra.ecoinfo.glacpe.dataset.chimie.jpa;

import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.glacpe.dataset.chimie.ISequenceChimieDAO;
import org.inra.ecoinfo.glacpe.dataset.chimie.entity.SequenceChimie;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public class JPASequenceChimieDAO extends AbstractJPADAO<SequenceChimie> implements ISequenceChimieDAO {

    /**
     *
     * @param datePrelevement
     * @param projetSiteId
     * @return
     * @throws PersistenceException
     */
    @Override
    public SequenceChimie getByDatePrelevementAndProjetSiteId(Date datePrelevement, Long projetSiteId) throws PersistenceException {
        try {
            Query query = null;

            String queryString = "from SequenceChimie s where s.datePrelevement = :date and s.projetSite.id = :id";

            query = entityManager.createQuery(queryString);
            query.setParameter("date", datePrelevement);
            query.setParameter("id", projetSiteId);

            List<?> results = query.getResultList();
            if (results != null && results.size() == 1) {
                return (SequenceChimie) results.get(0);
            }

        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
        return null;
    }

}
