package org.inra.ecoinfo.glacpe.dataset.chimie.jpa;

import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.glacpe.dataset.chimie.IValeurMesureChimieDAO;
import org.inra.ecoinfo.glacpe.dataset.chimie.entity.ValeurMesureChimie;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class JPAValeurMesureChimieDAO extends AbstractJPADAO<ValeurMesureChimie> implements IValeurMesureChimieDAO {

    private static String REQUEST_GET_BY_VARIABLE_CODE_AND_PROFONDEUR_AND_PROJET_CODE_AND_DATE_AND_PLATEFORME_CODE = "from ValeurMesureChimie vmc where vmc.variable.code = :variableCode and vmc.mesure.profondeurMin = :profondeurMin and vmc.mesure.profondeurMax = :profondeurMax and vmc.mesure.sousSequence.sequence.projetSite.projet.code = :projetCode and vmc.mesure.sousSequence.sequence.datePrelevement = :datePrelevement and vmc.mesure.sousSequence.plateforme.code = :plateformeCode";

    /**
     *
     * @param variableCode
     * @param profondeurMin
     * @param profondeurMax
     * @param projetCode
     * @param datePrelevement
     * @param plateformeCode
     * @return
     * @throws PersistenceException
     */
    @Override
    public ValeurMesureChimie getByVariableCodeAndProfondeurAndProjetCodeAndDateAndPlateformeCode(String variableCode, Float profondeurMin, Float profondeurMax, String projetCode, Date datePrelevement, String plateformeCode)
            throws PersistenceException
    {
        try {
            Query query = null;

            query = entityManager.createQuery(REQUEST_GET_BY_VARIABLE_CODE_AND_PROFONDEUR_AND_PROJET_CODE_AND_DATE_AND_PLATEFORME_CODE);
            query.setParameter("variableCode", variableCode);
            query.setParameter("profondeurMin", profondeurMin);
            query.setParameter("profondeurMax", profondeurMax);
            query.setParameter("projetCode", projetCode);
            query.setParameter("datePrelevement", datePrelevement);
            query.setParameter("plateformeCode", plateformeCode);

            List<?> results = query.getResultList();
            if (results != null && results.size() == 1) {
                return (ValeurMesureChimie) results.get(0);
            }

            return null;
        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

}
