package org.inra.ecoinfo.glacpe.dataset.chloro;

import java.util.Date;
import java.util.List;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.glacpe.dataset.chloro.entity.MesureChloro;
import org.inra.ecoinfo.glacpe.dataset.chloro.entity.ValeurMesureChloro;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesRequestParamVO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public interface IChlorophylleDAO extends IDAO<Object> {

    /**
     *
     * @return
     * @throws PersistenceException
     */
    List<Site> retrieveAvailablesSites() throws PersistenceException;

    /**
     *
     * @param id
     * @return
     * @throws PersistenceException
     */
    List<Plateforme> retrieveAvailablesPlateformesByProjetSiteId(Long id) throws PersistenceException;

    /**
     *
     * @param plateformesIds
     * @param firstDate
     * @param endDate
     * @return
     * @throws PersistenceException
     */
    List<VariableGLACPE> retrieveAvailablesVariables(List<Long> plateformesIds, Date firstDate, Date endDate) throws PersistenceException;

    /**
     *
     * @return
     * @throws PersistenceException
     */
    List<Projet> retrieveAvailablesProjets() throws PersistenceException;

    /**
     *
     * @param plateformesIds
     * @param selectedProjetSiteIds
     * @param datesRequestParamVO
     * @return
     * @throws PersistenceException
     */
    List<MesureChloro> extractDatasForAllDepth(List<Long> plateformesIds, List<Long> selectedProjetSiteIds, DatesRequestParamVO datesRequestParamVO) throws PersistenceException;

    /**
     *
     * @param plateformesIds
     * @param selectedProjetSiteIds
     * @param datesRequestParamVO
     * @param dephtMin
     * @param depthMax
     * @return
     * @throws PersistenceException
     */
    List<MesureChloro> extractDatasForRangeDepth(List<Long> plateformesIds, List<Long> selectedProjetSiteIds, DatesRequestParamVO datesRequestParamVO, Float dephtMin, Float depthMax) throws PersistenceException;

    /**
     *
     * @param plateformesIds
     * @param selectedProjetSiteIds
     * @param datesRequestParamVO
     * @param variablesId
     * @return
     * @throws PersistenceException
     */
    List<ValeurMesureChloro> extractAggregatedDatasForAllDepth(List<Long> plateformesIds, List<Long> selectedProjetSiteIds, DatesRequestParamVO datesRequestParamVO, List<Long> variablesId) throws PersistenceException;

    /**
     *
     * @param plateformesIds
     * @param selectedProjetSiteIds
     * @param datesRequestParamVO
     * @param variablesId
     * @param dephtMin
     * @param dephtMax
     * @return
     * @throws PersistenceException
     */
    List<ValeurMesureChloro> extractAggregatedDatasForRangeDepth(List<Long> plateformesIds, List<Long> selectedProjetSiteIds, DatesRequestParamVO datesRequestParamVO, List<Long> variablesId, Float dephtMin, Float dephtMax)
            throws PersistenceException;

}
