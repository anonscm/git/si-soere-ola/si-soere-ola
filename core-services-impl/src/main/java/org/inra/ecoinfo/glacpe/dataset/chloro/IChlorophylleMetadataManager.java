package org.inra.ecoinfo.glacpe.dataset.chloro;

import java.text.DateFormat;
import java.util.List;

import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public interface IChlorophylleMetadataManager {

    /**
     *
     * @param variable
     * @return
     * @throws BusinessException
     */
    List<String> retrieveAllowedSitesNamesByVariable(String variable) throws BusinessException;

    /**
     *
     * @param siteSelected
     * @param variableSelected
     * @param startString
     * @param endString
     * @param dateFormatter
     * @param datatType
     * @return
     * @throws BusinessException
     */
    String commentDateInRange(String siteSelected, String variableSelected, String startString, String endString, DateFormat dateFormatter, String datatType) throws BusinessException;

}
