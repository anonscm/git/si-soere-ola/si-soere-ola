package org.inra.ecoinfo.glacpe.dataset.chloro;

import java.util.Date;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.glacpe.dataset.chloro.entity.SequenceChloro;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public interface ISequenceChloroDAO extends IDAO<SequenceChloro> {

    /**
     *
     * @param date
     * @param plateformeCode
     * @param siteCode
     * @return
     * @throws PersistenceException
     */
    SequenceChloro getByDatePrelevementAndProjetCodeAndSiteCode(Date date, String plateformeCode, String siteCode) throws PersistenceException;

}
