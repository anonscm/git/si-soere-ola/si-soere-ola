package org.inra.ecoinfo.glacpe.dataset.chloro.impl;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.AbstractExtractor;
import org.inra.ecoinfo.glacpe.dataset.chloro.IChlorophylleDAO;
import org.inra.ecoinfo.glacpe.dataset.chloro.entity.ValeurMesureChloro;
import org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEAggregatedDatasExtractor;
import org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.impl.DefaultPPChloroTranspExtractor;
import org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.impl.PPChloroTranspParameter;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.DepthRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO;
import org.inra.ecoinfo.glacpe.extraction.vo.VariableVO;
import org.inra.ecoinfo.glacpe.refdata.projetsite.ProjetSite;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class ChloroAggregatedDatasExtractor extends AbstractGLACPEAggregatedDatasExtractor {

    private static final String MAP_INDEX_0 = "0";

    /**
     *
     */
    public static String CST_RESULT_EXTRACTION_CODE = "chloroAggregatedDatas";

    private static final String PROPERTY_MSG_NO_RESULT_EXTRACT = "PROPERTY_MSG_NO_RESULT_EXTRACT";

    /**
     *
     */
    protected IChlorophylleDAO chloroDAO;

    /**
     *
     * @param chloroDAO
     */
    public void setChloroDAO(IChlorophylleDAO chloroDAO) {
        this.chloroDAO = chloroDAO;
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, List> extractDatas(Map<String, Object> requestMetadatas) throws NoExtractionResultException, BusinessException {

        Map<String, List> extractedDatasMap = new HashMap<String, List>();
        DepthRequestParamVO depthRequestParamVO = (DepthRequestParamVO) requestMetadatas.get(DepthRequestParamVO.class.getSimpleName());
        List<PlateformeVO> selectedPlateformes = (List<PlateformeVO>) requestMetadatas.get(PlateformeVO.class.getSimpleName());
        List<Long> selectedProjetSiteIds = (List<Long>) requestMetadatas.get(ProjetSite.class.getSimpleName());
        Map<String, List<VariableVO>> selectedsVariables = (Map<String, List<VariableVO>>) requestMetadatas.get(VariableVO.class.getSimpleName());
        List<VariableVO> selectedsChloroVariables = selectedsVariables.get(PPChloroTranspParameter.CHLOROPHYLLE);
        List<Long> plateformesIds = buildPlateformesIds(selectedPlateformes);
        List<Long> variablesIds = buildVariablesIds(selectedsChloroVariables);
        DatesRequestParamVO datesRequestParamVO = (DatesRequestParamVO) requestMetadatas.get(DatesRequestParamVO.class.getSimpleName());

        List<ValeurMesureChloro> valeursMesuresChloro = null;
        try {
            if (depthRequestParamVO.getAllDepth()) {
                valeursMesuresChloro = chloroDAO.extractAggregatedDatasForAllDepth(plateformesIds, selectedProjetSiteIds, datesRequestParamVO, variablesIds);
            } else {
                valeursMesuresChloro = chloroDAO.extractAggregatedDatasForRangeDepth(plateformesIds, selectedProjetSiteIds, datesRequestParamVO, variablesIds, depthRequestParamVO.getDepthMin(), depthRequestParamVO.getDepthMax());
            }
            extractedDatasMap.put(ChloroAggregatedDatasExtractor.CST_RESULT_EXTRACTION_CODE, valeursMesuresChloro);

            if (valeursMesuresChloro == null || valeursMesuresChloro.size() == 0) {
                throw new NoExtractionResultException(getLocalizationManager().getMessage(NoExtractionResultException.BUNDLE_SOURCE_PATH, PROPERTY_MSG_NO_RESULT_EXTRACT));
            }

        } catch (PersistenceException e) {
            AbstractExtractor.LOGGER.error(e.getMessage());
            throw new BusinessException(e);
        }
        return extractedDatasMap;
    }

    private List<Long> buildPlateformesIds(List<PlateformeVO> selectedPlateformes) {
        List<Long> plateformesIds = new LinkedList<Long>();
        for (PlateformeVO plateforme : selectedPlateformes) {
            plateformesIds.add(plateforme.getId());
        }
        return plateformesIds;
    }

    private List<Long> buildVariablesIds(List<VariableVO> selectedVariables) {
        List<Long> variablesIds = new LinkedList<Long>();
        if (selectedVariables == null)
            return variablesIds;
        for (VariableVO variable : selectedVariables) {
            variablesIds.add(variable.getId());
        }
        return variablesIds;
    }

    @SuppressWarnings("rawtypes")
    @Override
    protected Map<String, List> filterExtractedDatas(Map<String, List> resultsDatasMap) throws BusinessException {
        filterExtractedValues(resultsDatasMap, CST_RESULT_EXTRACTION_CODE);
        return resultsDatasMap;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void prepareRequestMetadatas(Map<String, Object> requestMetadatasMap) throws BusinessException {
        Collections.sort(((Map<String, List<VariableVO>>) requestMetadatasMap.get(VariableVO.class.getSimpleName())).get(PPChloroTranspParameter.CHLOROPHYLLE), new Comparator<VariableVO>() {

            @Override
            public int compare(VariableVO o1, VariableVO o2) {
                return o1.getId().toString().compareTo(o2.getId().toString());
            }
        });
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public void extract(IParameter parameters) throws BusinessException {
        prepareRequestMetadatas(parameters.getParameters());
        Map<String, List> resultsDatasMap = extractDatas(parameters.getParameters());
        Map<String, List> filteredResultsDatasMap = filterExtractedDatas(resultsDatasMap);
        if (((PPChloroTranspParameter) parameters).getResults().get(CST_RESULT_EXTRACTION_CODE) == null || ((PPChloroTranspParameter) parameters).getResults().get(CST_RESULT_EXTRACTION_CODE).isEmpty())
            ((PPChloroTranspParameter) parameters).getResults().put(CST_RESULT_EXTRACTION_CODE, filteredResultsDatasMap);
        else
            ((PPChloroTranspParameter) parameters).getResults().get(CST_RESULT_EXTRACTION_CODE).put(CST_RESULT_EXTRACTION_CODE, filteredResultsDatasMap.get(CST_RESULT_EXTRACTION_CODE));
        if (((PPChloroTranspParameter) parameters).getParameters().get(DefaultPPChloroTranspExtractor.CST_RESULTS) == null) {
            Map<String, Boolean> resultTrack = new HashMap<String, Boolean>();
            resultTrack.put(CST_RESULT_EXTRACTION_CODE, filteredResultsDatasMap.get(CST_RESULT_EXTRACTION_CODE) != null);
            ((PPChloroTranspParameter) parameters).getParameters().put(DefaultPPChloroTranspExtractor.CST_RESULTS, resultTrack);
        } else {
            ((Map<String, Boolean>) ((PPChloroTranspParameter) parameters).getParameters().get(DefaultPPChloroTranspExtractor.CST_RESULTS)).put(CST_RESULT_EXTRACTION_CODE, filteredResultsDatasMap.get(CST_RESULT_EXTRACTION_CODE) != null);
        }
        ((PPChloroTranspParameter) parameters).getResults().get(CST_RESULT_EXTRACTION_CODE).put(MAP_INDEX_0, filteredResultsDatasMap.get(CST_RESULT_EXTRACTION_CODE));
    }
}
