package org.inra.ecoinfo.glacpe.dataset.chloro.impl;

import java.io.File;
import java.io.PrintStream;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.glacpe.dataset.IGLACPEAggregateData;
import org.inra.ecoinfo.glacpe.dataset.VariableAggregatedDatas;
import org.inra.ecoinfo.glacpe.dataset.VariableAggregatedDatas.ValueAggregatedData;
import org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEAggregatedDatasOutputBuilder;
import org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.impl.PPChloroTranspParameter;
import org.inra.ecoinfo.glacpe.extraction.vo.DatasRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.DepthRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO;
import org.inra.ecoinfo.glacpe.extraction.vo.ProjetSiteVO;
import org.inra.ecoinfo.glacpe.extraction.vo.VariableVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.IDatatypeVariableUniteGLACPEDAO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import com.google.common.base.Strings;

/**
 *
 * @author ptcherniati
 */
public class ChloroAggregatedDatasOutputBuilder extends AbstractGLACPEAggregatedDatasOutputBuilder {

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException, NoExtractionResultException {
        ((DefaultParameter) parameters).getFilesMaps().add(super.buildOutput(parameters, ChloroAggregatedDatasExtractor.CST_RESULT_EXTRACTION_CODE));
        return null;
    }

    private static final String BUNDLE_SOURCE_PATH_CHLORO = "org.inra.ecoinfo.glacpe.dataset.chloro.messages";

    /**
     *
     */
    protected static final String SUFFIX_FILENAME_AGGREGATED = "aggregated";
    private static final String CODE_DATATYPE_CHLOROPHYLLE = "chlorophylle";

    private static final String HEADER_RAW_DATA = "PROPERTY_MSG_HEADER_AGGREGATED_DATA";
    private static final String MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS = "PROPERTY_MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS";

    private static final String SUFFIX_FILENAME_CHLORO_AGGREGATED = SUFFIX_FILENAME_AGGREGATED.concat(SEPARATOR_TEXT).concat(CODE_DATATYPE_CHLOROPHYLLE);

    /**
     *
     */
    protected IVariableDAO variableDAO;

    /**
     *
     */
    protected IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO;

    /**
     *
     * @param variableDAO
     */
    public void setVariableDAO(IVariableDAO variableDAO) {
        this.variableDAO = variableDAO;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatas) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport();
        Properties propertiesVariableName = localizationManager.newProperties(Variable.NAME_ENTITY_JPA, "nom");
        Map<String, List<VariableVO>> selectedVariablesList = (Map<String, List<VariableVO>>) requestMetadatas.get(VariableVO.class.getSimpleName());

        List<VariableVO> selectedVariables = (List<VariableVO>) selectedVariablesList.get(PPChloroTranspParameter.CHLOROPHYLLE);
        DatasRequestParamVO datasRequestParamVO = (DatasRequestParamVO) requestMetadatas.get(DatasRequestParamVO.class.getSimpleName());

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format("%s", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_CHLORO, HEADER_RAW_DATA)));
        try {
            for (VariableVO variableVO : selectedVariables) {
                VariableGLACPE variable = (VariableGLACPE) variableDAO.getByCode(variableVO.getCode());
                String uniteNom;
                if (variable == null) {
                    errorsReport.addErrorMessage((String.format(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_CHLORO, MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS), selectedVariables)));
                }
                String localizedVariableName = propertiesVariableName.getProperty(variable.getNom());
                if (Strings.isNullOrEmpty(localizedVariableName))
                    localizedVariableName = variable.getNom();

                Unite unite = datatypeVariableUniteDAO.getUnite(CODE_DATATYPE_CHLOROPHYLLE, variable.getCode());
                if (unite == null) {
                    uniteNom = "nounit";
                } else {
                    uniteNom = unite.getCode();
                }

                if (datasRequestParamVO.getMinValueAndAssociatedDepth()) {
                    stringBuilder.append(String.format(";minimum(%s) (%s)", localizedVariableName, uniteNom));
                }
                if (datasRequestParamVO.getMaxValueAndAssociatedDepth()) {
                    stringBuilder.append(String.format(";maximum(%s) (%s)", localizedVariableName, uniteNom));
                }
            }
            if (errorsReport.hasErrors()) {
                throw new PersistenceException(errorsReport.getErrorsMessages());
            }
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
        return stringBuilder.toString();

    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {

        Map<String, List<VariableVO>> selectedVariablesList = (Map<String, List<VariableVO>>) requestMetadatasMap.get(VariableVO.class.getSimpleName());

        List<VariableVO> selectedVariables = (List<VariableVO>) selectedVariablesList.get(PPChloroTranspParameter.CHLOROPHYLLE);
        DatasRequestParamVO datasRequestParamVO = (DatasRequestParamVO) requestMetadatasMap.get(DatasRequestParamVO.class.getSimpleName());
        DepthRequestParamVO depthRequestParamVO = (DepthRequestParamVO) requestMetadatasMap.get(DepthRequestParamVO.class.getSimpleName());

        String depthMin = depthRequestParamVO.getAllDepth() ? "" : depthRequestParamVO.getDepthMin().toString();
        String depthMax = depthRequestParamVO.getAllDepth() ? "" : depthRequestParamVO.getDepthMax().toString();

        List<IGLACPEAggregateData> valeursMesures = resultsDatasMap.get(ChloroAggregatedDatasExtractor.CST_RESULT_EXTRACTION_CODE);

        Properties propertiesProjetName = localizationManager.newProperties(Projet.NAME_TABLE, "nom");
        Properties propertiesPlateformeName = localizationManager.newProperties(Plateforme.TABLE_NAME, "nom");
        Properties propertiesSiteName = localizationManager.newProperties(Site.NAME_ENTITY_JPA, "nom");

        String minSeparator = buildCSVSeparator(datasRequestParamVO.getMinValueAndAssociatedDepth());
        String maxSeparator = buildCSVSeparator(datasRequestParamVO.getMaxValueAndAssociatedDepth());

        List<PlateformeVO> selectedPlateformes = (List<PlateformeVO>) requestMetadatasMap.get(PlateformeVO.class.getSimpleName());
        ProjetSiteVO selectedProjet = selectedPlateformes.get(0).getProjet();
        Set<String> sitesNames = retrieveSitesCodes(selectedPlateformes);
        Map<String, File> filesMap = buildOutputsFiles(sitesNames, SUFFIX_FILENAME_CHLORO_AGGREGATED);
        Map<String, PrintStream> outputPrintStreamMap = buildOutputPrintStreamMap(filesMap);
        for (String siteName : outputPrintStreamMap.keySet()) {
            outputPrintStreamMap.get(siteName).println(headers);
        }

        Map<String, Map<Date, Map<String, VariableAggregatedDatas>>> variablesAggregatedDatas = buildVariablesAggregatedDatas(valeursMesures);

        for (String siteCode : variablesAggregatedDatas.keySet()) {
            List<Date> dateSet = asSortedList(variablesAggregatedDatas.get(siteCode).keySet());

            for (VariableVO variable : selectedVariables) {
                if (datasRequestParamVO.getMinValueAndAssociatedDepth()) {
                    for (Date dateKey : dateSet) {
                        VariableAggregatedDatas variableAggregatedDatas = null;
                        String localizedOutilPrelevementName = "";
                        String localizedOutilMesureName = "";

                        if (variablesAggregatedDatas.get(siteCode).get(dateKey).get(variable.getCode()) != null) {
                            variableAggregatedDatas = variablesAggregatedDatas.get(siteCode).get(dateKey).get(variable.getCode());
                        }
                        if (variableAggregatedDatas != null) {
                            PrintStream rawDataPrintStream = outputPrintStreamMap.get(Utils.createCodeFromString(variableAggregatedDatas.getSiteName()));

                            String localizedProjetName = propertiesProjetName.getProperty(selectedProjet.getNom());
                            if (Strings.isNullOrEmpty(localizedProjetName))
                                localizedProjetName = selectedProjet.getNom();

                            String localizedSiteName = propertiesSiteName.getProperty(variableAggregatedDatas.getSiteName());
                            if (Strings.isNullOrEmpty(localizedSiteName))
                                localizedSiteName = variableAggregatedDatas.getSiteName();

                            String localizedPlateformeName = propertiesPlateformeName.getProperty(variableAggregatedDatas.getPlateformeName());
                            if (Strings.isNullOrEmpty(localizedPlateformeName))
                                localizedPlateformeName = variableAggregatedDatas.getPlateformeName();

                            for (ValueAggregatedData value : variableAggregatedDatas.getMinAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey)) {
                                rawDataPrintStream.print(String.format("%s;%s;%s;", localizedProjetName, localizedSiteName, localizedPlateformeName));
                                rawDataPrintStream.print(String.format("%s;%s;%s;%s;%s;%s;", DateUtil.getSimpleDateFormatDateLocale().format(dateKey), localizedOutilPrelevementName, localizedOutilMesureName, depthMin, depthMax,
                                        value.getDepth() == null ? "" : value.getDepth()));

                                for (VariableVO variableVO : selectedVariables) {
                                    if (Utils.createCodeFromString(variableAggregatedDatas.getVariableName()).equals(variableVO.getCode())) {
                                        rawDataPrintStream.print(String.format("%s%s%s", value.getValue(), minSeparator, maxSeparator));
                                        break;
                                    } else {
                                        rawDataPrintStream.print(String.format("%s%s", minSeparator, maxSeparator));
                                    }
                                }
                                rawDataPrintStream.println();
                            }
                        }
                    }
                }
                if (datasRequestParamVO.getMaxValueAndAssociatedDepth()) {
                    for (Date dateKey : dateSet) {
                        VariableAggregatedDatas variableAggregatedDatas = null;
                        String localizedOutilPrelevementName = "";
                        String localizedOutilMesureName = "";

                        if (variablesAggregatedDatas.get(siteCode).get(dateKey).get(variable.getCode()) != null) {
                            variableAggregatedDatas = variablesAggregatedDatas.get(siteCode).get(dateKey).get(variable.getCode());
                        }
                        if (variableAggregatedDatas != null) {
                            PrintStream rawDataPrintStream = outputPrintStreamMap.get(Utils.createCodeFromString(variableAggregatedDatas.getSiteName()));

                            String localizedProjetName = propertiesProjetName.getProperty(selectedProjet.getNom());
                            if (Strings.isNullOrEmpty(localizedProjetName))
                                localizedProjetName = selectedProjet.getNom();

                            String localizedSiteName = propertiesSiteName.getProperty(variableAggregatedDatas.getSiteName());
                            if (Strings.isNullOrEmpty(localizedSiteName))
                                localizedSiteName = variableAggregatedDatas.getSiteName();

                            String localizedPlateformeName = propertiesPlateformeName.getProperty(variableAggregatedDatas.getPlateformeName());
                            if (Strings.isNullOrEmpty(localizedPlateformeName))
                                localizedPlateformeName = variableAggregatedDatas.getPlateformeName();

                            for (ValueAggregatedData value : variableAggregatedDatas.getMinAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey)) {
                                rawDataPrintStream.print(String.format("%s;%s;%s;", localizedProjetName, localizedSiteName, localizedPlateformeName));
                                rawDataPrintStream.print(String.format("%s;%s;%s;%s;%s;%s;", DateUtil.getSimpleDateFormatDateLocale().format(dateKey), localizedOutilPrelevementName, localizedOutilMesureName, depthMin, depthMax,
                                        value.getDepth() == null ? "" : value.getDepth()));

                                for (VariableVO variableVO : selectedVariables) {
                                    if (Utils.createCodeFromString(variableAggregatedDatas.getVariableName()).equals(variableVO.getCode())) {
                                        rawDataPrintStream.print(String.format("%s%s%s", minSeparator, value.getValue(), maxSeparator));
                                        break;
                                    } else {
                                        rawDataPrintStream.print(String.format("%s%s", minSeparator, maxSeparator));
                                    }
                                }
                                rawDataPrintStream.println();
                            }
                        }
                    }
                }
            }
        }
        closeStreams(outputPrintStreamMap);
        return filesMap;
    }

  

    /**
     *
     * @param <T>
     * @param c
     * @return
     */
    public static <T extends Comparable<? super T>> List<T> asSortedList(Collection<T> c) {
        List<T> list = new LinkedList<T>(c);
        Collections.sort(list);
        return list;
    }

    class ErrorsReport {

        private String errorsMessages = new String();

        public void addErrorMessage(String errorMessage) {
            errorsMessages = errorsMessages.concat("-").concat(errorMessage).concat("\n");
        }

        public String getErrorsMessages() {
            return errorsMessages;
        }

        public boolean hasErrors() {
            return (errorsMessages.length() > 0);
        }
    }

    /**
     *
     * @param datatypeVariableUniteDAO
     */
    public void setDatatypeVariableUniteDAO(IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO) {
        this.datatypeVariableUniteDAO = datatypeVariableUniteDAO;
    }
}
