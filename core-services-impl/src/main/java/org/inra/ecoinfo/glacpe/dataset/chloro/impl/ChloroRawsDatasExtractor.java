package org.inra.ecoinfo.glacpe.dataset.chloro.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.impl.DefaultPPChloroTranspExtractor;
import org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.impl.PPChloroTranspParameter;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public class ChloroRawsDatasExtractor extends AbstractChloroRawsDatasExtractor {

    /**
     *
     */
    public static String CST_RESULT_EXTRACTION_CODE = "chloroRawsDatas";

    @SuppressWarnings("rawtypes")
    @Override
    protected Map<String, List> filterExtractedDatas(Map<String, List> resultsDatasMap) throws BusinessException {
        filterExtractedDatas(resultsDatasMap, CST_RESULT_EXTRACTION_CODE);
        return resultsDatasMap;
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public void extract(IParameter parameters) throws BusinessException {
        prepareRequestMetadatas(parameters.getParameters());
        Map<String, List> resultsDatasMap = extractDatas(parameters.getParameters());
        Map<String, List> filteredResultsDatasMap = filterExtractedDatas(resultsDatasMap);
        if (((PPChloroTranspParameter) parameters).getResults().get(CST_RESULT_EXTRACTION_CODE) == null || ((PPChloroTranspParameter) parameters).getResults().get(CST_RESULT_EXTRACTION_CODE).isEmpty())
            ((PPChloroTranspParameter) parameters).getResults().put(CST_RESULT_EXTRACTION_CODE, filteredResultsDatasMap);
        else
            ((PPChloroTranspParameter) parameters).getResults().get(CST_RESULT_EXTRACTION_CODE).put(CST_RESULT_EXTRACTION_CODE, filteredResultsDatasMap.get(CST_RESULT_EXTRACTION_CODE));
        if (((PPChloroTranspParameter) parameters).getParameters().get(DefaultPPChloroTranspExtractor.CST_RESULTS) == null) {
            Map<String, Boolean> resultTrack = new HashMap<String, Boolean>();
            resultTrack.put(CST_RESULT_EXTRACTION_CODE, filteredResultsDatasMap.get(CST_RESULT_EXTRACTION_CODE) != null);
            ((PPChloroTranspParameter) parameters).getParameters().put(DefaultPPChloroTranspExtractor.CST_RESULTS, resultTrack);
        } else {
            ((Map<String, Boolean>) ((PPChloroTranspParameter) parameters).getParameters().get(DefaultPPChloroTranspExtractor.CST_RESULTS)).put(CST_RESULT_EXTRACTION_CODE, filteredResultsDatasMap.get(CST_RESULT_EXTRACTION_CODE) != null);
        }
        ((PPChloroTranspParameter) parameters).getResults().get(CST_RESULT_EXTRACTION_CODE).put(MAP_INDEX_0, filteredResultsDatasMap.get(CST_RESULT_EXTRACTION_CODE));
    }
}
