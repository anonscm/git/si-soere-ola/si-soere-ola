package org.inra.ecoinfo.glacpe.dataset.chloro.impl;

import java.io.File;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.glacpe.dataset.chloro.entity.MesureChloro;
import org.inra.ecoinfo.glacpe.dataset.chloro.entity.ValeurMesureChloro;
import org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.impl.AbstractPPChloroTranspRawsDatasOutputBuilder;
import org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.impl.PPChloroTranspParameter;
import org.inra.ecoinfo.glacpe.extraction.vo.DepthRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO;
import org.inra.ecoinfo.glacpe.extraction.vo.VariableVO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import com.google.common.base.Strings;

/**
 *
 * @author ptcherniati
 */
public class ChloroRawsDatasOutputBuilderByRow extends AbstractPPChloroTranspRawsDatasOutputBuilder {

    private static final String BUNDLE_SOURCE_PATH_CHLORO = "org.inra.ecoinfo.glacpe.dataset.chloro.messages";

    private static final String HEADER_RAW_DATA = "PROPERTY_MSG_HEADER_RAW_DATA";
    private static final String HEADER_RAW_DATA_ALLDEPTH = "PROPERTY_MSG_HEADER_RAW_DATA_ALLDEPTH";
    private static final String MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS = "PROPERTY_MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS";

    private static final String CODE_CHLOROPHYLLE = "chlorophylle";
    private static final String SUFFIX_FILENAME = CODE_CHLOROPHYLLE;

    @SuppressWarnings("unchecked")
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatas) throws BusinessException {

        Properties propertiesVariableName = localizationManager.newProperties(Variable.NAME_ENTITY_JPA, "nom");
        ErrorsReport errorsReport = new ErrorsReport();
        Map<String, List<VariableVO>> selectedVariablesList = (Map<String, List<VariableVO>>) requestMetadatas.get(VariableVO.class.getSimpleName());

        List<VariableVO> selectedVariables = (List<VariableVO>) selectedVariablesList.get(PPChloroTranspParameter.CHLOROPHYLLE);
        DepthRequestParamVO depthRequestParamVO = (DepthRequestParamVO) requestMetadatas.get(DepthRequestParamVO.class.getSimpleName());

        StringBuilder stringBuilder = new StringBuilder();
        try {
            if (depthRequestParamVO.getAllDepth()) {
                stringBuilder.append(String.format("%s", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_CHLORO, HEADER_RAW_DATA_ALLDEPTH)));
            } else {
                stringBuilder.append(String.format("%s", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_CHLORO, HEADER_RAW_DATA)));
            }

            for (VariableVO variableVO : selectedVariables) {
                VariableGLACPE variable = (VariableGLACPE) variableDAO.getByCode(variableVO.getCode());
                String uniteNom;
                if (variable == null) {
                    errorsReport.addErrorMessage((String.format(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_CHLORO, MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS), selectedVariables)));
                }
                String localizedVariableName = propertiesVariableName.getProperty(variable.getNom());
                if (Strings.isNullOrEmpty(localizedVariableName)) {
                    localizedVariableName = variable.getNom();
                }

                Unite unite = datatypeVariableUniteDAO.getUnite(CODE_CHLOROPHYLLE, variable.getCode());
                if (unite == null) {
                    uniteNom = "nounit";
                } else {
                    uniteNom = unite.getCode();
                }

                stringBuilder.append(String.format(";%s (%s)", localizedVariableName, uniteNom));
                if (errorsReport.hasErrors()) {
                    throw new PersistenceException(errorsReport.getErrorsMessages());
                }
            }
        } catch (Exception e) {
            throw new BusinessException(e);
        }
        return stringBuilder.toString();

    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {

        Map<String, List<VariableVO>> selectedVariablesList = (Map<String, List<VariableVO>>) requestMetadatasMap.get(VariableVO.class.getSimpleName());

        List<VariableVO> selectedVariables = (List<VariableVO>) selectedVariablesList.get(PPChloroTranspParameter.CHLOROPHYLLE);
        DepthRequestParamVO depthRequestParamVO = (DepthRequestParamVO) requestMetadatasMap.get(DepthRequestParamVO.class.getSimpleName());
        Iterator<MesureChloro> mesuresChloro = resultsDatasMap.get(ChloroRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE).iterator();

        Properties propertiesProjetName = localizationManager.newProperties(Projet.NAME_TABLE, "nom");
        Properties propertiesPlateformeName = localizationManager.newProperties(Plateforme.TABLE_NAME, "nom");
        Properties propertiesSiteName = localizationManager.newProperties(Site.NAME_ENTITY_JPA, "nom");

        List<PlateformeVO> selectedPlateformes = (List<PlateformeVO>) requestMetadatasMap.get(PlateformeVO.class.getSimpleName());
        Set<String> sitesNames = retrieveSitesCodes(selectedPlateformes);
        Map<String, File> filesMap = buildOutputsFiles(sitesNames, SUFFIX_FILENAME);
        Map<String, PrintStream> outputPrintStreamMap = buildOutputPrintStreamMap(filesMap);
        for (String siteName : outputPrintStreamMap.keySet()) {
            outputPrintStreamMap.get(siteName).println(headers);
        }
        while (mesuresChloro.hasNext()) {
            MesureChloro mesureChloro = mesuresChloro.next();

            PrintStream rawDataPrintStream = outputPrintStreamMap.get(((SiteGLACPE) mesureChloro.getSousSequenceChloro().getSequenceChloro().getProjetSite().getSite()).getCodeFromName());
            String line;

            String localizedProjetName = propertiesProjetName.getProperty(mesureChloro.getSousSequenceChloro().getSequenceChloro().getProjetSite().getProjet().getNom());
            if (Strings.isNullOrEmpty(localizedProjetName)) {
                localizedProjetName = mesureChloro.getSousSequenceChloro().getSequenceChloro().getProjetSite().getProjet().getNom();
            }

            String localizedSiteName = propertiesSiteName.getProperty(mesureChloro.getSousSequenceChloro().getPlateforme().getSite().getNom());
            if (Strings.isNullOrEmpty(localizedSiteName)) {
                localizedSiteName = mesureChloro.getSousSequenceChloro().getPlateforme().getSite().getNom();
            }

            String localizedPlateformeName = propertiesPlateformeName.getProperty(mesureChloro.getSousSequenceChloro().getPlateforme().getNom());
            if (Strings.isNullOrEmpty(localizedPlateformeName)) {
                localizedPlateformeName = mesureChloro.getSousSequenceChloro().getPlateforme().getNom();
            }

            // nom projet;nom site;nom plateforme;profondeur min demandée;profondeur max demandée;date prélèvement;profondeur min réelle observée;profondeur max réelle observée
            if (depthRequestParamVO.getAllDepth()) {
                line = String.format("%s;%s;%s;%s;%s;%s", localizedProjetName, localizedSiteName, localizedPlateformeName, DateUtil.getSimpleDateFormatDateLocale().format(mesureChloro.getSousSequenceChloro().getSequenceChloro().getDate()),
                        mesureChloro.getProfondeur_min(), mesureChloro.getProfondeur_max());
            } else {
                line = String.format("%s;%s;%s;%s;%s;%s;%s;%s", localizedProjetName, localizedSiteName, localizedPlateformeName, depthRequestParamVO.getDepthMin(), depthRequestParamVO.getDepthMax(),
                        DateUtil.getSimpleDateFormatDateLocale().format(mesureChloro.getSousSequenceChloro().getSequenceChloro().getDate()), mesureChloro.getProfondeur_min(), mesureChloro.getProfondeur_max());
            }
            rawDataPrintStream.print(line);

            Map<Long, Float> valeursMesuresChloro = buildValeurs(mesureChloro.getValeurs());
            for (VariableVO variableVO : selectedVariables) {
                rawDataPrintStream.print(";");
                Float valeur = valeursMesuresChloro.get(variableVO.getId());
                if (valeur != null) {
                    rawDataPrintStream.print(String.format("%.3f", valeur));
                } else {
                    rawDataPrintStream.print("");
                }
            }
            rawDataPrintStream.println();
            // commenter ?
            //mesuresChloro.remove(); 
        }

        closeStreams(outputPrintStreamMap);
        return filesMap;
    }

    private Map<Long, Float> buildValeurs(List<ValeurMesureChloro> valeurs) {
        Map<Long, Float> mapValue = new HashMap<Long, Float>();
        for (ValeurMesureChloro valeur : valeurs) {
            mapValue.put(valeur.getVariable().getId(), valeur.getValeur());
        }
        return mapValue;
    }

    class ErrorsReport {

        private String errorsMessages = new String();

        public void addErrorMessage(String errorMessage) {
            errorsMessages = errorsMessages.concat("-").concat(errorMessage).concat("\n");
        }

        public String getErrorsMessages() {
            return errorsMessages;
        }

        public boolean hasErrors() {
            return (errorsMessages.length() > 0);
        }
    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException, NoExtractionResultException {
        if (((DefaultParameter) parameters).getResults().get(ChloroRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE).get(ChloroRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE) == null
                || ((DefaultParameter) parameters).getResults().get(ChloroRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE).get(ChloroRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE).isEmpty()) {
            return null;
        }
        ((DefaultParameter) parameters).getFilesMaps().add(super.buildOutput(parameters, ChloroRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE));
        return null;

    }

}
