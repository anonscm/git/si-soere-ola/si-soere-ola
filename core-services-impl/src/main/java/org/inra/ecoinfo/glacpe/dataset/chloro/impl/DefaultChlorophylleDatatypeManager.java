package org.inra.ecoinfo.glacpe.dataset.chloro.impl;

import java.util.Date;
import java.util.List;

import org.inra.ecoinfo.MO;
import org.inra.ecoinfo.glacpe.dataset.chloro.IChlorophylleDAO;
import org.inra.ecoinfo.glacpe.dataset.chloro.IChlorophylleDatatypeManager;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ptcherniati
 */
public class DefaultChlorophylleDatatypeManager extends MO implements IChlorophylleDatatypeManager {

    /**
     *
     */
    protected IChlorophylleDAO chlorophylleDAO;

    /**
     *
     */
    protected IVariableDAO variableDAO;

    /**
     *
     * @param variableDAO
     */
    public void setVariableDAO(IVariableDAO variableDAO) {
        this.variableDAO = variableDAO;
    }

    /**
     *
     * @param chlorophylleDAO
     */
    public void setChlorophylleDAO(IChlorophylleDAO chlorophylleDAO) {
        this.chlorophylleDAO = chlorophylleDAO;
    }

    /**
     *
     * @return
     * @throws BusinessException
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public List<Site> retrieveChlorophylleAvailablesSites() throws BusinessException {
        try {
            return chlorophylleDAO.retrieveAvailablesSites();
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    /**
     *
     * @param plateformesIds
     * @param firstDate
     * @param endDate
     * @return
     * @throws BusinessException
     */
    @Override
    public List<VariableGLACPE> retrieveChlorophylleAvailablesVariables(List<Long> plateformesIds, Date firstDate, Date endDate) throws BusinessException {
        try {
            return chlorophylleDAO.retrieveAvailablesVariables(plateformesIds, firstDate, endDate);
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    /**
     *
     * @return
     * @throws BusinessException
     */
    @Override
    public List<Projet> retrieveChloroAvailablesProjets() throws BusinessException {
        try {
            return chlorophylleDAO.retrieveAvailablesProjets();
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    /**
     *
     * @param id
     * @return
     * @throws BusinessException
     */
    @Override
    public List<Plateforme> retrieveChloroAvailablesPlateformesByProjetSiteId(Long id) throws BusinessException {
        try {
            return chlorophylleDAO.retrieveAvailablesPlateformesByProjetSiteId(id);
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }

}
