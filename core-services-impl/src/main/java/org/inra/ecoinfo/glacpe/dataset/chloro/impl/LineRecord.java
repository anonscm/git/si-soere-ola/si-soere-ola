package org.inra.ecoinfo.glacpe.dataset.chloro.impl;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.inra.ecoinfo.glacpe.dataset.impl.VariableValue;

/**
 *
 * @author ptcherniati
 */
public class LineRecord {

    private String projetCode;
    private String nomSite;
    private String plateformeCode;
    private Date date;
    private Float profondeurMIN;
    private Float profondeurMAX;
    private List<VariableValue> variablesValues = new LinkedList<VariableValue>();
    private Long originalLineNumber;

    /**
     *
     * @param projetCode
     * @param nomSite
     * @param plateformeCode
     * @param date
     * @param profondeurMAX
     * @param profondeurMIN
     * @param variablesValues
     * @param originalLineNumber
     */
    public LineRecord(String projetCode, String nomSite, String plateformeCode, Date date, Float profondeurMAX, Float profondeurMIN, List<VariableValue> variablesValues, Long originalLineNumber) {
        super();
        this.projetCode = projetCode;
        this.nomSite = nomSite;
        this.plateformeCode = plateformeCode;
        this.date = date;
        this.profondeurMAX = profondeurMAX;
        this.profondeurMIN = profondeurMIN;
        this.variablesValues = variablesValues;
        this.originalLineNumber = originalLineNumber;
    }

    /**
     *
     * @param line
     */
    public void copy(LineRecord line) {
        this.projetCode = line.getProjetCode();
        this.nomSite = line.getNomSite();
        this.plateformeCode = line.getPlateformeCode();
        this.date = line.getDate();
        this.profondeurMAX = line.getProfondeurMAX();
        this.profondeurMIN = line.getProfondeurMIN();
        this.originalLineNumber = line.getOriginalLineNumber();
        this.variablesValues = line.getVariablesValues();

    }

    /**
     *
     * @return
     */
    public Date getDate() {
        return date;
    }

    /**
     *
     * @return
     */
    public Float getProfondeurMIN() {
        return profondeurMIN;
    }

    /**
     *
     * @return
     */
    public Float getProfondeurMAX() {
        return profondeurMAX;
    }

    /**
     *
     * @return
     */
    public List<VariableValue> getVariablesValues() {
        return variablesValues;
    }

    /**
     *
     * @return
     */
    public Long getOriginalLineNumber() {
        return originalLineNumber;
    }

    /**
     *
     * @return
     */
    public String getProjetCode() {
        return projetCode;
    }

    /**
     *
     * @param projetCode
     */
    public void setProjetCode(String projetCode) {
        this.projetCode = projetCode;
    }

    /**
     *
     * @return
     */
    public String getNomSite() {
        return nomSite;
    }

    /**
     *
     * @param nomSite
     */
    public void setNomSite(String nomSite) {
        this.nomSite = nomSite;
    }

    /**
     *
     * @return
     */
    public String getPlateformeCode() {
        return plateformeCode;
    }

    /**
     *
     * @param plateformeCode
     */
    public void setPlateformeCode(String plateformeCode) {
        this.plateformeCode = plateformeCode;
    }
}
