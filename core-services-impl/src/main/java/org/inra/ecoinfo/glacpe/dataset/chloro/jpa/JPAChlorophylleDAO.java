package org.inra.ecoinfo.glacpe.dataset.chloro.jpa;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.glacpe.dataset.chloro.IChlorophylleDAO;
import org.inra.ecoinfo.glacpe.dataset.chloro.entity.MesureChloro;
import org.inra.ecoinfo.glacpe.dataset.chloro.entity.ValeurMesureChloro;
import org.inra.ecoinfo.glacpe.extraction.vo.AbstractDatesFormParam;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesRequestParamVO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
@SuppressWarnings("unchecked")
public class JPAChlorophylleDAO extends AbstractJPADAO<Object> implements IChlorophylleDAO {

    private static final String REQUEST_AllDepth = "select m from MesureChloro m where m.sousSequenceChloro.plateforme.id in (:selectedPlateformesIds) and m.sousSequenceChloro.sequenceChloro.projetSite.id in (:selectedProjetSiteIds) %s  order by m.sousSequenceChloro.sequenceChloro.projetSite.projet.nom, m.sousSequenceChloro.plateforme.site.nom, m.sousSequenceChloro.plateforme.nom, m.sousSequenceChloro.sequenceChloro.date, m.profondeur_min, m.profondeur_max";
    private static final String REQUEST_BetweenDepth = "select m from MesureChloro m where m.sousSequenceChloro.plateforme.id in (:selectedPlateformesIds) and m.sousSequenceChloro.sequenceChloro.projetSite.id in (:selectedProjetSiteIds) and m.profondeur_max >= :depthMin and m.profondeur_min <= :depthMax %s  order by m.sousSequenceChloro.sequenceChloro.projetSite.projet.nom, m.sousSequenceChloro.plateforme.site.nom, m.sousSequenceChloro.plateforme.nom, m.sousSequenceChloro.sequenceChloro.date, m.profondeur_min, m.profondeur_max";
    private static final String REQUEST_AGGREGATED_AllDepth = "select vm from ValeurMesureChloro vm where vm.variable.id in(:variablesId) and vm.mesure.sousSequenceChloro.plateforme.id in (:selectedPlateformesIds) and vm.mesure.sousSequenceChloro.sequenceChloro.projetSite.id in(:selectedProjetSiteIds) %s  order by vm.mesure.sousSequenceChloro.sequenceChloro.projetSite.projet.nom, vm.mesure.sousSequenceChloro.plateforme.site.nom, vm.mesure.sousSequenceChloro.plateforme.nom, vm.mesure.sousSequenceChloro.sequenceChloro.date, vm.mesure.profondeur_min, vm.mesure.profondeur_max";
    private static final String REQUEST_AGGREGATED_BetweenDepth = "select vm from ValeurMesureChloro vm where  vm.variable.id in(:variablesId) and vm.mesure.sousSequenceChloro.plateforme.id in (:selectedPlateformesIds) and vm.mesure.sousSequenceChloro.sequenceChloro.projetSite.id in(:selectedProjetSiteIds) and vm.mesure.profondeur_max >= :depthMin and vm.mesure.profondeur_min <= :depthMax %s  order by vm.mesure.sousSequenceChloro.sequenceChloro.projetSite.projet.nom, vm.mesure.sousSequenceChloro.plateforme.site.nom, vm.mesure.sousSequenceChloro.plateforme.nom, vm.mesure.sousSequenceChloro.sequenceChloro.date, vm.mesure.profondeur_min, vm.mesure.profondeur_max";

    private static final String REQUEST_AVAILABLES_VARIABLES = "select distinct vmc.variable from ValeurMesureChloro vmc where vmc.mesure.sousSequenceChloro.plateforme.id in (:plateformesIds) and vmc.mesure.sousSequenceChloro.sequenceChloro.date between :firstDate and :lastDate order by vmc.variable.ordreAffichageGroupe";
    private static final String REQUEST_AVAILABLES_SITES = "select distinct ssc.plateforme.site from SousSequenceChloro ssc";
    private static final String REQUEST_AVAILABLE_PROJECTS = "select distinct s.projetSite.projet from SequenceChloro s";
    private static final String REQUEST_AVAILABLE_PLATEFORME_FOR_PROJECTS = "select distinct ss.plateforme from SousSequenceChloro ss where ss.sequenceChloro.projetSite.id=:id";

    private static final String HQL_DATE_FIELD_2 = "m.sousSequenceChloro.sequenceChloro.date";
    private static final String HQL_DATE_FIELD = "vm.mesure.sousSequenceChloro.sequenceChloro.date";

    /**
     *
     * @param selectedPlateformesIds
     * @param selectedProjetSiteIds
     * @param datesRequestParamVO
     * @return
     * @throws PersistenceException
     */
    @Override
    public List<MesureChloro> extractDatasForAllDepth(List<Long> selectedPlateformesIds, List<Long> selectedProjetSiteIds, DatesRequestParamVO datesRequestParamVO) throws PersistenceException {

        try {
            AbstractDatesFormParam datesFormParam = datesRequestParamVO.getCurrentDatesFormParam();
            Query query = entityManager.createQuery(String.format(REQUEST_AllDepth, datesFormParam.buildSQLCondition(HQL_DATE_FIELD_2)));
            Map<String, Date> datesMap = datesFormParam.retrieveParametersMap();

            for (String key : datesMap.keySet()) {
                query.setParameter(key, datesMap.get(key));
            }

            query.setParameter("selectedPlateformesIds", selectedPlateformesIds);
            query.setParameter("selectedProjetSiteIds", selectedProjetSiteIds);

            return query.getResultList();

        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param selectedPlateformesIds
     * @param selectedProjetSiteIds
     * @param datesRequestParamVO
     * @param depthMin
     * @param depthMax
     * @return
     * @throws PersistenceException
     */
    @Override
    public List<MesureChloro> extractDatasForRangeDepth(List<Long> selectedPlateformesIds, List<Long> selectedProjetSiteIds, DatesRequestParamVO datesRequestParamVO, Float depthMin, Float depthMax) throws PersistenceException {

        try {
            AbstractDatesFormParam datesFormParam = datesRequestParamVO.getCurrentDatesFormParam();
            Query query = entityManager.createQuery(String.format(REQUEST_BetweenDepth, datesFormParam.buildSQLCondition(HQL_DATE_FIELD_2)));
            Map<String, Date> datesMap = datesFormParam.retrieveParametersMap();

            for (String key : datesMap.keySet()) {
                query.setParameter(key, datesMap.get(key));
            }

            query.setParameter("selectedPlateformesIds", selectedPlateformesIds);
            query.setParameter("selectedProjetSiteIds", selectedProjetSiteIds);
            query.setParameter("depthMin", depthMin);
            query.setParameter("depthMax", depthMax);

            return query.getResultList();

        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     *
     * @return
     * @throws PersistenceException
     */
    @Override
    public List<Site> retrieveAvailablesSites() throws PersistenceException {
        try {
            Query query = entityManager.createQuery(REQUEST_AVAILABLES_SITES);
            List<Site> sites = query.getResultList();

            return sites;
        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param id
     * @return
     * @throws PersistenceException
     */
    @Override
    public List<Plateforme> retrieveAvailablesPlateformesByProjetSiteId(Long id) throws PersistenceException {
        try {
            Query query = entityManager.createQuery(REQUEST_AVAILABLE_PLATEFORME_FOR_PROJECTS);
            query.setParameter("id", id);
            List<Plateforme> plateformes = query.getResultList();

            return plateformes;
        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param plateformesIds
     * @param firstDate
     * @param endDate
     * @return
     * @throws PersistenceException
     */
    @Override
    public List<VariableGLACPE> retrieveAvailablesVariables(List<Long> plateformesIds, Date firstDate, Date endDate) throws PersistenceException {
        try {
            Query query = entityManager.createQuery(REQUEST_AVAILABLES_VARIABLES);
            query.setParameter("plateformesIds", plateformesIds);
            query.setParameter("firstDate", firstDate);
            query.setParameter("lastDate", endDate);
            List<VariableGLACPE> variables = query.getResultList();

            return variables;
        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     *
     * @return
     * @throws PersistenceException
     */
    @Override
    public List<Projet> retrieveAvailablesProjets() throws PersistenceException {
        try {
            Query query = entityManager.createQuery(REQUEST_AVAILABLE_PROJECTS);
            List<Projet> projets = query.getResultList();

            return projets;
        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param plateformesIds
     * @param selectedProjetSiteIds
     * @param datesRequestParamVO
     * @param variablesId
     * @return
     * @throws PersistenceException
     */
    @Override
    public List<ValeurMesureChloro> extractAggregatedDatasForAllDepth(List<Long> plateformesIds, List<Long> selectedProjetSiteIds, DatesRequestParamVO datesRequestParamVO, List<Long> variablesId) throws PersistenceException {
        try {
            AbstractDatesFormParam datesFormParam = datesRequestParamVO.getCurrentDatesFormParam();
            Query query = entityManager.createQuery(String.format(REQUEST_AGGREGATED_AllDepth, datesFormParam.buildSQLCondition(HQL_DATE_FIELD)));
            Map<String, Date> datesMap = datesFormParam.retrieveParametersMap();

            for (String key : datesMap.keySet()) {
                query.setParameter(key, datesMap.get(key));
            }

            query.setParameter("selectedPlateformesIds", plateformesIds);
            query.setParameter("variablesId", variablesId);
            query.setParameter("selectedProjetSiteIds", selectedProjetSiteIds);

            return query.getResultList();

        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param plateformesIds
     * @param selectedProjetSiteIds
     * @param datesRequestParamVO
     * @param variablesId
     * @param depthMin
     * @param depthMax
     * @return
     * @throws PersistenceException
     */
    @Override
    public List<ValeurMesureChloro> extractAggregatedDatasForRangeDepth(List<Long> plateformesIds, List<Long> selectedProjetSiteIds, DatesRequestParamVO datesRequestParamVO, List<Long> variablesId, Float depthMin, Float depthMax)
            throws PersistenceException
    {
        try {
            AbstractDatesFormParam datesFormParam = datesRequestParamVO.getCurrentDatesFormParam();
            Query query = entityManager.createQuery(String.format(REQUEST_AGGREGATED_BetweenDepth, datesFormParam.buildSQLCondition(HQL_DATE_FIELD)));
            Map<String, Date> datesMap = datesFormParam.retrieveParametersMap();

            for (String key : datesMap.keySet()) {
                query.setParameter(key, datesMap.get(key));
            }

            query.setParameter("selectedPlateformesIds", plateformesIds);
            query.setParameter("variablesId", variablesId);
            query.setParameter("selectedProjetSiteIds", selectedProjetSiteIds);
            query.setParameter("depthMin", depthMin);
            query.setParameter("depthMax", depthMax);

            return query.getResultList();

        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }
}
