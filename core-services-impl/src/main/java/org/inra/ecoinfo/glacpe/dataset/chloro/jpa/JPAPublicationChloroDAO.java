package org.inra.ecoinfo.glacpe.dataset.chloro.jpa;

import javax.persistence.Query;

import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.jpa.JPAVersionFileDAO;
import org.inra.ecoinfo.glacpe.dataset.ILocalPublicationDAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class JPAPublicationChloroDAO extends JPAVersionFileDAO implements ILocalPublicationDAO {

    private static final String REQUEST_NATIVE_DELETE_SEQUENCE_CHLORO_BY_IDS = "delete from SequenceChloro sce where sce.versionFile = :version";
    private static final String REQUEST_NATIVE_DELETE_SOUS_SEQUENCE_CHLORO_BY_IDS = "delete from SousSequenceChloro ssce where sChloro_id in (select id from SequenceChloro sce where sce.versionFile = :version)";
    private static final String REQUEST_NATIVE_DELETE_MESURE_CHLORO_BY_IDS = "delete from MesureChloro mce where ssChloro_id in (select id from SousSequenceChloro ssce where sChloro_id in (select id from SequenceChloro sce where sce.versionFile = :version))";
    private static final String REQUEST_NATIVE_DELETE_VALEUR_MESURE_CHLORO_BY_IDS = "delete from ValeurMesureChloro vmce where mChloro_id in (select id from MesureChloro mce where ssChloro_id in (select id from SousSequenceChloro ssce where sChloro_id in (select id from SequenceChloro sce where sce.versionFile = :version)))";

    @Override
    public void removeVersion(VersionFile version) throws PersistenceException {
        try {
            Query query = entityManager.createQuery(REQUEST_NATIVE_DELETE_VALEUR_MESURE_CHLORO_BY_IDS);
            query.setParameter("version", version);
            query.executeUpdate();

            query = entityManager.createQuery(REQUEST_NATIVE_DELETE_MESURE_CHLORO_BY_IDS);
            query.setParameter("version", version);
            query.executeUpdate();

            query = entityManager.createQuery(REQUEST_NATIVE_DELETE_SOUS_SEQUENCE_CHLORO_BY_IDS);
            query.setParameter("version", version);
            query.executeUpdate();

            query = entityManager.createQuery(REQUEST_NATIVE_DELETE_SEQUENCE_CHLORO_BY_IDS);
            query.setParameter("version", version);
            query.executeUpdate();

        } catch (Exception e) {
            JPAVersionFileDAO.LOGGER.error(e);
            throw new PersistenceException(e);
        }
    }
}
