/**
 * OREILacs project - see LICENCE.txt for use created: 24 septembre 11:18:20
 */
package org.inra.ecoinfo.glacpe.dataset.chloro.jpa;

import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.glacpe.dataset.chloro.ISequenceChloroDAO;
import org.inra.ecoinfo.glacpe.dataset.chloro.entity.SequenceChloro;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;


/**
 * @author "Cédric Anache"
 * 
 **/

public class JPASequenceChloroDAO extends AbstractJPADAO<SequenceChloro> implements ISequenceChloroDAO {

    // Récupére toutes les séquences chlorophylle d'une date et plateforme précise.

    /**
     *
     * @param date
     * @param projetCode
     * @param siteCode
     * @return
     * @throws PersistenceException
     */
        @Override
    public SequenceChloro getByDatePrelevementAndProjetCodeAndSiteCode(Date date, String projetCode, String siteCode) throws PersistenceException {
        try {
            Query query = null;

            String queryString = "from SequenceChloro s where s.date = :date and s.projetSite.projet.code = :projetCode and s.projetSite.site.code = :siteCode";

            query = entityManager.createQuery(queryString);
            query.setParameter("date", date);
            query.setParameter("projetCode", projetCode);
            query.setParameter("siteCode", siteCode);

            List<?> results = query.getResultList();
            if (results != null && results.size() == 1) {
                return (SequenceChloro) results.get(0);
            }

        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
        return null;
    }
}
