package org.inra.ecoinfo.glacpe.dataset.conditionprelevement;

import java.util.Date;
import java.util.List;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.entity.MesureConditionGenerale;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.entity.ValeurConditionGenerale;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesRequestParamVO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.projetsite.ProjetSite;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public interface IConditionGeneraleDAO extends IDAO<Object> {

    /**
     *
     * @return
     * @throws PersistenceException
     */
    List<Projet> retrieveConditionGeneraleAvailablesProjets() throws PersistenceException;

    /**
     *
     * @param projetSiteId
     * @return
     * @throws PersistenceException
     */
    List<Plateforme> retrieveConditionGeneraleAvailablesPlateformesByProjetSiteId(long projetSiteId) throws PersistenceException;

    /**
     *
     * @param selectedPlateformesIds
     * @param projetId
     * @param datesRequestParamVO
     * @return
     * @throws PersistenceException
     */
    List<MesureConditionGenerale> extractDatas(List<Long> selectedPlateformesIds, List<Long> projetId, DatesRequestParamVO datesRequestParamVO) throws PersistenceException;

    /**
     *
     * @param projetId
     * @param siteId
     * @return
     * @throws PersistenceException
     */
    ProjetSite retrieveConditionGeneraleAvailablesProjetsSiteByProjetAndSite(Long projetId, Long siteId) throws PersistenceException;

    /**
     *
     * @return
     * @throws PersistenceException
     */
    List<Projet> retrieveAvailablesProjets() throws PersistenceException;

    /**
     *
     * @param plateformesIds
     * @param firstDate
     * @param lastDate
     * @return
     * @throws PersistenceException
     */
    List<VariableGLACPE> retrieveSondeMultiAvailablesVariables(List<Long> plateformesIds, Date firstDate, Date lastDate) throws PersistenceException;

    /**
     *
     * @param plateformesIds
     * @param firstDate
     * @param endDate
     * @return
     * @throws PersistenceException
     */
    List<VariableGLACPE> retrieveAvailablesTransparenceVariables(List<Long> plateformesIds, Date firstDate, Date endDate) throws PersistenceException;

    /**
     *
     * @param plateformesIds
     * @param selectedProjetSiteIds
     * @param variablesIds
     * @param dateStart
     * @param dateEnd
     * @return
     * @throws PersistenceException
     */
    List<ValeurConditionGenerale> extractAggregatedDatas(List<Long> plateformesIds, List<Long> selectedProjetSiteIds, List<Long> variablesIds, String dateStart, String dateEnd) throws PersistenceException;
}
