package org.inra.ecoinfo.glacpe.dataset.conditionprelevement;

import java.util.Date;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.entity.MesureConditionGenerale;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.entity.ValeurConditionGenerale;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public interface IMesureConditionGeneraleDAO extends IDAO<MesureConditionGenerale> {

    /**
     *
     * @param datePrelevement
     * @param plateformeCode
     * @param projetCode
     * @return
     * @throws PersistenceException
     */
    MesureConditionGenerale getByDatePrelevementPlateformeCodeAndProjetCode(Date datePrelevement, String plateformeCode, String projetCode) throws PersistenceException;

    /**
     *
     * @param variableId
     * @param mesureId
     * @return
     * @throws PersistenceException
     */
    ValeurConditionGenerale getByVariableIdAndMesureId(Long variableId, Long mesureId) throws PersistenceException;

    void flush();

}
