package org.inra.ecoinfo.glacpe.dataset.conditionprelevement.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.IConditionGeneraleDAO;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.IDatatypeVariableUniteGLACPEDAO;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.utils.Utils;

/**
 *
 * @author ptcherniati
 */
public abstract class AbstractConditionGeneraleDatasOutputBuilder extends AbstractOutputBuilder {

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH_CONDITIONPRELEVEMENT = "org.inra.ecoinfo.glacpe.extraction.conditionprelevement.messages";

    /**
     *
     */
    protected static final String MAP_INDEX_0 = "0";

    /**
     *
     */
    protected IConditionGeneraleDAO conditionGeneraleDAO;

    /**
     *
     */
    protected IVariableDAO variableDAO;

    /**
     *
     */
    protected IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO;

    /**
     *
     * @param conditionGeneraleDAO
     */
    public void setConditionGeneraleDAO(IConditionGeneraleDAO conditionGeneraleDAO) {
        this.conditionGeneraleDAO = conditionGeneraleDAO;
    }

    /**
     *
     * @param selectedPlateformes
     * @return
     */
    protected Set<String> retrieveSitesCodes(List<PlateformeVO> selectedPlateformes) {
        Set<String> sitesNames = new HashSet<String>();
        for (PlateformeVO plateforme : selectedPlateformes) {
            sitesNames.add(Utils.createCodeFromString(plateforme.getNomSite()));
        }
        return sitesNames;
    }

    /**
     *
     * @param datatypeVariableUniteDAO
     */
    public void setDatatypeVariableUniteDAO(IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO) {
        this.datatypeVariableUniteDAO = datatypeVariableUniteDAO;
    }

    /**
     *
     * @param variableDAO
     */
    public void setVariableDAO(IVariableDAO variableDAO) {
        this.variableDAO = variableDAO;
    }
}
