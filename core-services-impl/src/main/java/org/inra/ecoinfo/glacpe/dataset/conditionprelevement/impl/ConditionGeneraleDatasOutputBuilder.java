package org.inra.ecoinfo.glacpe.dataset.conditionprelevement.impl;

import java.io.File;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.entity.MesureConditionGenerale;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.entity.ValeurConditionGenerale;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO;
import org.inra.ecoinfo.glacpe.extraction.vo.VariableVO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.refdata.valeurqualitative.ValeurQualitative;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import com.google.common.base.Strings;

/**
 *
 * @author ptcherniati
 */
public class ConditionGeneraleDatasOutputBuilder extends AbstractConditionGeneraleDatasOutputBuilder {

    private static final String HEADER_DATA = "PROPERTY_MSG_HEADER_RAW_CONDITION";
    private static final String MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS = "PROPERTY_MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS";

    private static final String CODE_DATATYPE_CONDITION_PRELEVEMENT = "conditions_prelevements";

    @SuppressWarnings("unchecked")
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatas) throws BusinessException {
        Properties propertiesVariableName = localizationManager.newProperties(Variable.NAME_ENTITY_JPA, "nom");
        ErrorsReport errorsReport = new ErrorsReport();
        StringBuilder stringBuilder = new StringBuilder();

        List<VariableVO> selectedVariables = (List<VariableVO>) requestMetadatas.get(VariableVO.class.getSimpleName());
        try {
            stringBuilder.append(String.format("%s", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_CONDITIONPRELEVEMENT, HEADER_DATA)));

            for (VariableVO variableVO : selectedVariables) {
                VariableGLACPE variable = (VariableGLACPE) variableDAO.getByCode(variableVO.getCode());
                if (variable == null) {
                    errorsReport.addErrorMessage((String.format(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_CONDITIONPRELEVEMENT, MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS), selectedVariables)));
                }
                String localizedVariableName = propertiesVariableName.getProperty(variable.getNom());
                if (Strings.isNullOrEmpty(localizedVariableName))
                    localizedVariableName = variable.getNom();

                Unite unite = datatypeVariableUniteDAO.getUnite(CODE_DATATYPE_CONDITION_PRELEVEMENT, variable.getCode());
                if (unite == null || unite.getCode().equals("nounit")) {
                    stringBuilder.append(String.format(";%s", localizedVariableName));
                } else {
                    stringBuilder.append(String.format(";%s (%s)", localizedVariableName, unite.getCode()));
                }

                if (errorsReport.hasErrors()) {
                    throw new PersistenceException(errorsReport.getErrorsMessages());
                }
            }
        } catch (Exception e) {
            throw new BusinessException(e);
        }
        return stringBuilder.toString();
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {

        Iterator<MesureConditionGenerale> mesuresConditionGenerale = resultsDatasMap.get(MAP_INDEX_0).iterator();
        Properties propertiesValeurQualitativeValue = localizationManager.newProperties(ValeurQualitative.NAME_ENTITY_JPA, "value");
        Properties propertiesProjetName = localizationManager.newProperties(Projet.NAME_TABLE, "nom");
        Properties propertiesPlateformeName = localizationManager.newProperties(Plateforme.TABLE_NAME, "nom");
        Properties propertiesSiteName = localizationManager.newProperties(Site.NAME_ENTITY_JPA, "nom");

        List<PlateformeVO> selectedPlateformes = (List<PlateformeVO>) requestMetadatasMap.get(PlateformeVO.class.getSimpleName());
        List<VariableVO> selectedVariables = (List<VariableVO>) requestMetadatasMap.get(VariableVO.class.getSimpleName());
        Set<String> sitesNames = retrieveSitesCodes(selectedPlateformes);
        Map<String, File> filesMap = buildOutputsFiles(sitesNames, SUFFIX_FILENAME_DEFAULT);
        Map<String, PrintStream> outputPrintStreamMap = buildOutputPrintStreamMap(filesMap);
        for (String siteName : outputPrintStreamMap.keySet()) {
            outputPrintStreamMap.get(siteName).println(headers);
        }
        while (mesuresConditionGenerale.hasNext()) {
            final MesureConditionGenerale mesureConditionGenerale = mesuresConditionGenerale.next();

            PrintStream rawDataPrintStream = outputPrintStreamMap.get(((SiteGLACPE) mesureConditionGenerale.getSite()).getCodeFromName());
            String line;
            String localizedProjetName = propertiesProjetName.getProperty(mesureConditionGenerale.getProjetSite().getProjet().getNom());
            if (Strings.isNullOrEmpty(localizedProjetName))
                localizedProjetName = mesureConditionGenerale.getProjetSite().getProjet().getNom();

            String localizedSiteName = propertiesSiteName.getProperty(mesureConditionGenerale.getProjetSite().getSite().getNom());
            if (Strings.isNullOrEmpty(localizedSiteName))
                localizedSiteName = mesureConditionGenerale.getProjetSite().getSite().getNom();

            String localizedPlateformeName = propertiesPlateformeName.getProperty(mesureConditionGenerale.getPlateforme().getNom());
            if (Strings.isNullOrEmpty(localizedPlateformeName))
                localizedPlateformeName = mesureConditionGenerale.getPlateforme().getNom();

            line = String.format("%s;%s;%s;%s;%s;%s", localizedProjetName, localizedSiteName, localizedPlateformeName, DateUtil.getSimpleDateFormatDateLocale().format(mesureConditionGenerale.getDatePrelevement()),
                    mesureConditionGenerale.getHeure() == null ? "" : DateUtil.getSimpleDateFormatTimeLocale().format(mesureConditionGenerale.getHeure()), mesureConditionGenerale.getCommentaire());

            rawDataPrintStream.print(line);

            Map<Long, ValeurConditionGenerale> valeursConditionGenerale = buildValeurs(mesureConditionGenerale.getValeurs());

            for (VariableVO variableVO : selectedVariables) {
                rawDataPrintStream.print(";");
                ValeurConditionGenerale valeurConditionGenerale = valeursConditionGenerale.get(variableVO.getId());
                if (valeurConditionGenerale != null) {
                    if (valeurConditionGenerale.getVariable().getIsQualitative()) {
                        if (valeurConditionGenerale.getValeurQualitative() != null) {
                            String localizedValue = propertiesValeurQualitativeValue.getProperty(valeurConditionGenerale.getValeurQualitative().getValeur());
                            if (Strings.isNullOrEmpty(localizedValue))
                                localizedValue = valeurConditionGenerale.getValeurQualitative().getValeur();
                            rawDataPrintStream.print(String.format("%s", localizedValue));
                        } else {
                            rawDataPrintStream.print(String.format("%s", ""));
                        }
                    } else {
                        if (valeurConditionGenerale.getValeur() != null) {
                            rawDataPrintStream.print(String.format("%s", valeurConditionGenerale.getValeur()));
                        } else {
                            rawDataPrintStream.print(String.format("%s", ""));
                        }
                    }
                }
            }

            rawDataPrintStream.println();
            //mesuresConditionGenerale.remove(); // commenté pour extraction du fichier d'informations
        }

        closeStreams(outputPrintStreamMap);
        return filesMap;
    }

    private Map<Long, ValeurConditionGenerale> buildValeurs(List<ValeurConditionGenerale> valeurs) {
        Map<Long, ValeurConditionGenerale> mapValue = new HashMap<Long, ValeurConditionGenerale>();
        for (ValeurConditionGenerale valeur : valeurs) {
            mapValue.put(valeur.getVariable().getId(), valeur);
        }
        return mapValue;
    }

    class ErrorsReport {

        private String errorsMessages = new String();

        public void addErrorMessage(String errorMessage) {
            errorsMessages = errorsMessages.concat("-").concat(errorMessage).concat("\n");
        }

        public String getErrorsMessages() {
            return errorsMessages;
        }

        public boolean hasErrors() {
            return (errorsMessages.length() > 0);
        }
    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException, NoExtractionResultException {
        ((DefaultParameter) parameters).getFilesMaps().add(super.buildOutput(parameters, ConditionGeneraleDatasExtractor.CST_RESULT_EXTRACTION_CODE));
        return null;

    }


}
