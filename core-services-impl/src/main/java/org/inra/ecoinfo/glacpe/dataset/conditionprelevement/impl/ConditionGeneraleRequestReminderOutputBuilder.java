package org.inra.ecoinfo.glacpe.dataset.conditionprelevement.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.IConditionGeneraleDAO;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public class ConditionGeneraleRequestReminderOutputBuilder extends AbstractOutputBuilder {

    /**
     *
     */
    public static String CST_RESULT_EXTRACTION_CODE = "conditionPrelevementRequestReminder";

    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.glacpe.extraction.messages";

    private static final String MSG_EXTRACTION_COMMENTS = "PROPERTY_MSG_EXTRACTION_COMMENTS";

    private static final String MSG_HEADER = "PROPERTY_MSG_HEADER";

    private static final String PATTERN_STRING_PLATEFORMS_SUMMARY = "   %s (%s: %s)";
    private static final String PATTERN_STRING_HEADER_PLATEFORMS_SUMMARY = "%s\n   %s";
    private static final String PATTERN_STRING_COMMENTS_SUMMARY = "   %s";

    private static final String MSG_SELECTED_PERIODS = "PROPERTY_MSG_SELECTED_PERIODS";
    private static final String MSG_SITE = "PROPERTY_MSG_SITE";
    private static final String MSG_SELECTED_PROJECT = "PROPERTY_MSG_SELECTED_PROJECT";
    private static final String MSG_SELECTED_PLATEFORMS = "PROPERTY_MSG_SELECTED_PLATEFORMS";

    private static final String KEYMAP_COMMENTS = "comments";

    /**
     *
     */
    protected IConditionGeneraleDAO conditionGeneraleDAO;

    /**
     *
     * @param conditionGeneraleDAO
     */
    public void setConditionGeneraleDAO(IConditionGeneraleDAO conditionGeneraleDAO) {
        this.conditionGeneraleDAO = conditionGeneraleDAO;
    }

    @Override
    protected String buildHeader(Map<String, Object> requestMetadatas) throws BusinessException {
        return getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_HEADER);
    }

    /**
     *
     * @param headers
     * @param resultsDatasMap
     * @param parameters
     * @return
     * @throws BusinessException
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, IParameter parameters) throws BusinessException {

        Map<String, Object> requestMetadatasMap = parameters.getParameters();

        Map<String, File> reminderMap = new HashMap<String, File>();
        File reminderFile = buildOutputFile(FILENAME_REMINDER, EXTENSION_TXT);
        PrintStream reminderPrintStream;
        // BufferedReader br;

        try {

            reminderPrintStream = new PrintStream(reminderFile, CHARACTER_ENCODING_ISO_8859_1);
            reminderPrintStream.println(headers);
            reminderPrintStream.println();

            printPlateformesSummary((List<PlateformeVO>) requestMetadatasMap.get(PlateformeVO.class.getSimpleName()), reminderPrintStream);
            printDatesSummary((DatesRequestParamVO) requestMetadatasMap.get(DatesRequestParamVO.class.getSimpleName()), reminderPrintStream);

            reminderPrintStream.println(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_EXTRACTION_COMMENTS));
            reminderPrintStream.println(String.format(PATTERN_STRING_COMMENTS_SUMMARY, requestMetadatasMap.get(KEYMAP_COMMENTS)));

            /*
             * br = new BufferedReader(new InputStreamReader(new FileInputStream(reminderFile), CHARACTER_ENCODING_ISO_8859_1)); StringBuilder reminderBuilder = new StringBuilder(); String line = br.readLine();
             * 
             * while(line != null) { reminderBuilder.append(line).append("\n"); line = br.readLine(); } parameters.setCommentaire(new String(reminderBuilder.toString().getBytes(), "UTF-8")); br.close();
             */
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            AbstractOutputBuilder.LOGGER.error(e);
            throw new BusinessException(e);
        }
        /*
         * catch (IOException e) { logger.error(e); throw new BusinessException(e); }
         */  /*
           * catch (IOException e) { logger.error(e); throw new BusinessException(e); }
           */
        reminderMap.put(FILENAME_REMINDER, reminderFile);

        reminderPrintStream.flush();
        reminderPrintStream.close();

        return reminderMap;
    }

    private void printDatesSummary(DatesRequestParamVO datesRequestParamVO, PrintStream reminderPrintStream) throws BusinessException {
        reminderPrintStream.println(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_SELECTED_PERIODS));
        try {
            datesRequestParamVO.getCurrentDatesFormParam().buildSummary(reminderPrintStream);
        } catch (ParseException e) {
            AbstractOutputBuilder.LOGGER.error(e);
            throw new BusinessException(e);
        }

        reminderPrintStream.println();
    }

    private void printPlateformesSummary(List<PlateformeVO> list, PrintStream reminderPrintStream) {
        Properties propertiesNomProjet = getLocalizationManager().newProperties(Projet.NAME_TABLE, "nom");
        Properties propertiesNomPlateforme = getLocalizationManager().newProperties(Plateforme.TABLE_NAME, "nom");
        Properties propertiesNomSite = getLocalizationManager().newProperties(Site.NAME_ENTITY_JPA, "nom");
        reminderPrintStream.println(String.format(PATTERN_STRING_HEADER_PLATEFORMS_SUMMARY, getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_SELECTED_PROJECT),
                propertiesNomProjet.getProperty(list.get(0).getProjet().getNom(), list.get(0).getProjet().getNom())));
        reminderPrintStream.println();
        reminderPrintStream.println(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_SELECTED_PLATEFORMS));
        for (PlateformeVO plateforme : list) {
            reminderPrintStream.println(String.format(PATTERN_STRING_PLATEFORMS_SUMMARY, propertiesNomPlateforme.getProperty(plateforme.getNom(), plateforme.getNom()), getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_SITE),
                    propertiesNomSite.getProperty(plateforme.getNomSite(), plateforme.getNomSite())));
        }
        reminderPrintStream.println();
    }

    @SuppressWarnings("rawtypes")
    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException, NoExtractionResultException {
        assert parameters.getResults() != null : "Empty results ";
        final Map<String, List> results = parameters.getResults().get(CST_RESULT_EXTRACTION_CODE);
        if (results != null && results.get(MAP_INDEX_0).isEmpty()) {
            throw new NoExtractionResultException(localizationManager.getMessage(NoExtractionResultException.BUNDLE_SOURCE_PATH, NoExtractionResultException.ERROR));
        }
        final String resultsHeader = buildHeader(parameters.getParameters());
        final Map<String, File> filesMap = buildBody(resultsHeader, results, parameters);
        ((DefaultParameter) parameters).getFilesMaps().add(filesMap);
        
        return null;
    }

   

    @SuppressWarnings("rawtypes")
    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {
        // non utilisé ici
        return null;
    }
}
