package org.inra.ecoinfo.glacpe.dataset.conditionprelevement.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.inra.ecoinfo.MO;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.IConditionGeneraleDAO;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.IConditionGeneraleDatatypeManager;
import org.inra.ecoinfo.glacpe.extraction.vo.VariableVO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.projetsite.ProjetSite;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.datatype.IDatatypeDAO;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ptcherniati
 */
public class DefaultConditionGeneraleDatatypeManager extends MO implements IConditionGeneraleDatatypeManager {

    /**
     *
     */
    protected IConditionGeneraleDAO conditionGeneraleDAO;

    /**
     *
     */
    protected IDatatypeDAO datatypeDAO;

    /**
     *
     * @return
     * @throws BusinessException
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public List<Projet> retrieveConditionGeneraleAvailablesProjets() throws BusinessException {
        try {
            return conditionGeneraleDAO.retrieveConditionGeneraleAvailablesProjets();
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    /**
     *
     * @param conditionGeneraleDAO
     */
    public void setConditionGeneraleDAO(IConditionGeneraleDAO conditionGeneraleDAO) {
        this.conditionGeneraleDAO = conditionGeneraleDAO;
    }

    /**
     *
     * @param projetSiteId
     * @return
     * @throws BusinessException
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public List<Plateforme> retrieveConditionGeneraleAvailablesPlateformesByProjetSiteId(long projetSiteId) throws BusinessException {
        try {
            return conditionGeneraleDAO.retrieveConditionGeneraleAvailablesPlateformesByProjetSiteId(projetSiteId);
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    /**
     *
     * @param projetId
     * @param siteId
     * @return
     * @throws BusinessException
     */
    @Override
    public ProjetSite retrieveConditionGeneraleAvailablesProjetsSiteByProjetAndSite(Long projetId, Long siteId) throws BusinessException {
        try {
            return conditionGeneraleDAO.retrieveConditionGeneraleAvailablesProjetsSiteByProjetAndSite(projetId, siteId);
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    /**
     *
     * @return
     * @throws BusinessException
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    @Transactional(rollbackFor = Exception.class)
    @Override
    public List<VariableVO> retrieveListVariables() throws BusinessException {
        try {
            List<VariableGLACPE> variables = (List) ((DataType) datatypeDAO.getByCode("conditions_prelevements")).getVariables();
            List<VariableVO> variablesVO = new ArrayList<VariableVO>();

            for (VariableGLACPE variable : variables) {
                VariableVO variableVO = new VariableVO(variable);
                variablesVO.add(variableVO);
            }
            return variablesVO;
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    /**
     *
     * @param plateformesIds
     * @param firstDate
     * @param lastDate
     * @return
     * @throws BusinessException
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public List<VariableVO> retrieveListsVariables(List<Long> plateformesIds, Date firstDate, Date lastDate) throws BusinessException {
        Map<String, VariableVO> rawVariablesMap = new HashMap<String, VariableVO>();
        for (VariableVO availableVariable : retrieveListAvailableVariables(plateformesIds, firstDate, lastDate)) {
            availableVariable.setIsDataAvailable(true);
            rawVariablesMap.put(availableVariable.getCode(), availableVariable);
        }

        return new LinkedList<VariableVO>(rawVariablesMap.values());
    }

    private List<VariableVO> retrieveListAvailableVariables(List<Long> plateformesIds, Date firstDate, Date lastDate) throws BusinessException {
        try {
            List<VariableGLACPE> variables = conditionGeneraleDAO.retrieveSondeMultiAvailablesVariables(plateformesIds, firstDate, lastDate);
            List<VariableVO> variablesVO = new ArrayList<VariableVO>();

            for (VariableGLACPE variable : variables) {
                VariableVO variableVO = new VariableVO(variable);
                variablesVO.add(variableVO);
            }
            return variablesVO;
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    /**
     *
     * @param datatypeDAO
     */
    public void setDatatypeDAO(IDatatypeDAO datatypeDAO) {
        this.datatypeDAO = datatypeDAO;
    }

    /**
     *
     * @return
     * @throws BusinessException
     */
    @Override
    public List<Projet> retrieveTransparenceAvailablesProjets() throws BusinessException {
        try {
            return conditionGeneraleDAO.retrieveAvailablesProjets();
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    /**
     *
     * @param plateformesIds
     * @param firstDate
     * @param endDate
     * @return
     * @throws BusinessException
     */
    @Override
    public List<VariableGLACPE> retrieveTransparenceAvailablesVariables(List<Long> plateformesIds, Date firstDate, Date endDate) throws BusinessException {
        try {
            return conditionGeneraleDAO.retrieveAvailablesTransparenceVariables(plateformesIds, firstDate, endDate);
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }

}
