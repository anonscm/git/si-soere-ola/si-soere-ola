package org.inra.ecoinfo.glacpe.dataset.conditionprelevement.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.inra.ecoinfo.MO;
import org.inra.ecoinfo.extraction.IExtractor;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.config.impl.Extraction;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.notifications.entity.Notification;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public class DefaultConditionPrelevementExtractor extends MO implements IExtractor {

    /**
     *
     */
    protected IExtractor conditionPrelevementDatasExtractor;
    private static final String MSG_EXTRACTION_ABORTED = "PROPERTY_MSG_FAILED_EXTRACT";

    @Override
    public void setExtraction(Extraction extraction) {

    }

    @Override
    public Extraction getExtraction() {
        return null;
    }

    private void addOutputBuilderByCondition(Boolean condition, IExtractor extractor, List<IExtractor> extractors) {
        if (condition) {
            extractors.add(extractor);
        }
    }

    /**
     *
     * @param metadatasMap
     * @return
     */
    public List<IExtractor> resolveExtractors(Map<String, Object> metadatasMap) {
        List<IExtractor> extractors = new LinkedList<IExtractor>();

        addOutputBuilderByCondition(true, conditionPrelevementDatasExtractor, extractors);
        return extractors;
    }

    @Override
    public void extract(IParameter parameters) throws BusinessException {
        Utilisateur utilisateur = securityContext.getUtilisateur();
        for (IExtractor extractor : resolveExtractors(parameters.getParameters())) {
            try {
                extractor.extract(parameters);
            } catch (NoExtractionResultException e) {
                sendNotification(String.format(localizationManager.getMessage(NoExtractionResultException.BUNDLE_SOURCE_PATH, MSG_EXTRACTION_ABORTED)), Notification.ERROR, e.getMessage(), utilisateur);
                throw new BusinessException(e);
            } catch (BusinessException e) {
                throw e;
            }
        }
    }

    /**
     *
     * @param conditionPrelevementDatasExtractor
     */
    public void setConditionPrelevementDatasExtractor(IExtractor conditionPrelevementDatasExtractor) {
        this.conditionPrelevementDatasExtractor = conditionPrelevementDatasExtractor;
    }

    /**
     *
     * @param parameters
     * @return
     */
    @Override
    public long getExtractionSize(IParameter parameters) {
        // TODO Auto-generated method stub
        return 0;
    }

}
