package org.inra.ecoinfo.glacpe.dataset.conditionprelevement.impl;

import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_DUPLICATE_SEQUENCE;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_ERROR;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_ERROR_PLATEFORME_INVALID;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_ERROR_PLATEFORME_SITE_MISMATCH;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_FLOAT_VALUE_EXPECTED;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_MISSING_PROJET_SITE_IN_REFERENCES_DATAS;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_NOT_IN_LIST;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.getGLACPEMessage;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.getGLACPEMessageWithBundle;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.hibernate.exception.ConstraintViolationException;
import org.inra.ecoinfo.dataset.exception.InsertionDatabaseException;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.IMesureConditionGeneraleDAO;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.entity.MesureConditionGenerale;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.entity.ValeurConditionGenerale;
import org.inra.ecoinfo.glacpe.dataset.impl.AbstractProcessRecord;
import org.inra.ecoinfo.glacpe.dataset.impl.CleanerValues;
import org.inra.ecoinfo.glacpe.dataset.impl.VariableValue;
import org.inra.ecoinfo.glacpe.refdata.controlecoherence.ControleCoherence;
import org.inra.ecoinfo.glacpe.refdata.controlecoherence.IControleCoherenceDAO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.IPlateformeDAO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projetsite.IProjetSiteDAO;
import org.inra.ecoinfo.glacpe.refdata.projetsite.ProjetSite;
import org.inra.ecoinfo.glacpe.refdata.projetsitethemedatatype.ProjetSiteThemeDatatype;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitativeDAO;
import org.inra.ecoinfo.refdata.valeurqualitative.ValeurQualitative;
import org.inra.ecoinfo.system.Allocator;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.DatasetDescriptorBuilder;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.ErrorsReport;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.xml.sax.SAXException;

import com.Ostermiller.util.CSVParser;

/**
 *
 * @author ptcherniati
 */
public class ProcessRecord extends AbstractProcessRecord {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private static final String BUNDLE_SOURCE_PATH_CONDITIONPRELEVEMENT = "org.inra.ecoinfo.glacpe.dataset.conditionprelevement.messages";

    private static final String PROPERTY_MSG_CONSTRAINT_EXCEPTION = "PROPERTY_MSG_CONSTRAINT_EXCEPTION";

    /**
     *
     */
    protected IMesureConditionGeneraleDAO mesureConditionGeneraleDAO;

    /**
     *
     */
    protected IControleCoherenceDAO controleCoherenceDAO;

    /**
     *
     */
    protected IValeurQualitativeDAO valeurQualitativeDAO;

    /**
     *
     */
    protected IPlateformeDAO plateformeDAO;

    /**
     *
     */
    protected IProjetSiteDAO projetSiteDAO;

    /**
     *
     * @return
     * @throws IOException
     * @throws SAXException
     */
    @Override
    public DatasetDescriptor holdDatasetDescriptor() throws IOException, SAXException {
        if (datasetDescriptor == null) {
            datasetDescriptor = DatasetDescriptorBuilder.buildDescriptor(IMesureConditionGeneraleDAO.class.getResource(DATASET_DESCRIPTOR_XML).openStream());
        }
        return datasetDescriptor;
    }

    @Override
    public void processRecord(CSVParser parser, VersionFile versionFile, String fileEncoding, DatasetDescriptor datasetDescriptor) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport(getGLACPEMessage(PROPERTY_MSG_ERROR));
        Utils.testNotNullArguments(getLocalizationManager(), versionFile, fileEncoding);
        Utils.testCastedArguments(versionFile, VersionFile.class, getLocalizationManager());

        try {

            String[] values = null;
            long lineCount = 1;
            int variableHeaderIndex = datasetDescriptor.getUndefinedColumn();
            Map<String, ControleCoherence> controlesCoherenceMap = controleCoherenceDAO.getControleCoherenceBySitecodeAndDatatypecode(((ProjetSiteThemeDatatype) versionFile.getDataset().getLeafNode()).getProjetSiteTheme().getProjetSite().getSite()
                    .getCode(), versionFile.getDataset().getLeafNode().getDatatype().getCode());
            List<VariableGLACPE> dbVariables = buildVariableHeaderAndSkipHeader(variableHeaderIndex, parser, errorsReport);
            Map<String, List<LineRecord>> sequencesMapLines = new HashMap<String, List<LineRecord>>();

            // On parcourt chaque ligne du fichier
            while ((values = parser.getLine()) != null) {

                CleanerValues cleanerValues = new CleanerValues(values);
                lineCount++;
                List<VariableValue> variablesValues = new LinkedList<VariableValue>();

                // On parcourt chaque colonne d'une ligne
                String projetCode = Utils.createCodeFromString(cleanerValues.nextToken());
                String nomSite = cleanerValues.nextToken();
                String plateformeCode = Utils.createCodeFromString(cleanerValues.nextToken());
                String datePrelevementString = cleanerValues.nextToken();
                Date datePrelevement = datePrelevementString.length() > 0 ? new SimpleDateFormat(datasetDescriptor.getColumns().get(cleanerValues.currentTokenIndex() - 1).getFormatType()).parse(datePrelevementString) : null;
                String heureString = cleanerValues.nextToken();
                Date heure = heureString.length() > 0 ? new SimpleDateFormat(datasetDescriptor.getColumns().get(cleanerValues.currentTokenIndex() - 1).getFormatType()).parse(heureString) : null;
                String commentaire = cleanerValues.nextToken();

                for (int actualVariableArray = variableHeaderIndex; actualVariableArray < values.length; actualVariableArray++) {
                    String value = values[actualVariableArray].replaceAll(" ", "");
                    try {
                        VariableGLACPE variable = dbVariables.get(actualVariableArray - variableHeaderIndex);
                        if (controlesCoherenceMap.get(variable.getCode()) != null)
                            testValueCoherence(Float.valueOf(value), controlesCoherenceMap.get(variable.getCode()).getValeurMin(), controlesCoherenceMap.get(variable.getCode()).getValeurMax(), lineCount, 11);
                        variablesValues.add(new VariableValue(dbVariables.get(actualVariableArray - variableHeaderIndex), values[actualVariableArray]));
                    } catch (NumberFormatException e) {
                        errorsReport.addException(new BadExpectedValueException(String.format(getGLACPEMessage(PROPERTY_MSG_FLOAT_VALUE_EXPECTED), lineCount, 11, value)));
                    } catch (BadExpectedValueException e) {
                        errorsReport.addException(e);
                    }
                }

                LineRecord line = new LineRecord(nomSite, plateformeCode, datePrelevement, heure, variablesValues, lineCount, projetCode, commentaire);
                List<LineRecord> sequencesLines = null;

                String mapSequenceKey = plateformeCode.concat(projetCode).concat(datePrelevementString);

                if (sequencesMapLines.containsKey(mapSequenceKey)) {
                    sequencesLines = sequencesMapLines.get(mapSequenceKey);
                } else {
                    sequencesLines = new LinkedList<LineRecord>();
                    sequencesMapLines.put(mapSequenceKey, sequencesLines);
                }
                sequencesLines.add(line);
            }
            Allocator allocator = Allocator.getInstance();

            int count = 0;
            allocator.allocate("publish", versionFile.getFileSize());
            Iterator<Entry<String, List<LineRecord>>> iterator = sequencesMapLines.entrySet().iterator();
            while (iterator.hasNext()) {
                final Entry<String, List<LineRecord>> entry = iterator.next();
                List<LineRecord> sequenceLines = sequencesMapLines.get(entry.getKey());

                if (!sequenceLines.isEmpty()) {

                    Date datePrelevementSequence = sequenceLines.get(0).getDatePrelevement();
                    String projetCodeSequence = sequenceLines.get(0).getProjetCode();
                    String plateformeCodeSequence = sequenceLines.get(0).getPlateformeCode();
                    Date heure = sequenceLines.get(0).getHeure();
                    String commentaire = sequenceLines.get(0).getCommentaire();
                    Long lineNumber = sequenceLines.get(0).getOriginalLineNumber();
                    try {
                        buildSequence(datePrelevementSequence, projetCodeSequence, plateformeCodeSequence, heure, commentaire, lineNumber, sequenceLines, versionFile, errorsReport);
                        logger.debug(String.format(getGLACPEMessageWithBundle(BUNDLE_SOURCE_PATH_CONDITIONPRELEVEMENT, "PROPERTY_MSG_5"), count++, datePrelevementSequence));

                        // Très important à cause de problèmes de performances
                        mesureConditionGeneraleDAO.flush();
                        versionFile = (VersionFile) versionFileDAO.merge(versionFile);

                    } catch (javax.persistence.PersistenceException e) {
                        if (e.getCause().getClass().equals(ConstraintViolationException.class)) {
                            String message = String.format(getGLACPEMessage(PROPERTY_MSG_DUPLICATE_SEQUENCE), DateUtil.getSimpleDateFormatDateyyyyMMddUTC().format(datePrelevementSequence), plateformeCodeSequence);
                            errorsReport.addException(new BusinessException(message));
                        } else {
                            String message = getGLACPEMessageWithBundle(BUNDLE_SOURCE_PATH_CONDITIONPRELEVEMENT, PROPERTY_MSG_CONSTRAINT_EXCEPTION);
                            errorsReport.addException(new PersistenceException(message));
                        }
                        break;
                    } catch (InsertionDatabaseException e) {
                        errorsReport.addException(e);
                        break;
                    }
                }
                iterator.remove();
            }

            if (errorsReport.hasErrors()) {
                throw new PersistenceException(errorsReport.buildHTMLMessages());
            }
        } catch (Exception e) {

            throw new BusinessException(e);
        }

    }

    private List<VariableGLACPE> buildVariableHeaderAndSkipHeader(int variableHeaderIndex, CSVParser parser, ErrorsReport errorsReport) throws PersistenceException, IOException {
        List<String> variablesHeaders = skipHeaderAndRetrieveVariableName(variableHeaderIndex, parser);
        List<VariableGLACPE> dbVariables = new LinkedList<VariableGLACPE>();
        for (String variableHeader : variablesHeaders) {
            if (!variableHeader.equals(getGLACPEMessageWithBundle(BUNDLE_SOURCE_PATH_CONDITIONPRELEVEMENT, "PROPERTY_MSG_COMENT"))) {
                VariableGLACPE variable = (VariableGLACPE) variableDAO.getByCode(Utils.createCodeFromString(variableHeader));
                if (variable == null) {
                    errorsReport.addException(new PersistenceException(String.format(getGLACPEMessage(PROPERTY_MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS), variableHeader)));
                }

                dbVariables.add(variable);
            }
        }
        if (errorsReport.hasErrors()) {
            throw new PersistenceException(errorsReport.buildHTMLMessages());
        }
        return dbVariables;
    }

    private void buildSequence(Date datePrelevementSequence, String projetCodeSequence, String plateformeCodeSequence, Date heure, String commentaire, Long originalLineNumber, List<LineRecord> mesureLines, VersionFile versionFile,
            ErrorsReport errorsReport) throws PersistenceException, InsertionDatabaseException
    {

        LineRecord firstLine = mesureLines.get(0);
        Plateforme plateforme = null;

        ProjetSite projetSite = null;

        MesureConditionGenerale mesureConditionGenerale;
        mesureConditionGenerale = mesureConditionGeneraleDAO.getByDatePrelevementPlateformeCodeAndProjetCode(datePrelevementSequence, plateformeCodeSequence, projetCodeSequence);

        if (mesureConditionGenerale == null) {
            mesureConditionGenerale = new MesureConditionGenerale();
            plateforme = plateformeDAO.getByNKey(plateformeCodeSequence, Utils.createCodeFromString(firstLine.getNomSite()));
            if (plateforme == null) {
                InsertionDatabaseException insertionDatabaseException = new InsertionDatabaseException(String.format(getGLACPEMessage(PROPERTY_MSG_ERROR_PLATEFORME_INVALID), plateformeCodeSequence, firstLine.getOriginalLineNumber()));
                throw insertionDatabaseException;
            }
            if (!((SiteGLACPE) plateforme.getSite()).getCodeFromName().equals(Utils.createCodeFromString(firstLine.getNomSite()))) {
                String message = String.format(getGLACPEMessage(PROPERTY_MSG_ERROR_PLATEFORME_SITE_MISMATCH), plateformeCodeSequence, firstLine.getOriginalLineNumber(), plateforme.getSite().getCodeFromName());
                errorsReport.addException(new BusinessException(message));
            }

            projetSite = projetSiteDAO.getBySiteCodeAndProjetCode(((SiteGLACPE) plateforme.getSite()).getCodeFromName(), projetCodeSequence);

            if (projetSite == null) {
                errorsReport.addException(new PersistenceException(String.format(getGLACPEMessage(PROPERTY_MSG_MISSING_PROJET_SITE_IN_REFERENCES_DATAS), projetCodeSequence)));
                throw new PersistenceException(String.format(getGLACPEMessage(PROPERTY_MSG_MISSING_PROJET_SITE_IN_REFERENCES_DATAS), projetCodeSequence));
            }

            mesureConditionGenerale.setPlateforme(plateforme);
            mesureConditionGenerale.setProjetSite(projetSite);
            mesureConditionGenerale.setDatePrelevement(datePrelevementSequence);

            mesureConditionGenerale.setVersionFile(versionFile);
            mesureConditionGenerale.setHeure(heure);
            mesureConditionGenerale.setCommentaire(commentaire);
            mesureConditionGenerale.setLigneFichierEchange(originalLineNumber);

            if (!errorsReport.hasErrors()) {
                mesureConditionGeneraleDAO.saveOrUpdate(mesureConditionGenerale);
            }

        }

        for (LineRecord line : mesureLines) {
            for (VariableValue variableValue : line.getVariablesValues()) {
                ValeurConditionGenerale valeurConditionGenerale = new ValeurConditionGenerale();
                VariableGLACPE variable = (VariableGLACPE) variableDAO.merge(variableValue.getVariableGLACPE());

                if (mesureConditionGeneraleDAO.getByVariableIdAndMesureId(variable.getId(), mesureConditionGenerale.getId()) != null) {
                    String message = String.format(getGLACPEMessage(PROPERTY_MSG_DUPLICATE_SEQUENCE), DateUtil.getSimpleDateFormatDateLocale().format(datePrelevementSequence), plateformeCodeSequence);
                    errorsReport.addException(new BusinessException(message));
                    break;
                }
                valeurConditionGenerale.setVariable(variable);

                if (variableValue.getValue() == null || variableValue.getValue().length() == 0) {
                    valeurConditionGenerale.setValeur(null);
                } else {
                    if (variable.getIsQualitative()) {
                        Boolean notInList = true;
                        List<ValeurQualitative> valeursQualitative = valeurQualitativeDAO.getByCode(variable.getCode());
                        for (ValeurQualitative valeurQualitative : valeursQualitative) {
                            if (valeurQualitative.getValeur().equals(variableValue.getValue())) {
                                valeurConditionGenerale.setValeurQualitative(valeurQualitative);
                                notInList = false;
                                break;
                            }
                        }
                        if (notInList) {
                            errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(PROPERTY_MSG_NOT_IN_LIST), variableValue.getValue(), variable.getNom())));
                        }
                    } else {
                        Float value = Float.parseFloat(variableValue.getValue());
                        valeurConditionGenerale.setValeur(value);
                    }
                }

                valeurConditionGenerale.setMesure(mesureConditionGenerale);
                mesureConditionGenerale.getValeurs().add(valeurConditionGenerale);
            }
        }
    }

    /**
     *
     * @param controleCoherenceDAO
     */
    public void setControleCoherenceDAO(IControleCoherenceDAO controleCoherenceDAO) {
        this.controleCoherenceDAO = controleCoherenceDAO;
    }

    /**
     *
     * @param valeurQualitativeDAO
     */
    public void setValeurQualitativeDAO(IValeurQualitativeDAO valeurQualitativeDAO) {
        this.valeurQualitativeDAO = valeurQualitativeDAO;
    }

    /**
     *
     * @param plateformeDAO
     */
    public void setPlateformeDAO(IPlateformeDAO plateformeDAO) {
        this.plateformeDAO = plateformeDAO;
    }

    /**
     *
     * @param projetSiteDAO
     */
    public void setProjetSiteDAO(IProjetSiteDAO projetSiteDAO) {
        this.projetSiteDAO = projetSiteDAO;
    }

    /**
     *
     * @param mesureConditionGeneraleDAO
     */
    public void setMesureConditionGeneraleDAO(IMesureConditionGeneraleDAO mesureConditionGeneraleDAO) {
        this.mesureConditionGeneraleDAO = mesureConditionGeneraleDAO;
    }

}
