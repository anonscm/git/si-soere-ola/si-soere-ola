package org.inra.ecoinfo.glacpe.dataset.conditionprelevement.impl;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.AbstractExtractor;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.IConditionGeneraleDAO;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.entity.ValeurConditionGenerale;
import org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEAggregatedDatasExtractor;
import org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.impl.DefaultPPChloroTranspExtractor;
import org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.impl.PPChloroTranspParameter;
import org.inra.ecoinfo.glacpe.extraction.vo.AbstractDatesFormParam.Periode;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO;
import org.inra.ecoinfo.glacpe.extraction.vo.VariableVO;
import org.inra.ecoinfo.glacpe.refdata.projetsite.ProjetSite;
import org.inra.ecoinfo.security.entity.ISecurityPathWithVariable;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class TranspAggregatedDatasExtractor extends AbstractGLACPEAggregatedDatasExtractor {

    /**
     *
     */
    public static String CST_RESULT_EXTRACTION_CODE = "transpAggregatedDatas";

    /**
     *
     */
    protected static final String MAP_INDEX_0 = "0";

    /**
     *
     */
    protected static final String PROPERTY_MSG_NO_EXTRACTION_RESULT = "PROPERTY_MSG_NO_EXTRACTION_RESULT";

    /**
     *
     */
    protected IConditionGeneraleDAO conditionGeneraleDAO;

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public void extract(IParameter parameters) throws BusinessException {
        prepareRequestMetadatas(parameters.getParameters());
        Map<String, List> resultsDatasMap = extractDatas(parameters.getParameters());
        Map<String, List> filteredResultsDatasMap = filterExtractedDatas(resultsDatasMap);
        if (((PPChloroTranspParameter) parameters).getResults().get(CST_RESULT_EXTRACTION_CODE) == null || ((PPChloroTranspParameter) parameters).getResults().get(CST_RESULT_EXTRACTION_CODE).isEmpty())
            ((PPChloroTranspParameter) parameters).getResults().put(CST_RESULT_EXTRACTION_CODE, filteredResultsDatasMap);
        else
            ((PPChloroTranspParameter) parameters).getResults().get(CST_RESULT_EXTRACTION_CODE).put(CST_RESULT_EXTRACTION_CODE, filteredResultsDatasMap.get(CST_RESULT_EXTRACTION_CODE));
        if (((PPChloroTranspParameter) parameters).getParameters().get(DefaultPPChloroTranspExtractor.CST_RESULTS) == null) {
            Map<String, Boolean> resultTrack = new HashMap<String, Boolean>();
            resultTrack.put(CST_RESULT_EXTRACTION_CODE, filteredResultsDatasMap.get(CST_RESULT_EXTRACTION_CODE) != null);
            ((PPChloroTranspParameter) parameters).getParameters().put(DefaultPPChloroTranspExtractor.CST_RESULTS, resultTrack);
        } else {
            ((Map<String, Boolean>) ((PPChloroTranspParameter) parameters).getParameters().get(DefaultPPChloroTranspExtractor.CST_RESULTS)).put(CST_RESULT_EXTRACTION_CODE, filteredResultsDatasMap.get(CST_RESULT_EXTRACTION_CODE) != null);
        }
        ((PPChloroTranspParameter) parameters).getResults().get(CST_RESULT_EXTRACTION_CODE).put(MAP_INDEX_0, filteredResultsDatasMap.get(CST_RESULT_EXTRACTION_CODE));

    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, List> extractDatas(Map<String, Object> requestMetadatasMap) throws NoExtractionResultException, BusinessException {

        Map<String, List> extractedDatasMap = new HashMap<String, List>();
        Map<String, List<VariableVO>> selectedVariablesList = (Map<String, List<VariableVO>>) requestMetadatasMap.get(VariableVO.class.getSimpleName());
        List<VariableVO> selectedVariables = (List<VariableVO>) selectedVariablesList.get(PPChloroTranspParameter.TRANSPARENCE);
        List<PlateformeVO> selectedPlateformes = (List<PlateformeVO>) requestMetadatasMap.get(PlateformeVO.class.getSimpleName());
        List<Long> selectedVariablesIds = buildVariablesIds(selectedVariables);
        List<Long> selectedProjetSiteIds = (List<Long>) requestMetadatasMap.get(ProjetSite.class.getSimpleName());
        List<Long> plateformesIds = buildPlateformesIds(selectedPlateformes);
        DatesRequestParamVO datesRequestParamVO = (DatesRequestParamVO) requestMetadatasMap.get(DatesRequestParamVO.class.getSimpleName());

        List<List<ValeurConditionGenerale>> valeursConditionGenerales = null;
        try {
            for (Periode datesMap : datesRequestParamVO.getCurrentDatesFormParam().getPeriodsFromDateFormParameter()) {
                List<ValeurConditionGenerale> tempValue = null;
                tempValue = conditionGeneraleDAO.extractAggregatedDatas(plateformesIds, selectedProjetSiteIds, selectedVariablesIds, datesMap.getDateStart(), datesMap.getDateEnd());
                if (tempValue != null && !tempValue.isEmpty()) {
                    if (valeursConditionGenerales == null) {
                        valeursConditionGenerales = new LinkedList<List<ValeurConditionGenerale>>();
                    }
                    valeursConditionGenerales.add(tempValue);
                }

            }

            extractedDatasMap.put(CST_RESULT_EXTRACTION_CODE, valeursConditionGenerales);
            if (valeursConditionGenerales == null || valeursConditionGenerales.isEmpty()) {
                throw new NoExtractionResultException(getLocalizationManager().getMessage(NoExtractionResultException.BUNDLE_SOURCE_PATH, PROPERTY_MSG_NO_EXTRACTION_RESULT));
            }
        } catch (PersistenceException e) {
            AbstractExtractor.LOGGER.error(e.getMessage());
            throw new BusinessException(e);
        }
        return extractedDatasMap;
    }

    @SuppressWarnings("rawtypes")
    @Override
    protected Map<String, List> filterExtractedDatas(Map<String, List> resultsDatasMap) throws BusinessException {
        filterExtractedDatas(resultsDatasMap, CST_RESULT_EXTRACTION_CODE);
        return resultsDatasMap;
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, List> filterExtractedDatas(final Map<String, List> resultsDatasMap, final String index) {
        if (securityContext.isRoot()) {
            return resultsDatasMap;
        }
        List<List<ISecurityPathWithVariable>> returnValues = new LinkedList<List<ISecurityPathWithVariable>>();

        for (List<ISecurityPathWithVariable> valeurs : (List<List<ISecurityPathWithVariable>>) resultsDatasMap.get(index)) {
            List<ISecurityPathWithVariable> returnValue = new LinkedList<ISecurityPathWithVariable>();
            for (ISecurityPathWithVariable valeur : valeurs) {
                filterSecurityPath(returnValue, valeur);
            }
            returnValues.add(returnValue);
        }
        resultsDatasMap.put(index, returnValues);
        return resultsDatasMap;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void prepareRequestMetadatas(Map<String, Object> requestMetadatasMap) throws BusinessException {
        Collections.sort(((Map<String, List<VariableVO>>) requestMetadatasMap.get(VariableVO.class.getSimpleName())).get(PPChloroTranspParameter.TRANSPARENCE), new Comparator<VariableVO>() {

            @Override
            public int compare(VariableVO o1, VariableVO o2) {
                return o1.getCode().compareTo(o2.getCode());
            }
        });
    }

    private List<Long> buildPlateformesIds(List<PlateformeVO> selectedPlateformes) {
        List<Long> plateformesIds = new LinkedList<Long>();
        for (PlateformeVO plateforme : selectedPlateformes) {
            plateformesIds.add(plateforme.getId());
        }
        return plateformesIds;
    }

    private List<Long> buildVariablesIds(List<VariableVO> selectedVariables) {
        List<Long> variablesIds = new LinkedList<Long>();
        for (VariableVO variable : selectedVariables) {
            variablesIds.add(variable.getId());
        }
        return variablesIds;
    }

    /**
     *
     * @return
     */
    public IConditionGeneraleDAO getConditionGeneraleDAO() {
        return conditionGeneraleDAO;
    }

    /**
     *
     * @param conditionGeneraleDAO
     */
    public void setConditionGeneraleDAO(IConditionGeneraleDAO conditionGeneraleDAO) {
        this.conditionGeneraleDAO = conditionGeneraleDAO;
    }
}
