package org.inra.ecoinfo.glacpe.dataset.conditionprelevement.impl;

import java.io.File;
import java.io.PrintStream;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.entity.ValeurConditionGenerale;
import org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEAggregatedDatasOutputBuilder;
import org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.impl.PPChloroTranspParameter;
import org.inra.ecoinfo.glacpe.extraction.vo.DatasRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO;
import org.inra.ecoinfo.glacpe.extraction.vo.VariableVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.IDatatypeVariableUniteGLACPEDAO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.ErrorsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import com.google.common.base.Strings;

/**
 *
 * @author ptcherniati
 */
public class TranspAggregatedDatasOutputBuilder extends AbstractGLACPEAggregatedDatasOutputBuilder {

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.glacpe.dataset.messages";
    private static final String BUNDLE_SOURCE_PATH_TRANSP = "org.inra.ecoinfo.glacpe.extraction.conditionprelevement.messages";

    /**
     *
     */
    protected static final String SUFFIX_FILENAME_AGGREGATED = "aggregated";
    private static final String CODE_DATATYPE_TRANSP = "transparence";
    private static final String CODE_DATATYPE_CONDITION_GENERALE = "condition_prelevement";

    /**
     *
     */
    protected static final String END_PREFIX = "end";

    /**
     *
     */
    protected static final String START_PREFIX = "start";

    private static final String HEADER_AGGREGATED_DATA = "PROPERTY_MSG_HEADER_RAW_TRANSP";
    private static final String MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS = "PROPERTY_MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS";

    /**
     *
     */
    protected static final String PROPERTY_MSG_ERROR = "PROPERTY_MSG_ERROR";

    private static final String SUFFIX_FILENAME_TRANSP_AGGREGATED = SUFFIX_FILENAME_AGGREGATED.concat(SEPARATOR_TEXT).concat(CODE_DATATYPE_TRANSP);

    /**
     *
     */
    protected IVariableDAO variableDAO;

    /**
     *
     */
    protected IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO;

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException, NoExtractionResultException {
        ((DefaultParameter) parameters).getFilesMaps().add(super.buildOutput(parameters, TranspAggregatedDatasExtractor.CST_RESULT_EXTRACTION_CODE));
        return null;
    }


    @SuppressWarnings("unchecked")
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatasMap) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_ERROR));
        Properties propertiesVariableName = localizationManager.newProperties(Variable.NAME_ENTITY_JPA, "nom");
        Map<String, List<VariableVO>> selectedVariablesList = (Map<String, List<VariableVO>>) requestMetadatasMap.get(VariableVO.class.getSimpleName());

        List<VariableVO> selectedVariables = (List<VariableVO>) selectedVariablesList.get(PPChloroTranspParameter.TRANSPARENCE);
        DatasRequestParamVO datasRequestParamVO = (DatasRequestParamVO) requestMetadatasMap.get(DatasRequestParamVO.class.getSimpleName());

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format("%s", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_TRANSP, HEADER_AGGREGATED_DATA)));
        try {
            for (VariableVO variableVO : selectedVariables) {
                VariableGLACPE variable = (VariableGLACPE) variableDAO.getByCode(variableVO.getCode());
                String uniteNom;
                if (variable == null) {
                    errorsReport.addException(new BusinessException(String.format(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_TRANSP, MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS), selectedVariables)));
                }
                String localizedVariableName = propertiesVariableName.getProperty(variable.getNom());
                if (Strings.isNullOrEmpty(localizedVariableName))
                    localizedVariableName = variable.getNom();

                Unite unite = datatypeVariableUniteDAO.getUnite(CODE_DATATYPE_CONDITION_GENERALE, variable.getCode());
                if (unite == null) {
                    uniteNom = "nounit";
                } else {
                    uniteNom = unite.getCode();
                }

                if (datasRequestParamVO.getMinValueAndAssociatedDepth()) {
                    stringBuilder.append(String.format(";minimum(%s) (%s)", localizedVariableName, uniteNom));
                }
                if (datasRequestParamVO.getMaxValueAndAssociatedDepth()) {
                    stringBuilder.append(String.format(";maximum(%s) (%s)", localizedVariableName, uniteNom));
                }
            }
            if (errorsReport.hasErrors()) {
                throw new PersistenceException(errorsReport.getMessages());
            }
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
        return stringBuilder.toString();
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {
        Map<String, List<VariableVO>> selectedVariablesList = (Map<String, List<VariableVO>>) requestMetadatasMap.get(VariableVO.class.getSimpleName());

        List<VariableVO> selectedVariables = (List<VariableVO>) selectedVariablesList.get(PPChloroTranspParameter.TRANSPARENCE);
        DatasRequestParamVO datasRequestParamVO = (DatasRequestParamVO) requestMetadatasMap.get(DatasRequestParamVO.class.getSimpleName());

        Map<Date, Map<Long, Map<String, ValeurConditionGenerale>>> valeursMesures = buildAggregatedData(resultsDatasMap.get(TranspAggregatedDatasExtractor.CST_RESULT_EXTRACTION_CODE));

        Properties propertiesProjetName = localizationManager.newProperties(Projet.NAME_TABLE, "nom");
        Properties propertiesPlateformeName = localizationManager.newProperties(Plateforme.TABLE_NAME, "nom");
        Properties propertiesSiteName = localizationManager.newProperties(Site.NAME_ENTITY_JPA, "nom");

        String minSeparator = buildCSVSeparator(datasRequestParamVO.getMinValueAndAssociatedDepth());
        String maxSeparator = buildCSVSeparator(datasRequestParamVO.getMaxValueAndAssociatedDepth());

        List<PlateformeVO> selectedPlateformes = (List<PlateformeVO>) requestMetadatasMap.get(PlateformeVO.class.getSimpleName());
        Set<String> sitesNames = retrieveSitesCodes(selectedPlateformes);
        Map<String, File> filesMap = buildOutputsFiles(sitesNames, SUFFIX_FILENAME_TRANSP_AGGREGATED);
        Map<String, PrintStream> outputPrintStreamMap = buildOutputPrintStreamMap(filesMap);
        for (String siteName : outputPrintStreamMap.keySet()) {
            outputPrintStreamMap.get(siteName).println(headers);
        }

        List<Date> dates = new LinkedList(valeursMesures.keySet());
        Collections.sort(dates);

        for (Date date : dates) {
            for (VariableVO variable : selectedVariables) {
                if (valeursMesures.get(date).get(variable.getId()) != null) {
                    ValeurConditionGenerale valeurRef = null;
                    if (valeursMesures.get(date).get(variable.getId()).get("min") == null) {
                        valeurRef = valeursMesures.get(date).get(variable.getId()).get("max");
                    } else {
                        valeurRef = valeursMesures.get(date).get(variable.getId()).get("min");
                    }
                    PrintStream rawDataPrintStream = outputPrintStreamMap.get(((SiteGLACPE) valeurRef.getMesure().getSite()).getCodeFromName());
                    String line;
                    String localizedProjetName = propertiesProjetName.getProperty(valeurRef.getMesure().getProjetSite().getProjet().getNom());
                    if (Strings.isNullOrEmpty(localizedProjetName))
                        localizedProjetName = valeurRef.getMesure().getProjetSite().getProjet().getNom();

                    String localizedSiteName = propertiesSiteName.getProperty(valeurRef.getMesure().getProjetSite().getSite().getNom());
                    if (Strings.isNullOrEmpty(localizedSiteName))
                        localizedSiteName = valeurRef.getMesure().getProjetSite().getSite().getNom();

                    String localizedPlateformeName = propertiesPlateformeName.getProperty(valeurRef.getMesure().getPlateforme().getNom());
                    if (Strings.isNullOrEmpty(localizedPlateformeName))
                        localizedPlateformeName = valeurRef.getMesure().getPlateforme().getNom();

                    line = String.format("%s;%s;%s;%s;%s", localizedProjetName, localizedSiteName, localizedPlateformeName, DateUtil.getSimpleDateFormatDateLocale().format(date),
                            DateUtil.getSimpleDateFormatTimeLocale().format(valeurRef.getMesure().getHeure()));

                    rawDataPrintStream.print(line);
                    for (VariableVO var : selectedVariables) {
                        if (var.equals(variable)) {
                            if (datasRequestParamVO.getMinValueAndAssociatedDepth()) {
                                if (valeursMesures.get(date).get(variable.getId()).get("min") != null)
                                    rawDataPrintStream.print(String.format(";%s", valeursMesures.get(date).get(variable.getId()).get("min").getValeur()));
                                else
                                    rawDataPrintStream.print(minSeparator);
                            }
                            if (datasRequestParamVO.getMaxValueAndAssociatedDepth()) {
                                if (valeursMesures.get(date).get(variable.getId()).get("max") != null)
                                    rawDataPrintStream.print(String.format(";%s", valeursMesures.get(date).get(variable.getId()).get("max").getValeur()));
                                else
                                    rawDataPrintStream.print(maxSeparator);
                            }
                        } else {
                            rawDataPrintStream.print(String.format("%s%s", minSeparator, maxSeparator));
                        }
                    }
                    rawDataPrintStream.println();
                }
            }
        }

        closeStreams(outputPrintStreamMap);
        return filesMap;
    }

    /**
     *
     * @param listValeursMesures
     * @return
     */
    public Map<Date, Map<Long, Map<String, ValeurConditionGenerale>>> buildAggregatedData(List<List<ValeurConditionGenerale>> listValeursMesures) {
        Map<Date, Map<Long, Map<String, ValeurConditionGenerale>>> sortedMapAggregated = new HashMap<Date, Map<Long, Map<String, ValeurConditionGenerale>>>();

        Iterator<List<ValeurConditionGenerale>> iteratorList = listValeursMesures.iterator();
        while (iteratorList.hasNext()) {
            List<ValeurConditionGenerale> valeurs = iteratorList.next();
            Map<Long, List<ValeurConditionGenerale>> listValeurs = new HashMap<Long, List<ValeurConditionGenerale>>();
            for (ValeurConditionGenerale valeur : valeurs) {
                if (listValeurs.get(valeur.getVariable().getId()) == null) {
                    List<ValeurConditionGenerale> valeursByVariables = new LinkedList<ValeurConditionGenerale>();
                    listValeurs.put(valeur.getVariable().getId(), valeursByVariables);
                }
                listValeurs.get(valeur.getVariable().getId()).add(valeur);
            }

            Iterator<Long> iterator = listValeurs.keySet().iterator();
            while (iterator.hasNext()) {
                Long id = iterator.next();
                ValeurConditionGenerale minValue = getMinValue(listValeurs.get(id));
                ValeurConditionGenerale maxValue = getMaxValue(listValeurs.get(id));
                if (sortedMapAggregated.get(minValue.getDatePrelevement()) == null) {
                    Map<Long, Map<String, ValeurConditionGenerale>> minMapByValue = new HashMap<Long, Map<String, ValeurConditionGenerale>>();
                    sortedMapAggregated.put(minValue.getDatePrelevement(), minMapByValue);
                }
                if (sortedMapAggregated.get(minValue.getDatePrelevement()).get(id) == null) {
                    Map<String, ValeurConditionGenerale> minMap = new HashMap<String, ValeurConditionGenerale>();
                    sortedMapAggregated.get(minValue.getDatePrelevement()).put(id, minMap);
                }
                sortedMapAggregated.get(minValue.getDatePrelevement()).get(id).put("min", minValue);

                if (sortedMapAggregated.get(maxValue.getDatePrelevement()) == null) {
                    Map<Long, Map<String, ValeurConditionGenerale>> maxMapByValue = new HashMap<Long, Map<String, ValeurConditionGenerale>>();
                    sortedMapAggregated.put(maxValue.getDatePrelevement(), maxMapByValue);
                }
                if (sortedMapAggregated.get(maxValue.getDatePrelevement()).get(id) == null) {
                    Map<String, ValeurConditionGenerale> maxMap = new HashMap<String, ValeurConditionGenerale>();
                    sortedMapAggregated.get(maxValue.getDatePrelevement()).put(id, maxMap);
                }
                sortedMapAggregated.get(maxValue.getDatePrelevement()).get(id).put("max", maxValue);
                iterator.remove();
            }
            iteratorList.remove();
        }

        return sortedMapAggregated;
    }

    private ValeurConditionGenerale getMinValue(List<ValeurConditionGenerale> valeurs) {
        ValeurConditionGenerale minValue = null;
        for (ValeurConditionGenerale valeur : valeurs) {
            if (valeur.getValeur() != null) {
                if (minValue == null) {
                    minValue = valeur;
                }
                if (minValue.getValeur().compareTo(valeur.getValeur()) > 0) {
                    minValue = valeur;
                }
            }
        }
        return minValue;
    }

    private ValeurConditionGenerale getMaxValue(List<ValeurConditionGenerale> valeurs) {
        ValeurConditionGenerale maxValue = null;
        for (ValeurConditionGenerale valeur : valeurs) {
            if (valeur.getValeur() != null) {
                if (maxValue == null) {
                    maxValue = valeur;
                }
                if (maxValue.getValeur().compareTo(valeur.getValeur()) < 0) {
                    maxValue = valeur;
                }
            }
        }
        return maxValue;
    }

    /**
     *
     * @param variableDAO
     */
    public void setVariableDAO(IVariableDAO variableDAO) {
        this.variableDAO = variableDAO;
    }

    /**
     *
     * @param datatypeVariableUniteDAO
     */
    public void setDatatypeVariableUniteDAO(IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO) {
        this.datatypeVariableUniteDAO = datatypeVariableUniteDAO;
    }
}
