package org.inra.ecoinfo.glacpe.dataset.conditionprelevement.impl;

import java.io.File;
import java.io.PrintStream;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.entity.MesureConditionGenerale;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.entity.ValeurConditionGenerale;
import org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.impl.AbstractPPChloroTranspRawsDatasOutputBuilder;
import org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.impl.PPChloroTranspParameter;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO;
import org.inra.ecoinfo.glacpe.extraction.vo.VariableVO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import com.google.common.base.Strings;

/**
 *
 * @author ptcherniati
 */
public class TranspRawsDatasOutputBuilderByRow extends AbstractPPChloroTranspRawsDatasOutputBuilder {

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH_CONDITIONPRELEVEMENT = "org.inra.ecoinfo.glacpe.extraction.conditionprelevement.messages";

    private static final String HEADER_RAW_DATA = "PROPERTY_MSG_HEADER_RAW_TRANSP";
    private static final String MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS = "PROPERTY_MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS";

    private static final String CODE_TRANSPARENCE = "transparence";
    private static final String CODE_CONDITION_GENERALE = "conditions_prelevements";
    private static final String SUFFIX_FILENAME = CODE_TRANSPARENCE;
    private IVariableDAO variableDAO;

    @SuppressWarnings("unchecked")
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatas) throws BusinessException {

        ErrorsReport errorsReport = new ErrorsReport();
        Properties propertiesVariableName = localizationManager.newProperties(Variable.NAME_ENTITY_JPA, "nom");
        Map<String, List<VariableVO>> selectedVariablesList = (Map<String, List<VariableVO>>) requestMetadatas.get(VariableVO.class.getSimpleName());

        List<VariableVO> selectedVariables = (List<VariableVO>) selectedVariablesList.get(PPChloroTranspParameter.TRANSPARENCE);

        StringBuilder stringBuilder = new StringBuilder();
        try {
            stringBuilder.append(String.format("%s", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_CONDITIONPRELEVEMENT, HEADER_RAW_DATA)));

            for (VariableVO variableVO : selectedVariables) {
                VariableGLACPE variable = (VariableGLACPE) variableDAO.getByCode(variableVO.getCode());
                String uniteNom;
                if (variable == null) {
                    errorsReport.addErrorMessage((String.format(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_CONDITIONPRELEVEMENT, MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS), selectedVariables)));
                }
                String localizedVariableName = propertiesVariableName.getProperty(variable.getNom());
                if (Strings.isNullOrEmpty(localizedVariableName))
                    localizedVariableName = variable.getNom();

                Unite unite = datatypeVariableUniteDAO.getUnite(CODE_CONDITION_GENERALE, variable.getCode());
                if (unite == null) {
                    uniteNom = "nounit";
                } else {
                    uniteNom = unite.getCode();
                }

                stringBuilder.append(String.format(";%s (%s)", localizedVariableName, uniteNom));
                if (errorsReport.hasErrors()) {
                    throw new PersistenceException(errorsReport.getErrorsMessages());
                }
            }
        } catch (Exception e) {
            throw new BusinessException(e);
        }
        return stringBuilder.toString();

    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {

        Map<String, List<VariableVO>> selectedVariablesList = (Map<String, List<VariableVO>>) requestMetadatasMap.get(VariableVO.class.getSimpleName());

        List<VariableVO> selectedVariables = (List<VariableVO>) selectedVariablesList.get(PPChloroTranspParameter.TRANSPARENCE);
        Iterator<MesureConditionGenerale> mesuresTransparence = resultsDatasMap.get(TranspRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE).iterator();

        Properties propertiesProjetName = localizationManager.newProperties(Projet.NAME_TABLE, "nom");
        Properties propertiesPlateformeName = localizationManager.newProperties(Plateforme.TABLE_NAME, "nom");
        Properties propertiesSiteName = localizationManager.newProperties(Site.NAME_ENTITY_JPA, "nom");

        List<PlateformeVO> selectedPlateformes = (List<PlateformeVO>) requestMetadatasMap.get(PlateformeVO.class.getSimpleName());
        Set<String> sitesNames = retrieveSitesCodes(selectedPlateformes);
        Map<String, File> filesMap = buildOutputsFiles(sitesNames, SUFFIX_FILENAME);
        Map<String, PrintStream> outputPrintStreamMap = buildOutputPrintStreamMap(filesMap);
        for (String siteName : outputPrintStreamMap.keySet()) {
            outputPrintStreamMap.get(siteName).println(headers);
        }
        while (mesuresTransparence.hasNext()) {
            final MesureConditionGenerale mesureTransparence = mesuresTransparence.next();
            PrintStream rawDataPrintStream = outputPrintStreamMap.get(((SiteGLACPE) mesureTransparence.getProjetSite().getSite()).getCodeFromName());
            String line;

            String localizedProjetName = propertiesProjetName.getProperty(mesureTransparence.getProjetSite().getProjet().getNom());
            if (Strings.isNullOrEmpty(localizedProjetName))
                localizedProjetName = mesureTransparence.getProjetSite().getProjet().getNom();

            String localizedSiteName = propertiesSiteName.getProperty(mesureTransparence.getProjetSite().getSite().getNom());
            if (Strings.isNullOrEmpty(localizedSiteName))
                localizedSiteName = mesureTransparence.getProjetSite().getSite().getNom();

            String localizedPlateformeName = propertiesPlateformeName.getProperty(mesureTransparence.getPlateforme().getNom());
            if (Strings.isNullOrEmpty(localizedPlateformeName))
                localizedPlateformeName = mesureTransparence.getPlateforme().getNom();

            // nom projet;nom site;nom plateforme;date prélèvement,heure prélèvement
            line = String.format("%s;%s;%s;%s;%s", localizedProjetName, localizedSiteName, localizedPlateformeName, DateUtil.getSimpleDateFormatDateLocale().format(mesureTransparence.getDatePrelevement()), mesureTransparence.getHeure() != null
                    ? DateUtil.getSimpleDateFormatTimeLocale().format(mesureTransparence.getHeure())
                    : "");

            rawDataPrintStream.print(line);

            List<ValeurConditionGenerale> valeursTransparence = mesureTransparence.getValeurs();
            for (VariableVO variableVO : selectedVariables) {
                rawDataPrintStream.print(";");

                for (ValeurConditionGenerale valeurTransparence : valeursTransparence) {
                    if (valeurTransparence.getVariable().getId().equals(variableVO.getId())) {
                        if (valeurTransparence.getValeur() != null) {
                            rawDataPrintStream.print(String.format("%s", valeurTransparence.getValeur()));
                        } else {
                            rawDataPrintStream.print("");
                        }
                        break;
                    }
                }
            }
            rawDataPrintStream.println();
            // commenter ?
            //mesuresTransparence.remove();
        }

        closeStreams(outputPrintStreamMap);
        return filesMap;
    }

    class ErrorsReport {

        private String errorsMessages = new String();

        public void addErrorMessage(String errorMessage) {
            errorsMessages = errorsMessages.concat("-").concat(errorMessage).concat("\n");
        }

        public String getErrorsMessages() {
            return errorsMessages;
        }

        public boolean hasErrors() {
            return (errorsMessages.length() > 0);
        }
    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException, NoExtractionResultException {
        if (((DefaultParameter) parameters).getResults().get(TranspRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE).get(TranspRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE) == null
                || ((DefaultParameter) parameters).getResults().get(TranspRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE).get(TranspRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE).isEmpty())
            return null;
        ((DefaultParameter) parameters).getFilesMaps().add(super.buildOutput(parameters, TranspRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE));
        return null;

    }

    /**
     *
     * @param selectedPlateformes
     * @return
     */
    protected Set<String> retrieveSitesCodes(List<PlateformeVO> selectedPlateformes) {
        Set<String> sitesNames = new HashSet<String>();
        for (PlateformeVO plateforme : selectedPlateformes) {
            sitesNames.add(Utils.createCodeFromString(plateforme.getNomSite()));
        }
        return sitesNames;
    }

    /**
     *
     * @param variableDAO
     */
    public void setVariableDAO(IVariableDAO variableDAO) {
        this.variableDAO = variableDAO;
    }
}
