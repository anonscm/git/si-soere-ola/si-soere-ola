package org.inra.ecoinfo.glacpe.dataset.conditionprelevement.jpa;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.IConditionGeneraleDAO;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.entity.MesureConditionGenerale;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.entity.ValeurConditionGenerale;
import org.inra.ecoinfo.glacpe.extraction.vo.AbstractDatesFormParam;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesRequestParamVO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.projetsite.ProjetSite;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
@SuppressWarnings("unchecked")
public class JPAConditionGeneraleDAO extends AbstractJPADAO<Object> implements IConditionGeneraleDAO {

    private static final String REQUEST_CONDITIONGENERALE_AVAILABLES_PROJETS = "select distinct m.projetSite.projet from MesureConditionGenerale m";
    private static final String REQUEST_CONDITIONGENERALE_AVAILABLES_PLATEFORMES_BY_PROJETSITE_ID = "select distinct m.plateforme from MesureConditionGenerale m where m.projetSite.id = :projetSiteId";
    private static final String REQUEST_CONDITIONGENERALE_PROJETSITE_BY_PROJET_ID_AND_SITE_ID = "select distinct m.projetSite from MesureConditionGenerale m where m.projetSite.projet.id=:projetId and m.projetSite.site.id=:siteId ";
    private static final String REQUEST_CONDITIONGENERALE_AVAILABLES_VARIABLES_BY_PLATEFORMES_ID = "select distinct vm.variable from ValeurConditionGenerale vm where vm.mesure.plateforme.id in (:plateformesIds) and vm.mesure.datePrelevement between :firstDate and :lastDate and vm.valeur is not null";
    private static final String REQUEST_Extract = "select m from MesureConditionGenerale m where m.plateforme.id in (:selectedPlateformesIds) and m.projetSite.id in (:selectedProjetId) %s  order by m.projetSite.projet.nom,m.plateforme.site.nom,m.plateforme.nom,m.datePrelevement";
    private static final String REQUEST_Extract_Aggregated = "select v from ValeurConditionGenerale v where v.mesure.plateforme.id in (:selectedPlateformesIds) and v.mesure.projetSite.id in (:selectedProjetId) and v.variable.id in (:selectedVariableId) and v.mesure.datePrelevement between :dateStart and :dateEnd order by v.mesure.projetSite.projet.nom,v.mesure.plateforme.site.nom,v.mesure.plateforme.nom,v.mesure.datePrelevement, v.variable.code";

    private static final String HQL_DATE_FIELD_2 = "m.datePrelevement";
    private static final String REQUEST_PROJECT_TRANSPARENCE = "select distinct vcg.mesure.projetSite.projet from ValeurConditionGenerale vcg where vcg.variable.code in('transparence_par_disque_inra','transparence_par_secchi_20_cm')";
    private static final String REQUEST_VARIABLES_TRANSPARENCE = "select distinct vcg.variable from ValeurConditionGenerale vcg where vcg.variable.code in('transparence_par_disque_inra','transparence_par_secchi_20_cm') and vcg.mesure.plateforme.id in (:plateformesIds) and vcg.mesure.datePrelevement between :firstDate and :lastDate order by vcg.variable.ordreAffichageGroupe";

    /**
     *
     */
    protected static final String DATE_FORMAT = "dd/MM/yyyy";

    /**
     *
     */
    protected static final DateFormat dateFormatter = new SimpleDateFormat(DATE_FORMAT);

    /**
     *
     * @return
     * @throws PersistenceException
     */
    @Override
    public List<Projet> retrieveConditionGeneraleAvailablesProjets() throws PersistenceException {
        try {

            Query query = entityManager.createQuery(REQUEST_CONDITIONGENERALE_AVAILABLES_PROJETS);
            List<Projet> projets = query.getResultList();

            return projets;
        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param selectedPlateformesIds
     * @param projetSiteId
     * @param datesRequestParamVO
     * @return
     * @throws PersistenceException
     */
    @Override
    public List<MesureConditionGenerale> extractDatas(List<Long> selectedPlateformesIds, List<Long> projetSiteId, DatesRequestParamVO datesRequestParamVO) throws PersistenceException {
        try {
            AbstractDatesFormParam datesFormParam = datesRequestParamVO.getCurrentDatesFormParam();
            Query query = entityManager.createQuery(String.format(REQUEST_Extract, datesFormParam.buildSQLCondition(HQL_DATE_FIELD_2)));
            Map<String, Date> datesMap = datesFormParam.retrieveParametersMap();

            for (String key : datesMap.keySet()) {
                query.setParameter(key, datesMap.get(key));
            }

            query.setParameter("selectedPlateformesIds", selectedPlateformesIds);
            query.setParameter("selectedProjetId", projetSiteId);

            return query.getResultList();

        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param projetSiteId
     * @return
     * @throws PersistenceException
     */
    @Override
    public List<Plateforme> retrieveConditionGeneraleAvailablesPlateformesByProjetSiteId(long projetSiteId) throws PersistenceException {
        try {

            Query query = entityManager.createQuery(REQUEST_CONDITIONGENERALE_AVAILABLES_PLATEFORMES_BY_PROJETSITE_ID);

            query.setParameter("projetSiteId", projetSiteId);
            List<Plateforme> plateformes = query.getResultList();

            return plateformes;
        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param projetId
     * @param siteId
     * @return
     * @throws PersistenceException
     */
    @Override
    public ProjetSite retrieveConditionGeneraleAvailablesProjetsSiteByProjetAndSite(Long projetId, Long siteId) throws PersistenceException {
        try {

            Query query = entityManager.createQuery(REQUEST_CONDITIONGENERALE_PROJETSITE_BY_PROJET_ID_AND_SITE_ID);

            query.setParameter("projetId", projetId);
            query.setParameter("siteId", siteId);
            List<ProjetSite> projetSite = query.getResultList();

            return projetSite.get(0);
        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     *
     * @return
     * @throws PersistenceException
     */
    @Override
    public List<Projet> retrieveAvailablesProjets() throws PersistenceException {
        List<Projet> projets = entityManager.createQuery(REQUEST_PROJECT_TRANSPARENCE).getResultList();
        return projets;
    }

    /**
     *
     * @param plateformesIds
     * @param firstDate
     * @param endDate
     * @return
     * @throws PersistenceException
     */
    @Override
    public List<VariableGLACPE> retrieveAvailablesTransparenceVariables(List<Long> plateformesIds, Date firstDate, Date endDate) throws PersistenceException {
        Query query = entityManager.createQuery(REQUEST_VARIABLES_TRANSPARENCE);
        query.setParameter("plateformesIds", plateformesIds);
        query.setParameter("firstDate", firstDate);
        query.setParameter("lastDate", endDate);
        List<VariableGLACPE> variables = query.getResultList();
        return variables;
    }

    /**
     *
     * @param plateformesIds
     * @param firstDate
     * @param lastDate
     * @return
     * @throws PersistenceException
     */
    @Override
    public List<VariableGLACPE> retrieveSondeMultiAvailablesVariables(List<Long> plateformesIds, Date firstDate, Date lastDate) throws PersistenceException {
        List<VariableGLACPE> variables;
        try {
            Query query = entityManager.createQuery(REQUEST_CONDITIONGENERALE_AVAILABLES_VARIABLES_BY_PLATEFORMES_ID);

            query.setParameter("plateformesIds", plateformesIds);
            query.setParameter("firstDate", firstDate);
            query.setParameter("lastDate", lastDate);
            variables = query.getResultList();
            return variables;
        } catch (Exception e) {
            AbstractJPADAO.LOGGER.error(e);
            throw new PersistenceException(e);
        }
    }

    /**
     *
     * @param plateformesIds
     * @param selectedProjetSiteIds
     * @param variablesIds
     * @param dateStart
     * @param dateEnd
     * @return
     * @throws PersistenceException
     */
    @Override
    public List<ValeurConditionGenerale> extractAggregatedDatas(List<Long> plateformesIds, List<Long> selectedProjetSiteIds, List<Long> variablesIds, String dateStart, String dateEnd) throws PersistenceException {
        try {
            Query query = entityManager.createQuery(REQUEST_Extract_Aggregated);
            query.setParameter("selectedPlateformesIds", plateformesIds);
            query.setParameter("selectedProjetId", selectedProjetSiteIds);
            query.setParameter("selectedVariableId", variablesIds);
            query.setParameter("dateStart", dateFormatter.parse(dateStart));
            query.setParameter("dateEnd", dateFormatter.parse(dateEnd));

            return (List<ValeurConditionGenerale>) query.getResultList();

        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }
}
