package org.inra.ecoinfo.glacpe.dataset.conditionprelevement.jpa;

import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.IMesureConditionGeneraleDAO;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.entity.MesureConditionGenerale;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.entity.ValeurConditionGenerale;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class JPAMesureConditionGeneraleDAO extends AbstractJPADAO<MesureConditionGenerale> implements IMesureConditionGeneraleDAO {

    /**
     *
     * @param datePrelevement
     * @param plateformeCode
     * @param projetCode
     * @return
     * @throws PersistenceException
     */
    @Override
    public MesureConditionGenerale getByDatePrelevementPlateformeCodeAndProjetCode(Date datePrelevement, String plateformeCode, String projetCode) throws PersistenceException {
        try {
            Query query = null;

            String queryString = "from MesureConditionGenerale s where s.datePrelevement = :date and s.plateforme.code = :plateformeCode and s.projetSite.projet.code = :projetCode";

            query = entityManager.createQuery(queryString);
            query.setParameter("date", datePrelevement);
            query.setParameter("plateformeCode", plateformeCode);
            query.setParameter("projetCode", projetCode);

            List<?> results = query.getResultList();
            if (results != null && results.size() == 1) {
                return (MesureConditionGenerale) results.get(0);
            }

        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
        return null;
    }

    /**
     *
     * @param variableId
     * @param mesureId
     * @return
     * @throws PersistenceException
     */
    @Override
    public ValeurConditionGenerale getByVariableIdAndMesureId(Long variableId, Long mesureId) throws PersistenceException {
        try {
            Query query = null;

            String queryString = "from ValeurConditionGenerale v where v.variable.id = :variableId and v.mesure.id = :mesureId";

            query = entityManager.createQuery(queryString);
            query.setParameter("variableId", variableId);
            query.setParameter("mesureId", mesureId);

            List<?> results = query.getResultList();
            if (results != null && results.size() == 1) {
                return (ValeurConditionGenerale) results.get(0);
            }

        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
        return null;
    }

}
