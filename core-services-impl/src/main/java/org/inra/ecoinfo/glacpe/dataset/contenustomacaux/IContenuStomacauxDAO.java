package org.inra.ecoinfo.glacpe.dataset.contenustomacaux;

import java.util.Date;
import java.util.List;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.projetsite.ProjetSite;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public interface IContenuStomacauxDAO extends IDAO<Object> {

    /**
     *
     * @return
     * @throws PersistenceException
     */
    List<Projet> retrieveContenuStomacauxAvailablesProjets() throws PersistenceException;

    /**
     *
     * @param projetSiteId
     * @return
     * @throws PersistenceException
     */
    List<Plateforme> retrieveContenuStomacauxAvailablesPlateformesByProjetSiteId(long projetSiteId) throws PersistenceException;

    /**
     *
     * @param plateformesIds
     * @param firstDate
     * @param lastDate
     * @return
     * @throws PersistenceException
     */
    List<VariableGLACPE> retrieveContenuStomacauxAvailablesVariables(List<Long> plateformesIds, Date firstDate, Date lastDate) throws PersistenceException;

    /**
     *
     * @param projetId
     * @param siteId
     * @return
     * @throws PersistenceException
     */
    ProjetSite retrieveContenuStomacauxAvailablesProjetsSiteByProjetAndSite(Long projetId, Long siteId) throws PersistenceException;
}
