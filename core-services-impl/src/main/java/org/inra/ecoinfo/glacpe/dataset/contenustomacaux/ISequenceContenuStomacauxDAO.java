package org.inra.ecoinfo.glacpe.dataset.contenustomacaux;

import java.util.Date;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.glacpe.dataset.contenustomacaux.entity.SequenceContenuStomacaux;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public interface ISequenceContenuStomacauxDAO extends IDAO<SequenceContenuStomacaux> {

    /**
     *
     * @param datePrelevement
     * @param projetCode
     * @param siteCode
     * @return
     * @throws PersistenceException
     */
    SequenceContenuStomacaux getByDatePrelevementAndProjetCodeAndSiteCode(Date datePrelevement, String projetCode, String siteCode) throws PersistenceException;

    void flush();
}
