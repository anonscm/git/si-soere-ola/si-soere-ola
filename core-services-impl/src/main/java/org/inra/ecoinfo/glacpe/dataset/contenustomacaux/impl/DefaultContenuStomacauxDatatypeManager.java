package org.inra.ecoinfo.glacpe.dataset.contenustomacaux.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.inra.ecoinfo.MO;
import org.inra.ecoinfo.glacpe.dataset.contenustomacaux.IContenuStomacauxDAO;
import org.inra.ecoinfo.glacpe.dataset.contenustomacaux.IContenuStomacauxDatatypeManager;
import org.inra.ecoinfo.glacpe.extraction.vo.VariableVO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.projetsite.ProjetSite;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.datatype.IDatatypeDAO;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ptcherniati
 */
public class DefaultContenuStomacauxDatatypeManager extends MO implements IContenuStomacauxDatatypeManager {

    /**
     *
     */
    protected IContenuStomacauxDAO contenuStomacauxDAO;

    /**
     *
     */
    protected IDatatypeDAO datatypeDAO;

    /**
     *
     * @return
     * @throws BusinessException
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public List<Projet> retrieveContenuStomacauxAvailablesProjets() throws BusinessException {
        try {
            return contenuStomacauxDAO.retrieveContenuStomacauxAvailablesProjets();
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }

    }

    /**
     *
     * @param projetSiteId
     * @return
     * @throws BusinessException
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public List<Plateforme> retrieveContenuStomacauxAvailablesPlateformesByProjetSiteId(long projetSiteId) throws BusinessException {
        try {
            return contenuStomacauxDAO.retrieveContenuStomacauxAvailablesPlateformesByProjetSiteId(projetSiteId);
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    private List<VariableVO> retrieveListVariables() throws BusinessException {
        try {
            List<VariableGLACPE> variables = (List) ((DataType) datatypeDAO.getByCode("contenus_stomacaux")).getVariables();
            List<VariableVO> variablesVO = new ArrayList<VariableVO>();

            for (VariableGLACPE variable : variables) {
                VariableVO variableVO = new VariableVO(variable);
                variablesVO.add(variableVO);
            }
            return variablesVO;
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    private List<VariableVO> retrieveListAvailableVariables(List<Long> plateformesIds, Date firstDate, Date lastDate) throws BusinessException {
        try {
            List<VariableGLACPE> variables = contenuStomacauxDAO.retrieveContenuStomacauxAvailablesVariables(plateformesIds, firstDate, lastDate);
            List<VariableVO> variablesVO = new ArrayList<VariableVO>();

            for (VariableGLACPE variable : variables) {
                VariableVO variableVO = new VariableVO(variable);
                variablesVO.add(variableVO);
            }
            return variablesVO;
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    /**
     *
     * @param plateformesIds
     * @param firstDate
     * @param lastDate
     * @return
     * @throws BusinessException
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public List<VariableVO> retrieveListsVariables(List<Long> plateformesIds, Date firstDate, Date lastDate) throws BusinessException {
        Map<String, VariableVO> rawVariablesMap = new HashMap<String, VariableVO>();

        for (VariableVO variable : retrieveListVariables()) {
            rawVariablesMap.put(variable.getCode(), variable);
        }

        for (VariableVO availableVariable : retrieveListAvailableVariables(plateformesIds, firstDate, lastDate)) {
            availableVariable.setIsDataAvailable(true);
            rawVariablesMap.put(availableVariable.getCode(), availableVariable);
        }

        return new LinkedList<VariableVO>(rawVariablesMap.values());
    }

    /**
     *
     * @param projetId
     * @param siteId
     * @return
     * @throws BusinessException
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public ProjetSite retrieveContenuStomacauxAvailablesProjetsSiteByProjetAndSite(Long projetId, Long siteId) throws BusinessException {
        try {
            return contenuStomacauxDAO.retrieveContenuStomacauxAvailablesProjetsSiteByProjetAndSite(projetId, siteId);
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    /**
     *
     * @param contenuStomacauxDAO
     */
    public void setContenuStomacauxDAO(IContenuStomacauxDAO contenuStomacauxDAO) {
        this.contenuStomacauxDAO = contenuStomacauxDAO;
    }

    /**
     *
     * @param datatypeDAO
     */
    public void setDatatypeDAO(IDatatypeDAO datatypeDAO) {
        this.datatypeDAO = datatypeDAO;
    }
}
