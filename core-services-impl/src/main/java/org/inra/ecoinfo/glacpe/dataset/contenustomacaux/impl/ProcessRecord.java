package org.inra.ecoinfo.glacpe.dataset.contenustomacaux.impl;

import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_DUPLICATE_SEQUENCE;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_ERROR;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_ERROR_PLATEFORME_INVALID;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_FLOAT_VALUE_EXPECTED;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_MISSING_OUTILS_MESURE_IN_REFERENCES_DATAS;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_MISSING_PROJET_SITE_IN_REFERENCES_DATAS;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_NOT_IN_LIST;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.getGLACPEMessage;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.getGLACPEMessageWithBundle;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.hibernate.exception.ConstraintViolationException;
import org.inra.ecoinfo.dataset.exception.InsertionDatabaseException;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.glacpe.dataset.contenustomacaux.ISequenceContenuStomacauxDAO;
import org.inra.ecoinfo.glacpe.dataset.contenustomacaux.entity.MesureContenuStomacaux;
import org.inra.ecoinfo.glacpe.dataset.contenustomacaux.entity.SequenceContenuStomacaux;
import org.inra.ecoinfo.glacpe.dataset.contenustomacaux.entity.SousSequenceContenuStomacaux;
import org.inra.ecoinfo.glacpe.dataset.contenustomacaux.entity.ValeurMesureContenuStomacaux;
import org.inra.ecoinfo.glacpe.dataset.impl.AbstractProcessRecord;
import org.inra.ecoinfo.glacpe.dataset.impl.CleanerValues;
import org.inra.ecoinfo.glacpe.refdata.controlecoherence.ControleCoherence;
import org.inra.ecoinfo.glacpe.refdata.controlecoherence.IControleCoherenceDAO;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.IOutilsMesureDAO;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.OutilsMesure;
import org.inra.ecoinfo.glacpe.refdata.plateforme.IPlateformeDAO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projetsite.IProjetSiteDAO;
import org.inra.ecoinfo.glacpe.refdata.projetsite.ProjetSite;
import org.inra.ecoinfo.glacpe.refdata.projetsitethemedatatype.ProjetSiteThemeDatatype;
import org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitativeDAO;
import org.inra.ecoinfo.refdata.valeurqualitative.ValeurQualitative;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.DatasetDescriptorBuilder;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.ErrorsReport;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.xml.sax.SAXException;

import com.Ostermiller.util.CSVParser;

/**
 *
 * @author ptcherniati
 */
public class ProcessRecord extends AbstractProcessRecord {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private static final String BUNDLE_SOURCE_PATH_CONTENUSTOMACAUX = "org.inra.ecoinfo.glacpe.dataset.contenustomacaux.messages";
    private static final String MSG_NO_LIST = "PROPERTY_MSG_NO_LIST";//$NON-NLS-1$

    /**
     *
     */
    protected ISequenceContenuStomacauxDAO sequenceContenuStomacauxDAO;

    /**
     *
     */
    protected IValeurQualitativeDAO valeurQualitativeDAO;

    /**
     *
     */
    protected IControleCoherenceDAO controleCoherenceDAO;

    /**
     *
     */
    protected IProjetSiteDAO projetSiteDAO;

    /**
     *
     */
    protected IPlateformeDAO plateformeDAO;

    /**
     *
     */
    protected IOutilsMesureDAO outilsMesureDAO;

    /**
     *
     * @return
     * @throws IOException
     * @throws SAXException
     */
    @Override
    public DatasetDescriptor holdDatasetDescriptor() throws IOException, SAXException {
        if (datasetDescriptor == null) {
            datasetDescriptor = DatasetDescriptorBuilder.buildDescriptor(ISequenceContenuStomacauxDAO.class.getResource(DATASET_DESCRIPTOR_XML).openStream());
        }
        return datasetDescriptor;
    }

    @Override
    public void processRecord(CSVParser parser, VersionFile versionFile, String fileEncoding, DatasetDescriptor datasetDescriptor) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport(getGLACPEMessage(PROPERTY_MSG_ERROR));
        Utils.testNotNullArguments(getLocalizationManager(), versionFile, fileEncoding);
        Utils.testCastedArguments(versionFile, VersionFile.class, getLocalizationManager());

        try {

            String[] values = null;
            long lineCount = 1;
            int variableHeaderIndex = 9;

            Map<String, ControleCoherence> controlesCoherenceMap = controleCoherenceDAO.getControleCoherenceBySitecodeAndDatatypecode(((ProjetSiteThemeDatatype) versionFile.getDataset().getLeafNode()).getProjetSiteTheme().getProjetSite().getSite()
                    .getCode(), versionFile.getDataset().getLeafNode().getDatatype().getCode());
            List<String> dbVariables = skipHeaderAndRetrieveVariableName(variableHeaderIndex, parser);
            Map<String, List<LineRecord>> sequencesMapLines = new HashMap<String, List<LineRecord>>();
            Map<String, List<ValeurQualitative>> referenceMap = buildReferenceMap();

            // On parcourt chaque ligne du fichier
            while ((values = parser.getLine()) != null) {

                CleanerValues cleanerValues = new CleanerValues(values);
                lineCount++;
                List<VariableValue> variablesValues = new LinkedList<VariableValue>();

                // On parcourt chaque colonne d'une ligne
                String projetCode = Utils.createCodeFromString(cleanerValues.nextToken());
                String nomSite = cleanerValues.nextToken();
                String plateformeCode = Utils.createCodeFromString(cleanerValues.nextToken());
                String datePrelevementString = cleanerValues.nextToken();
                Date datePrelevement = datePrelevementString.length() > 0 ? new SimpleDateFormat(datasetDescriptor.getColumns().get(cleanerValues.currentTokenIndex() - 1).getFormatType()).parse(datePrelevementString) : null;
                String nomPecheur = cleanerValues.nextToken();
                String nomEspece = cleanerValues.nextToken();
                String outilMesureCode = Utils.createCodeFromString(cleanerValues.nextToken());
                String maille = cleanerValues.nextToken();
                String profondeurString = cleanerValues.nextToken();
                Float profondeur = profondeurString.length() > 0 ? Float.parseFloat(profondeurString) : 0.0f;
                String dureeString = cleanerValues.nextToken();
                Float duree = dureeString.length() > 0 ? Float.parseFloat(dureeString) : 0.0f;
                String utilisationComptage = cleanerValues.nextToken();
                String numeroPoissonString = cleanerValues.nextToken();
                Integer numeroPoisson = numeroPoissonString.length() > 0 ? Integer.parseInt(numeroPoissonString) : 0;
                String taillePoissonString = cleanerValues.nextToken();
                Float taillePoisson = taillePoissonString.length() > 0 ? Float.parseFloat(taillePoissonString) : 0.0f;
                String malformation = cleanerValues.nextToken();
                String sexe = cleanerValues.nextToken();
                String maturite = cleanerValues.nextToken();
                String estomacVide = cleanerValues.nextToken();
                String poidsString = cleanerValues.nextToken();
                Float poids = poidsString.length() > 0 ? Float.parseFloat(poidsString) : 0.0f;
                String poissonCompte = cleanerValues.nextToken();
                String numeroPilulierString = cleanerValues.nextToken();
                Integer numeroPilulier = numeroPilulierString.length() > 0 ? Integer.parseInt(numeroPilulierString) : 0;
                String numeroEcailleString = cleanerValues.nextToken();
                Integer numeroEcaille = numeroPilulierString.length() > 0 ? Integer.parseInt(numeroEcailleString) : 0;
                String etatEstomac = cleanerValues.nextToken();
                String commentaires = cleanerValues.nextToken();

                for (int actualVariableArray = variableHeaderIndex - 1; actualVariableArray < values.length; actualVariableArray++) {
                    String value = values[actualVariableArray].replaceAll(" ", "");
                    try {
                        String variable = dbVariables.get(actualVariableArray - variableHeaderIndex + 1);
                        if (controlesCoherenceMap.get(Utils.createCodeFromString(variable)) != null)
                            testValueCoherence(Float.valueOf(value), controlesCoherenceMap.get(Utils.createCodeFromString(variable)).getValeurMin(), controlesCoherenceMap.get(Utils.createCodeFromString(variable)).getValeurMax(), lineCount, 11);
                        variablesValues.add(new VariableValue(dbVariables.get(actualVariableArray - variableHeaderIndex + 1), values[actualVariableArray].replaceAll(" ", "")));
                    } catch (NumberFormatException e) {
                        errorsReport.addException(new BadExpectedValueException(String.format(getGLACPEMessage(PROPERTY_MSG_FLOAT_VALUE_EXPECTED), lineCount, 11, value)));
                    } catch (BadExpectedValueException e) {
                        errorsReport.addException(e);
                    }
                }

                LineRecord line = new LineRecord(nomSite, plateformeCode, datePrelevement, outilMesureCode, nomPecheur, nomEspece, maille, duree, utilisationComptage, poissonCompte, numeroPoisson, taillePoisson, malformation, sexe, maturite,
                        estomacVide, poids, etatEstomac, numeroPilulier, numeroEcaille, commentaires, profondeur, variablesValues, lineCount, projetCode);
                String sequenceKey = projetCode.concat(nomSite).concat(datePrelevementString);
                fillLinesMap(sequencesMapLines, line, sequenceKey);
            }

            int count = 0;
            for (String sequenceKey : sequencesMapLines.keySet()) {

                List<LineRecord> sequenceLines = sequencesMapLines.get(sequenceKey);
                if (!sequenceLines.isEmpty()) {
                    try {
                        Date datePrelevement = sequenceLines.get(0).getDatePrelevement();
                        String projetCode = sequenceLines.get(0).getProjetCode();
                        String siteCode = Utils.createCodeFromString(sequenceLines.get(0).getNomSite());
                        String nomPecheur = sequenceLines.get(0).getNomPecheur();
                        Float profondeur = sequenceLines.get(0).getProfondeur();
                        Float duree = sequenceLines.get(0).getDuree();
                        buildSequence(datePrelevement, projetCode, siteCode, nomPecheur, profondeur, duree, sequenceLines, versionFile, referenceMap, errorsReport);
                        logger.debug(String.format("%d - %s", count++, datePrelevement));

                        // Très important à cause de problèmes de performances
                        sequenceContenuStomacauxDAO.flush();
                        versionFile = (VersionFile) versionFileDAO.merge(versionFile);
                    } catch (InsertionDatabaseException e) {
                        errorsReport.addException(e);
                    }
                }
            }

            if (errorsReport.hasErrors()) {
                throw new PersistenceException(errorsReport.buildHTMLMessages());
            }

        } catch (Exception e) {

            throw new BusinessException(e);
        }
    }

    private Map<String, List<ValeurQualitative>> buildReferenceMap() throws PersistenceException {
        Map<String, List<ValeurQualitative>> referenceMap = new HashMap<String, List<ValeurQualitative>>();

        List<ValeurQualitative> vqMaille = valeurQualitativeDAO.getByCode(SousSequenceContenuStomacaux.MAILLE_FILET);
        if (vqMaille == null || vqMaille.isEmpty()) {
            throw new PersistenceException(String.format(getGLACPEMessageWithBundle(BUNDLE_SOURCE_PATH_CONTENUSTOMACAUX, MSG_NO_LIST), "maille"));
        } else {
            referenceMap.put(SousSequenceContenuStomacaux.MAILLE_FILET, vqMaille);
        }
        List<ValeurQualitative> vqSexe = valeurQualitativeDAO.getByCode(MesureContenuStomacaux.SEXE_POISSON);
        if (vqSexe == null || vqSexe.isEmpty()) {
            throw new PersistenceException(String.format(getGLACPEMessageWithBundle(BUNDLE_SOURCE_PATH_CONTENUSTOMACAUX, MSG_NO_LIST), "sexe"));
        } else {
            referenceMap.put(MesureContenuStomacaux.SEXE_POISSON, vqSexe);
        }
        List<ValeurQualitative> vqMaturite = valeurQualitativeDAO.getByCode(MesureContenuStomacaux.SEXUALITE_POISSON);
        if (vqMaturite == null || vqMaturite.isEmpty()) {
            throw new PersistenceException(String.format(getGLACPEMessageWithBundle(BUNDLE_SOURCE_PATH_CONTENUSTOMACAUX, MSG_NO_LIST), "maturite"));
        } else {
            referenceMap.put(MesureContenuStomacaux.SEXUALITE_POISSON, vqMaturite);
        }
        List<ValeurQualitative> vqMalformation = valeurQualitativeDAO.getByCode(MesureContenuStomacaux.MALFORMATION_POISSON);
        if (vqMalformation == null || vqMalformation.isEmpty()) {
            throw new PersistenceException(String.format(getGLACPEMessageWithBundle(BUNDLE_SOURCE_PATH_CONTENUSTOMACAUX, MSG_NO_LIST), "malformation"));
        } else {
            referenceMap.put(MesureContenuStomacaux.MALFORMATION_POISSON, vqMalformation);
        }
        List<ValeurQualitative> vqEtatEstomac = valeurQualitativeDAO.getByCode(MesureContenuStomacaux.ETAT_ESTOMAC_POISSON);
        if (vqEtatEstomac == null || vqEtatEstomac.isEmpty()) {
            throw new PersistenceException(String.format(getGLACPEMessageWithBundle(BUNDLE_SOURCE_PATH_CONTENUSTOMACAUX, MSG_NO_LIST), "etat_estomac"));
        } else {
            referenceMap.put(MesureContenuStomacaux.ETAT_ESTOMAC_POISSON, vqEtatEstomac);
        }
        List<ValeurQualitative> vqNomItem = valeurQualitativeDAO.getByCode(ValeurMesureContenuStomacaux.PROIE);
        if (vqNomItem == null || vqNomItem.isEmpty()) {
            throw new PersistenceException(String.format(getGLACPEMessageWithBundle(BUNDLE_SOURCE_PATH_CONTENUSTOMACAUX, MSG_NO_LIST), "proie"));
        } else {
            referenceMap.put(ValeurMesureContenuStomacaux.PROIE, vqNomItem);
        }
        return referenceMap;
    }

    private void fillLinesMap(Map<String, List<LineRecord>> linesMap, LineRecord line, String keyLine) {
        List<LineRecord> sousSequenceLines = null;
        if (keyLine != null && keyLine.trim().length() > 0) {
            if (linesMap.containsKey(keyLine)) {
                linesMap.get(keyLine).add(line);
            } else {
                sousSequenceLines = new LinkedList<LineRecord>();
                sousSequenceLines.add(line);
                linesMap.put(keyLine, sousSequenceLines);
            }
        }
    }

    private void buildSequence(Date datePrelevement, String projetCode, String siteCode, String nomPecheur, Float profondeur, Float duree, List<LineRecord> sequenceLines, VersionFile versionFile, Map<String, List<ValeurQualitative>> referenceMap,
            ErrorsReport errorsReport) throws PersistenceException, InsertionDatabaseException
    {
        SequenceContenuStomacaux sequence = sequenceContenuStomacauxDAO.getByDatePrelevementAndProjetCodeAndSiteCode(datePrelevement, projetCode, siteCode);

        if (sequence == null) {

            ProjetSite projetSite = projetSiteDAO.getBySiteCodeAndProjetCode(siteCode, projetCode);

            if (projetSite == null) {
                errorsReport.addException(new PersistenceException(String.format(getGLACPEMessage(PROPERTY_MSG_MISSING_PROJET_SITE_IN_REFERENCES_DATAS), projetCode, siteCode)));
                throw new PersistenceException(String.format(getGLACPEMessage(PROPERTY_MSG_MISSING_PROJET_SITE_IN_REFERENCES_DATAS), projetCode, siteCode));
            }

            sequence = new SequenceContenuStomacaux();

            sequence.setProjetSite(projetSite);
            sequence.setDatePrelevement(datePrelevement);
            sequence.setNomPecheur(nomPecheur);
            sequence.setDuree(duree);
            sequence.setProfondeur(profondeur);

            sequence.setVersionFile(versionFile);
        }

        Map<String, List<LineRecord>> sousSequencesMap = new HashMap<String, List<LineRecord>>();

        for (LineRecord line : sequenceLines) {
            String sousSequenceKey = line.getPlateformeCode().concat(line.getNomEspece());
            fillLinesMap(sousSequencesMap, line, sousSequenceKey);
        }

        for (String sousSequenceKey : sousSequencesMap.keySet()) {
            try {
                List<LineRecord> projetLines = sousSequencesMap.get(sousSequenceKey);
                buildSousSequence(sousSequenceKey, projetLines.get(0).getNomEspece(), projetLines.get(0).getOutilsMesureCode(), projetLines.get(0).getMaille(), projetLines.get(0).getCommentaires(), projetLines, sequence, referenceMap, errorsReport);

            } catch (InsertionDatabaseException e) {
                errorsReport.addException(e);
            }
        }
        try {
            sequenceContenuStomacauxDAO.saveOrUpdate(sequence);
        } catch (ConstraintViolationException e) {
            String message = String.format(getGLACPEMessage(PROPERTY_MSG_DUPLICATE_SEQUENCE), DateUtil.getSimpleDateFormatDateyyyyMMddUTC().format(datePrelevement), siteCode);
            errorsReport.addException(new BusinessException(message));
        }
    }

    private void buildSousSequence(String projetCode, String nomEspece, String outilsMesureCode, String maille, String commentaires, List<LineRecord> sousSequenceLines, SequenceContenuStomacaux sequence,
            Map<String, List<ValeurQualitative>> referenceMap, ErrorsReport errorsReport) throws PersistenceException, InsertionDatabaseException
    {

        LineRecord firstLine = sousSequenceLines.get(0);

        Plateforme plateforme = null;

        plateforme = plateformeDAO.getByNKey(firstLine.getPlateformeCode(), Utils.createCodeFromString(firstLine.getNomSite()));
        if (plateforme == null) {
            InsertionDatabaseException insertionDatabaseException = new InsertionDatabaseException(String.format(getGLACPEMessage(PROPERTY_MSG_ERROR_PLATEFORME_INVALID), firstLine.getPlateformeCode(), firstLine.getOriginalLineNumber()));
            throw insertionDatabaseException;
        }

        OutilsMesure outilsMesure = outilsMesureDAO.getByCode(outilsMesureCode);
        if (outilsMesure == null) {
            errorsReport.addException((new PersistenceException(String.format(getGLACPEMessage(PROPERTY_MSG_MISSING_OUTILS_MESURE_IN_REFERENCES_DATAS), outilsMesureCode))));
            throw new PersistenceException(String.format(getGLACPEMessage(PROPERTY_MSG_MISSING_OUTILS_MESURE_IN_REFERENCES_DATAS), outilsMesureCode));
        }

        SousSequenceContenuStomacaux sousSequence = new SousSequenceContenuStomacaux();

        // penser aux vérification de valeurQualitatives sur le nom de l'espèce et la maille
        sousSequence.setPlateforme(plateforme);
        sousSequence.setOutilsMesure(outilsMesure);
        sousSequence.setSequence(sequence);
        sousSequence.setEspece(nomEspece);

        if (sousSequence.getEspece() == null) {
            errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(PROPERTY_MSG_NOT_IN_LIST), nomEspece, "espèce")));
        }

        for (ValeurQualitative vqm : referenceMap.get(SousSequenceContenuStomacaux.MAILLE_FILET)) {
            if (maille.equals(vqm.getNom())) {
                sousSequence.setMaille(vqm);
                break;
            }
        }
        if (sousSequence.getMaille() == null) {
            errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(PROPERTY_MSG_NOT_IN_LIST), maille, "maille")));
        }
        sequence.getSousSequence().add(sousSequence);

        Map<String, List<LineRecord>> poissonsLinesMap = new HashMap<String, List<LineRecord>>();

        for (LineRecord line : sousSequenceLines) {
            fillLinesMap(poissonsLinesMap, line, line.getNumeroPoisson().toString());
        }

        for (String numeroPoisson : poissonsLinesMap.keySet()) {
            try {
                List<LineRecord> poissonsLines = poissonsLinesMap.get(numeroPoisson);
                buildMesure(Integer.parseInt(numeroPoisson), poissonsLines.get(0).getSexe(), poissonsLines.get(0).getMaturite(), poissonsLines.get(0).getMalformation(), poissonsLines.get(0).getPoids(), poissonsLines.get(0).getEstomacVide(),
                        poissonsLines.get(0).getEtatEstomac(), poissonsLines.get(0).getPoissonCompte(), poissonsLines.get(0).getUtilisationComptage(), poissonsLines.get(0).getTaillePoisson(), poissonsLines.get(0).getNumeroPilulier(), poissonsLines
                                .get(0).getNumeroEcaille(), poissonsLines.get(0).getCommentaires(), poissonsLines, sousSequence, referenceMap, errorsReport);

            } catch (InsertionDatabaseException e) {
                errorsReport.addException(e);
            }
        }
    }

    private void buildMesure(Integer numeroPoisson, String sexe, String maturite, String malformation, Float poids, String estomacVide, String etatEstomac, String poissonCompte, String utilisationComptage, Float taillePoisson,
            Integer numeroPilulier, Integer numeroEcaille, String commentaires, List<LineRecord> poissonsLines, SousSequenceContenuStomacaux sousSequence, Map<String, List<ValeurQualitative>> referenceMap, ErrorsReport errorsReport)
            throws PersistenceException, InsertionDatabaseException
    {

        MesureContenuStomacaux mesure = new MesureContenuStomacaux();
        mesure.setSousSequence(sousSequence);
        sousSequence.getMesures().add(mesure);
        mesure.setLigneFichierEchange(poissonsLines.get(0).getOriginalLineNumber());
        mesure.setNumeroPoisson(numeroPoisson);

        // faire des vérifications sur les valeurs qualitatives

        for (ValeurQualitative vqs : referenceMap.get(MesureContenuStomacaux.SEXE_POISSON)) {
            if (sexe.equals(vqs.getNom())) {
                mesure.setSexePoisson(vqs);
                break;
            }
        }
        if (mesure.getSexePoisson() == null) {
            errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(PROPERTY_MSG_NOT_IN_LIST), sexe, MesureContenuStomacaux.SEXE_POISSON)));
        }

        for (ValeurQualitative vqm : referenceMap.get(MesureContenuStomacaux.SEXUALITE_POISSON)) {
            if (maturite.equals(vqm.getNom())) {
                mesure.setMaturitePoisson(vqm);
                break;
            }
        }
        if (mesure.getMaturitePoisson() == null) {
            errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(PROPERTY_MSG_NOT_IN_LIST), maturite, MesureContenuStomacaux.SEXUALITE_POISSON)));
        }

        for (ValeurQualitative vqm : referenceMap.get(MesureContenuStomacaux.MALFORMATION_POISSON)) {
            if (malformation.equals(vqm.getNom())) {
                mesure.setMalformation(vqm);
                break;
            }
        }
        if (mesure.getMalformation() == null) {
            errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(PROPERTY_MSG_NOT_IN_LIST), malformation, MesureContenuStomacaux.MALFORMATION_POISSON)));
        }

        for (ValeurQualitative vqe : referenceMap.get(MesureContenuStomacaux.ETAT_ESTOMAC_POISSON)) {
            if (etatEstomac.equals(vqe.getNom())) {
                mesure.setEtatEstomac(vqe);
                break;
            }
        }
        if (mesure.getEstomacVide() == null) {
            errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(PROPERTY_MSG_NOT_IN_LIST), etatEstomac, MesureContenuStomacaux.ETAT_ESTOMAC_POISSON)));
        }
        mesure.setPoids(poids);
        mesure.setLongueurPoisson(taillePoisson);
        mesure.setNumeroPilulier(numeroPilulier);
        mesure.setNumeroEcaille(numeroEcaille);
        mesure.setCommentaire(commentaires);

        if (estomacVide.equals("Oui") || estomacVide.equals("oui") || estomacVide.equals("OUI")) {
            mesure.setEstomacVide(true);
        } else {
            mesure.setEstomacVide(false);
        }
        if (poissonCompte.equals("Oui") || poissonCompte.equals("oui") || poissonCompte.equals("OUI")) {
            mesure.setPoissonCompte(true);
        } else {
            mesure.setPoissonCompte(false);
        }
        if (utilisationComptage.equals("Oui") || utilisationComptage.equals("oui") || utilisationComptage.equals("OUI")) {
            mesure.setUtilisationComptage(true);
        } else {
            mesure.setUtilisationComptage(false);
        }

        for (LineRecord poissonLine : poissonsLines) {
            for (VariableValue variableValue : poissonLine.getVariablesValues()) {
                ValeurMesureContenuStomacaux valeurMesure = new ValeurMesureContenuStomacaux();

                // vérification de la valeur qualitative de type "variable"

                for (ValeurQualitative vqn : referenceMap.get(ValeurMesureContenuStomacaux.PROIE)) {
                    if (variableValue.getVariable().equals(vqn.getNom())) {
                        valeurMesure.setProie(vqn);
                        break;
                    }
                }
                if (valeurMesure.getProie() == null) {
                    errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(PROPERTY_MSG_NOT_IN_LIST), etatEstomac, ValeurMesureContenuStomacaux.PROIE)));
                }
                if (variableValue.getValue() == null || variableValue.getValue().length() == 0) {
                    valeurMesure.setValeur(null);
                } else {
                    Float value = Float.parseFloat(variableValue.getValue());
                    valeurMesure.setValeur(value);
                }
                valeurMesure.setMesure(mesure);
                mesure.getValeurs().add(valeurMesure);
            }
        }
    }

    /**
     *
     * @param sequenceContenuStomacauxDAO
     */
    public void setSequenceContenuStomacauxDAO(ISequenceContenuStomacauxDAO sequenceContenuStomacauxDAO) {
        this.sequenceContenuStomacauxDAO = sequenceContenuStomacauxDAO;
    }

    /**
     *
     * @param valeurQualitativeDAO
     */
    public void setValeurQualitativeDAO(IValeurQualitativeDAO valeurQualitativeDAO) {
        this.valeurQualitativeDAO = valeurQualitativeDAO;
    }

    /**
     *
     * @param controleCoherenceDAO
     */
    public void setControleCoherenceDAO(IControleCoherenceDAO controleCoherenceDAO) {
        this.controleCoherenceDAO = controleCoherenceDAO;
    }

    /**
     *
     * @param projetSiteDAO
     */
    public void setProjetSiteDAO(IProjetSiteDAO projetSiteDAO) {
        this.projetSiteDAO = projetSiteDAO;
    }

    /**
     *
     * @param plateformeDAO
     */
    public void setPlateformeDAO(IPlateformeDAO plateformeDAO) {
        this.plateformeDAO = plateformeDAO;
    }

    /**
     *
     * @param outilsMesureDAO
     */
    public void setOutilsMesureDAO(IOutilsMesureDAO outilsMesureDAO) {
        this.outilsMesureDAO = outilsMesureDAO;
    }
}
