package org.inra.ecoinfo.glacpe.dataset.contenustomacaux.impl;

/**
 *
 * @author ptcherniati
 */
public class VariableValue {

    private String variable;
    private String value;

    /**
     *
     * @param variable
     * @param value
     */
    public VariableValue(String variable, String value) {
        super();
        this.variable = variable;
        this.value = value;
    }

    /**
     *
     * @return
     */
    public String getVariable() {
        return variable;
    }

    /**
     *
     * @param variable
     */
    public void setVariable(String variable) {
        this.variable = variable;
    }

    /**
     *
     * @return
     */
    public String getValue() {
        return value;
    }

    /**
     *
     * @param value
     */
    public void setValue(String value) {
        this.value = value;
    }
}
