package org.inra.ecoinfo.glacpe.dataset.contenustomacaux.jpa;

import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.glacpe.dataset.contenustomacaux.IContenuStomacauxDAO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.projetsite.ProjetSite;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class JPAContenuStomacauxDAO extends AbstractJPADAO<Object> implements IContenuStomacauxDAO {

    private static final String REQUEST_CONTENUSTOMACAUX_AVAILABLES_PROJETS = "select distinct ss.projetSite.projet from SousSequenceContenuStomacaux ss";
    private static final String REQUEST_CONTENUSTOMACAUX_PROJETSITE_BY_PROJET_ID_AND_SITE_ID = "select distinct ss.projetSite from SousSequenceContenuStomacaux ss where ss.projetSite.projet.id=:projetId and ss.projetSite.site.id=:siteId ";
    private static final String REQUEST_CONTENUSTOMACAUX_AVAILABLES_PLATEFORMES_BY_PROJETSITE_ID = "select distinct ss.sequence.plateforme from SousSequenceContenuStomacaux ss where ss.projetSite.id = :projetSiteId";
    private static final String REQUEST_CONTENUSTOMACAUX_AVAILABLES_VARIABLES_BY_PLATEFORMES_ID = "select distinct vm.variable from ValeurMesureContenuStomacaux vm where vm.mesure.sousSequence.sequence.plateforme.id in (:plateformesIds) and vm.mesure.sousSequence.sequence.datePrelevement between :firstDate and :lastDate and vm.valeur is not null order by vm.variable.ordreAffichageGroupe";

    /**
     *
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Projet> retrieveContenuStomacauxAvailablesProjets() throws PersistenceException {
        try {

            Query query = entityManager.createQuery(REQUEST_CONTENUSTOMACAUX_AVAILABLES_PROJETS);
            List<Projet> projets = query.getResultList();

            return projets;
        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param projetSiteId
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Plateforme> retrieveContenuStomacauxAvailablesPlateformesByProjetSiteId(long projetSiteId) throws PersistenceException {
        try {

            Query query = entityManager.createQuery(REQUEST_CONTENUSTOMACAUX_AVAILABLES_PLATEFORMES_BY_PROJETSITE_ID);
            query.setParameter("projetSiteId", projetSiteId);
            List<Plateforme> plateformes = query.getResultList();

            return plateformes;
        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param plateformesIds
     * @param firstDate
     * @param lastDate
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<VariableGLACPE> retrieveContenuStomacauxAvailablesVariables(List<Long> plateformesIds, Date firstDate, Date lastDate) throws PersistenceException {
        List<VariableGLACPE> variables;
        try {
            Query query = entityManager.createQuery(REQUEST_CONTENUSTOMACAUX_AVAILABLES_VARIABLES_BY_PLATEFORMES_ID);

            query.setParameter("plateformesIds", plateformesIds);
            query.setParameter("firstDate", firstDate);
            query.setParameter("lastDate", lastDate);
            variables = query.getResultList();
            return variables;
        } catch (Exception e) {
            AbstractJPADAO.LOGGER.error(e);
            throw new PersistenceException(e);
        }
    }

    /**
     *
     * @param projetId
     * @param siteId
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("unchecked")
    @Override
    public ProjetSite retrieveContenuStomacauxAvailablesProjetsSiteByProjetAndSite(Long projetId, Long siteId) throws PersistenceException {
        try {

            Query query = entityManager.createQuery(REQUEST_CONTENUSTOMACAUX_PROJETSITE_BY_PROJET_ID_AND_SITE_ID);

            query.setParameter("projetId", projetId);
            query.setParameter("siteId", siteId);
            List<ProjetSite> projetSite = query.getResultList();

            return projetSite.get(0);
        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

}
