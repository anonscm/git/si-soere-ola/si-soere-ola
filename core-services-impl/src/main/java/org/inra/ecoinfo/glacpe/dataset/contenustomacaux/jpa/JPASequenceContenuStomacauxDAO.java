package org.inra.ecoinfo.glacpe.dataset.contenustomacaux.jpa;

import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.glacpe.dataset.contenustomacaux.ISequenceContenuStomacauxDAO;
import org.inra.ecoinfo.glacpe.dataset.contenustomacaux.entity.SequenceContenuStomacaux;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class JPASequenceContenuStomacauxDAO extends AbstractJPADAO<SequenceContenuStomacaux> implements ISequenceContenuStomacauxDAO {

    /**
     *
     * @param datePrelevement
     * @param projetCode
     * @param siteCode
     * @return
     * @throws PersistenceException
     */
    @Override
    public SequenceContenuStomacaux getByDatePrelevementAndProjetCodeAndSiteCode(Date datePrelevement, String projetCode, String siteCode) throws PersistenceException {
        try {
            Query query = null;

            String queryString = "from SequenceContenuStomacaux s where s.datePrelevement = :date and s.projetSite.projet.code = :projetCode and s.projetSite.site.code = :siteCode";

            query = entityManager.createQuery(queryString);
            query.setParameter("date", datePrelevement);
            query.setParameter("projetCode", projetCode);
            query.setParameter("siteCode", siteCode);

            List<?> results = query.getResultList();
            if (results != null && results.size() == 1) {
                return (SequenceContenuStomacaux) results.get(0);
            }

        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
        return null;
    }

}
