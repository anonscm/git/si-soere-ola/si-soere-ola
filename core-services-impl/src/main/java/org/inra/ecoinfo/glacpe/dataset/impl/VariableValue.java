package org.inra.ecoinfo.glacpe.dataset.impl;

import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;


/**
 * The Class VariableValue.
 */
public class VariableValue {

    /** The variableGLACPE @link(VariableGLACPE). */
    private VariableGLACPE variableGLACPE;

    /** The value @link(String). */
    private String value;

    /**
     * Instantiates a new variable value.
     * 
     * @param variableGLACPE
     * @param value
     *            the value
     * @param datatypeVariableUnite
     *            the datatypeVariableUnite
     */
    public VariableValue(final VariableGLACPE variableGLACPE, final String value) {
        this.variableGLACPE = variableGLACPE;
        this.value = value;
    }

    /**
     * Gets the datatypeVariableUnite.
     * 
     * @return the datatypeVariableUnite
     */
    public VariableGLACPE getVariableGLACPE() {
        return variableGLACPE;
    }

    /**
     * Gets the value.
     * 
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the datatypeVariableUnite.
     * 
     * @param variableGLACPE
     * @param datatypeVariableUnite
     *            the new datatypeVariableUnite
     */
    public void setVariableGLACPE(final VariableGLACPE variableGLACPE) {
        this.variableGLACPE = variableGLACPE;
    }

    /**
     * Sets the value.
     * 
     * @param value
     *            the new value
     */
    public void setValue(final String value) {
        this.value = value;
    }
}
