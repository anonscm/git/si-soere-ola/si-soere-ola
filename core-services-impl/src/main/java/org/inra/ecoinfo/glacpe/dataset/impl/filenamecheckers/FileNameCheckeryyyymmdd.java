package org.inra.ecoinfo.glacpe.dataset.impl.filenamecheckers;

import java.util.regex.Matcher;

import org.inra.ecoinfo.dataset.exception.InvalidFileNameException;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.glacpe.refdata.projetsitethemedatatype.ProjetSiteThemeDatatype;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;

/**
 *
 * @author ptcherniati
 */
public class FileNameCheckeryyyymmdd extends AbstractGLACPEFileNameChecker {

    private static final String DATE_PATTERN = DateUtil.getSimpleDateFormatDateyyyyMMddUTCForFile().toPattern();

    @Override
    public String getValidPattern(String siteName, String datatypeName) {
        return String.format(INVALID_FILE_NAME, siteName, datatypeName, DATE_PATTERN, DATE_PATTERN, FILE_FORMAT);
    }

    @Override
    public String getFilePath(VersionFile version) {
        String currentSite = ((ProjetSiteThemeDatatype) version.getDataset().getLeafNode()).getProjetSiteTheme().getProjetSite().getSite().getPath();
        if (configuration.getSiteSeparatorForFileNames() != null && !CST_STRING_EMPTY.equals(configuration.getSiteSeparatorForFileNames())) {
            currentSite = currentSite.replaceAll(Site.CST_PROPERTY_RECURENT_SEPARATOR, configuration.getSiteSeparatorForFileNames());
        }
        return String.format(PATTERN_FILE_NAME_PATH, ((ProjetSiteThemeDatatype) version.getDataset().getLeafNode()).getProjetSiteTheme().getProjetSite().getProjet().getCode(), currentSite, version.getDataset().getLeafNode().getDatatype().getCode(),
                DateUtil.getSimpleDateFormatDateyyyyMMddUTCForFile().format(version.getDataset().getDateDebutPeriode()), DateUtil.getSimpleDateFormatDateyyyyMMddUTCForFile().format(version.getDataset().getDateFinPeriode()),
                version.getVersionNumber());
    }

    @Override
    protected String getDatePattern() {
        return DATE_PATTERN;
    }

    /**
     *
     * @param version
     * @param currentProject
     * @param currentSite
     * @param currentDatatype
     * @param splitFilename
     * @throws InvalidFileNameException
     */
    @Override
    protected void testDates(VersionFile version, String currentProject, String currentSite, String currentDatatype, Matcher splitFilename) throws InvalidFileNameException {
        IntervalDate intervalDate;
        try {
            intervalDate = IntervalDate.getIntervalDateyyyyMMdd(splitFilename.group(4), splitFilename.group(5));
        } catch (Exception e1) {
            throw new InvalidFileNameException(String.format(INVALID_FILE_NAME, currentProject, currentSite, currentDatatype, DATE_PATTERN, DATE_PATTERN, FILE_FORMAT));
        }
        if (version != null) {
            version.getDataset().setDateDebutPeriode(intervalDate.getBeginDate());
            version.getDataset().setDateFinPeriode(intervalDate.getEndDate());
        }
    }

    @Override
    protected void testDates(VersionFile version, String currentSite, String currentDatatype, Matcher splitFilename) throws InvalidFileNameException {
        // Non utilisé
    }
}
