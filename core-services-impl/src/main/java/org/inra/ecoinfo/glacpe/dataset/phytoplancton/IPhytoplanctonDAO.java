package org.inra.ecoinfo.glacpe.dataset.phytoplancton;

import java.util.Date;
import java.util.List;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.glacpe.dataset.phytoplancton.entity.MesurePhytoplancton;
import org.inra.ecoinfo.glacpe.dataset.phytoplancton.entity.ValeurMesurePhytoplancton;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesRequestParamVO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.projetsite.ProjetSite;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public interface IPhytoplanctonDAO extends IDAO<Object> {

    /**
     *
     * @return
     * @throws PersistenceException
     */
    List<Projet> retrievePhytoplanctonAvailablesProjets() throws PersistenceException;

    /**
     *
     * @param selectedPlateformesIds
     * @param selectedVariablesIds
     * @param projetId
     * @param datesRequestParamVO
     * @return
     * @throws PersistenceException
     */
    List<ValeurMesurePhytoplancton> extractAggregatedDatas(List<Long> selectedPlateformesIds, List<Long> selectedVariablesIds, List<Long> projetId, DatesRequestParamVO datesRequestParamVO) throws PersistenceException;

    /**
     *
     * @param plateformesIds
     * @param firstDate
     * @param endDate
     * @return
     * @throws PersistenceException
     */
    List<VariableGLACPE> retrievePhytoplanctonAvailablesVariables(List<Long> plateformesIds, Date firstDate, Date endDate) throws PersistenceException;

    /**
     *
     * @param selectedPlateformesIds
     * @param projetId
     * @param datesRequestParamVO
     * @param selectedTaxonsIds
     * @return
     * @throws PersistenceException
     */
    List<MesurePhytoplancton> extractDatas(List<Long> selectedPlateformesIds, List<Long> projetId, DatesRequestParamVO datesRequestParamVO, List<Long> selectedTaxonsIds) throws PersistenceException;

    /**
     *
     * @param projetSiteId
     * @return
     * @throws PersistenceException
     */
    List<Plateforme> retrievePhytoplanctonAvailablesPlateformesByProjetSiteId(long projetSiteId) throws PersistenceException;

    /**
     *
     * @param projetId
     * @param siteId
     * @return
     * @throws PersistenceException
     */
    ProjetSite retrievePhytoplanctonAvailablesProjetsSiteByProjetAndSite(long projetId, long siteId) throws PersistenceException;
}