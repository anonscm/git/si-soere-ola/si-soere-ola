package org.inra.ecoinfo.glacpe.dataset.phytoplancton;

import java.util.Date;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.glacpe.dataset.phytoplancton.entity.SequencePhytoplancton;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public interface ISequencePhytoplanctonDAO extends IDAO<SequencePhytoplancton> {

    /**
     *
     * @param datePrelevement
     * @param projetCode
     * @param siteCode
     * @return
     * @throws PersistenceException
     */
    SequencePhytoplancton getByDatePrelevementAndProjetCodeAndSiteCode(Date datePrelevement, String projetCode, String siteCode) throws PersistenceException;

    void flush();

}
