package org.inra.ecoinfo.glacpe.dataset.phytoplancton.impl;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.AbstractExtractor;
import org.inra.ecoinfo.glacpe.dataset.phytoplancton.IPhytoplanctonDAO;
import org.inra.ecoinfo.glacpe.dataset.phytoplancton.entity.MesurePhytoplancton;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO;
import org.inra.ecoinfo.glacpe.extraction.vo.VariableVO;
import org.inra.ecoinfo.glacpe.refdata.taxon.Taxon;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public abstract class AbstractPhytoplanctonRawsDatasExtractor extends AbstractExtractor {

    /**
     *
     */
    protected static final String MAP_INDEX_0 = "0";

    /**
     *
     */
    protected static final String PROPERTY_MSG_NO_EXTRACTION_RESULT = "PROPERTY_MSG_NO_EXTRACTION_RESULT";

    /**
     *
     */
    protected IPhytoplanctonDAO phytoplanctonDAO;

    /**
     *
     * @param phytoplanctonDAO
     */
    public void setPhytoplanctonDAO(IPhytoplanctonDAO phytoplanctonDAO) {
        this.phytoplanctonDAO = phytoplanctonDAO;
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, List> extractDatas(Map<String, Object> requestMetadatas) throws NoExtractionResultException, BusinessException {

        Map<String, List> extractedDatasMap = new HashMap<String, List>();
        List<PlateformeVO> selectedPlateformes = (List<PlateformeVO>) requestMetadatas.get(PlateformeVO.class.getSimpleName());
        List<Long> plateformesIds = buildPlateformesIds(selectedPlateformes);
        List<Long> projetSiteIds = buildProjetSiteIds(selectedPlateformes);
        List<Taxon> selectedTaxons = (List<Taxon>) requestMetadatas.get(Taxon.class.getSimpleName());
        List<Long> taxonsIds = buildTaxonsIds(selectedTaxons);
        DatesRequestParamVO datesRequestParamVO = (DatesRequestParamVO) requestMetadatas.get(DatesRequestParamVO.class.getSimpleName());

        List<MesurePhytoplancton> mesuresPhytoplancton = null;
        try {

            mesuresPhytoplancton = (List<MesurePhytoplancton>) phytoplanctonDAO.extractDatas(plateformesIds, projetSiteIds, datesRequestParamVO, taxonsIds);

            if (mesuresPhytoplancton == null || mesuresPhytoplancton.size() == 0) {
                throw new NoExtractionResultException(getLocalizationManager().getMessage(NoExtractionResultException.BUNDLE_SOURCE_PATH, PROPERTY_MSG_NO_EXTRACTION_RESULT));
            }

            extractedDatasMap.put(MAP_INDEX_0, mesuresPhytoplancton);
        } catch (PersistenceException e) {
            AbstractExtractor.LOGGER.error(e.getMessage());
            throw new BusinessException(e);
        }
        return extractedDatasMap;
    }

    private List<Long> buildPlateformesIds(List<PlateformeVO> selectedPlateformes) {
        List<Long> plateformesIds = new LinkedList<Long>();
        for (PlateformeVO plateforme : selectedPlateformes) {
            plateformesIds.add(plateforme.getId());
        }
        return plateformesIds;
    }

    private List<Long> buildProjetSiteIds(List<PlateformeVO> selectedPlateformes) {
        List<Long> projetSiteIds = new LinkedList<Long>();
        for (PlateformeVO plateforme : selectedPlateformes) {
            projetSiteIds.add(plateforme.getProjet().getId());
        }
        return projetSiteIds;
    }

    private List<Long> buildTaxonsIds(List<Taxon> selectedTaxons) {
        List<Long> taxonsIds = new LinkedList<Long>();
        for (Taxon taxon : selectedTaxons) {
            taxonsIds.add(taxon.getId());
            if (taxon.getTaxonsEnfants() != null && !taxon.getTaxonsEnfants().isEmpty()) {
                taxonsIds.addAll(buildTaxonsIds(taxon.getTaxonsEnfants()));
            }
        }
        return taxonsIds;
    }

    @Override
    protected void prepareRequestMetadatas(Map<String, Object> requestMetadatasMap) throws BusinessException {
        sortSelectedVariables(requestMetadatasMap);
    }

    @SuppressWarnings("unchecked")
    private void sortSelectedVariables(Map<String, Object> requestMetadatasMap) {
        Collections.sort((List<VariableVO>) requestMetadatasMap.get(VariableVO.class.getSimpleName()), new Comparator<VariableVO>() {

            @Override
            public int compare(VariableVO o1, VariableVO o2) {
                if (o1.getOrdreAffichageGroupe() == null) {
                    o1.setOrdreAffichageGroupe(0);
                }
                if (o2.getOrdreAffichageGroupe() == null) {
                    o2.setOrdreAffichageGroupe(0);
                }
                return o1.getOrdreAffichageGroupe().toString().concat(o1.getCode()).compareTo(o2.getOrdreAffichageGroupe().toString().concat(o2.getCode()));
            }
        });

    }

    /**
     *
     * @param selectedPlateformes
     * @return
     */
    protected Set<String> retrieveSitesCodes(List<PlateformeVO> selectedPlateformes) {
        Set<String> sitesNames = new HashSet<String>();
        for (PlateformeVO plateforme : selectedPlateformes) {
            sitesNames.add(Utils.createCodeFromString(plateforme.getNomSite()));
        }
        return sitesNames;
    }

}
