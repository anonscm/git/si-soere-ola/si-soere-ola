package org.inra.ecoinfo.glacpe.dataset.phytoplancton.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.glacpe.dataset.phytoplancton.IPhytoplanctonDAO;
import org.inra.ecoinfo.glacpe.dataset.phytoplancton.entity.MesurePhytoplancton;
import org.inra.ecoinfo.glacpe.dataset.phytoplancton.entity.ValeurMesurePhytoplancton;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.IDatatypeVariableUniteGLACPEDAO;
import org.inra.ecoinfo.glacpe.refdata.taxon.Taxon;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

import com.google.common.collect.Lists;

/**
 *
 * @author ptcherniati
 */
public abstract class AbstractPhytoplanctonRawsDatasOutputBuilder extends AbstractOutputBuilder {

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH_PHYTOPLANCTON = "org.inra.ecoinfo.glacpe.extraction.phytoplancton.messages";

    /**
     *
     */
    protected static final String MAP_INDEX_0 = "0";

    /**
     *
     */
    protected IPhytoplanctonDAO phytoplanctonDAO;

    /**
     *
     */
    protected IVariableDAO variableDAO;

    /**
     *
     */
    protected IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO;

    /**
     *
     * @param phytoplanctonDAO
     */
    public void setPhytoplanctonDAO(IPhytoplanctonDAO phytoplanctonDAO) {
        this.phytoplanctonDAO = phytoplanctonDAO;
    }

    /**
     *
     * @param variableDAO
     */
    public void setVariableDAO(IVariableDAO variableDAO) {
        this.variableDAO = variableDAO;
    }

    /**
     *
     * @param selectedPlateformes
     * @return
     */
    protected Set<String> retrieveSitesCodes(List<PlateformeVO> selectedPlateformes) {
        Set<String> sitesNames = new HashSet<String>();
        for (PlateformeVO plateforme : selectedPlateformes) {
            sitesNames.add(Utils.createCodeFromString(plateforme.getNomSite()));
        }
        return sitesNames;
    }

    /**
     *
     * @param datatypeVariableUniteDAO
     */
    public void setDatatypeVariableUniteDAO(IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO) {
        this.datatypeVariableUniteDAO = datatypeVariableUniteDAO;
    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException, NoExtractionResultException {
        ((DefaultParameter) parameters).getFilesMaps().add(super.buildOutput(parameters, PhytoplanctonRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE));
        return null;
    }

    /**
     *
     * @param sumBiovolume
     * @param mesuresPhytoplancton
     */
    protected void prepareBiovolumeSum(Map<Date, Map<String, Map<Long, List<MesurePhytoplancton>>>> sumBiovolume, List<MesurePhytoplancton> mesuresPhytoplancton) {
        for (MesurePhytoplancton mesurePhytoplancton : mesuresPhytoplancton) {
            if (sumBiovolume.get(mesurePhytoplancton.getDatePrelevement()) == null) {
                Map<String, Map<Long, List<MesurePhytoplancton>>> sumPlateforme = new HashMap<String, Map<Long, List<MesurePhytoplancton>>>();
                sumBiovolume.put(mesurePhytoplancton.getDatePrelevement(), sumPlateforme);
            }
            if (sumBiovolume.get(mesurePhytoplancton.getDatePrelevement()).get(mesurePhytoplancton.getSousSequence().getPlateforme().getCode()) == null) {
                Map<Long, List<MesurePhytoplancton>> sumTaxon = new HashMap<Long, List<MesurePhytoplancton>>();
                sumBiovolume.get(mesurePhytoplancton.getDatePrelevement()).put(mesurePhytoplancton.getSousSequence().getPlateforme().getCode(), sumTaxon);
            }

            reccursiveSumTaxon(sumBiovolume, mesurePhytoplancton.getTaxon(), mesurePhytoplancton.getSousSequence().getPlateforme().getCode(), mesurePhytoplancton);
        }
    }

    private void reccursiveSumTaxon(Map<Date, Map<String, Map<Long, List<MesurePhytoplancton>>>> sumBiovolume, Taxon taxon, String plateformeCode, MesurePhytoplancton mesurePhyto) {
        if (sumBiovolume.get(mesurePhyto.getDatePrelevement()).get(plateformeCode).get(taxon.getId()) == null) {
            List<MesurePhytoplancton> sumMesures = Lists.newArrayList();
            sumBiovolume.get(mesurePhyto.getDatePrelevement()).get(plateformeCode).put(taxon.getId(), sumMesures);
        }
        sumBiovolume.get(mesurePhyto.getDatePrelevement()).get(plateformeCode).get(taxon.getId()).add(mesurePhyto);

        if (taxon.getTaxonParent() != null) {
            reccursiveSumTaxon(sumBiovolume, taxon.getTaxonParent(), plateformeCode, mesurePhyto);
        }

    }

    /**
     *
     * @param mesuresPhytoplancton
     * @return
     */
    protected Float sumBiovolume(List<MesurePhytoplancton> mesuresPhytoplancton) {
        Float biovolume = 0f;
        for (MesurePhytoplancton mesurePhytoplancton : mesuresPhytoplancton) {
            for (ValeurMesurePhytoplancton valeurPhyto : mesurePhytoplancton.getValeurs()) {
                if (valeurPhyto.getVariable().getCode().equals("biovolume_de_l_espece_dans_l_echantillon")) {
                    biovolume += valeurPhyto.getValeur();
                }
            }
        }
        return biovolume;
    }
}
