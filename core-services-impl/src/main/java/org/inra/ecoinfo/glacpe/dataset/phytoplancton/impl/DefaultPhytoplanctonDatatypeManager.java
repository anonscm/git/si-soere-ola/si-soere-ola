package org.inra.ecoinfo.glacpe.dataset.phytoplancton.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.inra.ecoinfo.MO;
import org.inra.ecoinfo.glacpe.dataset.phytoplancton.IPhytoplanctonDAO;
import org.inra.ecoinfo.glacpe.dataset.phytoplancton.IPhytoplanctonDatatypeManager;
import org.inra.ecoinfo.glacpe.extraction.vo.VariableVO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.projetsite.ProjetSite;
import org.inra.ecoinfo.glacpe.refdata.taxon.ITaxonDAO;
import org.inra.ecoinfo.glacpe.refdata.taxon.Taxon;
import org.inra.ecoinfo.glacpe.refdata.taxon.TaxonProprieteTaxon;
import org.inra.ecoinfo.glacpe.refdata.taxon.ValeurProprieteTaxon;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.datatype.IDatatypeDAO;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ptcherniati
 */
public class DefaultPhytoplanctonDatatypeManager extends MO implements IPhytoplanctonDatatypeManager {

    /**
     *
     */
    protected IPhytoplanctonDAO phytoplanctonDAO;

    /**
     *
     */
    protected IDatatypeDAO datatypeDAO;

    /**
     *
     */
    protected ITaxonDAO taxonDAO;

    /**
     *
     * @return @throws BusinessException
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public List<Projet> retrievePhytoplanctonAvailablesProjets() throws BusinessException {
        try {
            return phytoplanctonDAO.retrievePhytoplanctonAvailablesProjets();
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    /**
     *
     * @param phytoplanctonDAO
     */
    public void setPhytoplanctonDAO(IPhytoplanctonDAO phytoplanctonDAO) {
        this.phytoplanctonDAO = phytoplanctonDAO;
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    private List<VariableVO> retrieveListVariables() throws BusinessException {
        try {
            List<VariableGLACPE> variables = (List) ((DataType) datatypeDAO.getByCode("phytoplancton")).getVariables();
            List<VariableVO> variablesVO = new ArrayList<VariableVO>();

            for (VariableGLACPE variable : variables) {
                VariableVO variableVO = new VariableVO(variable);
                variablesVO.add(variableVO);
            }
            return variablesVO;
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    private List<VariableVO> retrieveListAvailableVariables(List<Long> plateformesIds, Date firstDate, Date endDate) throws BusinessException {
        try {
            List<VariableGLACPE> variables = phytoplanctonDAO.retrievePhytoplanctonAvailablesVariables(plateformesIds, firstDate, endDate);
            List<VariableVO> variablesVO = new ArrayList<VariableVO>();

            for (VariableGLACPE variable : variables) {
                VariableVO variableVO = new VariableVO(variable);
                variablesVO.add(variableVO);
            }
            return variablesVO;
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    /**
     *
     * @param plateformesIds
     * @param firstDate
     * @param lastDate
     * @return
     * @throws BusinessException
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public List<VariableVO> retrieveListsVariables(List<Long> plateformesIds, Date firstDate, Date lastDate) throws BusinessException {
        Map<String, VariableVO> rawVariablesMap = new HashMap<String, VariableVO>();

        for (VariableVO variable : retrieveListVariables()) {
            rawVariablesMap.put(variable.getCode(), variable);
        }

        for (VariableVO availableVariable : retrieveListAvailableVariables(plateformesIds, firstDate, lastDate)) {
            availableVariable.setIsDataAvailable(true);
            rawVariablesMap.put(availableVariable.getCode(), availableVariable);
        }

        return new LinkedList<VariableVO>(rawVariablesMap.values());
    }

    /**
     *
     * @return @throws BusinessException
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public Map<Taxon, Map<String, ValeurProprieteTaxon>> retrievePhytoplanctonAvailablesTaxons() throws BusinessException {
        try {
            List<Taxon> taxons = taxonDAO.getPhytoTaxonInfos();
            Map<Taxon, Map<String, ValeurProprieteTaxon>> taxonsInfos = null;

            if (taxons != null && !taxons.isEmpty()) {
                taxonsInfos = new HashMap<Taxon, Map<String, ValeurProprieteTaxon>>();

                for (Taxon taxon : (List<Taxon>) taxons) {
                    Map<String, ValeurProprieteTaxon> valeurs = new HashMap<String, ValeurProprieteTaxon>();
                    if (taxon.isLeaf()) {
                        for (TaxonProprieteTaxon tpt : taxon.getTaxonProprieteTaxon()) {
                            if (tpt.getProprieteTaxon().getCode().equals("longueur_de_la_cellule") || tpt.getProprieteTaxon().getCode().equals("valeur_du_biovolume_specifique_choisi_pour_le_taxon")
                                    || tpt.getProprieteTaxon().getCode().equals("classe_algale_sensu_bourrelly")) {
                                valeurs.put(tpt.getProprieteTaxon().getCode(), tpt.getValeurProprieteTaxon());
                            }
                        }
                    }
                    taxonsInfos.put(taxon, valeurs);
                }

            }
            return taxonsInfos;
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    /**
     *
     * @return @throws BusinessException
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public List<Taxon> retrievePhytoplanctonRootTaxon() throws BusinessException {
        try {
            List<Taxon> taxonsRoots = taxonDAO.getPhytoRootTaxa();
            return taxonsRoots;
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    /**
     *
     * @param datatypeDAO
     */
    public void setDatatypeDAO(IDatatypeDAO datatypeDAO) {
        this.datatypeDAO = datatypeDAO;
    }

    /**
     *
     * @param taxonDAO
     */
    public void setTaxonDAO(ITaxonDAO taxonDAO) {
        this.taxonDAO = taxonDAO;
    }

    /**
     *
     * @param projetSiteId
     * @return
     * @throws BusinessException
     */
    @Override
    public List<Plateforme> retrievePhytoplanctonAvailablesPlateformesByProjetSiteId(long projetSiteId) throws BusinessException {
        try {
            return phytoplanctonDAO.retrievePhytoplanctonAvailablesPlateformesByProjetSiteId(projetSiteId);
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    /**
     *
     * @param projetId
     * @param siteId
     * @return
     * @throws BusinessException
     */
    @Override
    public ProjetSite retrievePhytoplanctonAvailablesProjetsSiteByProjetAndSite(long projetId, long siteId) throws BusinessException {
        try {
            return phytoplanctonDAO.retrievePhytoplanctonAvailablesProjetsSiteByProjetAndSite(projetId, siteId);
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    /**
     *
     * @param taxonId
     * @return
     * @throws BusinessException
     */
    @Override
    public List<TaxonProprieteTaxon> retrieveTaxonPropertyByTaxonId(long taxonId) throws BusinessException {
        try {
            return taxonDAO.getTaxonProprieteTaxonByTaxonId(taxonId);
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    /**
     *
     * @param taxonCode
     * @return
     * @throws BusinessException
     */
    @Override
    public Taxon retrieveTaxonByCode(String taxonCode) throws BusinessException {
        try {
            return taxonDAO.getByCode(taxonCode);
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }
}
