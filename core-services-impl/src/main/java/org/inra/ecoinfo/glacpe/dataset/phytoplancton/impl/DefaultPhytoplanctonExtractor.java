package org.inra.ecoinfo.glacpe.dataset.phytoplancton.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.inra.ecoinfo.MO;
import org.inra.ecoinfo.extraction.IExtractor;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.config.impl.Extraction;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.glacpe.extraction.vo.VariableVO;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.notifications.entity.Notification;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public class DefaultPhytoplanctonExtractor extends MO implements IExtractor {

    /**
     *
     */
    protected IExtractor phytoplanctonDatasExtractor;

    /**
     *
     */
    protected IExtractor phytoplanctonAggregatedDatasExtractor;
    private static final String MSG_EXTRACTION_ABORTED = "PROPERTY_MSG_FAILED_EXTRACT";

    @Override
    public void setExtraction(Extraction extraction) {

    }

    @Override
    public Extraction getExtraction() {
        return null;
    }

    private void addOutputBuilderByCondition(Boolean condition, IExtractor extractor, List<IExtractor> extractors) {
        if (condition) {
            extractors.add(extractor);
        }
    }

    /**
     *
     * @param metadatasMap
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<IExtractor> resolveExtractors(Map<String, Object> metadatasMap) {
        List<IExtractor> extractors = new LinkedList<IExtractor>();

        addOutputBuilderByCondition(!((List<VariableVO>) metadatasMap.get(VariableVO.class.getSimpleName())).isEmpty(), phytoplanctonDatasExtractor, extractors);
        addOutputBuilderByCondition((Boolean) metadatasMap.get("aggregated"), phytoplanctonAggregatedDatasExtractor, extractors);
        return extractors;
    }

    @Override
    public void extract(IParameter parameters) throws BusinessException {
        Utilisateur utilisateur = securityContext.getUtilisateur();
        for (IExtractor extractor : resolveExtractors(parameters.getParameters())) {
            try {
                extractor.extract(parameters);
            } catch (NoExtractionResultException e) {
                sendNotification(String.format(localizationManager.getMessage(NoExtractionResultException.BUNDLE_SOURCE_PATH, MSG_EXTRACTION_ABORTED)), Notification.ERROR, e.getMessage(), utilisateur);
                throw new BusinessException(e);
            } catch (BusinessException e) {
                throw e;
            }
        }
    }

    /**
     *
     * @param phytoplanctonDatasExtractor
     */
    public void setPhytoplanctonDatasExtractor(IExtractor phytoplanctonDatasExtractor) {
        this.phytoplanctonDatasExtractor = phytoplanctonDatasExtractor;
    }

    /**
     *
     * @param phytoplanctonAggregatedDatasExtractor
     */
    public void setPhytoplanctonAggregatedDatasExtractor(IExtractor phytoplanctonAggregatedDatasExtractor) {
        this.phytoplanctonAggregatedDatasExtractor = phytoplanctonAggregatedDatasExtractor;
    }

    /**
     *
     * @param parameters
     * @return
     */
    @Override
    public long getExtractionSize(IParameter parameters) {
        // TODO Auto-generated method stub
        return 0;
    }

}
