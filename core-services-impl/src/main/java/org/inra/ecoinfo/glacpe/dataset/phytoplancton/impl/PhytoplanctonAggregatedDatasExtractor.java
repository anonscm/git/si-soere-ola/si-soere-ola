package org.inra.ecoinfo.glacpe.dataset.phytoplancton.impl;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.AbstractExtractor;
import org.inra.ecoinfo.glacpe.dataset.phytoplancton.IPhytoplanctonDAO;
import org.inra.ecoinfo.glacpe.dataset.phytoplancton.entity.ValeurMesurePhytoplancton;
import org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEAggregatedDatasExtractor;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class PhytoplanctonAggregatedDatasExtractor extends AbstractGLACPEAggregatedDatasExtractor {

    private static final String MAP_INDEX_0 = "0";

    /**
     *
     */
    public static String CST_RESULT_EXTRACTION_CODE = "phytoplanctonAggregatedDatas";

    /**
     *
     */
    protected static final String PROPERTY_MSG_NO_EXTRACTION_RESULT = "PROPERTY_MSG_NO_EXTRACTION_RESULT";

    /**
     *
     */
    protected IPhytoplanctonDAO phytoplanctonDAO;

    /**
     *
     */
    protected IVariableDAO variableDAO;

    @SuppressWarnings("rawtypes")
    @Override
    public void extract(IParameter parameters) throws BusinessException {
        prepareRequestMetadatas(parameters.getParameters());
        Map<String, List> resultsDatasMap = extractDatas(parameters.getParameters());
        Map<String, List> filteredResultsDatasMap = filterExtractedDatas(resultsDatasMap);
        ((PhytoplanctonParameters) parameters).getResults().put(CST_RESULT_EXTRACTION_CODE, filteredResultsDatasMap);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, List> extractDatas(Map<String, Object> requestMetadatasMap) throws NoExtractionResultException, BusinessException {
        Map<String, List> extractedDatasMap = new HashMap<String, List>();
        List<PlateformeVO> selectedPlateformes = (List<PlateformeVO>) requestMetadatasMap.get(PlateformeVO.class.getSimpleName());
        List<Long> plateformesIds = buildPlateformesIds(selectedPlateformes);
        List<Long> projetSiteIds = buildProjetSiteIds(selectedPlateformes);
        List<Long> variablesIds = buildVariablesIds();
        DatesRequestParamVO datesRequestParamVO = (DatesRequestParamVO) requestMetadatasMap.get(DatesRequestParamVO.class.getSimpleName());

        List<ValeurMesurePhytoplancton> valeurMesurePhytoplancton = null;

        try {
            valeurMesurePhytoplancton = phytoplanctonDAO.extractAggregatedDatas(plateformesIds, variablesIds, projetSiteIds, datesRequestParamVO);
            extractedDatasMap.put(MAP_INDEX_0, valeurMesurePhytoplancton);

            if (valeurMesurePhytoplancton == null || valeurMesurePhytoplancton.size() == 0) {
                throw new NoExtractionResultException(getLocalizationManager().getMessage(NoExtractionResultException.BUNDLE_SOURCE_PATH, PROPERTY_MSG_NO_EXTRACTION_RESULT));
            }

        } catch (PersistenceException e) {
            AbstractExtractor.LOGGER.error(e.getMessage());
            throw new BusinessException(e);
        }
        return extractedDatasMap;
    }

    private List<Long> buildPlateformesIds(List<PlateformeVO> selectedPlateformes) {
        List<Long> plateformesIds = new LinkedList<Long>();
        for (PlateformeVO plateforme : selectedPlateformes) {
            plateformesIds.add(plateforme.getId());
        }
        return plateformesIds;
    }

    private List<Long> buildProjetSiteIds(List<PlateformeVO> selectedPlateformes) {
        List<Long> projetSiteIds = new LinkedList<Long>();
        for (PlateformeVO plateforme : selectedPlateformes) {
            projetSiteIds.add(plateforme.getProjet().getId());
        }
        return projetSiteIds;
    }

    private List<Long> buildVariablesIds() {

        List<Long> variablesIds = new LinkedList<Long>();
        try {
            Variable variable = variableDAO.getByCode("biovolume_de_l_espece_dans_l_echantillon");
            variablesIds.add(variable.getId());
        } catch (PersistenceException e) {
            e.printStackTrace();
        }
        return variablesIds;
    }

    @SuppressWarnings("rawtypes")
    @Override
    protected Map<String, List> filterExtractedDatas(Map<String, List> resultsDatasMap) throws BusinessException {
        filterExtractedValues(resultsDatasMap, MAP_INDEX_0);
        return resultsDatasMap;
    }

    @Override
    protected void prepareRequestMetadatas(Map<String, Object> requestMetadatasMap) throws BusinessException {
        // Rien à faire de spécial
    }

    /**
     *
     * @param phytoplanctonDAO
     */
    public void setPhytoplanctonDAO(IPhytoplanctonDAO phytoplanctonDAO) {
        this.phytoplanctonDAO = phytoplanctonDAO;
    }

    /**
     *
     * @param variableDAO
     */
    public void setVariableDAO(IVariableDAO variableDAO) {
        this.variableDAO = variableDAO;
    }

}
