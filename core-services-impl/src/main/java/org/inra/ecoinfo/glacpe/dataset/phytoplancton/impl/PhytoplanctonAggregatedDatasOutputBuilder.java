package org.inra.ecoinfo.glacpe.dataset.phytoplancton.impl;

import java.io.File;
import java.io.PrintStream;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.glacpe.dataset.IGLACPEAggregateData;
import org.inra.ecoinfo.glacpe.dataset.VariableAggregatedDatas;
import org.inra.ecoinfo.glacpe.dataset.VariableAggregatedDatas.AggregatedData;
import org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEAggregatedDatasOutputBuilder;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO;
import org.inra.ecoinfo.glacpe.extraction.vo.ProjetSiteVO;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.OutilsMesure;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.IDatatypeVariableUniteGLACPEDAO;

/**
 *
 * @author ptcherniati
 */
public class PhytoplanctonAggregatedDatasOutputBuilder extends AbstractGLACPEAggregatedDatasOutputBuilder {

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH_PHYTOPLANCTON = "org.inra.ecoinfo.glacpe.extraction.phytoplancton.messages";

    private static final String HEADER_RAW_DATA_ALLDEPTH = "PROPERTY_MSG_HEADER_AGGREGATED_DATA";

    /**
     *
     */
    protected static final String SUFFIX_FILENAME_AGGREGATED = "aggregated";
    
    protected IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO;

    @Override
    protected String buildHeader(Map<String, Object> requestMetadatasMap) throws BusinessException {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format("%s",
                                           getLocalizationManager().
                                                   getMessage(
                                                           BUNDLE_SOURCE_PATH_PHYTOPLANCTON,
                                                           HEADER_RAW_DATA_ALLDEPTH)));
        return stringBuilder.toString();
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    @Override
    protected Map<String, File> buildBody(String headers,
                                          Map<String, List> resultsDatasMap,
                                          Map<String, Object> requestMetadatasMap) throws BusinessException {
        List<PlateformeVO> selectedPlateformes = (List<PlateformeVO>) requestMetadatasMap.
                get(PlateformeVO.class.getSimpleName());
        List<IGLACPEAggregateData> valeursMesures = resultsDatasMap.get(
                MAP_INDEX_0);
        ProjetSiteVO selectedProjet = selectedPlateformes.get(0).
                getProjet();
        Set<String> sitesNames = retrieveSitesCodes(selectedPlateformes);
        Map<String, File> filesMap = buildOutputsFiles(sitesNames,
                                                       SUFFIX_FILENAME_AGGREGATED);
        Map<String, PrintStream> outputPrintStreamMap = buildOutputPrintStreamMap(
                filesMap);
        for (String siteName : outputPrintStreamMap.keySet()) {
            outputPrintStreamMap.get(siteName).
                    println(headers);
        }

        List<VariableAggregatedDatas> variablesAggregatedDatas = buildVariablesAggregated(
                valeursMesures);

        Properties propertiesProjetName = localizationManager.newProperties(
                Projet.NAME_TABLE,
                "nom");
        Properties propertiesPlateformeName = localizationManager.newProperties(
                Plateforme.TABLE_NAME,
                "nom");
        Properties propertiesSiteName = localizationManager.newProperties(
                Site.NAME_ENTITY_JPA,
                "nom");
        Properties propertiesOutilName = localizationManager.newProperties(
                OutilsMesure.TABLE_NAME,
                "nom");

        Iterator<VariableAggregatedDatas> iterator = variablesAggregatedDatas.
                iterator();
        while (iterator.hasNext()) {
            VariableAggregatedDatas variableAggregatedData = iterator.next();
            AggregatedData aggregatedData = null;
            if (variableAggregatedData != null) {
                PrintStream rawDataPrintStream = outputPrintStreamMap.get(Utils.
                        createCodeFromString(variableAggregatedData.
                                getSiteName()));

                String localizedProjetName = propertiesProjetName.getProperty(
                        selectedProjet.getNom());
                if (Strings.isNullOrEmpty(localizedProjetName)) {
                    localizedProjetName = selectedProjet.getNom();
                }

                String localizedSiteName = propertiesSiteName.getProperty(
                        variableAggregatedData.getSiteName());
                if (Strings.isNullOrEmpty(localizedSiteName)) {
                    localizedSiteName = variableAggregatedData.getSiteName();
                }

                String localizedPlateformeName = propertiesPlateformeName.
                        getProperty(variableAggregatedData.getPlateformeName());
                if (Strings.isNullOrEmpty(localizedPlateformeName)) {
                    localizedPlateformeName = variableAggregatedData.
                            getPlateformeName();
                }

                String localizedOutilPrelevementName = variableAggregatedData.
                        getOutilsPrelevement() == null ? "" : propertiesOutilName.
                                        getProperty(variableAggregatedData.
                                                getOutilsPrelevement().
                                                getNom());
                if (Strings.isNullOrEmpty(localizedOutilPrelevementName)) {
                    localizedOutilPrelevementName = variableAggregatedData.
                            getOutilsPrelevement() == null ? "" : variableAggregatedData.
                                            getOutilsPrelevement().
                                            getNom();
                }

                String localizedOutilMesureName = variableAggregatedData.
                        getOutilsMesure() == null ? "" : propertiesOutilName.
                                        getProperty(variableAggregatedData.
                                                getOutilsMesure().
                                                getNom());
                if (Strings.isNullOrEmpty(localizedOutilMesureName)) {
                    localizedOutilMesureName = variableAggregatedData.
                            getOutilsMesure() == null ? "" : variableAggregatedData.
                                            getOutilsMesure().
                                            getNom();
                }

                aggregatedData = variableAggregatedData.
                        getAverageAggregatedData();
                List<Date> datesKeys = new LinkedList<Date>(aggregatedData.
                        getValuesByDatesMap().
                        keySet());
                Collections.sort(datesKeys);
                for (Date dateKey : datesKeys) {

                    rawDataPrintStream.print(String.format("%s;%s;%s;",
                                                           localizedProjetName,
                                                           localizedSiteName,
                                                           localizedPlateformeName));
                    rawDataPrintStream.print(String.format("%s;%s;%s;",
                                                           DateUtil.
                                                                   getSimpleDateFormatDateLocale().
                                                                   format(dateKey),
                                                           localizedOutilPrelevementName,
                                                           localizedOutilMesureName));
                    rawDataPrintStream.print(String.format("%s",
                                                           aggregatedData.
                                                                   getValuesByDatesMap().
                                                                   get(dateKey)));

                    rawDataPrintStream.println();
                }
                iterator.remove();
            }
        }
        closeStreams(outputPrintStreamMap);
        return filesMap;
    }

    /**
     *
     * @param valeursMesures
     * @return
     */
    protected List<VariableAggregatedDatas> buildVariablesAggregated(
            List<IGLACPEAggregateData> valeursMesures) {
        Map<Site, Map<Plateforme, Map<Date, Map<VariableGLACPE, List<IGLACPEAggregateData>>>>> valeursMesuresReorderedByDatesVariablesAndDates = buildValeursMesuresReorderedByVariablesAndDates(
                valeursMesures);
        List<VariableAggregatedDatas> variablesAggregatedDatas = new LinkedList<VariableAggregatedDatas>();

        for (Site site : valeursMesuresReorderedByDatesVariablesAndDates.
                keySet()) {
            for (Plateforme plateforme : valeursMesuresReorderedByDatesVariablesAndDates.
                    get(site).
                    keySet()) {
                Map<Date, Map<VariableGLACPE, List<IGLACPEAggregateData>>> valeursMesuresReorderedByVariablesAndDates = valeursMesuresReorderedByDatesVariablesAndDates.
                        get(site).
                        get(plateforme);
                List<Date> datesKeys = Lists.newArrayList(valeursMesuresReorderedByVariablesAndDates.
                        keySet());
                Collections.sort(datesKeys);
                for (Date dateKey : datesKeys) {
                    Map<VariableGLACPE, List<IGLACPEAggregateData>> valeursMesuresDateMap = valeursMesuresReorderedByVariablesAndDates.
                            get(dateKey);
                    for (VariableGLACPE variableKey : valeursMesuresDateMap.
                            keySet()) {
                        VariableAggregatedDatas variableAggregatedDatas = new VariableAggregatedDatas(
                                site.getNom(),
                                plateforme.getNom(),
                                variableKey.getNom());
                        List<IGLACPEAggregateData> valeursMesureDates = valeursMesuresDateMap.
                                get(variableKey);
                        fillVariableAggregatedDatas(variableAggregatedDatas,
                                                    dateKey,
                                                    valeursMesureDates);
                        variablesAggregatedDatas.add(variableAggregatedDatas);
                    }
                }
            }
        }

        sortVariablesAggregatedDatas(variablesAggregatedDatas);
        return variablesAggregatedDatas;
    }
    
    /**
     *
     * @param variableAggregatedDatas
     * @param dateKey
     * @param valeursMesureDates
     */
    @Override
    protected void fillVariableAggregatedDatas(
            VariableAggregatedDatas variableAggregatedDatas,
            Date dateKey,
            List<IGLACPEAggregateData> valeursMesureDates) {
        int index = 0;
        Float m = 0f;

        variableAggregatedDatas.setOutilsMesure(valeursMesureDates.get(0).
                getOutilsMesure());
        variableAggregatedDatas.setOutilsPrelevement(valeursMesureDates.get(0).
                getOutilsPrelevement());
        Iterator<IGLACPEAggregateData> iterator = valeursMesureDates.iterator();
        
        final int size = valeursMesureDates.size();
        
        while (iterator.hasNext()) {
            IGLACPEAggregateData valeurMesure = iterator.next();
            if (valeurMesure.getValue() != null) {
                if (index <= size - 1) {
                    index++;
                    m += valeurMesure.getValue();
                }
            }
            iterator.remove();
        }

        variableAggregatedDatas.getAverageAggregatedData().
                getValuesByDatesMap().
                put(dateKey,
                    m);
    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException, NoExtractionResultException {
        ((DefaultParameter) parameters).getFilesMaps().
                add(super.buildOutput(parameters,
                                      PhytoplanctonAggregatedDatasExtractor.CST_RESULT_EXTRACTION_CODE));

        return null;
    }
    
    /**
     *
     * @param datatypeVariableUniteDAO
     */
    public void setDatatypeVariableUniteDAO(IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO) {
        this.datatypeVariableUniteDAO = datatypeVariableUniteDAO;
    }

}
