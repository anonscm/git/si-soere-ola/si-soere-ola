package org.inra.ecoinfo.glacpe.dataset.phytoplancton.impl;

import java.util.Map;

import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;

/**
 *
 * @author ptcherniati
 */
public class PhytoplanctonParameters extends DefaultParameter implements IParameter {

    /**
     *
     */
    public static final String CODE_EXTRACTIONTYPE_PHYTOPLANCTON = "phytoplancton";

    /**
     *
     * @param metadatasMap
     */
    public PhytoplanctonParameters(Map<String, Object> metadatasMap) {
        setParameters(metadatasMap);
        setCommentaire((String) metadatasMap.get(IExtractionManager.KEYMAP_COMMENTS));
    }

    @Override
    public String getExtractionTypeCode() {
        return CODE_EXTRACTIONTYPE_PHYTOPLANCTON;
    }

}
