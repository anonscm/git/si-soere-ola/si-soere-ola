package org.inra.ecoinfo.glacpe.dataset.phytoplancton.impl;

import java.util.List;
import java.util.Map;

import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public class PhytoplanctonRawsDatasExtractor extends AbstractPhytoplanctonRawsDatasExtractor {

    /**
     *
     */
    public static String CST_RESULT_EXTRACTION_CODE = "phytoplanctonRawDatas";

    @SuppressWarnings("rawtypes")
    @Override
    protected Map<String, List> filterExtractedDatas(Map<String, List> resultsDatasMap) throws BusinessException {
        filterExtractedDatas(resultsDatasMap, MAP_INDEX_0);
        return resultsDatasMap;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public void extract(IParameter parameters) throws BusinessException {
        prepareRequestMetadatas(parameters.getParameters());
        Map<String, List> resultsDatasMap = extractDatas(parameters.getParameters());
        Map<String, List> filteredResultsDatasMap = filterExtractedDatas(resultsDatasMap);
        ((PhytoplanctonParameters) parameters).getResults().put(CST_RESULT_EXTRACTION_CODE, filteredResultsDatasMap);
    }

}
