package org.inra.ecoinfo.glacpe.dataset.phytoplancton.impl;

import java.io.File;
import java.io.PrintStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.inra.ecoinfo.glacpe.dataset.phytoplancton.entity.MesurePhytoplancton;
import org.inra.ecoinfo.glacpe.dataset.phytoplancton.entity.ValeurMesurePhytoplancton;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO;
import org.inra.ecoinfo.glacpe.extraction.vo.VariableVO;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.OutilsMesure;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.taxon.Taxon;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import com.google.common.base.Strings;
import java.util.Collections;
import org.assertj.core.util.Lists;

/**
 *
 * @author ptcherniati
 */
public class PhytoplanctonRawsDatasOutputBuilderByLine extends AbstractPhytoplanctonRawsDatasOutputBuilder {

    private static final String HEADER_RAW_DATA_ALLDEPTH = "PROPERTY_MSG_HEADER_RAW_DATA_ROW";
    private static final String CODE_DATATYPE_PHYTOPLANCTON = "phytoplancton";

    @Override
    protected String buildHeader(Map<String, Object> requestMetadatasMap) throws BusinessException {
        StringBuilder stringBuilder = new StringBuilder();

        try {
            stringBuilder.append(String.format("%s", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_PHYTOPLANCTON, HEADER_RAW_DATA_ALLDEPTH)));
        } catch (Exception e) {
            throw new BusinessException(e);
        }
        return stringBuilder.toString();
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {
        List<VariableVO> selectedVariables = (List<VariableVO>) requestMetadatasMap.get(VariableVO.class.getSimpleName());
        List<Taxon> selectedTaxons = (List<Taxon>) requestMetadatasMap.get(Taxon.class.getSimpleName());
        List<MesurePhytoplancton> mesuresPhytoplancton = resultsDatasMap.get(MAP_INDEX_0);

        Properties propertiesProjetName = localizationManager.newProperties(Projet.NAME_TABLE, "nom");
        Properties propertiesPlateformeName = localizationManager.newProperties(Plateforme.TABLE_NAME, "nom");
        Properties propertiesSiteName = localizationManager.newProperties(Site.NAME_ENTITY_JPA, "nom");
        Properties propertiesOutilName = localizationManager.newProperties(OutilsMesure.TABLE_NAME, "nom");
        Properties propertiesVariableName = localizationManager.newProperties(Variable.NAME_ENTITY_JPA, "nom");

        Boolean sommation = (Boolean) requestMetadatasMap.get("sommation");
        List<PlateformeVO> selectedPlateformes = (List<PlateformeVO>) requestMetadatasMap.get(PlateformeVO.class.getSimpleName());
        Set<String> sitesNames = retrieveSitesCodes(selectedPlateformes);
        Map<Date, Map<String, Map<Long, List<MesurePhytoplancton>>>> sumBiovolumeMap = new HashMap<Date, Map<String, Map<Long, List<MesurePhytoplancton>>>>();
        Map<String, File> filesMap = buildOutputsFiles(sitesNames, SUFFIX_FILENAME_DEFAULT);
        Map<String, PrintStream> outputPrintStreamMap = buildOutputPrintStreamMap(filesMap);
        for (String siteName : outputPrintStreamMap.keySet()) {
            outputPrintStreamMap.get(siteName).println(headers);
        }

        prepareBiovolumeSum(sumBiovolumeMap, mesuresPhytoplancton);
        
        List<Date> dates = Lists.newArrayList(sumBiovolumeMap.keySet());
        Collections.sort(dates);
        Iterator<Date> dateIt = dates.iterator();
        
        while (dateIt.hasNext()) {
            Date date = dateIt.next();
            for (PlateformeVO plateformeVO : selectedPlateformes) {
                for (Taxon taxon : selectedTaxons) {
                    if (sumBiovolumeMap.get(date).containsKey(plateformeVO.getCode()) && sumBiovolumeMap.get(date).get(plateformeVO.getCode()).get(taxon.getId()) != null) {
                        try {
                            String localizedProjetName = propertiesProjetName.getProperty(plateformeVO.getProjet().getNom());
                            if (Strings.isNullOrEmpty(localizedProjetName))
                                localizedProjetName = plateformeVO.getProjet().getNom();

                            String localizedSiteName = propertiesSiteName.getProperty(plateformeVO.getNomSite());
                            if (Strings.isNullOrEmpty(localizedSiteName))
                                localizedSiteName = plateformeVO.getNomSite();

                            String localizedPlateformeName = propertiesPlateformeName.getProperty(plateformeVO.getNom());
                            if (Strings.isNullOrEmpty(localizedPlateformeName))
                                localizedPlateformeName = plateformeVO.getNom();

                            String localizedOutilNamePrelev = sumBiovolumeMap.get(date).get(plateformeVO.getCode()).get(taxon.getId()).get(0).getSousSequence().getOutilsPrelevement() != null ? propertiesOutilName.getProperty(sumBiovolumeMap
                                    .get(date).get(plateformeVO.getCode()).get(taxon.getId()).get(0).getSousSequence().getOutilsPrelevement().getNom()) : null;
                            if (Strings.isNullOrEmpty(localizedOutilNamePrelev))
                                localizedOutilNamePrelev = sumBiovolumeMap.get(date).get(plateformeVO.getCode()).get(taxon.getId()).get(0).getSousSequence().getOutilsPrelevement() != null ? sumBiovolumeMap.get(date).get(plateformeVO.getCode())
                                        .get(taxon.getId()).get(0).getSousSequence().getOutilsPrelevement().getNom() : null;

                            String localizedOutilNameMesure = sumBiovolumeMap.get(date).get(plateformeVO.getCode()).get(taxon.getId()).get(0).getSousSequence().getOutilsMesure() != null ? propertiesOutilName.getProperty(sumBiovolumeMap.get(date)
                                    .get(plateformeVO.getCode()).get(taxon.getId()).get(0).getSousSequence().getOutilsMesure().getNom()) : null;
                            if (Strings.isNullOrEmpty(localizedOutilNameMesure))
                                localizedOutilNameMesure = sumBiovolumeMap.get(date).get(plateformeVO.getCode()).get(taxon.getId()).get(0).getSousSequence().getOutilsMesure() != null ? sumBiovolumeMap.get(date).get(plateformeVO.getCode())
                                        .get(taxon.getId()).get(0).getSousSequence().getOutilsMesure().getNom() : null;

                            PrintStream rawDataPrintStream = outputPrintStreamMap.get(((SiteGLACPE) sumBiovolumeMap.get(date).get(plateformeVO.getCode()).get(taxon.getId()).get(0).getSite()).getCodeFromName());
                            String lineSkel = String.format("%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s", localizedProjetName, localizedSiteName, localizedPlateformeName, DateUtil.getSimpleDateFormatDateLocale().format(date), localizedOutilNamePrelev,
                                    localizedOutilNameMesure, sumBiovolumeMap.get(date).get(plateformeVO.getCode()).get(taxon.getId()).get(0).getSousSequence().getProfondeurMin(),
                                    sumBiovolumeMap.get(date).get(plateformeVO.getCode()).get(taxon.getId()).get(0).getSousSequence().getProfondeurMax(), sumBiovolumeMap.get(date).get(plateformeVO.getCode()).get(taxon.getId()).get(0)
                                            .getSousSequence().getSequence().getNomDeterminateur(), sumBiovolumeMap.get(date).get(plateformeVO.getCode()).get(taxon.getId()).get(0).getSousSequence().getSequence().getVolumeSedimente(), sumBiovolumeMap
                                            .get(date).get(plateformeVO.getCode()).get(taxon.getId()).get(0).getSousSequence().getSurfaceComptage(), taxon.getNomLatin());

                            Map<Long, Float> valeursMesuresPhytoplancton = buildValeurs(sumBiovolumeMap.get(date).get(plateformeVO.getCode()).get(taxon.getId()).get(0).getValeurs());

                            for (VariableVO variableVO : selectedVariables) {
                                String localizedVariableName = propertiesVariableName.getProperty(variableVO.getNom());
                                if (Strings.isNullOrEmpty(localizedVariableName))
                                    localizedVariableName = variableVO.getNom();
                                Unite unite;
                                unite = datatypeVariableUniteDAO.getUnite(CODE_DATATYPE_PHYTOPLANCTON, variableVO.getCode());
                                String uniteNom;
                                if (unite == null) {
                                    uniteNom = "nounit";
                                } else {
                                    uniteNom = unite.getCode();
                                }

                                if (sommation && sumBiovolumeMap.get(date).get(plateformeVO.getCode()).get(taxon.getId()).size() > 1 && variableVO.getCode().equals("biovolume_de_l_espece_dans_l_echantillon")) {
                                    Float biovolume = sumBiovolume(sumBiovolumeMap.get(date).get(plateformeVO.getCode()).get(taxon.getId()));
                                    String line = lineSkel.concat(String.format(";%s;%s;%s", localizedVariableName, biovolume, uniteNom));
                                    rawDataPrintStream.print(line);
                                    rawDataPrintStream.println();
                                } else if (sumBiovolumeMap.get(date).get(plateformeVO.getCode()).get(taxon.getId()).size() == 1) {
                                    Float valeur = valeursMesuresPhytoplancton.get(variableVO.getId());
                                    if (valeur != null) {
                                        String line = lineSkel.concat(String.format(";%s;%s;%s", localizedVariableName, valeur, uniteNom));
                                        rawDataPrintStream.print(line);
                                        rawDataPrintStream.println();
                                    }
                                }
                            }
                        } catch (PersistenceException e) {
                            throw new BusinessException(e);
                        }
                    }
                }
            }
            //dateIt.remove();
        }

        closeStreams(outputPrintStreamMap);
        return filesMap;
    }

    private Map<Long, Float> buildValeurs(List<ValeurMesurePhytoplancton> valeurs) {
        Map<Long, Float> mapValue = new HashMap<Long, Float>();
        for (ValeurMesurePhytoplancton valeur : valeurs) {
            mapValue.put(valeur.getVariable().getId(), valeur.getValeur());
        }
        return mapValue;
    }
}
