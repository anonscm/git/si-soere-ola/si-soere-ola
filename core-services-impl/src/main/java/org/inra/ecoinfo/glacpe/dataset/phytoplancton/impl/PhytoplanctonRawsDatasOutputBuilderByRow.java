package org.inra.ecoinfo.glacpe.dataset.phytoplancton.impl;

import java.io.File;
import java.io.PrintStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.inra.ecoinfo.glacpe.dataset.phytoplancton.entity.MesurePhytoplancton;
import org.inra.ecoinfo.glacpe.dataset.phytoplancton.entity.ValeurMesurePhytoplancton;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO;
import org.inra.ecoinfo.glacpe.extraction.vo.VariableVO;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.OutilsMesure;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.taxon.Taxon;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.ErrorsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import com.google.common.base.Strings;
import java.util.Collections;
import org.assertj.core.util.Lists;

/**
 *
 * @author ptcherniati
 */
public class PhytoplanctonRawsDatasOutputBuilderByRow extends AbstractPhytoplanctonRawsDatasOutputBuilder {

    private static final String HEADER_RAW_DATA_ALLDEPTH = "PROPERTY_MSG_HEADER_RAW_DATA_LINE";
    private static final String MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS = "PROPERTY_MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS";
    private static final String PROPERTY_MSG_ERROR = "PROPERTY_MSG_ERROR";
    private static final String CODE_DATATYPE_PHYTOPLANCTON = "phytoplancton";

    @SuppressWarnings("unchecked")
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatas) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_PHYTOPLANCTON, PROPERTY_MSG_ERROR));
        Properties propertiesVariableName = localizationManager.newProperties(Variable.NAME_ENTITY_JPA, "nom");
        List<VariableVO> selectedVariables = (List<VariableVO>) requestMetadatas.get(VariableVO.class.getSimpleName());

        StringBuilder stringBuilder = new StringBuilder();

        try {
            stringBuilder.append(String.format("%s", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_PHYTOPLANCTON, HEADER_RAW_DATA_ALLDEPTH)));

            for (VariableVO variableVO : selectedVariables) {
                VariableGLACPE variable = (VariableGLACPE) variableDAO.getByCode(variableVO.getCode());
                if (variable == null) {
                    errorsReport.addException(new BusinessException(String.format(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_PHYTOPLANCTON, MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS), selectedVariables)));
                }
                String localizedVariableName = propertiesVariableName.getProperty(variable.getNom());
                if (Strings.isNullOrEmpty(localizedVariableName)) {
                    localizedVariableName = variable.getNom();
                }

                Unite unite = datatypeVariableUniteDAO.getUnite(CODE_DATATYPE_PHYTOPLANCTON, variable.getCode());
                String uniteNom;
                if (unite == null) {
                    uniteNom = "nounit";
                } else {
                    uniteNom = unite.getCode();
                }
                stringBuilder.append(String.format(";%s (%s)", localizedVariableName, uniteNom));

                if (errorsReport.hasErrors()) {
                    throw new PersistenceException(errorsReport.buildHTMLMessages());
                }
            }

        } catch (Exception e) {
            throw new BusinessException(e);
        }
        return stringBuilder.toString();
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {
        List<VariableVO> selectedVariables = (List<VariableVO>) requestMetadatasMap.get(VariableVO.class.getSimpleName());
        List<Taxon> selectedTaxons = (List<Taxon>) requestMetadatasMap.get(Taxon.class.getSimpleName());
        List<MesurePhytoplancton> mesuresPhytoplancton = resultsDatasMap.get(MAP_INDEX_0);

        Properties propertiesProjetName = localizationManager.newProperties(Projet.NAME_TABLE, "nom");
        Properties propertiesPlateformeName = localizationManager.newProperties(Plateforme.TABLE_NAME, "nom");
        Properties propertiesSiteName = localizationManager.newProperties(Site.NAME_ENTITY_JPA, "nom");
        Properties propertiesOutilName = localizationManager.newProperties(OutilsMesure.TABLE_NAME, "nom");

        Boolean sommation = (Boolean) requestMetadatasMap.get("sommation");
        List<PlateformeVO> selectedPlateformes = (List<PlateformeVO>) requestMetadatasMap.get(PlateformeVO.class.getSimpleName());
        Set<String> sitesNames = retrieveSitesCodes(selectedPlateformes);
        Map<Date, Map<String, Map<Long, List<MesurePhytoplancton>>>> sumBiovolumeMap = new HashMap<Date, Map<String, Map<Long, List<MesurePhytoplancton>>>>();
        Map<String, File> filesMap = buildOutputsFiles(sitesNames, SUFFIX_FILENAME_DEFAULT);
        Map<String, PrintStream> outputPrintStreamMap = buildOutputPrintStreamMap(filesMap);
        for (String siteName : outputPrintStreamMap.keySet()) {
            outputPrintStreamMap.get(siteName).println(headers);
        }

        prepareBiovolumeSum(sumBiovolumeMap, mesuresPhytoplancton);

        List<Date> dates = Lists.newArrayList(sumBiovolumeMap.keySet());
        Collections.sort(dates);
        Iterator<Date> dateIt = dates.iterator();
        
        while (dateIt.hasNext()) {
            Date date = dateIt.next();
            for (PlateformeVO plateformeVO : selectedPlateformes) {
                for (Taxon taxon : selectedTaxons) {
                    if (sumBiovolumeMap.get(date).containsKey(plateformeVO.getCode())) {
                        if (sumBiovolumeMap.get(date).get(plateformeVO.getCode()).get(taxon.getId()) != null) {
                            String localizedProjetName = propertiesProjetName.getProperty(plateformeVO.getProjet().getNom());
                            if (Strings.isNullOrEmpty(localizedProjetName)) {
                                localizedProjetName = plateformeVO.getProjet().getNom();
                            }

                            String localizedSiteName = propertiesSiteName.getProperty(plateformeVO.getNomSite());
                            if (Strings.isNullOrEmpty(localizedSiteName)) {
                                localizedSiteName = plateformeVO.getNomSite();
                            }

                            String localizedPlateformeName = propertiesPlateformeName.getProperty(plateformeVO.getNom());
                            if (Strings.isNullOrEmpty(localizedPlateformeName)) {
                                localizedPlateformeName = plateformeVO.getNom();
                            }

                            String localizedOutilNamePrelev = sumBiovolumeMap.get(date).get(plateformeVO.getCode()).get(taxon.getId()).get(0).getSousSequence().getOutilsPrelevement() != null ? propertiesOutilName.getProperty(sumBiovolumeMap.get(date)
                                    .get(plateformeVO.getCode()).get(taxon.getId()).get(0).getSousSequence().getOutilsPrelevement().getNom()) : null;
                            if (Strings.isNullOrEmpty(localizedOutilNamePrelev)) {
                                localizedOutilNamePrelev = sumBiovolumeMap.get(date).get(plateformeVO.getCode()).get(taxon.getId()).get(0).getSousSequence().getOutilsPrelevement() != null ? sumBiovolumeMap.get(date).get(plateformeVO.getCode())
                                        .get(taxon.getId()).get(0).getSousSequence().getOutilsPrelevement().getNom() : null;
                            }

                            String localizedOutilNameMesure = sumBiovolumeMap.get(date).get(plateformeVO.getCode()).get(taxon.getId()).get(0).getSousSequence().getOutilsMesure() != null ? propertiesOutilName.getProperty(sumBiovolumeMap.get(date)
                                    .get(plateformeVO.getCode()).get(taxon.getId()).get(0).getSousSequence().getOutilsMesure().getNom()) : null;
                            if (Strings.isNullOrEmpty(localizedOutilNameMesure)) {
                                localizedOutilNameMesure = sumBiovolumeMap.get(date).get(plateformeVO.getCode()).get(taxon.getId()).get(0).getSousSequence().getOutilsMesure() != null ? sumBiovolumeMap.get(date).get(plateformeVO.getCode())
                                        .get(taxon.getId()).get(0).getSousSequence().getOutilsMesure().getNom() : null;
                            }

                            PrintStream rawDataPrintStream = outputPrintStreamMap.get(((SiteGLACPE) sumBiovolumeMap.get(date).get(plateformeVO.getCode()).get(taxon.getId()).get(0).getSite()).getCodeFromName());
                            String line = String.format("%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s", localizedProjetName, localizedSiteName, localizedPlateformeName, DateUtil.getSimpleDateFormatDateLocale().format(date), localizedOutilNamePrelev,
                                    localizedOutilNameMesure, sumBiovolumeMap.get(date).get(plateformeVO.getCode()).get(taxon.getId()).get(0).getSousSequence().getProfondeurMin(), sumBiovolumeMap.get(date).get(plateformeVO.getCode()).get(taxon.getId())
                                    .get(0).getSousSequence().getProfondeurMax(), sumBiovolumeMap.get(date).get(plateformeVO.getCode()).get(taxon.getId()).get(0).getSousSequence().getSequence().getNomDeterminateur(), sumBiovolumeMap.get(date)
                                    .get(plateformeVO.getCode()).get(taxon.getId()).get(0).getSousSequence().getSequence().getVolumeSedimente(), sumBiovolumeMap.get(date).get(plateformeVO.getCode()).get(taxon.getId()).get(0).getSousSequence()
                                    .getSurfaceComptage(), taxon.getNomLatin());
                            rawDataPrintStream.print(line);
                            if (sommation && sumBiovolumeMap.get(date).get(plateformeVO.getCode()).get(taxon.getId()).size() > 1) {
                                Float sumBiovolume = sumBiovolume(sumBiovolumeMap.get(date).get(plateformeVO.getCode()).get(taxon.getId()));
                                for (VariableVO variableVO : selectedVariables) {
                                    rawDataPrintStream.print(";");
                                    if (variableVO.getCode().equals("biovolume_de_l_espece_dans_l_echantillon")) {
                                        rawDataPrintStream.print(String.format("%s", sumBiovolume));
                                    }
                                }
                            } else if (sumBiovolumeMap.get(date).get(plateformeVO.getCode()).get(taxon.getId()).size() == 1) {
                                Map<Long, Float> valeursMesuresPhytoplancton = buildValeurs(sumBiovolumeMap.get(date).get(plateformeVO.getCode()).get(taxon.getId()).get(0).getValeurs());
                                for (VariableVO variableVO : selectedVariables) {
                                    rawDataPrintStream.print(";");
                                    Float valeur = valeursMesuresPhytoplancton.get(variableVO.getId());
                                    if (valeur != null) {
                                        rawDataPrintStream.print(String.format("%s", valeur));
                                    } else {
                                        rawDataPrintStream.print("");
                                    }
                                }
                            } else {
                                for (@SuppressWarnings("unused") VariableVO variableVO : selectedVariables) {
                                    rawDataPrintStream.print(";");
                                }
                            }

                            rawDataPrintStream.println();
                        }
                    }
                }
            }
            //dateIt.remove();
        }

        closeStreams(outputPrintStreamMap);
        return filesMap;
    }

    private Map<Long, Float> buildValeurs(List<ValeurMesurePhytoplancton> valeurs) {
        Map<Long, Float> mapValue = new HashMap<Long, Float>();
        for (ValeurMesurePhytoplancton valeur : valeurs) {
            mapValue.put(valeur.getVariable().getId(), valeur.getValeur());
        }
        return mapValue;
    }

}
