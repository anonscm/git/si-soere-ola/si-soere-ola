package org.inra.ecoinfo.glacpe.dataset.phytoplancton.impl;

import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_DOUBLON_LINE;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_DUPLICATE_SEQUENCE;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_ERROR;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_ERROR_PLATEFORME_INVALID;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_FLOAT_VALUE_EXPECTED;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_MISSING_OUTILS_MESURE_IN_REFERENCES_DATAS;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_MISSING_PROJET_SITE_IN_REFERENCES_DATAS;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.getGLACPEMessage;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.getGLACPEMessageWithBundle;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.hibernate.exception.ConstraintViolationException;
import org.inra.ecoinfo.dataset.exception.InsertionDatabaseException;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.glacpe.dataset.impl.AbstractProcessRecord;
import org.inra.ecoinfo.glacpe.dataset.impl.CleanerValues;
import org.inra.ecoinfo.glacpe.dataset.impl.VariableValue;
import org.inra.ecoinfo.glacpe.dataset.phytoplancton.ISequencePhytoplanctonDAO;
import org.inra.ecoinfo.glacpe.dataset.phytoplancton.entity.MesurePhytoplancton;
import org.inra.ecoinfo.glacpe.dataset.phytoplancton.entity.SequencePhytoplancton;
import org.inra.ecoinfo.glacpe.dataset.phytoplancton.entity.SousSequencePhytoplancton;
import org.inra.ecoinfo.glacpe.dataset.phytoplancton.entity.ValeurMesurePhytoplancton;
import org.inra.ecoinfo.glacpe.refdata.controlecoherence.ControleCoherence;
import org.inra.ecoinfo.glacpe.refdata.controlecoherence.IControleCoherenceDAO;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.IOutilsMesureDAO;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.OutilsMesure;
import org.inra.ecoinfo.glacpe.refdata.plateforme.IPlateformeDAO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projetsite.IProjetSiteDAO;
import org.inra.ecoinfo.glacpe.refdata.projetsite.ProjetSite;
import org.inra.ecoinfo.glacpe.refdata.projetsitethemedatatype.ProjetSiteThemeDatatype;
import org.inra.ecoinfo.glacpe.refdata.taxon.ITaxonDAO;
import org.inra.ecoinfo.glacpe.refdata.taxon.Taxon;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.system.Allocator;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.DatasetDescriptorBuilder;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.ErrorsReport;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.xml.sax.SAXException;

import com.Ostermiller.util.CSVParser;
import com.google.common.collect.Lists;

/**
 *
 * @author ptcherniati
 */
public class ProcessRecord extends AbstractProcessRecord {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    protected ISequencePhytoplanctonDAO sequencePhytoplanctonDAO;

    /**
     *
     */
    protected IOutilsMesureDAO outilsMesureDAO;

    /**
     *
     */
    protected IControleCoherenceDAO controleCoherenceDAO;

    /**
     *
     */
    protected IPlateformeDAO plateformeDAO;

    /**
     *
     */
    protected ITaxonDAO taxonDAO;

    /**
     *
     */
    protected IProjetSiteDAO projetSiteDAO;

    private static final String BUNDLE_SOURCE_PATH_PHYTOPLANCTON = "org.inra.ecoinfo.glacpe.dataset.phytoplancton.messages";

    private static final String MSG_MISSING_OUTILS_PRELEVEMENT_IN_REFERENCES_DATAS = "PROPERTY_MSG_MISSING_OUTILS_PRELEVEMENT_IN_REFERENCES_DATAS";
    private static final String MSG_MISSING_TAXON_IN_REFERENCES_DATAS = "PROPERTY_MSG_MISSING_TAXON_IN_REFERENCES_DATAS";

    /**
     *
     * @return
     * @throws IOException
     * @throws SAXException
     */
    @Override
    public DatasetDescriptor holdDatasetDescriptor() throws IOException, SAXException {
        if (datasetDescriptor == null) {
            datasetDescriptor = DatasetDescriptorBuilder.buildDescriptor(ISequencePhytoplanctonDAO.class.getResource(DATASET_DESCRIPTOR_XML).openStream());
        }
        return datasetDescriptor;
    }

    @Override
    public void processRecord(CSVParser parser, VersionFile versionFile, String fileEncoding, DatasetDescriptor datasetDescriptor) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport(getGLACPEMessage(PROPERTY_MSG_ERROR));
        Utils.testNotNullArguments(getLocalizationManager(), versionFile, fileEncoding);
        Utils.testCastedArguments(versionFile, VersionFile.class, getLocalizationManager());

        try {

            String[] values = null;
            long lineCount = 1;
            int variableHeaderIndex = datasetDescriptor.getUndefinedColumn();

            Map<String, ControleCoherence> controlesCoherenceMap = controleCoherenceDAO.getControleCoherenceBySitecodeAndDatatypecode(((ProjetSiteThemeDatatype) versionFile.getDataset().getLeafNode()).getProjetSiteTheme().getProjetSite().getSite()
                    .getCode(), versionFile.getDataset().getLeafNode().getDatatype().getCode());
            List<VariableGLACPE> dbVariables = buildVariableHeaderAndSkipHeader(variableHeaderIndex, parser, errorsReport);
            Map<String, List<LineRecord>> sequencesMapLines = new HashMap<String, List<LineRecord>>();

            // On parcourt chaque ligne du fichier
            while ((values = parser.getLine()) != null) {

                CleanerValues cleanerValues = new CleanerValues(values);
                lineCount++;
                List<VariableValue> variablesValues = new LinkedList<VariableValue>();

                // On parcourt chaque colonne d'une ligne
                String projetCode = Utils.createCodeFromString(cleanerValues.nextToken());
                String nomSite = Utils.createCodeFromString(cleanerValues.nextToken());
                String plateformeCode = Utils.createCodeFromString(cleanerValues.nextToken());
                String datePrelevementString = cleanerValues.nextToken();
                Date datePrelevement = datePrelevementString.length() > 0 ? new SimpleDateFormat(datasetDescriptor.getColumns().get(cleanerValues.currentTokenIndex() - 1).getFormatType()).parse(datePrelevementString) : null;
                String profondeurMinString = cleanerValues.nextToken();
                Float profondeurMin = profondeurMinString.length() > 0 ? Float.parseFloat(profondeurMinString) : 0.0f;
                String profondeurMaxString = cleanerValues.nextToken();
                Float profondeurMax = profondeurMaxString.length() > 0 ? Float.parseFloat(profondeurMaxString) : 0.0f;
                String outilPrelevementCode = Utils.createCodeFromString(cleanerValues.nextToken());
                String outilMesureCode = Utils.createCodeFromString(cleanerValues.nextToken());
                String determinateur = cleanerValues.nextToken();
                String volumeSedimenteString = cleanerValues.nextToken();
                Float volumeSedimente = volumeSedimenteString.length() > 0 ? Float.parseFloat(volumeSedimenteString) : null;
                String surfaceComptageString = cleanerValues.nextToken();
                Float surfaceComptage = surfaceComptageString.length() > 0 ? Float.parseFloat(surfaceComptageString) : null;
                String nomTaxon = Utils.createCodeFromString(cleanerValues.nextToken());

                for (int actualVariableArray = variableHeaderIndex; actualVariableArray < values.length; actualVariableArray++) {
                    String value = values[actualVariableArray].replaceAll(" ", "");
                    try {
                        VariableGLACPE variable = dbVariables.get(actualVariableArray - variableHeaderIndex);
                        if (controlesCoherenceMap.get(variable.getCode()) != null)
                            testValueCoherence(Float.valueOf(value), controlesCoherenceMap.get(variable.getCode()).getValeurMin(), controlesCoherenceMap.get(variable.getCode()).getValeurMax(), lineCount, 11);
                        variablesValues.add(new VariableValue(dbVariables.get(actualVariableArray - variableHeaderIndex), values[actualVariableArray].replaceAll(" ", "")));
                    } catch (NumberFormatException e) {
                        errorsReport.addException(new BadExpectedValueException(String.format(getGLACPEMessage(PROPERTY_MSG_FLOAT_VALUE_EXPECTED), lineCount, 11, value)));
                    } catch (BadExpectedValueException e) {
                        errorsReport.addException(e);
                    }
                }

                LineRecord line = new LineRecord(nomSite, plateformeCode, datePrelevement, profondeurMin, profondeurMax, outilMesureCode, outilPrelevementCode, determinateur, volumeSedimente, surfaceComptage, nomTaxon, variablesValues, lineCount,
                        projetCode);
                String sequenceKey = projetCode.concat(nomSite).concat(datePrelevementString);
                fillLinesMap(sequencesMapLines, line, sequenceKey);

            }
            List<String> listErrors = new LinkedList<String>();
            Allocator allocator = Allocator.getInstance();

            int count = 0;
            allocator.allocate("publish", versionFile.getFileSize());
            Iterator<Entry<String, List<LineRecord>>> iterator = sequencesMapLines.entrySet().iterator();
            while (iterator.hasNext()) {

                final Entry<String, List<LineRecord>> entry = iterator.next();
                List<LineRecord> sequenceLines = sequencesMapLines.get(entry.getKey());
                if (!sequenceLines.isEmpty()) {
                    try {
                        Date datePrelevement = sequenceLines.get(0).getDatePrelevement();
                        String projetCode = sequenceLines.get(0).getProjetCode();
                        String siteCode = Utils.createCodeFromString(sequenceLines.get(0).getNomSite());
                        buildSequence(datePrelevement, projetCode, siteCode, sequenceLines, versionFile, errorsReport, listErrors);
                        logger.debug(String.format("%d - %s", count++, datePrelevement));

                        // Très important à cause de problèmes de performances
                        sequencePhytoplanctonDAO.flush();
                        versionFile = (VersionFile) versionFileDAO.merge(versionFile);

                    } catch (InsertionDatabaseException e) {
                        errorsReport.addException(e);
                    }
                }
                iterator.remove();
            }

            if (errorsReport.hasErrors()) {
                throw new PersistenceException(errorsReport.buildHTMLMessages());
            }

        } catch (Exception e) {

            throw new BusinessException(e);
        }
    }

    private void fillLinesMap(Map<String, List<LineRecord>> linesMap, LineRecord line, String keyLine) {
        List<LineRecord> sousSequenceLines = null;
        if (keyLine != null && keyLine.trim().length() > 0) {
            if (linesMap.containsKey(keyLine)) {
                linesMap.get(keyLine).add(line);
            } else {
                sousSequenceLines = new LinkedList<LineRecord>();
                sousSequenceLines.add(line);
                linesMap.put(keyLine, sousSequenceLines);
            }
        }
    }

    private List<VariableGLACPE> buildVariableHeaderAndSkipHeader(int variableHeaderIndex, CSVParser parser, ErrorsReport errorsReport) throws PersistenceException, IOException {
        List<String> variablesHeaders = skipHeaderAndRetrieveVariableName(variableHeaderIndex, parser);
        List<VariableGLACPE> dbVariables = new LinkedList<VariableGLACPE>();
        for (String variableHeader : variablesHeaders) {
            VariableGLACPE variable = (VariableGLACPE) variableDAO.getByCode(Utils.createCodeFromString(variableHeader));
            if (variable == null) {
                errorsReport.addException(new PersistenceException(String.format(getGLACPEMessage(PROPERTY_MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS), variableHeader)));
            }

            dbVariables.add(variable);
        }
        if (errorsReport.hasErrors()) {
            throw new PersistenceException(errorsReport.buildHTMLMessages());
        }
        return dbVariables;
    }

    private void buildSequence(Date datePrelevement, String projetCode, String siteCode, List<LineRecord> sequenceLines, VersionFile versionFile, ErrorsReport errorsReport, List<String> listErrors) throws PersistenceException,
            InsertionDatabaseException
    {

        LineRecord firstLine = sequenceLines.get(0);

        SequencePhytoplancton sequencePhytoplancton = sequencePhytoplanctonDAO.getByDatePrelevementAndProjetCodeAndSiteCode(datePrelevement, projetCode, siteCode);

        if (sequencePhytoplancton == null) {
            ProjetSite projetSite = projetSiteDAO.getBySiteCodeAndProjetCode(siteCode, projetCode);

            if (projetSite == null) {
                errorsReport.addException(new PersistenceException(String.format(getGLACPEMessage(PROPERTY_MSG_MISSING_PROJET_SITE_IN_REFERENCES_DATAS), projetCode)));
                throw new PersistenceException(String.format(getGLACPEMessage(PROPERTY_MSG_MISSING_PROJET_SITE_IN_REFERENCES_DATAS), projetCode));
            }

            sequencePhytoplancton = new SequencePhytoplancton();

            sequencePhytoplancton.setProjetSite(projetSite);
            sequencePhytoplancton.setDatePrelevement(datePrelevement);
            sequencePhytoplancton.setNomDeterminateur(firstLine.getDeterminateur());
            sequencePhytoplancton.setVolumeSedimente(firstLine.getVolumeSedimente());

            sequencePhytoplancton.setVersionFile(versionFile);
        }

        Map<String, List<LineRecord>> sousSequencesMap = new HashMap<String, List<LineRecord>>();

        for (LineRecord line : sequenceLines) {
            fillLinesMap(sousSequencesMap, line, line.getPlateformeCode());
        }

        for (String sousSequenceKey : sousSequencesMap.keySet()) {
            try {
                List<LineRecord> projetLines = sousSequencesMap.get(sousSequenceKey);
                buildSousSequence(sousSequenceKey, projetLines.get(0).getProfondeurMin(), projetLines.get(0).getProfondeurMax(), projetLines.get(0).getSurfaceComptage(), projetLines.get(0).getOutilsMesureCode(), projetLines.get(0)
                        .getOutilsPrelevementCode(), projetLines, sequencePhytoplancton, errorsReport, listErrors);

            } catch (InsertionDatabaseException e) {
                errorsReport.addException(e);
            }
        }
        if (!errorsReport.hasErrors()) {
            try {
                sequencePhytoplanctonDAO.saveOrUpdate(sequencePhytoplancton);
            } catch (ConstraintViolationException e) {
                String message = String.format(getGLACPEMessage(PROPERTY_MSG_DUPLICATE_SEQUENCE), DateUtil.getSimpleDateFormatDateyyyyMMddUTC().format(datePrelevement));
                errorsReport.addException(new BusinessException(message));
            }
        }
    }

    private void buildSousSequence(String plateformeCode, Float profondeurMin, Float profondeurMax, Float surfaceComptage, String outilMesureCode, String outilPrelevementCode, List<LineRecord> sousSequenceLines,
            SequencePhytoplancton sequencePhytoplancton, ErrorsReport errorsReport, List<String> listErrors) throws PersistenceException, InsertionDatabaseException
    {

        LineRecord firstLine = sousSequenceLines.get(0);

        Plateforme plateforme = null;
        plateforme = plateformeDAO.getByNKey(plateformeCode, Utils.createCodeFromString(firstLine.getNomSite()));
        if (plateforme == null) {
            InsertionDatabaseException insertionDatabaseException = new InsertionDatabaseException(String.format(getGLACPEMessage(PROPERTY_MSG_ERROR_PLATEFORME_INVALID), plateformeCode, firstLine.getOriginalLineNumber()));
            throw insertionDatabaseException;
        }

        OutilsMesure outilsMesure = null;
        if (outilMesureCode != "") {
            outilsMesure = outilsMesureDAO.getByCode(outilMesureCode);
            if (outilsMesure == null || !outilsMesure.getTypeOutilsMesure().getType().getValeur().equals("mesure")) {
                errorsReport.addException(new PersistenceException(String.format(getGLACPEMessage(PROPERTY_MSG_MISSING_OUTILS_MESURE_IN_REFERENCES_DATAS), outilMesureCode)));
                throw new PersistenceException(String.format(getGLACPEMessage(PROPERTY_MSG_MISSING_OUTILS_MESURE_IN_REFERENCES_DATAS), outilMesureCode));
            }
        }

        OutilsMesure outilsPrelevement = null;
        if (outilPrelevementCode != "") {
            outilsPrelevement = outilsMesureDAO.getByCode(outilPrelevementCode);
            if (outilsPrelevement == null || !outilsPrelevement.getTypeOutilsMesure().getType().getValeur().equals("prélèvement")) {
                errorsReport.addException(new PersistenceException(String.format(getGLACPEMessageWithBundle(BUNDLE_SOURCE_PATH_PHYTOPLANCTON, MSG_MISSING_OUTILS_PRELEVEMENT_IN_REFERENCES_DATAS), outilPrelevementCode)));
                throw new PersistenceException(String.format(getGLACPEMessageWithBundle(BUNDLE_SOURCE_PATH_PHYTOPLANCTON, MSG_MISSING_OUTILS_PRELEVEMENT_IN_REFERENCES_DATAS), outilPrelevementCode));
            }
        }

        SousSequencePhytoplancton sousSequencePhytoplancton = new SousSequencePhytoplancton();

        sousSequencePhytoplancton.setPlateforme(plateforme);
        sousSequencePhytoplancton.setOutilsPrelevement(outilsPrelevement);
        sousSequencePhytoplancton.setOutilsMesure(outilsMesure);
        sousSequencePhytoplancton.setProfondeurMin(profondeurMin);
        sousSequencePhytoplancton.setProfondeurMax(profondeurMax);
        sousSequencePhytoplancton.setSurfaceComptage(surfaceComptage);
        sousSequencePhytoplancton.setSequence(sequencePhytoplancton);
        sequencePhytoplancton.getSousSequences().add(sousSequencePhytoplancton);

        Map<String, List<LineRecord>> taxonsLinesMap = new HashMap<String, List<LineRecord>>();

        List<String> taxonName = Lists.newArrayList();
        for (LineRecord line : sousSequenceLines) {
            if (!taxonName.contains(line.getTaxonCode())) {
                taxonName.add(line.getTaxonCode());
            } else {
                errorsReport.addException(new PersistenceException(String.format(getGLACPEMessage(PROPERTY_MSG_DOUBLON_LINE), line.getOriginalLineNumber())));
            }
            fillLinesMap(taxonsLinesMap, line, line.getTaxonCode());
        }

        for (String taxon : taxonsLinesMap.keySet()) {
            try {
                List<LineRecord> taxonLines = taxonsLinesMap.get(taxon);
                buildMesure(taxon, taxonLines, sousSequencePhytoplancton, errorsReport, listErrors);
            } catch (InsertionDatabaseException e) {
                errorsReport.addException(e);
            }
        }
    }

    private void buildMesure(String taxonCode, List<LineRecord> taxonLines, SousSequencePhytoplancton sousSequencePhytoplancton, ErrorsReport errorsReport, List<String> listErrors) throws PersistenceException, InsertionDatabaseException {
        MesurePhytoplancton mesurePhytoplancton = null;
        Taxon taxon = taxonDAO.getByCode(taxonCode);

        try {

            if (taxon == null) {
                if (!listErrors.contains(taxonCode)) {
                    listErrors.add(taxonCode);
                }
                errorsReport.addException(new BusinessException(String.format(getGLACPEMessageWithBundle(BUNDLE_SOURCE_PATH_PHYTOPLANCTON, MSG_MISSING_TAXON_IN_REFERENCES_DATAS), taxonCode)));
            } else {
                mesurePhytoplancton = new MesurePhytoplancton();
                mesurePhytoplancton.setSousSequence(sousSequencePhytoplancton);
                mesurePhytoplancton.setTaxon(taxon);
                sousSequencePhytoplancton.getMesures().add(mesurePhytoplancton);
                mesurePhytoplancton.setLigneFichierEchange(taxonLines.get(0).getOriginalLineNumber());

                for (LineRecord taxonLine : taxonLines) {
                    for (VariableValue variableValue : taxonLine.getVariablesValues()) {

                        ValeurMesurePhytoplancton valeurPhytoplancton = new ValeurMesurePhytoplancton();
                        VariableGLACPE variable = (VariableGLACPE) variableDAO.merge(variableValue.getVariableGLACPE());

                        valeurPhytoplancton.setVariable(variable);

                        if (variableValue.getValue() == null || variableValue.getValue().length() == 0) {
                            valeurPhytoplancton.setValeur(null);
                        } else {
                            Float value = Float.parseFloat(variableValue.getValue());
                            valeurPhytoplancton.setValeur(value);
                        }

                        valeurPhytoplancton.setMesure(mesurePhytoplancton);
                        mesurePhytoplancton.getValeurs().add(valeurPhytoplancton);
                    }
                }
            }
        } catch (Exception e) {
            errorsReport.addException(e);
        }
    }

    /**
     *
     * @param sequencePhytoplanctonDAO
     */
    public void setSequencePhytoplanctonDAO(ISequencePhytoplanctonDAO sequencePhytoplanctonDAO) {
        this.sequencePhytoplanctonDAO = sequencePhytoplanctonDAO;
    }

    /**
     *
     * @param outilsMesureDAO
     */
    public void setOutilsMesureDAO(IOutilsMesureDAO outilsMesureDAO) {
        this.outilsMesureDAO = outilsMesureDAO;
    }

    /**
     *
     * @param controleCoherenceDAO
     */
    public void setControleCoherenceDAO(IControleCoherenceDAO controleCoherenceDAO) {
        this.controleCoherenceDAO = controleCoherenceDAO;
    }

    /**
     *
     * @param plateformeDAO
     */
    public void setPlateformeDAO(IPlateformeDAO plateformeDAO) {
        this.plateformeDAO = plateformeDAO;
    }

    /**
     *
     * @param taxonDAO
     */
    public void setTaxonDAO(ITaxonDAO taxonDAO) {
        this.taxonDAO = taxonDAO;
    }

    /**
     *
     * @param projetSiteDAO
     */
    public void setProjetSiteDAO(IProjetSiteDAO projetSiteDAO) {
        this.projetSiteDAO = projetSiteDAO;
    }
}
