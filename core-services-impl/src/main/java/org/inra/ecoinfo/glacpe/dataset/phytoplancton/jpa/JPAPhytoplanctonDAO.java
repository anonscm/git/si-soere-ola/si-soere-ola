package org.inra.ecoinfo.glacpe.dataset.phytoplancton.jpa;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.glacpe.dataset.phytoplancton.IPhytoplanctonDAO;
import org.inra.ecoinfo.glacpe.dataset.phytoplancton.entity.MesurePhytoplancton;
import org.inra.ecoinfo.glacpe.dataset.phytoplancton.entity.ValeurMesurePhytoplancton;
import org.inra.ecoinfo.glacpe.extraction.IDatatypeSpecifigProjectStatisticsDAO;
import org.inra.ecoinfo.glacpe.extraction.jpa.AbstractJPAProjectSpecificDatatypeStatisticsDAO;
import org.inra.ecoinfo.glacpe.extraction.vo.AbstractDatesFormParam;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesRequestParamVO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.projetsite.ProjetSite;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class JPAPhytoplanctonDAO extends AbstractJPADAO<Object> implements IPhytoplanctonDAO {

    private static final String REQUEST_PHYTO_AVAILABLES_PROJETS = "select distinct s.projetSite.projet from SequencePhytoplancton s";
    private static final String REQUEST_PHYTO_AVAILABLES_PLATEFORMES_BY_PROJETSITE_ID = "select distinct ss.plateforme from SousSequencePhytoplancton ss where ss.sequence.projetSite.id = :projetSiteId";
    private static final String REQUEST_PHYTO_AVAILABLES_VARIABLES_BY_PLATEFORMES_ID = "select distinct vm.variable from ValeurMesurePhytoplancton vm where vm.mesure.sousSequence.plateforme.id in (:plateformesIds) and vm.mesure.sousSequence.sequence.datePrelevement between :firstDate and :lastDate and vm.valeur is not null order by vm.variable.ordreAffichageGroupe";
    private static final String REQUEST_PHYTO_PROJETSITE_BY_PROJET_ID_AND_SITE_ID = "select distinct s.projetSite from SequencePhytoplancton s where s.projetSite.projet.id=:projetId and s.projetSite.site.id=:siteId ";

    private static final String REQUEST_Extraction = "select distinct m, m.sousSequence.sequence.projetSite.projet.nom,m.sousSequence.plateforme.site.nom,m.sousSequence.plateforme.nom,m.sousSequence.sequence.datePrelevement from MesurePhytoplancton m where m.sousSequence.plateforme.id in (:selectedPlateformesIds) and m.sousSequence.sequence.projetSite.id in (:selectedProjetId) and m.taxon.id in (:selectedTaxonsIds) %s  order by m.sousSequence.sequence.projetSite.projet.nom,m.sousSequence.plateforme.site.nom,m.sousSequence.plateforme.nom,m.sousSequence.sequence.datePrelevement";

    private static final String HQL_DATE_FIELD_1 = "vm.mesure.sousSequence.sequence.datePrelevement";
    private static final String HQL_DATE_FIELD_2 = "m.sousSequence.sequence.datePrelevement";

    private IDatatypeSpecifigProjectStatisticsDAO datatypeStatisticsDAO;

    /**
     *
     * @return
     * @throws PersistenceException
     */
    @Override
    public List<Projet> retrievePhytoplanctonAvailablesProjets() throws PersistenceException {
        try {

            Query query = entityManager.createQuery(REQUEST_PHYTO_AVAILABLES_PROJETS);
            @SuppressWarnings("unchecked")
            List<Projet> projetsSite = query.getResultList();

            return projetsSite;
        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param plateformesIds
     * @param firstDate
     * @param endDate
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<VariableGLACPE> retrievePhytoplanctonAvailablesVariables(List<Long> plateformesIds, Date firstDate, Date endDate) throws PersistenceException {

        List<VariableGLACPE> variables;
        try {
            Query query = entityManager.createQuery(REQUEST_PHYTO_AVAILABLES_VARIABLES_BY_PLATEFORMES_ID);

            query.setParameter("plateformesIds", plateformesIds);
            query.setParameter("firstDate", firstDate);
            query.setParameter("lastDate", endDate);
            variables = query.getResultList();
            return variables;
        } catch (Exception e) {
            AbstractJPADAO.LOGGER.error(e);
            throw new PersistenceException(e);
        }

    }

    /**
     *
     * @param selectedPlateformesIds
     * @param projetId
     * @param datesRequestParamVO
     * @param selectedTaxonsIds
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("rawtypes")
    @Override
    public List<MesurePhytoplancton> extractDatas(List<Long> selectedPlateformesIds, List<Long> projetId, DatesRequestParamVO datesRequestParamVO, List<Long> selectedTaxonsIds) throws PersistenceException {
        try {
            AbstractDatesFormParam datesFormParam = datesRequestParamVO.getCurrentDatesFormParam();
            Query query = entityManager.createQuery(String.format(REQUEST_Extraction, datesFormParam.buildSQLCondition(HQL_DATE_FIELD_2)));
            Map<String, Date> datesMap = datesFormParam.retrieveParametersMap();

            for (String key : datesMap.keySet()) {
                query.setParameter(key, datesMap.get(key));
            }

            query.setParameter("selectedPlateformesIds", selectedPlateformesIds);
            query.setParameter("selectedProjetId", projetId);
            query.setParameter("selectedTaxonsIds", selectedTaxonsIds);

            List extractedDatas = query.getResultList();
            List<MesurePhytoplancton> extractedPhyto = new LinkedList<MesurePhytoplancton>();
            for (Object object : extractedDatas) {
                Object[] queryObject = (Object[]) object;
                MesurePhytoplancton mesurePhyto = (MesurePhytoplancton) queryObject[0];
                extractedPhyto.add(mesurePhyto);
            }
            return extractedPhyto;

        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param projetSiteId
     * @return
     * @throws PersistenceException
     */
    @Override
    public List<Plateforme> retrievePhytoplanctonAvailablesPlateformesByProjetSiteId(long projetSiteId) throws PersistenceException {
        try {

            Query query = entityManager.createQuery(REQUEST_PHYTO_AVAILABLES_PLATEFORMES_BY_PROJETSITE_ID);

            query.setParameter("projetSiteId", projetSiteId);
            @SuppressWarnings("unchecked")
            List<Plateforme> plateformes = query.getResultList();

            return plateformes;
        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param projetId
     * @param siteId
     * @return
     * @throws PersistenceException
     */
    @Override
    public ProjetSite retrievePhytoplanctonAvailablesProjetsSiteByProjetAndSite(long projetId, long siteId) throws PersistenceException {
        try {

            Query query = entityManager.createQuery(REQUEST_PHYTO_PROJETSITE_BY_PROJET_ID_AND_SITE_ID);

            query.setParameter("projetId", projetId);
            query.setParameter("siteId", siteId);
            @SuppressWarnings("unchecked")
            List<ProjetSite> projetSite = query.getResultList();

            return projetSite.get(0);
        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param datatypeStatisticsDAO
     */
    public void setDatatypeStatisticsDAO(IDatatypeSpecifigProjectStatisticsDAO datatypeStatisticsDAO) {
        this.datatypeStatisticsDAO = datatypeStatisticsDAO;
    }

    /**
     *
     * @param selectedPlateformesIds
     * @param selectedVariablesIds
     * @param projetId
     * @param datesRequestParamVO
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<ValeurMesurePhytoplancton> extractAggregatedDatas(List<Long> selectedPlateformesIds, List<Long> selectedVariablesIds, List<Long> projetId, DatesRequestParamVO datesRequestParamVO) throws PersistenceException {
        return (List<ValeurMesurePhytoplancton>) datatypeStatisticsDAO.extractAggregatedDatasForAllDepth(selectedPlateformesIds, selectedVariablesIds, projetId, datesRequestParamVO, HQL_DATE_FIELD_1);
    }

    /**
     *
     */
    public static class JPADatatypeStatisticsDAO extends AbstractJPAProjectSpecificDatatypeStatisticsDAO {

        private static final String REQUEST_AllDepthAggregatedDatas = "select vm from ValeurMesurePhytoplancton vm where vm.mesure.sousSequence.plateforme.id in (:selectedPlateformesIds) and vm.mesure.sousSequence.sequence.projetSite.id in (:selectedProjetId) and vm.variable.id in (:selectedVariablesIds) %s  order by vm.mesure.sousSequence.sequence.projetSite.projet.nom,vm.mesure.sousSequence.plateforme.site.nom,vm.mesure.sousSequence.plateforme.nom,vm.mesure.sousSequence.sequence.datePrelevement";

        /**
         *
         * @return
         */
        @Override
        protected String getQLRequestAggregatedDatasForAllDepth() {
            return REQUEST_AllDepthAggregatedDatas;
        }

        /**
         *
         * @return
         */
        @Override
        protected String getQLRequestAggregatedDatasForRangeDepth() {
            return null;
        }

        @Override
        protected String getQLRequestDatasForAllDepth() {
            return null;
        }

        /**
         *
         * @return
         */
        @Override
        protected String getRequestDatasForRangeDepth() {
            return null;
        }
    }
}
