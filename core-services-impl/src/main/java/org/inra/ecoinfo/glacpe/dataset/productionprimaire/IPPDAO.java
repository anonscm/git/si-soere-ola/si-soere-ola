package org.inra.ecoinfo.glacpe.dataset.productionprimaire;

import java.util.Date;
import java.util.List;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.glacpe.dataset.productionprimaire.entity.MesurePP;
import org.inra.ecoinfo.glacpe.dataset.productionprimaire.entity.ValeurMesurePP;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesRequestParamVO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public interface IPPDAO extends IDAO<Object> {

    /**
     *
     * @return
     * @throws PersistenceException
     */
    List<Site> retrieveAvailablesSites() throws PersistenceException;

    /**
     *
     * @return
     * @throws PersistenceException
     */
    List<Projet> retrieveAvailablesProjets() throws PersistenceException;

    /**
     *
     * @param id
     * @return
     * @throws PersistenceException
     */
    List<Plateforme> retrieveAvailablesPlateformesByProjetSiteId(Long id) throws PersistenceException;

    /**
     *
     * @param plateformesIds
     * @param firstDate
     * @param endDate
     * @return
     * @throws PersistenceException
     */
    List<VariableGLACPE> retrieveAvailablesVariables(List<Long> plateformesIds, Date firstDate, Date endDate) throws PersistenceException;

    /**
     *
     * @param plateformesIds
     * @param selectedProjetSiteIds
     * @param datesRequestParamVO
     * @return
     * @throws PersistenceException
     */
    List<MesurePP> extractDatasForAllDepth(List<Long> plateformesIds, List<Long> selectedProjetSiteIds, DatesRequestParamVO datesRequestParamVO) throws PersistenceException;

    /**
     *
     * @param plateformesIds
     * @param selectedProjetSiteIds
     * @param datesRequestParamVO
     * @param dephtMin
     * @param dephtMax
     * @return
     * @throws PersistenceException
     */
    List<MesurePP> extractDatasForRangeDepth(List<Long> plateformesIds, List<Long> selectedProjetSiteIds, DatesRequestParamVO datesRequestParamVO, Float dephtMin, Float dephtMax) throws PersistenceException;

    /**
     *
     * @param plateformesIds
     * @param selectedProjetSiteIds
     * @param datesRequestParamVO
     * @param variablesId
     * @return
     * @throws PersistenceException
     */
    List<ValeurMesurePP> extractAggregatedDatasForAllDepth(List<Long> plateformesIds, List<Long> selectedProjetSiteIds, DatesRequestParamVO datesRequestParamVO, List<Long> variablesId) throws PersistenceException;

    /**
     *
     * @param plateformesIds
     * @param selectedProjetSiteIds
     * @param datesRequestParamVO
     * @param variablesId
     * @param dephtMin
     * @param dephtMax
     * @return
     * @throws PersistenceException
     */
    List<ValeurMesurePP> extractAggregatedDatasForRangeDepth(List<Long> plateformesIds, List<Long> selectedProjetSiteIds, DatesRequestParamVO datesRequestParamVO, List<Long> variablesId, Float dephtMin, Float dephtMax) throws PersistenceException;
}
