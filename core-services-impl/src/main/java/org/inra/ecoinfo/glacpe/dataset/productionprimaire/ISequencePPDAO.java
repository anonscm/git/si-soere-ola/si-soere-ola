package org.inra.ecoinfo.glacpe.dataset.productionprimaire;

import java.util.Date;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.glacpe.dataset.productionprimaire.entity.SequencePP;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public interface ISequencePPDAO extends IDAO<SequencePP> {

    /**
     *
     * @param date
     * @param projetCode
     * @param siteCode
     * @return
     * @throws PersistenceException
     */
    SequencePP getByDatePrelevementAndProjetCodeAndSiteCode(Date date, String projetCode, String siteCode) throws PersistenceException;

}
