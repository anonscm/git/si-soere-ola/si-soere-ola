package org.inra.ecoinfo.glacpe.dataset.productionprimaire.impl;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.inra.ecoinfo.extraction.config.impl.Extraction;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.AbstractExtractor;
import org.inra.ecoinfo.glacpe.dataset.productionprimaire.IPPDAO;
import org.inra.ecoinfo.glacpe.dataset.productionprimaire.entity.MesurePP;
import org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.impl.PPChloroTranspParameter;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.DepthRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO;
import org.inra.ecoinfo.glacpe.extraction.vo.VariableVO;
import org.inra.ecoinfo.glacpe.refdata.projetsite.ProjetSite;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public abstract class AbstractPPRawsDatasExtractor extends AbstractExtractor {

    /**
     *
     */
    protected static final String MAP_INDEX_0 = "0";
    //private static final String PROPERTY_MSG_NO_RESULT_EXTRACT = "PROPERTY_MSG_NO_RESULT_EXTRACT";
    private static final String PROPERTY_MSG_NO_RESULT_EXTRACT = "The data may not exist for the selected period - Il est possible que les données soit inexistantes pour la période séléctionnée";
  
    /**
     *
     */
    protected IPPDAO productionPrimaireDAO;

    /**
     *
     */
    protected IVariableDAO variableDAO;

    /**
     *
     * @param productionPrimaireDAO
     */
    public void setProductionPrimaireDAO(IPPDAO productionPrimaireDAO) {
        this.productionPrimaireDAO = productionPrimaireDAO;
    }

    /**
     *
     * @param variableDAO
     */
    public void setVariableDAO(IVariableDAO variableDAO) {
        this.variableDAO = variableDAO;
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, List> extractDatas(Map<String, Object> requestMetadatas) throws NoExtractionResultException, BusinessException {

        Map<String, List> extractedDatasMap = new HashMap<String, List>();
        DepthRequestParamVO depthRequestParamVO = (DepthRequestParamVO) requestMetadatas.get(DepthRequestParamVO.class.getSimpleName());
        List<PlateformeVO> selectedPlateformes = (List<PlateformeVO>) requestMetadatas.get(PlateformeVO.class.getSimpleName());
        List<Long> selectedProjetSiteIds = (List<Long>) requestMetadatas.get(ProjetSite.class.getSimpleName());
        List<Long> plateformesIds = buildPlateformesIds(selectedPlateformes);

        DatesRequestParamVO datesRequestParamVO = (DatesRequestParamVO) requestMetadatas.get(DatesRequestParamVO.class.getSimpleName());

        List<MesurePP> mesuresPP = null;
        try {
            if (depthRequestParamVO.getAllDepth()) {
                mesuresPP = (List<MesurePP>) productionPrimaireDAO.extractDatasForAllDepth(plateformesIds, selectedProjetSiteIds, datesRequestParamVO);
            } else {
                mesuresPP = (List<MesurePP>) productionPrimaireDAO.extractDatasForRangeDepth(plateformesIds, selectedProjetSiteIds, datesRequestParamVO, depthRequestParamVO.getDepthMin(), depthRequestParamVO.getDepthMax());
            }

            if (mesuresPP == null || mesuresPP.size() == 0) {
                throw new NoExtractionResultException(getLocalizationManager().getMessage(NoExtractionResultException.BUNDLE_SOURCE_PATH, PROPERTY_MSG_NO_RESULT_EXTRACT));
            }

            extractedDatasMap.put(PPRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE, mesuresPP);
        } catch (PersistenceException e) {
            AbstractExtractor.LOGGER.error(e.getMessage());
            throw new BusinessException(e);
        }
        return extractedDatasMap;
    }

    private List<Long> buildPlateformesIds(List<PlateformeVO> selectedPlateformes) {
        List<Long> plateformesIds = new LinkedList<Long>();
        for (PlateformeVO plateforme : selectedPlateformes) {
            plateformesIds.add(plateforme.getId());
        }
        return plateformesIds;
    }

    @Override
    protected void prepareRequestMetadatas(Map<String, Object> requestMetadatasMap) throws BusinessException {
        sortSelectedVariables(requestMetadatasMap);
    }

    @SuppressWarnings("unchecked")
    private void sortSelectedVariables(Map<String, Object> requestMetadatasMap) {
        Collections.sort(((Map<String, List<VariableVO>>) requestMetadatasMap.get(VariableVO.class.getSimpleName())).get(PPChloroTranspParameter.PRODUCTION_PRIMAIRE), new Comparator<VariableVO>() {

            @Override
            public int compare(VariableVO o1, VariableVO o2) {
                if (o1.getOrdreAffichageGroupe() == null) {
                    o1.setOrdreAffichageGroupe(0);
                }
                if (o2.getOrdreAffichageGroupe() == null) {
                    o2.setOrdreAffichageGroupe(0);
                }
                return o1.getOrdreAffichageGroupe().toString().concat(o1.getCode()).compareTo(o2.getOrdreAffichageGroupe().toString().concat(o2.getCode()));
            }
        });

    }

    /**
     *
     * @param selectedPlateformes
     * @return
     */
    protected Set<String> retrieveSitesCodes(List<PlateformeVO> selectedPlateformes) {
        Set<String> sitesNames = new HashSet<String>();
        for (PlateformeVO plateforme : selectedPlateformes) {
            sitesNames.add(Utils.createCodeFromString(plateforme.getNomSite()));
        }
        return sitesNames;
    }

    @Override
    public void setExtraction(Extraction extraction) {
        // TODO Auto-generated method stub

    }

    @Override
    public Extraction getExtraction() {
        // TODO Auto-generated method stub
        return null;
    }
}
