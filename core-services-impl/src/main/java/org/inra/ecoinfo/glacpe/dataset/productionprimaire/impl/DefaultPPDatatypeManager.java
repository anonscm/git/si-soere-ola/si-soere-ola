package org.inra.ecoinfo.glacpe.dataset.productionprimaire.impl;

import java.util.Date;
import java.util.List;

import org.inra.ecoinfo.MO;
import org.inra.ecoinfo.glacpe.dataset.productionprimaire.IPPDAO;
import org.inra.ecoinfo.glacpe.dataset.productionprimaire.IPPDatatypeManager;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ptcherniati
 */
public class DefaultPPDatatypeManager extends MO implements IPPDatatypeManager {

    /**
     *
     */
    protected IPPDAO productionPrimaireDAO;

    /**
     *
     */
    protected IVariableDAO variableDAO;

    /**
     *
     * @param variableDAO
     */
    public void setVariableDAO(IVariableDAO variableDAO) {
        this.variableDAO = variableDAO;
    }

    /**
     *
     * @param productionPrimaireDAO
     */
    public void setProductionPrimaireDAO(IPPDAO productionPrimaireDAO) {
        this.productionPrimaireDAO = productionPrimaireDAO;
    }

    /**
     *
     * @return
     * @throws BusinessException
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public List<Site> retrievePPAvailablesSites() throws BusinessException {
        try {
            return productionPrimaireDAO.retrieveAvailablesSites();
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    /**
     *
     * @param plateformesIds
     * @param firstDate
     * @param endDate
     * @return
     * @throws BusinessException
     */
    @Override
    public List<VariableGLACPE> retrievePPAvailablesVariables(List<Long> plateformesIds, Date firstDate, Date endDate) throws BusinessException {
        try {
            return productionPrimaireDAO.retrieveAvailablesVariables(plateformesIds, firstDate, endDate);
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    /**
     *
     * @return
     * @throws BusinessException
     */
    @Override
    public List<Projet> retrievePPAvailablesProjets() throws BusinessException {
        try {
            return productionPrimaireDAO.retrieveAvailablesProjets();
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    /**
     *
     * @param id
     * @return
     * @throws BusinessException
     */
    @Override
    public List<Plateforme> retrievePPAvailablesPlateformesByProjetSiteId(Long id) throws BusinessException {
        try {
            return productionPrimaireDAO.retrieveAvailablesPlateformesByProjetSiteId(id);
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }

}
