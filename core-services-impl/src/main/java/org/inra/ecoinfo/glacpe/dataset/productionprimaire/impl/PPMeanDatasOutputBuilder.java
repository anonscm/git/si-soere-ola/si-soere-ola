package org.inra.ecoinfo.glacpe.dataset.productionprimaire.impl;

import java.io.File;
import java.io.PrintStream;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.glacpe.dataset.IGLACPEAggregateData;
import org.inra.ecoinfo.glacpe.dataset.VariableAggregatedDatas;
import org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEAggregatedDatasOutputBuilder;
import org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.impl.PPChloroTranspParameter;
import org.inra.ecoinfo.glacpe.extraction.vo.DepthRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO;
import org.inra.ecoinfo.glacpe.extraction.vo.ProjetSiteVO;
import org.inra.ecoinfo.glacpe.extraction.vo.VariableVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.IDatatypeVariableUniteGLACPEDAO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import com.google.common.base.Strings;

/**
 *
 * @author ptcherniati
 */
public class PPMeanDatasOutputBuilder extends AbstractGLACPEAggregatedDatasOutputBuilder {

    private static final String BUNDLE_SOURCE_PATH_PP = "org.inra.ecoinfo.glacpe.dataset.productionprimaire.messages";

    /**
     *
     */
    protected static final String SUFFIX_FILENAME_AGGREGATED = "mean";
    private static final String HEADER_RAW_DATA = "PROPERTY_MSG_HEADER_MEAN";
    private static final String MSG_MEAN = "PROPERTY_MSG_MEAN";

    private static final String CODE_DATATYPE_PP = "production_primaire";

    /**
     *
     */
    protected IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO;

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException, NoExtractionResultException {
        ((DefaultParameter) parameters).getFilesMaps().add(super.buildOutput(parameters, PPAggregatedDatasExtractor.CST_RESULT_EXTRACTION_CODE));
        return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatas) throws BusinessException {
        Properties propertiesVariableName = localizationManager.newProperties(Variable.NAME_ENTITY_JPA, "nom");
        Map<String, List<VariableVO>> selectedVariablesList = (Map<String, List<VariableVO>>) requestMetadatas.get(VariableVO.class.getSimpleName());

        List<VariableVO> selectedVariables = (List<VariableVO>) selectedVariablesList.get(PPChloroTranspParameter.PRODUCTION_PRIMAIRE);

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format("%s", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_PP, HEADER_RAW_DATA)));
        for (VariableVO variableVO : selectedVariables) {
            String uniteNom = "nounit";
            try {
                Unite unite = datatypeVariableUniteDAO.getUnite(CODE_DATATYPE_PP, variableVO.getCode());

                if (unite != null) {
                    uniteNom = unite.getCode();
                }
            } catch (PersistenceException e) {
                throw new BusinessException(e);
            }

            String localizedVariableName = propertiesVariableName.getProperty(variableVO.getNom());
            if (Strings.isNullOrEmpty(localizedVariableName))
                localizedVariableName = variableVO.getNom();

            stringBuilder.append(String.format(";\"%s(%s)%n%s\"", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_PP, MSG_MEAN), localizedVariableName, uniteNom));

        }
        return stringBuilder.toString();
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {
        Map<String, List<VariableVO>> selectedVariablesList = (Map<String, List<VariableVO>>) requestMetadatasMap.get(VariableVO.class.getSimpleName());

        List<VariableVO> selectedVariables = (List<VariableVO>) selectedVariablesList.get(PPChloroTranspParameter.PRODUCTION_PRIMAIRE);
        DepthRequestParamVO depthRequestParamVO = (DepthRequestParamVO) requestMetadatasMap.get(DepthRequestParamVO.class.getSimpleName());

        String depthMin = depthRequestParamVO.getAllDepth() ? "" : depthRequestParamVO.getDepthMin().toString();
        String depthMax = depthRequestParamVO.getAllDepth() ? "" : depthRequestParamVO.getDepthMax().toString();

        List<IGLACPEAggregateData> valeursMesures = resultsDatasMap.get(PPAggregatedDatasExtractor.CST_RESULT_EXTRACTION_CODE);

        Properties propertiesProjetName = localizationManager.newProperties(Projet.NAME_TABLE, "nom");
        Properties propertiesPlateformeName = localizationManager.newProperties(Plateforme.TABLE_NAME, "nom");
        Properties propertiesSiteName = localizationManager.newProperties(Site.NAME_ENTITY_JPA, "nom");

        List<PlateformeVO> selectedPlateformes = (List<PlateformeVO>) requestMetadatasMap.get(PlateformeVO.class.getSimpleName());
        ProjetSiteVO selectedProjet = selectedPlateformes.get(0).getProjet();
        Set<String> sitesNames = retrieveSitesCodes(selectedPlateformes);
        Map<String, File> filesMap = buildOutputsFiles(sitesNames, SUFFIX_FILENAME_AGGREGATED);
        Map<String, PrintStream> outputPrintStreamMap = buildOutputPrintStreamMap(filesMap);
        for (String siteName : outputPrintStreamMap.keySet()) {
            outputPrintStreamMap.get(siteName).println(headers);
        }

        Map<String, Map<Date, Map<String, VariableAggregatedDatas>>> variablesAggregatedDatas = buildVariablesMeanDatas(valeursMesures);

        for (String siteCode : variablesAggregatedDatas.keySet()) {
            List<Date> dateSet = asSortedList(variablesAggregatedDatas.get(siteCode).keySet());

            for (Date date : dateSet) {
                VariableAggregatedDatas variableAggregatedDatas = null;
                for (VariableVO variable : selectedVariables) {
                    variableAggregatedDatas = variablesAggregatedDatas.get(siteCode).get(date).get(variable.getCode());
                    if (variableAggregatedDatas != null)
                        break;
                }

                PrintStream rawDataPrintStream = outputPrintStreamMap.get(Utils.createCodeFromString(variableAggregatedDatas.getSiteName()));

                String localizedProjetName = propertiesProjetName.getProperty(selectedProjet.getNom());
                if (Strings.isNullOrEmpty(localizedProjetName))
                    localizedProjetName = selectedProjet.getNom();

                String localizedSiteName = propertiesSiteName.getProperty(variableAggregatedDatas.getSiteName());
                if (Strings.isNullOrEmpty(localizedSiteName))
                    localizedSiteName = variableAggregatedDatas.getSiteName();

                String localizedPlateformeName = propertiesPlateformeName.getProperty(variableAggregatedDatas.getPlateformeName());
                if (Strings.isNullOrEmpty(localizedPlateformeName))
                    localizedPlateformeName = variableAggregatedDatas.getPlateformeName();

                rawDataPrintStream.print(String.format("%s;%s;%s;", localizedProjetName, localizedSiteName, localizedPlateformeName));
                rawDataPrintStream.print(String.format("%s;%s;%s", DateUtil.getSimpleDateFormatDateLocale().format(date), depthMin, depthMax));

                for (VariableVO variable : selectedVariables) {
                    if (variablesAggregatedDatas.get(siteCode).get(date).get(variable.getCode()) == null) {
                        rawDataPrintStream.print(";");
                    } else {
                        rawDataPrintStream.print(String.format(";%s", variablesAggregatedDatas.get(siteCode).get(date).get(variable.getCode()).getAverageAggregatedData().getValuesByDatesMap().get(date)));
                    }
                }
                rawDataPrintStream.println();
                variablesAggregatedDatas.get(siteCode).get(date).remove(Utils.createCodeFromString(variableAggregatedDatas.getVariableName()));
                if (variablesAggregatedDatas.get(siteCode).get(date).isEmpty()) {
                    variablesAggregatedDatas.get(siteCode).remove(date);
                }
            }
            if (variablesAggregatedDatas.get(siteCode).isEmpty()) {
                variablesAggregatedDatas.remove(siteCode);
            }
        }
        closeStreams(outputPrintStreamMap);
        return filesMap;
    }

    /**
     * 
     * @param valeursMesures
     * @return
     */
    protected Map<String, Map<Date, Map<String, VariableAggregatedDatas>>> buildVariablesMeanDatas(List<IGLACPEAggregateData> valeursMesures) {
        Map<Site, Map<Plateforme, Map<Date, Map<VariableGLACPE, List<IGLACPEAggregateData>>>>> valeursMesuresReorderedByDatesVariablesAndDates = buildValeursMesuresReorderedByDatesAndVariables(valeursMesures);
        Map<String, Map<Date, Map<String, VariableAggregatedDatas>>> variablesAggregatedDatas = new HashMap<String, Map<Date, Map<String, VariableAggregatedDatas>>>();

        for (Site site : valeursMesuresReorderedByDatesVariablesAndDates.keySet()) {
            for (Plateforme plateforme : valeursMesuresReorderedByDatesVariablesAndDates.get(site).keySet()) {
                if (variablesAggregatedDatas.get(plateforme.getCode()) == null) {
                    Map<Date, Map<String, VariableAggregatedDatas>> variableAggregatedDatasBySite = new HashMap<Date, Map<String, VariableAggregatedDatas>>();
                    variablesAggregatedDatas.put(plateforme.getCode(), variableAggregatedDatasBySite);
                }
                Map<Date, Map<VariableGLACPE, List<IGLACPEAggregateData>>> valeursMesuresReorderedByVariablesAndDates = valeursMesuresReorderedByDatesVariablesAndDates.get(site).get(plateforme);
                for (Date dateKey : valeursMesuresReorderedByVariablesAndDates.keySet()) {
                    Map<VariableGLACPE, List<IGLACPEAggregateData>> valeursMesuresDateMap = valeursMesuresReorderedByVariablesAndDates.get(dateKey);
                    if (variablesAggregatedDatas.get(plateforme.getCode()).get(dateKey) == null) {
                        Map<String, VariableAggregatedDatas> mapVariableAggregated = new HashMap<String, VariableAggregatedDatas>();
                        variablesAggregatedDatas.get(plateforme.getCode()).put(dateKey, mapVariableAggregated);
                    }
                    for (VariableGLACPE variableKey : valeursMesuresDateMap.keySet()) {
                        VariableAggregatedDatas variableAggregatedDatas = new VariableAggregatedDatas(site.getNom(), plateforme.getNom(), variableKey.getNom(), variableKey.getOrdreAffichageGroupe());
                        List<IGLACPEAggregateData> valeursMesureDates = valeursMesuresDateMap.get(variableKey);
                        fillVariableAggregatedDatas(variableAggregatedDatas, dateKey, valeursMesureDates);
                        variablesAggregatedDatas.get(plateforme.getCode()).get(dateKey).put(variableKey.getCode(), variableAggregatedDatas);
                    }
                }
            }
        }

        return variablesAggregatedDatas;
    }

    /**
     *
     * @param variableAggregatedDatas
     * @param dateKey
     * @param valeursMesureDates
     */
    protected void fillVariableAggregatedDatas(VariableAggregatedDatas variableAggregatedDatas, Date dateKey, List<IGLACPEAggregateData> valeursMesureDates) {

        Float m = 0f;
        Float intermediateAverage = 0f;
        Float meanDepth = (valeursMesureDates.get(valeursMesureDates.size() - 1).getDepth() - valeursMesureDates.get(0).getDepth());

        variableAggregatedDatas.setOutilsPrelevement(valeursMesureDates.get(0).getOutilsPrelevement());

        if (valeursMesureDates.size() == 1) {
            if (valeursMesureDates.get(0).getValue() != null) {
                intermediateAverage = valeursMesureDates.get(0).getValue();
            } else {
                intermediateAverage = 0.0f;
            }
        } else {
            Iterator<IGLACPEAggregateData> iterator = valeursMesureDates.iterator();
            IGLACPEAggregateData valeurMesure = iterator.next();
            Float value = 0.0f;
            Float depth = 0.0f;
            while (iterator.hasNext()) {
                if (variableAggregatedDatas.getOutilsPrelevement() == null && valeurMesure.getOutilsPrelevement() != null) {
                    variableAggregatedDatas.setOutilsPrelevement(valeurMesure.getOutilsPrelevement());
                }
                if (variableAggregatedDatas.getOutilsMesure() == null && valeurMesure.getOutilsMesure() != null) {
                    variableAggregatedDatas.setOutilsMesure(valeurMesure.getOutilsMesure());
                }
                if (valeurMesure.getValue() != null) {
                    value = valeurMesure.getValue();
                    depth = valeurMesure.getDepth();
                }
                valeurMesure = iterator.next();

                if (value != null && valeurMesure.getValue() != null) {
                    Float indexValue = valeurMesure.getValue();
                    Float indexDepth = valeurMesure.getDepth();
                    m += ((value + indexValue) * (indexDepth - depth)) / 2;
                }
            }

            intermediateAverage = m / meanDepth;
        }
        variableAggregatedDatas.getAverageAggregatedData().getValuesByDatesMap().put(dateKey, intermediateAverage);
    }

    /**
     *
     * @param valeursMesures
     * @return
     */
    protected Map<Site, Map<Plateforme, Map<Date, Map<VariableGLACPE, List<IGLACPEAggregateData>>>>> buildValeursMesuresReorderedByDatesAndVariables(List<IGLACPEAggregateData> valeursMesures) {
        Map<Site, Map<Plateforme, Map<Date, Map<VariableGLACPE, List<IGLACPEAggregateData>>>>> valeursMesuresReorderedBySitesPlateformesVariablesDates = new HashMap<Site, Map<Plateforme, Map<Date, Map<VariableGLACPE, List<IGLACPEAggregateData>>>>>();

        for (IGLACPEAggregateData valeurMesure : valeursMesures) {

            Map<Plateforme, Map<Date, Map<VariableGLACPE, List<IGLACPEAggregateData>>>> valeursMesuresReorderedByPlateformesVariablesAndDates = valeursMesuresReorderedBySitesPlateformesVariablesDates.get(valeurMesure.getSite());

            if (valeursMesuresReorderedByPlateformesVariablesAndDates == null) {
                valeursMesuresReorderedByPlateformesVariablesAndDates = new HashMap<Plateforme, Map<Date, Map<VariableGLACPE, List<IGLACPEAggregateData>>>>();
            }

            Map<Date, Map<VariableGLACPE, List<IGLACPEAggregateData>>> valeursMesuresReorderedByVariablesAndDates = valeursMesuresReorderedByPlateformesVariablesAndDates.get(valeurMesure.getPlateforme());

            if (valeursMesuresReorderedByVariablesAndDates == null) {
                valeursMesuresReorderedByVariablesAndDates = new HashMap<Date, Map<VariableGLACPE, List<IGLACPEAggregateData>>>();
            }

            Map<VariableGLACPE, List<IGLACPEAggregateData>> dateVmMap = valeursMesuresReorderedByVariablesAndDates.get(valeurMesure.getDate());
            if (dateVmMap == null) {
                dateVmMap = new HashMap<VariableGLACPE, List<IGLACPEAggregateData>>();
            }

            List<IGLACPEAggregateData> valeursMesuresAggregated = dateVmMap.get(valeurMesure.getVariable());
            if (valeursMesuresAggregated == null) {
                valeursMesuresAggregated = new LinkedList<IGLACPEAggregateData>();
            }

            valeursMesuresAggregated.add(valeurMesure);
            dateVmMap.put(valeurMesure.getVariable(), valeursMesuresAggregated);
            valeursMesuresReorderedByVariablesAndDates.put(valeurMesure.getDate(), dateVmMap);
            valeursMesuresReorderedByPlateformesVariablesAndDates.put(valeurMesure.getPlateforme(), valeursMesuresReorderedByVariablesAndDates);
            valeursMesuresReorderedBySitesPlateformesVariablesDates.put(valeurMesure.getSite(), valeursMesuresReorderedByPlateformesVariablesAndDates);

        }
        return valeursMesuresReorderedBySitesPlateformesVariablesDates;
    }

    /**
     *
     * @param <T>
     * @param c
     * @return
     */
    public static <T extends Comparable<? super T>> List<T> asSortedList(Collection<T> c) {
        List<T> list = new LinkedList<T>(c);
        Collections.sort(list);
        return list;
    }


    /**
     *
     * @param datatypeVariableUniteDAO
     */
    public void setDatatypeVariableUniteDAO(IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO) {
        this.datatypeVariableUniteDAO = datatypeVariableUniteDAO;
    }
}
