package org.inra.ecoinfo.glacpe.dataset.productionprimaire.impl;

import java.io.File;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.glacpe.dataset.productionprimaire.entity.MesurePP;
import org.inra.ecoinfo.glacpe.dataset.productionprimaire.entity.ValeurMesurePP;
import org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.impl.AbstractPPChloroTranspRawsDatasOutputBuilder;
import org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.impl.PPChloroTranspParameter;
import org.inra.ecoinfo.glacpe.extraction.vo.DepthRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO;
import org.inra.ecoinfo.glacpe.extraction.vo.VariableVO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

import com.google.common.base.Strings;

/**
 *
 * @author ptcherniati
 */
public class PPRawsDatasOutputBuilderByLine extends AbstractPPChloroTranspRawsDatasOutputBuilder {

    private static final String BUNDLE_SOURCE_PATH_PP = "org.inra.ecoinfo.glacpe.dataset.productionprimaire.messages";

    private static final String HEADER_RAW_DATA = "PROPERTY_MSG_HEADER_RAW_DATA_ROW";
    private static final String HEADER_RAW_DATA_ALLDEPTH = "PROPERTY_MSG_HEADER_RAW_DATA_ALLDEPTH_ROW";

    private static final String CODE_DATATYPE_PRODUCTION_PRIMAIRE = "production_primaire";
    private static final String SUFFIX_FILENAME = CODE_DATATYPE_PRODUCTION_PRIMAIRE;

    @Override
    protected String buildHeader(Map<String, Object> requestMetadatas) throws BusinessException {

        DepthRequestParamVO depthRequestParamVO = (DepthRequestParamVO) requestMetadatas.get(DepthRequestParamVO.class.getSimpleName());

        StringBuilder stringBuilder = new StringBuilder();
        try {
            if (depthRequestParamVO.getAllDepth()) {
                stringBuilder.append(String.format("%s", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_PP, HEADER_RAW_DATA_ALLDEPTH)));
            } else {
                stringBuilder.append(String.format("%s", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_PP, HEADER_RAW_DATA)));
            }

        } catch (Exception e) {
            throw new BusinessException(e);
        }
        return stringBuilder.toString();

    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {

        Map<String, List<VariableVO>> selectedVariablesList = (Map<String, List<VariableVO>>) requestMetadatasMap.get(VariableVO.class.getSimpleName());

        List<VariableVO> selectedVariables = (List<VariableVO>) selectedVariablesList.get(PPChloroTranspParameter.PRODUCTION_PRIMAIRE);
        DepthRequestParamVO depthRequestParamVO = (DepthRequestParamVO) requestMetadatasMap.get(DepthRequestParamVO.class.getSimpleName());
        Iterator<MesurePP> mesuresPP = resultsDatasMap.get(PPRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE).iterator();

        List<PlateformeVO> selectedPlateformes = (List<PlateformeVO>) requestMetadatasMap.get(PlateformeVO.class.getSimpleName());
        Set<String> sitesNames = retrieveSitesCodes(selectedPlateformes);
        Map<String, File> filesMap = buildOutputsFiles(sitesNames, SUFFIX_FILENAME);
        Map<String, PrintStream> outputPrintStreamMap = buildOutputPrintStreamMap(filesMap);
        for (String siteName : outputPrintStreamMap.keySet()) {
            outputPrintStreamMap.get(siteName).println(headers);
        }

        try {
            DataType datatypePP = datatypeDAO.getByCode(CODE_DATATYPE_PRODUCTION_PRIMAIRE);

            Properties propertiesProjetName = localizationManager.newProperties(Projet.NAME_TABLE, "nom");
            Properties propertiesPlateformeName = localizationManager.newProperties(Plateforme.TABLE_NAME, "nom");
            Properties propertiesSiteName = localizationManager.newProperties(Site.NAME_ENTITY_JPA, "nom");
            Properties propertiesVariableName = localizationManager.newProperties(Variable.NAME_ENTITY_JPA, "nom");

            while (mesuresPP.hasNext()) {
                final MesurePP mesurePP = mesuresPP.next();
                PrintStream rawDataPrintStream = outputPrintStreamMap.get(((SiteGLACPE) mesurePP.getSousSequencePP().getPlateforme().getSite()).getCodeFromName());

                String localizedProjetName = propertiesProjetName.getProperty(mesurePP.getSousSequencePP().getSequencePP().getProjetSite().getProjet().getNom());
                if (Strings.isNullOrEmpty(localizedProjetName))
                    localizedProjetName = mesurePP.getSousSequencePP().getSequencePP().getProjetSite().getProjet().getNom();

                String localizedSiteName = propertiesSiteName.getProperty(mesurePP.getSousSequencePP().getPlateforme().getSite().getNom());
                if (Strings.isNullOrEmpty(localizedSiteName))
                    localizedSiteName = mesurePP.getSousSequencePP().getPlateforme().getSite().getNom();

                String localizedPlateformeName = propertiesPlateformeName.getProperty(mesurePP.getSousSequencePP().getPlateforme().getNom());
                if (Strings.isNullOrEmpty(localizedPlateformeName))
                    localizedPlateformeName = mesurePP.getSousSequencePP().getPlateforme().getNom();
                // nom projet;nom site;nom plateforme;date de prélèvement;profondeur;debut d'incubation;fin d'incubation;durée d'incubation
                Map<Long, ValeurMesurePP> valeursMesuresPP = buildValeurs(mesurePP.getValeurs());

                for (VariableVO variableVO : selectedVariables) {
                    ValeurMesurePP valeurMesurePP = valeursMesuresPP.get(variableVO.getId());
                    if (valeurMesurePP != null && valeurMesurePP.getValue() != null) {
                        String localizedVariableName = propertiesVariableName.getProperty(variableVO.getNom());
                        if (Strings.isNullOrEmpty(localizedVariableName))
                            localizedVariableName = variableVO.getNom();
                        if (depthRequestParamVO.getAllDepth()) {
                            rawDataPrintStream.println(String.format("%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s", localizedProjetName, localizedSiteName, localizedPlateformeName,
                                    DateUtil.getSimpleDateFormatDateLocale().format(mesurePP.getSousSequencePP().getSequencePP().getDate()), mesurePP.getProfondeur(), mesurePP.getSousSequencePP().getHeureDebutIncubation() != null ? DateUtil
                                            .getSimpleDateFormatTimeLocale().format(mesurePP.getSousSequencePP().getHeureDebutIncubation()) : "", mesurePP.getSousSequencePP().getHeureFinIncubation() != null ? DateUtil.getSimpleDateFormatTimeLocale()
                                            .format(mesurePP.getSousSequencePP().getHeureFinIncubation()) : "",
                                    mesurePP.getSousSequencePP().getDureeIncubation() != null ? DateUtil.getSimpleDateFormatTimeLocale().format(mesurePP.getSousSequencePP().getDureeIncubation()) : "", localizedVariableName, valeurMesurePP
                                            .getValeur(), valeurMesurePP.getVariable().getUnite(datatypePP).getNom()));
                        } else {
                            rawDataPrintStream.println(String.format("%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s", localizedProjetName, localizedSiteName, localizedPlateformeName, depthRequestParamVO.getDepthMin(), depthRequestParamVO.getDepthMax(),
                                    DateUtil.getSimpleDateFormatDateLocale().format(mesurePP.getSousSequencePP().getSequencePP().getDate()), mesurePP.getProfondeur(), mesurePP.getSousSequencePP().getHeureDebutIncubation() != null ? DateUtil
                                            .getSimpleDateFormatTimeLocale().format(mesurePP.getSousSequencePP().getHeureDebutIncubation()) : "", mesurePP.getSousSequencePP().getHeureFinIncubation() != null ? DateUtil.getSimpleDateFormatTimeLocale()
                                            .format(mesurePP.getSousSequencePP().getHeureFinIncubation()) : "",
                                    mesurePP.getSousSequencePP().getDureeIncubation() != null ? DateUtil.getSimpleDateFormatTimeLocale().format(mesurePP.getSousSequencePP().getDureeIncubation()) : "", localizedVariableName, valeurMesurePP
                                            .getValeur(), valeurMesurePP.getVariable().getUnite(datatypePP).getNom()));
                        }
                    }
                }
                // commenter ?
                //mesuresPP.remove();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        closeStreams(outputPrintStreamMap);
        return filesMap;
    }

    private Map<Long, ValeurMesurePP> buildValeurs(List<ValeurMesurePP> valeurs) {
        Map<Long, ValeurMesurePP> mapValue = new HashMap<Long, ValeurMesurePP>();
        for (ValeurMesurePP valeur : valeurs) {
            mapValue.put(valeur.getVariable().getId(), valeur);
        }
        return mapValue;
    }

    class ErrorsReport {

        private String errorsMessages = new String();

        public void addErrorMessage(String errorMessage) {
            errorsMessages = errorsMessages.concat("-").concat(errorMessage).concat("\n");
        }

        public String getErrorsMessages() {
            return errorsMessages;
        }

        public boolean hasErrors() {
            return (errorsMessages.length() > 0);
        }
    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException, NoExtractionResultException {
        if (((DefaultParameter) parameters).getResults().get(PPRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE).get(PPRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE) == null
                || ((DefaultParameter) parameters).getResults().get(PPRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE).get(PPRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE).isEmpty())
            return null;
        ((DefaultParameter) parameters).getFilesMaps().add(super.buildOutput(parameters, PPRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE));

        return null;
    }
}
