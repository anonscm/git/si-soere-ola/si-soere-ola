package org.inra.ecoinfo.glacpe.dataset.productionprimaire.impl;

import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.CST_SPACE;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.CST_STRING_EMPTY;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_DOUBLON_LINE;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_DUPLICATE_SEQUENCE;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_ERROR;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_ERROR_PLATEFORME_INVALID;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_ERROR_PLATEFORME_SITE_MISMATCH;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_FLOAT_VALUE_EXPECTED;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_INVALID_DATE;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_INVALID_DEPTH;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_INVALID_TIME;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_MISSING_PROJET_SITE_IN_REFERENCES_DATAS;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.getGLACPEMessage;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.hibernate.exception.ConstraintViolationException;
import org.inra.ecoinfo.dataset.exception.InsertionDatabaseException;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.glacpe.dataset.impl.AbstractProcessRecord;
import org.inra.ecoinfo.glacpe.dataset.impl.CleanerValues;
import org.inra.ecoinfo.glacpe.dataset.impl.VariableValue;
import org.inra.ecoinfo.glacpe.dataset.productionprimaire.ISequencePPDAO;
import org.inra.ecoinfo.glacpe.dataset.productionprimaire.entity.MesurePP;
import org.inra.ecoinfo.glacpe.dataset.productionprimaire.entity.SequencePP;
import org.inra.ecoinfo.glacpe.dataset.productionprimaire.entity.SousSequencePP;
import org.inra.ecoinfo.glacpe.dataset.productionprimaire.entity.ValeurMesurePP;
import org.inra.ecoinfo.glacpe.refdata.controlecoherence.ControleCoherence;
import org.inra.ecoinfo.glacpe.refdata.controlecoherence.IControleCoherenceDAO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.IPlateformeDAO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projetsite.IProjetSiteDAO;
import org.inra.ecoinfo.glacpe.refdata.projetsite.ProjetSite;
import org.inra.ecoinfo.glacpe.refdata.projetsitethemedatatype.ProjetSiteThemeDatatype;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.refdata.site.ISiteDAO;
import org.inra.ecoinfo.system.Allocator;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.DatasetDescriptorBuilder;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.ErrorsReport;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.xml.sax.SAXException;

import com.Ostermiller.util.CSVParser;

/**
 *
 * @author ptcherniati
 */
public class ProcessRecord extends AbstractProcessRecord {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    protected ISequencePPDAO sequencePPDAO;

    /**
     *
     */
    protected IProjetSiteDAO projetSiteDAO;

    /**
     *
     */
    protected IPlateformeDAO plateformeDAO;

    /**
     *
     */
    protected IControleCoherenceDAO controleCoherenceDAO;

    /**
     *
     */
    protected ISiteDAO siteDAO;

    /**
     *
     * @return
     * @throws IOException
     * @throws SAXException
     */
    @Override
    public DatasetDescriptor holdDatasetDescriptor() throws IOException, SAXException {
        if (datasetDescriptor == null) {
            datasetDescriptor = DatasetDescriptorBuilder.buildDescriptor(ISequencePPDAO.class.getResource(DATASET_DESCRIPTOR_XML).openStream());
        }
        return datasetDescriptor;
    }

    @Override
    public void processRecord(CSVParser parser, VersionFile versionFile, String fileEncoding, DatasetDescriptor datasetDescriptor) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport(getGLACPEMessage(PROPERTY_MSG_ERROR));
        Utils.testNotNullArguments(getLocalizationManager(), versionFile, fileEncoding);
        Utils.testCastedArguments(versionFile, VersionFile.class, getLocalizationManager());

        try {

            String[] values = null;
            long lineCount = 1;
            int variableHeaderIndex = datasetDescriptor.getUndefinedColumn();
            Map<String, List<LineRecord>> sequencesMapLines = new HashMap<String, List<LineRecord>>();
            List<VariableGLACPE> dbVariables = buildVariableHeaderAndSkipHeader(variableHeaderIndex, parser, errorsReport);
            Map<String, ControleCoherence> controlesCoherenceMap = controleCoherenceDAO.getControleCoherenceBySitecodeAndDatatypecode(((ProjetSiteThemeDatatype) versionFile.getDataset().getLeafNode()).getProjetSiteTheme().getProjetSite().getSite()
                    .getCode(), versionFile.getDataset().getLeafNode().getDatatype().getCode());

            // On parcourt chaque ligne du fichier
            while ((values = parser.getLine()) != null) {

                lineCount++;
                List<VariableValue> variablesValues = new LinkedList<VariableValue>();
                CleanerValues cleanerValues = new CleanerValues(values);

                // On parcourt chaque colonne d'une ligne
                String projetCode = Utils.createCodeFromString(cleanerValues.nextToken());
                String nomSite = cleanerValues.nextToken();
                String plateformeCode = Utils.createCodeFromString(cleanerValues.nextToken());

                String datePrelevementString = cleanerValues.nextToken();
                Date date;
                try {
                    date = new SimpleDateFormat(datasetDescriptor.getColumns().get(cleanerValues.currentTokenIndex() - 1).getFormatType()).parse(datePrelevementString);
                } catch (Exception e) {
                    date = null;
                    errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(PROPERTY_MSG_INVALID_DATE), datePrelevementString, lineCount, 4,
                            (datasetDescriptor.getColumns().get(cleanerValues.currentTokenIndex() - 1).getFormatType()))));
                }
                Float profondeur = null;
                String profondeurString = cleanerValues.nextToken();
                try {
                    profondeur = Float.parseFloat(profondeurString);
                } catch (Exception e) {
                    errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(PROPERTY_MSG_INVALID_DEPTH), profondeurString, lineCount, 5)));
                }

                String debutIncubationString = cleanerValues.nextToken();
                Date debutIncubation = null;
                if (debutIncubationString != null && !debutIncubationString.isEmpty()) {
                    try {
                        debutIncubation = new SimpleDateFormat(datasetDescriptor.getColumns().get(cleanerValues.currentTokenIndex() - 1).getFormatType()).parse(debutIncubationString);
                    } catch (Exception e) {
                        errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(PROPERTY_MSG_INVALID_TIME), debutIncubationString, lineCount, 6,
                                (datasetDescriptor.getColumns().get(cleanerValues.currentTokenIndex() - 1).getFormatType()))));
                    }
                }

                String finIncubationString = cleanerValues.nextToken();
                Date finIncubation = null;
                if (finIncubationString != null && !finIncubationString.isEmpty()) {
                    try {
                        finIncubation = new SimpleDateFormat(datasetDescriptor.getColumns().get(cleanerValues.currentTokenIndex() - 1).getFormatType()).parse(finIncubationString);
                    } catch (Exception e) {
                        errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(PROPERTY_MSG_INVALID_TIME), finIncubationString, lineCount, 7,
                                (datasetDescriptor.getColumns().get(cleanerValues.currentTokenIndex() - 1).getFormatType()))));
                    }
                }

                String dureeIncubationString = cleanerValues.nextToken();
                Date dureeIncubation = null;
                if (dureeIncubationString != null && !dureeIncubationString.isEmpty()) {
                    try {
                        dureeIncubation = new SimpleDateFormat(datasetDescriptor.getColumns().get(cleanerValues.currentTokenIndex() - 1).getFormatType()).parse(dureeIncubationString);
                    } catch (Exception e) {
                        errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(PROPERTY_MSG_INVALID_TIME), dureeIncubationString, lineCount, 8,
                                (datasetDescriptor.getColumns().get(cleanerValues.currentTokenIndex() - 1).getFormatType()))));
                    }
                }

                for (int actualVariableArray = variableHeaderIndex; actualVariableArray < values.length; actualVariableArray++) {
                    String valeur = values[actualVariableArray].replaceAll(CST_SPACE, CST_STRING_EMPTY);
                    try {
                        VariableGLACPE variable = dbVariables.get(actualVariableArray - variableHeaderIndex);
                        if (controlesCoherenceMap.get(variable.getCode()) != null)
                            testValueCoherence(Float.valueOf(valeur), controlesCoherenceMap.get(variable.getCode()).getValeurMin(), controlesCoherenceMap.get(variable.getCode()).getValeurMax(), lineCount, 11);
                        variablesValues.add(new VariableValue(dbVariables.get(actualVariableArray - variableHeaderIndex), valeur));

                    } catch (BadExpectedValueException e) {
                        errorsReport.addException(new BusinessException(e));
                    } catch (Exception e) {
                        errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(PROPERTY_MSG_FLOAT_VALUE_EXPECTED), lineCount, actualVariableArray + 1, valeur)));
                    }
                }
                if (errorsReport.hasErrors()) {
                    throw new PersistenceException(errorsReport.buildHTMLMessages());
                }

                LineRecord line = new LineRecord(projetCode, nomSite, plateformeCode, date, profondeur, debutIncubation, finIncubation, dureeIncubation, variablesValues, lineCount);
                String sequenceKey = projetCode.concat(nomSite).concat(datePrelevementString);
                fillLinesMap(sequencesMapLines, line, sequenceKey);
            }

            Allocator allocator = Allocator.getInstance();

            int count = 0;
            allocator.allocate("publish", versionFile.getFileSize());
            Iterator<Entry<String, List<LineRecord>>> iterator = sequencesMapLines.entrySet().iterator();
            while (iterator.hasNext()) {

                final Entry<String, List<LineRecord>> entry = iterator.next();
                List<LineRecord> sequenceLines = sequencesMapLines.get(entry.getKey());
                if (!sequenceLines.isEmpty()) {
                    try {
                        Date datePrelevement = sequenceLines.get(0).getDate();
                        String projetCode = sequenceLines.get(0).getProjetCode();
                        String siteCode = Utils.createCodeFromString(sequenceLines.get(0).getNomSite());
                        buildSequence(datePrelevement, projetCode, siteCode, sequenceLines, versionFile, errorsReport);
                        logger.debug(String.format("%d - %s", count++, datePrelevement));

                        // Très important à cause de problèmes de performances
                        sequencePPDAO.flush();
                        versionFile = (VersionFile) versionFileDAO.merge(versionFile);
                    } catch (InsertionDatabaseException e) {
                        errorsReport.addException(e);
                    }
                }
                iterator.remove();
            }

            if (errorsReport.hasErrors())
                throw new BusinessException(errorsReport.buildHTMLMessages());

        } catch (Exception e) {

            throw new BusinessException(e);
        }
    }

    private List<VariableGLACPE> buildVariableHeaderAndSkipHeader(int variableHeaderIndex, CSVParser parser, ErrorsReport errorsReport) throws PersistenceException, IOException {
        List<String> variablesHeaders = skipHeaderAndRetrieveVariableName(variableHeaderIndex, parser);
        List<VariableGLACPE> dbVariables = new LinkedList<VariableGLACPE>();
        for (String variableHeader : variablesHeaders) {
            VariableGLACPE variable = (VariableGLACPE) variableDAO.getByCode(variableHeader);
            if (variable == null) {
                errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(PROPERTY_MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS), variableHeader)));
            }

            dbVariables.add(variable);
        }
        if (errorsReport.hasErrors()) {
            throw new PersistenceException(errorsReport.buildHTMLMessages());
        }
        return dbVariables;
    }

    private void fillLinesMap(Map<String, List<LineRecord>> linesMap, LineRecord line, String keyLine) {
        List<LineRecord> sousSequenceLines = null;
        if (keyLine != null && keyLine.trim().length() > 0) {
            if (linesMap.containsKey(keyLine)) {
                linesMap.get(keyLine).add(line);
            } else {
                sousSequenceLines = new LinkedList<LineRecord>();
                sousSequenceLines.add(line);
                linesMap.put(keyLine, sousSequenceLines);
            }
        }
    }

    private void buildSequence(Date datePrelevement, String projetCode, String siteCode, List<LineRecord> sequenceLines, VersionFile versionFile, ErrorsReport errorsReport) throws PersistenceException, InsertionDatabaseException {

        LineRecord firstLine = sequenceLines.get(0);

        SequencePP sequencePP = sequencePPDAO.getByDatePrelevementAndProjetCodeAndSiteCode(datePrelevement, projetCode, siteCode);

        if (sequencePP == null) {

            ProjetSite projetSite = projetSiteDAO.getBySiteCodeAndProjetCode(siteCode, projetCode);
            if (projetSite == null) {
                errorsReport.addException(new PersistenceException(String.format(getGLACPEMessage(PROPERTY_MSG_MISSING_PROJET_SITE_IN_REFERENCES_DATAS), projetCode, siteCode)));
                throw new PersistenceException(String.format(getGLACPEMessage(PROPERTY_MSG_MISSING_PROJET_SITE_IN_REFERENCES_DATAS), projetCode, siteCode));
            }

            if (!siteCode.equals(Utils.createCodeFromString(firstLine.getNomSite()))) {
                InsertionDatabaseException insertionDatabaseException = new InsertionDatabaseException(String.format(getGLACPEMessage(PROPERTY_MSG_ERROR_PLATEFORME_SITE_MISMATCH), firstLine.getPlateformeCode(), firstLine.getOriginalLineNumber(),
                        siteCode));
                throw insertionDatabaseException;
            }

            sequencePP = new SequencePP();

            sequencePP.setProjetSite(projetSite);
            sequencePP.setDate(datePrelevement);

            sequencePP.setVersionFile(versionFile);
        }

        Map<String, List<LineRecord>> sousSequencesMap = new HashMap<String, List<LineRecord>>();

        for (LineRecord line : sequenceLines) {
            fillLinesMap(sousSequencesMap, line, line.getPlateformeCode());
        }

        for (String sousSequenceKey : sousSequencesMap.keySet()) {
            try {
                List<LineRecord> projetLines = sousSequencesMap.get(sousSequenceKey);
                Date debutIncubation = projetLines.get(0).getDebutIncubation();
                Date finIncubation = projetLines.get(0).getFinIncubation();
                Date dureeIncubation = projetLines.get(0).getDureeIncubation();
                buildSousSequence(sousSequenceKey, projetLines, debutIncubation, finIncubation, dureeIncubation, sequencePP, errorsReport);

            } catch (InsertionDatabaseException e) {
                errorsReport.addException(e);
            }
        }
        if (!errorsReport.hasErrors()) {
            try {
                sequencePPDAO.saveOrUpdate(sequencePP);
            } catch (ConstraintViolationException e) {
                String message = String.format(getGLACPEMessage(PROPERTY_MSG_DUPLICATE_SEQUENCE), DateUtil.getSimpleDateFormatDateyyyyMMddUTC().format(datePrelevement));
                errorsReport.addException(new BusinessException(message));
            }
        }
    }

    private void buildSousSequence(String plateformeCode, List<LineRecord> sousSequenceLines, Date debutIncubation, Date finIncubation, Date dureeIncubation, SequencePP sequencePP, ErrorsReport errorsReport) throws PersistenceException,
            InsertionDatabaseException
    {

        LineRecord firstLine = sousSequenceLines.get(0);

        Plateforme plateforme = null;
        plateforme = plateformeDAO.getByNKey(plateformeCode, Utils.createCodeFromString(firstLine.getNomSite()));
        if (plateforme == null) {
            InsertionDatabaseException insertionDatabaseException = new InsertionDatabaseException(String.format(getGLACPEMessage(PROPERTY_MSG_ERROR_PLATEFORME_INVALID), plateformeCode, firstLine.getOriginalLineNumber()));
            throw insertionDatabaseException;
        }

        SousSequencePP sousSequencePP = new SousSequencePP();
        sousSequencePP.setPlateforme(plateforme);
        sousSequencePP.setHeureDebutIncubation(debutIncubation);
        sousSequencePP.setHeureFinIncubation(finIncubation);
        sousSequencePP.setDureeIncubation(dureeIncubation);
        sousSequencePP.setSequencePP(sequencePP);
        sequencePP.getSousSequences().add(sousSequencePP);

        Map<String, List<LineRecord>> profondeursLinesMap = new HashMap<String, List<LineRecord>>();

        for (LineRecord line : sousSequenceLines) {
            String sousSequenceKey = line.getProfondeur().toString();
            fillLinesMap(profondeursLinesMap, line, sousSequenceKey);
        }

        for (String profondeur : profondeursLinesMap.keySet()) {
            try {
                List<LineRecord> profondeurLines = profondeursLinesMap.get(profondeur);
                buildMesure(profondeurLines, sousSequencePP, errorsReport);

            } catch (InsertionDatabaseException e) {
                errorsReport.addException(e);
            }
        }
    }

    private void buildMesure(List<LineRecord> profondeurLines, SousSequencePP sousSequencePP, ErrorsReport errorsReport) throws PersistenceException, InsertionDatabaseException {
        MesurePP mesurePP = new MesurePP();
        LineRecord firstLine = profondeurLines.get(0);

        mesurePP.setSousSequencePP(sousSequencePP);
        sousSequencePP.getMesures().add(mesurePP);
        mesurePP.setProfondeur(firstLine.getProfondeur());
        mesurePP.setLigneFichierEchange(profondeurLines.get(0).getOriginalLineNumber());

        if (profondeurLines.size() > 1) {
            errorsReport.addException(new BusinessException((String.format(getGLACPEMessage(PROPERTY_MSG_DOUBLON_LINE), mesurePP.getLigneFichierEchange()))));
        } else {
            for (LineRecord profondeurLine : profondeurLines) {
                for (VariableValue variableValue : profondeurLine.getVariablesValues()) {
                    ValeurMesurePP valeurMesureChloro = new ValeurMesurePP();
                    VariableGLACPE variable = (VariableGLACPE) variableDAO.merge(variableValue.getVariableGLACPE());

                    valeurMesureChloro.setVariable(variable);

                    if (variableValue.getValue() == null || variableValue.getValue().length() == 0) {
                        valeurMesureChloro.setValeur(null);
                    } else {

                        Float value = Float.parseFloat(variableValue.getValue());
                        valeurMesureChloro.setValeur(value);
                    }

                    valeurMesureChloro.setMesure(mesurePP);
                    mesurePP.getValeurs().add(valeurMesureChloro);

                }
            }
        }
    }

    /**
     *
     * @param sequencePPDAO
     */
    public void setSequencePPDAO(ISequencePPDAO sequencePPDAO) {
        this.sequencePPDAO = sequencePPDAO;
    }

    /**
     *
     * @param projetSiteDAO
     */
    public void setProjetSiteDAO(IProjetSiteDAO projetSiteDAO) {
        this.projetSiteDAO = projetSiteDAO;
    }

    /**
     *
     * @param plateformeDAO
     */
    public void setPlateformeDAO(IPlateformeDAO plateformeDAO) {
        this.plateformeDAO = plateformeDAO;
    }

    /**
     *
     * @param controleCoherenceDAO
     */
    public void setControleCoherenceDAO(IControleCoherenceDAO controleCoherenceDAO) {
        this.controleCoherenceDAO = controleCoherenceDAO;
    }

    /**
     *
     * @param siteDAO
     */
    public void setSiteDAO(ISiteDAO siteDAO) {
        this.siteDAO = siteDAO;
    }
}
