package org.inra.ecoinfo.glacpe.dataset.productionprimaire.jpa;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.glacpe.dataset.productionprimaire.IPPDAO;
import org.inra.ecoinfo.glacpe.dataset.productionprimaire.entity.MesurePP;
import org.inra.ecoinfo.glacpe.dataset.productionprimaire.entity.ValeurMesurePP;
import org.inra.ecoinfo.glacpe.extraction.vo.AbstractDatesFormParam;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesRequestParamVO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
@SuppressWarnings("unchecked")
public class JPAProductionPrimaireDAO extends AbstractJPADAO<Object> implements IPPDAO {

    private static final String REQUEST_AllDepth = "select m from MesurePP m where m.sousSequencePP.plateforme.id in (:selectedPlateformesIds) and m.sousSequencePP.sequencePP.projetSite.id in(:selectedProjetSiteIds) %s  order by m.sousSequencePP.sequencePP.projetSite.projet.nom, m.sousSequencePP.plateforme.site.nom, m.sousSequencePP.plateforme.nom, m.sousSequencePP.sequencePP.date, m.profondeur";
    private static final String REQUEST_BetweenDepth = "select m from MesurePP m where m.sousSequencePP.plateforme.id in (:selectedPlateformesIds) and m.sousSequencePP.sequencePP.projetSite.id in(:selectedProjetSiteIds) and m.profondeur between :depthMin and :depthMax %s  order by m.sousSequencePP.sequencePP.projetSite.projet.nom, m.sousSequencePP.plateforme.site.nom, m.sousSequencePP.plateforme.nom, m.sousSequencePP.sequencePP.date, m.profondeur";
    private static final String REQUEST_AGGREGATED_AllDepth = "select vm from ValeurMesurePP vm where vm.variable.id in(:variablesId) and vm.mesure.sousSequencePP.plateforme.id in (:selectedPlateformesIds) and vm.mesure.sousSequencePP.sequencePP.projetSite.id in(:selectedProjetSiteIds) %s  order by vm.mesure.sousSequencePP.sequencePP.projetSite.projet.nom, vm.mesure.sousSequencePP.plateforme.site.nom, vm.mesure.sousSequencePP.plateforme.nom, vm.mesure.sousSequencePP.sequencePP.date, vm.mesure.profondeur";
    private static final String REQUEST_AGGREGATED_BetweenDepth = "select vm from ValeurMesurePP vm where  vm.variable.id in(:variablesId) and vm.mesure.sousSequencePP.plateforme.id in (:selectedPlateformesIds) and vm.mesure.sousSequencePP.sequencePP.projetSite.id in(:selectedProjetSiteIds) and vm.mesure.profondeur between :depthMin and :depthMax %s  order by vm.mesure.sousSequencePP.sequencePP.projetSite.projet.nom, vm.mesure.sousSequencePP.plateforme.site.nom, vm.mesure.sousSequencePP.plateforme.nom, vm.mesure.sousSequencePP.sequencePP.date, vm.mesure.profondeur";

    private static final String REQUEST_AVAILABLES_VARIABLES = "select distinct vmp.variable from ValeurMesurePP vmp where vmp.mesure.sousSequencePP.plateforme.id in (:plateformesIds) and vmp.mesure.sousSequencePP.sequencePP.date between :firstDate and :lastDate order by vmp.variable.ordreAffichageGroupe";
    private static final String REQUEST_AVAILABLES_SITES = "select distinct ss.plateforme.site from SousSequencePP ss";
    private static final String REQUEST_AVAILABLE_PROJECTS = "select distinct s.projetSite.projet from SequencePP s";
    private static final String REQUEST_AVAILABLE_PLATEFORME_FOR_PROJECTS = "select distinct ss.plateforme from SousSequencePP ss where ss.sequencePP.projetSite.id=:id";

    private static final String HQL_DATE_FIELD_2 = "m.sousSequencePP.sequencePP.date";
    private static final String HQL_DATE_FIELD = "vm.mesure.sousSequencePP.sequencePP.date";

    /**
     *
     * @param selectedPlateformesIds
     * @param selectedProjetSiteIds
     * @param datesRequestParamVO
     * @param depthMin
     * @param depthMax
     * @return
     * @throws PersistenceException
     */
    @Override
    public List<MesurePP> extractDatasForRangeDepth(List<Long> selectedPlateformesIds, List<Long> selectedProjetSiteIds, DatesRequestParamVO datesRequestParamVO, Float depthMin, Float depthMax) throws PersistenceException {
        try {
            AbstractDatesFormParam datesFormParam = datesRequestParamVO.getCurrentDatesFormParam();
            Query query = entityManager.createQuery(String.format(REQUEST_BetweenDepth, datesFormParam.buildSQLCondition(HQL_DATE_FIELD_2)));
            Map<String, Date> datesMap = datesFormParam.retrieveParametersMap();

            for (String key : datesMap.keySet()) {
                query.setParameter(key, datesMap.get(key));
            }

            query.setParameter("selectedPlateformesIds", selectedPlateformesIds);
            query.setParameter("selectedProjetSiteIds", selectedProjetSiteIds);
            query.setParameter("depthMin", depthMin);
            query.setParameter("depthMax", depthMax);

            return query.getResultList();

        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param selectedPlateformesIds
     * @param selectedProjetSiteIds
     * @param datesRequestParamVO
     * @return
     * @throws PersistenceException
     */
    @Override
    public List<MesurePP> extractDatasForAllDepth(List<Long> selectedPlateformesIds, List<Long> selectedProjetSiteIds, DatesRequestParamVO datesRequestParamVO) throws PersistenceException {
        try {
            AbstractDatesFormParam datesFormParam = datesRequestParamVO.getCurrentDatesFormParam();
            Query query = entityManager.createQuery(String.format(REQUEST_AllDepth, datesFormParam.buildSQLCondition(HQL_DATE_FIELD_2)));
            Map<String, Date> datesMap = datesFormParam.retrieveParametersMap();

            for (String key : datesMap.keySet()) {
                query.setParameter(key, datesMap.get(key));
            }

            query.setParameter("selectedPlateformesIds", selectedPlateformesIds);
            query.setParameter("selectedProjetSiteIds", selectedProjetSiteIds);

            return query.getResultList();

        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param plateformesIds
     * @param selectedProjetSiteIds
     * @param datesRequestParamVO
     * @param variablesId
     * @return
     * @throws PersistenceException
     */
    @Override
    public List<ValeurMesurePP> extractAggregatedDatasForAllDepth(List<Long> plateformesIds, List<Long> selectedProjetSiteIds, DatesRequestParamVO datesRequestParamVO, List<Long> variablesId) throws PersistenceException {
        try {
            AbstractDatesFormParam datesFormParam = datesRequestParamVO.getCurrentDatesFormParam();
            Query query = entityManager.createQuery(String.format(REQUEST_AGGREGATED_AllDepth, datesFormParam.buildSQLCondition(HQL_DATE_FIELD)));
            Map<String, Date> datesMap = datesFormParam.retrieveParametersMap();

            for (String key : datesMap.keySet()) {
                query.setParameter(key, datesMap.get(key));
            }

            query.setParameter("selectedPlateformesIds", plateformesIds);
            query.setParameter("variablesId", variablesId);
            query.setParameter("selectedProjetSiteIds", selectedProjetSiteIds);

            return query.getResultList();

        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param plateformesIds
     * @param selectedProjetSiteIds
     * @param datesRequestParamVO
     * @param variablesId
     * @param depthMin
     * @param depthMax
     * @return
     * @throws PersistenceException
     */
    @Override
    public List<ValeurMesurePP> extractAggregatedDatasForRangeDepth(List<Long> plateformesIds, List<Long> selectedProjetSiteIds, DatesRequestParamVO datesRequestParamVO, List<Long> variablesId, Float depthMin, Float depthMax)
            throws PersistenceException
    {
        try {
            AbstractDatesFormParam datesFormParam = datesRequestParamVO.getCurrentDatesFormParam();
            Query query = entityManager.createQuery(String.format(REQUEST_AGGREGATED_BetweenDepth, datesFormParam.buildSQLCondition(HQL_DATE_FIELD)));
            Map<String, Date> datesMap = datesFormParam.retrieveParametersMap();

            for (String key : datesMap.keySet()) {
                query.setParameter(key, datesMap.get(key));
            }

            query.setParameter("selectedPlateformesIds", plateformesIds);
            query.setParameter("variablesId", variablesId);
            query.setParameter("selectedProjetSiteIds", selectedProjetSiteIds);
            query.setParameter("depthMin", depthMin);
            query.setParameter("depthMax", depthMax);

            return query.getResultList();

        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     *
     * @return
     * @throws PersistenceException
     */
    @Override
    public List<Site> retrieveAvailablesSites() throws PersistenceException {
        try {
            Query query = entityManager.createQuery(REQUEST_AVAILABLES_SITES);
            List<Site> sites = query.getResultList();

            return sites;
        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param id
     * @return
     * @throws PersistenceException
     */
    @Override
    public List<Plateforme> retrieveAvailablesPlateformesByProjetSiteId(Long id) throws PersistenceException {
        try {
            Query query = entityManager.createQuery(REQUEST_AVAILABLE_PLATEFORME_FOR_PROJECTS);
            query.setParameter("id", id);
            List<Plateforme> plateformes = query.getResultList();

            return plateformes;
        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param plateformesIds
     * @param firstDate
     * @param endDate
     * @return
     * @throws PersistenceException
     */
    @Override
    public List<VariableGLACPE> retrieveAvailablesVariables(List<Long> plateformesIds, Date firstDate, Date endDate) throws PersistenceException {
        try {
            Query query = entityManager.createQuery(REQUEST_AVAILABLES_VARIABLES);
            query.setParameter("plateformesIds", plateformesIds);
            query.setParameter("firstDate", firstDate);
            query.setParameter("lastDate", endDate);
            List<VariableGLACPE> variables = query.getResultList();

            return variables;
        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     *
     * @return
     * @throws PersistenceException
     */
    @Override
    public List<Projet> retrieveAvailablesProjets() throws PersistenceException {
        try {
            Query query = entityManager.createQuery(REQUEST_AVAILABLE_PROJECTS);
            List<Projet> projets = query.getResultList();

            return projets;
        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }
}
