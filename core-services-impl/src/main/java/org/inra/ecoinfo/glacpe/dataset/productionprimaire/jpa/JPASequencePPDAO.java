/**
 * OREILacs project - see LICENCE.txt for use created: 13 août 2009 14:33:43
 */
package org.inra.ecoinfo.glacpe.dataset.productionprimaire.jpa;

import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.glacpe.dataset.productionprimaire.ISequencePPDAO;
import org.inra.ecoinfo.glacpe.dataset.productionprimaire.entity.SequencePP;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public class JPASequencePPDAO extends AbstractJPADAO<SequencePP> implements ISequencePPDAO {

    /**
     *
     * @param date
     * @param projetCode
     * @param siteCode
     * @return
     * @throws PersistenceException
     */
    @Override
    public SequencePP getByDatePrelevementAndProjetCodeAndSiteCode(Date date, String projetCode, String siteCode) throws PersistenceException {
        try {
            Query query = null;

            String queryString = "from SequencePP s where s.date = :date and s.projetSite.projet.code = :projetCode and s.projetSite.site.code = :siteCode";

            query = entityManager.createQuery(queryString);
            query.setParameter("date", date);
            query.setParameter("projetCode", projetCode);
            query.setParameter("siteCode", siteCode);

            List<?> results = query.getResultList();
            if (results != null && results.size() == 1) {
                return (SequencePP) results.get(0);
            }

        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
        return null;
    }
}
