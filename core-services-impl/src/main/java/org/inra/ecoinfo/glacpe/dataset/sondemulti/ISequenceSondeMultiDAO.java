package org.inra.ecoinfo.glacpe.dataset.sondemulti;

import java.util.Date;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.glacpe.dataset.sondemulti.entity.SequenceSondeMulti;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public interface ISequenceSondeMultiDAO extends IDAO<SequenceSondeMulti> {

    /**
     *
     * @param datePrelevement
     * @param projetCode
     * @param siteCode
     * @return
     * @throws PersistenceException
     */
    SequenceSondeMulti getByDatePrelevementAndProjetCodeAndSiteCode(Date datePrelevement, String projetCode, String siteCode) throws PersistenceException;

    void flush();

}
