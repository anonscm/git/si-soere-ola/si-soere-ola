package org.inra.ecoinfo.glacpe.dataset.sondemulti;

import java.util.Date;
import java.util.List;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.glacpe.dataset.sondemulti.entity.MesureSondeMulti;
import org.inra.ecoinfo.glacpe.dataset.sondemulti.entity.SousSequenceSondeMulti;
import org.inra.ecoinfo.glacpe.dataset.sondemulti.entity.ValeurMesureSondeMulti;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesRequestParamVO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.projetsite.ProjetSite;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public interface ISondeMultiDAO extends IDAO<Object> {

    /**
     *
     * @return
     * @throws PersistenceException
     */
    List<Projet> retrieveSondeMultiAvailablesProjets() throws PersistenceException;

    /**
     *
     * @param projetSiteId
     * @return
     * @throws PersistenceException
     */
    List<Plateforme> retrieveSondeMultiAvailablesPlateformesByProjetSiteId(long projetSiteId) throws PersistenceException;

    /**
     *
     * @param plateformesIds
     * @param firstDate
     * @param lastDate
     * @return
     * @throws PersistenceException
     */
    List<VariableGLACPE> retrieveSondeMultiAvailablesVariables(List<Long> plateformesIds, Date firstDate, Date lastDate) throws PersistenceException;

    /**
     *
     * @param selectedPlateformesIds
     * @param selectedVariablesIds
     * @param projetId
     * @param datesRequestParamVO
     * @return
     * @throws PersistenceException
     */
    List<ValeurMesureSondeMulti> extractAggregatedDatasForAllDepth(List<Long> selectedPlateformesIds, List<Long> selectedVariablesIds, List<Long> projetId, DatesRequestParamVO datesRequestParamVO) throws PersistenceException;

    /**
     *
     * @param selectedPlateformesIds
     * @param selectedVariablesIds
     * @param projetId
     * @param datesRequestParamVO
     * @param depthMin
     * @param depthMax
     * @return
     * @throws PersistenceException
     */
    List<ValeurMesureSondeMulti> extractAggregatedDatasForRangeDepth(List<Long> selectedPlateformesIds, List<Long> selectedVariablesIds, List<Long> projetId, DatesRequestParamVO datesRequestParamVO, Float depthMin, Float depthMax)
            throws PersistenceException;

    /**
     *
     * @param selectedPlateformesIds
     * @param projetId
     * @param datesRequestParamVO
     * @param depthMin
     * @param depthMax
     * @return
     * @throws PersistenceException
     */
    List<MesureSondeMulti> extractDatasForRangeDepth(List<Long> selectedPlateformesIds, List<Long> projetId, DatesRequestParamVO datesRequestParamVO, Float depthMin, Float depthMax) throws PersistenceException;

    /**
     *
     * @param selectedPlateformesIds
     * @param projetId
     * @param datesRequestParamVO
     * @return
     * @throws PersistenceException
     */
    List<SousSequenceSondeMulti> extractDatasForUniqueDepth(List<Long> selectedPlateformesIds, List<Long> projetId, DatesRequestParamVO datesRequestParamVO) throws PersistenceException;

    /**
     *
     * @param selectedPlateformesIds
     * @param projetId
     * @param datesRequestParamVO
     * @return
     * @throws PersistenceException
     */
    List<MesureSondeMulti> extractDatasForAllDepth(List<Long> selectedPlateformesIds, List<Long> projetId, DatesRequestParamVO datesRequestParamVO) throws PersistenceException;

    /**
     *
     * @param projetId
     * @param siteId
     * @return
     * @throws PersistenceException
     */
    ProjetSite retrieveSondeMultiAvailablesProjetsSiteByProjetAndSite(Long projetId, Long siteId) throws PersistenceException;
}
