package org.inra.ecoinfo.glacpe.dataset.sondemulti.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.glacpe.dataset.sondemulti.ISondeMultiDAO;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.IDatatypeVariableUniteGLACPEDAO;
import org.inra.ecoinfo.refdata.datatype.IDatatypeDAO;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.utils.Utils;

/**
 *
 * @author ptcherniati
 */
public abstract class AbstractSondeMultiRawsDatasOutputBuilder extends AbstractOutputBuilder {

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH_SONDEMULTI = "org.inra.ecoinfo.glacpe.extraction.sondemulti.messages";

    /**
     *
     */
    protected static final String MAP_INDEX_0 = "0";

    /**
     *
     */
    protected ISondeMultiDAO sondeMultiDAO;

    /**
     *
     */
    protected IVariableDAO variableDAO;

    /**
     *
     */
    protected IDatatypeDAO datatypeDAO;

    /**
     *
     */
    protected IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO;

    /**
     *
     * @param selectedPlateformes
     * @return
     */
    protected Set<String> retrieveSitesCodes(List<PlateformeVO> selectedPlateformes) {
        Set<String> sitesNames = new HashSet<String>();
        for (PlateformeVO plateforme : selectedPlateformes) {
            sitesNames.add(Utils.createCodeFromString(plateforme.getNomSite()));
        }
        return sitesNames;
    }

    /**
     *
     * @param sondeMultiDAO
     */
    public void setSondeMultiDAO(ISondeMultiDAO sondeMultiDAO) {
        this.sondeMultiDAO = sondeMultiDAO;
    }

    /**
     *
     * @param variableDAO
     */
    public void setVariableDAO(IVariableDAO variableDAO) {
        this.variableDAO = variableDAO;
    }

    public void setDatatypeDAO(IDatatypeDAO datatypeDAO) {
        this.datatypeDAO = datatypeDAO;
    }

    /**
     *
     * @param datatypeVariableUniteDAO
     */
    public void setDatatypeVariableUniteDAO(IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO) {
        this.datatypeVariableUniteDAO = datatypeVariableUniteDAO;
    }
}
