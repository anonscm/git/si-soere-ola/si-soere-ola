package org.inra.ecoinfo.glacpe.dataset.sondemulti.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.inra.ecoinfo.MO;
import org.inra.ecoinfo.glacpe.dataset.sondemulti.ISondeMultiDAO;
import org.inra.ecoinfo.glacpe.dataset.sondemulti.ISondeMultiDatatypeManager;
import org.inra.ecoinfo.glacpe.extraction.vo.VariableVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.IDatatypeVariableUniteGLACPEDAO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.projetsite.ProjetSite;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.datatype.IDatatypeDAO;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ptcherniati
 */
public class DefaultSondeMultiDatatypeManager extends MO implements ISondeMultiDatatypeManager {

    /**
     *
     */
    protected ISondeMultiDAO sondeMultiDAO;

    /**
     *
     */
    protected IDatatypeDAO datatypeDAO;

    /**
     *
     */
    protected IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO;

    /**
     *
     * @return
     * @throws BusinessException
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public List<Projet> retrieveSondeMultiAvailablesProjets() throws BusinessException {
        try {
            return sondeMultiDAO.retrieveSondeMultiAvailablesProjets();
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }

    }

    /**
     *
     * @param projetSiteId
     * @return
     * @throws BusinessException
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public List<Plateforme> retrieveSondeMultiAvailablesPlateformesByProjetSiteId(long projetSiteId) throws BusinessException {
        try {
            return sondeMultiDAO.retrieveSondeMultiAvailablesPlateformesByProjetSiteId(projetSiteId);
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    private List<VariableVO> retrieveListVariables() throws BusinessException {
        try {
            List<VariableGLACPE> variables = (List) ((DataType) datatypeDAO.getByCode("sonde_multiparametres")).getVariables();
            List<VariableVO> variablesVO = new ArrayList<VariableVO>();

            for (VariableGLACPE variable : variables) {
                VariableVO variableVO = new VariableVO(variable);
                variablesVO.add(variableVO);
            }
            return variablesVO;
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    private List<VariableVO> retrieveListAvailableVariables(List<Long> plateformesIds, Date firstDate, Date lastDate) throws BusinessException {
        try {
            List<VariableGLACPE> variables = sondeMultiDAO.retrieveSondeMultiAvailablesVariables(plateformesIds, firstDate, lastDate);
            List<VariableVO> variablesVO = new ArrayList<VariableVO>();

            for (VariableGLACPE variable : variables) {
                VariableVO variableVO = new VariableVO(variable);
                variablesVO.add(variableVO);
            }
            return variablesVO;
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    /**
     *
     * @param plateformesIds
     * @param firstDate
     * @param lastDate
     * @return
     * @throws BusinessException
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public List<VariableVO> retrieveListsVariables(List<Long> plateformesIds, Date firstDate, Date lastDate) throws BusinessException {
        Map<String, VariableVO> rawVariablesMap = new HashMap<String, VariableVO>();

        for (VariableVO variable : retrieveListVariables()) {
            rawVariablesMap.put(variable.getCode(), variable);
        }

        for (VariableVO availableVariable : retrieveListAvailableVariables(plateformesIds, firstDate, lastDate)) {
            availableVariable.setIsDataAvailable(true);
            rawVariablesMap.put(availableVariable.getCode(), availableVariable);
        }

        return new LinkedList<VariableVO>(rawVariablesMap.values());
    }

    /**
     *
     * @param sondeMultiDAO
     */
    public void setSondeMultiDAO(ISondeMultiDAO sondeMultiDAO) {
        this.sondeMultiDAO = sondeMultiDAO;
    }

    /**
     *
     * @param datatypeDAO
     */
    public void setDatatypeDAO(IDatatypeDAO datatypeDAO) {
        this.datatypeDAO = datatypeDAO;
    }

    /**
     *
     * @param projetId
     * @param siteId
     * @return
     * @throws BusinessException
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public ProjetSite retrieveSondeMultiAvailablesProjetsSiteByProjetAndSite(Long projetId, Long siteId) throws BusinessException {
        try {
            return sondeMultiDAO.retrieveSondeMultiAvailablesProjetsSiteByProjetAndSite(projetId, siteId);
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    /**
     *
     * @param code
     * @return
     * @throws BusinessException
     */
    @Override
    public Unite retrieveVariableUnitByCode(String code) throws BusinessException {
        try {
            return datatypeVariableUniteDAO.getUnite(CODE_DATATYPE_SONDE_MULTI, code);
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    /**
     *
     * @param datatypeVariableUniteDAO
     */
    public void setDatatypeVariableUniteDAO(IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO) {
        this.datatypeVariableUniteDAO = datatypeVariableUniteDAO;
    }
}
