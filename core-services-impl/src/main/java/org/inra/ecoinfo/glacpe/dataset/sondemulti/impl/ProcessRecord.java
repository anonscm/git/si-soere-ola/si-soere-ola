package org.inra.ecoinfo.glacpe.dataset.sondemulti.impl;

import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_DOUBLON_LINE;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_DUPLICATE_SEQUENCE;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_ERROR;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_ERROR_PLATEFORME_INVALID;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_ERROR_PLATEFORME_SITE_MISMATCH;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_FLOAT_VALUE_EXPECTED;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_MISSING_OUTILS_MESURE_IN_REFERENCES_DATAS;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_MISSING_PROJET_SITE_IN_REFERENCES_DATAS;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.getGLACPEMessage;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.hibernate.exception.ConstraintViolationException;
import org.inra.ecoinfo.dataset.exception.InsertionDatabaseException;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.glacpe.dataset.impl.AbstractProcessRecord;
import org.inra.ecoinfo.glacpe.dataset.impl.CleanerValues;
import org.inra.ecoinfo.glacpe.dataset.impl.VariableValue;
import org.inra.ecoinfo.glacpe.dataset.sondemulti.ISequenceSondeMultiDAO;
import org.inra.ecoinfo.glacpe.dataset.sondemulti.ISondeMultiDAO;
import org.inra.ecoinfo.glacpe.dataset.sondemulti.entity.MesureSondeMulti;
import org.inra.ecoinfo.glacpe.dataset.sondemulti.entity.SequenceSondeMulti;
import org.inra.ecoinfo.glacpe.dataset.sondemulti.entity.SousSequenceSondeMulti;
import org.inra.ecoinfo.glacpe.dataset.sondemulti.entity.ValeurMesureSondeMulti;
import org.inra.ecoinfo.glacpe.refdata.controlecoherence.ControleCoherence;
import org.inra.ecoinfo.glacpe.refdata.controlecoherence.IControleCoherenceDAO;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.IOutilsMesureDAO;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.OutilsMesure;
import org.inra.ecoinfo.glacpe.refdata.plateforme.IPlateformeDAO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projetsite.IProjetSiteDAO;
import org.inra.ecoinfo.glacpe.refdata.projetsite.ProjetSite;
import org.inra.ecoinfo.glacpe.refdata.projetsitethemedatatype.ProjetSiteThemeDatatype;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.system.Allocator;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.DatasetDescriptorBuilder;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.ErrorsReport;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.xml.sax.SAXException;

import com.Ostermiller.util.CSVParser;

/**
 *
 * @author ptcherniati
 */
public class ProcessRecord extends AbstractProcessRecord {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    protected ISequenceSondeMultiDAO sequenceSondeMultiDAO;

    /**
     *
     */
    protected ISondeMultiDAO sondeMultiDAO;

    /**
     *
     */
    protected IControleCoherenceDAO controleCoherenceDAO;

    /**
     *
     */
    protected IProjetSiteDAO projetSiteDAO;

    /**
     *
     */
    protected IPlateformeDAO plateformeDAO;

    /**
     *
     */
    protected IOutilsMesureDAO outilsMesureDAO;

    @Override
    public void processRecord(CSVParser parser, VersionFile versionFile, String fileEncoding, DatasetDescriptor datasetDescriptor) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport(getGLACPEMessage(PROPERTY_MSG_ERROR));
        Utils.testNotNullArguments(getLocalizationManager(), versionFile, fileEncoding);
        Utils.testCastedArguments(versionFile, VersionFile.class, getLocalizationManager());

        try {

            String[] values = null;
            long lineCount = 1;
            int variableHeaderIndex = datasetDescriptor.getUndefinedColumn();

            Map<String, ControleCoherence> controlesCoherenceMap = controleCoherenceDAO.getControleCoherenceBySitecodeAndDatatypecode(((ProjetSiteThemeDatatype) versionFile.getDataset().getLeafNode()).getProjetSiteTheme().getProjetSite().getSite()
                    .getCode(), versionFile.getDataset().getLeafNode().getDatatype().getCode());
            List<VariableGLACPE> dbVariables = buildVariableHeaderAndSkipHeader(variableHeaderIndex, parser, errorsReport);
            Map<String, List<LineRecord>> sequencesMapLines = new HashMap<String, List<LineRecord>>();

            // On parcourt chaque ligne du fichier
            while ((values = parser.getLine()) != null) {

                CleanerValues cleanerValues = new CleanerValues(values);
                lineCount++;
                List<VariableValue> variablesValues = new LinkedList<VariableValue>();

                // On parcourt chaque colonne d'une ligne
                String projetCode = Utils.createCodeFromString(cleanerValues.nextToken());
                String nomSite = cleanerValues.nextToken();
                String plateformeCode = Utils.createCodeFromString(cleanerValues.nextToken());
                String datePrelevementString = cleanerValues.nextToken();
                Date datePrelevement = datePrelevementString.length() > 0 ? new SimpleDateFormat(datasetDescriptor.getColumns().get(cleanerValues.currentTokenIndex() - 1).getFormatType()).parse(datePrelevementString) : null;
                String outilMesureCode = Utils.createCodeFromString(cleanerValues.nextToken());
                String commentaires = cleanerValues.nextToken();
                String heureString = cleanerValues.nextToken();
                Date heure = heureString.length() > 0 ? new SimpleDateFormat(datasetDescriptor.getColumns().get(cleanerValues.currentTokenIndex() - 1).getFormatType()).parse(heureString) : null;
                String profondeurString = cleanerValues.nextToken();
                Float profondeur = profondeurString.length() > 0 ? Float.parseFloat(profondeurString) : 0.0f;

                for (int actualVariableArray = variableHeaderIndex; actualVariableArray < values.length; actualVariableArray++) {
                    String value = values[actualVariableArray].replaceAll(" ", "");
                    try {
                        VariableGLACPE variable = dbVariables.get(actualVariableArray - variableHeaderIndex);
                        if (controlesCoherenceMap.get(variable.getCode()) != null)
                            testValueCoherence(Float.valueOf(value), controlesCoherenceMap.get(variable.getCode()).getValeurMin(), controlesCoherenceMap.get(variable.getCode()).getValeurMax(), lineCount, 11);
                        variablesValues.add(new VariableValue(dbVariables.get(actualVariableArray - variableHeaderIndex), values[actualVariableArray].replaceAll(" ", "")));
                    } catch (NumberFormatException e) {
                        errorsReport.addException(new BadExpectedValueException(String.format(getGLACPEMessage(PROPERTY_MSG_FLOAT_VALUE_EXPECTED), lineCount, 11, value)));
                    } catch (BadExpectedValueException e) {
                        errorsReport.addException(e);
                    }
                }

                LineRecord line = new LineRecord(nomSite, plateformeCode, datePrelevement, outilMesureCode, commentaires, profondeur, heure, variablesValues, lineCount, projetCode);
                String sequenceKey = projetCode.concat(nomSite).concat(datePrelevementString);
                fillLinesMap(sequencesMapLines, line, sequenceKey);

            }
            Allocator allocator = Allocator.getInstance();

            int count = 0;
            allocator.allocate("publish", versionFile.getFileSize());
            Iterator<Entry<String, List<LineRecord>>> iterator = sequencesMapLines.entrySet().iterator();
            while (iterator.hasNext()) {

                final Entry<String, List<LineRecord>> entry = iterator.next();
                List<LineRecord> sequenceLines = sequencesMapLines.get(entry.getKey());
                if (!sequenceLines.isEmpty()) {
                    try {
                        Date datePrelevement = sequenceLines.get(0).getDatePrelevement();
                        String projetCode = sequenceLines.get(0).getProjetCode();
                        String siteCode = Utils.createCodeFromString(sequenceLines.get(0).getNomSite());
                        buildSequence(datePrelevement, projetCode, siteCode, sequenceLines, versionFile, errorsReport);
                        logger.debug(String.format("%d - %s", count++, datePrelevement));

                        // Très important à cause de problèmes de performances
                        sequenceSondeMultiDAO.flush();
                        versionFile = (VersionFile) versionFileDAO.merge(versionFile);
                    } catch (InsertionDatabaseException e) {
                        errorsReport.addException(e);
                    }
                }
                iterator.remove();
            }

            if (errorsReport.hasErrors()) {
                throw new PersistenceException(errorsReport.buildHTMLMessages());
            }

        } catch (Exception e) {

            throw new BusinessException(e);
        }

    }

    private List<VariableGLACPE> buildVariableHeaderAndSkipHeader(int variableHeaderIndex, CSVParser parser, ErrorsReport errorsReport) throws PersistenceException, IOException {
        List<String> variablesHeaders = skipHeaderAndRetrieveVariableName(variableHeaderIndex, parser);
        List<VariableGLACPE> dbVariables = new LinkedList<VariableGLACPE>();
        for (String variableHeader : variablesHeaders) {
            VariableGLACPE variable = (VariableGLACPE) variableDAO.getByCode(Utils.createCodeFromString(variableHeader));
            if (variable == null) {
                errorsReport.addException(new BusinessException((String.format(getGLACPEMessage(PROPERTY_MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS), variableHeader))));
            }

            dbVariables.add(variable);
        }
        if (errorsReport.hasErrors()) {
            throw new PersistenceException(errorsReport.buildHTMLMessages());
        }
        return dbVariables;
    }

    private void fillLinesMap(Map<String, List<LineRecord>> linesMap, LineRecord line, String keyLine) {
        List<LineRecord> sousSequenceLines = null;
        if (keyLine != null && keyLine.trim().length() > 0) {
            if (linesMap.containsKey(keyLine)) {
                linesMap.get(keyLine).add(line);
            } else {
                sousSequenceLines = new LinkedList<LineRecord>();
                sousSequenceLines.add(line);
                linesMap.put(keyLine, sousSequenceLines);
            }
        }
    }

    private void buildSequence(Date datePrelevement, String projetCode, String siteCode, List<LineRecord> sequenceLines, VersionFile versionFile, ErrorsReport errorsReport) throws PersistenceException, InsertionDatabaseException {

        LineRecord firstLine = sequenceLines.get(0);

        SequenceSondeMulti sequenceSondeMulti = sequenceSondeMultiDAO.getByDatePrelevementAndProjetCodeAndSiteCode(datePrelevement, projetCode, siteCode);

        if (sequenceSondeMulti == null) {

            ProjetSite projetSite = projetSiteDAO.getBySiteCodeAndProjetCode(siteCode, projetCode);
            if (projetSite == null) {
                errorsReport.addException(new PersistenceException(String.format(getGLACPEMessage(PROPERTY_MSG_MISSING_PROJET_SITE_IN_REFERENCES_DATAS), projetCode, siteCode)));
                throw new PersistenceException(String.format(getGLACPEMessage(PROPERTY_MSG_MISSING_PROJET_SITE_IN_REFERENCES_DATAS), projetCode, siteCode));
            }

            if (!siteCode.equals(Utils.createCodeFromString(firstLine.getNomSite()))) {
                InsertionDatabaseException insertionDatabaseException = new InsertionDatabaseException(String.format(getGLACPEMessage(PROPERTY_MSG_ERROR_PLATEFORME_SITE_MISMATCH), firstLine.getPlateformeCode(), firstLine.getOriginalLineNumber(),
                        siteCode));
                throw insertionDatabaseException;
            }

            sequenceSondeMulti = new SequenceSondeMulti();

            sequenceSondeMulti.setProjetSite(projetSite);
            sequenceSondeMulti.setDatePrelevement(datePrelevement);

            sequenceSondeMulti.setVersionFile(versionFile);
        }

        Map<String, List<LineRecord>> sousSequencesMap = new HashMap<String, List<LineRecord>>();

        for (LineRecord line : sequenceLines) {
            fillLinesMap(sousSequencesMap, line, line.getPlateformeCode().concat(line.getOutilsMesureCode()));
        }

        for (String sousSequenceKey : sousSequencesMap.keySet()) {
            try {
                List<LineRecord> projetLines = sousSequencesMap.get(sousSequenceKey);
                buildSousSequence(projetLines.get(0).getPlateformeCode(), projetLines.get(0).getOutilsMesureCode(), projetLines.get(0).getCommentaires(), projetLines, sequenceSondeMulti, errorsReport);

            } catch (InsertionDatabaseException e) {
                errorsReport.addException(e);
            }
        }
        if (!errorsReport.hasErrors()) {
            try {
                sequenceSondeMultiDAO.saveOrUpdate(sequenceSondeMulti);
            } catch (ConstraintViolationException e) {
                String message = String.format(getGLACPEMessage(PROPERTY_MSG_DUPLICATE_SEQUENCE), DateUtil.getSimpleDateFormatDateyyyyMMddUTC().format(datePrelevement));
                errorsReport.addException(new BusinessException(message));
            }
        }
    }

    private void buildSousSequence(String plateformeCode, String outilsMesureCode, String commentaires, List<LineRecord> sousSequenceLines, SequenceSondeMulti sequenceSondeMulti, ErrorsReport errorsReport) throws PersistenceException,
            InsertionDatabaseException
    {

        LineRecord firstLine = sousSequenceLines.get(0);

        Plateforme plateforme = null;
        plateforme = plateformeDAO.getByNKey(plateformeCode, Utils.createCodeFromString(firstLine.getNomSite()));
        if (plateforme == null) {
            InsertionDatabaseException insertionDatabaseException = new InsertionDatabaseException(String.format(getGLACPEMessage(PROPERTY_MSG_ERROR_PLATEFORME_INVALID), plateformeCode, firstLine.getOriginalLineNumber()));
            throw insertionDatabaseException;
        }

        OutilsMesure outilsMesure = outilsMesureDAO.getByCode(outilsMesureCode);
        if (outilsMesure == null) {
            errorsReport.addException((new PersistenceException(String.format(getGLACPEMessage(PROPERTY_MSG_MISSING_OUTILS_MESURE_IN_REFERENCES_DATAS), outilsMesureCode))));
            throw new PersistenceException(String.format(getGLACPEMessage(PROPERTY_MSG_MISSING_OUTILS_MESURE_IN_REFERENCES_DATAS), outilsMesureCode));
        }

        SousSequenceSondeMulti sousSequenceSondeMulti = new SousSequenceSondeMulti();
        sousSequenceSondeMulti.setPlateforme(plateforme);
        sousSequenceSondeMulti.setSequence(sequenceSondeMulti);
        sousSequenceSondeMulti.setCommentaires(commentaires);
        sousSequenceSondeMulti.setOutilsMesure(outilsMesure);
        sequenceSondeMulti.getSousSequences().add(sousSequenceSondeMulti);

        Map<String, List<LineRecord>> profondeursLinesMap = new HashMap<String, List<LineRecord>>();

        for (LineRecord line : sousSequenceLines) {
            String sousSequenceKey = line.getProfondeur().toString().concat(line.getOutilsMesureCode());
            fillLinesMap(profondeursLinesMap, line, sousSequenceKey);
        }

        for (String profondeurKey : profondeursLinesMap.keySet()) {
            try {
                List<LineRecord> profondeurLines = profondeursLinesMap.get(profondeurKey);
                buildMesure(profondeurLines.get(0).getProfondeur(), profondeurLines.get(0).getHeure(), profondeurLines, sousSequenceSondeMulti, errorsReport);

            } catch (InsertionDatabaseException e) {
                errorsReport.addException(e);
            }
        }
    }

    private void buildMesure(Float profondeur, Date heure, List<LineRecord> profondeurLines, SousSequenceSondeMulti sousSequenceSondeMulti, ErrorsReport errorsReport) throws PersistenceException, InsertionDatabaseException {
        MesureSondeMulti mesureSondeMulti = null;

        mesureSondeMulti = new MesureSondeMulti();
        mesureSondeMulti.setSousSequence(sousSequenceSondeMulti);
        sousSequenceSondeMulti.getMesures().add(mesureSondeMulti);
        mesureSondeMulti.setProfondeur(profondeur);
        mesureSondeMulti.setHeure(heure);
        mesureSondeMulti.setLigneFichierEchange(profondeurLines.get(0).getOriginalLineNumber());

        if (profondeurLines.size() > 1) {
            errorsReport.addException(new BusinessException((String.format(getGLACPEMessage(PROPERTY_MSG_DOUBLON_LINE), mesureSondeMulti.getLigneFichierEchange()))));
        } else {
            for (LineRecord profondeurLine : profondeurLines) {
                for (VariableValue variableValue : profondeurLine.getVariablesValues()) {
                    ValeurMesureSondeMulti valeurMesureSondeMulti = new ValeurMesureSondeMulti();
                    VariableGLACPE variable = (VariableGLACPE) variableDAO.merge(variableValue.getVariableGLACPE());

                    valeurMesureSondeMulti.setVariable(variable);

                    if (variableValue.getValue() == null || variableValue.getValue().length() == 0) {
                        valeurMesureSondeMulti.setValeur(null);
                    } else {

                        Float value = Float.parseFloat(variableValue.getValue());
                        valeurMesureSondeMulti.setValeur(value);
                    }

                    valeurMesureSondeMulti.setMesure(mesureSondeMulti);
                    mesureSondeMulti.getValeurs().add(valeurMesureSondeMulti);

                }
            }
        }
    }

    /**
     *
     * @return
     * @throws IOException
     * @throws SAXException
     */
    @Override
    public DatasetDescriptor holdDatasetDescriptor() throws IOException, SAXException {
        if (datasetDescriptor == null) {
            datasetDescriptor = DatasetDescriptorBuilder.buildDescriptor(ISondeMultiDAO.class.getResource(DATASET_DESCRIPTOR_XML).openStream());
        }
        return datasetDescriptor;
    }

    /**
     *
     * @param sequenceSondeMultiDAO
     */
    public void setSequenceSondeMultiDAO(ISequenceSondeMultiDAO sequenceSondeMultiDAO) {
        this.sequenceSondeMultiDAO = sequenceSondeMultiDAO;
    }

    /**
     *
     * @param sondeMultiDAO
     */
    public void setSondeMultiDAO(ISondeMultiDAO sondeMultiDAO) {
        this.sondeMultiDAO = sondeMultiDAO;
    }

    /**
     *
     * @param controleCoherenceDAO
     */
    public void setControleCoherenceDAO(IControleCoherenceDAO controleCoherenceDAO) {
        this.controleCoherenceDAO = controleCoherenceDAO;
    }

    /**
     *
     * @param projetSiteDAO
     */
    public void setProjetSiteDAO(IProjetSiteDAO projetSiteDAO) {
        this.projetSiteDAO = projetSiteDAO;
    }

    /**
     *
     * @param plateformeDAO
     */
    public void setPlateformeDAO(IPlateformeDAO plateformeDAO) {
        this.plateformeDAO = plateformeDAO;
    }

    /**
     *
     * @param outilsMesureDAO
     */
    public void setOutilsMesureDAO(IOutilsMesureDAO outilsMesureDAO) {
        this.outilsMesureDAO = outilsMesureDAO;
    }
}
