package org.inra.ecoinfo.glacpe.dataset.sondemulti.impl;

import java.io.File;
import java.io.PrintStream;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.glacpe.dataset.IGLACPEAggregateData;
import org.inra.ecoinfo.glacpe.dataset.VariableAggregatedDatas;
import org.inra.ecoinfo.glacpe.dataset.VariableAggregatedDatas.AggregatedData;
import org.inra.ecoinfo.glacpe.dataset.VariableAggregatedDatas.ValueAggregatedData;
import org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEAggregatedDatasOutputBuilder;
import org.inra.ecoinfo.glacpe.extraction.vo.DatasRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.DepthRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.ExtractionParametersVO;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO;
import org.inra.ecoinfo.glacpe.extraction.vo.ProjetSiteVO;
import org.inra.ecoinfo.glacpe.extraction.vo.VariableVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.IDatatypeVariableUniteGLACPEDAO;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.OutilsMesure;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;

/**
 *
 * @author ptcherniati
 */
public class SondeMultiAggregatedDatasOutputBuilder extends AbstractGLACPEAggregatedDatasOutputBuilder {

    private static final String BUNDLE_SOURCE_PATH_SONDEMULTI = "org.inra.ecoinfo.glacpe.extraction.sondemulti.messages";

    /**
     *
     */
    protected static final String SUFFIX_FILENAME_AGGREGATED = "aggregated";
    private static final String MAP_INDEX_0 = "0";
    private static final String HEADER_RAW_DATA = "PROPERTY_MSG_HEADER_AGGREGATED_DATA";
    private static final String HEADER_RAW_DATA_ALLDEPTH = "PROPERTY_MSG_HEADER_AGGREGATED_DATA_ALLDEPTH";
    private static final String TARGET_VALUE = "PROPERTY_MSG_HEADER_TARGET_VALUE";

    private static final String CODE_DATATYPE_SONDE_MULTI = "sonde_multiparametres";

    /**
     *
     */
    protected IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO;

    @SuppressWarnings("unchecked")
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatas) throws BusinessException {
        Properties propertiesVariableName = localizationManager.newProperties(Variable.NAME_ENTITY_JPA, "nom");
        List<VariableVO> selectedVariables = (List<VariableVO>) requestMetadatas.get(VariableVO.class.getSimpleName());
        DepthRequestParamVO depthRequestParamVO = (DepthRequestParamVO) requestMetadatas.get(DepthRequestParamVO.class.getSimpleName());
        DatasRequestParamVO datasRequestParamVO = (DatasRequestParamVO) requestMetadatas.get(DatasRequestParamVO.class.getSimpleName());

        List<ExtractionParametersVO> variablesExtractionParameters = datasRequestParamVO.getExtractionParameters();

        StringBuilder stringBuilder = new StringBuilder();

        if (depthRequestParamVO.getAllDepth()) {
            stringBuilder.append(String.format("%s", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_SONDEMULTI, HEADER_RAW_DATA_ALLDEPTH)));
        } else {
            stringBuilder.append(String.format("%s", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_SONDEMULTI, HEADER_RAW_DATA)));
        }

        for (ExtractionParametersVO variableExtractionParameters : variablesExtractionParameters) {
            VariableVO variableVO = new VariableVO();

            for (VariableVO variable : selectedVariables) {
                if (Utils.createCodeFromString(variableExtractionParameters.getName()).equals(variable.getCode())) {
                    variableVO = variable;
                    break;
                }
            }

            String localizedVariableName = propertiesVariableName.getProperty(variableVO.getNom());
            if (Strings.isNullOrEmpty(localizedVariableName)) {
                localizedVariableName = variableVO.getNom();
            }
            String uniteNom = "nounit";
            try {
                Unite unite = datatypeVariableUniteDAO.getUnite(CODE_DATATYPE_SONDE_MULTI, variableVO.getCode());

                if (unite != null) {
                    uniteNom = unite.getCode();
                }
            } catch (PersistenceException e) {
                throw new BusinessException(e);
            }
            if (variableExtractionParameters.getValueMin()) {
                stringBuilder.append(String.format(";\"minimum(%s)%n%s\"", localizedVariableName, uniteNom));
            }
            if (variableExtractionParameters.getValueMax()) {
                stringBuilder.append(String.format(";\"maximum(%s)%n%s\"", localizedVariableName, uniteNom));
            }

            if (!variableExtractionParameters.getTargetValues().isEmpty()) {
                for (int i = 0; i < variableExtractionParameters.getTargetValues().size(); i++) {
                    if (!variableExtractionParameters.getTargetValues().get(i).equals("-1")) {
                        stringBuilder.append(String.format(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_SONDEMULTI, TARGET_VALUE), localizedVariableName, variableExtractionParameters.getTargetValues().get(i), variableExtractionParameters
                                .getUncertainties().get(i)));
                    }
                }
            }
        }
        return stringBuilder.toString();

    }

    @SuppressWarnings({"rawtypes", "unchecked", "unused"})
    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {
        DatasRequestParamVO datasRequestParamVO = (DatasRequestParamVO) requestMetadatasMap.get(DatasRequestParamVO.class.getSimpleName());
        DepthRequestParamVO depthRequestParamVO = (DepthRequestParamVO) requestMetadatasMap.get(DepthRequestParamVO.class.getSimpleName());

        List<ExtractionParametersVO> variablesExtractionParameters = datasRequestParamVO.getExtractionParameters();

        String depthMin = depthRequestParamVO.getAllDepth() ? "" : depthRequestParamVO.getDepthMin().toString();
        String depthMax = depthRequestParamVO.getAllDepth() ? "" : depthRequestParamVO.getDepthMax().toString();

        List<IGLACPEAggregateData> valeursMesures = resultsDatasMap.get(MAP_INDEX_0);

        Properties propertiesProjetName = localizationManager.newProperties(Projet.NAME_TABLE, "nom");
        Properties propertiesPlateformeName = localizationManager.newProperties(Plateforme.TABLE_NAME, "nom");
        Properties propertiesSiteName = localizationManager.newProperties(Site.NAME_ENTITY_JPA, "nom");
        Properties propertiesOutilName = localizationManager.newProperties(OutilsMesure.TABLE_NAME, "nom");

        List<ExtractionParametersVO> variablesExtraction = Lists.newLinkedList();
        for (IGLACPEAggregateData valeurMesure : valeursMesures) {
            for (ExtractionParametersVO variableExtractionParameters : variablesExtractionParameters) {
                String variableExtractionCode = Utils.createCodeFromString(variableExtractionParameters.getName());
                if (valeurMesure.getVariable().getCode().equals(variableExtractionCode) || variableExtractionParameters.getValueMin() || variableExtractionParameters.getValueMax() || variableExtractionParameters.getTargetValues().size() > 0) {
                    if (!variablesExtraction.contains(variableExtractionParameters)) {
                        variablesExtraction.add(variableExtractionParameters);
                    }
                }
            }
        }

        List<PlateformeVO> selectedPlateformes = (List<PlateformeVO>) requestMetadatasMap.get(PlateformeVO.class.getSimpleName());
        ProjetSiteVO selectedProjet = selectedPlateformes.get(0).getProjet();
        Set<String> sitesNames = retrieveSitesCodes(selectedPlateformes);
        Map<String, File> filesMap = buildOutputsFiles(sitesNames, SUFFIX_FILENAME_AGGREGATED);
        Map<String, PrintStream> outputPrintStreamMap = buildOutputPrintStreamMap(filesMap);
        for (String siteName : outputPrintStreamMap.keySet()) {
            outputPrintStreamMap.get(siteName).println(headers);
        }

        List<VariableAggregatedDatas> variablesAggregatedDatas = buildVariablesAggregatedDatas(valeursMesures, variablesExtraction);

        for (PlateformeVO plateforme : selectedPlateformes) {
            for (ExtractionParametersVO extractionParametersVO : variablesExtraction) {
                VariableAggregatedDatas variableAggregatedDatas = null;
                for (VariableAggregatedDatas variableAggregatedData : variablesAggregatedDatas) {

                    String variableExtractionParametersCode = Utils.createCodeFromString(extractionParametersVO.getName());
                    if (Utils.createCodeFromString(variableAggregatedData.getVariableName()).equals(variableExtractionParametersCode) && variableAggregatedData.getPlateformeName().equals(plateforme.getNom())) {
                        variableAggregatedDatas = variableAggregatedData;
                        break;
                    }
                }

                AggregatedData aggregatedData = null;
                PrintStream rawDataPrintStream = outputPrintStreamMap.get(Utils.createCodeFromString(variableAggregatedDatas.getSiteName()));

                String localizedProjetName = propertiesProjetName.getProperty(selectedProjet.getNom());
                if (Strings.isNullOrEmpty(localizedProjetName)) {
                    localizedProjetName = selectedProjet.getNom();
                }

                String localizedSiteName = propertiesSiteName.getProperty(variableAggregatedDatas.getSiteName());
                if (Strings.isNullOrEmpty(localizedSiteName)) {
                    localizedSiteName = variableAggregatedDatas.getSiteName();
                }

                String localizedPlateformeName = propertiesPlateformeName.getProperty(variableAggregatedDatas.getPlateformeName());
                if (Strings.isNullOrEmpty(localizedPlateformeName)) {
                    localizedPlateformeName = variableAggregatedDatas.getPlateformeName();
                }

                String localizedOutilName = propertiesOutilName.getProperty(variableAggregatedDatas.getOutilsMesure().getNom());
                if (Strings.isNullOrEmpty(localizedOutilName)) {
                    localizedOutilName = variableAggregatedDatas.getOutilsMesure().getNom();
                }

                if (extractionParametersVO.getValueMin()) {

                    aggregatedData = variableAggregatedDatas.getMinAggregatedData();
                    List<Date> datesKeys = new LinkedList<Date>(aggregatedData.getValuesAggregatedDatasByDatesMap().keySet());
                    Collections.sort(datesKeys);
                    for (Date dateKey : datesKeys) {
                        for (ValueAggregatedData value : aggregatedData.getValuesAggregatedDatasByDatesMap().get(dateKey)) {
                            rawDataPrintStream.print(String.format("%s;%s;%s;%s;%s;%s;", localizedProjetName, localizedSiteName, localizedPlateformeName, localizedOutilName, DateUtil.getSimpleDateFormatDateLocale().format(dateKey),
                                    value.getHeure() != null ? DateUtil.getSimpleDateFormatTimeLocale().format(value.getHeure()) : ""));
                            if (depthRequestParamVO.getAllDepth()) {
                                rawDataPrintStream.print(String.format("%s;", value.getDepth()));
                            } else {
                                rawDataPrintStream.print(String.format("%s;%s;%s;", depthMin, depthMax, value.getDepth()));
                            }

                            for (ExtractionParametersVO variableExtractionParameters : variablesExtraction) {
                                String minSeparator = buildCSVSeparator(variableExtractionParameters.getValueMin());
                                String maxSeparator = buildCSVSeparator(variableExtractionParameters.getValueMax());
                                String targetSeparator = "";
                                for (String targetValue : variableExtractionParameters.getTargetValues()) {
                                    targetSeparator = targetSeparator + ";";
                                }
                                String variableExtractionParametersCode = Utils.createCodeFromString(variableExtractionParameters.getName());
                                if (Utils.createCodeFromString(variableAggregatedDatas.getVariableName()).equals(variableExtractionParametersCode)) {
                                    rawDataPrintStream.print(String.format("%s%s%s%s", value.getValue(), minSeparator, maxSeparator, targetSeparator));
                                    break;
                                } else {
                                    rawDataPrintStream.print(String.format("%s%s%s", minSeparator, maxSeparator, targetSeparator));
                                }
                            }

                            rawDataPrintStream.println();
                        }
                    }
                }

                if (extractionParametersVO.getValueMax()) {
                    aggregatedData = variableAggregatedDatas.getMaxAggregatedData();
                    List<Date> datesKeys = new LinkedList<Date>(aggregatedData.getValuesAggregatedDatasByDatesMap().keySet());
                    Collections.sort(datesKeys);
                    for (Date dateKey : datesKeys) {
                        for (ValueAggregatedData value : aggregatedData.getValuesAggregatedDatasByDatesMap().get(dateKey)) {
                            rawDataPrintStream.print(String.format("%s;%s;%s;%s;%s;%s;", localizedProjetName, localizedSiteName, localizedPlateformeName, localizedOutilName, DateUtil.getSimpleDateFormatDateLocale().format(dateKey),
                                    value.getHeure() != null ? DateUtil.getSimpleDateFormatTimeLocale().format(value.getHeure()) : ""));
                            if (depthRequestParamVO.getAllDepth()) {
                                rawDataPrintStream.print(String.format("%s;", value.getDepth()));
                            } else {
                                rawDataPrintStream.print(String.format("%s;%s;%s;", depthMin, depthMax, value.getDepth()));
                            }

                            for (ExtractionParametersVO variableExtractionParameters : variablesExtraction) {
                                String minSeparator = buildCSVSeparator(variableExtractionParameters.getValueMin());
                                String maxSeparator = buildCSVSeparator(variableExtractionParameters.getValueMax());
                                String targetSeparator = "";
                                for (String targetValue : variableExtractionParameters.getTargetValues()) {
                                    targetSeparator = targetSeparator + ";";
                                }
                                String variableExtractionParametersCode = Utils.createCodeFromString(variableExtractionParameters.getName());
                                if (Utils.createCodeFromString(variableAggregatedDatas.getVariableName()).equals(variableExtractionParametersCode)) {
                                    rawDataPrintStream.print(String.format("%s%s%s%s", minSeparator, value.getValue(), maxSeparator, targetSeparator));
                                    break;
                                } else {
                                    rawDataPrintStream.print(String.format("%s%s%s", minSeparator, maxSeparator, targetSeparator));
                                }
                            }

                            rawDataPrintStream.println();
                        }
                    }
                }

                for (String targetValue : extractionParametersVO.getTargetValues()) {
                    for (AggregatedData aggregatedTargetData : variableAggregatedDatas.getTargetAggregatedDatas()) {

                        List<Date> datesKeys = new LinkedList<Date>(aggregatedTargetData.getValuesAggregatedDatasByDatesMap().keySet());
                        Collections.sort(datesKeys);
                        for (Date dateKey : datesKeys) {
                            for (ValueAggregatedData value : aggregatedData.getValuesAggregatedDatasByDatesMap().get(dateKey)) {
                                rawDataPrintStream.print(String.format("%s;%s;%s;%s;%s;%s;", localizedProjetName, localizedSiteName, localizedPlateformeName, localizedOutilName, DateUtil.getSimpleDateFormatDateLocale().format(dateKey),
                                        value.getHeure() != null ? DateUtil.getSimpleDateFormatTimeLocale().format(value.getHeure()) : ""));
                                if (depthRequestParamVO.getAllDepth()) {
                                    rawDataPrintStream.print(String.format("%s;", value.getDepth()));
                                } else {
                                    rawDataPrintStream.print(String.format("%s;%s;%s;", depthMin, depthMax, value.getDepth()));
                                }

                                for (ExtractionParametersVO variableExtractionParameters : variablesExtraction) {
                                    String minSeparator = buildCSVSeparator(variableExtractionParameters.getValueMin());
                                    String maxSeparator = buildCSVSeparator(variableExtractionParameters.getValueMax());
                                    String targetBeforeSeparator = "";
                                    String targetAfterSeparator = "";
                                    for (String targetValueBuilder : variableExtractionParameters.getTargetValues()) {
                                        if (variableExtractionParameters.getTargetValues().indexOf(targetValueBuilder) < extractionParametersVO.getTargetValues().indexOf(targetValue)) {
                                            targetBeforeSeparator = targetBeforeSeparator + ";";
                                        } else {
                                            targetAfterSeparator = targetAfterSeparator + ";";
                                        }
                                    }
                                    String variableExtractionParametersCode = Utils.createCodeFromString(variableExtractionParameters.getName());
                                    if (Utils.createCodeFromString(variableAggregatedDatas.getVariableName()).equals(variableExtractionParametersCode)) {
                                        rawDataPrintStream.print(String.format("%s%s%s%s%s", minSeparator, maxSeparator, targetBeforeSeparator, value.getValue(), targetAfterSeparator));
                                    } else {
                                        rawDataPrintStream.print(String.format("%s%s%s%s", minSeparator, maxSeparator, targetBeforeSeparator, targetAfterSeparator));
                                    }
                                }

                                rawDataPrintStream.println();
                            }
                        }
                    }
                }
                variablesAggregatedDatas.remove(variableAggregatedDatas);
            }
        }

        closeStreams(outputPrintStreamMap);
        return filesMap;
    }

    /**
     *
     * @param valeursMesures
     * @param extractionParametersVO
     * @return
     */
    protected List<VariableAggregatedDatas> buildVariablesAggregatedDatas(List<IGLACPEAggregateData> valeursMesures, List<ExtractionParametersVO> extractionParametersVO) {
        Map<Site, Map<Plateforme, Map<Date, Map<VariableGLACPE, List<IGLACPEAggregateData>>>>> valeursMesuresReorderedByDatesVariablesAndDates = buildValeursMesuresReorderedByVariablesAndDates(valeursMesures);
        List<VariableAggregatedDatas> variablesAggregatedDatas = new LinkedList<VariableAggregatedDatas>();

        for (Site site : valeursMesuresReorderedByDatesVariablesAndDates.keySet()) {
            for (Plateforme plateforme : valeursMesuresReorderedByDatesVariablesAndDates.get(site).keySet()) {
                Map<Date, Map<VariableGLACPE, List<IGLACPEAggregateData>>> valeursMesuresReorderedByVariablesAndDates = valeursMesuresReorderedByDatesVariablesAndDates.get(site).get(plateforme);
                for (Date dateKey : valeursMesuresReorderedByVariablesAndDates.keySet()) {
                    Map<VariableGLACPE, List<IGLACPEAggregateData>> valeursMesuresDateMap = valeursMesuresReorderedByVariablesAndDates.get(dateKey);
                    for (VariableGLACPE variableKey : valeursMesuresDateMap.keySet()) {
                        VariableAggregatedDatas variableAggregatedDatas = new VariableAggregatedDatas(site.getNom(), plateforme.getNom(), variableKey.getNom());
                        ExtractionParametersVO extractionParam = new ExtractionParametersVO();
                        for (ExtractionParametersVO variableExtractionParameters : extractionParametersVO) {
                            String variableExtractionParametersCode = Utils.createCodeFromString(variableExtractionParameters.getName());
                            if (Utils.createCodeFromString(variableAggregatedDatas.getVariableName()).equals(variableExtractionParametersCode)) {
                                extractionParam = variableExtractionParameters;
                                break;
                            }
                        }
                        List<IGLACPEAggregateData> valeursMesureDates = valeursMesuresDateMap.get(variableKey);
                        sortValeursMesuresDatesByDepth(valeursMesureDates);
                        fillVariableAggregatedDatas(variableAggregatedDatas, dateKey, valeursMesureDates, extractionParam);
                        variablesAggregatedDatas.add(variableAggregatedDatas);
                    }
                }
            }
        }

        sortVariablesAggregatedDatas(variablesAggregatedDatas);
        return variablesAggregatedDatas;

    }

    /**
     *
     * @param variableAggregatedDatas
     * @param dateKey
     * @param valeursMesureDates
     * @param extractionParametersVO
     */
    protected void fillVariableAggregatedDatas(VariableAggregatedDatas variableAggregatedDatas, Date dateKey, List<IGLACPEAggregateData> valeursMesureDates, ExtractionParametersVO extractionParametersVO) {

        Map<String, IGLACPEAggregateData> approximativeTargetValues = new HashMap<String, IGLACPEAggregateData>();

        for (int i = 0; i < extractionParametersVO.getTargetValues().size(); i++) {
            AggregatedData listAggregatedDatas = variableAggregatedDatas.new AggregatedData();
            variableAggregatedDatas.getTargetAggregatedDatas().add(listAggregatedDatas);
            approximativeTargetValues.put(extractionParametersVO.getTargetValues().get(i), null);
        }

        Iterator<IGLACPEAggregateData> iterator = valeursMesureDates.iterator();
        while (iterator.hasNext()) {
            IGLACPEAggregateData valeurMesure = iterator.next();
            if (valeurMesure.getValue() != null) {
                if (valeurMesure.getOutilsMesure() != null) {
                    variableAggregatedDatas.setOutilsMesure(valeurMesure.getOutilsMesure());
                }

                if (variableAggregatedDatas.getMinAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey) == null) {
                    List<ValueAggregatedData> values = new LinkedList<ValueAggregatedData>();
                    variableAggregatedDatas.getMinAggregatedData().getValuesAggregatedDatasByDatesMap().put(dateKey, values);

                }
                if (variableAggregatedDatas.getMinAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey).isEmpty()) {
                    ValueAggregatedData valueAggregatedData = variableAggregatedDatas.new ValueAggregatedData(valeurMesure.getDepth(), valeurMesure.getValue());
                    valueAggregatedData.setHeure(valeurMesure.getHeure());
                    variableAggregatedDatas.getMinAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey).add(valueAggregatedData);
                } else if (variableAggregatedDatas.getMinAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey).get(0).getValue() != null) {
                    if (variableAggregatedDatas.getMinAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey).get(0).getValue() > valeurMesure.getValue()) {
                        variableAggregatedDatas.getMinAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey).clear();
                        ValueAggregatedData valueAggregatedData = variableAggregatedDatas.new ValueAggregatedData(valeurMesure.getDepth(), valeurMesure.getValue());
                        valueAggregatedData.setHeure(valeurMesure.getHeure());
                        variableAggregatedDatas.getMinAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey).add(valueAggregatedData);
                    } else if (Math.abs(variableAggregatedDatas.getMinAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey).get(0).getValue() - valeurMesure.getValue()) < Math.pow(10, -10)) {
                        ValueAggregatedData valueAggregatedData = variableAggregatedDatas.new ValueAggregatedData(valeurMesure.getDepth(), valeurMesure.getValue());
                        valueAggregatedData.setHeure(valeurMesure.getHeure());
                        variableAggregatedDatas.getMinAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey).add(valueAggregatedData);
                    }
                }

                if (variableAggregatedDatas.getMaxAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey) == null) {
                    List<ValueAggregatedData> values = new LinkedList<ValueAggregatedData>();
                    variableAggregatedDatas.getMaxAggregatedData().getValuesAggregatedDatasByDatesMap().put(dateKey, values);

                }
                if (variableAggregatedDatas.getMaxAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey).isEmpty()) {
                    ValueAggregatedData valueAggregatedData = variableAggregatedDatas.new ValueAggregatedData(valeurMesure.getDepth(), valeurMesure.getValue());
                    valueAggregatedData.setHeure(valeurMesure.getHeure());
                    variableAggregatedDatas.getMaxAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey).add(valueAggregatedData);
                } else if (variableAggregatedDatas.getMaxAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey).get(0).getValue() != null) {
                    if (variableAggregatedDatas.getMaxAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey).get(0).getValue() < valeurMesure.getValue()) {
                        variableAggregatedDatas.getMaxAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey).clear();
                        ValueAggregatedData valueAggregatedData = variableAggregatedDatas.new ValueAggregatedData(valeurMesure.getDepth(), valeurMesure.getValue());
                        valueAggregatedData.setHeure(valeurMesure.getHeure());
                        variableAggregatedDatas.getMaxAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey).add(valueAggregatedData);
                    } else if (Math.abs(variableAggregatedDatas.getMaxAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey).get(0).getValue() - valeurMesure.getValue()) < Math.pow(10, -10)) {
                        ValueAggregatedData valueAggregatedData = variableAggregatedDatas.new ValueAggregatedData(valeurMesure.getDepth(), valeurMesure.getValue());
                        valueAggregatedData.setHeure(valeurMesure.getHeure());
                        variableAggregatedDatas.getMaxAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey).add(valueAggregatedData);
                    }
                }

                for (int i = 0; i < extractionParametersVO.getTargetValues().size(); i++) {
                    if (valeurMesure.getValue() <= Float.parseFloat(extractionParametersVO.getTargetValues().get(i)) + Float.parseFloat(extractionParametersVO.getUncertainties().get(i))
                            && valeurMesure.getValue() >= Float.parseFloat(extractionParametersVO.getTargetValues().get(i)) - Float.parseFloat(extractionParametersVO.getUncertainties().get(i))) {
                        AggregatedData aggregatedDatas;

                        aggregatedDatas = variableAggregatedDatas.getTargetAggregatedDatas().get(i);
                        variableAggregatedDatas.getTargetAggregatedDatas().remove(i);

                        ValueAggregatedData valueAggregatedData = variableAggregatedDatas.new ValueAggregatedData(valeurMesure.getDepth(), valeurMesure.getValue());
                        valueAggregatedData.setHeure(valeurMesure.getHeure());
                        if (aggregatedDatas.getValuesAggregatedDatasByDatesMap().get(dateKey) == null) {
                            List<ValueAggregatedData> values = new LinkedList<ValueAggregatedData>();
                            aggregatedDatas.getValuesAggregatedDatasByDatesMap().put(dateKey, values);
                        }
                        aggregatedDatas.getValuesAggregatedDatasByDatesMap().get(dateKey).add(valueAggregatedData);
                        variableAggregatedDatas.getTargetAggregatedDatas().add(i, aggregatedDatas);
                    } else if (variableAggregatedDatas.getTargetAggregatedDatas().get(i) == null) {
                        if (approximativeTargetValues.get(extractionParametersVO.getTargetValues().get(i)) != null) {
                            if (Math.abs(valeurMesure.getValue() - Float.parseFloat(extractionParametersVO.getTargetValues().get(i))) < Math.abs(approximativeTargetValues.get(extractionParametersVO.getTargetValues().get(i)).getValue()
                                    - Float.parseFloat(extractionParametersVO.getTargetValues().get(i)))) {
                                approximativeTargetValues.put(extractionParametersVO.getTargetValues().get(i), valeurMesure);
                            }
                        } else {
                            approximativeTargetValues.put(extractionParametersVO.getTargetValues().get(i), valeurMesure);
                        }
                    }
                }
            }
            iterator.remove();
        }
        for (int i = 0; i < extractionParametersVO.getTargetValues().size(); i++) {
            if (variableAggregatedDatas.getTargetAggregatedDatas().get(i).getValuesAggregatedDatasByDatesMap().get(dateKey) == null) {
                List<ValueAggregatedData> values = new LinkedList<ValueAggregatedData>();
                variableAggregatedDatas.getTargetAggregatedDatas().get(i).getValuesAggregatedDatasByDatesMap().put(dateKey, values);
            }
            if (variableAggregatedDatas.getTargetAggregatedDatas().get(i).getValuesAggregatedDatasByDatesMap().get(dateKey).isEmpty() && approximativeTargetValues.get(extractionParametersVO.getTargetValues().get(i)) != null) {
                IGLACPEAggregateData valeurMesure = approximativeTargetValues.get(extractionParametersVO.getTargetValues().get(i));

                ValueAggregatedData valueAggregatedData = variableAggregatedDatas.new ValueAggregatedData(valeurMesure.getDepth(), valeurMesure.getValue());
                valueAggregatedData.setHeure(valeurMesure.getHeure());
                variableAggregatedDatas.getTargetAggregatedDatas().get(i).getValuesAggregatedDatasByDatesMap().get(dateKey).add(valueAggregatedData);
            }
        }

    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException, NoExtractionResultException {
        ((DefaultParameter) parameters).getFilesMaps().add(super.buildOutput(parameters, SondeMultiAggregatedDatasExtractor.CST_RESULT_EXTRACTION_CODE));

        return null;
    }

    /**
     *
     * @param datatypeVariableUniteDAO
     */
    public void setDatatypeVariableUniteDAO(IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO) {
        this.datatypeVariableUniteDAO = datatypeVariableUniteDAO;
    }
}
