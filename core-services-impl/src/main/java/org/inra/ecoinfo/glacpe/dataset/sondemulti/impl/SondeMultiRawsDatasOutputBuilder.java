package org.inra.ecoinfo.glacpe.dataset.sondemulti.impl;

import java.io.File;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.glacpe.dataset.sondemulti.entity.MesureSondeMulti;
import org.inra.ecoinfo.glacpe.dataset.sondemulti.entity.ValeurMesureSondeMulti;
import org.inra.ecoinfo.glacpe.extraction.vo.DepthRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO;
import org.inra.ecoinfo.glacpe.extraction.vo.VariableVO;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.OutilsMesure;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import com.google.common.base.Strings;

/**
 *
 * @author ptcherniati
 */
public class SondeMultiRawsDatasOutputBuilder extends AbstractSondeMultiRawsDatasOutputBuilder {

    private static final String HEADER_RAW_DATA = "PROPERTY_MSG_HEADER_RAW_DATA";
    private static final String HEADER_RAW_DATA_ALLDEPTH = "PROPERTY_MSG_HEADER_RAW_DATA_ALLDEPTH";
    private static final String MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS = "PROPERTY_MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS";

    private static final String CODE_DATATYPE_SONDE_MULTI = "sonde_multiparametres";

    @SuppressWarnings("unchecked")
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatas) throws BusinessException {

        ErrorsReport errorsReport = new ErrorsReport();
        Properties propertiesVariableName = localizationManager.newProperties(Variable.NAME_ENTITY_JPA, "nom");
        List<VariableVO> selectedVariables = (List<VariableVO>) requestMetadatas.get(VariableVO.class.getSimpleName());
        DepthRequestParamVO depthRequestParamVO = (DepthRequestParamVO) requestMetadatas.get(DepthRequestParamVO.class.getSimpleName());

        StringBuilder stringBuilder = new StringBuilder();
        try {
            if (depthRequestParamVO.getAllDepth()) {
                stringBuilder.append(String.format("%s", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_SONDEMULTI, HEADER_RAW_DATA_ALLDEPTH)));
            } else {
                stringBuilder.append(String.format("%s", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_SONDEMULTI, HEADER_RAW_DATA)));
            }

            for (VariableVO variableVO : selectedVariables) {
                VariableGLACPE variable = (VariableGLACPE) variableDAO.getByCode(variableVO.getCode());
                if (variable == null) {
                    errorsReport.addErrorMessage((String.format(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_SONDEMULTI, MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS), variableVO.getNom())));
                }
                String localizedVariableName = propertiesVariableName.getProperty(variable.getNom());
                if (Strings.isNullOrEmpty(localizedVariableName)) {
                    localizedVariableName = variable.getNom();
                }

                Unite unite = datatypeVariableUniteDAO.getUnite(CODE_DATATYPE_SONDE_MULTI, variable.getCode());
                String uniteNom;
                if (unite == null) {
                    uniteNom = "nounit";
                } else {
                    uniteNom = unite.getCode();
                }
                stringBuilder.append(String.format(";%s (%s)", localizedVariableName, uniteNom));
                if (errorsReport.hasErrors()) {
                    throw new PersistenceException(errorsReport.getErrorsMessages());
                }
            }
        } catch (Exception e) {
            throw new BusinessException(e);
        }
        return stringBuilder.toString();

    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {

        List<VariableVO> selectedVariables = (List<VariableVO>) requestMetadatasMap.get(VariableVO.class.getSimpleName());
        DepthRequestParamVO depthRequestParamVO = (DepthRequestParamVO) requestMetadatasMap.get(DepthRequestParamVO.class.getSimpleName());
        Iterator<MesureSondeMulti> iterator = resultsDatasMap.get(MAP_INDEX_0).iterator();

        Properties propertiesProjetName = localizationManager.newProperties(Projet.NAME_TABLE, "nom");
        Properties propertiesPlateformeName = localizationManager.newProperties(Plateforme.TABLE_NAME, "nom");
        Properties propertiesSiteName = localizationManager.newProperties(Site.NAME_ENTITY_JPA, "nom");
        Properties propertiesOutilName = localizationManager.newProperties(OutilsMesure.TABLE_NAME, "nom");

        List<PlateformeVO> selectedPlateformes = (List<PlateformeVO>) requestMetadatasMap.get(PlateformeVO.class.getSimpleName());
        Set<String> sitesNames = retrieveSitesCodes(selectedPlateformes);
        Map<String, File> filesMap = buildOutputsFiles(sitesNames, SUFFIX_FILENAME_DEFAULT);
        Map<String, PrintStream> outputPrintStreamMap = buildOutputPrintStreamMap(filesMap);
        for (String siteName : outputPrintStreamMap.keySet()) {
            outputPrintStreamMap.get(siteName).println(headers);
        }
        while (iterator.hasNext()) {
            final MesureSondeMulti mesureSondeMulti = iterator.next();

            PrintStream rawDataPrintStream = outputPrintStreamMap.get(((SiteGLACPE) mesureSondeMulti.getSite()).getCodeFromName());
            String line;

            String localizedProjetName = propertiesProjetName.getProperty(mesureSondeMulti.getSousSequence().getSequence().getProjetSite().getProjet().getNom());
            if (Strings.isNullOrEmpty(localizedProjetName)) {
                localizedProjetName = mesureSondeMulti.getSousSequence().getSequence().getProjetSite().getProjet().getNom();
            }

            String localizedSiteName = propertiesSiteName.getProperty(mesureSondeMulti.getSousSequence().getSequence().getProjetSite().getSite().getNom());
            if (Strings.isNullOrEmpty(localizedSiteName)) {
                localizedSiteName = mesureSondeMulti.getSousSequence().getSequence().getProjetSite().getSite().getNom();
            }

            String localizedPlateformeName = propertiesPlateformeName.getProperty(mesureSondeMulti.getSousSequence().getPlateforme().getNom());
            if (Strings.isNullOrEmpty(localizedPlateformeName)) {
                localizedPlateformeName = mesureSondeMulti.getSousSequence().getPlateforme().getNom();
            }

            String localizedOutilName = propertiesOutilName.getProperty(mesureSondeMulti.getSousSequence().getOutilsMesure().getNom());
            if (Strings.isNullOrEmpty(localizedOutilName)) {
                localizedOutilName = mesureSondeMulti.getSousSequence().getOutilsMesure().getNom();
            }

            if (depthRequestParamVO.getAllDepth()) {
                line = String.format("%s;%s;%s;%s;%s;%s;%s;%s", localizedProjetName, localizedSiteName, localizedPlateformeName, localizedOutilName, mesureSondeMulti.getSousSequence().getCommentaires() != null ? mesureSondeMulti.getSousSequence()
                        .getCommentaires() : "", DateUtil.getSimpleDateFormatDateLocale().format(mesureSondeMulti.getSousSequence().getSequence().getDatePrelevement()), mesureSondeMulti.getHeure() != null ? DateUtil.getSimpleDateFormatTimeLocale()
                        .format(mesureSondeMulti.getHeure()) : "", mesureSondeMulti.getProfondeur());
            } else {
                line = String.format("%s;%s;%s;%s;%s;%s;%s;%s;%s;%s", localizedProjetName, localizedSiteName, localizedPlateformeName, localizedOutilName, mesureSondeMulti.getSousSequence().getCommentaires() != null ? mesureSondeMulti
                        .getSousSequence().getCommentaires() : "", depthRequestParamVO.getDepthMin(), depthRequestParamVO.getDepthMax(),
                        DateUtil.getSimpleDateFormatDateLocale().format(mesureSondeMulti.getSousSequence().getSequence().getDatePrelevement()),
                        mesureSondeMulti.getHeure() != null ? DateUtil.getSimpleDateFormatTimeLocale().format(mesureSondeMulti.getHeure()) : "", mesureSondeMulti.getProfondeur());
            }
            rawDataPrintStream.print(line);

            Map<Long, Float> valeursMesuresSondeMulti = buildValeurs(mesureSondeMulti.getValeurs());
            for (VariableVO variableVO : selectedVariables) {
                rawDataPrintStream.print(";");
                Float valeur = valeursMesuresSondeMulti.get(variableVO.getId());
                if (valeur != null) {
                    rawDataPrintStream.print(String.format("%s", valeur));
                } else {
                    rawDataPrintStream.print("");
                }
            }
            rawDataPrintStream.println();
            // iterator.remove(); // ???? 
        }

        closeStreams(outputPrintStreamMap);
        return filesMap;
    }

    private Map<Long, Float> buildValeurs(List<ValeurMesureSondeMulti> valeurs) {
        Map<Long, Float> mapValue = new HashMap<Long, Float>();
        for (ValeurMesureSondeMulti valeur : valeurs) {
            mapValue.put(valeur.getVariable().getId(), valeur.getValeur());
        }
        return mapValue;
    }

    class ErrorsReport {

        private String errorsMessages = new String();

        public void addErrorMessage(String errorMessage) {
            errorsMessages = errorsMessages.concat("-").concat(errorMessage).concat("\n");
        }

        public String getErrorsMessages() {
            return errorsMessages;
        }

        public boolean hasErrors() {
            return (errorsMessages.length() > 0);
        }
    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException, NoExtractionResultException {
        ((DefaultParameter) parameters).getFilesMaps().add(super.buildOutput(parameters, SondeMultiRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE));

        return null;
    }

}
