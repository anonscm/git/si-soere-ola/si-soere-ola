package org.inra.ecoinfo.glacpe.dataset.sondemulti.jpa;

import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.glacpe.dataset.sondemulti.ISequenceSondeMultiDAO;
import org.inra.ecoinfo.glacpe.dataset.sondemulti.entity.SequenceSondeMulti;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;


/**
 * @author "Guillaume Enrico"
 * 
 */

public class JPASequenceSondeMultiDAO extends AbstractJPADAO<SequenceSondeMulti> implements ISequenceSondeMultiDAO {

    /**
     *
     * @param datePrelevement
     * @param projetCode
     * @param siteCode
     * @return
     * @throws PersistenceException
     */
    @Override
    public SequenceSondeMulti getByDatePrelevementAndProjetCodeAndSiteCode(Date datePrelevement, String projetCode, String siteCode) throws PersistenceException {
        try {
            Query query = null;

            String queryString = "from SequenceSondeMulti s where s.datePrelevement = :date and s.projetSite.projet.code = :projetCode and s.projetSite.site.code = :siteCode";

            query = entityManager.createQuery(queryString);
            query.setParameter("date", datePrelevement);
            query.setParameter("projetCode", projetCode);
            query.setParameter("siteCode", siteCode);

            List<?> results = query.getResultList();
            if (results != null && results.size() == 1) {
                return (SequenceSondeMulti) results.get(0);
            }

        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
        return null;
    }

}
