package org.inra.ecoinfo.glacpe.dataset.sondemulti.jpa;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.glacpe.dataset.sondemulti.ISondeMultiDAO;
import org.inra.ecoinfo.glacpe.dataset.sondemulti.entity.MesureSondeMulti;
import org.inra.ecoinfo.glacpe.dataset.sondemulti.entity.SousSequenceSondeMulti;
import org.inra.ecoinfo.glacpe.dataset.sondemulti.entity.ValeurMesureSondeMulti;
import org.inra.ecoinfo.glacpe.extraction.IDatatypeSpecifigProjectStatisticsDAO;
import org.inra.ecoinfo.glacpe.extraction.jpa.AbstractJPAProjectSpecificDatatypeStatisticsDAO;
import org.inra.ecoinfo.glacpe.extraction.vo.AbstractDatesFormParam;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesRequestParamVO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.projetsite.ProjetSite;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class JPASondeMultiDAO extends AbstractJPADAO<Object> implements ISondeMultiDAO {

    private static final String REQUEST_SONDEMULTI_AVAILABLES_PROJETS = "select distinct s.projetSite.projet from SequenceSondeMulti s";
    private static final String REQUEST_SONDEMULTI_PROJETSITE_BY_PROJET_ID_AND_SITE_ID = "select distinct s.projetSite from SequenceSondeMulti s where s.projetSite.projet.id=:projetId and s.projetSite.site.id=:siteId ";
    private static final String REQUEST_SONDEMULTI_AVAILABLES_PLATEFORMES_BY_PROJETSITE_ID = "select distinct ss.plateforme from SousSequenceSondeMulti ss where ss.sequence.projetSite.id = :projetSiteId";
    private static final String REQUEST_SONDEMULTI_AVAILABLES_VARIABLES_BY_PLATEFORMES_ID = "select distinct vm.variable from ValeurMesureSondeMulti vm where vm.mesure.sousSequence.plateforme.id in (:plateformesIds) and vm.mesure.sousSequence.sequence.datePrelevement between :firstDate and :lastDate and vm.valeur is not null order by vm.variable.ordreAffichageGroupe";

    private static final String REQUEST_AllDepth = "select m from MesureSondeMulti m where m.sousSequence.plateforme.id in (:selectedPlateformesIds) and m.sousSequence.sequence.projetSite.id in (:selectedProjetId) %s  order by m.sousSequence.sequence.projetSite.projet.nom,m.sousSequence.plateforme.site.nom,m.sousSequence.plateforme.nom,m.sousSequence.sequence.datePrelevement,m.profondeur";
    private static final String REQUEST_BetweenDepth = "select m from MesureSondeMulti m where m.sousSequence.plateforme.id in (:selectedPlateformesIds) and m.sousSequence.sequence.projetSite.id in (:selectedProjetId) and m.profondeur >= :depthMin and m.profondeur <= :depthMax %s  order by m.sousSequence.sequence.projetSite.projet.nom,m.sousSequence.plateforme.site.nom,m.sousSequence.plateforme.nom,m.sousSequence.sequence.datePrelevement,m.profondeur";
    private static final String REQUEST_UniqueDepth = "select ss from SousSequenceSondeMulti ss where ss.plateforme.id in (:selectedPlateformesIds) and ss.sequence.projetSite.id in (:selectedProjetId) %s  order by ss.sequence.projetSite.projet.nom,ss.plateforme.site.nom,ss.plateforme.nom,ss.sequence.datePrelevement";

    private static final String HQL_DATE_FIELD_1 = "vm.mesure.sousSequence.sequence.datePrelevement";
    private static final String HQL_DATE_FIELD_2 = "m.sousSequence.sequence.datePrelevement";
    private static final String HQL_DATE_FIELD_3 = "ss.sequence.datePrelevement";

    private IDatatypeSpecifigProjectStatisticsDAO datatypeStatisticsDAO;

    /**
     *
     * @return
     * @throws PersistenceException
     */
    @Override
    public List<Projet> retrieveSondeMultiAvailablesProjets() throws PersistenceException {
        try {

            Query query = entityManager.createQuery(REQUEST_SONDEMULTI_AVAILABLES_PROJETS);
            @SuppressWarnings("unchecked")
            List<Projet> projets = query.getResultList();

            return projets;
        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param projetSiteId
     * @return
     * @throws PersistenceException
     */
    @Override
    public List<Plateforme> retrieveSondeMultiAvailablesPlateformesByProjetSiteId(long projetSiteId) throws PersistenceException {
        try {

            Query query = entityManager.createQuery(REQUEST_SONDEMULTI_AVAILABLES_PLATEFORMES_BY_PROJETSITE_ID);

            query.setParameter("projetSiteId", projetSiteId);
            @SuppressWarnings("unchecked")
            List<Plateforme> plateformes = query.getResultList();

            return plateformes;
        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param plateformesIds
     * @param firstDate
     * @param lastDate
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<VariableGLACPE> retrieveSondeMultiAvailablesVariables(List<Long> plateformesIds, Date firstDate, Date lastDate) throws PersistenceException {

        List<VariableGLACPE> variables;
        try {
            Query query = entityManager.createQuery(REQUEST_SONDEMULTI_AVAILABLES_VARIABLES_BY_PLATEFORMES_ID);

            query.setParameter("plateformesIds", plateformesIds);
            query.setParameter("firstDate", firstDate);
            query.setParameter("lastDate", lastDate);
            variables = query.getResultList();
            return variables;
        } catch (Exception e) {
            AbstractJPADAO.LOGGER.error(e);
            throw new PersistenceException(e);
        }

    }

    /**
     *
     * @param selectedPlateformesIds
     * @param selectedVariablesIds
     * @param projetId
     * @param datesRequestParamVO
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<ValeurMesureSondeMulti> extractAggregatedDatasForAllDepth(List<Long> selectedPlateformesIds, List<Long> selectedVariablesIds, List<Long> projetId, DatesRequestParamVO datesRequestParamVO) throws PersistenceException {
        return (List<ValeurMesureSondeMulti>) datatypeStatisticsDAO.extractAggregatedDatasForAllDepth(selectedPlateformesIds, selectedVariablesIds, projetId, datesRequestParamVO, HQL_DATE_FIELD_1);
    }

    /**
     *
     * @param selectedPlateformesIds
     * @param projetId
     * @param datesRequestParamVO
     * @param depthMin
     * @param depthMax
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<MesureSondeMulti> extractDatasForRangeDepth(List<Long> selectedPlateformesIds, List<Long> projetId, DatesRequestParamVO datesRequestParamVO, Float depthMin, Float depthMax) throws PersistenceException {
        try {
            AbstractDatesFormParam datesFormParam = datesRequestParamVO.getCurrentDatesFormParam();
            Query query = entityManager.createQuery(String.format(REQUEST_BetweenDepth, datesFormParam.buildSQLCondition(HQL_DATE_FIELD_2)));
            Map<String, Date> datesMap = datesFormParam.retrieveParametersMap();

            for (String key : datesMap.keySet()) {
                query.setParameter(key, datesMap.get(key));
            }

            query.setParameter("selectedPlateformesIds", selectedPlateformesIds);
            query.setParameter("selectedProjetId", projetId);
            query.setParameter("depthMin", depthMin);
            query.setParameter("depthMax", depthMax);

            return query.getResultList();

        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param selectedPlateformesIds
     * @param projetId
     * @param datesRequestParamVO
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<MesureSondeMulti> extractDatasForAllDepth(List<Long> selectedPlateformesIds, List<Long> projetId, DatesRequestParamVO datesRequestParamVO) throws PersistenceException {
        try {
            AbstractDatesFormParam datesFormParam = datesRequestParamVO.getCurrentDatesFormParam();
            Query query = entityManager.createQuery(String.format(REQUEST_AllDepth, datesFormParam.buildSQLCondition(HQL_DATE_FIELD_2)));
            Map<String, Date> datesMap = datesFormParam.retrieveParametersMap();

            for (String key : datesMap.keySet()) {
                query.setParameter(key, datesMap.get(key));
            }

            query.setParameter("selectedPlateformesIds", selectedPlateformesIds);
            query.setParameter("selectedProjetId", projetId);

            return query.getResultList();

        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param datatypeStatisticsDAO
     */
    public void setDatatypeStatisticsDAO(IDatatypeSpecifigProjectStatisticsDAO datatypeStatisticsDAO) {
        this.datatypeStatisticsDAO = datatypeStatisticsDAO;
    }

    /**
     *
     * @param selectedPlateformesIds
     * @param selectedVariablesIds
     * @param projetId
     * @param datesRequestParamVO
     * @param depthMin
     * @param depthMax
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<ValeurMesureSondeMulti> extractAggregatedDatasForRangeDepth(List<Long> selectedPlateformesIds, List<Long> selectedVariablesIds, List<Long> projetId, DatesRequestParamVO datesRequestParamVO, Float depthMin, Float depthMax)
            throws PersistenceException
    {
        return (List<ValeurMesureSondeMulti>) datatypeStatisticsDAO.extractAggregatedDatasForRangeDepth(selectedPlateformesIds, selectedVariablesIds, projetId, datesRequestParamVO, depthMin, depthMax, HQL_DATE_FIELD_1);
    }

    /**
     *
     * @param projetId
     * @param siteId
     * @return
     * @throws PersistenceException
     */
    @Override
    public ProjetSite retrieveSondeMultiAvailablesProjetsSiteByProjetAndSite(Long projetId, Long siteId) throws PersistenceException {
        try {

            Query query = entityManager.createQuery(REQUEST_SONDEMULTI_PROJETSITE_BY_PROJET_ID_AND_SITE_ID);

            query.setParameter("projetId", projetId);
            query.setParameter("siteId", siteId);
            @SuppressWarnings("unchecked")
            List<ProjetSite> projetSite = query.getResultList();

            return projetSite.get(0);
        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param selectedPlateformesIds
     * @param projetId
     * @param datesRequestParamVO
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<SousSequenceSondeMulti> extractDatasForUniqueDepth(List<Long> selectedPlateformesIds, List<Long> projetId, DatesRequestParamVO datesRequestParamVO) throws PersistenceException {
        try {
            AbstractDatesFormParam datesFormParam = datesRequestParamVO.getCurrentDatesFormParam();
            Query query = entityManager.createQuery(String.format(REQUEST_UniqueDepth, datesFormParam.buildSQLCondition(HQL_DATE_FIELD_3)));
            Map<String, Date> datesMap = datesFormParam.retrieveParametersMap();

            for (String key : datesMap.keySet()) {
                query.setParameter(key, datesMap.get(key));
            }

            query.setParameter("selectedPlateformesIds", selectedPlateformesIds);
            query.setParameter("selectedProjetId", projetId);

            return query.getResultList();

        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     *
     */
    public static class JPADatatypeStatisticsDAO extends AbstractJPAProjectSpecificDatatypeStatisticsDAO {

        private static final String REQUEST_AllDepthAggregatedDatas = "select vm from ValeurMesureSondeMulti vm where vm.mesure.sousSequence.plateforme.id in (:selectedPlateformesIds) and vm.mesure.sousSequence.sequence.projetSite.id in (:selectedProjetId) and vm.variable.id in (:selectedVariablesIds) %s  order by vm.mesure.sousSequence.sequence.projetSite.projet.nom,vm.mesure.sousSequence.plateforme.site.nom,vm.mesure.sousSequence.plateforme.nom,vm.mesure.sousSequence.sequence.datePrelevement,vm.mesure.profondeur";

        private static final String REQUEST_BetweenDepthAggregatedDatas = "select vm from ValeurMesureSondeMulti vm where vm.mesure.sousSequence.plateforme.id in (:selectedPlateformesIds) and vm.mesure.sousSequence.sequence.projetSite.id in (:selectedProjetId) and vm.variable.id in (:selectedVariablesIds) and vm.mesure.profondeur >= :depthMin and vm.mesure.profondeur <= :depthMax %s  order by vm.mesure.sousSequence.sequence.projetSite.projet.nom,vm.mesure.sousSequence.plateforme.site.nom,vm.mesure.sousSequence.plateforme.nom,vm.mesure.sousSequence.sequence.datePrelevement,vm.mesure.profondeur";

        @Override
        protected String getQLRequestDatasForAllDepth() {
            return REQUEST_AllDepth;
        }

        /**
         *
         * @return
         */
        @Override
        protected String getRequestDatasForRangeDepth() {
            return REQUEST_BetweenDepth;
        }

        /**
         *
         * @return
         */
        @Override
        protected String getQLRequestAggregatedDatasForAllDepth() {
            return REQUEST_AllDepthAggregatedDatas;
        }

        /**
         *
         * @return
         */
        @Override
        protected String getQLRequestAggregatedDatasForRangeDepth() {
            return REQUEST_BetweenDepthAggregatedDatas;
        }
    }

}
