package org.inra.ecoinfo.glacpe.extraction;

import java.util.List;

import org.inra.ecoinfo.glacpe.extraction.vo.DatesRequestParamVO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public interface IDatatypeSpecifigProjectStatisticsDAO {

    /**
     *
     * @param selectedPlateformesIds
     * @param selectedVariablesIds
     * @param projetId
     * @param datesRequestParamVO
     * @param hqlDateField
     * @return
     * @throws PersistenceException
     */
    List<?> extractAggregatedDatasForAllDepth(List<Long> selectedPlateformesIds, List<Long> selectedVariablesIds, List<Long> projetId, DatesRequestParamVO datesRequestParamVO, String hqlDateField) throws PersistenceException;

    /**
     *
     * @param selectedPlateformesIds
     * @param selectedVariablesIds
     * @param projetId
     * @param datesRequestParamVO
     * @param depthMin
     * @param depthMax
     * @param hqlDateField1
     * @return
     * @throws PersistenceException
     */
    List<?> extractAggregatedDatasForRangeDepth(List<Long> selectedPlateformesIds, List<Long> selectedVariablesIds, List<Long> projetId, DatesRequestParamVO datesRequestParamVO, Float depthMin, Float depthMax, String hqlDateField1)
            throws PersistenceException;

}
