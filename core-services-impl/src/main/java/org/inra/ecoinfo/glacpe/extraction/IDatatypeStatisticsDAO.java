package org.inra.ecoinfo.glacpe.extraction;

import java.util.Date;
import java.util.List;

import org.inra.ecoinfo.glacpe.extraction.vo.DatesRequestParamVO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public interface IDatatypeStatisticsDAO {

    /**
     *
     * @param selectedSitesIds
     * @param variablePpIdSlected
     * @param allDepth
     * @param datesRequestParamVO
     * @return
     * @throws PersistenceException
     */
    List<Object> extractDatasForAllDepth(List<Long> selectedSitesIds, List<Long> variablePpIdSlected, Boolean allDepth, DatesRequestParamVO datesRequestParamVO) throws PersistenceException;

    /**
     *
     * @param selectedSitesIds
     * @param variablePpIdSlected
     * @param allDepth
     * @param depthMin
     * @param depthMax
     * @param datesRequestParamVO
     * @return
     * @throws PersistenceException
     */
    List<Object> extractDatasForRangeDepth(List<Long> selectedSitesIds, List<Long> variablePpIdSlected, Boolean allDepth, Float depthMin, Float depthMax, DatesRequestParamVO datesRequestParamVO) throws PersistenceException;

    /**
     *
     * @param selectedSitesIds
     * @param variablePpIdSlected
     * @param allDepth
     * @param datesRequestParamVO
     * @return
     * @throws PersistenceException
     */
    List<Object> extractMinDatasForAllDepth(Long selectedSitesIds, Long variablePpIdSlected, Boolean allDepth, DatesRequestParamVO datesRequestParamVO) throws PersistenceException;

    /**
     *
     * @param selectedSitesIds
     * @param variablePpIdSlected
     * @param allDepth
     * @param datesRequestParamVO
     * @return
     * @throws PersistenceException
     */
    List<Object> extractMaxDatasForAllDepth(Long selectedSitesIds, Long variablePpIdSlected, Boolean allDepth, DatesRequestParamVO datesRequestParamVO) throws PersistenceException;

    /**
     *
     * @param selectedSitesIds
     * @param variablePpIdSlected
     * @param allDepth
     * @param depthMin
     * @param depthMax
     * @param datesRequestParamVO
     * @return
     * @throws PersistenceException
     */
    List<Object> extractMinDatasForRangeDepth(Long selectedSitesIds, Long variablePpIdSlected, Boolean allDepth, Float depthMin, Float depthMax, DatesRequestParamVO datesRequestParamVO) throws PersistenceException;

    /**
     *
     * @param selectedSitesIds
     * @param variablePpIdSlected
     * @param allDepth
     * @param depthMin
     * @param depthMax
     * @param datesRequestParamVO
     * @return
     * @throws PersistenceException
     */
    List<Object> extractMaxDatasForRangeDepth(Long selectedSitesIds, Long variablePpIdSlected, Boolean allDepth, Float depthMin, Float depthMax, DatesRequestParamVO datesRequestParamVO) throws PersistenceException;

    /**
     *
     * @param selectedSiteId
     * @param variablePpIdSlected
     * @param allDepth
     * @param datesRequestParamVO
     * @return
     * @throws PersistenceException
     */
    List<Date> retrieveAvailablesDatesBySiteAllDepth(Long selectedSiteId, Long variablePpIdSlected, Boolean allDepth, DatesRequestParamVO datesRequestParamVO) throws PersistenceException;

    /**
     *
     * @param selectedSiteId
     * @param variablePpIdSlected
     * @param allDepth
     * @param availableDate
     * @param datesRequestParamVO
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("rawtypes")
    List retrieveValeursBySiteByAllDepthByVariableId(Long selectedSiteId, Long variablePpIdSlected, Boolean allDepth, Date availableDate, DatesRequestParamVO datesRequestParamVO) throws PersistenceException;

    /**
     *
     * @param selectedSiteId
     * @param variableIdSelected
     * @param allDepth
     * @param availableDate
     * @param datesRequestParamVO
     * @param profondeurMin
     * @param profondeurMax
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("rawtypes")
    List retrieveValeursBySiteByRangeDepthByVariableId(Long selectedSiteId, Long variableIdSelected, Boolean allDepth, Date availableDate, DatesRequestParamVO datesRequestParamVO, Float profondeurMin, Float profondeurMax) throws PersistenceException;

    /**
     *
     * @param selectedSiteId
     * @param variableIdSelected
     * @param allDepth
     * @param datesRequestParamVO
     * @param profondeurMin
     * @param profondeurMax
     * @return
     * @throws PersistenceException
     */
    List<Date> retrieveAvailablesDatesBySiteForRangeDepth(Long selectedSiteId, Long variableIdSelected, Boolean allDepth, DatesRequestParamVO datesRequestParamVO, Float profondeurMin, Float profondeurMax) throws PersistenceException;

}
