package org.inra.ecoinfo.glacpe.extraction.impl;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.inra.ecoinfo.extraction.config.impl.Extraction;
import org.inra.ecoinfo.extraction.impl.AbstractExtractor;
import org.inra.ecoinfo.glacpe.dataset.IGLACPEAggregateData;
import org.inra.ecoinfo.glacpe.dataset.VariableAggregatedDatas;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO;
import org.inra.ecoinfo.glacpe.extraction.vo.VariableVO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public abstract class AbstractGLACPEAggregatedDatasExtractor extends AbstractExtractor {

    /**
     *
     */
    protected static final String SUFFIX_FILENAME_AGGREGATED = "aggregated";

    @Override
    protected void prepareRequestMetadatas(Map<String, Object> requestMetadatasMap) throws BusinessException {
        sortSelectedVariables(requestMetadatasMap);
    }

    @SuppressWarnings("unchecked")
    private void sortSelectedVariables(Map<String, Object> requestMetadatasMap) {
        Collections.sort((List<VariableVO>) requestMetadatasMap.get(VariableVO.class.getSimpleName()), new Comparator<VariableVO>() {

            @Override
            public int compare(VariableVO o1, VariableVO o2) {
                if (o1.getOrdreAffichageGroupe() == null) {
                    o1.setOrdreAffichageGroupe(0);
                }
                if (o2.getOrdreAffichageGroupe() == null) {
                    o2.setOrdreAffichageGroupe(0);
                }
                if (o1.getGroupeVariable() != null && o2.getGroupeVariable() != null) {
                    return o1.getGroupeVariable().getId().toString().concat(o1.getOrdreAffichageGroupe().toString()).compareTo(o2.getGroupeVariable().getId().toString().concat(o2.getOrdreAffichageGroupe().toString()));
                } else {
                    return o1.getOrdreAffichageGroupe().toString().concat(o1.getCode()).compareTo(o2.getOrdreAffichageGroupe().toString().concat(o2.getCode()));
                }

                /*
                 * if(o1.getOrdreAffichageGroupe() != null) { if(o2.getOrdreAffichageGroupe() != null) { return
                 * o1.getGroupeVariable().getId().toString().concat(o1.getOrdreAffichageGroupe().toString()).compareTo(o2.getGroupeVariable().getId().toString().concat(o2.getOrdreAffichageGroupe().toString())); } else { return
                 * o1.getGroupeVariable().getId().toString().concat(o1.getOrdreAffichageGroupe().toString()).compareTo(o2.getGroupeVariable().getId().toString()); } } else { if(o2.getOrdreAffichageGroupe() != null) { return
                 * o1.getGroupeVariable().getId().toString().compareTo(o2.getGroupeVariable().getId().toString().concat(o2.getOrdreAffichageGroupe().toString())); } else { return
                 * o1.getGroupeVariable().getId().toString().compareTo(o2.getGroupeVariable().getId().toString()); } }
                 */
            }
        });

    }

    /**
     *
     * @param variablesAggregatedDatas
     */
    protected void sortVariablesAggregatedDatas(List<VariableAggregatedDatas> variablesAggregatedDatas) {
        Collections.sort(variablesAggregatedDatas, new Comparator<VariableAggregatedDatas>() {

            @Override
            public int compare(VariableAggregatedDatas o1, VariableAggregatedDatas o2) {
                String str1 = o1.getSiteName().concat(o1.getPlateformeName());
                String str2 = o2.getSiteName().concat(o2.getPlateformeName());
                return str1.compareTo(str2);
            }

        });
    }

    /**
     *
     * @param valeursMesureDates
     */
    protected void sortValeursMesuresDatesByDepth(List<IGLACPEAggregateData> valeursMesureDates) {
        Collections.sort(valeursMesureDates, new Comparator<IGLACPEAggregateData>() {

            @Override
            public int compare(IGLACPEAggregateData o1, IGLACPEAggregateData o2) {
                return o1.getDepth().compareTo(o2.getDepth());
            }
        });
    }

    /**
     *
     * @param valeursMesures
     * @return
     */
    protected Map<Site, Map<Plateforme, Map<VariableGLACPE, Map<Date, List<IGLACPEAggregateData>>>>> buildValeursMesuresReorderedByVariablesAndDates(List<IGLACPEAggregateData> valeursMesures) {
        Map<Site, Map<Plateforme, Map<VariableGLACPE, Map<Date, List<IGLACPEAggregateData>>>>> valeursMesuresReorderedBySitesPlateformesVariablesDates = new HashMap<Site, Map<Plateforme, Map<VariableGLACPE, Map<Date, List<IGLACPEAggregateData>>>>>();

        for (IGLACPEAggregateData valeurMesure : valeursMesures) {

            Map<Plateforme, Map<VariableGLACPE, Map<Date, List<IGLACPEAggregateData>>>> valeursMesuresReorderedByPlateformesVariablesAndDates = valeursMesuresReorderedBySitesPlateformesVariablesDates.get(valeurMesure.getSite());

            if (valeursMesuresReorderedByPlateformesVariablesAndDates == null) {
                valeursMesuresReorderedByPlateformesVariablesAndDates = new HashMap<Plateforme, Map<VariableGLACPE, Map<Date, List<IGLACPEAggregateData>>>>();
            }

            Map<VariableGLACPE, Map<Date, List<IGLACPEAggregateData>>> valeursMesuresReorderedByVariablesAndDates = valeursMesuresReorderedByPlateformesVariablesAndDates.get(valeurMesure.getPlateforme());

            if (valeursMesuresReorderedByVariablesAndDates == null) {
                valeursMesuresReorderedByVariablesAndDates = new HashMap<VariableGLACPE, Map<Date, List<IGLACPEAggregateData>>>();
            }

            Map<Date, List<IGLACPEAggregateData>> dateVmMap = valeursMesuresReorderedByVariablesAndDates.get(valeurMesure.getVariable());
            if (dateVmMap == null) {
                dateVmMap = new HashMap<Date, List<IGLACPEAggregateData>>();
            }

            List<IGLACPEAggregateData> valeursMesuresAggregated = dateVmMap.get(valeurMesure.getDate());
            if (valeursMesuresAggregated == null) {
                valeursMesuresAggregated = new LinkedList<IGLACPEAggregateData>();
            }

            valeursMesuresAggregated.add(valeurMesure);
            dateVmMap.put(valeurMesure.getDate(), valeursMesuresAggregated);
            valeursMesuresReorderedByVariablesAndDates.put(valeurMesure.getVariable(), dateVmMap);
            valeursMesuresReorderedByPlateformesVariablesAndDates.put(valeurMesure.getPlateforme(), valeursMesuresReorderedByVariablesAndDates);
            valeursMesuresReorderedBySitesPlateformesVariablesDates.put(valeurMesure.getSite(), valeursMesuresReorderedByPlateformesVariablesAndDates);

        }
        return valeursMesuresReorderedBySitesPlateformesVariablesDates;
    }

    /**
     *
     * @param selectedPlateformes
     * @return
     */
    protected Set<String> retrieveSitesCodes(List<PlateformeVO> selectedPlateformes) {
        Set<String> sitesNames = new HashSet<String>();
        for (PlateformeVO plateforme : selectedPlateformes) {
            sitesNames.add(Utils.createCodeFromString(plateforme.getNomSite()));
        }
        return sitesNames;
    }

    @Override
    public void setExtraction(Extraction extraction) {
        // TODO Auto-generated method stub

    }

    @Override
    public Extraction getExtraction() {
        // TODO Auto-generated method stub
        return null;
    }

}
