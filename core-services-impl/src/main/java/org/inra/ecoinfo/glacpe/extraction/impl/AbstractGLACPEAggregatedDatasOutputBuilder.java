package org.inra.ecoinfo.glacpe.extraction.impl;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.glacpe.dataset.IGLACPEAggregateData;
import org.inra.ecoinfo.glacpe.dataset.VariableAggregatedDatas;
import org.inra.ecoinfo.glacpe.dataset.VariableAggregatedDatas.ValueAggregatedData;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.utils.Utils;

/**
 *
 * @author ptcherniati
 */
public abstract class AbstractGLACPEAggregatedDatasOutputBuilder extends AbstractOutputBuilder {

    /**
     *
     * @param displaySeparator
     * @return
     */
    protected String buildCSVSeparator(Boolean displaySeparator) {
        String separator = "";
        if (displaySeparator) {
            separator = ";";
        }
        return separator;
    }

    /**
     * 
     * @param valeursMesures
     * @return
     */
    protected Map<String, Map<Date, Map<String, VariableAggregatedDatas>>> buildVariablesAggregatedDatas(List<IGLACPEAggregateData> valeursMesures) {
        Map<Site, Map<Plateforme, Map<Date, Map<VariableGLACPE, List<IGLACPEAggregateData>>>>> valeursMesuresReorderedByDatesVariablesAndDates = buildValeursMesuresReorderedByVariablesAndDates(valeursMesures);
        Map<String, Map<Date, Map<String, VariableAggregatedDatas>>> variablesAggregatedDatas = new HashMap<String, Map<Date, Map<String, VariableAggregatedDatas>>>();

        for (Site site : valeursMesuresReorderedByDatesVariablesAndDates.keySet()) {
            for (Plateforme plateforme : valeursMesuresReorderedByDatesVariablesAndDates.get(site).keySet()) {
                Map<Date, Map<VariableGLACPE, List<IGLACPEAggregateData>>> valeursMesuresReorderedByVariablesAndDates = valeursMesuresReorderedByDatesVariablesAndDates.get(site).get(plateforme);
                if (variablesAggregatedDatas.get(plateforme.getCode()) == null) {
                    Map<Date, Map<String, VariableAggregatedDatas>> variableAggregatedDatasBySite = new HashMap<Date, Map<String, VariableAggregatedDatas>>();
                    variablesAggregatedDatas.put(plateforme.getCode(), variableAggregatedDatasBySite);
                }
                for (Date dateKey : valeursMesuresReorderedByVariablesAndDates.keySet()) {
                    Map<VariableGLACPE, List<IGLACPEAggregateData>> valeursMesuresDateMap = valeursMesuresReorderedByVariablesAndDates.get(dateKey);
                    if (variablesAggregatedDatas.get(plateforme.getCode()).get(dateKey) == null) {
                        Map<String, VariableAggregatedDatas> mapVariableAggregated = new HashMap<String, VariableAggregatedDatas>();
                        variablesAggregatedDatas.get(plateforme.getCode()).put(dateKey, mapVariableAggregated);
                    }
                    for (VariableGLACPE variableKey : valeursMesuresDateMap.keySet()) {
                        VariableAggregatedDatas variableAggregatedDatas = new VariableAggregatedDatas(site.getNom(), plateforme.getNom(), variableKey.getNom(), variableKey.getOrdreAffichageGroupe());
                        List<IGLACPEAggregateData> valeursMesureDates = valeursMesuresDateMap.get(variableKey);
                        sortValeursMesuresDatesByDepth(valeursMesureDates);
                        fillVariableAggregatedDatas(variableAggregatedDatas, dateKey, valeursMesureDates);
                        variablesAggregatedDatas.get(plateforme.getCode()).get(dateKey).put(variableKey.getCode(), variableAggregatedDatas);
                    }
                }
            }
        }

        return variablesAggregatedDatas;

    }

    /**
     *
     * @param variablesAggregatedDatas
     */
    protected void sortVariablesAggregatedDatas(List<VariableAggregatedDatas> variablesAggregatedDatas) {
        Collections.sort(variablesAggregatedDatas, new Comparator<VariableAggregatedDatas>() {

            @Override
            public int compare(VariableAggregatedDatas o1, VariableAggregatedDatas o2) {                
                String str1 = o1.getSiteName().concat(o1.getPlateformeName());
                String str2 = o2.getSiteName().concat(o2.getPlateformeName());
                return str1.compareTo(str2);
            }

        });
    }

    /**
     *
     * @param valeursMesureDates
     */
    protected void sortValeursMesuresDatesByDepth(List<IGLACPEAggregateData> valeursMesureDates) {
        Collections.sort(valeursMesureDates, new Comparator<IGLACPEAggregateData>() {

            @Override
            public int compare(IGLACPEAggregateData o1, IGLACPEAggregateData o2) {
                return o1.getDepth().compareTo(o2.getDepth());
            }

        });
    }

    /**
     *
     * @param variableAggregatedDatas
     * @param dateKey
     * @param valeursMesureDates
     */
    protected void fillVariableAggregatedDatas(VariableAggregatedDatas variableAggregatedDatas, Date dateKey, List<IGLACPEAggregateData> valeursMesureDates) {

        Iterator<IGLACPEAggregateData> iterator = valeursMesureDates.iterator();
        while (iterator.hasNext()) {
            IGLACPEAggregateData valeurMesure = iterator.next();
            if (valeurMesure.getValue() != null) {
                if (variableAggregatedDatas.getMinAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey) == null) {
                    List<ValueAggregatedData> variables = new LinkedList<ValueAggregatedData>();
                    variables.add(variableAggregatedDatas.new ValueAggregatedData(valeurMesure.getDepth(), valeurMesure.getValue()));
                    variableAggregatedDatas.getMinAggregatedData().getValuesAggregatedDatasByDatesMap().put(dateKey, variables);
                    if (variableAggregatedDatas.getOutilsPrelevement() == null && valeurMesure.getOutilsPrelevement() != null) {
                        variableAggregatedDatas.setOutilsPrelevement(valeurMesure.getOutilsPrelevement());
                    }
                    if (variableAggregatedDatas.getOutilsMesure() == null && valeurMesure.getOutilsMesure() != null) {
                        variableAggregatedDatas.setOutilsMesure(valeurMesure.getOutilsMesure());
                    }
                } else if (variableAggregatedDatas.getMinAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey).get(0).getValue() > valeurMesure.getValue()) {
                    variableAggregatedDatas.getMinAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey).clear();
                    variableAggregatedDatas.getMinAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey).add(variableAggregatedDatas.new ValueAggregatedData(valeurMesure.getDepth(), valeurMesure.getValue()));
                    if (variableAggregatedDatas.getOutilsPrelevement() == null && valeurMesure.getOutilsPrelevement() != null) {
                        variableAggregatedDatas.setOutilsPrelevement(valeurMesure.getOutilsPrelevement());
                    }
                    if (variableAggregatedDatas.getOutilsMesure() == null && valeurMesure.getOutilsMesure() != null) {
                        variableAggregatedDatas.setOutilsMesure(valeurMesure.getOutilsMesure());
                    }
                } else if (Math.abs(variableAggregatedDatas.getMinAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey).get(0).getValue() - valeurMesure.getValue()) < Math.pow(10, -10)) {
                    variableAggregatedDatas.getMinAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey).add(variableAggregatedDatas.new ValueAggregatedData(valeurMesure.getDepth(), valeurMesure.getValue()));
                    if (variableAggregatedDatas.getOutilsPrelevement() == null && valeurMesure.getOutilsPrelevement() != null) {
                        variableAggregatedDatas.setOutilsPrelevement(valeurMesure.getOutilsPrelevement());
                    }
                    if (variableAggregatedDatas.getOutilsMesure() == null && valeurMesure.getOutilsMesure() != null) {
                        variableAggregatedDatas.setOutilsMesure(valeurMesure.getOutilsMesure());
                    }
                }

                if (variableAggregatedDatas.getMaxAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey) == null) {
                    List<ValueAggregatedData> variables = new LinkedList<ValueAggregatedData>();
                    variables.add(variableAggregatedDatas.new ValueAggregatedData(valeurMesure.getDepth(), valeurMesure.getValue()));
                    variableAggregatedDatas.getMaxAggregatedData().getValuesAggregatedDatasByDatesMap().put(dateKey, variables);
                    if (variableAggregatedDatas.getOutilsPrelevement() == null && valeurMesure.getOutilsPrelevement() != null) {
                        variableAggregatedDatas.setOutilsPrelevement(valeurMesure.getOutilsPrelevement());
                    }
                    if (variableAggregatedDatas.getOutilsMesure() == null && valeurMesure.getOutilsMesure() != null) {
                        variableAggregatedDatas.setOutilsMesure(valeurMesure.getOutilsMesure());
                    }
                } else if (variableAggregatedDatas.getMaxAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey).get(0).getValue() < valeurMesure.getValue()) {
                    variableAggregatedDatas.getMaxAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey).clear();
                    variableAggregatedDatas.getMaxAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey).add(variableAggregatedDatas.new ValueAggregatedData(valeurMesure.getDepth(), valeurMesure.getValue()));
                    if (variableAggregatedDatas.getOutilsPrelevement() == null && valeurMesure.getOutilsPrelevement() != null) {
                        variableAggregatedDatas.setOutilsPrelevement(valeurMesure.getOutilsPrelevement());
                    }
                    if (variableAggregatedDatas.getOutilsMesure() == null && valeurMesure.getOutilsMesure() != null) {
                        variableAggregatedDatas.setOutilsMesure(valeurMesure.getOutilsMesure());
                    }
                } else if (Math.abs(variableAggregatedDatas.getMaxAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey).get(0).getValue() - valeurMesure.getValue()) < Math.pow(10, -10)) {
                    variableAggregatedDatas.getMaxAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey).add(variableAggregatedDatas.new ValueAggregatedData(valeurMesure.getDepth(), valeurMesure.getValue()));
                    if (variableAggregatedDatas.getOutilsPrelevement() == null && valeurMesure.getOutilsPrelevement() != null) {
                        variableAggregatedDatas.setOutilsPrelevement(valeurMesure.getOutilsPrelevement());
                    }
                    if (variableAggregatedDatas.getOutilsMesure() == null && valeurMesure.getOutilsMesure() != null) {
                        variableAggregatedDatas.setOutilsMesure(valeurMesure.getOutilsMesure());
                    }
                }
            }
        }
    }

    /**
     *
     * @param valeursMesures
     * @return
     */
    protected Map<Site, Map<Plateforme, Map<Date, Map<VariableGLACPE, List<IGLACPEAggregateData>>>>> buildValeursMesuresReorderedByVariablesAndDates(List<IGLACPEAggregateData> valeursMesures) {
        Map<Site, Map<Plateforme, Map<Date, Map<VariableGLACPE, List<IGLACPEAggregateData>>>>> valeursMesuresReorderedBySitesPlateformesVariablesDates = new HashMap<Site, Map<Plateforme, Map<Date, Map<VariableGLACPE, List<IGLACPEAggregateData>>>>>();

        for (IGLACPEAggregateData valeurMesure : valeursMesures) {

            Map<Plateforme, Map<Date, Map<VariableGLACPE, List<IGLACPEAggregateData>>>> valeursMesuresReorderedByPlateformesVariablesAndDates = valeursMesuresReorderedBySitesPlateformesVariablesDates.get(valeurMesure.getSite());

            if (valeursMesuresReorderedByPlateformesVariablesAndDates == null) {
                valeursMesuresReorderedByPlateformesVariablesAndDates = new HashMap<Plateforme, Map<Date, Map<VariableGLACPE, List<IGLACPEAggregateData>>>>();
            }

            Map<Date, Map<VariableGLACPE, List<IGLACPEAggregateData>>> valeursMesuresReorderedByVariablesAndDates = valeursMesuresReorderedByPlateformesVariablesAndDates.get(valeurMesure.getPlateforme());

            if (valeursMesuresReorderedByVariablesAndDates == null) {
                valeursMesuresReorderedByVariablesAndDates = new HashMap<Date, Map<VariableGLACPE, List<IGLACPEAggregateData>>>();
            }

            Map<VariableGLACPE, List<IGLACPEAggregateData>> dateVmMap = valeursMesuresReorderedByVariablesAndDates.get(valeurMesure.getDate());
            if (dateVmMap == null) {
                dateVmMap = new HashMap<VariableGLACPE, List<IGLACPEAggregateData>>();
            }

            List<IGLACPEAggregateData> valeursMesuresAggregated = dateVmMap.get(valeurMesure.getVariable());
            if (valeursMesuresAggregated == null) {
                valeursMesuresAggregated = new LinkedList<IGLACPEAggregateData>();
            }

            valeursMesuresAggregated.add(valeurMesure);
            dateVmMap.put(valeurMesure.getVariable(), valeursMesuresAggregated);
            valeursMesuresReorderedByVariablesAndDates.put(valeurMesure.getDate(), dateVmMap);
            valeursMesuresReorderedByPlateformesVariablesAndDates.put(valeurMesure.getPlateforme(), valeursMesuresReorderedByVariablesAndDates);
            valeursMesuresReorderedBySitesPlateformesVariablesDates.put(valeurMesure.getSite(), valeursMesuresReorderedByPlateformesVariablesAndDates);

        }
        return valeursMesuresReorderedBySitesPlateformesVariablesDates;
    }

    /**
     *
     * @param selectedPlateformes
     * @return
     */
    protected Set<String> retrieveSitesCodes(List<PlateformeVO> selectedPlateformes) {
        Set<String> sitesNames = new HashSet<String>();
        for (PlateformeVO plateforme : selectedPlateformes) {
            sitesNames.add(Utils.createCodeFromString(plateforme.getNomSite()));
        }
        return sitesNames;
    }
}
