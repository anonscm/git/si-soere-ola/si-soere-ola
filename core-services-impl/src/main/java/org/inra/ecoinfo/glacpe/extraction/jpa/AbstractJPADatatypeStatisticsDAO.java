package org.inra.ecoinfo.glacpe.extraction.jpa;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.glacpe.extraction.IDatatypeStatisticsDAO;
import org.inra.ecoinfo.glacpe.extraction.vo.AbstractDatesFormParam;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesYearsContinuousFormParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesYearsDiscretsFormParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesYearsRangeFormParamVO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public abstract class AbstractJPADatatypeStatisticsDAO extends AbstractJPADAO<Object> implements IDatatypeStatisticsDAO {

    /**
     *
     */
    protected static final String EMPTY_STRING = "";
    private static final String HQL_DATE_FIELD = "s.date";

    /**
     *
     * @param datesRequestParamVO
     * @param allDepth
     * @param min
     * @param max
     * @param average
     * @return
     * @throws ParseException
     */
    protected Query buildQueryFromDateTemplate(DatesRequestParamVO datesRequestParamVO, Boolean allDepth, Boolean min, Boolean max, Boolean average) throws ParseException {

        if (datesRequestParamVO.getSelectedFormSelection().equals(DatesYearsDiscretsFormParamVO.LABEL)) {
            return buildQuery(datesRequestParamVO.getDatesYearsDiscretsFormParam(), allDepth, min, max, average);
        } else if (datesRequestParamVO.getSelectedFormSelection().equals(DatesYearsRangeFormParamVO.LABEL)) {
            return buildQuery(datesRequestParamVO.getDatesYearsRangeFormParam(), allDepth, min, max, average);
        } else if (datesRequestParamVO.getSelectedFormSelection().equals(DatesYearsContinuousFormParamVO.LABEL)) {
            return buildQuery(datesRequestParamVO.getDatesYearsContinuousFormParam(), allDepth, min, max, average);
        }
        return null;
    }

    /**
     *
     * @return
     */
    abstract protected String getQLRequestDatasForAllDepth();

    /**
     *
     * @return
     */
    abstract protected String getQLRequestMinDatasForAllDepth();

    /**
     *
     * @return
     */
    abstract protected String getQLRequestMaxDatasForAllDepth();

    /**
     *
     * @return
     */
    abstract protected String getQLRequestAvailablesDatesDatasForAllDepth();

    /**
     *
     * @return
     */
    abstract protected String getQLRequestDatasForAvailablesDatesAndAllDepth();

    /**
     *
     * @return
     */
    abstract protected String getQLRequestAvailablesDatesDatasForRangeDepth();

    /**
     *
     * @return
     */
    abstract protected String getQLRequestDatasForAvailablesDatesAndRangeDepth();

    /**
     *
     * @return
     */
    abstract protected String getRequestDatasForRangeDepth();

    /**
     *
     * @return
     */
    abstract protected String getRequestMinDatasForRangeDepth();

    /**
     *
     * @return
     */
    abstract protected String getRequestMaxDatasForRangeDepth();

    /**
     *
     * @param datesFormParam
     * @param allDepth
     * @param min
     * @param max
     * @param average
     * @return
     * @throws ParseException
     */
    protected Query buildQuery(AbstractDatesFormParam datesFormParam, Boolean allDepth, Boolean min, Boolean max, Boolean average) throws ParseException {

        Query query = null;

        String queryString = null;

        // Pour une date donnée
        if (!datesFormParam.isEmpty()) {
            // Pour toute les profondeurs
            if (allDepth) {
                if (min) {
                    queryString = String.format(getQLRequestMinDatasForAllDepth(), datesFormParam.buildSQLCondition(HQL_DATE_FIELD));

                } else if (max) {
                    queryString = String.format(getQLRequestMaxDatasForAllDepth(), datesFormParam.buildSQLCondition(HQL_DATE_FIELD));

                } else if (average) {
                    queryString = String.format(getQLRequestDatasForAvailablesDatesAndAllDepth(), datesFormParam.buildSQLCondition(HQL_DATE_FIELD));
                } else {
                    queryString = String.format(getQLRequestDatasForAllDepth(), datesFormParam.buildSQLCondition(HQL_DATE_FIELD));

                }

                // Pour un intervalle de pronfondeur
            } else {
                if (min) {
                    queryString = String.format(getRequestMinDatasForRangeDepth(), datesFormParam.buildSQLCondition(HQL_DATE_FIELD));

                } else if (max) {
                    queryString = String.format(getRequestMaxDatasForRangeDepth(), datesFormParam.buildSQLCondition(HQL_DATE_FIELD));

                } else if (average) {
                    queryString = String.format(getQLRequestDatasForAvailablesDatesAndRangeDepth(), datesFormParam.buildSQLCondition(HQL_DATE_FIELD));
                } else {
                    queryString = String.format(getRequestDatasForRangeDepth(), datesFormParam.buildSQLCondition(HQL_DATE_FIELD));
                }
            }

            // Pour toutes les dates
        } else {
            // Pour totues les profondeurs
            if (allDepth) {
                if (min) {
                    queryString = String.format(getQLRequestMinDatasForAllDepth(), EMPTY_STRING);

                } else if (max) {
                    queryString = String.format(getQLRequestMaxDatasForAllDepth(), EMPTY_STRING);

                } else {
                    queryString = String.format(getQLRequestDatasForAllDepth(), EMPTY_STRING);
                }

                // Pour un interval de pronfondeur
            } else {
                if (min) {
                    queryString = String.format(getRequestMinDatasForRangeDepth(), EMPTY_STRING);

                } else if (max) {
                    queryString = String.format(getRequestMaxDatasForRangeDepth(), EMPTY_STRING);

                } else {
                    queryString = String.format(getRequestDatasForRangeDepth(), EMPTY_STRING);
                }
            }
        }

        query = entityManager.createQuery(queryString);
        Map<String, Date> datesMap = datesFormParam.retrieveParametersMap();
        for (String key : datesMap.keySet()) {
            query.setParameter(key, datesMap.get(key));
        }
        return query;
    }

    /**
     *
     * @param selectedSiteId
     * @param variableIdSelected
     * @param allDepth
     * @param datesRequestParamVO
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Date> retrieveAvailablesDatesBySiteAllDepth(Long selectedSiteId, Long variableIdSelected, Boolean allDepth, DatesRequestParamVO datesRequestParamVO) throws PersistenceException {
        try {

            Query query = null;
            AbstractDatesFormParam datesFormParam = null;
            if (datesRequestParamVO.getSelectedFormSelection().equals(DatesYearsDiscretsFormParamVO.LABEL)) {
                datesFormParam = datesRequestParamVO.getDatesYearsDiscretsFormParam();
                query = entityManager.createQuery(String.format(getQLRequestAvailablesDatesDatasForAllDepth(), datesFormParam.buildSQLCondition(HQL_DATE_FIELD)));
            } else if (datesRequestParamVO.getSelectedFormSelection().equals(DatesYearsRangeFormParamVO.LABEL)) {
                datesFormParam = datesRequestParamVO.getDatesYearsRangeFormParam();
                query = entityManager.createQuery(String.format(getQLRequestAvailablesDatesDatasForAllDepth(), datesFormParam.buildSQLCondition(HQL_DATE_FIELD)));
            } else if (datesRequestParamVO.getSelectedFormSelection().equals(DatesYearsContinuousFormParamVO.LABEL)) {
                datesFormParam = datesRequestParamVO.getDatesYearsContinuousFormParam();
                query = entityManager.createQuery(String.format(getQLRequestAvailablesDatesDatasForAllDepth(), datesFormParam.buildSQLCondition(HQL_DATE_FIELD)));
            }

            query.setParameter("siteId", selectedSiteId);
            query.setParameter("variableIdSelected", variableIdSelected);

            Map<String, Date> datesMap = datesFormParam.retrieveParametersMap();
            for (String key : datesMap.keySet()) {
                query.setParameter(key, datesMap.get(key));
            }

            return query.getResultList();

        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param selectedSiteId
     * @param variableIdSelected
     * @param allDepth
     * @param datesRequestParamVO
     * @param profondeurMin
     * @param profondeurMax
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Date> retrieveAvailablesDatesBySiteForRangeDepth(Long selectedSiteId, Long variableIdSelected, Boolean allDepth, DatesRequestParamVO datesRequestParamVO, Float profondeurMin, Float profondeurMax) throws PersistenceException {
        try {

            Query query = null;
            AbstractDatesFormParam datesFormParam = null;
            if (datesRequestParamVO.getSelectedFormSelection().equals(DatesYearsDiscretsFormParamVO.LABEL)) {
                datesFormParam = datesRequestParamVO.getDatesYearsDiscretsFormParam();
                query = entityManager.createQuery(String.format(getQLRequestAvailablesDatesDatasForRangeDepth(), datesFormParam.buildSQLCondition(HQL_DATE_FIELD)));
            } else if (datesRequestParamVO.getSelectedFormSelection().equals(DatesYearsRangeFormParamVO.LABEL)) {
                datesFormParam = datesRequestParamVO.getDatesYearsRangeFormParam();
                query = entityManager.createQuery(String.format(getQLRequestAvailablesDatesDatasForRangeDepth(), datesFormParam.buildSQLCondition(HQL_DATE_FIELD)));
            } else if (datesRequestParamVO.getSelectedFormSelection().equals(DatesYearsContinuousFormParamVO.LABEL)) {
                datesFormParam = datesRequestParamVO.getDatesYearsContinuousFormParam();
                query = entityManager.createQuery(String.format(getQLRequestAvailablesDatesDatasForRangeDepth(), datesFormParam.buildSQLCondition(HQL_DATE_FIELD)));
            }

            query.setParameter("siteId", selectedSiteId);
            query.setParameter("variableIdSelected", variableIdSelected);
            query.setParameter("depthMin", profondeurMin);
            query.setParameter("depthMax", profondeurMax);

            Map<String, Date> datesMap = datesFormParam.retrieveParametersMap();
            for (String key : datesMap.keySet()) {
                query.setParameter(key, datesMap.get(key));
            }

            return query.getResultList();

        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param selectedSitesIds
     * @param variableIdSelected
     * @param allDepth
     * @param datesRequestParamVO
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Object> extractDatasForAllDepth(List<Long> selectedSitesIds, List<Long> variableIdSelected, Boolean allDepth, DatesRequestParamVO datesRequestParamVO) throws PersistenceException {

        try {
            Query query = buildQueryFromDateTemplate(datesRequestParamVO, allDepth, false, false, false);

            query.setParameter("sitesIds", selectedSitesIds);
            query.setParameter("variableIdSelected", variableIdSelected);

            return query.getResultList();

        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param selectedSitesIds
     * @param variableIdSelected
     * @param allDepth
     * @param depthMin
     * @param depthMax
     * @param datesRequestParamVO
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Object> extractDatasForRangeDepth(List<Long> selectedSitesIds, List<Long> variableIdSelected, Boolean allDepth, Float depthMin, Float depthMax, DatesRequestParamVO datesRequestParamVO) throws PersistenceException {

        try {
            Query query = buildQueryFromDateTemplate(datesRequestParamVO, allDepth, false, false, false);

            query.setParameter("sitesIds", selectedSitesIds);
            query.setParameter("depthMin", depthMin);
            query.setParameter("depthMax", depthMax);
            query.setParameter("variableIdSelected", variableIdSelected);

            return query.getResultList();

        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param selectedSitesIds
     * @param variableIdSelected
     * @param allDepth
     * @param datesRequestParamVO
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Object> extractMinDatasForAllDepth(Long selectedSitesIds, Long variableIdSelected, Boolean allDepth, DatesRequestParamVO datesRequestParamVO) throws PersistenceException {

        try {
            Query query = buildQueryFromDateTemplate(datesRequestParamVO, allDepth, true, false, false);

            query.setParameter("sitesIds", selectedSitesIds);
            query.setParameter("variableIdSelected", variableIdSelected);
            query.setParameter("sitesIdsMin", selectedSitesIds);
            query.setParameter("variableIdSelectedMin", variableIdSelected);

            return query.getResultList();

        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }

    }

    /**
     *
     * @param selectedSitesIds
     * @param variableIdSelected
     * @param allDepth
     * @param datesRequestParamVO
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Object> extractMaxDatasForAllDepth(Long selectedSitesIds, Long variableIdSelected, Boolean allDepth, DatesRequestParamVO datesRequestParamVO) throws PersistenceException {

        try {
            Query query = buildQueryFromDateTemplate(datesRequestParamVO, allDepth, false, true, false);

            query.setParameter("sitesIds", selectedSitesIds);
            query.setParameter("variableIdSelected", variableIdSelected);
            query.setParameter("sitesIdsMax", selectedSitesIds);
            query.setParameter("variableIdSelectedMax", variableIdSelected);

            return query.getResultList();

        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }

    }

    /**
     *
     * @param selectedSitesIds
     * @param variableIdSelected
     * @param allDepth
     * @param depthMin
     * @param depthMax
     * @param datesRequestParamVO
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Object> extractMinDatasForRangeDepth(Long selectedSitesIds, Long variableIdSelected, Boolean allDepth, Float depthMin, Float depthMax, DatesRequestParamVO datesRequestParamVO) throws PersistenceException {

        try {
            Query query = buildQueryFromDateTemplate(datesRequestParamVO, allDepth, true, false, false);

            query.setParameter("sitesIds", selectedSitesIds);
            query.setParameter("depthMin", depthMin);
            query.setParameter("depthMax", depthMax);
            query.setParameter("variableIdSelected", variableIdSelected);
            query.setParameter("sitesIdsMin", selectedSitesIds);
            query.setParameter("depthMinMin", depthMin);
            query.setParameter("depthMaxMax", depthMax);
            query.setParameter("variableIdSelectedMin", variableIdSelected);

            return query.getResultList();

        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }

    }

    /**
     *
     * @param selectedSitesIds
     * @param variableIdSelected
     * @param allDepth
     * @param depthMin
     * @param depthMax
     * @param datesRequestParamVO
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Object> extractMaxDatasForRangeDepth(Long selectedSitesIds, Long variableIdSelected, Boolean allDepth, Float depthMin, Float depthMax, DatesRequestParamVO datesRequestParamVO) throws PersistenceException {

        try {
            Query query = buildQueryFromDateTemplate(datesRequestParamVO, allDepth, false, true, false);

            query.setParameter("sitesIds", selectedSitesIds);
            query.setParameter("depthMin", depthMin);
            query.setParameter("depthMax", depthMax);
            query.setParameter("variableIdSelected", variableIdSelected);
            query.setParameter("sitesIdsMax", selectedSitesIds);
            query.setParameter("depthMinMin", depthMin);
            query.setParameter("depthMaxMax", depthMax);
            query.setParameter("variableIdSelectedMax", variableIdSelected);

            return query.getResultList();

        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }

    }

    /**
     *
     * @param selectedSiteId
     * @param variableIdSelected
     * @param allDepth
     * @param availableDate
     * @param datesRequestParamVO
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("rawtypes")
    @Override
    public List retrieveValeursBySiteByAllDepthByVariableId(Long selectedSiteId, Long variableIdSelected, Boolean allDepth, Date availableDate, DatesRequestParamVO datesRequestParamVO) throws PersistenceException {
        try {
            Query query = buildQueryFromDateTemplate(datesRequestParamVO, allDepth, false, false, true);

            query.setParameter("siteId", selectedSiteId);
            query.setParameter("variableIdSelected", variableIdSelected);
            query.setParameter("date", availableDate);

            return query.getResultList();

        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param selectedSiteId
     * @param variableIdSelected
     * @param allDepth
     * @param availableDate
     * @param datesRequestParamVO
     * @param profondeurMin
     * @param profondeurMax
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("rawtypes")
    @Override
    public List retrieveValeursBySiteByRangeDepthByVariableId(Long selectedSiteId, Long variableIdSelected, Boolean allDepth, Date availableDate, DatesRequestParamVO datesRequestParamVO, Float profondeurMin, Float profondeurMax)
            throws PersistenceException
    {
        try {
            Query query = buildQueryFromDateTemplate(datesRequestParamVO, allDepth, false, false, true);

            query.setParameter("siteId", selectedSiteId);
            query.setParameter("variableIdSelected", variableIdSelected);
            query.setParameter("date", availableDate);
            query.setParameter("depthMin", profondeurMin);
            query.setParameter("depthMax", profondeurMax);

            return query.getResultList();

        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

}
