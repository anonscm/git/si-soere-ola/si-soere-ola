package org.inra.ecoinfo.glacpe.extraction.jpa;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.glacpe.extraction.IDatatypeSpecifigProjectStatisticsDAO;
import org.inra.ecoinfo.glacpe.extraction.vo.AbstractDatesFormParam;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesRequestParamVO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public abstract class AbstractJPAProjectSpecificDatatypeStatisticsDAO extends AbstractJPADAO<Object> implements IDatatypeSpecifigProjectStatisticsDAO {

    /**
     *
     */
    protected static final String EMPTY_STRING = "";

    /**
     *
     * @param datesFormParam
     * @param allDepth
     * @param hqlDateField
     * @return
     * @throws ParseException
     */
    protected Query buildQuery(AbstractDatesFormParam datesFormParam, Boolean allDepth, String hqlDateField) throws ParseException {

        Query query = null;

        String queryString = null;

        // Pour une date donnée
        if (!datesFormParam.isEmpty()) {
            queryString = buildQueryString(datesFormParam, allDepth, hqlDateField);
            // Pour toutes les dates
        } else {
            queryString = buildQueryString(datesFormParam, allDepth, EMPTY_STRING);
        }

        query = entityManager.createQuery(queryString);
        Map<String, Date> datesMap = datesFormParam.retrieveParametersMap();
        for (String key : datesMap.keySet()) {
            query.setParameter(key, datesMap.get(key));
        }
        return query;
    }

    private String buildQueryString(AbstractDatesFormParam datesFormParam, Boolean allDepth, String hqlDateField) throws ParseException {
        String queryString;
        // Pour toute les profondeurs
        if (allDepth) {
            queryString = String.format(getQLRequestAggregatedDatasForAllDepth(), datesFormParam.buildSQLCondition(hqlDateField));
        }
        // Pour un interval de profondeur
        else {
            queryString = String.format(getQLRequestAggregatedDatasForRangeDepth(), datesFormParam.buildSQLCondition(hqlDateField));
        }
        return queryString;
    }

    /**
     *
     * @return
     */
    abstract protected String getQLRequestAggregatedDatasForAllDepth();

    /**
     *
     * @return
     */
    abstract protected String getQLRequestAggregatedDatasForRangeDepth();

    /**
     * Retourne la requête permettant d'extraire les données pour toutes les profondeurs
     * 
     * @return la requête permettant d'extraire les données pour toutes les profondeurs
     */
    abstract protected String getQLRequestDatasForAllDepth();

    /**
     *
     * @return
     */
    abstract protected String getRequestDatasForRangeDepth();

    /**
     *
     * @param selectedPlateformesIds
     * @param selectedVariablesIds
     * @param projetId
     * @param datesRequestParamVO
     * @param hqlDateField
     * @return
     * @throws PersistenceException
     */
    @Override
    public List<?> extractAggregatedDatasForAllDepth(List<Long> selectedPlateformesIds, List<Long> selectedVariablesIds, List<Long> projetId, DatesRequestParamVO datesRequestParamVO, String hqlDateField) throws PersistenceException {
        try {
            Query query = buildQuery(datesRequestParamVO.getCurrentDatesFormParam(), true, hqlDateField);

            query.setParameter("selectedPlateformesIds", selectedPlateformesIds);
            query.setParameter("selectedVariablesIds", selectedVariablesIds);
            query.setParameter("selectedProjetId", projetId);

            return query.getResultList();

        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }

    }

    /**
     *
     * @param selectedPlateformesIds
     * @param selectedVariablesIds
     * @param projetId
     * @param datesRequestParamVO
     * @param depthMin
     * @param depthMax
     * @param hqlDateField
     * @return
     * @throws PersistenceException
     */
    @Override
    public List<?> extractAggregatedDatasForRangeDepth(List<Long> selectedPlateformesIds, List<Long> selectedVariablesIds, List<Long> projetId, DatesRequestParamVO datesRequestParamVO, Float depthMin, Float depthMax, String hqlDateField)
            throws PersistenceException
    {
        try {
            Query query = buildQuery(datesRequestParamVO.getCurrentDatesFormParam(), false, hqlDateField);

            query.setParameter("selectedPlateformesIds", selectedPlateformesIds);
            query.setParameter("selectedVariablesIds", selectedVariablesIds);
            query.setParameter("selectedProjetId", projetId);
            query.setParameter("depthMin", depthMin);
            query.setParameter("depthMax", depthMax);

            return query.getResultList();

        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }

    }

}
