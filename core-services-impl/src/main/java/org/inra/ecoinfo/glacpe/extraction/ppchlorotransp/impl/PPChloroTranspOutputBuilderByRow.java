package org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.impl;

import org.inra.ecoinfo.extraction.IOutputBuilder;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public class PPChloroTranspOutputBuilderByRow extends PPChloroTranspOutputsBuildersResolver {

    private IOutputBuilder ppOutputBuilderByRow;
    private IOutputBuilder chloroOutputBuilderByRow;
    private IOutputBuilder transpOutputBuilderByRow;

    /**
     *
     */
    public PPChloroTranspOutputBuilderByRow() {
        super();
    }

    @Override
    public RObuildZipOutputStream buildOutput(final IParameter parameters) throws BusinessException, NoExtractionResultException {
        super.setPpRawsDatasOutputBuilder(ppOutputBuilderByRow);
        super.setChloroRawsDatasOutputBuilder(chloroOutputBuilderByRow);
        super.setTranspRawsDatasOutputBuilder(transpOutputBuilderByRow);
        return super.buildOutput(parameters);
    }

    /**
     *
     * @param ppOutputBuilderByRow
     */
    public void setPpOutputBuilderByRow(IOutputBuilder ppOutputBuilderByRow) {
        this.ppOutputBuilderByRow = ppOutputBuilderByRow;
    }

    /**
     *
     * @param chloroOutputBuilderByRow
     */
    public void setChloroOutputBuilderByRow(IOutputBuilder chloroOutputBuilderByRow) {
        this.chloroOutputBuilderByRow = chloroOutputBuilderByRow;
    }

    /**
     *
     * @param transpOutputBuilderByRow
     */
    public void setTranspOutputBuilderByRow(IOutputBuilder transpOutputBuilderByRow) {
        this.transpOutputBuilderByRow = transpOutputBuilderByRow;
    }
}
