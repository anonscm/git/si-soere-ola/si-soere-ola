package org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.glacpe.extraction.vo.DatasRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.DepthRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO;
import org.inra.ecoinfo.glacpe.extraction.vo.VariableVO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public class PPChloroTranspRequestReminderOutputBuilder extends AbstractOutputBuilder {

    /**
     *
     */
    public static String CST_RESULT_EXTRACTION_CODE = "ppRequestReminder";

    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.glacpe.extraction.messages";

    private static final String MSG_EXTRACTION_COMMENTS = "PROPERTY_MSG_EXTRACTION_COMMENTS";

    private static final String MSG_HEADER = "PROPERTY_MSG_HEADER";

    private static final String PATTERN_STRING_PLATEFORMS_SUMMARY = "   %s (%s: %s)";
    private static final String PATTERN_STRING_VARIABLES_SUMMARY = "   %s";
    private static final String PATTERN_STRING_HEADER_PLATEFORMS_SUMMARY = "%s\n   %s";
    private static final String PATTERN_STRING_COMMENTS_SUMMARY = "   %s";

    private static final String MSG_SELECTED_PERIODS = "PROPERTY_MSG_SELECTED_PERIODS";
    private static final String MSG_SELECTED_VARIABLES = "PROPERTY_MSG_SELECTED_VARIABLES";
    private static final String MSG_SITE = "PROPERTY_MSG_SITE";
    private static final String MSG_SELECTED_PROJECT = "PROPERTY_MSG_SELECTED_PROJECT";
    private static final String MSG_SELECTED_PLATEFORMS = "PROPERTY_MSG_SELECTED_PLATEFORMS";

    private static final String KEYMAP_COMMENTS = "comments";

    @Override
    protected String buildHeader(Map<String, Object> requestMetadatas) throws BusinessException {
        return getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_HEADER);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {

        Map<String, File> reminderMap = new HashMap<String, File>();
        File reminderFile = buildOutputFile(FILENAME_REMINDER, EXTENSION_TXT);
        PrintStream reminderPrintStream;

        try {

            reminderPrintStream = new PrintStream(reminderFile, CHARACTER_ENCODING_ISO_8859_1);
            reminderPrintStream.println(headers);
            reminderPrintStream.println();

            printPlateformesSummary((List<PlateformeVO>) requestMetadatasMap.get(PlateformeVO.class.getSimpleName()), reminderPrintStream);
            Map<String, List<VariableVO>> selectedVariablesList = (Map<String, List<VariableVO>>) requestMetadatasMap.get(VariableVO.class.getSimpleName());
            List<VariableVO> selectedVariables = (List<VariableVO>) selectedVariablesList.get(PPChloroTranspParameter.PRODUCTION_PRIMAIRE);
            if (selectedVariables != null && !selectedVariables.isEmpty()) {
                printVariablesSummary(selectedVariables, reminderPrintStream, "Production primaire");
            }
            selectedVariables = (List<VariableVO>) selectedVariablesList.get(PPChloroTranspParameter.CHLOROPHYLLE);
            if (selectedVariables != null && !selectedVariables.isEmpty()) {
                printVariablesSummary(selectedVariables, reminderPrintStream, "Chlorophylle");
            }
            selectedVariables = (List<VariableVO>) selectedVariablesList.get(PPChloroTranspParameter.TRANSPARENCE);
            if (selectedVariables != null && !selectedVariables.isEmpty()) {
                printVariablesSummary(selectedVariables, reminderPrintStream, "Transparence");
            }
            ((DepthRequestParamVO) requestMetadatasMap.get(DepthRequestParamVO.class.getSimpleName())).buildSummary(reminderPrintStream);
            ((DatasRequestParamVO) requestMetadatasMap.get(DatasRequestParamVO.class.getSimpleName())).buildSummary(reminderPrintStream);
            printDatesSummary((DatesRequestParamVO) requestMetadatasMap.get(DatesRequestParamVO.class.getSimpleName()), reminderPrintStream);

            reminderPrintStream.println(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_EXTRACTION_COMMENTS));
            reminderPrintStream.println(String.format(PATTERN_STRING_COMMENTS_SUMMARY, requestMetadatasMap.get(KEYMAP_COMMENTS)));

        } catch (FileNotFoundException e) {
            AbstractOutputBuilder.LOGGER.error(e);
            throw new BusinessException(e);
        } catch (UnsupportedEncodingException e) {
            AbstractOutputBuilder.LOGGER.error(e);
            throw new BusinessException(e);
        }
        reminderMap.put(FILENAME_REMINDER, reminderFile);

        reminderPrintStream.flush();
        reminderPrintStream.close();

        return reminderMap;
    }

    private void printDatesSummary(DatesRequestParamVO datesRequestParamVO, PrintStream reminderPrintStream) throws BusinessException {
        reminderPrintStream.println(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_SELECTED_PERIODS));
        try {
            datesRequestParamVO.getCurrentDatesFormParam().buildSummary(reminderPrintStream);
        } catch (ParseException e) {
            AbstractOutputBuilder.LOGGER.error(e);
            throw new BusinessException(e);
        }

        reminderPrintStream.println();
    }

    private void printPlateformesSummary(List<PlateformeVO> list, PrintStream reminderPrintStream) {
        Properties propertiesNomProjet = getLocalizationManager().newProperties(Projet.NAME_TABLE, "nom");
        Properties propertiesNomPlateforme = getLocalizationManager().newProperties(Plateforme.TABLE_NAME, "nom");
        Properties propertiesNomSite = getLocalizationManager().newProperties(Site.NAME_ENTITY_JPA, "nom");
        reminderPrintStream.println(String.format(PATTERN_STRING_HEADER_PLATEFORMS_SUMMARY, getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_SELECTED_PROJECT),
                propertiesNomProjet.getProperty(list.get(0).getProjet().getNom(), list.get(0).getProjet().getNom())));
        reminderPrintStream.println();
        reminderPrintStream.println(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_SELECTED_PLATEFORMS));
        for (PlateformeVO plateforme : list) {
            reminderPrintStream.println(String.format(PATTERN_STRING_PLATEFORMS_SUMMARY, propertiesNomPlateforme.getProperty(plateforme.getNom(), plateforme.getNom()), getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_SITE),
                    propertiesNomSite.getProperty(plateforme.getNomSite(), plateforme.getNomSite())));
        }
        reminderPrintStream.println();
    }

    private void printVariablesSummary(List<VariableVO> list, PrintStream reminderPrintStream, String datatype) {
        Properties propertiesNomVariable = getLocalizationManager().newProperties(Variable.NAME_ENTITY_JPA, "nom");
        Properties propertiesNomDatatype = getLocalizationManager().newProperties(DataType.NAME_ENTITY_JPA, "name");
        String datatypeName = propertiesNomDatatype.getProperty(datatype, datatype);
        if (datatypeName == null)
            datatypeName = "Transparency";
        reminderPrintStream.println(String.format(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_SELECTED_VARIABLES), datatypeName));
        for (VariableVO variable : list) {
            reminderPrintStream.println(String.format(PATTERN_STRING_VARIABLES_SUMMARY, propertiesNomVariable.getProperty(variable.getNom(), variable.getNom())));
        }
        reminderPrintStream.println();
    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException, NoExtractionResultException {
       
        //ficher recap
        ((DefaultParameter) parameters).getFilesMaps().add(super.buildOutput(parameters, CST_RESULT_EXTRACTION_CODE));
   
        return null;
    }
}
