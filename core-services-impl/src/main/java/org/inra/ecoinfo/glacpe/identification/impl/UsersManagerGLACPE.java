package org.inra.ecoinfo.glacpe.identification.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import org.inra.ecoinfo.glacpe.identification.entity.RightRequestGLACPE;
import org.inra.ecoinfo.identification.impl.DefaultUsersManager;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public class UsersManagerGLACPE extends DefaultUsersManager {

    private static final String BUNDLE_NAME_GLACPE = "org.inra.ecoinfo.glacpe.identification.jsf.requestRights";
    /**
     * The Constant PROPERTY_MSG_RIGHT_DEMAND.
     */
    private static final String PROPERTY_MSG_RIGHT_DEMAND = "PROPERTY_MSG_RIGHT_DEMAND";
    /**
     * The Constant PROPERTY_MSG_NEW_RIGHT_REQUEST_MAIL_CONTENT.
     */
    private static final String PROPERTY_MSG_NEW_RIGHT_REQUEST_MAIL_CONTENT_HEAD = "PROPERTY_MSG_NEW_RIGHT_REQUEST_MAIL_CONTENT_HEAD";
    /**
     * The Constant PROPERTY_MSG_NEW_RIGHT_REQUEST_MAIL_CONTENT.
     */
    private static final String PROPERTY_MSG_NEW_RIGHT_REQUEST_MAIL_CONTENT_BODY = "PROPERTY_MSG_NEW_RIGHT_REQUEST_MAIL_CONTENT_BODY";
    /**
     * The Constant PROPERTY_MSG_NEW_RIGHT_REQUEST_MAIL_CONTENT.
     */
    private static final String PROPERTY_MSG_NEW_RIGHT_REQUEST_MAIL_CONTENT_FOOT = "PROPERTY_MSG_NEW_RIGHT_REQUEST_MAIL_CONTENT_FOOT";
    /**
     * The Constant PROPERTY_MSG_NEW_RIGHT_REQUEST
     */
    private static final String PROPERTY_MSG_NEW_RIGHT_REQUEST = "PROPERTY_MSG_NEW_RIGHT_REQUEST";
    /**
     * The Constant PROPERTY_MSG_NEW_RIGHT_REQUEST_BODY
     */
    private static final String PROPERTY_MSG_NEW_RIGHT_REQUEST_BODY = "PROPERTY_MSG_NEW_RIGHT_REQUEST_BODY";
    /**
     * The Constant PROPERTY_MSG_NEW_RIGHT_REQUEST_HEADER
     */
    private static final String PROPERTY_MSG_NEW_RIGHT_REQUEST_HEADER = "PROPERTY_MSG_NEW_RIGHT_REQUEST_HEADER";

    private static final String PARAM_MSG_REQUEST = "PARAM_MSG_REQUEST";
    private static final String PARAM_MSG_ENCADRANT = "PARAM_MSG_ENCADRANT";
    private static final String PARAM_MSG_ADRESS = "PARAM_MSG_ADRESS";
    private static final String PARAM_MSG_PHONE = "PARAM_MSG_PHONE";
    private static final String PARAM_MSG_LOCAL_SCIENTIST = "PARAM_MSG_LOCAL_SCIENTIST";
    private static final String PARAM_MSG_VARIABLES = "PARAM_MSG_VARIABLES";
    private static final String PARAM_MSG_SITES = "PARAM_MSG_SITES";
    private static final String PARAM_MSG_DATES = "PARAM_MSG_DATES";
    private static final String PARAM_MSG_DATE_REQUEST = "PARAM_MSG_DATE_REQUEST";

    /**
     * The Constant EXTENSION_TXT @link(String).
     */
    protected static final String EXTENSION_TXT = ".txt";
    /**
     * The Constant EXTRACTION @link(String).
     */
    protected static final String REQUEST = "request";
    /**
     * The Constant FILE_SEPARATOR @link(String).
     */
    protected static final String FILE_SEPARATOR = System.getProperty("file.separator");
    /**
     * The Constant FILE_SEPARATOR @link(String).
     */
    protected static final String LINE_SEPARATOR = System.getProperty("line.separator");
    /**
     * The Constant SEPARATOR_TEXT @link(String).
     */
    protected static final String SEPARATOR_TEXT = "_";
    /**
     * The Constant FILENAME_REMINDER @link(String).
     */
    protected static final String FILENAME_REMINDER = "recapitulatif_requete";

    /**
     * <p>write the requestReminder file from <i>rightRequest </i>
     * @param rightRequest
     * @return
     * @throws BusinessException 
     */
    private String buildFileFromRequest(RightRequestGLACPE rightRequest) throws BusinessException {
        File recap = buildOutputFile(FILENAME_REMINDER, EXTENSION_TXT, rightRequest.getUser().getLogin(), rightRequest.getDateRequest());
        PrintStream requestPrintStream;
        try {
            requestPrintStream = new PrintStream(recap, CHARACTER_ENCODING_ISO_8859_1);
            requestPrintStream.println(String.format(localizationManager.getMessage(BUNDLE_NAME_GLACPE, UsersManagerGLACPE.PROPERTY_MSG_NEW_RIGHT_REQUEST_HEADER), rightRequest.getUser().getPrenom(), rightRequest.getUser().getNom(), rightRequest
                    .getUser().getEmploi(), rightRequest.getUser().getPoste()));
            requestPrintStream.println();
            requestPrintStream.println(localizationManager.getMessage(BUNDLE_NAME_GLACPE, UsersManagerGLACPE.PARAM_MSG_REQUEST));
            requestPrintStream.println(rightRequest.getRequest());
            requestPrintStream.println(localizationManager.getMessage(BUNDLE_NAME_GLACPE, UsersManagerGLACPE.PARAM_MSG_ENCADRANT));
            requestPrintStream.println(rightRequest.getMotivation());
            requestPrintStream.println(localizationManager.getMessage(BUNDLE_NAME_GLACPE, UsersManagerGLACPE.PARAM_MSG_ADRESS));
            requestPrintStream.println(rightRequest.getInfos());
            requestPrintStream.println(localizationManager.getMessage(BUNDLE_NAME_GLACPE, UsersManagerGLACPE.PARAM_MSG_PHONE));
            requestPrintStream.println(rightRequest.getTelephone());
            requestPrintStream.println(localizationManager.getMessage(BUNDLE_NAME_GLACPE, UsersManagerGLACPE.PARAM_MSG_LOCAL_SCIENTIST));
            requestPrintStream.println(rightRequest.getReference());
            requestPrintStream.println();
            requestPrintStream.println(localizationManager.getMessage(BUNDLE_NAME_GLACPE, UsersManagerGLACPE.PARAM_MSG_VARIABLES));
            requestPrintStream.println(rightRequest.getVariables());
            requestPrintStream.println(localizationManager.getMessage(BUNDLE_NAME_GLACPE, UsersManagerGLACPE.PARAM_MSG_SITES));
            requestPrintStream.println(rightRequest.getSites());
            requestPrintStream.println(localizationManager.getMessage(BUNDLE_NAME_GLACPE, UsersManagerGLACPE.PARAM_MSG_DATES));
            requestPrintStream.println(rightRequest.getDates());
            requestPrintStream.println(localizationManager.getMessage(BUNDLE_NAME_GLACPE, UsersManagerGLACPE.PARAM_MSG_DATE_REQUEST));
            requestPrintStream.println(DateUtil.getSimpleDateFormatDateLocale().format(rightRequest.getDateRequest()));
        } catch (UnsupportedEncodingException e) {
            throw new BusinessException(e);
        } catch (FileNotFoundException e) {
            throw new BusinessException(e);
        }
        requestPrintStream.flush();
        requestPrintStream.close();
        return recap.getPath();
    }

    /**
     * Builds the output file.
     *
     * @param filename the filename
     * @param extension the extension
     * @param dateRequest
     * @return the file
     * @link(String) the filename
     * @link(String) the extension
     */
    protected File buildOutputFile(final String filename, final String extension, final String userLogin, final Date dateRequest) {
        final String extractionPath = getConfiguration().getRepositoryURI().concat(FILE_SEPARATOR).concat(REQUEST).concat(FILE_SEPARATOR);
        final String fileSuffix = userLogin.concat(SEPARATOR_TEXT).concat(DateUtil.getSimpleDateFormatDateLocaleForFile().format(dateRequest));
        return new File(extractionPath.concat(filename).concat(SEPARATOR_TEXT).concat(fileSuffix).concat(extension));
    }

}
