package org.inra.ecoinfo.glacpe.logging;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * @author "Guillaume Enrico"
 */
@WebServlet(value = "/downloadLogs", name = "Servlet download logs")
public class ServletDownloadLogs extends HttpServlet {

    private static final long serialVersionUID = 1L;

    private static final String filePathName = "/../../logs/activity.csv";
    private static final String fileName = "si_production.csv";

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletDownloadLogs() {
        super();
    }

    /**
     * @throws java.io.IOException
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    @SuppressWarnings("resource")
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String filePath = this.getServletContext().getRealPath("");
        File file = new File(filePath.concat(filePathName));
        byte[] datas = new byte[(int) file.length()];
        try {
            FileInputStream fis = new FileInputStream(file);

            fis.read(datas, 0, (int) file.length());

            response.setContentType("application/text");
            response.setContentLength(datas.length);
            response.setHeader("Content-Disposition", String.format("attachment; filename=%s", fileName));
            response.getOutputStream().write(datas);
            response.getOutputStream().flush();
            response.getOutputStream().close();
        } catch (FileNotFoundException e) {
            throw new IOException(e);
        }
    }

    /**
     * @throws java.io.IOException
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
