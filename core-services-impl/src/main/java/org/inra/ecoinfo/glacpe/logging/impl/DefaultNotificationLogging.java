package org.inra.ecoinfo.glacpe.logging.impl;

import java.io.IOException;

import javax.servlet.ServletContext;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.RollingFileAppender;
import org.aspectj.lang.JoinPoint.StaticPart;
import org.inra.ecoinfo.notifications.entity.Notification;
import org.inra.ecoinfo.utils.LoggerObject;
import org.springframework.web.context.ServletContextAware;

/**
 *
 * @author ptcherniati
 */
public class DefaultNotificationLogging implements ServletContextAware {

    private static final Logger logger = Logger.getLogger("notification");
    private String pathContext;

    /**
     *
     * @param staticPart
     * @param result
     */
    public void logAddNotificationExit(StaticPart staticPart, Object result) {
        logger.info(new LoggerObject((Notification) ((org.springframework.aop.aspectj.MethodInvocationProceedingJoinPoint) staticPart).getArgs()[0]));
    }

    @Override
    public void setServletContext(final ServletContext servletContext) {
        pathContext = servletContext.getRealPath("");

        String pathLogs = pathContext.concat("/../../logs/activity.csv");

        logger.setLevel(Level.INFO);
        PatternLayout layout = new PatternLayout("%m%n");
        try {
            RollingFileAppender fileAppender = new RollingFileAppender(layout, pathLogs);
            fileAppender.setMaxFileSize("1000Kb");
            fileAppender.setMaxBackupIndex(10);
            logger.addAppender(fileAppender);
        } catch (IOException e) {
            // TODO
        }
    }
}