package org.inra.ecoinfo.glacpe.logging.impl;

import java.io.IOException;

import javax.servlet.ServletContext;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.RollingFileAppender;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.JoinPoint.StaticPart;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.security.ISecurityContext;
import org.inra.ecoinfo.utils.LoggerObject;
import org.springframework.web.context.ServletContextAware;

/**
 *
 * @author ptcherniati
 */
public class DefaultSecurityContextLogging implements ServletContextAware {

    private static final Logger logger = Logger.getLogger("auth");
    private String pathContext;

    /**
     *
     * @param staticPart
     * @param result
     */
    public void logCheckPasswordExit(StaticPart staticPart, Object result) {
        StackTraceElement[] sts = new Throwable().getStackTrace();
        for (int i = 0; i < sts.length; i++) {
            if (sts[i].getClassName().contains("UIBeanIdentification") || sts[i].getClassName().contains("UIBeanChangePasswordLost")) {
                logger.info(new LoggerObject(((Utilisateur) result).getLogin(), null, "s'est connecté"));
            }
        }
    }

    /**
     *
     * @param joinPoint
     */
    public void logSecurityContextBefore(JoinPoint joinPoint) {
        logger.info(new LoggerObject(((ISecurityContext) joinPoint.getTarget()).getUtilisateur().getLogin(), null, "s'est déconnecté"));
    }

    @Override
    public void setServletContext(final ServletContext servletContext) {
        pathContext = servletContext.getRealPath("");

        String pathLogs = pathContext.concat("/../../logs/activity.csv");

        logger.setLevel(Level.INFO);
        PatternLayout layout = new PatternLayout("%m%n");
        try {
            RollingFileAppender fileAppender = new RollingFileAppender(layout, pathLogs);
            fileAppender.setMaxFileSize("1000Kb");
            fileAppender.setMaxBackupIndex(10);
            logger.addAppender(fileAppender);
        } catch (IOException e) {
            // TODO
        }
    }
}