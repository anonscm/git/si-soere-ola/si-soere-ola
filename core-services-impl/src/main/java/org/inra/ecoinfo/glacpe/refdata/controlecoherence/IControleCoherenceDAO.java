package org.inra.ecoinfo.glacpe.refdata.controlecoherence;

import java.util.Map;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public interface IControleCoherenceDAO extends IDAO<ControleCoherence> {

    /**
     *
     * @param createCodeFromString
     * @param variableCode
     * @return
     */
    ControleCoherence getBySiteCodeAndVariableCode(String createCodeFromString, String variableCode);

    /**
     *
     * @param site
     * @param datatypeVariableUnite
     * @return
     */
    ControleCoherence getBySiteAndDatatypeVariableUnite(SiteGLACPE site, DatatypeVariableUniteGLACPE datatypeVariableUnite);

    /**
     *
     * @param site
     * @param datatypeCode
     * @return
     * @throws PersistenceException
     */
    Map<String, ControleCoherence> getControleCoherenceBySitecodeAndDatatypecode(String site, String datatypeCode) throws PersistenceException;

    /**
     *
     * @param datatypeVariableUnite
     * @return
     */
    ControleCoherence getByNullSiteAndDatatypeVariableUnite(DatatypeVariableUniteGLACPE datatypeVariableUnite);

    /**
     *
     * @param datatypeCode
     * @return
     * @throws PersistenceException
     */
    Map<String, ControleCoherence> getControleCoherenceBySitecodeAndDatatypecode(String datatypeCode) throws PersistenceException;
}
