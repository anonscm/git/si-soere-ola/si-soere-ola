package org.inra.ecoinfo.glacpe.refdata.controlecoherence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import com.google.common.base.Strings;

/**
 *
 * @author ptcherniati
 */
public class JPAControleCoherenceDAO extends AbstractJPADAO<ControleCoherence> implements IControleCoherenceDAO {

    private static final String QUERY_BY_SITE_AND_DATATYPE_VARIABLE_UNITE = "from ControleCoherence where site=:site and datatypeVariableUnite=:datatypeVariableUnite";
    private static final String QUERY_BY_NULL_SITE_AND_DATATYPE_VARIABLE_UNITE = "from ControleCoherence where site is null and datatypeVariableUnite=:datatypeVariableUnite";
    private static final String QUERY_HQL_GET_BY_SITE_CODE_AND_DATATYPE_CODE = "from ControleCoherence cc where cc.site.code = :siteCode and cc.datatypeVariableUnite.datatype.code = :datatypeCode";
    private static final String QUERY_HQL_GET_BY_NULL_SITE_CODE_AND_DATATYPE_CODE = "from ControleCoherence cc where cc.site.code is null and cc.datatypeVariableUnite.datatype.code = :datatypeCode";

    /**
     *
     * @param createCodeFromString
     * @param variableCode
     * @return
     */
    @Override
    public ControleCoherence getBySiteCodeAndVariableCode(String createCodeFromString, String variableCode) {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     *
     * @param site
     * @param datatypeVariableUnite
     * @return
     */
    @Override
    public ControleCoherence getBySiteAndDatatypeVariableUnite(SiteGLACPE site, DatatypeVariableUniteGLACPE datatypeVariableUnite) {
        try {
            Query query = entityManager.createQuery(site == null ? QUERY_BY_NULL_SITE_AND_DATATYPE_VARIABLE_UNITE : QUERY_BY_SITE_AND_DATATYPE_VARIABLE_UNITE);
            if (site != null)
                query.setParameter("site", site);
            query.setParameter("datatypeVariableUnite", datatypeVariableUnite);
            return (ControleCoherence) query.getSingleResult();
        } catch (Exception e) {
            return getByNullSiteAndDatatypeVariableUnite(datatypeVariableUnite);
        }
    }

    /**
     *
     * @param datatypeVariableUnite
     * @return
     */
    @Override
    public ControleCoherence getByNullSiteAndDatatypeVariableUnite(DatatypeVariableUniteGLACPE datatypeVariableUnite) {
        try {
            Query query = entityManager.createQuery(QUERY_BY_NULL_SITE_AND_DATATYPE_VARIABLE_UNITE);
            query.setParameter("datatypeVariableUnite", datatypeVariableUnite);
            return (ControleCoherence) query.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

    /**
     *
     * @param siteCode
     * @param datatypeCode
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("unchecked")
    @Override
    public Map<String, ControleCoherence> getControleCoherenceBySitecodeAndDatatypecode(String siteCode, String datatypeCode) throws PersistenceException {
        Map<String, ControleCoherence> results = new HashMap<String, ControleCoherence>();
        try {
            if (Strings.isNullOrEmpty(siteCode))
                return getControleCoherenceBySitecodeAndDatatypecode(datatypeCode);
            String queryString = QUERY_HQL_GET_BY_SITE_CODE_AND_DATATYPE_CODE;
            Query query = entityManager.createQuery(queryString);
            query.setParameter("datatypeCode", datatypeCode);
            query.setParameter("siteCode", siteCode);
            List<ControleCoherence> controlesCoherence = query.getResultList();
            for (ControleCoherence controleCoherence : controlesCoherence) {
                results.put(controleCoherence.getDatatypeVariableUnite().getVariable().getCode(), controleCoherence);
            }
        } catch (NoResultException e) {
            return getControleCoherenceBySitecodeAndDatatypecode(datatypeCode);
        }
        return results;
    }

    /**
     *
     * @param datatypeCode
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("unchecked")
    @Override
    public Map<String, ControleCoherence> getControleCoherenceBySitecodeAndDatatypecode(String datatypeCode) throws PersistenceException {
        Map<String, ControleCoherence> results = new HashMap<String, ControleCoherence>();
        try {
            String queryString = QUERY_HQL_GET_BY_NULL_SITE_CODE_AND_DATATYPE_CODE;
            Query query = entityManager.createQuery(queryString);
            query.setParameter("datatypeCode", datatypeCode);
            List<ControleCoherence> controlesCoherence = query.getResultList();
            for (ControleCoherence controleCoherence : controlesCoherence) {
                results.put(controleCoherence.getDatatypeVariableUnite().getVariable().getCode(), controleCoherence);
            }

        } catch (NoResultException e) {
            return results;
        }
        return results;
    }

}
