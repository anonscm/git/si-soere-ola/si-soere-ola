/**
 * OREILacs project - see LICENCE.txt for use created: 7 avr. 2009 16:17:33
 */
package org.inra.ecoinfo.glacpe.refdata.controlecoherence;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.IDatatypeVariableUniteGLACPEDAO;
import org.inra.ecoinfo.glacpe.refdata.projetsitethemedatatype.IProjetSiteThemeDatatypeDAO;
import org.inra.ecoinfo.glacpe.refdata.projetsitethemedatatype.ProjetSiteThemeDatatype;
import org.inra.ecoinfo.glacpe.refdata.site.ISiteGLACPEDAO;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.variable.IVariableGLACPEDAO;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.AbstractRecurentObject;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.datatype.IDatatypeDAO;
import org.inra.ecoinfo.refdata.datatypevariableunite.DatatypeVariableUnite;
import org.inra.ecoinfo.refdata.node.ILeafTreeNode;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import com.Ostermiller.util.CSVParser;
import com.google.common.base.Strings;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public class Recorder extends AbstractCSVMetadataRecorder<ControleCoherence> {

    private static final String BUNDLE_NAME = "org.inra.ecoinfo.glacpe.refdata.messages";

    /**
     *
     */
    public final static String PROPERTY_MSG_MISSING_VARIABLE = "PROPERTY_MSG_MISSING_VARIABLE";

    /**
     *
     */
    public final static String PROPERTY_MSG_MISSING_DATATYPE = "PROPERTY_MSG_MISSING_DATATYPE";

    /**
     *
     */
    public final static String PROPERTY_MSG_BAD_VARIABLE = "PROPERTY_MSG_BAD_VARIABLE";

    /**
     *
     */
    public final static String PROPERTY_MSG_BAD_DATATYPE = "PROPERTY_MSG_BAD_DATATYPE";

    /**
     *
     */
    public final static String PROPERTY_MSG_BAD_SITE = "PROPERTY_MSG_BAD_SITE";

    /**
     *
     */
    public final static String PROPERTY_MSG_BAD_MIN_VALUE = "PROPERTY_MSG_BAD_MIN_VALUE";

    /**
     *
     */
    public final static String PROPERTY_MSG_BAD_MAX_VALUE = "PROPERTY_MSG_BAD_MAX_VALUE";

    /**
     *
     */
    public final static String PROPERTY_MSG_BAD_DATATYPE_VARIABLE_UNITE = "PROPERTY_MSG_BAD_DATATYPE_VARIABLE_UNITE";

    /**
     *
     */
    public final static String PROPERTY_MSG_BAD_INTERVAL_VALUE = "PROPERTY_MSG_BAD_INTERVAL_VALUE";

    /**
     *
     */
    public final static String PROPERTY_MSG_ERROR_SAVE_OR_UPDATE = "PROPERTY_MSG_ERROR_SAVE_OR_UPDATE";
    private IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO;
    private ISiteGLACPEDAO siteDAO;
    private IDatatypeDAO datatypeDAO;
    private IVariableGLACPEDAO variableDAO;
    private IControleCoherenceDAO controleCoherenceDAO;
    private IProjetSiteThemeDatatypeDAO projetSiteThemeDatatypeDAO;

    String[] namesVariablesPossibles = null;
    Map<String, String[]> namesDatatypesPossibles = new ConcurrentHashMap<String, String[]>();
    Map<String, String[]> namesSitesPossibles = new ConcurrentHashMap<String, String[]>();

    /**
     * @param parser
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @throws IOException
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport();
        try {

            skipHeader(parser);

            // On parcourt chaque ligne du fichier
            String[] values = null;
            int lineNumber = 0;
            VariableGLACPE variable = null;
            SiteGLACPE site = null;
            DataType dataType = null;
            DatatypeVariableUniteGLACPE dataTypeVariableUnite = null;
            while ((values = parser.getLine()) != null) {
                lineNumber++;
                TokenizerValues tokenizerValues = new TokenizerValues(values);

                String variableCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                if (Strings.isNullOrEmpty(variableCode)) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, PROPERTY_MSG_MISSING_VARIABLE), lineNumber, 1));
                } else {
                    try {
                        variable = (VariableGLACPE) variableDAO.getByCode(variableCode);
                        if (variable == null)
                            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, PROPERTY_MSG_BAD_VARIABLE), lineNumber, 1, variableCode));
                    } catch (Exception e) {
                        errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, PROPERTY_MSG_BAD_VARIABLE), lineNumber, 1, variableCode));
                    }
                }
                String datatypeCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                if (Strings.isNullOrEmpty(datatypeCode)) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, PROPERTY_MSG_MISSING_DATATYPE), lineNumber, 2));
                } else {
                    try {
                        dataType = datatypeDAO.getByCode(datatypeCode);
                        if (dataType == null)
                            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, PROPERTY_MSG_BAD_DATATYPE), lineNumber, 2, datatypeCode));
                    } catch (Exception e) {
                        errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, PROPERTY_MSG_BAD_DATATYPE), lineNumber, 2, datatypeCode));
                    }
                }
                if (variable != null && dataType != null) {
                    try {
                        dataTypeVariableUnite = (DatatypeVariableUniteGLACPE) datatypeVariableUniteDAO.getByDatatypeAndVariable(datatypeCode, variableCode);
                        if (dataTypeVariableUnite == null)
                            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, PROPERTY_MSG_BAD_DATATYPE_VARIABLE_UNITE), variableCode, datatypeCode));
                    } catch (Exception e) {
                        errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, PROPERTY_MSG_BAD_DATATYPE_VARIABLE_UNITE), variableCode, datatypeCode));
                    }
                }
                String siteCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                if (!Strings.isNullOrEmpty(siteCode)) {
                    try {
                        site = siteDAO.getByCode(siteCode);
                        if (!Strings.isNullOrEmpty(siteCode) && site == null)
                            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, PROPERTY_MSG_BAD_SITE), lineNumber, 3, siteCode));
                    } catch (Exception e) {
                        errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, PROPERTY_MSG_BAD_SITE), lineNumber, 3, siteCode));
                    }
                }
                Float minValue = null;
                Float maxValue = null;
                try {
                    minValue = tokenizerValues.nextTokenFloat();
                } catch (Exception e) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, PROPERTY_MSG_BAD_MIN_VALUE), lineNumber, 4));
                }
                if (minValue == null)
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, PROPERTY_MSG_BAD_MIN_VALUE), lineNumber, 4));
                try {
                    maxValue = tokenizerValues.nextTokenFloat();
                } catch (Exception e) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, PROPERTY_MSG_BAD_MAX_VALUE), lineNumber, 5));
                }
                if (maxValue == null)
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, PROPERTY_MSG_BAD_MAX_VALUE), lineNumber, 5));
                if (minValue > maxValue)
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, PROPERTY_MSG_BAD_INTERVAL_VALUE), lineNumber));

                if (!errorsReport.hasErrors())
                    persistControleCoherence(errorsReport, dataTypeVariableUnite, site, minValue, maxValue, lineNumber);
            }

            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }

        } catch (IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = null;

            while ((values = parser.getLine()) != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values);

                String variableCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                String dataypeCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                String siteCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                controleCoherenceDAO.remove(controleCoherenceDAO.getBySiteAndDatatypeVariableUnite(siteDAO.getByCode(siteCode), (DatatypeVariableUniteGLACPE) datatypeVariableUniteDAO.getByDatatypeAndVariable(dataypeCode, variableCode)));
            }

        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    private void persistControleCoherence(ErrorsReport errorsReport, DatatypeVariableUniteGLACPE datatypeVariableUnite, SiteGLACPE site, Float minValue, Float maxValue, int lineNumber) {
        ControleCoherence dbControleCoherence = controleCoherenceDAO.getBySiteAndDatatypeVariableUnite(site, datatypeVariableUnite);
        try {
            createOrUpdateSite(site, datatypeVariableUnite, minValue, maxValue, dbControleCoherence);
        } catch (PersistenceException e) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, PROPERTY_MSG_ERROR_SAVE_OR_UPDATE), lineNumber));
        }
    }

    private void createOrUpdateSite(SiteGLACPE site, DatatypeVariableUniteGLACPE datatypeVariableUnite, Float minValue, Float maxValue, ControleCoherence dbControleCoherence) throws PersistenceException {
        if (dbControleCoherence == null) {
            createControleCoherence(site, datatypeVariableUnite, minValue, maxValue);
        } else {
            updateControleCoherence(minValue, maxValue, dbControleCoherence);
        }
    }

    private void createControleCoherence(SiteGLACPE site, DatatypeVariableUniteGLACPE datatypeVariableUnite, Float minValue, Float maxValue) throws PersistenceException {
        ControleCoherence controleCoherence = new ControleCoherence(site, datatypeVariableUnite, minValue, maxValue);
        controleCoherenceDAO.saveOrUpdate(controleCoherence);
    }

    private void updateControleCoherence(Float minValue, Float maxValue, ControleCoherence dbControleCoherence) throws PersistenceException {
        dbControleCoherence.setValeurMin(minValue);
        dbControleCoherence.setValeurMax(maxValue);
        controleCoherenceDAO.saveOrUpdate(dbControleCoherence);
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(ControleCoherence controleCoherence) throws PersistenceException {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        ColumnModelGridMetadata columnModelGridMetadataVariable = new ColumnModelGridMetadata(controleCoherence == null ? EMPTY_STRING : controleCoherence.getDatatypeVariableUnite().getVariable().getNom(), namesVariablesPossibles, null, true, false,
                true);
        ColumnModelGridMetadata columnModelGridMetadataDatatype = new ColumnModelGridMetadata(controleCoherence == null ? EMPTY_STRING : controleCoherence.getDatatypeVariableUnite().getDatatype().getName(), namesDatatypesPossibles, null, true,
                false, true);
        ColumnModelGridMetadata columnModelGridMetadataSite = new ColumnModelGridMetadata(controleCoherence == null || controleCoherence.getSite() == null ? EMPTY_STRING : controleCoherence.getSite().getNom(), namesSitesPossibles, null, true, false,
                false);
        List<ColumnModelGridMetadata> refsVariable = new LinkedList<ColumnModelGridMetadata>();
        refsVariable.add(columnModelGridMetadataDatatype);
        columnModelGridMetadataVariable.setRefs(refsVariable);
        List<ColumnModelGridMetadata> refsDatatype = new LinkedList<ColumnModelGridMetadata>();
        refsDatatype.add(columnModelGridMetadataSite);
        columnModelGridMetadataDatatype.setRefs(refsDatatype);
        columnModelGridMetadataDatatype.setValue(controleCoherence == null ? EMPTY_STRING : controleCoherence.getDatatypeVariableUnite().getDatatype().getName());
        columnModelGridMetadataSite.setValue(controleCoherence == null || controleCoherence.getSite() == null ? EMPTY_STRING : controleCoherence.getSite().getNom());

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnModelGridMetadataVariable);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnModelGridMetadataDatatype);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnModelGridMetadataSite);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(controleCoherence == null ? EMPTY_STRING : controleCoherence.getValeurMin(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(controleCoherence == null ? EMPTY_STRING : controleCoherence.getValeurMax(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, true));

        return lineModelGridMetadata;
    }

    @Override
    protected List<ControleCoherence> getAllElements() throws PersistenceException {
        return controleCoherenceDAO.getAll(ControleCoherence.class);
    }

    @Override
    protected ModelGridMetadata<ControleCoherence> initModelGridMetadata() {
        try {
            updateVariableDatatypeAndSitePossibles();
        } catch (PersistenceException e) {
            e.printStackTrace();
        }
        return super.initModelGridMetadata();
    }

    private void updateVariableDatatypeAndSitePossibles() throws PersistenceException {
        List<Variable> variables = variableDAO.getAll(VariableGLACPE.class);
        String[] variablesPossibles = new String[variables.size()];
        Map<String, String[]> mapDatatypesPossibles = new ConcurrentHashMap<String, String[]>();
        Map<String, String[]> mapSitesPossibles = new ConcurrentHashMap<String, String[]>();
        int i = 0;
        for (Variable variable : variables) {
            variablesPossibles[i++] = variable.getNom();

            String[] datatypesPossibles = updateDatatypeAndSitePossibles(mapSitesPossibles, variable);
            mapDatatypesPossibles.put(variable.getNom(), datatypesPossibles);

        }
        namesVariablesPossibles = variablesPossibles;
        namesDatatypesPossibles = mapDatatypesPossibles;
        namesSitesPossibles = mapSitesPossibles;
    }

    private String[] updateDatatypeAndSitePossibles(Map<String, String[]> mapSitesPossibles, Variable variable) throws PersistenceException {
        List<DatatypeVariableUnite> datatypeVariableUnites = ((VariableGLACPE) variable).getDatatypesUnitesVariables();
        String[] datatypesPossibles = new String[datatypeVariableUnites.size()];
        int j = 0;
        for (DatatypeVariableUnite datatypeVariableUnite : datatypeVariableUnites) {
            datatypesPossibles[j++] = datatypeVariableUnite.getDatatype().getName();
            String[] sitesPossibles = updateSitePossibles(datatypeVariableUnite);
            mapSitesPossibles.put(String.format("%s%s%s", datatypeVariableUnite.getVariable().getNom(), AbstractRecurentObject.CST_PROPERTY_RECURENT_SEPARATOR, datatypeVariableUnite.getDatatype().getName()), sitesPossibles);
        }
        return datatypesPossibles;
    }

    private String[] updateSitePossibles(DatatypeVariableUnite datatypeVariableUnite) throws PersistenceException {
        List<ILeafTreeNode> projetSitesThemesDatatypes = projetSiteThemeDatatypeDAO.getAll();
        List<String> sites = new LinkedList<String>();
        for (ILeafTreeNode node : projetSitesThemesDatatypes) {
            ProjetSiteThemeDatatype projetSiteThemeDatatype = (ProjetSiteThemeDatatype) node;
            if (projetSiteThemeDatatype.getDatatype().getCode().equals(datatypeVariableUnite.getDatatype().getCode()) && !sites.contains(projetSiteThemeDatatype.getProjetSiteTheme().getProjetSite().getSite().getCode())) {
                sites.add(projetSiteThemeDatatype.getProjetSiteTheme().getProjetSite().getSite().getCode());
            }
        }
        return sites.toArray(new String[0]);
    }

    /**
     *
     * @param datatypeVariableUniteDAO
     */
    public void setDatatypeVariableUniteDAO(IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO) {
        this.datatypeVariableUniteDAO = datatypeVariableUniteDAO;
    }

    /**
     *
     * @param siteDAO
     */
    public void setSiteDAO(ISiteGLACPEDAO siteDAO) {
        this.siteDAO = siteDAO;
    }

    /**
     *
     * @param datatypeDAO
     */
    public void setDatatypeDAO(IDatatypeDAO datatypeDAO) {
        this.datatypeDAO = datatypeDAO;
    }

    /**
     *
     * @param variableDAO
     */
    public void setVariableDAO(IVariableGLACPEDAO variableDAO) {
        this.variableDAO = variableDAO;
    }

    /**
     *
     * @param controleCoherenceDAO
     */
    public void setControleCoherenceDAO(IControleCoherenceDAO controleCoherenceDAO) {
        this.controleCoherenceDAO = controleCoherenceDAO;
    }

    /**
     *
     * @param projetSiteThemeDatatypeDAO
     */
    public void setProjetSiteThemeDatatypeDAO(IProjetSiteThemeDatatypeDAO projetSiteThemeDatatypeDAO) {
        this.projetSiteThemeDatatypeDAO = projetSiteThemeDatatypeDAO;
    }

}
