/**
 * OREILacs project - see LICENCE.txt for use created: 7 avr. 2009 16:17:33
 */
package org.inra.ecoinfo.glacpe.refdata.datatype;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.datatype.IDatatypeDAO;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import com.Ostermiller.util.CSVParser;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public class Recorder extends AbstractCSVMetadataRecorder<DataType> {

    private static final String BUNDLE_NAME = "org.inra.ecoinfo.glacpe.refdata.messages";

    private static final String MSG_USE_POSIX_CHARACTER = "PROPERTY_MSG_USE_POSIX_CHARACTER";

    /**
     *
     */
    protected IDatatypeDAO datatypeDAO;
    private Properties propertiesNomEN;
    private Properties propertiesDescriptionEN;

    /**
     * @param parser
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @throws IOException
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport();
        try {

            skipHeader(parser);

            // On parcourt chaque ligne du fichier
            String[] values = null;

            while ((values = parser.getLine()) != null) {

                TokenizerValues tokenizerValues = new TokenizerValues(values, DataType.NAME_ENTITY_JPA);

                String nom = tokenizerValues.nextToken();
                if (!Utils.isPOSIXFree(nom)) {
                    errorsReport.addErrorMessage(localizationManager.getMessage(BUNDLE_NAME, MSG_USE_POSIX_CHARACTER));
                }
                String description = tokenizerValues.nextToken();

                DataType dbDatatype = (DataType) datatypeDAO.getByCode(Utils.createCodeFromString(nom));

                // Enregistre un site uniquement s'il n'existe pas en BD ou bien
                // s'il est considéré comme une mise à jour
                if (dbDatatype == null) {
                    DataType datatype = new DataType(nom, description);
                    datatypeDAO.saveOrUpdate(datatype);

                } else {
                    dbDatatype.setDescription(description);
                    // plateformeDAO.saveOrUpdate(dbSite);
                }
            }
            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }
        } catch (IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                String code = Utils.createCodeFromString(tokenizerValues.nextToken());
                datatypeDAO.remove((DataType) datatypeDAO.getByCode(code));
            }
        } catch (IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param datatypeDAO
     */
    public void setDatatypeDAO(IDatatypeDAO datatypeDAO) {
        this.datatypeDAO = datatypeDAO;
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(DataType datatype) throws PersistenceException {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(datatype == null ? EMPTY_STRING : datatype.getName(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(datatype == null ? EMPTY_STRING : propertiesNomEN.get(datatype.getName()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(datatype == null ? EMPTY_STRING : datatype.getDescription(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(datatype == null ? EMPTY_STRING : propertiesDescriptionEN.get(datatype.getDescription()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        return lineModelGridMetadata;
    }

    @Override
    protected ModelGridMetadata<DataType> initModelGridMetadata() {
        propertiesNomEN = localizationManager.newProperties(DataType.NAME_ENTITY_JPA, "name", Locale.ENGLISH);
        propertiesDescriptionEN = localizationManager.newProperties(DataType.NAME_ENTITY_JPA, "description", Locale.ENGLISH);
        return super.initModelGridMetadata();
    }

    @Override
    protected List<DataType> getAllElements() throws PersistenceException {
        return datatypeDAO.getAll(DataType.class);
    }

}
