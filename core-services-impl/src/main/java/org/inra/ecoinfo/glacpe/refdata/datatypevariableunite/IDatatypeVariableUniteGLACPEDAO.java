package org.inra.ecoinfo.glacpe.refdata.datatypevariableunite;

import java.util.List;

import org.inra.ecoinfo.refdata.datatypevariableunite.IDatatypeVariableUniteDAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public interface IDatatypeVariableUniteGLACPEDAO extends IDatatypeVariableUniteDAO {

    /**
     *
     * @param datatypeCode
     * @param variableCode
     * @return
     * @throws PersistenceException
     */
    public DatatypeVariableUniteGLACPE getByDatatypeAndVariable(String datatypeCode, String variableCode) throws PersistenceException;

    /**
     *
     * @return
     * @throws PersistenceException
     */
    public List<DatatypeVariableUniteGLACPE> getAllDatatypesVariablesUnitesGLACPE() throws PersistenceException;

    /**
     *
     * @param variableCode
     * @return
     * @throws PersistenceException
     */
    public List<DatatypeVariableUniteGLACPE> getByVariable(String variableCode) throws PersistenceException;
}
