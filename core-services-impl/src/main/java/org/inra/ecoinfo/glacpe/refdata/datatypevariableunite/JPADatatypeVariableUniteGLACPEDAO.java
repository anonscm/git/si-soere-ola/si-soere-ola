package org.inra.ecoinfo.glacpe.refdata.datatypevariableunite;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.inra.ecoinfo.refdata.datatypevariableunite.JPADatatypeVariableUniteDAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
@SuppressWarnings("unchecked")
public class JPADatatypeVariableUniteGLACPEDAO extends JPADatatypeVariableUniteDAO implements IDatatypeVariableUniteGLACPEDAO {

    private static final String QUERY_HQL_GET_BY_DATATYPE_AND_VARIABLE = "from DatatypeVariableUniteGLACPE d where d.datatype.code = :datatypeCode and d.variable.code = :variableCode";
    private static final String QUERY_HQL_GET_BY_VARIABLE = "from DatatypeVariableUniteGLACPE d where d.variable.code = :variableCode";
    private static final String QUERY_ALL = "from DatatypeVariableUniteGLACPE";

    /**
     *
     * @return
     * @throws PersistenceException
     */
    @Override
    public List<DatatypeVariableUniteGLACPE> getAllDatatypesVariablesUnitesGLACPE() throws PersistenceException {
        try {
            String queryString = QUERY_ALL;
            Query query = entityManager.createQuery(queryString);

            return query.getResultList();

        } catch (NoResultException e) {
            return new LinkedList<DatatypeVariableUniteGLACPE>();
        }
    }

    /**
     *
     * @param datatypeCode
     * @param variableCode
     * @return
     * @throws PersistenceException
     */
    @Override
    public DatatypeVariableUniteGLACPE getByDatatypeAndVariable(String datatypeCode, String variableCode) throws PersistenceException {
        try {
            String queryString = QUERY_HQL_GET_BY_DATATYPE_AND_VARIABLE;
            Query query = entityManager.createQuery(queryString);
            query.setParameter("datatypeCode", datatypeCode);
            query.setParameter("variableCode", variableCode);

            return (DatatypeVariableUniteGLACPE) query.getSingleResult();

        } catch (NoResultException e) {
            return null;
        }
    }

    /**
     *
     * @param variableCode
     * @return
     * @throws PersistenceException
     */
    @Override
    public List<DatatypeVariableUniteGLACPE> getByVariable(String variableCode) throws PersistenceException {
        try {
            String queryString = QUERY_HQL_GET_BY_VARIABLE;
            Query query = entityManager.createQuery(queryString);
            query.setParameter("variableCode", variableCode);

            return (List<DatatypeVariableUniteGLACPE>) query.getResultList();

        } catch (NoResultException e) {
            return null;
        }
    }
}
