/**
 * OREILacs project - see LICENCE.txt for use created: 7 avr. 2009 16:17:33
 */
package org.inra.ecoinfo.glacpe.refdata.datatypevariableunite;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.datatype.IDatatypeDAO;
import org.inra.ecoinfo.refdata.datatypevariableunite.DatatypeVariableUnite;
import org.inra.ecoinfo.refdata.unite.IUniteDAO;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import com.Ostermiller.util.CSVParser;

/**
 * @author "Antoine Schellenberger"
 *
 */
public class Recorder extends AbstractCSVMetadataRecorder<DatatypeVariableUniteGLACPE> {

    private static final String BUNDLE_NAME = "org.inra.ecoinfo.glacpe.refdata.messages";

    private static final String MSG_VARIABLE_MISSING_IN_DATABASE = "PROPERTY_MSG_VARIABLE_MISSING_IN_DATABASE";
    private static final String MSG_DATATYPE_MISSING_IN_DATABASE = "PROPERTY_MSG_DATATYPE_MISSING_IN_DATABASE";
    private static final String MSG_UNITE_MISSING_IN_DATABASE = "PROPERTY_MSG_UNITE_MISSING_IN_DATABASE";

    /**
     *
     */
    protected IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO;
    private Map<String, String[]> variablesPossibles = new ConcurrentHashMap<String, String[]>();
    private Map<String, String[]> unitesPossibles = new ConcurrentHashMap<String, String[]>();
    private Map<String, String[]> datatypesPossibles = new ConcurrentHashMap<String, String[]>();

    private IVariableDAO variableDAO;

    private IUniteDAO uniteDAO;

    private IDatatypeDAO datatypeDAO;

    /**
     * @param parser
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @throws IOException
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport();

        try {

            skipHeader(parser);

            // On parcourt chaque ligne du fichier
            String[] values = null;

            while ((values = parser.getLine()) != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values);

                String datatypeCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                String variableCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                String uniteCode = Utils.createCodeFromString(tokenizerValues.nextToken());

                // afiocca
                String codeSandre = tokenizerValues.nextToken();
                String contexte = tokenizerValues.nextToken();

                persistDatatypeVariable(errorsReport, datatypeCode, variableCode, uniteCode, codeSandre, contexte);

            }
            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }
        } catch (IOException e) {
            AbstractCSVMetadataRecorder.LOGGER.error(e);
            throw new BusinessException(e);
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                String datatypeCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                String variableCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                String uniteCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                datatypeVariableUniteDAO.remove(datatypeVariableUniteDAO.getByNKey(datatypeCode, variableCode, uniteCode));
            }
        } catch (IOException e) {
            throw new BusinessException(e);
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    private void persistDatatypeVariable(ErrorsReport errorsReport, String datatypeCode, String variableCode, String uniteCode, String codeSandre, String contexte) throws PersistenceException {
        DataType dbDatatype = retrieveDBDatatype(errorsReport, datatypeCode);
        Variable dbVariable = retrieveDBVariable(errorsReport, variableCode);
        Unite dbUnite = retrieveDBUnite(errorsReport, uniteCode);

        if (dbDatatype != null && dbVariable != null && dbUnite != null) {
            final DatatypeVariableUniteGLACPE dbDatatypeVariableUnite = (DatatypeVariableUniteGLACPE) datatypeVariableUniteDAO.getByNKey(datatypeCode, variableCode, uniteCode);

            createOrUpdateDatatypeVariable(dbDatatypeVariableUnite, dbDatatype, dbUnite, dbVariable, codeSandre, contexte);

        }
    }

    /**
     * 
     * @param dbDatatypeVariableUnite
     * @param dbDatatype
     * @param dbUnite
     * @param dbVariable
     * @param codeSandre
     * @param contexte
     * @throws PersistenceException 
     */
    private void createOrUpdateDatatypeVariable(final DatatypeVariableUniteGLACPE dbDatatypeVariableUnite, DataType dbDatatype, Unite dbUnite, Variable dbVariable, String codeSandre, String contexte) throws PersistenceException {
        if (dbDatatypeVariableUnite == null) {
            
            DatatypeVariableUniteGLACPE datatypeVariableUnite = new DatatypeVariableUniteGLACPE(dbDatatype, dbUnite, dbVariable);
            datatypeVariableUnite.setCodeSandre(codeSandre);
            datatypeVariableUnite.setContexte(contexte);
            dbDatatype.getDatatypesUnitesVariables().add(datatypeVariableUnite);
            dbVariable.getDatatypesUnitesVariables().add(datatypeVariableUnite);
            datatypeVariableUniteDAO.saveOrUpdate(datatypeVariableUnite);
        } else {
            dbDatatypeVariableUnite.setContexte(contexte);
            dbDatatypeVariableUnite.setCodeSandre(codeSandre);
            datatypeVariableUniteDAO.saveOrUpdate(dbDatatypeVariableUnite);
        }
    }


    private Variable retrieveDBVariable(ErrorsReport errorsReport, String variableCode) throws PersistenceException {
        Variable variable = variableDAO.getByCode(variableCode);
        if (variable == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, MSG_VARIABLE_MISSING_IN_DATABASE), variableCode));
        }
        return variable;
    }

    private Unite retrieveDBUnite(ErrorsReport errorsReport, String uniteCode) throws PersistenceException {
        Unite unite = uniteDAO.getByCode(uniteCode);
        if (unite == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, MSG_UNITE_MISSING_IN_DATABASE), uniteCode));
        }
        return unite;
    }

    private DataType retrieveDBDatatype(ErrorsReport errorsReport, String datatypeCode) throws PersistenceException {
        DataType datatype = datatypeDAO.getByCode(datatypeCode);
        if (datatype == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, MSG_DATATYPE_MISSING_IN_DATABASE), datatypeCode));
        }
        return datatype;
    }

    private void updateNamesVariablesPossibles() throws PersistenceException {
        List<Variable> variables = variableDAO.getAll(Variable.class);
        String[] namesVariablesPossibles = new String[variables.size()];
        int index = 0;
        for (Variable variable : variables) {
            namesVariablesPossibles[index++] = variable.getNom();
        }
        variablesPossibles.put(ColumnModelGridMetadata.NULL_KEY, namesVariablesPossibles);

    }

    private void updateNamesDatatypesPossibles() throws PersistenceException {
        List<DataType> datatypes = datatypeDAO.getAll(DataType.class);
        String[] namesDatatypesPossibles = new String[datatypes.size()];
        int index = 0;
        for (DataType datatype : datatypes) {
            namesDatatypesPossibles[index++] = datatype.getName();
        }
        datatypesPossibles.put(ColumnModelGridMetadata.NULL_KEY, namesDatatypesPossibles);
    }

    private void updateNamesUnitesPossibles() throws PersistenceException {
        List<Unite> unites = uniteDAO.getAll(Unite.class);
        String[] namesUnitesPossibles = new String[unites.size()];
        int index = 0;
        for (Unite unite : unites) {
            namesUnitesPossibles[index++] = unite.getCode();
        }
        unitesPossibles.put(ColumnModelGridMetadata.NULL_KEY, namesUnitesPossibles);
    }

    /**
     *
     * @param datatypeDAO
     */
    public void setDatatypeDAO(IDatatypeDAO datatypeDAO) {
        this.datatypeDAO = datatypeDAO;
    }

    /**
     *
     * @param variableDAO
     */
    public void setVariableDAO(IVariableDAO variableDAO) {
        this.variableDAO = variableDAO;
    }

    /**
     *
     * @param uniteDAO
     */
    public void setUniteDAO(IUniteDAO uniteDAO) {
        this.uniteDAO = uniteDAO;
    }

    /**
     *
     * @param datatypeVariableUniteDAO
     */
    public void setDatatypeVariableUniteDAO(IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO) {
        this.datatypeVariableUniteDAO = datatypeVariableUniteDAO;
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(DatatypeVariableUniteGLACPE datatypeVariableUnite) throws PersistenceException {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(datatypeVariableUnite == null ? EMPTY_STRING : datatypeVariableUnite.getDatatype().getName(), datatypesPossibles, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(datatypeVariableUnite == null ? EMPTY_STRING : datatypeVariableUnite.getVariable().getNom(), variablesPossibles, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(datatypeVariableUnite == null ? EMPTY_STRING : datatypeVariableUnite.getUnite().getCode(), unitesPossibles, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(datatypeVariableUnite == null ? EMPTY_STRING : datatypeVariableUnite.getCodeSandre(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(datatypeVariableUnite == null ? EMPTY_STRING : datatypeVariableUnite.getContexte(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        
        
        return lineModelGridMetadata;
    }

    @Override
    protected List<DatatypeVariableUniteGLACPE> getAllElements() throws PersistenceException {
        List<DatatypeVariableUnite> dvus = datatypeVariableUniteDAO.getAll(DatatypeVariableUnite.class);
        List<DatatypeVariableUniteGLACPE> dvug = new LinkedList<DatatypeVariableUniteGLACPE>();

        for (DatatypeVariableUnite dvu : dvus) {
            dvug.add((DatatypeVariableUniteGLACPE) dvu);
        }
        return dvug;
    }

    @Override
    protected ModelGridMetadata<DatatypeVariableUniteGLACPE> initModelGridMetadata() {
        try {
            updateNamesVariablesPossibles();
            updateNamesUnitesPossibles();
            updateNamesDatatypesPossibles();
        } catch (PersistenceException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return super.initModelGridMetadata();
    }

}
