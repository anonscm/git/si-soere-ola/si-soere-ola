package org.inra.ecoinfo.glacpe.refdata.groupevariable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.inra.ecoinfo.glacpe.extraction.vo.GroupeVariableVO;
import org.inra.ecoinfo.glacpe.extraction.vo.VariableVO;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;

/**
 *
 * @author ptcherniati
 */
public class DefaultGroupeVariableBuilder implements IGroupeVariableBuilder {

    List<GroupeVariableVO> groupesVariables = new ArrayList<GroupeVariableVO>();
    Map<String, GroupeVariableVO> groupesVariablesVOsMap = new HashMap<String, GroupeVariableVO>();

    /**
     *
     * @param variables
     * @return
     */
    @Override
    public List<GroupeVariableVO> build(List<VariableGLACPE> variables) {

        for (VariableGLACPE variable : variables) {
            VariableVO variableVO = new VariableVO(variable);

            if (variable.getGroupe() != null) {
                GroupeVariableVO groupeVariableVO = retrieveOrCreateGroupesVariablesVO(variable.getGroupe());
                variableVO.setGroupeVariable(groupeVariableVO);
                if (!groupeVariableVO.getVariables().contains(variableVO))
                    groupeVariableVO.getVariables().add(variableVO);
            }
        }
        return groupesVariables;
    }

    private GroupeVariableVO fillRecursiveGroupeVariableVO(GroupeVariable groupeVariable) {
        GroupeVariableVO groupeVariableVO = new GroupeVariableVO(groupeVariable);
        if (groupeVariable.getParent() == null) {
            groupesVariables.add(groupeVariableVO);

        } else {
            GroupeVariableVO parentGroupeVariableVO = retrieveOrCreateGroupesVariablesVO(groupeVariable.getParent());
            groupeVariableVO.setParent(parentGroupeVariableVO);
            parentGroupeVariableVO.getChildren().add(groupeVariableVO);
        }

        return groupeVariableVO;
    }

    private GroupeVariableVO retrieveOrCreateGroupesVariablesVO(GroupeVariable groupeVariable) {
        if (groupesVariablesVOsMap.containsKey(groupeVariable.getCode())) {
            return groupesVariablesVOsMap.get(groupeVariable.getCode());
        } else {
            GroupeVariableVO groupeVariableVO = fillRecursiveGroupeVariableVO(groupeVariable);
            groupesVariablesVOsMap.put(groupeVariable.getCode(), groupeVariableVO);
            return groupeVariableVO;
        }
    }

}
