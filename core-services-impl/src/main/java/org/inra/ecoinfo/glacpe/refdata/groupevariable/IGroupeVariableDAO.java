/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:53:17
 */
package org.inra.ecoinfo.glacpe.refdata.groupevariable;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public interface IGroupeVariableDAO extends IDAO<GroupeVariable> {

    /**
     *
     * @param nom
     * @return
     * @throws PersistenceException
     */
    public GroupeVariable getByCode(String nom) throws PersistenceException;

}
