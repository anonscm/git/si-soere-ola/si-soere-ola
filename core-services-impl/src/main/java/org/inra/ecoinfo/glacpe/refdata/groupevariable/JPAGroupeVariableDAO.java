/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:50:44
 */
package org.inra.ecoinfo.glacpe.refdata.groupevariable;

import java.util.List;

import javax.persistence.Query;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public class JPAGroupeVariableDAO extends AbstractJPADAO<GroupeVariable> implements IGroupeVariableDAO {

    /**
     *
     * @param code
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("rawtypes")
    @Override
    public GroupeVariable getByCode(String code) throws PersistenceException {
        try {
            String queryString = "from GroupeVariable v where v.code = :code";
            Query query = entityManager.createQuery(queryString);
            query.setParameter("code", code);
            List groupeVariables = query.getResultList();

            GroupeVariable groupeVariable = null;
            if (groupeVariables != null && !groupeVariables.isEmpty())
                groupeVariable = (GroupeVariable) groupeVariables.get(0);

            return groupeVariable;
        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

}
