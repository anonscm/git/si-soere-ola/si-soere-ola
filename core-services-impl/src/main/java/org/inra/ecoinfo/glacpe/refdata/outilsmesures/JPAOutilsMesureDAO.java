/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:50:44
 */
package org.inra.ecoinfo.glacpe.refdata.outilsmesures;

import java.util.List;

import javax.persistence.Query;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public class JPAOutilsMesureDAO extends AbstractJPADAO<OutilsMesure> implements IOutilsMesureDAO {

    /**
     *
     * @param code
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("rawtypes")
    @Override
    public OutilsMesure getByCode(String code) throws PersistenceException {
        try {
            String queryString = "from OutilsMesure o where o.code = :code";
            Query query = entityManager.createQuery(queryString);
            query.setParameter("code", code);
            List outilsMesures = query.getResultList();

            OutilsMesure outilsMesure = null;
            if (outilsMesures != null && !outilsMesures.isEmpty())
                outilsMesure = (OutilsMesure) outilsMesures.get(0);

            return outilsMesure;
        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

}
