/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:50:44
 */
package org.inra.ecoinfo.glacpe.refdata.plateforme;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public class JPAPlateformeDAO extends AbstractJPADAO<Plateforme> implements IPlateformeDAO {

    @Override
    public Plateforme getByNKey(String codePlateforme, String codeSite) throws PersistenceException {
        try {
            String queryString = "from Plateforme p where p.code = :codePlateforme and p.site.code = :codeSite";
            Query query = entityManager.createQuery(queryString);
            query.setParameter("codePlateforme", codePlateforme);
            query.setParameter("codeSite", codeSite);
            return (Plateforme) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

}
