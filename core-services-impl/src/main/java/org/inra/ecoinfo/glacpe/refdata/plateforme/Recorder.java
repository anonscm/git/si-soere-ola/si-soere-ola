/**
 * OREILacs project - see LICENCE.txt for use created: 7 avr. 2009 16:17:33
 */
package org.inra.ecoinfo.glacpe.refdata.plateforme;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.typeplateforme.ITypePlateformeDAO;
import org.inra.ecoinfo.glacpe.refdata.typeplateforme.TypePlateforme;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.refdata.site.ISiteDAO;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import com.Ostermiller.util.CSVParser;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public class Recorder extends AbstractCSVMetadataRecorder<Plateforme> {

    private static final String BUNDLE_NAME = "org.inra.ecoinfo.glacpe.refdata.messages";

    private static final String MSG_ERROR_CODE_SITE_NOT_FOUND_IN_DB = "PROPERTY_MSG_ERROR_CODE_SITE_NOT_FOUND_IN_DB";
    private static final String MSG_ERROR_CODE_TYPE_PLATEFORME_NOT_FOUND_IN_DB = "PROPERTY_MSG_ERROR_CODE_TYPE_PLATEFORME_NOT_FOUND_IN_DB";

    /**
     *
     */
    protected ITypePlateformeDAO typePlateformeDAO;

    /**
     *
     */
    protected IPlateformeDAO plateformeDAO;

    /**
     *
     */
    protected ISiteDAO siteDAO;
    private String[] namesTypesPlateformesPossibles;
    private String[] namesSitesPossibles;

    private Properties propertiesNomEN;

    /**
     * @param parser
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @throws IOException
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {

        ErrorsReport errorsReport = new ErrorsReport();
        try {

            skipHeader(parser);

            // On parcourt chaque ligne du fichier
            String[] values = null;

            while ((values = parser.getLine()) != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values, Plateforme.TABLE_NAME);

                // On parcourt chaque colonne d'une ligne
                String siteCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                String nom = tokenizerValues.nextToken();
                Float latitude = tokenizerValues.nextTokenFloat();
                Float longitude = tokenizerValues.nextTokenFloat();
                Float altitude = tokenizerValues.nextTokenFloat();
                String typePlateformeCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                
                String codeSandre = tokenizerValues.nextToken();
                String contexte = tokenizerValues.nextToken();
                
                

                persistPlateforme(errorsReport, siteCode, nom, latitude, longitude, altitude, typePlateformeCode, codeSandre, contexte);

            }

            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }

        } catch (IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                String codeSite = tokenizerValues.nextToken();
                String codePlateforme = Utils.createCodeFromString(tokenizerValues.nextToken());
                plateformeDAO.remove(plateformeDAO.getByNKey(codePlateforme, codeSite));
            }
        } catch (IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    private void persistPlateforme(ErrorsReport errorsReport, String siteCode, String nom, Float latitude, Float longitude, Float altitude, String typePlateformeCode, String codeSandre, String contexte) throws PersistenceException {
        TypePlateforme dbTypePlateforme = retrieveDBTypePlateforme(errorsReport, typePlateformeCode);
        SiteGLACPE dbSite = retrieveDBSite(errorsReport, siteCode);

        if (dbSite != null && dbTypePlateforme != null) {
            Plateforme dbPlateforme = plateformeDAO.getByNKey(Utils.createCodeFromString(nom), dbSite.getCode());
            createOrUpdatePlateforme(nom, latitude, longitude, altitude, dbTypePlateforme, dbSite, dbPlateforme, codeSandre, contexte);
        }
    }

    private void createOrUpdatePlateforme(String nom, Float latitude, Float longitude, Float altitude, TypePlateforme dbTypePlateforme, SiteGLACPE dbSite, Plateforme dbPlateforme, String codeSandre, String contexte) {
        if (dbPlateforme == null) {
            createPlateforme(nom, latitude, longitude, altitude, dbTypePlateforme, dbSite, codeSandre, contexte);

        } else {
            updatePlateforme(latitude, longitude, altitude, dbTypePlateforme, dbSite, dbPlateforme, codeSandre, contexte);
        }
    }

    private void updatePlateforme(Float latitude, Float longitude, Float altitude, TypePlateforme dbTypePlateforme, SiteGLACPE dbSite, Plateforme dbPlateforme, String codeSandre, String contexte) {
        dbPlateforme.setLatitude(latitude);
        dbPlateforme.setAltitude(altitude);
        dbPlateforme.setLongitude(longitude);

        dbPlateforme.setTypePlateforme(dbTypePlateforme);
        dbTypePlateforme.getPlateformes().add(dbPlateforme);

        dbPlateforme.setSite(dbSite);
        dbSite.getPlateformes().add(dbPlateforme);
        dbPlateforme.setCodeSandre(codeSandre);
        dbPlateforme.setContexte(contexte);
    }

    private void createPlateforme(String nom, Float latitude, Float longitude, Float altitude, TypePlateforme dbTypePlateforme, SiteGLACPE dbSite, String codeSandre, String contexte) {
        Plateforme dbPlateforme = new Plateforme(nom, latitude, longitude, altitude);
        dbPlateforme.setTypePlateforme(dbTypePlateforme);
        dbTypePlateforme.getPlateformes().add(dbPlateforme);

        dbPlateforme.setSite(dbSite);
        dbSite.getPlateformes().add(dbPlateforme);
        dbPlateforme.setCodeSandre(codeSandre);
        dbPlateforme.setContexte(contexte);
    }

    private TypePlateforme retrieveDBTypePlateforme(ErrorsReport errorsReport, String typePlateformeCode) throws PersistenceException {
        TypePlateforme typePlateforme = typePlateformeDAO.getByCode(typePlateformeCode);
        if (typePlateforme == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, MSG_ERROR_CODE_TYPE_PLATEFORME_NOT_FOUND_IN_DB), typePlateformeCode));
        }
        return typePlateforme;
    }

    private SiteGLACPE retrieveDBSite(ErrorsReport errorsReport, String siteCode) throws PersistenceException {
        SiteGLACPE site = (SiteGLACPE) siteDAO.getByCode(siteCode);
        if (site == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, MSG_ERROR_CODE_SITE_NOT_FOUND_IN_DB), siteCode));
        }
        return site;
    }

    private void updateNamesTypesPlateformesPossibles() throws PersistenceException {
        List<TypePlateforme> typesPlateformes = typePlateformeDAO.getAll(TypePlateforme.class);
        String[] namesTypesPlateformesPossibles = new String[typesPlateformes.size()];
        int index = 0;
        for (TypePlateforme typePlateforme : typesPlateformes) {
            namesTypesPlateformesPossibles[index++] = typePlateforme.getNom();
        }
        this.namesTypesPlateformesPossibles = namesTypesPlateformesPossibles;

    }

    private void updateNamesSitesPossibles() throws PersistenceException {
        List<Site> sites = siteDAO.getAll(SiteGLACPE.class);
        String[] namesSitesPossibles = new String[sites.size()];
        int index = 0;
        for (Site site : sites) {
            namesSitesPossibles[index++] = site.getNom();
        }
        this.namesSitesPossibles = namesSitesPossibles;
    }

    /**
     *
     * @param typePlateformeDAO
     */
    public void setTypePlateformeDAO(ITypePlateformeDAO typePlateformeDAO) {
        this.typePlateformeDAO = typePlateformeDAO;
    }

    /**
     *
     * @param plateformeDAO
     */
    public void setPlateformeDAO(IPlateformeDAO plateformeDAO) {
        this.plateformeDAO = plateformeDAO;
    }

    /**
     *
     * @param siteDAO
     */
    public void setSiteDAO(ISiteDAO siteDAO) {
        this.siteDAO = siteDAO;
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Plateforme plateforme) throws PersistenceException {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(plateforme == null ? EMPTY_STRING : plateforme.getSite().getNom(), namesSitesPossibles, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(plateforme == null ? EMPTY_STRING : plateforme.getNom(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(plateforme == null ? EMPTY_STRING : propertiesNomEN.get(plateforme.getNom()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(plateforme == null ? EMPTY_STRING : plateforme.getLatitude() != null ? plateforme.getLatitude().toString() : "", ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(plateforme == null ? EMPTY_STRING : plateforme.getLongitude() != null ? plateforme.getLongitude().toString() : "", ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(plateforme == null ? EMPTY_STRING : plateforme.getAltitude() != null ? plateforme.getAltitude().toString() : "", ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(plateforme == null ? EMPTY_STRING : plateforme.getTypePlateforme().getNom(), namesTypesPlateformesPossibles, null, false, false, true));

        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(plateforme == null ? EMPTY_STRING : plateforme.getCodeSandre(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(plateforme == null ? EMPTY_STRING : plateforme.getContexte(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        
        return lineModelGridMetadata;
    }

    @Override
    protected List<Plateforme> getAllElements() throws PersistenceException {
        return plateformeDAO.getAll(Plateforme.class);
    }

    @Override
    protected ModelGridMetadata<Plateforme> initModelGridMetadata() {
        try {
            propertiesNomEN = localizationManager.newProperties(Plateforme.TABLE_NAME, "nom", Locale.ENGLISH);
            updateNamesSitesPossibles();
            updateNamesTypesPlateformesPossibles();
        } catch (PersistenceException e) {
            e.printStackTrace();
        }
        return super.initModelGridMetadata();
    }

}
