/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:53:17
 */
package org.inra.ecoinfo.glacpe.refdata.projet;

import java.util.List;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public interface IProjetDAO extends IDAO<Projet> {

    /**
     *
     * @param code
     * @return
     * @throws PersistenceException
     */
    public Projet getByCode(String code) throws PersistenceException;

    /**
     *
     * @return
     * @throws PersistenceException
     */
    public List<Projet> getAll() throws PersistenceException;
}
