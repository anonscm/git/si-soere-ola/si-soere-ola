/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:50:44
 */
package org.inra.ecoinfo.glacpe.refdata.projet;

import java.util.List;

import javax.persistence.Query;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public class JPAProjetDAO extends AbstractJPADAO<Projet> implements IProjetDAO {

    /**
     *
     * @param code
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("unchecked")
    @Override
    public Projet getByCode(String code) throws PersistenceException {
        try {
            String queryString = "from Projet p where p.code = :code";
            Query query = entityManager.createQuery(queryString);
            query.setParameter("code", code);
            List<Projet> projets = query.getResultList();

            Projet projet = null;
            if (projets != null && !projets.isEmpty())
                projet = projets.get(0);

            return projet;
        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     *
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Projet> getAll() throws PersistenceException {
        try {
            String queryString = "from Projet p";
            Query query = entityManager.createQuery(queryString);
            List<Projet> projets = query.getResultList();

            return projets;
        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

}
