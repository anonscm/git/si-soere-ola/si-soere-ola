/**
 * OREILacs project - see LICENCE.txt for use created: 7 avr. 2009 16:17:33
 */
package org.inra.ecoinfo.glacpe.refdata.projet;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import com.Ostermiller.util.CSVParser;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public class Recorder extends AbstractCSVMetadataRecorder<Projet> {

    private static final String BUNDLE_NAME = "org.inra.ecoinfo.glacpe.refdata.messages";

    private static final String MSG_USE_POSIX_CHARACTER = "PROPERTY_MSG_USE_POSIX_CHARACTER";

    /**
     *
     */
    protected IProjetDAO projetDAO;

    private Properties propertiesNomEN;
    private Properties propertiesDescriptionEN;

    /**
     * @param parser
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @throws IOException
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport();

        try {

            skipHeader(parser);

            // On parcourt chaque ligne du fichier
            String[] values = null;

            while ((values = parser.getLine()) != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values, Projet.NAME_TABLE);

                String projetNom = tokenizerValues.nextToken();
                if (!Utils.isPOSIXFree(projetNom)) {
                    errorsReport.addErrorMessage(localizationManager.getMessage(BUNDLE_NAME, MSG_USE_POSIX_CHARACTER));
                }
                String projetCode = Utils.createCodeFromString(projetNom);
                String description = tokenizerValues.nextToken();
                persistProjet(errorsReport, projetCode, description, projetNom);

            }

            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }

        } catch (IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                String projetCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                projetDAO.remove(projetDAO.getByCode(projetCode));
            }
        } catch (IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    private void persistProjet(ErrorsReport errorsReport, String projetCode, String description, String projetNom) throws PersistenceException {
        Projet projet = retrieveOrCreateDBProjet(errorsReport, projetCode, projetNom);
        projet.setDescriptionProjet(description);
        projetDAO.saveOrUpdate(projet);
    }

    private Projet retrieveOrCreateDBProjet(ErrorsReport errorsReport, String projetCode, String projetNom) throws PersistenceException {
        Projet projet = projetDAO.getByCode(projetCode);
        if (projet == null) {
            projet = new Projet();
            projet.setNom(projetNom);
        }

        return projet;
    }

    /**
     *
     * @param projetDAO
     */
    public void setProjetDAO(IProjetDAO projetDAO) {
        this.projetDAO = projetDAO;
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Projet projet) throws PersistenceException {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(projet == null ? EMPTY_STRING : projet.getNom(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(projet == null ? EMPTY_STRING : propertiesNomEN.get(projet.getNom()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(projet == null ? EMPTY_STRING : projet.getDescriptionProjet(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(projet == null ? EMPTY_STRING : propertiesDescriptionEN.get(projet.getDescriptionProjet()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        return lineModelGridMetadata;
    }

    @Override
    protected ModelGridMetadata<Projet> initModelGridMetadata() {
        propertiesNomEN = localizationManager.newProperties(Projet.NAME_TABLE, "nom", Locale.ENGLISH);
        propertiesDescriptionEN = localizationManager.newProperties(Projet.NAME_TABLE, "description_projet", Locale.ENGLISH);
        return super.initModelGridMetadata();
    }

    @Override
    protected List<Projet> getAllElements() throws PersistenceException {
        return projetDAO.getAll(Projet.class);
    }
}
