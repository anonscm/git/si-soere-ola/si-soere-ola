/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:53:17
 */
package org.inra.ecoinfo.glacpe.refdata.projetsite;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public interface IProjetSiteDAO extends IDAO<ProjetSite> {

    /**
     *
     * @param siteCode
     * @param projetCode
     * @return
     * @throws PersistenceException
     */
    public ProjetSite getBySiteCodeAndProjetCode(String siteCode, String projetCode) throws PersistenceException;

    /**
     *
     * @return
     */
    public ProjetSite newEntity();
}
