/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:50:44
 */
package org.inra.ecoinfo.glacpe.refdata.projetsite;

import java.util.List;

import javax.persistence.Query;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public class JPAProjetSiteDAO extends AbstractJPADAO<ProjetSite> implements IProjetSiteDAO {

    /**
     *
     * @param siteCode
     * @param projetCode
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("rawtypes")
    @Override
    public ProjetSite getBySiteCodeAndProjetCode(String siteCode, String projetCode) throws PersistenceException {
        try {
            String queryString = "from ProjetSite ps where ps.site.codeFromName = :siteCode and ps.projet.code = :projetCode";
            Query query = entityManager.createQuery(queryString);
            query.setParameter("siteCode", Utils.createCodeFromString(siteCode));
            query.setParameter("projetCode", projetCode);
            List projetsSites = query.getResultList();

            ProjetSite projetSite = null;
            if (projetsSites != null && !projetsSites.isEmpty()) {
                projetSite = (ProjetSite) projetsSites.get(0);
            }

            return projetSite;
        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     *
     * @return
     */
    @Override
    public ProjetSite newEntity() {
        return new ProjetSite();
    }

}
