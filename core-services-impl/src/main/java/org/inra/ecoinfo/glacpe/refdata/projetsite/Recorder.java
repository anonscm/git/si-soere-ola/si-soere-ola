/**
 * OREILacs project - see LICENCE.txt for use created: 7 avr. 2009 16:17:33
 */
package org.inra.ecoinfo.glacpe.refdata.projetsite;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.inra.ecoinfo.glacpe.refdata.projet.IProjetDAO;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.refdata.site.ISiteDAO;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import com.Ostermiller.util.CSVParser;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public class Recorder extends AbstractCSVMetadataRecorder<ProjetSite> {

    private static final String BUNDLE_NAME = "org.inra.ecoinfo.glacpe.refdata.messages";

    /**
     *
     */
    protected ISiteDAO siteDAO;

    /**
     *
     */
    protected IProjetSiteDAO projetSiteDAO;

    /**
     *
     */
    protected IProjetDAO projetDAO;
    private String[] namesSitesPossibles;
    private String[] namesProjetsPossibles;

    private static final String MSG_ERROR_CODE_SITE_NOT_FOUND_IN_DB = "PROPERTY_MSG_ERROR_CODE_SITE_NOT_FOUND_IN_DB";
    private static final String MSG_ERROR_CODE_PROJET_NOT_FOUND_IN_DB = "PROPERTY_MSG_ERROR_CODE_PROJET_NOT_FOUND_IN_DB";

    /**
     * @param parser
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @throws IOException
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport();

        try {

            skipHeader(parser);

            // On parcourt chaque ligne du fichier
            String[] values = null;

            while ((values = parser.getLine()) != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values);

                String projetNom = tokenizerValues.nextToken();
                String projetCode = Utils.createCodeFromString(projetNom);
                String siteCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                String commanditaire = tokenizerValues.nextToken();
                String dateDebutString = tokenizerValues.nextToken();
                Date dateDebut = dateDebutString != null && dateDebutString.length() > 0 ? new SimpleDateFormat(datasetDescriptor.getColumns().get(3).getFormatType()).parse(dateDebutString) : null;
                String dateFinString = tokenizerValues.nextToken();
                Date dateFin = dateFinString != null && dateFinString.length() > 0 ? new SimpleDateFormat(datasetDescriptor.getColumns().get(4).getFormatType()).parse(dateFinString) : null;
                String commentaire = tokenizerValues.nextToken();
                persistProjetSite(errorsReport, projetCode, siteCode, commanditaire, dateDebut, dateFin, commentaire, projetNom);

            }

            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }

        } catch (IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (ParseException e) {
            throw new BusinessException(e);
        }
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                String projetCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                String siteCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                ProjetSite ps = projetSiteDAO.getBySiteCodeAndProjetCode(siteCode, projetCode);
                ps.getProjet().getProjetsSites().remove(ps);
                ps.getSite().getProjetsSites().remove(ps);
                projetSiteDAO.remove(ps);
            }
        } catch (IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    private void persistProjetSite(ErrorsReport errorsReport, String projetCode, String siteCode, String commanditaire, Date dateDebut, Date dateFin, String commentaire, String projetNom) throws PersistenceException {
        SiteGLACPE site = retrieveDBSite(errorsReport, siteCode);
        Projet projet = retrieveDBProjet(errorsReport, projetCode);

        if (site != null && projet != null) {
            ProjetSite projetSite = retrieveOrCreateProjetSite(projetCode, siteCode, site, projet);
            projetSite.setCommanditaire(commanditaire);
            projetSite.setCommentaire(commentaire);
            projetSite.setDebut(dateDebut);
            projetSite.setFin(dateFin);
        }
    }

    private ProjetSite retrieveOrCreateProjetSite(String projetCode, String siteCode, SiteGLACPE site, Projet projet) throws PersistenceException {
        ProjetSite projetSite = projetSiteDAO.getBySiteCodeAndProjetCode(siteCode, projetCode);
        if (projetSite == null) {
            projetSite = new ProjetSite(site, projet);
            site.getProjetsSites().add(projetSite);
            projet.getProjetsSites().add(projetSite);
        }
        return projetSite;
    }

    private Projet retrieveDBProjet(ErrorsReport errorsReport, String projetCode) throws PersistenceException {
        Projet projet = projetDAO.getByCode(projetCode);
        if (projet == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, MSG_ERROR_CODE_PROJET_NOT_FOUND_IN_DB), projetCode));
        }
        return projet;
    }

    private SiteGLACPE retrieveDBSite(ErrorsReport errorsReport, String siteCode) throws PersistenceException {
        SiteGLACPE site = (SiteGLACPE) siteDAO.getByCode(siteCode);
        if (site == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, MSG_ERROR_CODE_SITE_NOT_FOUND_IN_DB), siteCode));
        }
        return site;
    }

    private void updateNamesSitesPossibles() throws PersistenceException {
        List<Site> sites = siteDAO.getAll();
        String[] namesSitesPossibles = new String[sites.size()];
        int index = 0;
        for (Site site : sites) {
            namesSitesPossibles[index++] = site.getNom();
        }
        this.namesSitesPossibles = namesSitesPossibles;
    }

    private void updateNamesProjetsPossibles() throws PersistenceException {
        List<Projet> projets = projetDAO.getAll();
        String[] namesProjetsPossibles = new String[projets.size()];
        int index = 0;
        for (Projet projet : projets) {
            namesProjetsPossibles[index++] = projet.getNom();
        }
        this.namesProjetsPossibles = namesProjetsPossibles;
    }

    private String buildStringDateDebut(ProjetSite projetSite) {
        String dateDebut = "";
        if (projetSite != null && projetSite.getDebut() != null) {
            dateDebut = new SimpleDateFormat(datasetDescriptor.getColumns().get(4).getFormatType()).format(projetSite.getDebut());
        }
        return dateDebut;
    }

    private String buildStringDateFin(ProjetSite projetSite) {
        String dateFin = "";
        if (projetSite != null && projetSite.getFin() != null) {
            dateFin = new SimpleDateFormat(datasetDescriptor.getColumns().get(4).getFormatType()).format(projetSite.getFin());
        }
        return dateFin;
    }

    @SuppressWarnings("unused")
    private Site retrieveSiteFromProjetSite(ProjetSite projetSite) {
        Site site = null;
        if (projetSite != null) {
            site = projetSite.getSite();
        }
        return site;
    }

    /**
     *
     * @param siteDAO
     */
    public void setSiteDAO(ISiteDAO siteDAO) {
        this.siteDAO = siteDAO;
    }

    /**
     *
     * @param projetSiteDAO
     */
    public void setProjetSiteDAO(IProjetSiteDAO projetSiteDAO) {
        this.projetSiteDAO = projetSiteDAO;
    }

    /**
     *
     * @param projetDAO
     */
    public void setProjetDAO(IProjetDAO projetDAO) {
        this.projetDAO = projetDAO;
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(ProjetSite projetSite) throws PersistenceException {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(projetSite == null ? EMPTY_STRING : projetSite.getProjet().getNom(), namesProjetsPossibles, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(projetSite == null || projetSite.getSite() == null ? EMPTY_STRING : projetSite.getSite().getNom(), namesSitesPossibles, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas()
                .add(new ColumnModelGridMetadata(projetSite == null ? EMPTY_STRING : projetSite != null ? projetSite.getCommanditaire() : "", ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false));

        String dateDebut = projetSite == null ? EMPTY_STRING : buildStringDateDebut(projetSite);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(dateDebut, ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false));

        String dateFin = projetSite == null ? EMPTY_STRING : buildStringDateFin(projetSite);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(dateFin, ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(projetSite == null ? EMPTY_STRING : projetSite.getCommentaire(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false));
        return lineModelGridMetadata;
    }

    @Override
    protected List<ProjetSite> getAllElements() throws PersistenceException {
        return projetSiteDAO.getAll(ProjetSite.class);
    }

    @Override
    protected ModelGridMetadata<ProjetSite> initModelGridMetadata() {
        try {
            updateNamesProjetsPossibles();
            updateNamesSitesPossibles();
        } catch (PersistenceException e) {
            e.printStackTrace();
        }
        return super.initModelGridMetadata();
    }
}
