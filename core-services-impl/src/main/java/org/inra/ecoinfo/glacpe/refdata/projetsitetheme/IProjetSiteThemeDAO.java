package org.inra.ecoinfo.glacpe.refdata.projetsitetheme;

import java.util.List;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public interface IProjetSiteThemeDAO extends IDAO<ProjetSiteTheme> {

    /**
     *
     * @return
     * @throws PersistenceException
     */
    List<ProjetSiteTheme> getAll() throws PersistenceException;

    /**
     *
     * @param projetCode
     * @param siteCode
     * @param themeCode
     * @return
     * @throws PersistenceException
     */
    ProjetSiteTheme getByProjetSiteCodeAndThemeCode(String projetCode, String siteCode, String themeCode) throws PersistenceException;

}
