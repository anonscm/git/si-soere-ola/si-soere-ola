package org.inra.ecoinfo.glacpe.refdata.projetsitetheme;

import java.util.List;

import javax.persistence.Query;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class JPAProjetSiteThemeDAO extends AbstractJPADAO<ProjetSiteTheme> implements IProjetSiteThemeDAO {

    private static final String QUERY_ALL = "from ProjetSiteTheme s";
    private static final String QUERY_BY_NKEY = "from ProjetSiteTheme pst where pst.projetSite.projet.code = :projetCode and pst.projetSite.site.code = :siteCode and pst.theme.code = :themeCode";

    /**
     *
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    @Override
    public List<ProjetSiteTheme> getAll() throws PersistenceException {
        try {
            Query query = entityManager.createQuery(QUERY_ALL);

            return (List) query.getResultList();
        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param projetCode
     * @param siteCode
     * @param themeCode
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings({"rawtypes"})
    @Override
    public ProjetSiteTheme getByProjetSiteCodeAndThemeCode(final String projetCode, final String siteCode, final String themeCode) throws PersistenceException {
        try {
            Query query = entityManager.createQuery(QUERY_BY_NKEY);
            query.setParameter("projetCode", projetCode);
            query.setParameter("siteCode", siteCode);
            query.setParameter("themeCode", themeCode);
            List projetsSitesThemes = query.getResultList();

            ProjetSiteTheme projetSiteTheme = null;
            if (projetsSitesThemes != null && !projetsSitesThemes.isEmpty())
                projetSiteTheme = (ProjetSiteTheme) projetsSitesThemes.get(0);

            return projetSiteTheme;
        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

}
