package org.inra.ecoinfo.glacpe.refdata.projetsitethemedatatype;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.refdata.node.ILeafTreeNode;
import org.inra.ecoinfo.refdata.node.INodeDAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public interface IProjetSiteThemeDatatypeDAO extends IDAO<ILeafTreeNode>, INodeDAO {

    /**
     *
     * @param projetCode
     * @param pathSite
     * @param themeCode
     * @param datatypeCode
     * @return
     * @throws PersistenceException
     */
    ProjetSiteThemeDatatype getByPathProjetSiteThemeCodeAndDatatypeCode(String projetCode, String pathSite, String themeCode, String datatypeCode) throws PersistenceException;
}
