package org.inra.ecoinfo.glacpe.refdata.projetsitethemedatatype;

import java.util.List;

import javax.persistence.Query;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.refdata.node.ILeafTreeNode;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class JPAProjetSiteThemeDatatypeDAO extends AbstractJPADAO<ILeafTreeNode> implements IProjetSiteThemeDatatypeDAO {

    private static final String QUERY_GET_ALL = "from ProjetSiteThemeDatatype pstd order by pstd.projetSiteTheme.projetSite.projet.code, pstd.projetSiteTheme.projetSite.site.code, pstd.projetSiteTheme.theme.code, pstd.datatype.code";
    private static final String QUERY_GET_BY_NKEY = "from ProjetSiteThemeDatatype pstd where pstd.projetSiteTheme.projetSite.projet.code = :projetCode and pstd.projetSiteTheme.projetSite.site.code = :siteCode and pstd.projetSiteTheme.theme.code= :themeCode and pstd.datatype.code= :datatypeCode";

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public List<ILeafTreeNode> getAll() throws PersistenceException {
        try {
            Query query = entityManager.createQuery(QUERY_GET_ALL);
            return (List) query.getResultList();
        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param projetCode
     * @param pathSite
     * @param themeCode
     * @param datatypeCode
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("rawtypes")
    @Override
    public ProjetSiteThemeDatatype getByPathProjetSiteThemeCodeAndDatatypeCode(final String projetCode, final String pathSite, final String themeCode, final String datatypeCode) throws PersistenceException {
        try {
            Query query = entityManager.createQuery(QUERY_GET_BY_NKEY);
            query.setParameter("projetCode", projetCode);
            query.setParameter("siteCode", pathSite);
            query.setParameter("themeCode", themeCode);
            query.setParameter("datatypeCode", datatypeCode);
            List projetSitesThemes = query.getResultList();

            ProjetSiteThemeDatatype projetSiteThemeDatatype = null;
            if (projetSitesThemes != null && !projetSitesThemes.isEmpty())
                projetSiteThemeDatatype = (ProjetSiteThemeDatatype) projetSitesThemes.get(0);

            return projetSiteThemeDatatype;
        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }
}
