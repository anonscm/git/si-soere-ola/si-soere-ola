/**
 * OREILacs project - see LICENCE.txt for use created: 7 avr. 2009 16:17:33
 */
package org.inra.ecoinfo.glacpe.refdata.projetsitethemedatatype;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.inra.ecoinfo.dataset.versioning.IDatasetDAO;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.glacpe.refdata.projet.IProjetDAO;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.projetsite.IProjetSiteDAO;
import org.inra.ecoinfo.glacpe.refdata.projetsite.ProjetSite;
import org.inra.ecoinfo.glacpe.refdata.projetsitetheme.IProjetSiteThemeDAO;
import org.inra.ecoinfo.glacpe.refdata.projetsitetheme.ProjetSiteTheme;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.datatype.IDatatypeDAO;
import org.inra.ecoinfo.refdata.node.ILeafTreeNode;
import org.inra.ecoinfo.refdata.site.ISiteDAO;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.refdata.theme.IThemeDAO;
import org.inra.ecoinfo.refdata.theme.Theme;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import com.Ostermiller.util.CSVParser;

/**
 * @author "Guillaume Enrico"
 *
 */
public class Recorder extends AbstractCSVMetadataRecorder<ProjetSiteThemeDatatype> {
    
    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.dataset.refdata.sitethemedatatype.messages";
    
    private static final String PROPERTY_MSG_ERROR_CODE_PROJET_NOT_FOUND_IN_DB = "PROPERTY_MSG_ERROR_CODE_PROJET_NOT_FOUND_IN_DB";
    
    private static final String PROPERTY_MSG_ERROR_CODE_DATATYPE_NOT_FOUND_IN_DB = "PROPERTY_MSG_ERROR_CODE_DATATYPE_NOT_FOUND_IN_DB";
    private static final String PROPERTY_MSG_ERROR_CODE_SITE_NOT_FOUND_IN_DB = "PROPERTY_MSG_ERROR_CODE_SITE_NOT_FOUND_IN_DB";
    private static final String PROPERTY_MSG_ERROR_CODE_THEME_NOT_FOUND_IN_DB = "PROPERTY_MSG_ERROR_CODE_THEME_NOT_FOUND_IN_DB";
    
    private Map<String, String[]> datatypesPossibles = new ConcurrentHashMap<String, String[]>();

    /**
     *
     */
    protected ISiteDAO siteDAO;

    /**
     *
     */
    protected IThemeDAO themeDAO;

    /**
     *
     */
    protected IProjetDAO projetDAO;

    /**
     *
     */
    protected IDatatypeDAO datatypeDAO;
    private Map<String, String[]> projetsPossibles = new ConcurrentHashMap<String, String[]>();
    private Map<String, String[]> sitesPossibles = new ConcurrentHashMap<String, String[]>();
    private Map<String, String[]> themesPossibles = new ConcurrentHashMap<String, String[]>();

    /**
     *
     */
    protected IProjetSiteDAO projetSiteDAO;

    /**
     *
     */
    protected IProjetSiteThemeDAO projetSiteThemeDAO;

    /**
     *
     */
    protected IProjetSiteThemeDatatypeDAO projetSiteThemeDatatypeDAO;
    /**
     * The dataset dao @link(IDatasetDAO).
     */
    protected IDatasetDAO datasetDAO;
    
    @Override
    public void deleteRecord(final CSVParser parser, final File file, final String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            final ErrorsReport errorsReport = new ErrorsReport();
            while (values != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                String projetCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                String pathSite = Utils.createCodeFromString(tokenizerValues.nextToken());
                String themeCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                String datatypeCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                ProjetSiteThemeDatatype pstd = projetSiteThemeDatatypeDAO.getByPathProjetSiteThemeCodeAndDatatypeCode(projetCode, pathSite, themeCode, datatypeCode);
                List<Dataset> dataset = datasetDAO.retrieveDatasets(pstd);
                if (dataset == null || dataset.isEmpty()) {
                    pstd.getProjetSiteTheme().getProjetSitesThemesDatatypes().remove(pstd);
                    projetSiteThemeDatatypeDAO.remove(pstd);
                    ProjetSiteTheme pst = projetSiteThemeDAO.getByProjetSiteCodeAndThemeCode(projetCode, pathSite, themeCode);
                    if (pst.getProjetSitesThemesDatatypes().isEmpty()) {
                        projetSiteThemeDAO.remove(pst);
                    }
                } else {
                    throw new BusinessException(errorsReport.getErrorsMessages());
                }
                values = parser.getLine();
            }
        } catch (IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }
    
    @Override
    protected List<ProjetSiteThemeDatatype> getAllElements() throws PersistenceException {
        List<ILeafTreeNode> nodes = projetSiteThemeDatatypeDAO.getAll();
        List<ProjetSiteThemeDatatype> projetSiteThemeDatatypes = new LinkedList<ProjetSiteThemeDatatype>();
        for (ILeafTreeNode node : nodes) {
            projetSiteThemeDatatypes.add((ProjetSiteThemeDatatype) node);
        }
        return projetSiteThemeDatatypes;
    }
    
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(final ProjetSiteThemeDatatype projetSiteThemeDatatype) throws javax.persistence.PersistenceException, PersistenceException {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(projetSiteThemeDatatype == null ? EMPTY_STRING : projetSiteThemeDatatype.getProjetSiteTheme().getProjetSite().getProjet().getNom(), projetsPossibles, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(projetSiteThemeDatatype == null ? EMPTY_STRING : projetSiteThemeDatatype.getProjetSiteTheme().getProjetSite().getSite().getPath(), sitesPossibles, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(projetSiteThemeDatatype == null ? EMPTY_STRING : projetSiteThemeDatatype.getProjetSiteTheme().getTheme().getName(), themesPossibles, null, true, false, true));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(projetSiteThemeDatatype == null ? EMPTY_STRING : projetSiteThemeDatatype.getDatatype().getName(), datatypesPossibles, null, true, false, true));
        return lineModelGridMetadata;
    }
    
    @Override
    protected ModelGridMetadata<ProjetSiteThemeDatatype> initModelGridMetadata() {
        try {
            updatenamesProjetsPossibles();
            updatePathsSitesPossibles();
            updateNamesThemesPossibles();
            updateNamesDatatypesPossibles();
        } catch (PersistenceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return super.initModelGridMetadata();
    }
    
    private void persistProjetSiteThemeDatatype(ErrorsReport errorsReport, final String projetCode, final String pathSite, final String themeCode, final String datatypeCode) throws PersistenceException {
        
        ProjetSiteTheme projetSiteTheme = retrieveOrCreateDBProjetSiteTheme(errorsReport, projetCode, pathSite, themeCode);
        if (datatypeCode != null && !datatypeCode.trim().isEmpty()) {
            DataType datatype = retrieveDBDatatype(errorsReport, datatypeCode);
            
            if (!errorsReport.hasErrors() && datatype != null) {
                ProjetSiteThemeDatatype projetSiteThemeDatatype = (ProjetSiteThemeDatatype) projetSiteThemeDatatypeDAO.getByPathProjetSiteThemeCodeAndDatatypeCode(projetSiteTheme.getProjetSite().getProjet().getCode(), projetSiteTheme.getProjetSite()
                        .getSite().getPath(), themeCode, datatypeCode);
                
                if (projetSiteThemeDatatype == null) {
                    projetSiteThemeDatatype = new ProjetSiteThemeDatatype(datatype, projetSiteTheme);
                    projetSiteThemeDatatypeDAO.saveOrUpdate(projetSiteThemeDatatype);
                }
                
            }
        }
    }

    /**
     * @param parser
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @throws IOException
     */
    @Override
    public void processRecord(final CSVParser parser, final File file, final String encoding) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport();
        try {
            
            skipHeader(parser);

            // On parcourt chaque ligne du fichier
            String[] values = parser.getLine();
            
            while (values != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                
                String projetCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                String pathSite = Utils.createCodeFromString(tokenizerValues.nextToken());
                String themeCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                String datatypeCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                
                persistProjetSiteThemeDatatype(errorsReport, projetCode, pathSite, themeCode, datatypeCode);
                values = parser.getLine();
            }
            
            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }
            
        } catch (IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }
    
    private Projet retrieveDBProjet(ErrorsReport errorsReport, final String projetCode) throws PersistenceException {
        Projet projet = (Projet) projetDAO.getByCode(projetCode);
        if (projet == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_ERROR_CODE_PROJET_NOT_FOUND_IN_DB), projetCode));
        }
        return projet;
    }
    
    private DataType retrieveDBDatatype(ErrorsReport errorsReport, final String datatypeCode) throws PersistenceException {
        DataType datatype = (DataType) datatypeDAO.getByCode(datatypeCode);
        if (datatype == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_ERROR_CODE_DATATYPE_NOT_FOUND_IN_DB), datatypeCode));
        }
        return datatype;
    }
    
    private Site retrieveDBSite(ErrorsReport errorsReport, final String pathSite) throws PersistenceException {
        Site site = siteDAO.getByCode(pathSite);
        if (site == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_ERROR_CODE_SITE_NOT_FOUND_IN_DB), pathSite));
        }
        return site;
    }
    
    private Theme retrieveDBTheme(ErrorsReport errorsReport, final String themeCode) throws PersistenceException {
        Theme theme = (Theme) themeDAO.getByCode(themeCode);
        if (theme == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_ERROR_CODE_THEME_NOT_FOUND_IN_DB), themeCode));
        }
        return theme;
    }
    
    private ProjetSiteTheme retrieveOrCreateDBProjetSiteTheme(ErrorsReport errorsReport, final String projetCode, final String pathSite, final String themeCode) throws PersistenceException {
        ProjetSiteTheme projetSiteTheme = (ProjetSiteTheme) projetSiteThemeDAO.getByProjetSiteCodeAndThemeCode(projetCode, pathSite, themeCode);
        if (projetSiteTheme == null) {
            
            ProjetSite projetSite = retrieveOrCreateDBProjetSite(errorsReport, projetCode, pathSite);
            Theme dbTheme = (Theme) retrieveDBTheme(errorsReport, themeCode);
            projetSiteTheme = new ProjetSiteTheme(projetSite, dbTheme);
            projetSiteThemeDAO.saveOrUpdate(projetSiteTheme);
        }
        return projetSiteTheme;
    }
    
    private ProjetSite retrieveOrCreateDBProjetSite(ErrorsReport errorsReport, final String projetCode, final String sitePath) throws PersistenceException {
        ProjetSite projetSite = (ProjetSite) projetSiteDAO.getBySiteCodeAndProjetCode(sitePath, projetCode);
        if (projetSite == null) {
            SiteGLACPE site = (SiteGLACPE) retrieveDBSite(errorsReport, sitePath);
            Projet projet = retrieveDBProjet(errorsReport, projetCode);
            projetSite = new ProjetSite(site, projet);
            projetSiteDAO.saveOrUpdate(projetSite);
        }
        return projetSite;
    }

    /**
     *
     * @param datatypeDAO
     */
    public void setDatatypeDAO(final IDatatypeDAO datatypeDAO) {
        this.datatypeDAO = datatypeDAO;
    }

    /**
     *
     * @param siteDAO
     */
    public void setSiteDAO(final ISiteDAO siteDAO) {
        this.siteDAO = siteDAO;
    }

    /**
     *
     * @param projetSiteThemeDAO
     */
    public void setProjetSiteThemeDAO(final IProjetSiteThemeDAO projetSiteThemeDAO) {
        this.projetSiteThemeDAO = projetSiteThemeDAO;
    }

    /**
     *
     * @param projetSiteThemeDatatypeDAO
     */
    public void setProjetSiteThemeDatatypeDAO(final IProjetSiteThemeDatatypeDAO projetSiteThemeDatatypeDAO) {
        this.projetSiteThemeDatatypeDAO = projetSiteThemeDatatypeDAO;
    }

    /**
     *
     * @param projetSiteDAO
     */
    public void setProjetSiteDAO(final IProjetSiteDAO projetSiteDAO) {
        this.projetSiteDAO = projetSiteDAO;
    }

    /**
     *
     * @param projetDAO
     */
    public void setProjetDAO(final IProjetDAO projetDAO) {
        this.projetDAO = projetDAO;
    }

    /**
     *
     * @param themeDAO
     */
    public void setThemeDAO(final IThemeDAO themeDAO) {
        this.themeDAO = themeDAO;
    }
    
    private void updateNamesDatatypesPossibles() throws PersistenceException {
        List<DataType> datatypes = datatypeDAO.getAll(DataType.class);
        String[] namesDatatypesPossibles = new String[datatypes.size()];
        int index = 0;
        for (DataType datatype : datatypes) {
            namesDatatypesPossibles[index++] = datatype.getName();
        }
        datatypesPossibles.put(ColumnModelGridMetadata.NULL_KEY, namesDatatypesPossibles);
    }
    
    private void updateNamesThemesPossibles() throws PersistenceException {
        List<Theme> themes = themeDAO.getAll(Theme.class);
        String[] namesThemesPossibles = new String[themes.size()];
        int index = 0;
        for (Theme theme : themes) {
            namesThemesPossibles[index++] = theme.getName();
        }
        themesPossibles.put(ColumnModelGridMetadata.NULL_KEY, namesThemesPossibles);
    }
    
    private void updatePathsSitesPossibles() throws PersistenceException {
        List<Site> sites = siteDAO.getAll(Site.class);
        String[] pathsSitesPossibles = new String[sites.size()];
        int index = 0;
        for (Site site : sites) {
            pathsSitesPossibles[index++] = site.getPath();
        }
        sitesPossibles.put(ColumnModelGridMetadata.NULL_KEY, pathsSitesPossibles);
    }
    
    private void updatenamesProjetsPossibles() throws PersistenceException {
        List<Projet> projets = projetDAO.getAll(Projet.class);
        String[] pathsProjetsPossibles = new String[projets.size()];
        int index = 0;
        for (Projet projet : projets) {
            pathsProjetsPossibles[index++] = projet.getNom();
        }
        projetsPossibles.put(ColumnModelGridMetadata.NULL_KEY, pathsProjetsPossibles);
    }

    /**
     *
     * @return
     */
    public Map<String, String[]> getDatatypesPossibles() {
        return datatypesPossibles;
    }

    /**
     *
     * @param datatypesPossibles
     */
    public void setDatatypesPossibles(final Map<String, String[]> datatypesPossibles) {
        this.datatypesPossibles = datatypesPossibles;
    }

    /**
     *
     * @return
     */
    public Map<String, String[]> getProjetsPossibles() {
        return projetsPossibles;
    }

    /**
     *
     * @param projetsPossibles
     */
    public void setProjetsPossibles(final Map<String, String[]> projetsPossibles) {
        this.projetsPossibles = projetsPossibles;
    }

    /**
     *
     * @return
     */
    public Map<String, String[]> getSitesPossibles() {
        return sitesPossibles;
    }

    /**
     *
     * @param sitesPossibles
     */
    public void setSitesPossibles(final Map<String, String[]> sitesPossibles) {
        this.sitesPossibles = sitesPossibles;
    }

    /**
     *
     * @return
     */
    public Map<String, String[]> getThemesPossibles() {
        return themesPossibles;
    }

    /**
     *
     * @param themesPossibles
     */
    public void setThemesPossibles(final Map<String, String[]> themesPossibles) {
        this.themesPossibles = themesPossibles;
    }

    /**
     *
     * @return
     */
    public ISiteDAO getSiteDAO() {
        return siteDAO;
    }

    /**
     *
     * @return
     */
    public IThemeDAO getThemeDAO() {
        return themeDAO;
    }

    /**
     *
     * @return
     */
    public IProjetDAO getProjetDAO() {
        return projetDAO;
    }

    /**
     *
     * @return
     */
    public IDatatypeDAO getDatatypeDAO() {
        return datatypeDAO;
    }

    /**
     *
     * @return
     */
    public IProjetSiteDAO getProjetSiteDAO() {
        return projetSiteDAO;
    }

    /**
     *
     * @return
     */
    public IProjetSiteThemeDAO getProjetSiteThemeDAO() {
        return projetSiteThemeDAO;
    }

    /**
     *
     * @return
     */
    public IProjetSiteThemeDatatypeDAO getProjetSiteThemeDatatypeDAO() {
        return projetSiteThemeDatatypeDAO;
    }

    /**
     *
     * @param datasetDAO
     */
    public void setDatasetDAO(IDatasetDAO datasetDAO) {
        this.datasetDAO = datasetDAO;
    }
    
}
