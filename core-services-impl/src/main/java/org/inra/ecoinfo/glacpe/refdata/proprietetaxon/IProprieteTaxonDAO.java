/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:53:17
 */
package org.inra.ecoinfo.glacpe.refdata.proprietetaxon;

import java.util.List;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public interface IProprieteTaxonDAO extends IDAO<ProprieteTaxon> {

    /**
     *
     * @param code
     * @return
     * @throws PersistenceException
     */
    public ProprieteTaxon getByCode(String code) throws PersistenceException;

    /**
     *
     * @return
     * @throws PersistenceException
     */
    public List<ProprieteTaxon> getAllQualitativeValues() throws PersistenceException;

    /**
     *
     * @param type
     * @return
     * @throws PersistenceException
     */
    public List<ProprieteTaxon> getAllProprietesTaxonsByType(String type) throws PersistenceException;
}
