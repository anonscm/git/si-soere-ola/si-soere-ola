/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:50:44
 */
package org.inra.ecoinfo.glacpe.refdata.proprietetaxon;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.Query;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public class JPAProprieteTaxonDAO extends AbstractJPADAO<ProprieteTaxon> implements IProprieteTaxonDAO {

    /**
     *
     * @param code
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("rawtypes")
    @Override
    public ProprieteTaxon getByCode(String code) throws PersistenceException {
        try {
            String queryString = "from ProprieteTaxon p where p.code = :code";
            Query query = entityManager.createQuery(queryString);
            query.setParameter("code", code);
            List proprietesTaxons = query.getResultList();

            ProprieteTaxon proprieteTaxon = null;
            if (proprietesTaxons != null && !proprietesTaxons.isEmpty())
                proprieteTaxon = (ProprieteTaxon) proprietesTaxons.get(0);

            return proprieteTaxon;
        } catch (Exception e) {
            AbstractJPADAO.LOGGER.error(e.getMessage());
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     *
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public List<ProprieteTaxon> getAllQualitativeValues() throws PersistenceException {
        try {
            String queryString = "from ProprieteTaxon p where p.isQualitative = true";
            Query query = entityManager.createQuery(queryString);
            List proprietesTaxons = query.getResultList();

            return proprietesTaxons;
        } catch (Exception e) {
            AbstractJPADAO.LOGGER.error(e.getMessage());
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param type
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public List<ProprieteTaxon> getAllProprietesTaxonsByType(String type) throws PersistenceException {
        try {
            List<String> types = new LinkedList<String>();
            types.add(type);
            types.add("");
            String queryString = "from ProprieteTaxon p where p.typeTaxon in (:type)";
            Query query = entityManager.createQuery(queryString);
            query.setParameter("type", types);
            List proprietesTaxons = query.getResultList();

            return proprietesTaxons;
        } catch (Exception e) {
            AbstractJPADAO.LOGGER.error(e.getMessage());
            throw new PersistenceException(e.getMessage(), e);
        }
    }

}
