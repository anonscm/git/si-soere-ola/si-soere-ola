package org.inra.ecoinfo.glacpe.refdata.site;

import java.util.List;

import org.inra.ecoinfo.refdata.site.ISiteDAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public interface ISiteGLACPEDAO extends ISiteDAO {

    /**
     *
     * @param id
     * @return
     */
    List<SiteGLACPE> getByTyepsiteId(Long id);

    /**
     *
     * @param path
     * @return
     * @throws PersistenceException
     */
    public SiteGLACPE getByPath(String path) throws PersistenceException;

    /**
     *
     * @return
     * @throws PersistenceException
     */
    public List<SiteGLACPE> getAllSitesGLACPE() throws PersistenceException;

    @Override
    SiteGLACPE getByCode(String code) throws PersistenceException;
}
