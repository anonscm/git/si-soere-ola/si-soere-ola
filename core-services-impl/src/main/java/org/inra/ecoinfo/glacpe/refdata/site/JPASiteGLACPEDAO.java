package org.inra.ecoinfo.glacpe.refdata.site;

import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.inra.ecoinfo.refdata.site.JPASiteDAO;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class JPASiteGLACPEDAO extends JPASiteDAO implements ISiteGLACPEDAO {

    private static final String QUERY_HQL_GET_BY_CODE = "from SiteGLACPE sg wheresg.typesite.id = :id";

    /**
     *
     * @param id
     * @return
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<SiteGLACPE> getByTyepsiteId(Long id) {
        String queryString = String.format(QUERY_HQL_GET_BY_CODE);
        Query query = entityManager.createQuery(queryString);
        query.setParameter("id", id);
        List<SiteGLACPE> sitesGLACPE;
        try {
            sitesGLACPE = (List<SiteGLACPE>) query.getResultList();
        } catch (NoResultException e) {
            return null;
        }

        return sitesGLACPE;
    }

    /**
     *
     * @param path
     * @return
     * @throws PersistenceException
     */
    @Override
    public SiteGLACPE getByPath(String path) throws PersistenceException {
        if (path == null)
            return null;
        try {
            List<Site> sites = getAll();
            for (Site site : sites)
                if (path.equalsIgnoreCase(site.getPath()))
                    return (SiteGLACPE) site;
            return null;
        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public SiteGLACPE getByCode(String code) throws PersistenceException {
        try {
            if (code == null)
                return null;
            String queryString = "from SiteGLACPE s where s.codeFromName = :code";
            Query query = entityManager.createQuery(queryString);
            query.setParameter("code", code);
            List<SiteGLACPE> sites = query.getResultList();
            if (sites.isEmpty()) {
                queryString = "from SiteGLACPE s where s.code = :code";
                query = entityManager.createQuery(queryString);
                query.setParameter("code", code);
                sites = query.getResultList();
            }

            SiteGLACPE site = null;
            if (sites != null && !sites.isEmpty())
                return sites.get(0);
            return site;
        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     *
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("unchecked")
    public List<SiteGLACPE> getAllSitesGLACPE() throws PersistenceException {
        String queryString = "from SiteGLACPE";
        Query query = entityManager.createQuery(queryString);
        List<SiteGLACPE> sites = query.getResultList();
        return sites;
    }
}
