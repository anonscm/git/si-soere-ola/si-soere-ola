/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:53:17
 */
package org.inra.ecoinfo.glacpe.refdata.taxon;

import java.util.List;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public interface ITaxonDAO extends IDAO<Taxon> {

    /**
     *
     * @param code
     * @return
     * @throws PersistenceException
     */
    Taxon getByCode(String code) throws PersistenceException;

    /**
     * 
     * @param parentTaxonId
     * @return Les Ids des taxons composants l'arborescence complète du taxon d'id parentTaxonId
     * @throws PersistenceException
     */
    List<Long> getAllSubtaxaIdsAndParentTaxaId(Long parentTaxonId) throws PersistenceException;

    /**
     *
     * @return
     * @throws PersistenceException
     */
    List<Taxon> getPhytoRootTaxa() throws PersistenceException;

    /**
     *
     * @return
     * @throws PersistenceException
     */
    List<Taxon> getZooRootTaxa() throws PersistenceException;

    /**
     *
     * @param nom
     * @return
     * @throws PersistenceException
     */
    Taxon getByNomLatin(String nom) throws PersistenceException;

    /**
     *
     * @param taxonId
     * @return
     * @throws PersistenceException
     */
    List<TaxonProprieteTaxon> getTaxonProprieteTaxonByTaxonId(long taxonId) throws PersistenceException;

    /**
     *
     * @param theme
     * @return
     * @throws PersistenceException
     */
    List<String> getTaxonNameByTheme(String theme) throws PersistenceException;

    /**
     *
     * @param theme
     * @return
     * @throws PersistenceException
     */
    List<Taxon> getTaxonByTheme(String theme) throws PersistenceException;

    /**
     *
     * @return
     * @throws PersistenceException
     */
    List<Taxon> getPhytoTaxonInfos() throws PersistenceException;
    
 
}
