/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:50:44
 */
package org.inra.ecoinfo.glacpe.refdata.taxon;

import java.util.List;

import javax.persistence.Query;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public class JPATaxonDAO extends AbstractJPADAO<Taxon> implements ITaxonDAO {

    private static final String REQUEST_GET_SUBTAXA = "select t.id from Taxon t where t.taxonParent.id in (:taxaIds)";
    private static final String REQUEST_GET_SUBTAXA_IDS_AND_PARENT_ID = "select t.id from Taxon t where t.taxonParent.id = :taxonParentId";
    private static final String REQUEST_GET_TAXA_BY_NOMLATIN = "from Taxon t where t.nomLatin = :nom";
    private static final String REQUEST_GET_TAXA_BY_CODE = "from Taxon t where t.code = :code";
    private static final String REQUEST_GET_TAXA_NAME_BY_THEME = "select t.nomLatin from Taxon t where t.theme = :theme order by t.nomLatin";
    private static final String REQUEST_GET_TAXA_BY_THEME = "from Taxon t where t.theme = :theme order by t.nomLatin";
    private static final String REQUEST_GET_ALL_PHYTO_ROOT_TAXA = "from Taxon t where t.taxonParent is null and t.theme = 'Phytoplancton' order by t.nomLatin";
    private static final String REQUEST_GET_ALL_ZOO_ROOT_TAXA = "from Taxon t where t.taxonParent is null and t.theme = 'Zooplancton' order by t.nomLatin";
    private static final String REQUEST_GET_TAXAPROPERTIES_BY_TAXA_ID = "from TaxonProprieteTaxon tpt where tpt.taxon.id = :taxonId";
    private static final String REQUEST_GET_PHYTO_TAXA_INFOS = "from Taxon t where t.theme = 'Phytoplancton' order by t.nomLatin";
    private static final String REQUEST_GET_PHYTO_TAXA_INFOS_WITH_DATA = "select m.taxon from MesurePhytoplancton m where m.taxon.theme = 'Phytoplancton' order by m.taxon.nomLatin";

    /**
     *
     * @param code
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("rawtypes")
    @Override
    public Taxon getByCode(String code) throws PersistenceException {
        try {
            String queryString = REQUEST_GET_TAXA_BY_CODE;
            Query query = entityManager.createQuery(queryString);
            query.setParameter("code", code);
            List taxons = query.getResultList();

            Taxon taxon = null;
            if (taxons != null && !taxons.isEmpty())
                taxon = (Taxon) taxons.get(0);

            return taxon;
        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param nom
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("rawtypes")
    @Override
    public Taxon getByNomLatin(String nom) throws PersistenceException {
        try {
            String queryString = REQUEST_GET_TAXA_BY_NOMLATIN;
            Query query = entityManager.createQuery(queryString);
            query.setParameter("nom", nom);
            List taxons = query.getResultList();

            Taxon taxon = null;
            if (taxons != null && !taxons.isEmpty())
                taxon = (Taxon) taxons.get(0);

            return taxon;
        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     *
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("unchecked")
    @Override
    @Transactional(readOnly = true, propagation = Propagation.MANDATORY)
    public List<Taxon> getPhytoRootTaxa() throws PersistenceException {
        try {

            String queryString = REQUEST_GET_ALL_PHYTO_ROOT_TAXA;
            Query query = entityManager.createQuery(queryString);
            return query.getResultList();
        } catch (Exception e) {
            AbstractJPADAO.LOGGER.error(Utils.getLastCauseExceptionMessage(e));
            throw new PersistenceException(e);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Long> getAllSubtaxaIdsAndParentTaxaId(Long parentTaxonId) throws PersistenceException {
        try {
            String queryString = REQUEST_GET_SUBTAXA_IDS_AND_PARENT_ID;
            Query query = entityManager.createQuery(queryString);
            long taxonParentId = parentTaxonId;
            query.setParameter("taxonParentId", taxonParentId);
            List<Long> taxaIds = query.getResultList();
            if (taxaIds.size() > 0) {
                taxaIds.addAll(getAllSubTaxa(taxaIds));
            }
            taxaIds.add(parentTaxonId);

            return taxaIds;
        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    @SuppressWarnings("unchecked")
    private List<Long> getAllSubTaxa(List<Long> taxaIds) {
        String queryString = REQUEST_GET_SUBTAXA;
        Query query = entityManager.createQuery(queryString);
        query.setParameter("taxaIds", taxaIds);
        List<Long> subTaxaIds = query.getResultList();
        if (subTaxaIds.size() > 0) {
            taxaIds.addAll(getAllSubTaxa(subTaxaIds));
        }

        return subTaxaIds;
    }

    /**
     *
     * @param theme
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<String> getTaxonNameByTheme(String theme) throws PersistenceException {
        try {
            String queryString = REQUEST_GET_TAXA_NAME_BY_THEME;
            Query query = entityManager.createQuery(queryString);
            query.setParameter("theme", theme);
            return query.getResultList();
        } catch (Exception e) {
            AbstractJPADAO.LOGGER.error(Utils.getLastCauseExceptionMessage(e));
            throw new PersistenceException(e);
        }
    }

    /**
     *
     * @param taxonId
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<TaxonProprieteTaxon> getTaxonProprieteTaxonByTaxonId(long taxonId) throws PersistenceException {
        try {
            String queryString = REQUEST_GET_TAXAPROPERTIES_BY_TAXA_ID;
            Query query = entityManager.createQuery(queryString);
            query.setParameter("taxonId", taxonId);
            List<TaxonProprieteTaxon> taxonProprietesTaxons = query.getResultList();

            return taxonProprietesTaxons;
        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param theme
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Taxon> getTaxonByTheme(String theme) throws PersistenceException {
        try {
            String queryString = REQUEST_GET_TAXA_BY_THEME;
            Query query = entityManager.createQuery(queryString);
            query.setParameter("theme", theme);
            return query.getResultList();
        } catch (Exception e) {
            AbstractJPADAO.LOGGER.error(Utils.getLastCauseExceptionMessage(e));
            throw new PersistenceException(e);
        }
    }

    /**
     *
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Taxon> getPhytoTaxonInfos() throws PersistenceException {
        try {
            String queryString = REQUEST_GET_PHYTO_TAXA_INFOS;
            Query query = entityManager.createQuery(queryString);

            return query.getResultList();
        } catch (Exception e) {
            AbstractJPADAO.LOGGER.error(Utils.getLastCauseExceptionMessage(e));
            throw new PersistenceException(e);
        }
    }
     

    /**
     *
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Taxon> getZooRootTaxa() throws PersistenceException {
        try {

            String queryString = REQUEST_GET_ALL_ZOO_ROOT_TAXA;
            Query query = entityManager.createQuery(queryString);
            return query.getResultList();
        } catch (Exception e) {
            AbstractJPADAO.LOGGER.error(Utils.getLastCauseExceptionMessage(e));
            throw new PersistenceException(e);
        }
    }
}
