/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:50:44
 */
package org.inra.ecoinfo.glacpe.refdata.taxonniveau;

import java.util.List;

import javax.persistence.Query;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public class JPATaxonNiveauDAO extends AbstractJPADAO<TaxonNiveau> implements ITaxonNiveauDAO {

    /**
     *
     * @param code
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("rawtypes")
    @Override
    public TaxonNiveau getByCode(String code) throws PersistenceException {
        try {
            String queryString = "from TaxonNiveau t where t.code = :code";
            Query query = entityManager.createQuery(queryString);
            query.setParameter("code", code);
            List taxonsNiveaux = query.getResultList();

            TaxonNiveau taxonNiveau = null;
            if (taxonsNiveaux != null && !taxonsNiveaux.isEmpty())
                taxonNiveau = (TaxonNiveau) taxonsNiveaux.get(0);

            return taxonNiveau;
        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

}
