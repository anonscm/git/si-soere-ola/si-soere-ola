/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:53:17
 */
package org.inra.ecoinfo.glacpe.refdata.typeoutilsmesure;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public interface ITypeOutilsMesureDAO extends IDAO<TypeOutilsMesure> {

    /**
     *
     * @param code
     * @return
     * @throws PersistenceException
     */
    public TypeOutilsMesure getByCode(String code) throws PersistenceException;
}
