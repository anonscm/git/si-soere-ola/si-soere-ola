/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:50:44
 */
package org.inra.ecoinfo.glacpe.refdata.typeoutilsmesure;

import java.util.List;

import javax.persistence.Query;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public class JPATypeOutilsMesureDAO extends AbstractJPADAO<TypeOutilsMesure> implements ITypeOutilsMesureDAO {

    /**
     *
     * @param code
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("rawtypes")
    @Override
    public TypeOutilsMesure getByCode(String code) throws PersistenceException {
        try {
            String queryString = "from TypeOutilsMesure t where t.code = :code";
            Query query = entityManager.createQuery(queryString);
            query.setParameter("code", code);
            List typesOutilsMesures = query.getResultList();

            TypeOutilsMesure typeOutilsMesure = null;
            if (typesOutilsMesures != null && !typesOutilsMesures.isEmpty())
                typeOutilsMesure = (TypeOutilsMesure) typesOutilsMesures.get(0);

            return typeOutilsMesure;
        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

}
