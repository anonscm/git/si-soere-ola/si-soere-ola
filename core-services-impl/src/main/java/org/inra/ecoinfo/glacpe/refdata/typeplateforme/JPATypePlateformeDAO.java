/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:50:44
 */
package org.inra.ecoinfo.glacpe.refdata.typeplateforme;

import java.util.List;

import javax.persistence.Query;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public class JPATypePlateformeDAO extends AbstractJPADAO<TypePlateforme> implements ITypePlateformeDAO {

    /**
     *
     * @param code
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("rawtypes")
    @Override
    public TypePlateforme getByCode(String code) throws PersistenceException {
        try {
            String queryString = "from TypePlateforme t where t.code = :code";
            Query query = entityManager.createQuery(queryString);
            query.setParameter("code", code);
            List typesPlateformes = query.getResultList();

            TypePlateforme typePlateforme = null;
            if (typesPlateformes != null && !typesPlateformes.isEmpty())
                typePlateforme = (TypePlateforme) typesPlateformes.get(0);

            return typePlateforme;
        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

}
