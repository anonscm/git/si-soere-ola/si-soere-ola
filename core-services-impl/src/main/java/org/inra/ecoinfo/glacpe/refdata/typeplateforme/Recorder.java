/**
 * OREILacs project - see LICENCE.txt for use created: 7 avr. 2009 16:17:33
 */
package org.inra.ecoinfo.glacpe.refdata.typeplateforme;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import com.Ostermiller.util.CSVParser;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public class Recorder extends AbstractCSVMetadataRecorder<TypePlateforme> {

    /**
     *
     */
    protected ITypePlateformeDAO typePlateformeDAO;
    private Properties propertiesNomEN;
    private Properties propertiesDescriptionEN;

    /**
     * @param parser
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @throws IOException
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {

            skipHeader(parser);

            // On parcourt chaque ligne du fichier
            String[] values = null;
            int lineNumber = 0;
            while ((values = parser.getLine()) != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values, TypePlateforme.TABLE_NAME);
                lineNumber++;

                testColumnsNumbers(values, lineNumber);

                // On parcourt chaque colonne d'une ligne
                String nom = tokenizerValues.nextToken();
                String description = tokenizerValues.nextToken();
                
                String codeSandre = tokenizerValues.nextToken();
                String contexte = tokenizerValues.nextToken();
               

                TypePlateforme typePlateforme = new TypePlateforme(nom, description);
                TypePlateforme dbTypePlateforme = typePlateformeDAO.getByCode(typePlateforme.getCode());

                // Enregistre un site uniquement s'il n'existe pas en BD ou bien
                // s'il est considéré comme une mise à jour
                if (dbTypePlateforme == null) {
                    typePlateforme.setCodeSandre(codeSandre);
                    typePlateforme.setContexte(contexte);
                    typePlateformeDAO.saveOrUpdate(typePlateforme);
                } else {
                    dbTypePlateforme.setCodeSandre(codeSandre);
                    dbTypePlateforme.setContexte(contexte);
                    dbTypePlateforme.setDescription(description);
                    typePlateformeDAO.saveOrUpdate(dbTypePlateforme);
                }
            }

        } catch (IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                String code = Utils.createCodeFromString(tokenizerValues.nextToken());
                typePlateformeDAO.remove(typePlateformeDAO.getByCode(code));
            }
        } catch (IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param typePlateformeDAO
     */
    public void setTypePlateformeDAO(ITypePlateformeDAO typePlateformeDAO) {
        this.typePlateformeDAO = typePlateformeDAO;
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(TypePlateforme typePlateforme) throws PersistenceException {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(typePlateforme == null ? EMPTY_STRING : typePlateforme.getNom(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(typePlateforme == null ? EMPTY_STRING : propertiesNomEN.get(typePlateforme.getNom()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(typePlateforme == null ? EMPTY_STRING : typePlateforme.getDescription(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(typePlateforme == null ? EMPTY_STRING : propertiesDescriptionEN.get(typePlateforme.getDescription()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(typePlateforme == null ? EMPTY_STRING : typePlateforme.getCodeSandre() , ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(typePlateforme == null ? EMPTY_STRING : typePlateforme.getContexte() , ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        
        return lineModelGridMetadata;
    }

    @Override
    protected ModelGridMetadata<TypePlateforme> initModelGridMetadata() {
        propertiesNomEN = localizationManager.newProperties(TypePlateforme.TABLE_NAME, "nom", Locale.ENGLISH);
        propertiesDescriptionEN = localizationManager.newProperties(TypePlateforme.TABLE_NAME, "description", Locale.ENGLISH);
        return super.initModelGridMetadata();
    }

    @Override
    protected List<TypePlateforme> getAllElements() throws PersistenceException {
        return typePlateformeDAO.getAll(TypePlateforme.class);
    }

}
