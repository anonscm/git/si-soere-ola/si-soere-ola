/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:50:44
 */
package org.inra.ecoinfo.glacpe.refdata.typesite;

import java.util.List;

import javax.persistence.Query;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public class JPATypeSiteDAO extends AbstractJPADAO<TypeSite> implements ITypeSiteDAO {

    /**
     *
     * @param code
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("rawtypes")
    @Override
    public TypeSite getByCode(String code) throws PersistenceException {
        try {
            String queryString = "from TypeSite s where s.code = :code";
            Query query = entityManager.createQuery(queryString);
            query.setParameter("code", code);
            List typesSites = query.getResultList();

            TypeSite typeSite = null;
            if (typesSites != null && !typesSites.isEmpty())
                typeSite = (TypeSite) typesSites.get(0);

            return typeSite;
        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

}
