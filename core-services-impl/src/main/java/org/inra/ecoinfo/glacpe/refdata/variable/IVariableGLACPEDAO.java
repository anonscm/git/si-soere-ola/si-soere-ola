package org.inra.ecoinfo.glacpe.refdata.variable;

import java.util.List;

import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public interface IVariableGLACPEDAO extends IVariableDAO {

    /**
     *
     * @return
     * @throws PersistenceException
     */
    public List<VariableGLACPE> getAllQualitativeValues() throws PersistenceException;
}
