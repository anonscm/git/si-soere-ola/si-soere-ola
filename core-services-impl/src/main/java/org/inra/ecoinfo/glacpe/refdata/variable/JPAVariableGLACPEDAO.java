package org.inra.ecoinfo.glacpe.refdata.variable;

import java.util.List;

import javax.persistence.Query;
import org.inra.ecoinfo.AbstractJPADAO;

import org.inra.ecoinfo.refdata.variable.JPAVariableDAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class JPAVariableGLACPEDAO extends JPAVariableDAO implements IVariableGLACPEDAO {

    /**
     *
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<VariableGLACPE> getAllQualitativeValues() throws PersistenceException {
        try {
            String queryString = "from Variable v where v.isQualitative = true";
            Query query = entityManager.createQuery(queryString);
            @SuppressWarnings("rawtypes")
            List variablesGLACPE = query.getResultList();

            return variablesGLACPE;
        } catch (Exception e) {
            AbstractJPADAO.LOGGER.error(e.getMessage());
            throw new PersistenceException(e.getMessage(), e);
        }
    }

}
