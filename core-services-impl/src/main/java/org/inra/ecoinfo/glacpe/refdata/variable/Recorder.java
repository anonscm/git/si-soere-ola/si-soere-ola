/**
 * OREILacs project - see LICENCE.txt for use created: 7 avr. 2009 16:17:33
 */
package org.inra.ecoinfo.glacpe.refdata.variable;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.IDatatypeVariableUniteGLACPEDAO;
import org.inra.ecoinfo.glacpe.refdata.groupevariable.GroupeVariable;
import org.inra.ecoinfo.glacpe.refdata.groupevariable.IGroupeVariableDAO;
import org.inra.ecoinfo.glacpe.refdata.variablenorme.IVariableNormeDAO;
import org.inra.ecoinfo.glacpe.refdata.variablenorme.VariableNorme;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.refdata.unite.IUniteDAO;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import com.Ostermiller.util.CSVParser;


/**
 * @author "Antoine Schellenberger"
 */
public class Recorder extends AbstractCSVMetadataRecorder<VariableGLACPE> {

    private static final String BUNDLE_NAME = "org.inra.ecoinfo.glacpe.refdata.messages";

    private static final String MSG_MISSING_REFERENCE_DATA_DBVARIABLENORME = "PROPERTY_MSG_MISSING_REFERENCE_DATA_DBVARIABLENORME";
    private static final String MSG_MISSING_REFERENCE_DATA_DBGROUPEVARIABLE = "PROPERTY_MSG_MISSING_REFERENCE_DATA_DBGROUPEVARIABLE";
    private static final String MSG_USE_POSIX_CHARACTER = "PROPERTY_MSG_USE_POSIX_CHARACTER";
    private static final String TRUE_FR = "vrai";
    private static final String TRUE_EN = "true";
    private static final String TRUE_FR_ABREV = "v";
    private static final String TRUE_EN_ABREV = "t";

    /**
     *
     */
    protected IUniteDAO uniteDAO;

    /**
     *
     */
    protected IVariableDAO variableDAO;

    /**
     *
     */
    protected IVariableNormeDAO variableNormeDAO;

    /**
     *
     */
    protected IGroupeVariableDAO groupeVariableDAO;

    /**
     *
     */
    protected IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO;
    private String[] groupesVariablesPossibles;
    private String[] namesVariablesNormesPossible;

    private Properties propertiesNomEN;
    private Properties propertiesAffichageEN;
    private Properties propertiesDescriptionEN;

    /**
     * @param parser
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @throws IOException
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {

        ErrorsReport errorsReport = new ErrorsReport();
        try {

            skipHeader(parser);

            // On parcourt chaque ligne du fichier
            String[] values = null;

            while ((values = parser.getLine()) != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values, Variable.NAME_ENTITY_JPA);

                String normeCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                String nom = tokenizerValues.nextToken();
                if (!Utils.isPOSIXFree(nom)) {
                    errorsReport.addErrorMessage(localizationManager.getMessage(BUNDLE_NAME, MSG_USE_POSIX_CHARACTER));
                }
                String affichage = tokenizerValues.nextToken();
                String definition = tokenizerValues.nextToken();
                String groupeString = tokenizerValues.nextToken();
                
               
                String groupeCode = null;
                if (groupeString != null && groupeString.length() > 0) {
                    groupeCode = Utils.createCodeFromString(groupeString);
                }
                String ordreAffichageGroupe = tokenizerValues.nextToken();
                Integer ordreAffichage = null;
                if (ordreAffichageGroupe != null && ordreAffichageGroupe.length() > 0) {
                    ordreAffichage = Integer.parseInt(ordreAffichageGroupe);
                }
                String isQualitativeString = tokenizerValues.nextToken();
                Boolean isQualitative = false;
                if (isQualitativeString != null && isQualitativeString.length() > 0) {
                    isQualitative = buildIsQualitativeValue(isQualitativeString);
                }
                
                // afiocca
                String codeSandre = tokenizerValues.nextToken();
                String contexte = tokenizerValues.nextToken();

                persistVariable(errorsReport, normeCode, nom, definition, affichage, groupeCode, ordreAffichage, isQualitative, codeSandre, contexte);
            }

            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }

        } catch (IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = null;
            final ErrorsReport errorsReport = new ErrorsReport();
            while ((values = parser.getLine()) != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                tokenizerValues.nextToken();
                String code = Utils.createCodeFromString(tokenizerValues.nextToken());
                List<DatatypeVariableUniteGLACPE> dvu = datatypeVariableUniteDAO.getByVariable(code);
                if (dvu == null || dvu.isEmpty()) {
                    variableDAO.remove(variableDAO.getByCode(code));
                } else {
                    throw new BusinessException(errorsReport.getErrorsMessages());
                }
            }
        } catch (IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    private Boolean buildIsQualitativeValue(String qualitativeValue) {
        if (qualitativeValue.equalsIgnoreCase(TRUE_EN) || qualitativeValue.equalsIgnoreCase(TRUE_EN_ABREV) || qualitativeValue.equalsIgnoreCase(TRUE_FR) || qualitativeValue.equalsIgnoreCase(TRUE_FR_ABREV)) {
            return new Boolean(true);
        } else {
            return new Boolean(false);
        }

    }

    private void persistVariable(ErrorsReport errorsReport, String variableNormeCode, String nom, String definition, String affichage, String groupeCode, Integer ordreAffichageGroupe, Boolean isQualitative, String codeSandre, String contexte) throws PersistenceException {

        VariableNorme dbVariableNorme = retrieveDBVariableNorme(errorsReport, variableNormeCode);
        GroupeVariable dbGroupeVariable = retrieveDBGroupeVariable(errorsReport, groupeCode);

        if (dbVariableNorme != null) {
            VariableGLACPE dbVariable = (VariableGLACPE) variableDAO.getByCode(Utils.createCodeFromString(nom));
            VariableGLACPE variable = new VariableGLACPE(nom, definition, affichage, ordreAffichageGroupe, isQualitative);
            createOrUpdateVariable(variable, dbVariableNorme, dbVariable, dbGroupeVariable, codeSandre, contexte);
        }
    }

    private GroupeVariable retrieveDBGroupeVariable(ErrorsReport errorsReport, String groupeVariableCode) throws PersistenceException {
        GroupeVariable groupeVariable = null;

        if (groupeVariableCode != null && groupeVariableCode.length() > 0) {
            groupeVariable = groupeVariableDAO.getByCode(groupeVariableCode);
            if (groupeVariable == null && groupeVariableCode.length() > 0) {
                errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, MSG_MISSING_REFERENCE_DATA_DBGROUPEVARIABLE), groupeVariableCode));
            }
        }
        return groupeVariable;
    }

    private void createOrUpdateVariable(VariableGLACPE variable, VariableNorme dbVariableNorme, VariableGLACPE dbVariable, GroupeVariable dbGroupeVariable, String codeSandre, String contexte) {
        if (dbVariable == null) {
            createVariable(variable, dbVariableNorme, dbGroupeVariable, codeSandre, contexte);
        } else {
            updateDBVariable(variable, dbVariable, dbVariableNorme, dbGroupeVariable, codeSandre, contexte);
        }
    }

    private VariableNorme retrieveDBVariableNorme(ErrorsReport errorsReport, String variableNormeCode) throws PersistenceException {
        VariableNorme variableNorme = variableNormeDAO.getByCode(variableNormeCode);
        if (variableNorme == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, MSG_MISSING_REFERENCE_DATA_DBVARIABLENORME), variableNormeCode));
        }
        return variableNorme;
    }

    /**
     * @param variable
     * @param dbVariable
     * @param dbUnite
     * @param dbVariableNorme
     * @param dbVariableCategorie
     * @param dbGroupeVariable
     */
    private void updateDBVariable(VariableGLACPE variable, VariableGLACPE dbVariable, VariableNorme dbVariableNorme, GroupeVariable dbGroupeVariable, String codeSandre, String contexte) {
        dbVariable.setCode(variable.getCode());
        dbVariable.setDefinition(variable.getDefinition());
        dbVariable.setAffichage(variable.getAffichage());
        dbVariable.setOrdreAffichageGroupe(variable.getOrdreAffichageGroupe());
        dbVariable.setIsQualitative(variable.getIsQualitative());
        
        //
        dbVariable.setCodeSandre(variable.getCodeSandre());
        dbVariable.setCodeSandre(variable.getContexte());
 
        createVariable(dbVariable, dbVariableNorme, dbGroupeVariable, codeSandre, contexte);
    }

    /**
     * @param variable
     * @param dbUnite
     * @param dbVariableNorme
     * @param dbVariableCategorie
     * @param dbGroupeVariable
     */
    private void createVariable(VariableGLACPE variable, VariableNorme dbVariableNorme, GroupeVariable dbGroupeVariable, String codeSandre,String contexte) {

        variable.setVariableNorme(dbVariableNorme);
        dbVariableNorme.getVariables().add(variable);
        //
        variable.setCodeSandre(codeSandre);
        variable.setContexte(contexte);

        if (dbGroupeVariable != null) {
            variable.setGroupe(dbGroupeVariable);
            dbGroupeVariable.getVariables().add(variable);
            //
            variable.setCodeSandre(codeSandre);
            variable.setContexte(contexte);
        }
    }

    private void updateNamesNormesCodesPossibles() throws PersistenceException {
        List<VariableNorme> variablesNormes = variableNormeDAO.getAll(VariableNorme.class);
        String[] namesVariablesNormesPossibles = new String[variablesNormes.size()];
        int index = 0;
        for (VariableNorme variablesNorme : variablesNormes) {
            namesVariablesNormesPossibles[index++] = variablesNorme.getNom();
        }
        this.namesVariablesNormesPossible = namesVariablesNormesPossibles;

    }

    private void updateNamesGroupesVariablesPossibles() throws PersistenceException {
        List<GroupeVariable> groupesVariables = groupeVariableDAO.getAll(GroupeVariable.class);
        String[] groupesVariablesPossibles = new String[groupesVariables.size() + 1];
        groupesVariablesPossibles[0] = "";
        int index = 1;
        for (GroupeVariable groupeVariable : groupesVariables) {
            groupesVariablesPossibles[index++] = groupeVariable.getNom();
        }
        this.groupesVariablesPossibles = groupesVariablesPossibles;

    }

    /**
     *
     * @param uniteDAO
     */
    public void setUniteDAO(IUniteDAO uniteDAO) {
        this.uniteDAO = uniteDAO;
    }

    /**
     *
     * @param variableDAO
     */
    public void setVariableDAO(IVariableDAO variableDAO) {
        this.variableDAO = variableDAO;
    }

    /**
     *
     * @param variableNormeDAO
     */
    public void setVariableNormeDAO(IVariableNormeDAO variableNormeDAO) {
        this.variableNormeDAO = variableNormeDAO;
    }

    /**
     *
     * @param groupeVariableDAO
     */
    public void setGroupeVariableDAO(IGroupeVariableDAO groupeVariableDAO) {
        this.groupeVariableDAO = groupeVariableDAO;
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(VariableGLACPE variable) throws PersistenceException {
        String[] qualitativesValues = new String[]{"vrai", "faux"};
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(variable == null ? EMPTY_STRING : variable.getVariableNorme().getNom(), namesVariablesNormesPossible, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(variable == null ? EMPTY_STRING : variable.getNom(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(variable == null ? EMPTY_STRING : propertiesNomEN.get(variable.getNom()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(variable == null ? EMPTY_STRING : variable.getAffichage(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(variable == null ? EMPTY_STRING : propertiesAffichageEN.get(variable.getAffichage()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(variable == null ? EMPTY_STRING : variable.getDefinition(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(variable == null ? EMPTY_STRING : propertiesDescriptionEN.get(variable.getDefinition()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(variable == null ? EMPTY_STRING : variable.getGroupe() != null ? variable.getGroupe().getNom() : "", groupesVariablesPossibles, null, true, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(variable == null ? EMPTY_STRING : variable.getOrdreAffichageGroupe() != null ? variable.getOrdreAffichageGroupe().toString() : "", ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(variable == null ? EMPTY_STRING : variable.getIsQualitative() ? "vrai" : "faux", qualitativesValues, null, true, false, true));


        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(variable == null ? EMPTY_STRING : variable.getCodeSandre(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(variable == null ? EMPTY_STRING : variable.getContexte(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        return lineModelGridMetadata;
    }

    @Override
    protected List<VariableGLACPE> getAllElements() throws PersistenceException {
        List<Variable> variables = variableDAO.getAll(VariableGLACPE.class);
        List<VariableGLACPE> variablesGLACPE = new LinkedList<VariableGLACPE>();
        for (Variable variable : variables) {
            variablesGLACPE.add((VariableGLACPE) variable);
        }
        return variablesGLACPE;
    }

    @Override
    protected ModelGridMetadata<VariableGLACPE> initModelGridMetadata() {
        try {
            propertiesNomEN = localizationManager.newProperties(Variable.NAME_ENTITY_JPA, "nom", Locale.ENGLISH);
            propertiesAffichageEN = localizationManager.newProperties(Variable.NAME_ENTITY_JPA, "affichage", Locale.ENGLISH);
            propertiesDescriptionEN = localizationManager.newProperties(Variable.NAME_ENTITY_JPA, "definition", Locale.ENGLISH);
            updateNamesGroupesVariablesPossibles();
            updateNamesNormesCodesPossibles();
        } catch (PersistenceException e) {
            e.printStackTrace();
        }
        return super.initModelGridMetadata();
    }

    /**
     *
     * @param datatypeVariableUniteDAO
     */
    public void setDatatypeVariableUniteDAO(IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO) {
        this.datatypeVariableUniteDAO = datatypeVariableUniteDAO;
    }
}
