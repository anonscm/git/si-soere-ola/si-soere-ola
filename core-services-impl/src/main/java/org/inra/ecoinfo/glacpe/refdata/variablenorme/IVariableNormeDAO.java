/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:53:17
 */
package org.inra.ecoinfo.glacpe.refdata.variablenorme;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public interface IVariableNormeDAO extends IDAO<VariableNorme> {

    /**
     *
     * @param nom
     * @return
     * @throws PersistenceException
     */
    public VariableNorme getByCode(String nom) throws PersistenceException;
}
