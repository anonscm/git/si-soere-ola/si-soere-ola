/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:50:44
 */
package org.inra.ecoinfo.glacpe.refdata.variablenorme;

import java.util.List;

import javax.persistence.Query;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public class JPAVariableNormeDAO extends AbstractJPADAO<VariableNorme> implements IVariableNormeDAO {

    /**
     *
     * @param code
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("rawtypes")
    @Override
    public VariableNorme getByCode(String code) throws PersistenceException {
        try {
            String queryString = "from VariableNorme v where v.code = :code";
            Query query = entityManager.createQuery(queryString);
            query.setParameter("code", code);
            List variablesNormes = query.getResultList();

            VariableNorme variableNorme = null;
            if (variablesNormes != null && !variablesNormes.isEmpty())
                variableNorme = (VariableNorme) variablesNormes.get(0);

            return variableNorme;
        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

}
