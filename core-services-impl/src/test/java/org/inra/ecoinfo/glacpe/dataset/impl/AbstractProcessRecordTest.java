/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.glacpe.dataset.impl;

import com.Ostermiller.util.CSVParser;
import java.io.IOException;
import org.inra.ecoinfo.dataset.AbstractRecorder;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.localization.entity.Localization;
import org.inra.ecoinfo.localization.impl.SpringLocalizationManager;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BadValueTypeException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Spy;
import org.xml.sax.SAXException;

/**
 *
 * @author mylene
 */
public class AbstractProcessRecordTest {
    @Spy
    AbstractProcessRecord instance ;
    public AbstractProcessRecordTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        Localization.addLanguage("fr");
        Localization.setDefaultLocalisation("fr");
        new RecorderGLACPE().setLocalizationManager(new SpringLocalizationManager());
        instance = new AbstractProcessRecordImpl();
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of testValueCoherence method, of class AbstractProcessRecord.
     */
    @Test
    public void testTestValueCoherence() throws Exception {
        System.out.println("testValueCoherence");
        //value is greater than valueMax
        Float value = 40f;
        Float valeurMin = 3f;
        Float valeurMax = 6f;
        long lineNumber = 4L;
        int columnNumber = 2;
        try {
           instance.testValueCoherence(value, valeurMin, valeurMax, lineNumber, columnNumber); 
            Assert.fail("la valeur est coherente");
        } catch (BadExpectedValueException e) {
            Assert.assertEquals("Ligne 4, colonne 2, la valeur 40,000000 doit être comprise entre 3,000000 et 6,000000.", e.getMessage());
        }
        
        //value is less than valueMin
         value = 1f;
         valeurMin = 3f;
         valeurMax = 6f;
         lineNumber = 4L;
         columnNumber = 2;
        try {
           instance.testValueCoherence(value, valeurMin, valeurMax, lineNumber, columnNumber); 
           throw new Exception("la valeur est coherente");
        } catch (BadExpectedValueException e) {
            Assert.assertEquals("Ligne 4, colonne 2, la valeur 1,000000 doit être comprise entre 3,000000 et 6,000000.", e.getMessage());
        }
        
    }

    public class AbstractProcessRecordImpl extends AbstractProcessRecord {

        public void processRecord(CSVParser parser, VersionFile versionFile, String fileEncoding, DatasetDescriptor datasetDescriptor) throws BusinessException {
        }

        @Override
        public DatasetDescriptor holdDatasetDescriptor() throws IOException, SAXException {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }
    
}
