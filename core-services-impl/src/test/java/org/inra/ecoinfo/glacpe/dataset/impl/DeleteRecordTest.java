/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.glacpe.dataset.impl;

import org.inra.ecoinfo.dataset.versioning.IVersionFileDAO;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.glacpe.dataset.ILocalPublicationDAO;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.*;
import org.mockito.MockitoAnnotations;

/**
 *
 * @author mylene
 */
public class DeleteRecordTest {
    DeleteRecord instance;
    @Mock
    IVersionFileDAO versionDAO;
    @Mock
    ILocalPublicationDAO publiDAO;
    @Mock(name = "versionDetached")
    VersionFile versio;
    @Mock(name = "versionAttached")
    VersionFile versioMerge;
    public DeleteRecordTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        instance = new DeleteRecord();
        instance.setPublicationDAO(publiDAO);
        instance.setVersionFileDAO(versionDAO);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of deleteRecord method, of class DeleteRecord.
     */
    @Test
    public void testDeleteRecord() throws Exception {
        System.out.println("deleteRecord");
        Mockito.when(versionDAO.merge(versio)).thenReturn(versioMerge);
        instance.deleteRecord(versio);
        verify(versionDAO, times(1)).merge(versio);
        verify(publiDAO, times(1)).removeVersion(versioMerge);
        verify(publiDAO, times(1)).removeVersion(any());
    }
    
    @Test
    public void testDeleteRecordThrowException() throws Exception {
        System.out.println("deleteRecord");
        Mockito.when(versionDAO.merge(versio)).thenThrow(new PersistenceException("Monexception"));
        try{
        instance.deleteRecord(versio);
        fail("pas d'exception lancée");
        }catch(BusinessException e){
            assertEquals("Monexception", e.getMessage());
        }
        verify(versionDAO, times(1)).merge(versio);
        verify(publiDAO, never()).removeVersion(any());
    }
    
}
