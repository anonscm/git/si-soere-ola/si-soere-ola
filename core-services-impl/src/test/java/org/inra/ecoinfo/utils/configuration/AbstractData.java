package org.inra.ecoinfo.utils.configuration;

/**
 *
 * @author ptcherniati
 */
public class AbstractData {

    private int order;
    private String code;
    private FileTest inFile;
    private String inFileCode;
    private FileTest outFile;
    private String outFileCode;

    /**
     *
     */
    public AbstractData() {
        super();
    }

    /**
     *
     * @return
     */
    public int getOrder() {
        return order;
    }

    /**
     *
     * @param order
     */
    public void setOrder(int order) {
        this.order = order;
    }

    /**
     *
     * @return
     */
    public String getCode() {
        return code;
    }

    /**
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     *
     * @return
     */
    public FileTest getInFile() {
        return inFile;
    }

    /**
     *
     * @param inFile
     */
    public void setInFile(FileTest inFile) {
        this.inFile = inFile;
    }

    /**
     *
     * @return
     */
    public String getInFileCode() {
        return inFileCode;
    }

    /**
     *
     * @param inFileCode
     */
    public void setInFileCode(String inFileCode) {
        this.inFileCode = inFileCode;
    }

    /**
     *
     * @return
     */
    public FileTest getOutFile() {
        return outFile;
    }

    /**
     *
     * @param outFile
     */
    public void setOutFile(FileTest outFile) {
        this.outFile = outFile;
    }

    /**
     *
     * @return
     */
    public String getOutFileCode() {
        return outFileCode;
    }

    /**
     *
     * @param outFileCode
     */
    public void setOutFileCode(String outFileCode) {
        this.outFileCode = outFileCode;
    }
}