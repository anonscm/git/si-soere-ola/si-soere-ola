package org.inra.ecoinfo.utils.configuration;

/**
 *
 * @author ptcherniati
 */
public class AbstractEntityRef {

    /**
     *
     */
    protected String code;

    /**
     *
     */
    public AbstractEntityRef() {
        super();
    }

    /**
     *
     * @param code
     */
    public AbstractEntityRef(String code) {
        super();
        this.code = code;
    }

    /**
     *
     * @return
     */
    public String getCode() {
        return code;
    }

    /**
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return code;
    }
}