/**
 * OREILacs project - see LICENCE.txt for use created: 12 févr. 2009 14:08:38
 */
package org.inra.ecoinfo.utils.configuration;

import org.apache.commons.digester.Digester;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public class ConfigurationFactoryTests extends AbstractConfigurationFactoryTests {

    /**
     *
     */
    public ConfigurationFactoryTests() {

    }

    /**
     *
     * @param digester
     */
    protected void parseDigester(Digester digester) {
        parseUtilisateurs(digester);
        parseFiles(digester);
        parseMetadatas(digester);
        parseDatas(digester);
        parsePrivileges(digester);
        parseParameters(digester);
        parseTest(digester);
        parseInclude(digester);
    }
}
