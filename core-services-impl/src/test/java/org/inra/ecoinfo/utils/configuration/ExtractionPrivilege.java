package org.inra.ecoinfo.utils.configuration;

import java.util.Date;

/**
 *
 * @author ptcherniati
 */
public class ExtractionPrivilege {

    private Date beginDate;
    private Date EndDate;
    private boolean extraction;

    /**
     *
     */
    public ExtractionPrivilege() {
        super();
    }

    /**
     *
     * @return
     */
    public Date getBeginDate() {
        return beginDate;
    }

    /**
     *
     * @return
     */
    public Date getEndDate() {
        return EndDate;
    }

    /**
     *
     * @return
     */
    public boolean isExtraction() {
        return extraction;
    }

    /**
     *
     * @param beginDate
     */
    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    /**
     *
     * @param endDate
     */
    public void setEndDate(Date endDate) {
        EndDate = endDate;
    }

    /**
     *
     * @param extraction
     */
    public void setExtraction(boolean extraction) {
        this.extraction = extraction;
    }
}