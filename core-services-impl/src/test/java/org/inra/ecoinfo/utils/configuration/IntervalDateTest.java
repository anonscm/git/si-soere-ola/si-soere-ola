package org.inra.ecoinfo.utils.configuration;

/**
 *
 * @author ptcherniati
 */
public class IntervalDateTest {

    private String beginDate;
    private String EndDate;

    /**
     *
     * @return
     */
    public String getBeginDate() {
        return beginDate;
    }

    /**
     *
     * @return
     */
    public String getEndDate() {
        return EndDate;
    }

    /**
     *
     * @param beginDate
     */
    public void setBeginDate(String beginDate) {
        this.beginDate = beginDate;
    }

    /**
     *
     * @param endDate
     */
    public void setEndDate(String endDate) {
        EndDate = endDate;
    }
}