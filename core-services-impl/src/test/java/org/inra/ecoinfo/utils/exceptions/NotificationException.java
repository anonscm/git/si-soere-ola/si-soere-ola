package org.inra.ecoinfo.utils.exceptions;

import org.inra.ecoinfo.notifications.entity.Notification;

/**
 *
 * @author ptcherniati
 */
public class NotificationException extends Exception {

    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;
    private Notification notification;

    /**
     *
     */
    public NotificationException() {
        super();
    }

    /**
     *
     * @param message
     */
    public NotificationException(String message) {
        super(message);
    }

    /**
     *
     * @param cause
     */
    public NotificationException(Throwable cause) {
        super(cause);
    }

    /**
     *
     * @param message
     * @param cause
     */
    public NotificationException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     *
     * @param notification
     */
    public NotificationException(Notification notification) {
        super(notification.getMessage());
        this.notification = notification;
    }

    /**
     *
     * @return
     */
    public Notification getNotification() {
        return notification;
    }
}