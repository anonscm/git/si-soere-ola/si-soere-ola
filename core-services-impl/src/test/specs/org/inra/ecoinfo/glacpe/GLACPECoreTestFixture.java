package org.inra.ecoinfo.glacpe;

import org.concordion.api.ExpectedToPass;
import org.inra.ecoinfo.AbstractTestFixture;
import org.inra.ecoinfo.ConcordionSpringJunit4ClassRunner;
import org.inra.ecoinfo.TransactionalTestFixtureExecutionListener;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ptcherniati
 */
@RunWith(ConcordionSpringJunit4ClassRunner.class)
@ContextConfiguration(locations = {"/META-INF/spring/applicationContextTest.xml"})
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@Transactional(rollbackFor = Exception.class)
@TestExecutionListeners(listeners = {GLACPETransactionalTestFixtureExecutionListener.class})
@ExpectedToPass
public class GLACPECoreTestFixture extends AbstractTestFixture {

    /**
     *
     */
    public GLACPECoreTestFixture() {
        super();
        TransactionalTestFixtureExecutionListener.startSession();
    }
}
