package org.inra.ecoinfo.glacpe;

import java.io.IOException;
import java.sql.SQLException;

import org.dbunit.DatabaseUnitException;
import org.dbunit.dataset.DataSetException;
import org.inra.ecoinfo.TransactionalTestFixtureExecutionListener;
import org.inra.ecoinfo.dataset.IDatasetManager;
import org.inra.ecoinfo.dataset.versioning.IDatasetDAO;
import org.inra.ecoinfo.dataset.versioning.IVersionFileDAO;
import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.filecomp.IFileCompManager;
import org.inra.ecoinfo.filecomp.associate.IAssociateDAO;
import org.inra.ecoinfo.filecomp.associate.IAssociateFileCompManager;
import org.inra.ecoinfo.filecomp.config.impl.IFileCompConfiguration;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.IDatatypeVariableUniteGLACPEDAO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.IPlateformeDAO;
import org.inra.ecoinfo.glacpe.refdata.projet.IProjetDAO;
import org.inra.ecoinfo.glacpe.refdata.projetsite.IProjetSiteDAO;
import org.inra.ecoinfo.glacpe.refdata.projetsitethemedatatype.IProjetSiteThemeDatatypeDAO;
import org.inra.ecoinfo.glacpe.refdata.site.ISiteGLACPEDAO;
import org.inra.ecoinfo.glacpe.refdata.taxon.ITaxonDAO;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.notifications.INotificationsManager;
import org.inra.ecoinfo.refdata.IMetadataManager;
import org.inra.ecoinfo.refdata.datatype.IDatatypeDAO;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.springframework.test.context.TestContext;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ptcherniati
 */
public class GLACPETransactionalTestFixtureExecutionListener extends TransactionalTestFixtureExecutionListener {
    private static String testName="";

    /**
     *
     */
    public static IMetadataManager metadataManager;

    /**
     *
     */
    public static IDatasetManager datasetManager;

    /**
     *
     */
    public static IProjetSiteThemeDatatypeDAO projetSiteThemeDatatypeDAO;

    /**
     *
     */
    public static IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteGLACPEDAO;

    /**
     *
     */
    public static IDatasetDAO datasetDAO;

    /**
     *
     */
    public static IVersionFileDAO versionFileDAO;

    /**
     *
     */
    public static IDatatypeDAO datatypeDAO;

    /**
     *
     */
    public static IProjetDAO projetDAO;

    /**
     *
     */
    public static IProjetSiteDAO projetSiteDAO;

    /**
     *
     */
    public static ISiteGLACPEDAO siteGLACPEDAO;

    /**
     *
     */
    public static IVariableDAO variableDAO;

    /**
     *
     */
    public static IPlateformeDAO plateformeDAO;

    /**
     *
     */
    public static ITaxonDAO taxonDAO;

    /**
     *
     */
    public static ILocalizationManager localizationManager;

    /**
     *
     */
    public static IExtractionManager extractionManager;

    /**
     *
     */
    public static INotificationsManager notificationsManager;

    /**
     *
     */
    public static IFileCompManager fileCompManager;

    /**
     *
     */
    public static IAssociateFileCompManager associateFileCompManager;

    /**
     *
     */
    public static IFileCompConfiguration fileCompConfiguration;

    /**
     *
     */
    public static IAssociateDAO associateDAO;

    /**
     * After super class.
     * 
     * @param testContext
     * @link(TestContext) the test context
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws DataSetException
     *             the data set exception
     * @throws DatabaseUnitException
     *             the database unit exception
     * @throws SQLException
     *             the sQL exception
     * @throws Exception
     *             the exception
     * @link(TestContext) the test context
     */
    @Transactional(rollbackFor = Exception.class)
    void afterSuperClass(final TestContext testContext) throws IOException, DataSetException, DatabaseUnitException, SQLException, Exception {
        super.afterTestClass(testContext);
    }

    /**
     * Before test class.
     * 
     * @param testContext
     * @link(TestContext) the test context
     * @throws Exception
     *             the exception
     * @see org.springframework.test.context.support.AbstractTestExecutionListener #beforeTestClass(org.springframework.test.context.TestContext)
     */
    @Override
    public void beforeTestClass(final TestContext testContext) throws Exception {
        super.beforeTestClass(testContext);
        GLACPETransactionalTestFixtureExecutionListener.metadataManager = (IMetadataManager) TransactionalTestFixtureExecutionListener.applicationContext.getBean("metadataManager");
        GLACPETransactionalTestFixtureExecutionListener.datasetManager = (IDatasetManager) TransactionalTestFixtureExecutionListener.applicationContext.getBean("datasetManager");
        GLACPETransactionalTestFixtureExecutionListener.projetSiteThemeDatatypeDAO = (IProjetSiteThemeDatatypeDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("projetSiteThemeDatatypeDAO");
        GLACPETransactionalTestFixtureExecutionListener.datatypeVariableUniteGLACPEDAO = (IDatatypeVariableUniteGLACPEDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("datatypeVariableUniteDAO");
        GLACPETransactionalTestFixtureExecutionListener.datasetDAO = (IDatasetDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("datasetDAO");
        GLACPETransactionalTestFixtureExecutionListener.versionFileDAO = (IVersionFileDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("versionFileDAO");
        GLACPETransactionalTestFixtureExecutionListener.datatypeDAO = (IDatatypeDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("datatypeDAO");
        GLACPETransactionalTestFixtureExecutionListener.projetDAO = (IProjetDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("projetDAO");
        GLACPETransactionalTestFixtureExecutionListener.projetSiteDAO = (IProjetSiteDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("projetSiteDAO");
        GLACPETransactionalTestFixtureExecutionListener.siteGLACPEDAO = (ISiteGLACPEDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("siteGLACPEDAO");
        GLACPETransactionalTestFixtureExecutionListener.plateformeDAO = (IPlateformeDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("plateformeDAO");
        GLACPETransactionalTestFixtureExecutionListener.variableDAO = (IVariableDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("variableDAO");
        GLACPETransactionalTestFixtureExecutionListener.taxonDAO = (ITaxonDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("taxonDAO");
        GLACPETransactionalTestFixtureExecutionListener.localizationManager = (ILocalizationManager) TransactionalTestFixtureExecutionListener.applicationContext.getBean("localizationManager");
        GLACPETransactionalTestFixtureExecutionListener.extractionManager = (IExtractionManager) TransactionalTestFixtureExecutionListener.applicationContext.getBean("extractionManager");
        GLACPETransactionalTestFixtureExecutionListener.notificationsManager = (INotificationsManager) TransactionalTestFixtureExecutionListener.applicationContext.getBean("notificationsManager");
        GLACPETransactionalTestFixtureExecutionListener.fileCompManager = (IFileCompManager) TransactionalTestFixtureExecutionListener.applicationContext.getBean("fileCompManager");
        GLACPETransactionalTestFixtureExecutionListener.associateFileCompManager = (IAssociateFileCompManager) TransactionalTestFixtureExecutionListener.applicationContext.getBean("associateFileCompManager");
        GLACPETransactionalTestFixtureExecutionListener.fileCompConfiguration = (IFileCompConfiguration) TransactionalTestFixtureExecutionListener.applicationContext.getBean("fileCompConfiguration");
        GLACPETransactionalTestFixtureExecutionListener.associateDAO = (IAssociateDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("associateDAO");
        if (testName.isEmpty()) {
            testName = "../target/concordion/" + testContext.getTestClass().getName().replaceAll(".*\\.(.*?)Fixture", "$1");
        }
        System.setProperty("concordion.output.dir",testName);
    }

    /**
     * Clean tables.
     * 
     * @throws SQLException
     *             the sQL exception
     */
    @Override
    protected void cleanTables() throws SQLException {
        getDatasourceConnexion().getConnection().prepareStatement("delete from utilisateur;").execute();
    }
}
