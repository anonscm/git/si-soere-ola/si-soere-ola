package org.inra.ecoinfo.glacpe.phytoplancton.extraction;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.inra.ecoinfo.ConcordionSpringJunit4ClassRunner;
import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.glacpe.GLACPETransactionalTestFixtureExecutionListener;
import org.inra.ecoinfo.glacpe.dataset.chimie.impl.ChimieParameters;
import org.inra.ecoinfo.glacpe.extraction.vo.AbstractDatesFormParam;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesYearsContinuousFormParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesYearsDiscretsFormParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesYearsRangeFormParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO;
import org.inra.ecoinfo.glacpe.extraction.vo.ProjetSiteVO;
import org.inra.ecoinfo.glacpe.extraction.vo.VariableVO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projetsite.ProjetSite;
import org.inra.ecoinfo.glacpe.refdata.taxon.Taxon;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ptcherniati
 */
@RunWith(ConcordionSpringJunit4ClassRunner.class)
@ContextConfiguration(locations = {"/META-INF/spring/applicationContextTest.xml"})
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@Transactional(rollbackFor = Exception.class)
@TestExecutionListeners(listeners = {GLACPETransactionalTestFixtureExecutionListener.class})
public class ExtractionFixture extends org.inra.ecoinfo.extraction.AbstractExtractionFixture {

    /**
     *
     */
    public ExtractionFixture() {
        super();
        MockitoAnnotations.initMocks(this);
    }

    /**
     *
     * @param projetName
     * @param plateformesListNames
     * @param variableListNames
     * @param selectedDates
     * @param dateDedebut
     * @param datedeFin
     * @param annees
     * @param taxonListNames
     * @param sommation
     * @param propriete
     * @param aggregated
     * @param filecomps
     * @param commentaire
     * @param affichage
     * @return
     * @throws BusinessException
     */
    public String extract(String projetName, String plateformesListNames, String variableListNames, String selectedDates, String dateDedebut, String datedeFin, String annees, String taxonListNames, String sommation, String propriete, String aggregated, String filecomps, String commentaire, String affichage) throws BusinessException {
        final Map<String, Object> metadatasMap = new HashMap<String, Object>();
        final List<ProjetSite> projetSites = new LinkedList<ProjetSite>();
        final List<PlateformeVO> plateformesVO = new LinkedList<PlateformeVO>();
        final String[] listSitesPlateformesNames=plateformesListNames.split("[*][*]");
        for(String listSitePlateforme : listSitesPlateformesNames)
        {
        	final String siteName = listSitePlateforme.split("*;*")[0];
        	final String[] plateformesNames = listSitePlateforme.split("*;*")[1].split("*,*");
        	try {
	        	final ProjetSite projetSite = GLACPETransactionalTestFixtureExecutionListener.projetSiteDAO.getBySiteCodeAndProjetCode(Utils.createCodeFromString(siteName), Utils.createCodeFromString(projetName));
	            if(!projetSites.contains(projetSite))
	            {
	            	projetSites.add(projetSite);
	            }
	        	for(String plateformeName : plateformesNames)
	        	{
	        		final Plateforme plateforme = GLACPETransactionalTestFixtureExecutionListener.plateformeDAO.getByNKey(Utils.createCodeFromString(plateformeName), Utils.createCodeFromString(siteName));
	        		final PlateformeVO plateformeVO = new PlateformeVO(plateforme);
	        		plateformeVO.setProjet(new ProjetSiteVO(projetSite));
	        		plateformesVO.add(plateformeVO);
	        	}
        	 } catch (final org.inra.ecoinfo.utils.exceptions.PersistenceException e) {
                 throw new BusinessException("site non valide" + plateformesNames + " et le projet " + projetName, e);
             }
        }       
        final String[] variableNames = variableListNames.split(" *, *");
        final List<VariableVO> variables = new LinkedList<VariableVO>();
        for (final String variableName : variableNames) {
            try {
                final VariableGLACPE variable = (VariableGLACPE) GLACPETransactionalTestFixtureExecutionListener.variableDAO.getByCode(Utils.createCodeFromString(variableName));
                variables.add(new VariableVO(variable));
            } catch (final org.inra.ecoinfo.utils.exceptions.PersistenceException e) {
                throw new BusinessException("variable non valide" + variableName, e);
            }
        }
        final DatesRequestParamVO datesRequestFormParamVO = new DatesRequestParamVO();
        if(selectedDates.equals(DatesYearsContinuousFormParamVO.LABEL))
        {
        	String[] debuts = dateDedebut.split(",");
        	String[] fins = datedeFin.split(",");
        	datesRequestFormParamVO.setSelectedFormSelection(DatesYearsContinuousFormParamVO.LABEL);
        	final DatesYearsContinuousFormParamVO datesYearsContinuousFormParamVO = new DatesYearsContinuousFormParamVO(GLACPETransactionalTestFixtureExecutionListener.localizationManager);
        	for(int i=0; i<debuts.length; i++)
        	{
        		Map<String, String> periods = new HashMap<String, String>();
        		periods.put(AbstractDatesFormParam.START_INDEX, debuts[i]);
        		periods.put(AbstractDatesFormParam.END_INDEX, fins[i]);
        		if(datesYearsContinuousFormParamVO.getPeriods() == null)
        		{
        			List<Map<String, String>> periodsList = new LinkedList<Map<String, String>>();
        			datesYearsContinuousFormParamVO.setPeriods(periodsList);
        		}
        		datesYearsContinuousFormParamVO.getPeriods().add(periods);
        	}
        	datesRequestFormParamVO.setDatesYearsContinuousFormParam(datesYearsContinuousFormParamVO);
        }
        else if(selectedDates.equals(DatesYearsDiscretsFormParamVO.LABEL))
        {
        	String[] debuts = dateDedebut.split(",");
        	String[] fins = datedeFin.split(",");
        	datesRequestFormParamVO.setSelectedFormSelection(DatesYearsDiscretsFormParamVO.LABEL);
        	final DatesYearsDiscretsFormParamVO datesYearsDiscretsFormParamVO = new DatesYearsDiscretsFormParamVO(GLACPETransactionalTestFixtureExecutionListener.localizationManager);
        	for(int i=0; i<debuts.length; i++)
        	{
        		Map<String, String> periods = new HashMap<String, String>();
        		periods.put(AbstractDatesFormParam.START_INDEX, debuts[i]);
        		periods.put(AbstractDatesFormParam.END_INDEX, fins[i]);
        		if(datesYearsDiscretsFormParamVO.getPeriods() == null)
        		{
        			List<Map<String, String>> periodsList = new LinkedList<Map<String, String>>();
        			datesYearsDiscretsFormParamVO.setPeriods(periodsList);
        		}
        		datesYearsDiscretsFormParamVO.getPeriods().add(periods);
        	}
        	datesYearsDiscretsFormParamVO.setYears(annees);
        	datesRequestFormParamVO.setDatesYearsDiscretsFormParam(datesYearsDiscretsFormParamVO);
        }
        else if(selectedDates.equals(DatesYearsRangeFormParamVO.LABEL))
        {
        	String[] debuts = dateDedebut.split(",");
        	String[] fins = datedeFin.split(",");
        	datesRequestFormParamVO.setSelectedFormSelection(DatesYearsRangeFormParamVO.LABEL);
        	final DatesYearsRangeFormParamVO datesYearsRangeFormParamVO = new DatesYearsRangeFormParamVO(GLACPETransactionalTestFixtureExecutionListener.localizationManager);
        	for(int i=0; i<debuts.length; i++)
        	{
        		Map<String, String> periods = new HashMap<String, String>();
        		periods.put(AbstractDatesFormParam.START_INDEX, debuts[i]);
        		periods.put(AbstractDatesFormParam.END_INDEX, fins[i]);
        		if(datesYearsRangeFormParamVO.getPeriods() == null)
        		{
        			List<Map<String, String>> periodsList = new LinkedList<Map<String, String>>();
        			datesYearsRangeFormParamVO.setPeriods(periodsList);
        		}
        		datesYearsRangeFormParamVO.getPeriods().add(periods);
        	}
        	String[] yearsRange = annees.split("-");
        	List<Map<String, String>> yearsList = new LinkedList<Map<String, String>>();
        	for(String range : yearsRange)
        	{
        		Map<String, String> yearsMap = new HashMap<String, String>();
        		String[] year = range.split(",");
        		yearsMap.put(AbstractDatesFormParam.START_INDEX, year[0]);
        		yearsMap.put(AbstractDatesFormParam.END_INDEX, year[1]);
        		yearsList.add(yearsMap);
        	}
        	datesYearsRangeFormParamVO.setYears(yearsList);
        	datesRequestFormParamVO.setDatesYearsRangeFormParam(datesYearsRangeFormParamVO);
        }
        
        final String[] taxonNames = taxonListNames.split(" *, *");
        final List<Taxon> taxons = new LinkedList<Taxon>();
        for (final String taxonName : taxonNames) {
            try {
                final Taxon taxon = (Taxon) GLACPETransactionalTestFixtureExecutionListener.taxonDAO.getByCode(Utils.createCodeFromString(taxonName));
                taxons.add(taxon);
            } catch (final org.inra.ecoinfo.utils.exceptions.PersistenceException e) {
                throw new BusinessException("taxon non valide" + taxonName, e);
            }
        }
        
        Boolean sommationB = sommation.equals("true");
        Boolean proprieteB = propriete.equals("true");
        Boolean aggregatedB = aggregated.equals("true");
        
        metadatasMap.put(PlateformeVO.class.getSimpleName(), plateformesVO);
        metadatasMap.put(VariableVO.class.getSimpleName(), variables);
        metadatasMap.put(DatesRequestParamVO.class.getSimpleName(), datesRequestFormParamVO);
        metadatasMap.put(Taxon.class.getSimpleName(), taxons);
        metadatasMap.put("sommation", sommationB);
        metadatasMap.put("propriete", proprieteB);
        metadatasMap.put("aggregated", aggregatedB);
        metadatasMap.put(IExtractionManager.KEYMAP_COMMENTS, commentaire);

        final ChimieParameters parameters = new ChimieParameters(metadatasMap);
        try {
        	GLACPETransactionalTestFixtureExecutionListener.extractionManager.extract(parameters, Integer.parseInt(affichage));
        } catch (final Exception e) {
            return "false : " + e.getMessage();
        }
        return "true";
    }
}
