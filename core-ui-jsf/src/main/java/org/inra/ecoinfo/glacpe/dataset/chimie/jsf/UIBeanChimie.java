package org.inra.ecoinfo.glacpe.dataset.chimie.jsf;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.inra.ecoinfo.dataset.security.ISecurityDatasetManager;
import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.filecomp.IFileCompManager;
import org.inra.ecoinfo.filecomp.ItemFile;
import org.inra.ecoinfo.filecomp.entity.FileComp;
import org.inra.ecoinfo.glacpe.dataset.chimie.IChimieDatatypeManager;
import org.inra.ecoinfo.glacpe.dataset.chimie.impl.ChimieParameters;
import org.inra.ecoinfo.glacpe.extraction.jsf.AbstractUIBeanForSteps;
import org.inra.ecoinfo.glacpe.extraction.vo.DatasRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.DepthRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.GroupeVariableVO;
import org.inra.ecoinfo.glacpe.extraction.vo.ProjetSiteVO;
import org.inra.ecoinfo.glacpe.extraction.vo.VariableVO;
import org.inra.ecoinfo.glacpe.refdata.groupevariable.GroupeVariable;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.projetsite.ProjetSite;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.notifications.INotificationsManager;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.security.ISecurityContext;
import org.inra.ecoinfo.security.entity.Role;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.richfaces.component.UITree;
import org.richfaces.component.UITreeNode;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.util.Objects;

/**
 *
 * @author ptcherniati
 */
@ManagedBean(name = "uiChimie")
@ViewScoped
public class UIBeanChimie extends AbstractUIBeanForSteps implements Serializable {

    private static final String RULE_NAVIGATION_JSF = "chimie";

    private static final long serialVersionUID = 1L;

    private static final String DATATYPE = "physico_chimie";
    private static final String THEME = "physico_chimie";
    private static final String PATH_PATTERN = "%s/%s/%s/%s/%s";

    private Map<Long, ProjectVO> projectsAvailables = new HashMap<Long, UIBeanChimie.ProjectVO>();

    /**
     *
     */
    @ManagedProperty(value = "#{chimieDatatypeManager}")
    protected IChimieDatatypeManager chimieDatatypeManager;

    /**
     *
     */
    @ManagedProperty(value = "#{notificationsManager}")
    protected INotificationsManager notificationsManager;

    /**
     *
     */
    @ManagedProperty(value = "#{extractionManager}")
    protected IExtractionManager extractionManager;

    /**
     *
     */
    @ManagedProperty(value = "#{transactionManager}")
    protected JpaTransactionManager transactionManager;

    /**
     *
     */
    @ManagedProperty(value = "#{securityContext}")
    protected ISecurityContext securityContext;

    /**
     *
     */
    @ManagedProperty(value = "#{localizationManager}")
    protected ILocalizationManager localizationManager;

    /**
     *
     */
    @ManagedProperty(value = "#{fileCompManager}")
    protected IFileCompManager fileCompManager;

    private Properties propertiesProjectsNames;
    private Properties propertiesPlatformNames;
    private Properties propertiesSiteNames;
    private Properties propertiesVariableNames;
    private Properties propertiesGroupeNames;

    private UITree treeListVariables;

    private UITreeNode treeNodeGroupeVariable;

    private Long idPlateformeSelected;

    private Long idProjectSelected;

    private Long idVariableSelected;

    private TreeNodeGroupeVariable treeNodeGroupeVariableSelected;

    private List<TreeNodeGroupeVariable> rootNodes = new ArrayList<TreeNodeGroupeVariable>();

    private VOVariableJSF variableSelected = new VOVariableJSF(null);

    List<VOVariableJSF> variablesSelected = new LinkedList<VOVariableJSF>();
    private ParametersRequest parametersRequest = new ParametersRequest();

    /** The affichage @link(String). */
    private String affichage = null;

    /**
     *
     */
    public UIBeanChimie() {}

    /**
     *
     * @return
     */
    public String navigate() {
        return RULE_NAVIGATION_JSF;
    }

    private void expandAndSelectGroupeVariable(TreeNodeGroupeVariable treeNodeGroupeVariable) {
        treeNodeGroupeVariable.setExpanded(true);
        if (treeNodeGroupeVariable.getGroupesVariables() == null || treeNodeGroupeVariable.getGroupesVariables().isEmpty()) {
            for (VOVariableJSF variable : treeNodeGroupeVariable.getVariables()) {
                enableOrDisableLeaf(false, variable);
            }

        } else {
            for (TreeNodeGroupeVariable childTreeNodeGroupeVariable : treeNodeGroupeVariable.getGroupesVariables()) {
                expandAndSelectGroupeVariable(childTreeNodeGroupeVariable);
            }
        }
    }

    /**
     *
     * @return
     */
    public String deployNodeGroupeVariable() {

        expandAndSelectGroupeVariable(treeNodeGroupeVariableSelected);

        return null;
    }

    /**
     *
     * @param variable
     */
    public void selectVariable(VOVariableJSF variable) {
        Map<Long, VOVariableJSF> variablesSelecteds = getParametersRequest().getVariablesSelected();
        enableOrDisableLeaf(variablesSelecteds.containsKey(variable.getVariable().getId()), variable);
    }

    private void enableOrDisableLeaf(Boolean conditionDisabling, VOVariableJSF variable) {
        Map<Long, VOVariableJSF> variablesSelecteds = getParametersRequest().getVariablesSelected();

        if (conditionDisabling) {
            variablesSelecteds.remove(variable.getVariable().getId());
            variable.setSelected(false);
        } else {
            variablesSelecteds.put(variable.getVariable().getId(), variable);
            variable.setSelected(true);
        }
    }

    /**
     *
     * @return
     */
    public String selectVariable() {

        selectVariable(variableSelected);
        return null;
    }

    /**
     *
     * @return
     */
    public String deSelectVariable() {

        for (TreeNodeGroupeVariable groupeVariable : rootNodes) {
            deSelectVariable(groupeVariable);
        }
        return null;
    }

    private String deSelectVariable(TreeNodeGroupeVariable groupeVariable) {
        if (groupeVariable.getGroupeVariable().getChildren().isEmpty()) {
            for (VOVariableJSF variable : groupeVariable.getVariables()) {
                if (variable.getVariable().getId().equals(idVariableSelected)) {
                    variableSelected = variable;
                    selectVariable(variableSelected);
                    return null;
                }
            }
        } else {
            for (TreeNodeGroupeVariable groupeVariables : groupeVariable.getGroupesVariables()) {

                deSelectVariable(groupeVariables);
            }
        }
        return null;
    }

    /**
     *
     * @return
     */
    public String addAllVariables() {
        Map<Long, VOVariableJSF> variablesSelected = getParametersRequest().getVariablesSelected();
        variablesSelected.clear();

        for (TreeNodeGroupeVariable groupeVariable : rootNodes) {
            addAllGroupVariables(groupeVariable);
        }
        return null;
    }

    private String addAllGroupVariables(TreeNodeGroupeVariable groupeVariable) {
        groupeVariable.setExpanded(true);
        if (groupeVariable.getGroupeVariable().getChildren().isEmpty()) {
            for (VOVariableJSF variable : groupeVariable.getVariables()) {
                getParametersRequest().getVariablesSelected().put(variable.getVariable().getId(), variable);
                variable.setSelected(true);

            }
        } else {
            for (TreeNodeGroupeVariable groupeVariables : groupeVariable.getGroupesVariables()) {

                addAllGroupVariables(groupeVariables);
            }
        }
        return null;
    }

    /**
     *
     * @return
     * @throws BusinessException
     * @throws ParseException
     */
    public String removeAllVariables() throws BusinessException, ParseException {
        createOrUpdateGroupVariablesAvailables();
        return null;
    }

    /**
     *
     * @return
     * @throws BusinessException
     */
    public String selectPlateforme() throws BusinessException {
        ProjectVO projectSelected = projectsAvailables.get(idProjectSelected);
        PlateformeVO plateformeSelected = projectSelected.getPlateformes().get(idPlateformeSelected);
        if (plateformeSelected.getSelected()) {
            getParametersRequest().getPlateformesSelected().remove(idPlateformeSelected);
            plateformeSelected.setSelected(false);
        } else {
            getParametersRequest().getPlateformesSelected().put(idPlateformeSelected, plateformeSelected);
            plateformeSelected.setSelected(true);
        }

        return null;
    }

    /**
     *
     * @return
     * @throws BusinessException
     */
    public String getProjetSelected() throws BusinessException {
        String localizedProjetSelected = null;
        if (idProjectSelected != null) {
            String nom = projectsAvailables.get(idProjectSelected).getProjet().getNom();
            localizedProjetSelected = getPropertiesProjectsNames().getProperty(nom);
            if (Strings.isNullOrEmpty(localizedProjetSelected))
                localizedProjetSelected = nom;
            return localizedProjetSelected;
        }
        return localizedProjetSelected;
    }

    /**
     *
     * @return
     * @throws BusinessException
     */
    public String changeProject() throws BusinessException {
        getParametersRequest().getPlateformesSelected().clear();
        for (ProjectVO projectSelected : projectsAvailables.values()) {
            for (PlateformeVO plateformeSelected : projectSelected.getPlateformes().values()) {
                plateformeSelected.setSelected(false);
            }
        }
        return null;
    }

    /**
     *
     * @return
     * @throws BusinessException
     * @throws ParseException
     */
    public List<TreeNodeGroupeVariable> getRootNodes() throws BusinessException, ParseException {
        if (rootNodes == null) {
            createOrUpdateGroupVariablesAvailables();
        }
        return rootNodes;
    }

    private void createOrUpdateGroupVariablesAvailables() throws BusinessException, ParseException {

        getParametersRequest().getVariablesSelected().clear();
        rootNodes.clear();

        if (!getParametersRequest().getPlateformesSelected().isEmpty() && parametersRequest.getDateStepIsValid()) {

            List<GroupeVariableVO> groupeVariables = chimieDatatypeManager.retrieveGroupesVariables(new LinkedList<Long>(getParametersRequest().getPlateformesSelected().keySet()), getParametersRequest().getFirstStartDate(), getParametersRequest()
                    .getLastEndDate());
            for (GroupeVariableVO groupeVariable : groupeVariables) {
                TreeNodeGroupeVariable treeNodeGroupeVariable = new TreeNodeGroupeVariable(groupeVariable);
                rootNodes.add(treeNodeGroupeVariable);
            }
        }
    }

    /**
     *
     * @param variable
     * @return
     */
    public Boolean getIsSelected(VariableVO variable) {
        if (getParametersRequest().getVariablesSelected().containsValue(variable))
            return true;
        else
            return false;
    }

    /**
     *
     * @return
     * @throws BusinessException
     */
    public Map<Long, ProjectVO> getProjectsAvailables() throws BusinessException {

        TransactionStatus status = transactionManager.getTransaction(new DefaultTransactionDefinition(TransactionDefinition.PROPAGATION_REQUIRES_NEW));

        if (projectsAvailables.isEmpty()) {
            List<Projet> projets = chimieDatatypeManager.retrieveChimieAvailablesProjets();

            for (Projet projet : projets) {
                List<Plateforme> plateformes = Lists.newArrayList();
                for (ProjetSite projetSite : projet.getProjetsSites()) {
                    for (Plateforme plateforme : chimieDatatypeManager.retrieveChimieAvailablesPlateformesByProjetSiteId(projetSite.getId())) {
                        if (!plateformes.contains(plateforme)
                                && (securityContext.isRoot() || securityContext.matchPrivilege(String.format("%s/%s/*/physico_chimie/*", projetSite.getProjet().getCode(), projetSite.getSite().getCode()), Role.ROLE_EXTRACTION,
                                        ISecurityDatasetManager.PRIVILEGE_TYPE_DATASET)))
                        {
                            plateformes.add(plateforme);
                        }
                    }
                }
                if (!plateformes.isEmpty()) {
                    ProjectVO projectVO = new ProjectVO(projet, plateformes);
                    projectsAvailables.put(projet.getId(), projectVO);
                }
            }

        }

        transactionManager.commit(status);

        return projectsAvailables;

    }

    private List<String> getPathes(List<ProjetSite> projetSites, List<VariableVO> variables) {
        String sitePath, projet, theme, path;
        theme = THEME;
        List<String> pathes = new LinkedList<>();
        for (ProjetSite projetSite : projetSites) {
            sitePath = projetSite.getSite().getPath();
            projet = projetSite.getProjet().getCode();
            for (VariableVO variable : variables) {
                path = String.format(PATH_PATTERN, projet, sitePath, theme, DATATYPE, variable.getCode());
                if (!pathes.contains(path)) {
                    pathes.add(path);
                }
            }
        }
        return pathes;
    }

    /**
     *
     * @return
     * @throws BusinessException
     */
    public String extract() throws BusinessException {
        Map<String, Object> metadatasMap = new HashMap<String, Object>();
        List<org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO> metadatasPlateformesVos = new LinkedList<org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO>();
        List<ProjetSite> projetSites = new LinkedList<ProjetSite>();
        for (PlateformeVO plateforme : getParametersRequest().getPlateformesSelected().values()) {
            org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO plateformeVO = new org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO(plateforme.getPlateforme());
            ProjetSite projetSite = chimieDatatypeManager.retrieveChimieAvailablesProjetsSiteByProjetAndSite(projectsAvailables.get(idProjectSelected).getProjet().getId(), plateforme.getPlateforme().getSite().getId());
            ProjetSiteVO projetSiteVO = new ProjetSiteVO(projetSite);
            if (!projetSites.contains(projetSite)) {
                projetSites.add(projetSite);
            }

            plateformeVO.setProjet(projetSiteVO);
            metadatasPlateformesVos.add(plateformeVO);
        }

        metadatasMap.put(PlateformeVO.class.getSimpleName(), metadatasPlateformesVos);
        List<VariableVO> variablesVO = new LinkedList<VariableVO>();
        for (VOVariableJSF variable : parametersRequest.getListVariablesSelected()) {
            variablesVO.add(variable.getVariable());
        }
        metadatasMap.put(VariableVO.class.getSimpleName(), variablesVO);

        List<FileComp> listFiles = new LinkedList<FileComp>();
        try {
            if (!parametersRequest.plateformesSelected.isEmpty() && parametersRequest.getFormIsValid()) {
                IntervalDate intervalDate;

                intervalDate = new IntervalDate(parametersRequest.getFirstStartDate(), parametersRequest.getLastEndDate(), DateUtil.getSimpleDateFormatDateLocale());

                List<IntervalDate> intervalsDate = new LinkedList<>();
                intervalsDate.add(intervalDate);
                final List<ItemFile> files = fileCompManager.getFileCompFromPathesAndIntervalsDate(getPathes(projetSites, variablesVO), intervalsDate);
                for (final ItemFile file : files) {
                    if (file.getMandatory()) {
                        listFiles.add(file.getFileComp());
                    }
                }
            }
        } catch (BadExpectedValueException e) {
            throw new BusinessException("bad file", e);
        } catch (ParseException e) {
            throw new BusinessException("bad file name", e);
        }
        metadatasMap.put(FileComp.class.getSimpleName(), listFiles);
        metadatasMap.put(DatasRequestParamVO.class.getSimpleName(), getParametersRequest().getDatasRequestParam().clone());
        metadatasMap.put(DepthRequestParamVO.class.getSimpleName(), getParametersRequest().getDepthRequestParam().clone());

        getParametersRequest().getDatesRequestParam().getDatesYearsDiscretsFormParam().setPeriods(getParametersRequest().getDatesRequestParam().getNonPeriodsYearsContinuous());
        getParametersRequest().getDatesRequestParam().getDatesYearsRangeFormParam().setPeriods(getParametersRequest().getDatesRequestParam().getNonPeriodsYearsContinuous());

        metadatasMap.put(DatesRequestParamVO.class.getSimpleName(), getParametersRequest().getDatesRequestParam().clone());
        metadatasMap.put(IExtractionManager.KEYMAP_COMMENTS, new String(getParametersRequest().getCommentExtraction()));
        IParameter parameters = new ChimieParameters(metadatasMap);
        Integer aff = Integer.parseInt(affichage);
        extractionManager.extract(parameters, aff);
        return null;
    }

    /**
     *
     */
    public class ProjectVO {

        private Projet projet;
        private String localizedProjetName;
        private Map<Long, PlateformeVO> plateformes = new HashMap<Long, PlateformeVO>();

        /**
         *
         * @param projet
         * @param plateformes
         */
        public ProjectVO(Projet projet, List<Plateforme> plateformes) {
            super();
            this.projet = projet;
            if (projet != null) {
                this.localizedProjetName = getPropertiesProjectsNames().getProperty(projet.getNom());
                if (Strings.isNullOrEmpty(this.localizedProjetName))
                    this.localizedProjetName = projet.getNom();
            }
            for (Plateforme plateforme : plateformes) {
                this.plateformes.put(plateforme.getId(), new PlateformeVO(plateforme));
            }

        }

        /**
         *
         * @return
         */
        public Projet getProjet() {
            return projet;
        }

        /**
         *
         * @return
         */
        public Map<Long, PlateformeVO> getPlateformes() {
            return plateformes;
        }

        /**
         *
         * @return
         */
        public String getLocalizedProjetName() {
            return localizedProjetName;
        }

        /**
         *
         * @param localizedProjetName
         */
        public void setLocalizedProjetName(String localizedProjetName) {
            this.localizedProjetName = localizedProjetName;
        }
    }

    /**
     *
     */
    public class PlateformeVO {

        private Plateforme plateforme;
        private String localizedPlateformeName;
        private String localizedSiteName;
        private Boolean selected = false;
        private Boolean type = true;

        /**
         *
         * @param plateforme
         */
        public PlateformeVO(Plateforme plateforme) {
            super();
            this.plateforme = plateforme;
            if (plateforme != null) {
                this.localizedPlateformeName = getPropertiesPlatformNames().getProperty(plateforme.getNom());
                if (Strings.isNullOrEmpty(this.localizedPlateformeName))
                    this.localizedPlateformeName = plateforme.getNom();
                this.localizedSiteName = getPropertiesSiteNames().getProperty(plateforme.getSite().getNom());
                if (Strings.isNullOrEmpty(this.localizedSiteName))
                    this.localizedSiteName = plateforme.getSite().getNom();
                if (plateforme.getSite().getTypeSite().getCode().equals("riviere"))
                    type = false;
            }
        }

        /**
         *
         * @return
         */
        public Plateforme getPlateforme() {
            return plateforme;
        }

        /**
         *
         * @return
         */
        public Boolean getSelected() {
            return selected;
        }

        /**
         *
         * @param selected
         */
        public void setSelected(Boolean selected) {
            this.selected = selected;
        }

        /**
         *
         * @return
         */
        public String getLocalizedPlateformeName() {
            return localizedPlateformeName;
        }

        /**
         *
         * @param localizedPlateformeName
         */
        public void setLocalizedPlateformeName(String localizedPlateformeName) {
            this.localizedPlateformeName = localizedPlateformeName;
        }

        /**
         *
         * @return
         */
        public String getLocalizedSiteName() {
            return localizedSiteName;
        }

        /**
         *
         * @param localizedSiteName
         */
        public void setLocalizedSiteName(String localizedSiteName) {
            this.localizedSiteName = localizedSiteName;
        }

        /**
         *
         * @return
         */
        public Boolean getType() {
            return type;
        }

        /**
         *
         * @param type
         */
        public void setType(Boolean type) {
            this.type = type;
        }
    }

    /**
     *
     */
    public class TreeNodeGroupeVariable {
        

        private GroupeVariableVO groupeVariable;
        private String localizedGroupeName;
        private List<TreeNodeGroupeVariable> groupesVariables = Lists.newArrayList();
        private Boolean expanded = false;

        private List<VOVariableJSF> variables = Lists.newArrayList();

        

        /**
         *
         * @return
         */
        public GroupeVariableVO getGroupeVariable() {
            return groupeVariable;
        }

        /**
         *
         * @param groupeVariable
         */
        public TreeNodeGroupeVariable(GroupeVariableVO groupeVariable) {
            super();
            this.groupeVariable = groupeVariable;
            if (groupeVariable != null) {
                this.localizedGroupeName = getPropertiesGroupeNames().getProperty(groupeVariable.getNom());
                if (Strings.isNullOrEmpty(this.localizedGroupeName))
                    this.localizedGroupeName = groupeVariable.getNom();
            }

        }

        /**
         *
         * @return
         */
        public List<TreeNodeGroupeVariable> getGroupesVariables() {
            if (groupesVariables.isEmpty()) {

                if (groupeVariable.getChildren() != null && !groupeVariable.getChildren().isEmpty()) {
                    for (GroupeVariableVO groupeVariableVO : groupeVariable.getChildren()) {
                        groupesVariables.add(new TreeNodeGroupeVariable(groupeVariableVO));
                    }
                }
            }
            return groupesVariables;
        }

        /**
         *
         * @return
         */
        public List<VOVariableJSF> getVariables() {
            if (variables.isEmpty()) {
                if (groupeVariable.getVariables() != null && !groupeVariable.getVariables().isEmpty() && !parametersRequest.plateformesSelected.isEmpty()) {
                    for (PlateformeVO plateforme : parametersRequest.plateformesSelected.values()) {
                        for (VariableVO variableVO : groupeVariable.getVariables()) {
                            if (securityContext.isRoot()
                                    || securityContext.matchPrivilege(String.format("*/%s/*/physico_chimie/%s", plateforme.getPlateforme().getSite().getCode(), variableVO.getCode()), Role.ROLE_EXTRACTION,
                                            ISecurityDatasetManager.PRIVILEGE_TYPE_DATASET))
                            {
                                VOVariableJSF voVariableJSF = new VOVariableJSF(variableVO);
                                Boolean isIn = false;
                                for (VOVariableJSF variable : variables) {
                                    if (voVariableJSF.getVariable().getCode().equals(variable.getVariable().getCode())) {
                                        isIn = true;
                                        break;
                                    }
                                }
                                if (!isIn) {
                                    variables.add(voVariableJSF);
                                }
                            }
                        }
                    }
                }
            }
            return variables;
        }

        /**
         *
         * @return
         */
        public Boolean getExpanded() {
            return expanded;
        }

        /**
         *
         * @param expanded
         */
        public void setExpanded(Boolean expanded) {
            this.expanded = expanded;
        }

        /**
         *
         * @return
         */
        public String getLocalizedGroupeName() {
            return localizedGroupeName;
        }

        /**
         *
         * @param localizedGroupeName
         */
        public void setLocalizedGroupeName(String localizedGroupeName) {
            this.localizedGroupeName = localizedGroupeName;
        }
    }

    /**
     *
     */
    public class VOVariableJSF {

        private VariableVO variable;
        private String localizedVariableName;
        private Boolean selected = false;

        /**
         *
         * @param variable
         */
        public VOVariableJSF(VariableVO variable) {
            super();
            this.variable = variable;
            if (variable != null) {
                this.localizedVariableName = getPropertiesVariableNames().getProperty(variable.getAffichage());
                if (Strings.isNullOrEmpty(this.localizedVariableName))
                    this.localizedVariableName = variable.getAffichage();
            }

        }

        /**
         *
         * @return
         */
        public VariableVO getVariable() {
            return variable;
        }

        /**
         *
         * @param variable
         */
        public void setVariable(VariableVO variable) {
            this.variable = variable;
        }

        /**
         *
         * @return
         */
        public Boolean getSelected() {
            return selected;
        }

        /**
         *
         * @param selected
         */
        public void setSelected(Boolean selected) {
            this.selected = selected;
        }

        /**
         *
         * @return
         */
        public String getLocalizedVariableName() {
            return localizedVariableName;
        }

        /**
         *
         * @param localizedVariableName
         */
        public void setLocalizedVariableName(String localizedVariableName) {
            this.localizedVariableName = localizedVariableName;
        }
    }

    /**
     *
     */
    public class ParametersRequest {

        private Map<Long, PlateformeVO> plateformesSelected = new HashMap<Long, PlateformeVO>();
        private Map<Long, VOVariableJSF> variablesSelected = new HashMap<Long, VOVariableJSF>();

        private DepthRequestParamVO depthRequestParam;
        private DatasRequestParamVO datasRequestParam;
        private DatesRequestParamVO datesRequestParam;
        private String commentExtraction;

        /**
         *
         * @return
         */
        public Boolean getPlateformeStepIsValid() {
            return !plateformesSelected.isEmpty();
        }

        /**
         *
         * @return
         */
        public Boolean getVariableStepIsValid() {
            return !variablesSelected.isEmpty() && getDateStepIsValid();
        }

        /**
         *
         * @return
         */
        public Boolean getType() {
            Boolean type = true;
            for (PlateformeVO plateforme : plateformesSelected.values()) {
                if (!plateforme.getType()) {
                    type = false;
                    break;
                }
            }
            return type;
        }

        /**
         *
         * @return
         */
        public Boolean getDephtDatatypeStepIsValid() {
            if (getDepthRequestParam().isValidDepht()
                    && (getDatasRequestParam().getDataBalancedByDepth() || getDatasRequestParam().getRawData() || getDatasRequestParam().getMaxValueAndAssociatedDepth() || getDatasRequestParam().getMinValueAndAssociatedDepth())
                    && getVariableStepIsValid())
            {
                return true;
            } else {
                return false;
            }
        }

        /**
         *
         * @return
         */
        public Boolean getDateStepIsValid() {

            if ((datesRequestParam.getSelectedFormSelection() != null) && getPlateformeStepIsValid() && datesRequestParam.getCurrentDatesFormParam().getIsValid()) {
                return true;
            } else {
                return false;
            }
        }

        private void initDatesRequestParamVO() {
            datesRequestParam = new DatesRequestParamVO(localizationManager);
        }

        /**
         *
         * @return
         * @throws ParseException
         */
        public Date getFirstStartDate() throws ParseException {
            if (!datesRequestParam.getCurrentDatesFormParam().getPeriods().isEmpty()) {
                Date firstDate = null;
                for (Date mapDate : datesRequestParam.getCurrentDatesFormParam().retrieveParametersMap().values()) {
                    if (firstDate == null) {
                        firstDate = mapDate;
                    } else if (firstDate.after(mapDate)) {
                        firstDate = mapDate;
                    }
                }
                return firstDate;
            }
            return null;
        }

        /**
         *
         * @return
         * @throws ParseException
         */
        public Date getLastEndDate() throws ParseException {
            if (!datesRequestParam.getCurrentDatesFormParam().getPeriods().isEmpty()) {
                Date lastDate = null;
                for (Date mapDate : datesRequestParam.getCurrentDatesFormParam().retrieveParametersMap().values()) {
                    if (lastDate == null) {
                        lastDate = mapDate;
                    } else if (lastDate.before(mapDate)) {
                        lastDate = mapDate;
                    }
                }
                return lastDate;
            }
            return null;
        }

        /**
         *
         * @return
         */
        public DatesRequestParamVO getDatesRequestParam() {
            if (datesRequestParam == null) {
                initDatesRequestParamVO();
            }
            return datesRequestParam;
        }

        /**
         *
         * @return
         */
        public Map<Long, PlateformeVO> getPlateformesSelected() {
            return plateformesSelected;
        }

        /**
         *
         * @return
         */
        public List<PlateformeVO> getListPlateformesSelected() {
            return new LinkedList<PlateformeVO>(plateformesSelected.values());
        }

        /**
         *
         * @return
         */
        public Map<Long, VOVariableJSF> getVariablesSelected() {
            return variablesSelected;
        }

        /**
         *
         * @return
         */
        public List<VOVariableJSF> getListVariablesSelected() {
            return new LinkedList<VOVariableJSF>(variablesSelected.values());
        }

        private void initDepthRequestParam() {
            depthRequestParam = new DepthRequestParamVO(localizationManager);
        }

        /**
         *
         * @return
         */
        public DepthRequestParamVO getDepthRequestParam() {
            if (depthRequestParam == null) {
                initDepthRequestParam();
            }
            return depthRequestParam;
        }

        private void initDatasRequestParam() {
            datasRequestParam = new DatasRequestParamVO(localizationManager);
        }

        /**
         *
         * @return
         */
        public DatasRequestParamVO getDatasRequestParam() {
            if (datasRequestParam == null) {
                initDatasRequestParam();
            }
            return datasRequestParam;
        }

        /**
         *
         * @return
         */
        public String getCommentExtraction() {
            return commentExtraction;
        }

        /**
         *
         * @param commentExtraction
         */
        public void setCommentExtraction(String commentExtraction) {
            this.commentExtraction = commentExtraction;
        }

        /**
         *
         * @return
         */
        public boolean getFormIsValid() {
            return getDephtDatatypeStepIsValid() && affichage != null;
        }

    }

    /**
     *
     * @param notificationsManager
     */
    public void setNotificationsManager(INotificationsManager notificationsManager) {
        this.notificationsManager = notificationsManager;
    }

    /**
     *
     * @param chimieDatatypeManager
     */
    public void setChimieDatatypeManager(IChimieDatatypeManager chimieDatatypeManager) {
        this.chimieDatatypeManager = chimieDatatypeManager;
    }

    /**
     *
     * @param idPlateformeSelected
     */
    public void setIdPlateformeSelected(Long idPlateformeSelected) {
        this.idPlateformeSelected = idPlateformeSelected;
    }

    /**
     *
     * @param idProjectSelected
     */
    public void setIdProjectSelected(Long idProjectSelected) {
        this.idProjectSelected = idProjectSelected;
    }

    /**
     *
     * @param extractionManager
     */
    public void setExtractionManager(IExtractionManager extractionManager) {
        this.extractionManager = extractionManager;
    }

    /**
     *
     * @param variableSelected
     */
    public void setVariableSelected(VOVariableJSF variableSelected) {
        this.variableSelected = variableSelected;
    }

    /**
     *
     * @return
     */
    public VOVariableJSF getVariableSelected() {
        return variableSelected;
    }

    /**
     *
     * @return
     */
    public UITree getTreeListVariables() {
        return treeListVariables;
    }

    /**
     *
     * @param treeListVariables
     */
    public void setTreeListVariables(UITree treeListVariables) {
        this.treeListVariables = treeListVariables;
    }

    /**
     *
     * @return
     */
    public UITreeNode getTreeNodeGroupeVariable() {
        return treeNodeGroupeVariable;
    }

    /**
     *
     * @param treeNodeGroupeVariable
     */
    public void setTreeNodeGroupeVariable(UITreeNode treeNodeGroupeVariable) {
        this.treeNodeGroupeVariable = treeNodeGroupeVariable;
    }

    /**
     *
     * @param treeNodeGroupeVariableSelected
     */
    public void setTreeNodeGroupeVariableSelected(TreeNodeGroupeVariable treeNodeGroupeVariableSelected) {
        this.treeNodeGroupeVariableSelected = treeNodeGroupeVariableSelected;
    }

    /**
     *
     * @return
     */
    public Long getIdProjectSelected() {
        return idProjectSelected;
    }

    /**
     *
     * @return
     */
    public Long getIdVariableSelected() {
        return idVariableSelected;
    }

    /**
     *
     * @param idVariableSelected
     */
    public void setIdVariableSelected(Long idVariableSelected) {
        this.idVariableSelected = idVariableSelected;
    }

    /**
     *
     * @return
     */
    public ParametersRequest getParametersRequest() {
        return parametersRequest;
    }

    /**
     *
     * @param securityContext
     */
    public void setSecurityContext(ISecurityContext securityContext) {
        this.securityContext = securityContext;
    }

    /**
     *
     * @return
     */
    @Override
    public boolean getIsStepValid() {
        if (getStep() == 1) {
            return getParametersRequest().getPlateformeStepIsValid();
        } else if (getStep() == 2) {
            return getParametersRequest().getDateStepIsValid();
        } else if (getStep() == 3) {
            return getParametersRequest().getVariableStepIsValid();
        } else if (getStep() == 4) {
            return getParametersRequest().getDephtDatatypeStepIsValid();
        }
        return false;
    }

    /**
     *
     * @param transactionManager
     */
    public void setTransactionManager(JpaTransactionManager transactionManager) {
        this.transactionManager = transactionManager;
    }

    /**
     *
     * @return
     */
    @Override
    public String nextStep() {
        super.nextStep();
        switch (getStep()) {
            case 3 :
                try {
                    createOrUpdateGroupVariablesAvailables();
                } catch (BusinessException e) {
                    rootNodes.clear();
                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                break;
            default :
                break;
        }
        return null;
    }

    /**
     *
     * @return
     */
    public Properties getPropertiesProjectsNames() {
        if (this.propertiesProjectsNames == null)
            this.setPropertiesProjectsNames(localizationManager.newProperties(Projet.NAME_TABLE, "nom"));
        return propertiesProjectsNames;
    }

    /**
     *
     * @param propertiesProjectsNames
     */
    public void setPropertiesProjectsNames(Properties propertiesProjectsNames) {
        this.propertiesProjectsNames = propertiesProjectsNames;
    }

    /**
     *
     * @return
     */
    public Properties getPropertiesPlatformNames() {
        if (this.propertiesPlatformNames == null)
            this.setPropertiesPlatformNames(localizationManager.newProperties(Plateforme.TABLE_NAME, "nom"));
        return propertiesPlatformNames;
    }

    /**
     *
     * @param propertiesPlatformNames
     */
    public void setPropertiesPlatformNames(Properties propertiesPlatformNames) {
        this.propertiesPlatformNames = propertiesPlatformNames;
    }

    /**
     *
     * @param localizationManager
     */
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    /**
     *
     * @return
     */
    public Properties getPropertiesSiteNames() {
        if (this.propertiesSiteNames == null)
            this.setPropertiesSiteNames(localizationManager.newProperties(Site.NAME_ENTITY_JPA, "nom"));
        return propertiesSiteNames;
    }

    /**
     *
     * @param propertiesSiteNames
     */
    public void setPropertiesSiteNames(Properties propertiesSiteNames) {
        this.propertiesSiteNames = propertiesSiteNames;
    }

    /**
     *
     * @return
     */
    public Properties getPropertiesVariableNames() {
        if (this.propertiesVariableNames == null)
            this.setPropertiesVariableNames(localizationManager.newProperties(Variable.NAME_ENTITY_JPA, "affichage"));
        return propertiesVariableNames;
    }

    /**
     *
     * @param propertiesVariableNames
     */
    public void setPropertiesVariableNames(Properties propertiesVariableNames) {
        this.propertiesVariableNames = propertiesVariableNames;
    }

    /**
     *
     * @return
     */
    public Properties getPropertiesGroupeNames() {
        if (this.propertiesGroupeNames == null)
            this.setPropertiesGroupeNames(localizationManager.newProperties(GroupeVariable.TABLE_NAME, "nom_groupe"));
        return propertiesGroupeNames;
    }

    /**
     *
     * @param propertiesGroupeNames
     */
    public void setPropertiesGroupeNames(Properties propertiesGroupeNames) {
        this.propertiesGroupeNames = propertiesGroupeNames;
    }

    /**
     *
     * @return
     */
    public String getAffichage() {
        return affichage;
    }

    /**
     *
     * @param affichage
     */
    public void setAffichage(String affichage) {
        this.affichage = affichage;
    }

    /**
     *
     * @param fileCompManager
     */
    public void setFileCompManager(IFileCompManager fileCompManager) {
        this.fileCompManager = fileCompManager;
    }
}
