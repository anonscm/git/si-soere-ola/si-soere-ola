package org.inra.ecoinfo.glacpe.dataset.conditionprelevement.jsf;

import java.io.Serializable;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.inra.ecoinfo.dataset.security.ISecurityDatasetManager;
import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.filecomp.IFileCompManager;
import org.inra.ecoinfo.filecomp.ItemFile;
import org.inra.ecoinfo.filecomp.entity.FileComp;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.IConditionGeneraleDatatypeManager;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.impl.ConditionPrelevementParameters;
import org.inra.ecoinfo.glacpe.extraction.jsf.AbstractUIBeanForSteps;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.ProjetSiteVO;
import org.inra.ecoinfo.glacpe.extraction.vo.VariableVO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.projetsite.ProjetSite;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.notifications.INotificationsManager;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.security.ISecurityContext;
import org.inra.ecoinfo.security.entity.Role;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;

/**
 *
 * @author ptcherniati
 */
@ManagedBean(name = "uiConditionPrelevement")
@ViewScoped
public class UIBeanConditionPrelevement extends AbstractUIBeanForSteps implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final String DATATYPE = "conditions_prelevements";
    private static final String THEME = "conditions_generales";
    private static final String PATH_PATTERN = "%s/%s/%s/%s/%s";

    /**
     *
     */
    @ManagedProperty(value = "#{conditionGeneraleDatatypeManager}")
    protected IConditionGeneraleDatatypeManager conditionGeneraleDatatypeManager;

    /**
     *
     */
    @ManagedProperty(value = "#{notificationsManager}")
    protected INotificationsManager notificationsManager;

    /**
     *
     */
    @ManagedProperty(value = "#{extractionManager}")
    protected IExtractionManager extractionManager;

    /**
     *
     */
    @ManagedProperty(value = "#{transactionManager}")
    protected JpaTransactionManager transactionManager;

    /**
     *
     */
    @ManagedProperty(value = "#{securityContext}")
    protected ISecurityContext securityContext;

    /**
     *
     */
    @ManagedProperty(value = "#{localizationManager}")
    protected ILocalizationManager localizationManager;

    /**
     *
     */
    @ManagedProperty(value = "#{fileCompManager}")
    protected IFileCompManager fileCompManager;

    private Map<Long, ProjectVO> projectsAvailables = new HashMap<Long, ProjectVO>();

    private ParametersRequest parametersRequest = new ParametersRequest();

    private Properties propertiesProjectsNames;
    private Properties propertiesPlatformNames;
    private Properties propertiesSiteNames;

    private Long idProjectSelected;

    private Long idPlateformeSelected;

    /**
     *
     */
    public UIBeanConditionPrelevement() {}

    /**
     *
     * @return
     */
    public String navigate() {
        return "conditionPrelevement";
    }

    /**
     *
     * @return
     * @throws BusinessException
     */
    public String selectPlateforme() throws BusinessException {
        ProjectVO projectSelected = projectsAvailables.get(idProjectSelected);
        PlateformeVO plateformeSelected = projectSelected.getPlateformes().get(idPlateformeSelected);
        if (plateformeSelected.getSelected()) {
            parametersRequest.getPlateformesSelected().remove(idPlateformeSelected);
            plateformeSelected.setSelected(false);
        } else {
            parametersRequest.getPlateformesSelected().put(idPlateformeSelected, plateformeSelected);
            plateformeSelected.setSelected(true);
        }

        return null;
    }

    /**
     *
     * @return
     * @throws BusinessException
     */
    public String changeProject() throws BusinessException {
        parametersRequest.getPlateformesSelected().clear();
        for (ProjectVO projectSelected : projectsAvailables.values()) {
            for (PlateformeVO plateformeSelected : projectSelected.getPlateformes().values()) {
                plateformeSelected.setSelected(false);
            }
        }
        return null;
    }

    /**
     *
     * @return
     * @throws BusinessException
     */
    public String getProjetSelected() throws BusinessException {
        String localizedProjetSelected = null;
        if (idProjectSelected != null) {
            String nom = projectsAvailables.get(idProjectSelected).getProjet().getNom();
            localizedProjetSelected = getPropertiesProjectsNames().getProperty(nom);
            if (Strings.isNullOrEmpty(localizedProjetSelected))
                localizedProjetSelected = nom;
            return localizedProjetSelected;
        }
        return localizedProjetSelected;
    }

    /**
     *
     * @return
     * @throws BusinessException
     */
    public Map<Long, ProjectVO> getProjectsAvailables() throws BusinessException {

        TransactionStatus status = transactionManager.getTransaction(new DefaultTransactionDefinition(TransactionDefinition.PROPAGATION_REQUIRES_NEW));
        if (projectsAvailables.isEmpty()) {

            List<Projet> projets = conditionGeneraleDatatypeManager.retrieveConditionGeneraleAvailablesProjets();

            for (Projet projet : projets) {
                List<Plateforme> plateformes = Lists.newArrayList();
                for (ProjetSite projetSite : projet.getProjetsSites()) {
                    for (Plateforme plateforme : conditionGeneraleDatatypeManager.retrieveConditionGeneraleAvailablesPlateformesByProjetSiteId(projetSite.getId())) {
                        if (!plateformes.contains(plateforme)
                                && (securityContext.isRoot() || securityContext.matchPrivilege(String.format("%s/%s/*/conditions_prelevements/*", projetSite.getProjet().getCode(), projetSite.getSite().getCode()), Role.ROLE_EXTRACTION,
                                        ISecurityDatasetManager.PRIVILEGE_TYPE_DATASET)))
                        {
                            plateformes.add(plateforme);
                        }
                    }
                }
                if (!plateformes.isEmpty()) {
                    ProjectVO projectVO = new ProjectVO(projet, plateformes);
                    projectsAvailables.put(projet.getId(), projectVO);
                }
            }

        }

        transactionManager.commit(status);
        return projectsAvailables;
    }

    private Boolean checkAvailableVariables() throws BusinessException, ParseException {
        if (parametersRequest.getDateStepIsValid()) {
            List<VariableVO> variables = conditionGeneraleDatatypeManager.retrieveListsVariables(new LinkedList<Long>(parametersRequest.plateformesSelected.keySet()), parametersRequest.getFirstStartDate(), parametersRequest.getLastEndDate());
            List<VariableVO> variablesAvailable = new LinkedList<VariableVO>();
            for (PlateformeVO plateforme : parametersRequest.plateformesSelected.values()) {
                for (VariableVO variable : variables) {
                    if (securityContext.isRoot()
                            || securityContext.matchPrivilege(String.format("*/%s/*/conditions_prelevements/%s", plateforme.getPlateforme().getSite().getCode(), variable.getCode()), Role.ROLE_EXTRACTION,
                                    ISecurityDatasetManager.PRIVILEGE_TYPE_DATASET))
                    {
                        if (!variablesAvailable.contains(variable)) {
                            variablesAvailable.add(variable);
                        }
                    }
                }
            }
            if (!variablesAvailable.isEmpty())
                return true;
        }
        return false;
    }

    private List<String> getPathes(List<ProjetSite> projetSites, List<VariableVO> variables) {
        String sitePath, projet, theme, path;
        theme = THEME;
        List<String> pathes = new LinkedList<>();
        for (ProjetSite projetSite : projetSites) {
            sitePath = projetSite.getSite().getPath();
            projet = projetSite.getProjet().getCode();
            for (VariableVO variable : variables) {
                path = String.format(PATH_PATTERN, projet, sitePath, theme, DATATYPE, variable.getCode());
                if (!pathes.contains(path)) {
                    pathes.add(path);
                }
            }
        }
        return pathes;
    }

    /**
     *
     * @return
     * @throws BusinessException
     */
    public String extract() throws BusinessException {
        Map<String, Object> metadatasMap = new HashMap<String, Object>();
        List<org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO> metadatasPlateformesVos = new LinkedList<org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO>();
        List<ProjetSite> projetSites = new LinkedList<ProjetSite>();
        for (PlateformeVO plateforme : parametersRequest.getPlateformesSelected().values()) {
            org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO plateformeVO = new org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO(plateforme.getPlateforme());
            ProjetSite projetSite = conditionGeneraleDatatypeManager.retrieveConditionGeneraleAvailablesProjetsSiteByProjetAndSite(projectsAvailables.get(idProjectSelected).getProjet().getId(), plateforme.getPlateforme().getSite().getId());
            ProjetSiteVO projetSiteVO = new ProjetSiteVO(projetSite);
            if (!projetSites.contains(projetSite)) {
                projetSites.add(projetSite);
            }
            plateformeVO.setProjet(projetSiteVO);
            metadatasPlateformesVos.add(plateformeVO);
        }

        metadatasMap.put(PlateformeVO.class.getSimpleName(), metadatasPlateformesVos);
        List<VariableVO> variablesVO = conditionGeneraleDatatypeManager.retrieveListVariables();
        metadatasMap.put(VariableVO.class.getSimpleName(), variablesVO);
        List<FileComp> listFiles = new LinkedList<FileComp>();
        try {
            if (!parametersRequest.plateformesSelected.isEmpty() && parametersRequest.getFormIsValid()) {
                IntervalDate intervalDate;

                intervalDate = new IntervalDate(parametersRequest.getFirstStartDate(), parametersRequest.getLastEndDate(), DateUtil.getSimpleDateFormatDateLocale());

                List<IntervalDate> intervalsDate = new LinkedList<>();
                intervalsDate.add(intervalDate);
                final List<ItemFile> files = fileCompManager.getFileCompFromPathesAndIntervalsDate(getPathes(projetSites, variablesVO), intervalsDate);
                for (final ItemFile file : files) {
                    if (file.getMandatory()) {
                        listFiles.add(file.getFileComp());
                    }
                }
            }
        } catch (BadExpectedValueException e) {
            throw new BusinessException("bad file", e);
        } catch (ParseException e) {
            throw new BusinessException("bad file name", e);
        }
        metadatasMap.put(FileComp.class.getSimpleName(), listFiles);

        parametersRequest.getDatesRequestParam().getDatesYearsDiscretsFormParam().setPeriods(parametersRequest.getDatesRequestParam().getNonPeriodsYearsContinuous());
        parametersRequest.getDatesRequestParam().getDatesYearsRangeFormParam().setPeriods(parametersRequest.getDatesRequestParam().getNonPeriodsYearsContinuous());

        metadatasMap.put(DatesRequestParamVO.class.getSimpleName(), parametersRequest.getDatesRequestParam().clone());
        metadatasMap.put(IExtractionManager.KEYMAP_COMMENTS, new String(parametersRequest.getCommentExtraction()));
        IParameter parameters = new ConditionPrelevementParameters(metadatasMap);
        extractionManager.extract(parameters, 1);

        return null;
    }

    /**
     *
     */
    public class ProjectVO {

        private Projet projet;
        private String localizedProjetName;
        private Map<Long, PlateformeVO> plateformes = new HashMap<Long, PlateformeVO>();

        /**
         *
         * @param projet
         * @param plateformes
         */
        public ProjectVO(Projet projet, List<Plateforme> plateformes) {
            super();
            this.projet = projet;
            if (projet != null) {
                this.localizedProjetName = getPropertiesProjectsNames().getProperty(projet.getNom());
                if (Strings.isNullOrEmpty(this.localizedProjetName))
                    this.localizedProjetName = projet.getNom();
            }
            for (Plateforme plateforme : plateformes) {
                this.plateformes.put(plateforme.getId(), new PlateformeVO(plateforme));
            }

        }

        /**
         *
         * @return
         */
        public Projet getProjet() {
            return projet;
        }

        /**
         *
         * @return
         */
        public Map<Long, PlateformeVO> getPlateformes() {
            return plateformes;
        }

        /**
         *
         * @return
         */
        public String getLocalizedProjetName() {
            return localizedProjetName;
        }

        /**
         *
         * @param localizedProjetName
         */
        public void setLocalizedProjetName(String localizedProjetName) {
            this.localizedProjetName = localizedProjetName;
        }
    }

    /**
     *
     */
    public class PlateformeVO {

        private Plateforme plateforme;
        private String localizedPlateformeName;
        private String localizedSiteName;
        private Boolean selected = false;

        /**
         *
         * @param plateforme
         */
        public PlateformeVO(Plateforme plateforme) {
            super();
            this.plateforme = plateforme;
            if (plateforme != null) {
                this.localizedPlateformeName = getPropertiesPlatformNames().getProperty(plateforme.getNom());
                if (Strings.isNullOrEmpty(this.localizedPlateformeName))
                    this.localizedPlateformeName = plateforme.getNom();
                this.localizedSiteName = getPropertiesSiteNames().getProperty(plateforme.getSite().getNom());
                if (Strings.isNullOrEmpty(this.localizedSiteName))
                    this.localizedSiteName = plateforme.getSite().getNom();
            }
        }

        /**
         *
         * @return
         */
        public Plateforme getPlateforme() {
            return plateforme;
        }

        /**
         *
         * @return
         */
        public Boolean getSelected() {
            return selected;
        }

        /**
         *
         * @param selected
         */
        public void setSelected(Boolean selected) {
            this.selected = selected;
        }

        /**
         *
         * @return
         */
        public String getLocalizedPlateformeName() {
            return localizedPlateformeName;
        }

        /**
         *
         * @param localizedPlateformeName
         */
        public void setLocalizedPlateformeName(String localizedPlateformeName) {
            this.localizedPlateformeName = localizedPlateformeName;
        }

        /**
         *
         * @return
         */
        public String getLocalizedSiteName() {
            return localizedSiteName;
        }

        /**
         *
         * @param localizedSiteName
         */
        public void setLocalizedSiteName(String localizedSiteName) {
            this.localizedSiteName = localizedSiteName;
        }
    }

    /**
     *
     */
    public class ParametersRequest {

        private Map<Long, PlateformeVO> plateformesSelected = new HashMap<Long, PlateformeVO>();

        private DatesRequestParamVO datesRequestParam;
        private String commentExtraction;

        /**
         *
         * @return
         */
        public Boolean getPlateformeStepIsValid() {
            return !plateformesSelected.isEmpty();
        }

        /**
         *
         * @return
         */
        public Boolean getDateStepIsValid() {
            if ((datesRequestParam.getSelectedFormSelection() != null) && getPlateformeStepIsValid() && datesRequestParam.getCurrentDatesFormParam().getIsValid()) {
                return true;
            } else {
                return false;
            }
        }

        /**
         *
         * @return
         */
        public DatesRequestParamVO getDatesRequestParam() {
            if (datesRequestParam == null) {
                initDatesRequestParam();
            }
            return datesRequestParam;
        }

        /**
         *
         * @return
         * @throws ParseException
         */
        public Date getFirstStartDate() throws ParseException {
            if (!datesRequestParam.getCurrentDatesFormParam().getPeriods().isEmpty()) {
                Date firstDate = null;
                for (Date mapDate : datesRequestParam.getCurrentDatesFormParam().retrieveParametersMap().values()) {
                    if (firstDate == null) {
                        firstDate = mapDate;
                    } else if (firstDate.after(mapDate)) {
                        firstDate = mapDate;
                    }
                }
                return firstDate;
            }
            return null;
        }

        /**
         *
         * @return
         * @throws ParseException
         */
        public Date getLastEndDate() throws ParseException {
            if (!datesRequestParam.getCurrentDatesFormParam().getPeriods().isEmpty()) {
                Date lastDate = null;
                for (Date mapDate : datesRequestParam.getCurrentDatesFormParam().retrieveParametersMap().values()) {
                    if (lastDate == null) {
                        lastDate = mapDate;
                    } else if (lastDate.before(mapDate)) {
                        lastDate = mapDate;
                    }
                }
                return lastDate;
            }
            return null;
        }

        private void initDatesRequestParam() {
            datesRequestParam = new DatesRequestParamVO(localizationManager);
        }

        /**
         *
         * @return
         */
        public Map<Long, PlateformeVO> getPlateformesSelected() {
            return plateformesSelected;
        }

        /**
         *
         * @return
         */
        public List<PlateformeVO> getListPlateformesSelected() {
            return new LinkedList<PlateformeVO>(plateformesSelected.values());
        }

        /**
         *
         * @return
         */
        public String getCommentExtraction() {
            return commentExtraction;
        }

        /**
         *
         * @param commentExtraction
         */
        public void setCommentExtraction(String commentExtraction) {
            this.commentExtraction = commentExtraction;
        }

        /**
         *
         * @return
         * @throws BusinessException
         * @throws ParseException
         */
        public boolean getFormIsValid() throws BusinessException, ParseException {
            return getDateStepIsValid() && checkAvailableVariables();
        }

        /**
         *
         * @return
         * @throws BusinessException
         * @throws ParseException
         */
        public boolean getResultAvailable() throws BusinessException, ParseException {
            return getDateStepIsValid() && !checkAvailableVariables();
        }
    }

    /**
     *
     * @return
     */
    @Override
    public boolean getIsStepValid() {
        if (getStep() == 1) {
            return getParametersRequest().getPlateformeStepIsValid();
        }
        return false;
    }

    /**
     *
     * @return
     */
    public ParametersRequest getParametersRequest() {
        return parametersRequest;
    }

    /**
     *
     * @param parametersRequest
     */
    public void setParametersRequest(ParametersRequest parametersRequest) {
        this.parametersRequest = parametersRequest;
    }

    /**
     *
     * @param projectsAvailables
     */
    public void setProjectsAvailables(Map<Long, ProjectVO> projectsAvailables) {
        this.projectsAvailables = projectsAvailables;
    }

    /**
     *
     * @return
     */
    public Long getIdProjectSelected() {
        return idProjectSelected;
    }

    /**
     *
     * @param idProjectSelected
     */
    public void setIdProjectSelected(Long idProjectSelected) {
        this.idProjectSelected = idProjectSelected;
    }

    /**
     *
     * @return
     */
    public Long getIdPlateformeSelected() {
        return idPlateformeSelected;
    }

    /**
     *
     * @param idPlateformeSelected
     */
    public void setIdPlateformeSelected(Long idPlateformeSelected) {
        this.idPlateformeSelected = idPlateformeSelected;
    }

    /**
     *
     * @param conditionGeneraleDatatypeManager
     */
    public void setConditionGeneraleDatatypeManager(IConditionGeneraleDatatypeManager conditionGeneraleDatatypeManager) {
        this.conditionGeneraleDatatypeManager = conditionGeneraleDatatypeManager;
    }

    /**
     *
     * @param notificationsManager
     */
    public void setNotificationsManager(INotificationsManager notificationsManager) {
        this.notificationsManager = notificationsManager;
    }

    /**
     *
     * @param extractionManager
     */
    public void setExtractionManager(IExtractionManager extractionManager) {
        this.extractionManager = extractionManager;
    }

    /**
     *
     * @param transactionManager
     */
    public void setTransactionManager(JpaTransactionManager transactionManager) {
        this.transactionManager = transactionManager;
    }

    /**
     *
     * @param securityContext
     */
    public void setSecurityContext(ISecurityContext securityContext) {
        this.securityContext = securityContext;
    }

    /**
     *
     * @return
     */
    public Properties getPropertiesProjectsNames() {
        if (this.propertiesProjectsNames == null)
            this.setPropertiesProjectsNames(localizationManager.newProperties(Projet.NAME_TABLE, "nom"));
        return propertiesProjectsNames;
    }

    /**
     *
     * @param propertiesProjectsNames
     */
    public void setPropertiesProjectsNames(Properties propertiesProjectsNames) {
        this.propertiesProjectsNames = propertiesProjectsNames;
    }

    /**
     *
     * @return
     */
    public Properties getPropertiesPlatformNames() {
        if (this.propertiesPlatformNames == null)
            this.setPropertiesPlatformNames(localizationManager.newProperties(Plateforme.TABLE_NAME, "nom"));
        return propertiesPlatformNames;
    }

    /**
     *
     * @param propertiesPlatformNames
     */
    public void setPropertiesPlatformNames(Properties propertiesPlatformNames) {
        this.propertiesPlatformNames = propertiesPlatformNames;
    }

    /**
     *
     * @param localizationManager
     */
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    /**
     *
     * @return
     */
    public Properties getPropertiesSiteNames() {
        if (this.propertiesSiteNames == null)
            this.setPropertiesSiteNames(localizationManager.newProperties(Site.NAME_ENTITY_JPA, "nom"));
        return propertiesSiteNames;
    }

    /**
     *
     * @param propertiesSiteNames
     */
    public void setPropertiesSiteNames(Properties propertiesSiteNames) {
        this.propertiesSiteNames = propertiesSiteNames;
    }

    /**
     *
     * @param fileCompManager
     */
    public void setFileCompManager(IFileCompManager fileCompManager) {
        this.fileCompManager = fileCompManager;
    }

}
