package org.inra.ecoinfo.glacpe.dataset.contenustomacaux.jsf;

import java.io.Serializable;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.faces.bean.ManagedProperty;

import org.inra.ecoinfo.dataset.security.ISecurityDatasetManager;
import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.glacpe.dataset.contenustomacaux.IContenuStomacauxDatatypeManager;
import org.inra.ecoinfo.glacpe.extraction.jsf.AbstractUIBeanForSteps;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.VariableVO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.projetsite.ProjetSite;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.notifications.INotificationsManager;
import org.inra.ecoinfo.security.ISecurityContext;
import org.inra.ecoinfo.security.entity.Role;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;

/**
 *
 * @author ptcherniati
 */
public class UIBeanContenuStomacaux extends AbstractUIBeanForSteps implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @ManagedProperty(value = "#{contenuStomacauxDatatypeManager}")
    protected IContenuStomacauxDatatypeManager contenuStomacauxDatatypeManager;

    /**
     *
     */
    @ManagedProperty(value = "#{notificationsManager}")
    protected INotificationsManager notificationsManager;

    /**
     *
     */
    @ManagedProperty(value = "#{extractionManager}")
    protected IExtractionManager extractionManager;

    /**
     *
     */
    @ManagedProperty(value = "#{transactionManager}")
    protected JpaTransactionManager transactionManager;

    /**
     *
     */
    @ManagedProperty(value = "#{securityContext}")
    protected ISecurityContext securityContext;

    /**
     *
     */
    @ManagedProperty(value = "#{localizationManager}")
    protected ILocalizationManager localizationManager;

    private Map<Long, ProjectVO> projectsAvailables = new HashMap<Long, ProjectVO>();
    private Map<Long, VOVariableJSF> variablesAvailables = new HashMap<Long, VOVariableJSF>();

    private ParametersRequest parametersRequest = new ParametersRequest();

    private Properties propertiesProjectsNames;
    private Properties propertiesPlatformNames;
    private Properties propertiesSiteNames;
    private Properties propertiesVariableNames;

    private Long idProjectSelected;

    private Long idPlateformeSelected;

    private Long idVariableSelected;

    /**
     *
     */
    public UIBeanContenuStomacaux() {}

    /**
     *
     * @return
     */
    public String navigate() {
        return "contenuStomacaux";
    }

    /**
     *
     * @return
     * @throws BusinessException
     */
    public String addAllVariables() throws BusinessException {
        Map<Long, VOVariableJSF> variablesSelected = parametersRequest.getVariablesSelected();
        variablesSelected.clear();

        for (VOVariableJSF variable : variablesAvailables.values()) {
            if (variable.getVariable().getIsDataAvailable()) {
                variablesSelected.put(variable.getVariable().getId(), variable);
                variable.setSelected(true);
            }
        }
        return null;
    }

    /**
     *
     * @return
     * @throws BusinessException
     * @throws ParseException
     */
    public String removeAllVariables() throws BusinessException, ParseException {
        createOrUpdateListVariablesAvailables();
        return null;
    }

    /**
     *
     * @return
     * @throws BusinessException
     */
    public String selectPlateforme() throws BusinessException {
        ProjectVO projectSelected = projectsAvailables.get(idProjectSelected);
        PlateformeVO plateformeSelected = projectSelected.getPlateformes().get(idPlateformeSelected);
        if (plateformeSelected.getSelected()) {
            parametersRequest.getPlateformesSelected().remove(idPlateformeSelected);
            plateformeSelected.setSelected(false);
        } else {
            parametersRequest.getPlateformesSelected().put(idPlateformeSelected, plateformeSelected);
            plateformeSelected.setSelected(true);
        }

        return null;
    }

    /**
     *
     * @return
     * @throws BusinessException
     */
    public String changeProject() throws BusinessException {
        parametersRequest.getPlateformesSelected().clear();
        for (ProjectVO projectSelected : projectsAvailables.values()) {
            for (PlateformeVO plateformeSelected : projectSelected.getPlateformes().values()) {
                plateformeSelected.setSelected(false);
            }
        }

        return null;
    }

    /**
     *
     * @return
     * @throws BusinessException
     */
    public String getProjetSelected() throws BusinessException {
        String localizedProjetSelected = null;
        if (idProjectSelected != null) {
            String nom = projectsAvailables.get(idProjectSelected).getProjet().getNom();
            localizedProjetSelected = getPropertiesProjectsNames().getProperty(nom);
            if (Strings.isNullOrEmpty(localizedProjetSelected))
                localizedProjetSelected = nom;
            return localizedProjetSelected;
        }
        return localizedProjetSelected;
    }

    /**
     *
     * @return
     * @throws BusinessException
     */
    public String selectVariable() throws BusinessException {
        VOVariableJSF variableSelected = variablesAvailables.get(idVariableSelected);

        if (variableSelected.getSelected()) {
            parametersRequest.getVariablesSelected().remove(idVariableSelected);
            variableSelected.setSelected(false);
        } else {
            parametersRequest.getVariablesSelected().put(idVariableSelected, variableSelected);
            variableSelected.setSelected(true);

        }
        return null;
    }

    /**
     *
     * @return
     * @throws BusinessException
     * @throws ParseException
     */
    public List<VOVariableJSF> getVariablesAvailables() throws BusinessException, ParseException {
        if (variablesAvailables == null) {
            createOrUpdateListVariablesAvailables();
        }
        return new LinkedList<VOVariableJSF>(variablesAvailables.values());

    }

    private void createOrUpdateListVariablesAvailables() throws BusinessException, ParseException {
        variablesAvailables.clear();
        parametersRequest.getVariablesSelected().clear();

        if (!parametersRequest.plateformesSelected.isEmpty() && parametersRequest.getDateStepIsValid()) {

            List<VariableVO> variables = contenuStomacauxDatatypeManager.retrieveListsVariables(new LinkedList<Long>(parametersRequest.plateformesSelected.keySet()), parametersRequest.getFirstStartDate(), parametersRequest.getLastEndDate());
            for (PlateformeVO plateforme : parametersRequest.plateformesSelected.values()) {
                for (VariableVO variable : variables) {
                    if (securityContext.isRoot()
                            || securityContext.matchPrivilege(String.format("*/%s/*/contenus_stomacaux/%s", plateforme.getPlateforme().getSite().getCode(), variable.getCode()), Role.ROLE_EXTRACTION, ISecurityDatasetManager.PRIVILEGE_TYPE_DATASET))
                    {
                        if (!variablesAvailables.keySet().contains(variable.getId())) {
                            VOVariableJSF variableVO = new VOVariableJSF(variable);
                            variablesAvailables.put(variable.getId(), variableVO);
                        }
                    }
                }
            }
        }
    }

    /**
     *
     * @return
     * @throws BusinessException
     */
    public Map<Long, ProjectVO> getProjectsAvailables() throws BusinessException {
        TransactionStatus status = transactionManager.getTransaction(new DefaultTransactionDefinition(TransactionDefinition.PROPAGATION_REQUIRES_NEW));

        if (projectsAvailables.isEmpty()) {
            List<Projet> projets = contenuStomacauxDatatypeManager.retrieveContenuStomacauxAvailablesProjets();

            for (Projet projet : projets) {
                List<Plateforme> plateformes = Lists.newArrayList();
                for (ProjetSite projetSite : projet.getProjetsSites()) {
                    for (Plateforme plateforme : contenuStomacauxDatatypeManager.retrieveContenuStomacauxAvailablesPlateformesByProjetSiteId(projetSite.getId())) {
                        if (!plateformes.contains(plateforme)
                                && (securityContext.isRoot() || securityContext.matchPrivilege(String.format("%s/%s/*/contenus_stomacaux/*", projetSite.getProjet().getCode(), projetSite.getSite().getCode()), Role.ROLE_EXTRACTION,
                                        ISecurityDatasetManager.PRIVILEGE_TYPE_DATASET)))
                        {
                            plateformes.add(plateforme);
                        }
                    }
                }
                if (!plateformes.isEmpty()) {
                    ProjectVO projectVO = new ProjectVO(projet, plateformes);
                    projectsAvailables.put(projet.getId(), projectVO);
                }
            }

        }

        transactionManager.commit(status);

        return projectsAvailables;
    }

    /**
     *
     */
    public class ParametersRequest {

        private Map<Long, PlateformeVO> plateformesSelected = new HashMap<Long, PlateformeVO>();
        private Map<Long, VOVariableJSF> variablesSelected = new HashMap<Long, VOVariableJSF>();

        private DatesRequestParamVO datesRequestParam;
        private String commentExtraction;

        /**
         *
         * @return
         */
        public Boolean getPlateformeStepIsValid() {
            return !plateformesSelected.isEmpty();
        }

        /**
         *
         * @return
         */
        public Boolean getVariableStepIsValid() {
            return !variablesSelected.isEmpty() && getDateStepIsValid();
        }

        /**
         *
         * @return
         */
        public Boolean getDateStepIsValid() {
            if ((datesRequestParam.getSelectedFormSelection() != null) && datesRequestParam.getCurrentDatesFormParam().getIsValid() && getPlateformeStepIsValid()) {
                return true;
            } else {
                return false;
            }
        }

        /**
         *
         * @return
         * @throws ParseException
         */
        public Date getFirstStartDate() throws ParseException {
            if (!datesRequestParam.getCurrentDatesFormParam().getPeriods().isEmpty()) {
                Date firstDate = null;
                for (Date mapDate : datesRequestParam.getCurrentDatesFormParam().retrieveParametersMap().values()) {
                    if (firstDate == null) {
                        firstDate = mapDate;
                    } else if (firstDate.after(mapDate)) {
                        firstDate = mapDate;
                    }
                }
                return firstDate;
            }
            return null;
        }

        /**
         *
         * @return
         * @throws ParseException
         */
        public Date getLastEndDate() throws ParseException {
            if (!datesRequestParam.getCurrentDatesFormParam().getPeriods().isEmpty()) {
                Date lastDate = null;
                for (Date mapDate : datesRequestParam.getCurrentDatesFormParam().retrieveParametersMap().values()) {
                    if (lastDate == null) {
                        lastDate = mapDate;
                    } else if (lastDate.before(mapDate)) {
                        lastDate = mapDate;
                    }
                }
                return lastDate;
            }
            return null;
        }

        /**
         *
         * @return
         */
        public DatesRequestParamVO getDatesRequestParam() {
            if (datesRequestParam == null) {
                initDatesRequestParam();
            }
            return datesRequestParam;
        }

        private void initDatesRequestParam() {
            datesRequestParam = new DatesRequestParamVO(localizationManager);
        }

        /**
         *
         * @return
         */
        public Map<Long, PlateformeVO> getPlateformesSelected() {
            return plateformesSelected;
        }

        /**
         *
         * @return
         */
        public List<PlateformeVO> getListPlateformesSelected() {
            return new LinkedList<PlateformeVO>(plateformesSelected.values());
        }

        /**
         *
         * @return
         */
        public Map<Long, VOVariableJSF> getVariablesSelected() {
            return variablesSelected;
        }

        /**
         *
         * @return
         */
        public List<VOVariableJSF> getListVariablesSelected() {
            return new LinkedList<VOVariableJSF>(variablesSelected.values());
        }

        /**
         *
         * @return
         */
        public String getCommentExtraction() {
            return commentExtraction;
        }

        /**
         *
         * @param commentExtraction
         */
        public void setCommentExtraction(String commentExtraction) {
            this.commentExtraction = commentExtraction;
        }

        /**
         *
         * @return
         */
        public boolean getFormIsValid() {
            return getVariableStepIsValid();
        }
    }

    /**
     *
     */
    public class ProjectVO {

        private Projet projet;
        private String localizedProjetName;
        private Map<Long, PlateformeVO> plateformes = new HashMap<Long, PlateformeVO>();

        /**
         *
         * @param projet
         * @param plateformes
         */
        public ProjectVO(Projet projet, List<Plateforme> plateformes) {
            super();
            this.projet = projet;
            if (projet != null) {
                this.localizedProjetName = getPropertiesProjectsNames().getProperty(projet.getNom());
                if (Strings.isNullOrEmpty(this.localizedProjetName))
                    this.localizedProjetName = projet.getNom();
            }
            for (Plateforme plateforme : plateformes) {
                this.plateformes.put(plateforme.getId(), new PlateformeVO(plateforme));
            }

        }

        /**
         *
         * @return
         */
        public Projet getProjet() {
            return projet;
        }

        /**
         *
         * @return
         */
        public Map<Long, PlateformeVO> getPlateformes() {
            return plateformes;
        }

        /**
         *
         * @return
         */
        public String getLocalizedProjetName() {
            return localizedProjetName;
        }

        /**
         *
         * @param localizedProjetName
         */
        public void setLocalizedProjetName(String localizedProjetName) {
            this.localizedProjetName = localizedProjetName;
        }
    }

    /**
     *
     */
    public class PlateformeVO {

        private Plateforme plateforme;
        private String localizedPlateformeName;
        private String localizedSiteName;
        private Boolean selected = false;

        /**
         *
         * @param plateforme
         */
        public PlateformeVO(Plateforme plateforme) {
            super();
            this.plateforme = plateforme;
            if (plateforme != null) {
                this.localizedPlateformeName = getPropertiesPlatformNames().getProperty(plateforme.getNom());
                if (Strings.isNullOrEmpty(this.localizedPlateformeName))
                    this.localizedPlateformeName = plateforme.getNom();
                this.localizedSiteName = getPropertiesSiteNames().getProperty(plateforme.getSite().getNom());
                if (Strings.isNullOrEmpty(this.localizedSiteName))
                    this.localizedSiteName = plateforme.getSite().getNom();
            }
        }

        /**
         *
         * @return
         */
        public Plateforme getPlateforme() {
            return plateforme;
        }

        /**
         *
         * @return
         */
        public Boolean getSelected() {
            return selected;
        }

        /**
         *
         * @param selected
         */
        public void setSelected(Boolean selected) {
            this.selected = selected;
        }

        /**
         *
         * @return
         */
        public String getLocalizedPlateformeName() {
            return localizedPlateformeName;
        }

        /**
         *
         * @param localizedPlateformeName
         */
        public void setLocalizedPlateformeName(String localizedPlateformeName) {
            this.localizedPlateformeName = localizedPlateformeName;
        }

        /**
         *
         * @return
         */
        public String getLocalizedSiteName() {
            return localizedSiteName;
        }

        /**
         *
         * @param localizedSiteName
         */
        public void setLocalizedSiteName(String localizedSiteName) {
            this.localizedSiteName = localizedSiteName;
        }
    }

    /**
     *
     */
    public class VOVariableJSF {

        private VariableVO variable;
        private String localizedVariableName;
        private Boolean selected = false;

        /**
         *
         * @param variable
         */
        public VOVariableJSF(VariableVO variable) {
            super();
            this.variable = variable;
            if (variable != null) {
                this.localizedVariableName = getPropertiesVariableNames().getProperty(variable.getAffichage());
                if (Strings.isNullOrEmpty(this.localizedVariableName))
                    this.localizedVariableName = variable.getAffichage();
            }

        }

        /**
         *
         * @return
         */
        public VariableVO getVariable() {
            return variable;
        }

        /**
         *
         * @return
         */
        public Boolean getSelected() {
            return selected;
        }

        /**
         *
         * @param selected
         */
        public void setSelected(Boolean selected) {
            this.selected = selected;
        }

        /**
         *
         * @return
         */
        public String getLocalizedVariableName() {
            return localizedVariableName;
        }

        /**
         *
         * @param localizedVariableName
         */
        public void setLocalizedVariableName(String localizedVariableName) {
            this.localizedVariableName = localizedVariableName;
        }
    }

    /**
     *
     * @return
     */
    @Override
    public String nextStep() {
        super.nextStep();
        switch (getStep()) {
            case 3 :
                try {
                    createOrUpdateListVariablesAvailables();
                } catch (BusinessException e) {
                    variablesAvailables.clear();
                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                break;
            default :
                break;
        }
        return null;
    }

    /**
     *
     * @return
     */
    @Override
    public boolean getIsStepValid() {
        if (getStep() == 1) {
            return getParametersRequest().getPlateformeStepIsValid();
        } else if (getStep() == 2) {
            return getParametersRequest().getDateStepIsValid();
        } else if (getStep() == 3) {
            return getParametersRequest().getVariableStepIsValid();
        }
        return false;
    }

    /**
     *
     * @param projectsAvailables
     */
    public void setProjectsAvailables(Map<Long, ProjectVO> projectsAvailables) {
        this.projectsAvailables = projectsAvailables;
    }

    /**
     *
     * @param variablesAvailables
     */
    public void setVariablesAvailables(Map<Long, VOVariableJSF> variablesAvailables) {
        this.variablesAvailables = variablesAvailables;
    }

    /**
     *
     * @return
     */
    public ParametersRequest getParametersRequest() {
        return parametersRequest;
    }

    /**
     *
     * @param parametersRequest
     */
    public void setParametersRequest(ParametersRequest parametersRequest) {
        this.parametersRequest = parametersRequest;
    }

    /**
     *
     * @return
     */
    public Properties getPropertiesProjectsNames() {
        return propertiesProjectsNames;
    }

    /**
     *
     * @param propertiesProjectsNames
     */
    public void setPropertiesProjectsNames(Properties propertiesProjectsNames) {
        this.propertiesProjectsNames = propertiesProjectsNames;
    }

    /**
     *
     * @return
     */
    public Properties getPropertiesPlatformNames() {
        return propertiesPlatformNames;
    }

    /**
     *
     * @param propertiesPlatformNames
     */
    public void setPropertiesPlatformNames(Properties propertiesPlatformNames) {
        this.propertiesPlatformNames = propertiesPlatformNames;
    }

    /**
     *
     * @return
     */
    public Properties getPropertiesSiteNames() {
        return propertiesSiteNames;
    }

    /**
     *
     * @param propertiesSiteNames
     */
    public void setPropertiesSiteNames(Properties propertiesSiteNames) {
        this.propertiesSiteNames = propertiesSiteNames;
    }

    /**
     *
     * @return
     */
    public Properties getPropertiesVariableNames() {
        return propertiesVariableNames;
    }

    /**
     *
     * @param propertiesVariableNames
     */
    public void setPropertiesVariableNames(Properties propertiesVariableNames) {
        this.propertiesVariableNames = propertiesVariableNames;
    }

    /**
     *
     * @return
     */
    public Long getIdProjectSelected() {
        return idProjectSelected;
    }

    /**
     *
     * @param idProjectSelected
     */
    public void setIdProjectSelected(Long idProjectSelected) {
        this.idProjectSelected = idProjectSelected;
    }

    /**
     *
     * @return
     */
    public Long getIdPlateformeSelected() {
        return idPlateformeSelected;
    }

    /**
     *
     * @param idPlateformeSelected
     */
    public void setIdPlateformeSelected(Long idPlateformeSelected) {
        this.idPlateformeSelected = idPlateformeSelected;
    }

    /**
     *
     * @return
     */
    public Long getIdVariableSelected() {
        return idVariableSelected;
    }

    /**
     *
     * @param idVariableSelected
     */
    public void setIdVariableSelected(Long idVariableSelected) {
        this.idVariableSelected = idVariableSelected;
    }

}
