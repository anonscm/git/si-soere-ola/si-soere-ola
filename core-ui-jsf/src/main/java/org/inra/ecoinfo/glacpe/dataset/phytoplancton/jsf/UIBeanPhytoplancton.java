package org.inra.ecoinfo.glacpe.dataset.phytoplancton.jsf;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.swing.tree.TreeNode;

import org.inra.ecoinfo.dataset.security.ISecurityDatasetManager;
import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.filecomp.IFileCompManager;
import org.inra.ecoinfo.filecomp.ItemFile;
import org.inra.ecoinfo.filecomp.entity.FileComp;
import org.inra.ecoinfo.glacpe.dataset.phytoplancton.IPhytoplanctonDatatypeManager;
import org.inra.ecoinfo.glacpe.dataset.phytoplancton.impl.PhytoplanctonParameters;
import org.inra.ecoinfo.glacpe.extraction.jsf.AbstractUIBeanForSteps;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.ProjetSiteVO;
import org.inra.ecoinfo.glacpe.extraction.vo.VariableVO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.projetsite.ProjetSite;
import org.inra.ecoinfo.glacpe.refdata.taxon.Taxon;
import org.inra.ecoinfo.glacpe.refdata.taxon.TaxonProprieteTaxon;
import org.inra.ecoinfo.glacpe.refdata.taxon.ValeurProprieteTaxon;
import org.inra.ecoinfo.glacpe.refdata.taxonniveau.TaxonNiveau;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.notifications.INotificationsManager;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.refdata.valeurqualitative.ValeurQualitative;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.security.ISecurityContext;
import org.inra.ecoinfo.security.entity.Role;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.richfaces.component.UITree;
import org.richfaces.component.UITreeNode;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.google.common.base.Strings;
import com.google.common.collect.Iterators;
import com.google.common.collect.Lists;
import java.util.Set;

/**
 *
 * @author ptcherniati
 */
@ManagedBean(name = "uiPhytoplancton")
@ViewScoped
public class UIBeanPhytoplancton extends AbstractUIBeanForSteps implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final String DATATYPE = "phytoplancton";
    private static final String THEME = "compartiments_biologiques";
    private static final String PATH_PATTERN = "%s/%s/%s/%s/%s";

    /**
     *
     */
    @ManagedProperty(value = "#{phytoplanctonDatatypeManager}")
    protected IPhytoplanctonDatatypeManager phytoplanctonDatatypeManager;

    /**
     *
     */
    @ManagedProperty(value = "#{notificationsManager}")
    protected INotificationsManager notificationsManager;

    /**
     *
     */
    @ManagedProperty(value = "#{extractionManager}")
    protected IExtractionManager extractionManager;

    /**
     *
     */
    @ManagedProperty(value = "#{transactionManager}")
    protected JpaTransactionManager transactionManager;

    /**
     *
     */
    @ManagedProperty(value = "#{securityContext}")
    protected ISecurityContext securityContext;

    /**
     *
     */
    @ManagedProperty(value = "#{localizationManager}")
    protected ILocalizationManager localizationManager;

    /**
     *
     */
    @ManagedProperty(value = "#{fileCompManager}")
    protected IFileCompManager fileCompManager;

    private Properties propertiesProjectsNames;
    private Properties propertiesPlatformNames;
    private Properties propertiesSiteNames;
    private Properties propertiesVariableNames;
    private Properties propertiesQualitativeNames;
    private Properties propertiesTaxonLevelNames;

    private Map<Long, ProjetVO> projectsAvailables = new HashMap<Long, UIBeanPhytoplancton.ProjetVO>();

    private Map<Long, VOTaxonJSF> taxonsAvailables = new HashMap<Long, VOTaxonJSF>();

    private List<VOTaxonJSF> taxonsVOAvailables = Lists.newArrayList();

    private List<VOTaxonJSF> taxonsLengthAvailables = Lists.newArrayList();

    private Map<String, List<VOTaxonJSF>> preselectionTaxonsAvailables = new HashMap<String, List<UIBeanPhytoplancton.VOTaxonJSF>>();

    private Map<Long, VOVariableJSF> variablesAvailables = new HashMap<Long, VOVariableJSF>();

    private ParametersRequest parametersRequest = new ParametersRequest();

    private Long idPlateformeSelected;

    private Long idProjectSelected;

    private Long idVariableSelected;

    private Long idTaxonVOSelected;

    private UITree treeListTaxon;

    private UITree treePreselectionListTaxons;

    private UITreeNode treeNodePreselection;

    private UITreeNode treeNodePreselectionLeaf;

    private UITreeNode treeNodeTaxon;

    private TreeNodeTaxon treeNodeTaxonSelected;

    private TreeNodeAlgal treeNodeAlgalSelected;

    private VOTaxonJSF taxonVOSelected;

    private String taxonGroupSelected;

    private String taxonSearch = "";

    private Boolean sommation = true;

    private Boolean detail = false;

    private Boolean genre = false;

    private Boolean propriete = false;

    private Boolean choiceLength = false;

    private Boolean choiceBiovolume = false;

    private Boolean aggregated = false;

    private Boolean filter = false;

    private String minLength = "0";

    private String maxLength = "20";

    private String minBiovolume = "0";

    private String maxBiovolume = "10000";

    private String lengthValue;

    private String biovolumeValue;

    private String lengthOrder;

    private String biovolumeOrder;

    private String affichage = null;

    private List<TreeNodeTaxon> rootNodes = new ArrayList<TreeNodeTaxon>();

    private List<TreeNodeAlgal> preselectionRootNodes = new ArrayList<TreeNodeAlgal>();
    private Set<Taxon> taxonsWithData;

    /**
     *
     */
    public UIBeanPhytoplancton() {
    }

    /**
     *
     * @return
     */
    public String navigate() {
        return "phytoplancton";
    }

    /**
     *
     * @return
     */
    public String addAllVariables() {
        Map<Long, VOVariableJSF> variablesSelected = parametersRequest.getVariablesSelected();
        variablesSelected.clear();

        for (VOVariableJSF variable : variablesAvailables.values()) {
            if (variable.getVariable().getIsDataAvailable()) {
                variablesSelected.put(variable.getVariable().getId(), variable);
                variable.setSelected(true);
            }
        }
        return null;
    }

    /**
     *
     * @return @throws BusinessException
     * @throws ParseException
     */
    public String removeAllVariables() throws BusinessException, ParseException {
        createOrUpdateListVariablesAvailables();
        return null;
    }

    /**
     *
     * @return @throws BusinessException
     */
    public String addAllTaxons() throws BusinessException {
        removeAllTaxons();

        sommation = false;
        detail = true;

        for (VOTaxonJSF taxon : taxonsVOAvailables) {
            if (taxon.getTaxon().isLeaf()) {
//            if (taxon.getTaxon().getTaxonNiveau().getCode().equals("genre_espece")) {
                parametersRequest.getTaxonsSelected().put(taxon.getTaxon().getId(), taxon);
                taxon.setSelected(true);

            }
        }
        return null;
    }

    /**
     *
     * @return @throws BusinessException
     */
    public String removeAllTaxons() throws BusinessException {
        parametersRequest.getTaxonsSelected().clear();

        for (VOTaxonJSF taxon : taxonsVOAvailables) {
            taxon.setSelected(false);
        }

        for (TreeNodeAlgal algal : preselectionRootNodes) {
            algal.getAlgalClass().setSelected(false);
        }

        for (TreeNodeTaxon tnt : rootNodes.get(0).getChildren()) {
            enableOrDisableAllLeaves(true, tnt);
        }

        return null;
    }

    /**
     *
     * @return
     */
    public String getUpdatePropriete() {
        return null;
    }

    /**
     *
     * @return @throws BusinessException
     */
    public String selectPlateforme() throws BusinessException {
        ProjetVO projectSelected = projectsAvailables.get(idProjectSelected);
        PlateformeVO plateformeSelected = projectSelected.getPlateformes().get(idPlateformeSelected);
        if (plateformeSelected.getSelected()) {
            parametersRequest.getPlateformesSelected().remove(idPlateformeSelected);
            plateformeSelected.setSelected(false);
        } else {
            parametersRequest.getPlateformesSelected().put(idPlateformeSelected, plateformeSelected);
            plateformeSelected.setSelected(true);
        }

        return null;
    }

    /**
     *
     * @return @throws BusinessException
     */
    public String getProjetSelected() throws BusinessException {
        String localizedProjetSelected = null;
        if (idProjectSelected != null) {
            String nom = projectsAvailables.get(idProjectSelected).getProjet().getNom();
            localizedProjetSelected = getPropertiesProjectsNames().getProperty(nom);
            if (Strings.isNullOrEmpty(localizedProjetSelected)) {
                localizedProjetSelected = nom;
            }
            return localizedProjetSelected;
        }
        return localizedProjetSelected;
    }

    /**
     *
     * @return @throws BusinessException
     */
    public String changeProject() throws BusinessException {
        parametersRequest.getPlateformesSelected().clear();
        for (ProjetVO projectSelected : projectsAvailables.values()) {
            for (PlateformeVO plateformeSelected : projectSelected.getPlateformes().values()) {
                plateformeSelected.setSelected(false);
            }
        }

        return null;
    }

    /**
     *
     * @return
     */
    public String selectVariable() {
        VOVariableJSF variableSelected = variablesAvailables.get(idVariableSelected);

        if (variableSelected.getSelected()) {
            parametersRequest.getVariablesSelected().remove(idVariableSelected);
        } else {
            parametersRequest.getVariablesSelected().put(idVariableSelected, variableSelected);
        }
        variableSelected.setSelected(!variableSelected.getSelected());
        return null;
    }

    /**
     *
     * @return
     */
    public String changeAggregated() {
        aggregated = !aggregated;
        return null;
    }

    /**
     *
     * @return @throws BusinessException
     */
    public String deployNodeTaxon() throws BusinessException {

        if ((treeNodeTaxonSelected.getChildren() == null || treeNodeTaxonSelected.getChildren().isEmpty()) && (treeNodeTaxonSelected.getLeaves() == null || treeNodeTaxonSelected.getLeaves().isEmpty())) {
            enableOrDisableLeaf(treeNodeTaxonSelected.getTaxon().getSelected(), treeNodeTaxonSelected);
        } else {
            if (treeNodeTaxonSelected.getChildren() != null && !treeNodeTaxonSelected.getChildren().isEmpty() && treeNodeTaxonSelected.getTaxon() != null) {
                enableOrDisableAllLeaves(treeNodeTaxonSelected.getTaxon().getSelected(), treeNodeTaxonSelected);
            }

        }

        return null;
    }

    /**
     *
     * @return @throws BusinessException
     */
    public String deployNodeAlgal() throws BusinessException {
        if (!treeNodeAlgalSelected.getExpanded()) {
            treeNodeAlgalSelected.setExpanded(true);
        }
        if (treeNodeAlgalSelected.getChildren() != null && !treeNodeAlgalSelected.getChildren().isEmpty()) {
            for (TreeNodeTaxon TNT : treeNodeAlgalSelected.getChildren()) {
                enableOrDisableLeaf(treeNodeAlgalSelected.getAlgalClass().getSelected(), TNT);
            }
        }
        treeNodeAlgalSelected.getAlgalClass().setSelected(!treeNodeAlgalSelected.getAlgalClass().getSelected());

        return null;
    }

    private void enableOrDisableAllLeaves(boolean selected, TreeNodeTaxon TNTaxon) throws BusinessException {
        if (!TNTaxon.getExpanded()) {
            TNTaxon.setExpanded(true);
        }
        enableOrDisableLeaf(selected, TNTaxon);
        if (detail) {
            if (TNTaxon.getChildren() != null && !TNTaxon.getChildren().isEmpty()) {
                for (TreeNodeTaxon taxon : TNTaxon.getChildren()) {
                    enableOrDisableLeaf(selected, taxon);
                    if ((taxon.getChildren() != null && !taxon.getChildren().isEmpty()) || (taxon.getLeaves() != null && !taxon.getLeaves().isEmpty())) {
                        enableOrDisableAllLeaves(selected, taxon);
                    }
                }
            } else if (TNTaxon.getLeaves() != null && !TNTaxon.getLeaves().isEmpty()) {
                for (TreeNodeTaxon taxon : TNTaxon.getLeaves()) {
                    enableOrDisableLeaf(selected, taxon);
                }
            }
        }
    }

    private void enableOrDisableLeaf(Boolean conditionDisabling, TreeNodeTaxon taxon) throws BusinessException {
        Map<Long, VOTaxonJSF> taxonsSelecteds = parametersRequest.getTaxonsSelected();

        if (conditionDisabling) {
            taxonsSelecteds.remove(taxon.getTaxon().getTaxon().getId());
        } else {
            taxonsSelecteds.put(taxon.getTaxon().getTaxon().getId(), taxon.getTaxon());
        }
        taxon.getTaxon().setSelected(!conditionDisabling);
        // selectSynonyme(conditionDisabling, taxon.getTaxon());
    }

    /**
     *
     * @return @throws BusinessException
     */
    public String selectTaxonVO() throws BusinessException {
        VOTaxonJSF taxonsSelecteds = taxonsAvailables.get(idTaxonVOSelected);

        if (taxonsSelecteds.getSelected()) {
            parametersRequest.getTaxonsSelected().remove(taxonsSelecteds.getTaxon().getId());
        } else {
            parametersRequest.getTaxonsSelected().put(taxonsSelecteds.getTaxon().getId(), taxonsSelecteds);
        }

        if (detail) {
            reccursiveTaxonAdd(taxonsSelecteds.getSelected(), taxonsSelecteds);
        }
        taxonsSelecteds.setSelected(!taxonsSelecteds.getSelected());
        selectSynonyme(taxonsSelecteds.getSelected(), taxonsSelecteds);

        return null;
    }

    /**
     *
     * @param selected
     * @param taxonCurrent
     */
    public void reccursiveTaxonAdd(Boolean selected, VOTaxonJSF taxonCurrent) {
        for (Taxon taxon : taxonCurrent.getTaxon().getTaxonsEnfants()) {
            if (parametersRequest.getTaxonsSelected().keySet().contains(taxon.getId())) {
                if (selected) {
                    parametersRequest.getTaxonsSelected().remove(taxon.getId());
                } else {
                    parametersRequest.getTaxonsSelected().put(taxon.getId(), taxonsAvailables.get(taxon.getId()));
                }
            }
            reccursiveTaxonAdd(selected, taxonsAvailables.get(taxon.getId()));
        }
    }

    /**
     *
     * @param selected
     * @param taxonSelected
     * @throws BusinessException
     */
    public void selectSynonyme(Boolean selected, VOTaxonJSF taxonSelected) throws BusinessException {
        TransactionStatus status = transactionManager.getTransaction(new DefaultTransactionDefinition(TransactionDefinition.PROPAGATION_REQUIRES_NEW));

        List<TaxonProprieteTaxon> taxonProprietesTaxon = phytoplanctonDatatypeManager.retrieveTaxonPropertyByTaxonId(taxonSelected.getTaxon().getId());

        if (taxonProprietesTaxon != null && !taxonProprietesTaxon.isEmpty()) {
            String synonymes = "";
            for (TaxonProprieteTaxon taprota : taxonProprietesTaxon) {
                if (taprota.getProprieteTaxon().getCode().equals("synonyme_ancien") || taprota.getProprieteTaxon().getCode().equals("synonyme_recent")) {
                    synonymes = synonymes.concat(taprota.getValeurProprieteTaxon().getStringValue()).concat(",");
                }
            }
            if (synonymes.length() != 0) {
                for (String synonyme : synonymes.split(",")) {
                    if (synonyme.length() > 0) {
                        Taxon taxon = phytoplanctonDatatypeManager.retrieveTaxonByCode(Utils.createCodeFromString(synonyme));
                        if (taxon != null) {
                            VOTaxonJSF taxonJSF = taxonsAvailables.get(taxon.getId());
                            taxonJSF.setSelected(!selected);
                            if (selected) {
                                parametersRequest.getTaxonsSelected().remove(taxon.getId());
                            } else {
                                parametersRequest.getTaxonsSelected().put(taxon.getId(), taxonJSF);
                            }
                        }
                    }
                }
            }
        }

        transactionManager.commit(status);
    }

    /**
     *
     * @return
     */
    public String updateTaxonList() {
        taxonsVOAvailables.clear();
        for (Long taxonID : taxonsAvailables.keySet()) {
            if (taxonSearch.trim().length() > 0) {
                String taxonVOSearch = Utils.createCodeFromString(taxonSearch);
                if (Utils.createCodeFromString(taxonsAvailables.get(taxonID).getTaxon().getNomLatin()).contains(taxonVOSearch)) {
                    if (genre) {
//                        if (taxonsAvailables.get(taxonID).getTaxon().getTaxonNiveau().getCode().equals("genre_espece")) {
                        if (taxonsAvailables.get(taxonID).getTaxon().isLeaf()) {
                            taxonsVOAvailables.add(taxonsAvailables.get(taxonID));
                        }
                    } else {
                        taxonsVOAvailables.add(taxonsAvailables.get(taxonID));
                    }
                }
            } else {
                if (genre) {
//                    if (taxonsAvailables.get(taxonID).getTaxon().getTaxonNiveau().getCode().equals("genre_espece")) {
                    if (taxonsAvailables.get(taxonID).getTaxon().isLeaf()) {
                        taxonsVOAvailables.add(taxonsAvailables.get(taxonID));
                    }
                } else {
                    taxonsVOAvailables.add(taxonsAvailables.get(taxonID));
                }
            }
        }
        return null;
    }

    /**
     *
     * @return @throws BusinessException
     */
    public String updateLengthList() throws BusinessException {

        if ("min".equals(lengthOrder) && lengthValue.length() > 0) {
            minLength = lengthValue;
        } else if ("max".equals(lengthOrder) && lengthValue.length() > 0) {
            maxLength = lengthValue;
        }

        List<VOTaxonJSF> listTaxon = new LinkedList<VOTaxonJSF>();
        Boolean specialCase = false;
        if (choiceBiovolume && !filter) {
            specialCase = true;
        }

        if (choiceBiovolume && filter) {
            listTaxon.addAll(taxonsLengthAvailables);
        } else {
            listTaxon.addAll(taxonsAvailables.values());
        }

        for (VOTaxonJSF taxon : listTaxon) {
            if (taxon.getCellLength() != null) {
                if (taxon.getCellLength() > Float.parseFloat(minLength) && taxon.getCellLength() < Float.parseFloat(maxLength)) {
                    if (!taxonsLengthAvailables.contains(taxon)) {
//                        if (taxon.getTaxon().getTaxonNiveau().getCode().equals("genre_espece")) {
                        if (taxon.getTaxon().isLeaf()) {
                            taxonsLengthAvailables.add(taxon);
                        }
                    }
                } else {
                    if (taxonsLengthAvailables.contains(taxon) && !specialCase) {
                        taxonsLengthAvailables.remove(taxon);
                    }
                }
            }
        }
        return null;
    }

    /**
     *
     * @return @throws BusinessException
     */
    public String updateBiovolumeList() throws BusinessException {

        if ("min".equals(biovolumeOrder) && biovolumeValue.length() > 0) {
            minBiovolume = biovolumeValue;
        } else if ("max".equals(biovolumeOrder) && biovolumeValue.length() > 0) {
            maxBiovolume = biovolumeValue;
        }

        Boolean specialCase = false;
        if (choiceLength && !filter) {
            specialCase = true;
        }

        List<VOTaxonJSF> listTaxon = new LinkedList<VOTaxonJSF>();
        if (choiceLength && filter) {
            listTaxon.addAll(taxonsLengthAvailables);
        } else {
            listTaxon.addAll(taxonsAvailables.values());
        }

        for (VOTaxonJSF taxon : listTaxon) {
            if (taxon.getBiovolume() != null) {
                if (taxon.getBiovolume() > Float.parseFloat(minBiovolume) && taxon.getBiovolume() < Float.parseFloat(maxBiovolume)) {
                    if (!taxonsLengthAvailables.contains(taxon)) {
//                        if (taxon.getTaxon().getTaxonNiveau().getCode().equals("genre_espece")) {
                        if (taxon.getTaxon().isLeaf()) {
                            taxonsLengthAvailables.add(taxon);
                        }
                    }
                } else {
                    if (taxonsLengthAvailables.contains(taxon) && !specialCase) {
                        taxonsLengthAvailables.remove(taxon);
                    }
                }
            }
        }
        return null;
    }

    /**
     *
     * @return @throws BusinessException
     */
    public String updateList() throws BusinessException {
        updateLengthList();
        updateBiovolumeList();
        return null;
    }

    /**
     *
     * @return @throws BusinessException
     */
    public String changeFilter() throws BusinessException {
        updateLengthList();
        updateBiovolumeList();
        return null;
    }

    /**
     *
     * @return @throws BusinessException
     */
    public String addListTaxon() throws BusinessException {
        for (VOTaxonJSF taxonJSF : taxonsLengthAvailables) {
            idTaxonVOSelected = taxonJSF.getTaxon().getId();
            selectTaxonVO();
        }
        return null;
    }

    /**
     *
     * @return @throws BusinessException
     */
    public String addFreeTaxon() throws BusinessException {
        for (VOTaxonJSF taxonJSF : taxonsVOAvailables) {
            idTaxonVOSelected = taxonJSF.getTaxon().getId();
            selectTaxonVO();
        }
        return null;
    }

    /**
     *
     * @return @throws BusinessException
     * @throws ParseException
     */
    public List<VOVariableJSF> getVariablesAvailables() throws BusinessException, ParseException {
        if (variablesAvailables.isEmpty() && parametersRequest.getDateStepIsValid()) {
            createOrUpdateListVariablesAvailables();
        }
        return new LinkedList<VOVariableJSF>(variablesAvailables.values());

    }

    private void createOrUpdateListVariablesAvailables() throws BusinessException, ParseException {
        variablesAvailables.clear();
        parametersRequest.getVariablesSelected().clear();

        if (!parametersRequest.plateformesSelected.isEmpty()) {

            List<VariableVO> variables = phytoplanctonDatatypeManager.retrieveListsVariables(new LinkedList<Long>(parametersRequest.plateformesSelected.keySet()), parametersRequest.getFirstStartDate(), parametersRequest.getLastEndDate());
            for (PlateformeVO plateforme : parametersRequest.plateformesSelected.values()) {
                for (VariableVO variable : variables) {
                    if (securityContext.isRoot()
                            || securityContext.matchPrivilege(String.format("*/%s/*/phytoplancton/%s", plateforme.getPlateforme().getSite().getCode(), variable.getCode()), Role.ROLE_EXTRACTION, ISecurityDatasetManager.PRIVILEGE_TYPE_DATASET)) {
                        if (!variablesAvailables.keySet().contains(variable.getId())) {
                            VOVariableJSF variableVO = new VOVariableJSF(variable);
                            variablesAvailables.put(variable.getId(), variableVO);
                        }
                    }
                }
            }
        }
    }

    /**
     *
     * @return @throws BusinessException
     */
    public Map<Long, ProjetVO> getProjectsAvailables() throws BusinessException {
        TransactionStatus status = transactionManager.getTransaction(new DefaultTransactionDefinition(TransactionDefinition.PROPAGATION_REQUIRES_NEW));
        if (projectsAvailables.isEmpty()) {

            List<Projet> projets = phytoplanctonDatatypeManager.retrievePhytoplanctonAvailablesProjets();
            for (Projet projet : projets) {
                List<Plateforme> plateformes = Lists.newArrayList();
                for (ProjetSite projetSite : projet.getProjetsSites()) {
                    for (Plateforme plateforme : phytoplanctonDatatypeManager.retrievePhytoplanctonAvailablesPlateformesByProjetSiteId(projetSite.getId())) {
                        if (!plateformes.contains(plateforme)
                                && (securityContext.isRoot() || securityContext.matchPrivilege(String.format("%s/%s/*/phytoplancton/*", projetSite.getProjet().getCode(), projetSite.getSite().getCode()), Role.ROLE_EXTRACTION,
                                ISecurityDatasetManager.PRIVILEGE_TYPE_DATASET))) {
                            plateformes.add(plateforme);
                        }
                    }
                }
                if (!plateformes.isEmpty()) {
                    ProjetVO projectVO = new ProjetVO(projet, plateformes);
                    projectsAvailables.put(projet.getId(), projectVO);
                }
            }

        }

        transactionManager.commit(status);
        return projectsAvailables;
    }

    /**
     *
     * @return @throws BusinessException
     */
    public List<String> getPreselectionTaxonsAvailables() throws BusinessException {
        return new LinkedList<String>(preselectionTaxonsAvailables.keySet());
    }

    /**
     *
     * @throws BusinessException
     */
    public void filterLists() throws BusinessException {
        taxonsVOAvailables.clear();

        for (VOTaxonJSF taxon : taxonsAvailables.values()) {
            if (genre) {
//                if (taxon.getTaxon().getTaxonNiveau().getCode().equals("genre_espece")) {
                if (taxon.getTaxon().isLeaf()) {
                    taxonsVOAvailables.add(taxon);
                }
            } else {
                taxonsVOAvailables.add(taxon);
            }
        }
        updateTaxonList();
        updateLengthList();
        updateBiovolumeList();
    }

    /**
     *
     * @param taxonGroupSelected
     */
    public void setTaxonGroupSelected(String taxonGroupSelected) {
        this.taxonGroupSelected = taxonGroupSelected;
    }

    /**
     *
     * @return @throws BusinessException
     */
    public List<VOTaxonJSF> getTaxonsAvailables() throws BusinessException {
        return taxonsVOAvailables;
    }

    /**
     *
     * @return @throws BusinessException
     */
    public List<TreeNodeTaxon> getRootNodes() throws BusinessException {
        return rootNodes;
    }

    /**
     *
     * @return
     */
    public List<TreeNodeAlgal> getPreselectionRootNodes() {
        return preselectionRootNodes;
    }

    /**
     *
     * @throws BusinessException
     */
    public void initTaxons() throws BusinessException {
        if (taxonsAvailables.isEmpty()) {
            Map<Taxon, Map<String, ValeurProprieteTaxon>> taxons = phytoplanctonDatatypeManager.retrievePhytoplanctonAvailablesTaxons();
            for (Taxon taxon : taxons.keySet()) {
                VOTaxonJSF taxonVO = new VOTaxonJSF(taxon);
//                if (taxon.getTaxonNiveau().getCode().equals("genre_espece")) {
                if (taxon.isLeaf()) {
                    taxonVO.setCellLength(taxons.get(taxon).get("longueur_de_la_cellule") == null ? null : taxons.get(taxon).get("longueur_de_la_cellule").getFloatValue());
                    taxonVO.setBiovolume(taxons.get(taxon).get("valeur_du_biovolume_specifique_choisi_pour_le_taxon") == null ? null : taxons.get(taxon).get("valeur_du_biovolume_specifique_choisi_pour_le_taxon").getFloatValue());
                    taxonVO.setAlgalClass(taxons.get(taxon).get("classe_algale_sensu_bourrelly") == null ? null : taxons.get(taxon).get("classe_algale_sensu_bourrelly").getQualitativeValue().getValeur());
                }
                taxonsVOAvailables.add(taxonVO);
                taxonsAvailables.put(taxon.getId(), taxonVO);
                if (taxon.isLeaf()) {
                    taxonsLengthAvailables.add(taxonVO);
                }
            }
        }
        taxonsWithData = phytoplanctonDatatypeManager.retrievePhytoplanctonAvailablesTaxons().keySet();

        if (preselectionTaxonsAvailables.isEmpty()) {
            for (VOTaxonJSF taxonVO : taxonsAvailables.values()) {
                if (taxonVO.getAlgalClass() != null) {
                    if (preselectionTaxonsAvailables.get(taxonVO.getAlgalClass()) != null) {
                        preselectionTaxonsAvailables.get(taxonVO.getAlgalClass()).add(taxonVO);
                    } else {
                        List<VOTaxonJSF> taxonList = Lists.newArrayList();
                        taxonList.add(taxonVO);
                        preselectionTaxonsAvailables.put(taxonVO.getAlgalClass(), taxonList);
                    }
                }
            }
        }

        if (rootNodes == null || rootNodes.isEmpty()) {
            List<Taxon> rootTaxon = phytoplanctonDatatypeManager.retrievePhytoplanctonRootTaxon();
            List<TreeNodeTaxon> rootChildNode = Lists.newLinkedList();
            List<TreeNodeTaxon> rootLeavesNode = Lists.newLinkedList();
            for (Taxon taxon : rootTaxon) {
                TreeNodeTaxon treeNodeRoot = new TreeNodeTaxon(new VOTaxonJSF(taxon));
                if (taxon.getTaxonsEnfants().isEmpty()) {
                    rootLeavesNode.add(treeNodeRoot);
                } else {
                    rootChildNode.add(treeNodeRoot);
                }
            }
            TreeNodeTaxon rootNode = new TreeNodeTaxon(rootChildNode, rootLeavesNode);
            rootNodes = Lists.newArrayList(rootNode);
        }

        if (preselectionRootNodes.isEmpty()) {
            List<String> preselectionTaxonNames = getPreselectionTaxonsAvailables();
            for (String preselectionTaxonName : preselectionTaxonNames) {
                preselectionRootNodes.add(new TreeNodeAlgal(new VOAlgalJSF(preselectionTaxonName), preselectionTaxonsAvailables.get(preselectionTaxonName)));
            }
        }
    }

    private List<String> getPathes(List<ProjetSite> projetSites, List<VariableVO> variables) {
        String sitePath, projet, theme, path;
        theme = THEME;
        List<String> pathes = new LinkedList<>();
        for (ProjetSite projetSite : projetSites) {
            sitePath = projetSite.getSite().getPath();
            projet = projetSite.getProjet().getCode();
            for (VariableVO variable : variables) {
                path = String.format(PATH_PATTERN, projet, sitePath, theme, DATATYPE, variable.getCode());
                if (!pathes.contains(path)) {
                    pathes.add(path);
                }
            }
        }
        return pathes;
    }

    /**
     *
     * @return @throws BusinessException
     */
    public String extract() throws BusinessException {
        Map<String, Object> metadatasMap = new HashMap<String, Object>();
        List<org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO> metadatasPlateformesVos = new LinkedList<org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO>();
        List<ProjetSite> projetSites = new LinkedList<ProjetSite>();
        for (PlateformeVO plateforme : parametersRequest.getPlateformesSelected().values()) {
            org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO plateformeVO = new org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO(plateforme.getPlateforme());
            ProjetSite projetSite = phytoplanctonDatatypeManager.retrievePhytoplanctonAvailablesProjetsSiteByProjetAndSite(projectsAvailables.get(idProjectSelected).getProjet().getId(), plateforme.getPlateforme().getSite().getId());
            ProjetSiteVO projetSiteVO = new ProjetSiteVO(projetSite);
            if (!projetSites.contains(projetSite)) {
                projetSites.add(projetSite);
            }

            plateformeVO.setProjet(projetSiteVO);
            metadatasPlateformesVos.add(plateformeVO);
        }

        metadatasMap.put(PlateformeVO.class.getSimpleName(), metadatasPlateformesVos);
        List<VariableVO> variablesVO = new LinkedList<VariableVO>();
        for (VOVariableJSF variable : parametersRequest.getListVariablesSelected()) {
            variablesVO.add(variable.getVariable());
        }
        metadatasMap.put(VariableVO.class.getSimpleName(), variablesVO);

        List<FileComp> listFiles = new LinkedList<FileComp>();
        try {
            if (!parametersRequest.plateformesSelected.isEmpty() && parametersRequest.getFormIsValid()) {
                IntervalDate intervalDate;

                intervalDate = new IntervalDate(parametersRequest.getFirstStartDate(), parametersRequest.getLastEndDate(), DateUtil.getSimpleDateFormatDateLocale());

                List<IntervalDate> intervalsDate = new LinkedList<>();
                intervalsDate.add(intervalDate);
                final List<ItemFile> files = fileCompManager.getFileCompFromPathesAndIntervalsDate(getPathes(projetSites, variablesVO), intervalsDate);
                for (final ItemFile file : files) {
                    if (file.getMandatory()) {
                        listFiles.add(file.getFileComp());
                    }
                }
            }
        } catch (BadExpectedValueException e) {
            throw new BusinessException("bad file", e);
        } catch (ParseException e) {
            throw new BusinessException("bad file name", e);
        }
        metadatasMap.put(FileComp.class.getSimpleName(), listFiles);

        List<Taxon> taxons = new LinkedList<Taxon>();
        for (VOTaxonJSF taxon : parametersRequest.getListTaxonsSelected()) {
            taxons.add(taxon.getTaxon());
        }
        metadatasMap.put(Taxon.class.getSimpleName(), taxons);

        parametersRequest.getDatesRequestParam().getDatesYearsDiscretsFormParam().setPeriods(parametersRequest.getDatesRequestParam().getNonPeriodsYearsContinuous());
        parametersRequest.getDatesRequestParam().getDatesYearsRangeFormParam().setPeriods(parametersRequest.getDatesRequestParam().getNonPeriodsYearsContinuous());

        Boolean somme = new Boolean(sommation);
        Boolean prop = new Boolean(propriete);
        Boolean agg = new Boolean(aggregated);

        metadatasMap.put("sommation", somme);
        metadatasMap.put("propriete", prop);
        metadatasMap.put("aggregated", agg);
        metadatasMap.put(DatesRequestParamVO.class.getSimpleName(), parametersRequest.getDatesRequestParam().clone());
        metadatasMap.put(IExtractionManager.KEYMAP_COMMENTS, new String(parametersRequest.getCommentExtraction()));
        IParameter parameters = new PhytoplanctonParameters(metadatasMap);
        Integer aff = Integer.parseInt(getAffichage());
        extractionManager.extract(parameters, aff);

        return null;
    }

    /**
     *
     */
    public class PlateformeVO {

        private Plateforme plateforme;
        private String localizedPlateformeName;
        private String localizedSiteName;
        private Boolean selected = false;

        /**
         *
         * @param plateforme
         */
        public PlateformeVO(Plateforme plateforme) {
            super();
            this.plateforme = plateforme;
            if (plateforme != null) {
                this.localizedPlateformeName = getPropertiesPlatformNames().getProperty(plateforme.getNom());
                if (Strings.isNullOrEmpty(this.localizedPlateformeName)) {
                    this.localizedPlateformeName = plateforme.getNom();
                }
                this.localizedSiteName = getPropertiesSiteNames().getProperty(plateforme.getSite().getNom());
                if (Strings.isNullOrEmpty(this.localizedSiteName)) {
                    this.localizedSiteName = plateforme.getSite().getNom();
                }
            }
        }

        /**
         *
         * @return
         */
        public Plateforme getPlateforme() {
            return plateforme;
        }

        /**
         *
         * @return
         */
        public Boolean getSelected() {
            return selected;
        }

        /**
         *
         * @param selected
         */
        public void setSelected(Boolean selected) {
            this.selected = selected;
        }

        /**
         *
         * @return
         */
        public String getLocalizedPlateformeName() {
            return localizedPlateformeName;
        }

        /**
         *
         * @param localizedPlateformeName
         */
        public void setLocalizedPlateformeName(String localizedPlateformeName) {
            this.localizedPlateformeName = localizedPlateformeName;
        }

        /**
         *
         * @return
         */
        public String getLocalizedSiteName() {
            return localizedSiteName;
        }

        /**
         *
         * @param localizedSiteName
         */
        public void setLocalizedSiteName(String localizedSiteName) {
            this.localizedSiteName = localizedSiteName;
        }
    }

    /**
     *
     */
    public class ProjetVO {

        private Projet projet;
        private String localizedProjetName;
        private Map<Long, PlateformeVO> plateformes = new HashMap<Long, PlateformeVO>();

        /**
         *
         * @param projet
         * @param plateformes
         */
        public ProjetVO(Projet projet, List<Plateforme> plateformes) {
            super();
            this.projet = projet;
            if (projet != null) {
                this.localizedProjetName = getPropertiesProjectsNames().getProperty(projet.getNom());
                if (Strings.isNullOrEmpty(this.localizedProjetName)) {
                    this.localizedProjetName = projet.getNom();
                }
            }
            for (Plateforme plateforme : plateformes) {
                this.plateformes.put(plateforme.getId(), new PlateformeVO(plateforme));
            }

        }

        /**
         *
         * @return
         */
        public Projet getProjet() {
            return projet;
        }

        /**
         *
         * @return
         */
        public Map<Long, PlateformeVO> getPlateformes() {
            return plateformes;
        }

        /**
         *
         * @return
         */
        public String getLocalizedProjetName() {
            return localizedProjetName;
        }

        /**
         *
         * @param localizedProjetName
         */
        public void setLocalizedProjetName(String localizedProjetName) {
            this.localizedProjetName = localizedProjetName;
        }
    }

    /**
     *
     */
    public class VOTaxonJSF {

        private Taxon taxon;
        private Float cellLength;
        private Float biovolume;
        private String algalClass;
        private String localizedName;
        private String localizedLevelName;
        private Boolean selected;

        /**
         *
         * @param taxon
         */
        public VOTaxonJSF(Taxon taxon) {
            super();
            this.taxon = taxon;
            this.selected = false;
            if (taxon != null) {
                this.localizedName = taxon.getNomLatin();
                this.localizedLevelName = getPropertiesTaxonLevelNames().getProperty(taxon.getTaxonNiveau().getNom());
                if (Strings.isNullOrEmpty(this.localizedLevelName)) {
                    this.localizedLevelName = taxon.getTaxonNiveau().getNom();
                }
            }
        }

        /**
         *
         * @return
         */
        public Taxon getTaxon() {
            return taxon;
        }

        /**
         *
         * @return
         */
        public Boolean getSelected() {
            return selected;
        }

        /**
         *
         * @param selected
         */
        public void setSelected(Boolean selected) {
            this.selected = selected;
        }

        /**
         *
         * @return
         */
        public Float getCellLength() {
            return cellLength;
        }

        /**
         *
         * @param cellLength
         */
        public void setCellLength(Float cellLength) {
            this.cellLength = cellLength;
        }

        /**
         *
         * @return
         */
        public Float getBiovolume() {
            return biovolume;
        }

        /**
         *
         * @param biovolume
         */
        public void setBiovolume(Float biovolume) {
            this.biovolume = biovolume;
        }

        /**
         *
         * @return
         */
        public String getAlgalClass() {
            return algalClass;
        }

        /**
         *
         * @param algalClass
         */
        public void setAlgalClass(String algalClass) {
            this.algalClass = algalClass;
        }

        /**
         *
         * @return
         */
        public String getLocalizedName() {
            return localizedName;
        }

        /**
         *
         * @param localizedName
         */
        public void setLocalizedName(String localizedName) {
            this.localizedName = localizedName;
        }

        /**
         *
         * @return
         */
        public String getLocalizedLevelName() {
            return localizedLevelName;
        }

        /**
         *
         * @param localizedLevelName
         */
        public void setLocalizedLevelName(String localizedLevelName) {
            this.localizedLevelName = localizedLevelName;
        }
    }

    /**
     *
     */
    public class VOAlgalJSF {

        private String nom;
        private Boolean selected = false;

        /**
         *
         * @param nom
         */
        public VOAlgalJSF(String nom) {
            super();
            this.nom = nom;
        }

        /**
         *
         * @return
         */
        public Boolean getSelected() {
            return selected;
        }

        /**
         *
         * @param selected
         */
        public void setSelected(Boolean selected) {
            this.selected = selected;
        }

        /**
         *
         * @return
         */
        public String getNom() {
            return nom;
        }

        /**
         *
         * @param nom
         */
        public void setNom(String nom) {
            this.nom = nom;
        }
    }

    /**
     *
     */
    public class VOVariableJSF {

        private VariableVO variable;
        private String localizedVariableName;
        private Boolean selected = false;

        /**
         *
         * @param variable
         */
        public VOVariableJSF(VariableVO variable) {
            super();
            this.variable = variable;
            if (variable != null) {
                this.localizedVariableName = getPropertiesVariableNames().getProperty(variable.getNom());
                if (Strings.isNullOrEmpty(this.localizedVariableName)) {
                    this.localizedVariableName = variable.getNom();
                }
            }
        }

        /**
         *
         * @return
         */
        public VariableVO getVariable() {
            return variable;
        }

        /**
         *
         * @return
         */
        public Boolean getSelected() {
            return selected;
        }

        /**
         *
         * @param selected
         */
        public void setSelected(Boolean selected) {
            this.selected = selected;
        }

        /**
         *
         * @return
         */
        public String getLocalizedVariableName() {
            return localizedVariableName;
        }

        /**
         *
         * @param localizedVariableName
         */
        public void setLocalizedVariableName(String localizedVariableName) {
            this.localizedVariableName = localizedVariableName;
        }
    }

    /**
     *
     */
    public class TreeNodeAlgal implements TreeNode {

        private VOAlgalJSF algalClass;
        private List<TreeNodeAlgal> algalChildren = Lists.newArrayList();
        private List<TreeNodeTaxon> children;
        private Boolean expanded = false;

        /**
         *
         * @param algal
         * @param preselectionTaxons
         */
        public TreeNodeAlgal(VOAlgalJSF algal, List<VOTaxonJSF> preselectionTaxons) {
            super();
            this.algalClass = algal;
            this.children = Lists.newArrayList();
            for (VOTaxonJSF taxon : preselectionTaxons) {
                this.children.add(new TreeNodeTaxon(taxon));
            }
        }

        @Override
        public int getChildCount() {
            return algalChildren.size();
        }

        @Override
        public TreeNode getChildAt(int childIndex) {
            if (algalChildren != null) {
                return algalChildren.get(childIndex);
            }
            return null;
        }

        @Override
        public TreeNode getParent() {
            return null;
        }

        @Override
        public int getIndex(TreeNode node) {
            return algalChildren.indexOf(node);
        }

        @Override
        public boolean getAllowsChildren() {
            return false;
        }

        @Override
        public boolean isLeaf() {
            return false;
        }

        @SuppressWarnings("rawtypes")
        @Override
        public Enumeration children() {
            if (isLeaf()) {
                return new Enumeration<TreeNode>() {

                    public boolean hasMoreElements() {
                        return false;
                    }

                    public TreeNode nextElement() {
                        return null;
                    }
                };
            } else {
                return Iterators.asEnumeration(algalChildren.iterator());
            }
        }

        /**
         *
         * @param algalChildren
         */
        public void setAlgalChildren(List<TreeNodeAlgal> algalChildren) {
            this.algalChildren = algalChildren;
        }

        /**
         *
         * @return
         */
        public List<TreeNodeAlgal> getAlgalChildren() {
            return algalChildren;
        }

        /**
         *
         * @return
         */
        public VOAlgalJSF getAlgalClass() {
            return algalClass;
        }

        /**
         *
         * @param algalClass
         */
        public void setAlgalClass(VOAlgalJSF algalClass) {
            this.algalClass = algalClass;
        }

        /**
         *
         * @return
         */
        public List<TreeNodeTaxon> getChildren() {
            return children;
        }

        /**
         *
         * @param children
         */
        public void setChildren(List<TreeNodeTaxon> children) {
            this.children = children;
        }

        /**
         *
         * @return
         */
        public Boolean getExpanded() {
            return expanded;
        }

        /**
         *
         * @param expanded
         */
        public void setExpanded(Boolean expanded) {
            this.expanded = expanded;
        }
    }

    /**
     *
     */
    public class TreeNodeTaxon implements TreeNode {

        private VOTaxonJSF taxon;
        private List<TreeNodeTaxon> children;
        private List<TreeNodeTaxon> leaves;
        private Boolean expanded = false;

        /**
         *
         * @param taxon
         */
        public TreeNodeTaxon(VOTaxonJSF taxon) {
            super();
            this.taxon = taxon;
        }

        /**
         *
         * @param children
         * @param leaves
         */
        public TreeNodeTaxon(List<TreeNodeTaxon> children, List<TreeNodeTaxon> leaves) {
            super();
            this.taxon = null;
            this.children = Lists.newArrayList();
            for (TreeNodeTaxon child : children) {
                this.children.add(child);
            }
            this.leaves = Lists.newArrayList();
            for (TreeNodeTaxon leaf : leaves) {
                this.leaves.add(leaf);
            }
        }

        /**
         *
         * @return
         */
        public Boolean getExpanded() {
            return expanded;
        }

        /**
         *
         * @param expanded
         */
        public void setExpanded(Boolean expanded) {
            this.expanded = expanded;
        }

        /**
         *
         * @return
         */
        public VOTaxonJSF getTaxon() {
            return taxon;
        }

        /**
         *
         * @return
         */
        public List<TreeNodeTaxon> getChildren() {
            if (children == null || children.isEmpty()) {
                children = null;
                if (!taxon.getTaxon().getTaxonsEnfants().isEmpty() && (leaves == null || leaves.isEmpty())) {
                    children = Lists.newArrayList();
                    leaves = getLeaves();
                    for (Taxon taxonEnfant : taxon.getTaxon().getTaxonsEnfants()) {
                        if (taxonEnfant.getTaxonsEnfants().isEmpty()) {
                            leaves.add(new TreeNodeTaxon(new VOTaxonJSF(taxonEnfant)));
                        } else {
                            children.add(new TreeNodeTaxon(new VOTaxonJSF(taxonEnfant)));
                        }
                    }
                }
            }
            return children;
        }

        @Override
        public TreeNode getChildAt(int childIndex) {
            if (children != null) {
                return children.get(childIndex);
            }
            return null;
        }

        @Override
        public int getChildCount() {
            return children.size();
        }

        @Override
        public TreeNode getParent() {
            return null;
        }

        @Override
        public int getIndex(TreeNode node) {
            return children.indexOf(node);
        }

        @Override
        public boolean getAllowsChildren() {
            return false;
        }

        @Override
        public boolean isLeaf() {
            if (children == null || children.isEmpty()) {
                return true;
            }
            return false;
        }

        @SuppressWarnings("rawtypes")
        @Override
        public Enumeration children() {
            if (isLeaf()) {
                return new Enumeration<TreeNode>() {

                    public boolean hasMoreElements() {
                        return false;
                    }

                    public TreeNode nextElement() {
                        return null;
                    }
                };
            } else {
                return Iterators.asEnumeration(children.iterator());
            }
        }

        /**
         *
         * @return
         */
        public List<TreeNodeTaxon> getLeaves() {
            if (leaves == null) {
                leaves = Lists.newArrayList();
            }
            return leaves;
        }

        /**
         *
         * @param leaves
         */
        public void setLeaves(List<TreeNodeTaxon> leaves) {
            this.leaves = leaves;
        }

    }

    /**
     *
     */
    public class ParametersRequest {

        private Map<Long, PlateformeVO> plateformesSelected = new HashMap<Long, PlateformeVO>();
        private Map<Long, VOVariableJSF> variablesSelected = new HashMap<Long, VOVariableJSF>();
        private Map<Long, VOTaxonJSF> taxonsSelected = new HashMap<Long, VOTaxonJSF>();

        private DatesRequestParamVO datesRequestParam;
        private String commentExtraction;

        /**
         *
         * @return
         */
        public Boolean getPlateformeStepIsValid() {
            return !plateformesSelected.isEmpty();
        }

        /**
         *
         * @return
         */
        public Boolean getVariableStepIsValid() {
            return (!variablesSelected.isEmpty() || aggregated) && getDateStepIsValid();
        }

        /**
         *
         * @return
         */
        public Boolean getTaxonStepIsValid() {
            if (getVariableStepIsValid() && !taxonsSelected.isEmpty()) {
                return true;
            }
            if (aggregated && variablesSelected.isEmpty() && getVariableStepIsValid()) {
                return true;
            }

            return false;
        }

        /**
         *
         * @return
         */
        public Boolean getDateStepIsValid() {
            if ((datesRequestParam.getSelectedFormSelection() != null) && getPlateformeStepIsValid() && datesRequestParam.getCurrentDatesFormParam().getIsValid()) {
                return true;
            } else {
                return false;
            }
        }

        /**
         *
         * @return @throws ParseException
         */
        public Date getFirstStartDate() throws ParseException {
            if (!datesRequestParam.getCurrentDatesFormParam().getPeriods().isEmpty()) {
                Date firstDate = null;
                for (Date mapDate : datesRequestParam.getCurrentDatesFormParam().retrieveParametersMap().values()) {
                    if (firstDate == null) {
                        firstDate = mapDate;
                    } else if (firstDate.after(mapDate)) {
                        firstDate = mapDate;
                    }
                }
                return firstDate;
            }
            return null;
        }

        /**
         *
         * @return @throws ParseException
         */
        public Date getLastEndDate() throws ParseException {
            if (!datesRequestParam.getCurrentDatesFormParam().getPeriods().isEmpty()) {
                Date lastDate = null;
                for (Date mapDate : datesRequestParam.getCurrentDatesFormParam().retrieveParametersMap().values()) {
                    if (lastDate == null) {
                        lastDate = mapDate;
                    } else if (lastDate.before(mapDate)) {
                        lastDate = mapDate;
                    }
                }
                return lastDate;
            }
            return null;
        }

        /**
         *
         * @return
         */
        public DatesRequestParamVO getDatesRequestParam() {
            if (datesRequestParam == null) {
                initDatesRequestParam();
            }
            return datesRequestParam;
        }

        private void initDatesRequestParam() {
            datesRequestParam = new DatesRequestParamVO(localizationManager);
        }

        /**
         *
         * @return
         */
        public Map<Long, PlateformeVO> getPlateformesSelected() {
            return plateformesSelected;
        }

        /**
         *
         * @return
         */
        public List<PlateformeVO> getListPlateformesSelected() {
            return new LinkedList<PlateformeVO>(plateformesSelected.values());
        }

        /**
         *
         * @return
         */
        public Map<Long, VOVariableJSF> getVariablesSelected() {
            return variablesSelected;
        }

        /**
         *
         * @return
         */
        public List<VOVariableJSF> getListVariablesSelected() {
            return new LinkedList<VOVariableJSF>(variablesSelected.values());
        }

        /**
         *
         * @return
         */
        public String getCommentExtraction() {
            return commentExtraction;
        }

        /**
         *
         * @param commentExtraction
         */
        public void setCommentExtraction(String commentExtraction) {
            this.commentExtraction = commentExtraction;
        }

        /**
         *
         * @return
         */
        public Map<Long, VOTaxonJSF> getTaxonsSelected() {
            return taxonsSelected;
        }

        /**
         *
         * @return
         */
        public List<VOTaxonJSF> getListTaxonsSelected() {
            return new LinkedList<VOTaxonJSF>(taxonsSelected.values());
        }

        /**
         *
         * @param taxonsSelected
         */
        public void setTaxonsSelected(Map<Long, VOTaxonJSF> taxonsSelected) {
            this.taxonsSelected = taxonsSelected;
        }

        /**
         *
         * @return
         */
        public boolean getFormIsValid() {
            return getTaxonStepIsValid() && affichage != null;
        }

    }

    // GETTERS SETTERS - BEANS
    /**
     *
     * @param notificationsManager
     */
    public void setNotificationsManager(INotificationsManager notificationsManager) {
        this.notificationsManager = notificationsManager;
    }

    /**
     *
     * @param phytoplanctonDatatypeManager
     */
    public void setPhytoplanctonDatatypeManager(IPhytoplanctonDatatypeManager phytoplanctonDatatypeManager) {
        this.phytoplanctonDatatypeManager = phytoplanctonDatatypeManager;
    }

    /**
     *
     * @param idPlateformeSelected
     */
    public void setIdPlateformeSelected(Long idPlateformeSelected) {
        this.idPlateformeSelected = idPlateformeSelected;
    }

    /**
     *
     * @param idProjectSelected
     */
    public void setIdProjectSelected(Long idProjectSelected) {
        this.idProjectSelected = idProjectSelected;
    }

    /**
     *
     * @return
     */
    public ParametersRequest getParametersRequest() {
        return parametersRequest;
    }

    /**
     *
     * @param idVariableSelected
     */
    public void setIdVariableSelected(Long idVariableSelected) {
        this.idVariableSelected = idVariableSelected;
    }

    /**
     *
     * @param extractionManager
     */
    public void setExtractionManager(IExtractionManager extractionManager) {
        this.extractionManager = extractionManager;
    }

    /**
     *
     * @return
     */
    public Long getIdProjectSelected() {
        return idProjectSelected;
    }

    /**
     *
     * @param treeListTaxon
     */
    public void setTreeListTaxon(UITree treeListTaxon) {
        this.treeListTaxon = treeListTaxon;
    }

    /**
     *
     * @return
     */
    public UITree getTreeListTaxon() {
        return treeListTaxon;
    }

    /**
     *
     * @param rootNodes
     */
    public void setRootNodes(List<TreeNodeTaxon> rootNodes) {
        this.rootNodes = rootNodes;
    }

    /**
     *
     * @return
     */
    public UITreeNode getTreeNodeTaxon() {
        return treeNodeTaxon;
    }

    /**
     *
     * @param treeNodeTaxon
     */
    public void setTreeNodeTaxon(UITreeNode treeNodeTaxon) {
        this.treeNodeTaxon = treeNodeTaxon;
    }

    /**
     *
     * @return
     */
    public VOTaxonJSF getTaxonVOSelected() {
        return taxonVOSelected;
    }

    /**
     *
     * @param taxonVOSelected
     */
    public void setTaxonVOSelected(VOTaxonJSF taxonVOSelected) {
        this.taxonVOSelected = taxonVOSelected;
    }

    /**
     *
     * @return
     */
    public String getTaxonGroupSelected() {
        return taxonGroupSelected;
    }

    /**
     *
     * @return
     */
    public TreeNodeTaxon getTreeNodeTaxonSelected() {
        return treeNodeTaxonSelected;
    }

    /**
     *
     * @param treeNodeTaxonSelected
     */
    public void setTreeNodeTaxonSelected(TreeNodeTaxon treeNodeTaxonSelected) {
        this.treeNodeTaxonSelected = treeNodeTaxonSelected;
    }

    /**
     *
     * @return
     */
    public Long getIdTaxonVOSelected() {
        return idTaxonVOSelected;
    }

    /**
     *
     * @param idTaxonVOSelected
     */
    public void setIdTaxonVOSelected(Long idTaxonVOSelected) {
        this.idTaxonVOSelected = idTaxonVOSelected;
    }

    /**
     *
     * @return
     */
    @Override
    public boolean getIsStepValid() {
        if (getStep() == 1) {
            return getParametersRequest().getPlateformeStepIsValid();
        } else if (getStep() == 2) {
            return getParametersRequest().getDateStepIsValid();
        } else if (getStep() == 3) {
            return getParametersRequest().getVariableStepIsValid();
        } else if (getStep() == 4) {
            return getParametersRequest().getTaxonStepIsValid();
        }
        return false;
    }

    /**
     *
     * @return
     */
    public String getTaxonSearch() {
        return taxonSearch;
    }

    /**
     *
     * @param taxonSearch
     */
    public void setTaxonSearch(String taxonSearch) {
        this.taxonSearch = taxonSearch;
    }

    /**
     *
     * @return
     */
    public List<VOTaxonJSF> getTaxonsVOAvailables() {
        return taxonsVOAvailables;
    }

    /**
     *
     * @param taxonsVOAvailables
     */
    public void setTaxonsVOAvailables(List<VOTaxonJSF> taxonsVOAvailables) {
        this.taxonsVOAvailables = taxonsVOAvailables;
    }

    /**
     *
     * @return
     */
    public Boolean getChoiceLength() {
        return choiceLength;
    }

    /**
     *
     * @param choiceLength
     * @throws BusinessException
     */
    public void setChoiceLength(Boolean choiceLength) throws BusinessException {
        this.choiceLength = choiceLength;
        if (!choiceLength) {
            this.minLength = "0";
            this.maxLength = "20";
            this.lengthOrder = "";
            updateLengthList();
        }
    }

    /**
     *
     * @return
     */
    public Boolean getChoiceBiovolume() {
        return choiceBiovolume;
    }

    /**
     *
     * @param choiceBiovolume
     * @throws BusinessException
     */
    public void setChoiceBiovolume(Boolean choiceBiovolume) throws BusinessException {
        this.choiceBiovolume = choiceBiovolume;
        if (!choiceBiovolume) {
            this.minBiovolume = "0";
            this.maxBiovolume = "10000";
            this.lengthOrder = "";
            updateBiovolumeList();
        }
    }

    /**
     *
     * @return
     */
    public String getMinLength() {
        return minLength;
    }

    /**
     *
     * @param minLength
     */
    public void setMinLength(String minLength) {
        this.minLength = minLength;
    }

    /**
     *
     * @return
     */
    public String getMaxLength() {
        return maxLength;
    }

    /**
     *
     * @param maxLength
     */
    public void setMaxLength(String maxLength) {
        this.maxLength = maxLength;
    }

    /**
     *
     * @return
     */
    public String getMinBiovolume() {
        return minBiovolume;
    }

    /**
     *
     * @param minBiovolume
     */
    public void setMinBiovolume(String minBiovolume) {
        this.minBiovolume = minBiovolume;
    }

    /**
     *
     * @return
     */
    public String getMaxBiovolume() {
        return maxBiovolume;
    }

    /**
     *
     * @param maxBiovolume
     */
    public void setMaxBiovolume(String maxBiovolume) {
        this.maxBiovolume = maxBiovolume;
    }

    /**
     *
     * @return
     */
    public String getLengthValue() {
        return lengthValue;
    }

    /**
     *
     * @param lengthValue
     */
    public void setLengthValue(String lengthValue) {
        this.lengthValue = lengthValue;
    }

    /**
     *
     * @return
     */
    public String getLengthOrder() {
        return lengthOrder;
    }

    /**
     *
     * @param lengthOrder
     */
    public void setLengthOrder(String lengthOrder) {
        this.lengthOrder = lengthOrder;
    }

    /**
     *
     * @return
     */
    public String getBiovolumeValue() {
        return biovolumeValue;
    }

    /**
     *
     * @param biovolumeValue
     */
    public void setBiovolumeValue(String biovolumeValue) {
        this.biovolumeValue = biovolumeValue;
    }

    /**
     *
     * @return
     */
    public String getBiovolumeOrder() {
        return biovolumeOrder;
    }

    /**
     *
     * @param biovolumeOrder
     */
    public void setBiovolumeOrder(String biovolumeOrder) {
        this.biovolumeOrder = biovolumeOrder;
    }

    /**
     *
     * @return
     */
    public List<VOTaxonJSF> getTaxonsLengthAvailables() {
        return taxonsLengthAvailables;
    }

    /**
     *
     * @param taxonsLengthAvailables
     */
    public void setTaxonsLengthAvailables(List<VOTaxonJSF> taxonsLengthAvailables) {
        this.taxonsLengthAvailables = taxonsLengthAvailables;
    }

    /**
     *
     * @param transactionManager
     */
    public void setTransactionManager(JpaTransactionManager transactionManager) {
        this.transactionManager = transactionManager;
    }

    /**
     *
     * @param securityContext
     */
    public void setSecurityContext(ISecurityContext securityContext) {
        this.securityContext = securityContext;
    }

    /**
     *
     * @return
     */
    public UITree getTreePreselectionListTaxons() {
        return treePreselectionListTaxons;
    }

    /**
     *
     * @param treePreselectionListTaxons
     */
    public void setTreePreselectionListTaxons(UITree treePreselectionListTaxons) {
        this.treePreselectionListTaxons = treePreselectionListTaxons;
    }

    /**
     *
     * @param treeNodePreselection
     */
    public void setTreeNodePreselection(UITreeNode treeNodePreselection) {
        this.treeNodePreselection = treeNodePreselection;
    }

    /**
     *
     * @return
     */
    public UITreeNode getTreeNodePreselection() {
        return treeNodePreselection;
    }

    /**
     *
     * @return
     */
    public Boolean getSommation() {
        return sommation;
    }

    /**
     *
     * @param sommation
     */
    public void setSommation(Boolean sommation) {
        this.sommation = sommation;
    }

    /**
     *
     * @return
     */
    public Boolean getDetail() {
        return detail;
    }

    /**
     *
     * @param detail
     */
    public void setDetail(Boolean detail) {
        this.detail = detail;
    }

    /**
     *
     * @return
     */
    public Boolean getGenre() {
        return genre;
    }

    /**
     *
     * @param genre
     */
    public void setGenre(Boolean genre) {
        this.genre = genre;
        try {
            filterLists();
        } catch (BusinessException e) {
            e.printStackTrace();
        }

    }

    /**
     *
     * @return
     */
    public UITreeNode getTreeNodePreselectionLeaf() {
        return treeNodePreselectionLeaf;
    }

    /**
     *
     * @param treeNodePreselectionLeaf
     */
    public void setTreeNodePreselectionLeaf(UITreeNode treeNodePreselectionLeaf) {
        this.treeNodePreselectionLeaf = treeNodePreselectionLeaf;
    }

    /**
     *
     * @return
     */
    @Override
    public String nextStep() {
        super.nextStep();
        switch (getStep()) {
            case 3:
                try {
                    createOrUpdateListVariablesAvailables();
                } catch (BusinessException e) {
                    variablesAvailables.clear();
                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                break;
            case 4:
                if (!getParametersRequest().getVariablesSelected().isEmpty()) {
                    try {
                        initTaxons();
                    } catch (BusinessException e) {
                        taxonsAvailables.clear();
                        taxonsVOAvailables.clear();
                        preselectionTaxonsAvailables.clear();
                        rootNodes.clear();
                        preselectionRootNodes.clear();
                    }
                }
                break;
            default:
                break;
        }
        return null;
    }

    /**
     *
     * @return
     */
    public Boolean getPropriete() {
        return propriete;
    }

    /**
     *
     * @param propriete
     */
    public void setPropriete(Boolean propriete) {
        this.propriete = propriete;
    }

    /**
     *
     * @return
     */
    public Boolean getAggregated() {
        return aggregated;
    }

    /**
     *
     * @param aggregated
     */
    public void setAggregated(Boolean aggregated) {
        this.aggregated = aggregated;
    }

    /**
     *
     * @return
     */
    public TreeNodeAlgal getTreeNodeAlgalSelected() {
        return treeNodeAlgalSelected;
    }

    /**
     *
     * @param treeNodeAlgalSelected
     */
    public void setTreeNodeAlgalSelected(TreeNodeAlgal treeNodeAlgalSelected) {
        this.treeNodeAlgalSelected = treeNodeAlgalSelected;
    }

    /**
     *
     * @return
     */
    public Properties getPropertiesProjectsNames() {
        if (this.propertiesProjectsNames == null) {
            this.setPropertiesProjectsNames(localizationManager.newProperties(Projet.NAME_TABLE, "nom"));
        }
        return propertiesProjectsNames;
    }

    /**
     *
     * @param propertiesProjectsNames
     */
    public void setPropertiesProjectsNames(Properties propertiesProjectsNames) {
        this.propertiesProjectsNames = propertiesProjectsNames;
    }

    /**
     *
     * @return
     */
    public Properties getPropertiesPlatformNames() {
        if (this.propertiesPlatformNames == null) {
            this.setPropertiesPlatformNames(localizationManager.newProperties(Plateforme.TABLE_NAME, "nom"));
        }
        return propertiesPlatformNames;
    }

    /**
     *
     * @param propertiesPlatformNames
     */
    public void setPropertiesPlatformNames(Properties propertiesPlatformNames) {
        this.propertiesPlatformNames = propertiesPlatformNames;
    }

    /**
     *
     * @param localizationManager
     */
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    /**
     *
     * @return
     */
    public Properties getPropertiesSiteNames() {
        if (this.propertiesSiteNames == null) {
            this.setPropertiesSiteNames(localizationManager.newProperties(Site.NAME_ENTITY_JPA, "nom"));
        }
        return propertiesSiteNames;
    }

    /**
     *
     * @param propertiesSiteNames
     */
    public void setPropertiesSiteNames(Properties propertiesSiteNames) {
        this.propertiesSiteNames = propertiesSiteNames;
    }

    /**
     *
     * @return
     */
    public Properties getPropertiesVariableNames() {
        if (this.propertiesVariableNames == null) {
            this.setPropertiesVariableNames(localizationManager.newProperties(Variable.NAME_ENTITY_JPA, "nom"));
        }
        return propertiesVariableNames;
    }

    /**
     *
     * @param propertiesVariableNames
     */
    public void setPropertiesVariableNames(Properties propertiesVariableNames) {
        this.propertiesVariableNames = propertiesVariableNames;
    }

    /**
     *
     * @return
     */
    public Properties getPropertiesTaxonLevelNames() {
        if (this.propertiesTaxonLevelNames == null) {
            this.setPropertiesTaxonLevelNames(localizationManager.newProperties(TaxonNiveau.TABLE_NAME, "nom"));
        }
        return propertiesTaxonLevelNames;
    }

    /**
     *
     * @param propertiesTaxonLevelNames
     */
    public void setPropertiesTaxonLevelNames(Properties propertiesTaxonLevelNames) {
        this.propertiesTaxonLevelNames = propertiesTaxonLevelNames;
    }

    /**
     *
     * @return
     */
    public Properties getPropertiesQualitativeNames() {
        if (this.propertiesQualitativeNames == null) {
            this.setPropertiesQualitativeNames(localizationManager.newProperties(ValeurQualitative.NAME_ENTITY_JPA, "value"));
        }
        return propertiesQualitativeNames;
    }

    /**
     *
     * @param propertiesQualitativeNames
     */
    public void setPropertiesQualitativeNames(Properties propertiesQualitativeNames) {
        this.propertiesQualitativeNames = propertiesQualitativeNames;
    }

    /**
     *
     * @return
     */
    public Boolean getFilter() {
        return filter;
    }

    /**
     *
     * @param filter
     */
    public void setFilter(Boolean filter) {
        this.filter = filter;
    }

    /**
     *
     * @return
     */
    public String getAffichage() {
        return affichage;
    }

    /**
     *
     * @param affichage
     */
    public void setAffichage(String affichage) {
        this.affichage = affichage;
    }

    /**
     *
     * @param fileCompManager
     */
    public void setFileCompManager(IFileCompManager fileCompManager) {
        this.fileCompManager = fileCompManager;
    }
}
