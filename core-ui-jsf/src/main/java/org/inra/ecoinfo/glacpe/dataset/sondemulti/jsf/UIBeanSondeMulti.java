package org.inra.ecoinfo.glacpe.dataset.sondemulti.jsf;

import java.io.Serializable;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.inra.ecoinfo.dataset.security.ISecurityDatasetManager;
import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.filecomp.IFileCompManager;
import org.inra.ecoinfo.filecomp.ItemFile;
import org.inra.ecoinfo.filecomp.entity.FileComp;
import org.inra.ecoinfo.glacpe.dataset.sondemulti.ISondeMultiDatatypeManager;
import org.inra.ecoinfo.glacpe.dataset.sondemulti.impl.SondeMultiParameters;
import org.inra.ecoinfo.glacpe.extraction.jsf.AbstractUIBeanForSteps;
import org.inra.ecoinfo.glacpe.extraction.vo.DatasRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.DepthRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.ExtractionParametersVO;
import org.inra.ecoinfo.glacpe.extraction.vo.ProjetSiteVO;
import org.inra.ecoinfo.glacpe.extraction.vo.VariableVO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.projetsite.ProjetSite;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.notifications.INotificationsManager;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.security.ISecurityContext;
import org.inra.ecoinfo.security.entity.Role;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;

/**
 *
 * @author ptcherniati
 */
@ManagedBean(name = "uiSondeMulti")
@ViewScoped
public class UIBeanSondeMulti extends AbstractUIBeanForSteps implements Serializable {

    private static final String DEFAULT_UNCERTAINTY = "0";

    private static final String DEFAULT_TARGETVALUE = "0";

    private static final long serialVersionUID = 1L;

    private static final String DATATYPE = "sonde_multiparametres";
    private static final String THEME = "profils_verticaux_multiparametres";
    private static final String PATH_PATTERN = "%s/%s/%s/%s/%s";

    private Map<Long, ProjectVO> projectsAvailables = new HashMap<Long, UIBeanSondeMulti.ProjectVO>();

    private Map<Long, VOVariableJSF> variablesAvailables = new HashMap<Long, VOVariableJSF>();

    /**
     *
     */
    @ManagedProperty(value = "#{sondeMultiDatatypeManager}")
    protected ISondeMultiDatatypeManager sondeMultiDatatypeManager;

    /**
     *
     */
    @ManagedProperty(value = "#{notificationsManager}")
    protected INotificationsManager notificationsManager;

    /**
     *
     */
    @ManagedProperty(value = "#{extractionManager}")
    protected IExtractionManager extractionManager;

    /**
     *
     */
    @ManagedProperty(value = "#{transactionManager}")
    protected JpaTransactionManager transactionManager;

    /**
     *
     */
    @ManagedProperty(value = "#{securityContext}")
    protected ISecurityContext securityContext;

    /**
     *
     */
    @ManagedProperty(value = "#{localizationManager}")
    protected ILocalizationManager localizationManager;

    /**
     *
     */
    @ManagedProperty(value = "#{fileCompManager}")
    protected IFileCompManager fileCompManager;

    private Properties propertiesProjectsNames;
    private Properties propertiesPlatformNames;
    private Properties propertiesSiteNames;
    private Properties propertiesVariableNames;

    private ParametersRequest parametersRequest = new ParametersRequest();

    private Long idPlateformeSelected;

    private Long idProjectSelected;

    private Long idVariableSelected;

    private ExtractionParametersVO currentParameterExtraction;

    /**
     *
     */
    public UIBeanSondeMulti() {}

    /**
     *
     * @return
     */
    public String navigate() {
        return "sondeMulti";
    }

    /**
     *
     * @return
     * @throws BusinessException
     */
    public String addAllVariables() throws BusinessException {
        Map<Long, VOVariableJSF> variablesSelected = parametersRequest.getVariablesSelected();
        variablesSelected.clear();

        for (VOVariableJSF variable : variablesAvailables.values()) {
            if (variable.getVariable().getIsDataAvailable()) {
                variablesSelected.put(variable.getVariable().getId(), variable);
                variable.setSelected(true);
            }
        }

        updateDatasRequestParamWithVariables();
        return null;
    }

    /**
     *
     * @return
     * @throws BusinessException
     * @throws ParseException
     */
    public String removeAllVariables() throws BusinessException, ParseException {
        createOrUpdateListVariablesAvailables();
        return null;
    }

    /**
     *
     * @return
     * @throws BusinessException
     */
    public String selectPlateforme() throws BusinessException {
        ProjectVO projectSelected = projectsAvailables.get(idProjectSelected);
        PlateformeVO plateformeSelected = projectSelected.getPlateformes().get(idPlateformeSelected);
        if (plateformeSelected.getSelected()) {
            parametersRequest.getPlateformesSelected().remove(idPlateformeSelected);
            plateformeSelected.setSelected(false);
        } else {
            parametersRequest.getPlateformesSelected().put(idPlateformeSelected, plateformeSelected);
            plateformeSelected.setSelected(true);
        }

        return null;
    }

    /**
     *
     * @return
     * @throws BusinessException
     */
    public String changeProject() throws BusinessException {
        parametersRequest.getPlateformesSelected().clear();
        for (ProjectVO projectSelected : projectsAvailables.values()) {
            for (PlateformeVO plateformeSelected : projectSelected.getPlateformes().values()) {
                plateformeSelected.setSelected(false);
            }
        }

        return null;
    }

    /**
     *
     * @return
     * @throws BusinessException
     */
    public String getProjetSelected() throws BusinessException {
        String localizedProjetSelected = null;
        if (idProjectSelected != null) {
            String nom = projectsAvailables.get(idProjectSelected).getProjet().getNom();
            localizedProjetSelected = getPropertiesProjectsNames().getProperty(nom);
            if (Strings.isNullOrEmpty(localizedProjetSelected))
                localizedProjetSelected = nom;
            return localizedProjetSelected;
        }
        return localizedProjetSelected;
    }

    /**
     *
     * @return
     * @throws BusinessException
     */
    public String selectVariable() throws BusinessException {
        VOVariableJSF variableSelected = variablesAvailables.get(idVariableSelected);

        if (variableSelected.getSelected()) {
            parametersRequest.getVariablesSelected().remove(idVariableSelected);
            variableSelected.setSelected(false);
        } else {
            parametersRequest.getVariablesSelected().put(idVariableSelected, variableSelected);
            variableSelected.setSelected(true);

        }
        updateDatasRequestParamWithVariables();
        return null;
    }

    /**
     *
     * @return
     */
    public String addParameterExtraction() {
        currentParameterExtraction.getTargetValues().add(DEFAULT_TARGETVALUE);
        currentParameterExtraction.getUncertainties().add(DEFAULT_UNCERTAINTY);
        return null;
    }

    private void updateDatasRequestParamWithVariables() throws BusinessException {

        List<ExtractionParametersVO> extractionParameters = parametersRequest.getDatasRequestParam().getExtractionParameters();
        extractionParameters.clear();

        for (VOVariableJSF variable : parametersRequest.getVariablesSelected().values()) {
            Unite unit = sondeMultiDatatypeManager.retrieveVariableUnitByCode(variable.getVariable().getCode());
            String unite = "";
            if (!unit.getCode().equals("nounit")) {
                unite = unit.getCode();
            }
            ExtractionParametersVO extractionParameter = new ExtractionParametersVO(variable.getVariable().getNom(), unite);
            extractionParameter.setLocalizedName(variable.getLocalizedVariableName());
            extractionParameters.add(extractionParameter);
        }
    }

    /**
     *
     * @return
     * @throws BusinessException
     * @throws ParseException
     */
    public List<VOVariableJSF> getVariablesAvailables() throws BusinessException, ParseException {
        if (variablesAvailables == null) {
            createOrUpdateListVariablesAvailables();
        }
        return new LinkedList<VOVariableJSF>(variablesAvailables.values());

    }

    private void createOrUpdateListVariablesAvailables() throws BusinessException, ParseException {
        variablesAvailables.clear();
        parametersRequest.getVariablesSelected().clear();

        if (!parametersRequest.plateformesSelected.isEmpty() && parametersRequest.getDateStepIsValid()) {

            List<VariableVO> variables = sondeMultiDatatypeManager.retrieveListsVariables(new LinkedList<Long>(parametersRequest.plateformesSelected.keySet()), parametersRequest.getFirstStartDate(), parametersRequest.getLastEndDate());
            for (PlateformeVO plateforme : parametersRequest.plateformesSelected.values()) {
                for (VariableVO variable : variables) {
                    if (securityContext.isRoot()
                            || securityContext.matchPrivilege(String.format("*/%s/*/sonde_multiparametres/%s", plateforme.getPlateforme().getSite().getCode(), variable.getCode()), Role.ROLE_EXTRACTION, ISecurityDatasetManager.PRIVILEGE_TYPE_DATASET))
                    {
                        if (!variablesAvailables.keySet().contains(variable.getId())) {
                            VOVariableJSF variableVO = new VOVariableJSF(variable);
                            variablesAvailables.put(variable.getId(), variableVO);
                        }
                    }
                }
            }
        }

        updateDatasRequestParamWithVariables();
    }

    /**
     *
     * @return
     * @throws BusinessException
     */
    public Map<Long, ProjectVO> getProjectsAvailables() throws BusinessException {
        TransactionStatus status = transactionManager.getTransaction(new DefaultTransactionDefinition(TransactionDefinition.PROPAGATION_REQUIRES_NEW));

        if (projectsAvailables.isEmpty()) {
            List<Projet> projets = sondeMultiDatatypeManager.retrieveSondeMultiAvailablesProjets();

            for (Projet projet : projets) {
                List<Plateforme> plateformes = Lists.newArrayList();
                for (ProjetSite projetSite : projet.getProjetsSites()) {
                    for (Plateforme plateforme : sondeMultiDatatypeManager.retrieveSondeMultiAvailablesPlateformesByProjetSiteId(projetSite.getId())) {
                        if (!plateformes.contains(plateforme)
                                && (securityContext.isRoot() || securityContext.matchPrivilege(String.format("%s/%s/*/sonde_multiparametres/*", projetSite.getProjet().getCode(), projetSite.getSite().getCode()), Role.ROLE_EXTRACTION,
                                        ISecurityDatasetManager.PRIVILEGE_TYPE_DATASET)))
                        {
                            plateformes.add(plateforme);
                        }
                    }
                }
                if (!plateformes.isEmpty()) {
                    ProjectVO projectVO = new ProjectVO(projet, plateformes);
                    projectsAvailables.put(projet.getId(), projectVO);
                }
            }

        }

        transactionManager.commit(status);

        return projectsAvailables;
    }

    private List<String> getPathes(List<ProjetSite> projetSites, List<VariableVO> variables) {
        String sitePath, projet, theme, path;
        theme = THEME;
        List<String> pathes = new LinkedList<>();
        for (ProjetSite projetSite : projetSites) {
            sitePath = projetSite.getSite().getPath();
            projet = projetSite.getProjet().getCode();
            for (VariableVO variable : variables) {
                path = String.format(PATH_PATTERN, projet, sitePath, theme, DATATYPE, variable.getCode());
                if (!pathes.contains(path)) {
                    pathes.add(path);
                }
            }
        }
        return pathes;
    }

    /**
     *
     * @return
     * @throws BusinessException
     */
    public String extract() throws BusinessException {
        Map<String, Object> metadatasMap = new HashMap<String, Object>();
        List<org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO> metadatasPlateformesVos = new LinkedList<org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO>();
        List<ProjetSite> projetSites = new LinkedList<ProjetSite>();
        for (PlateformeVO plateforme : parametersRequest.getPlateformesSelected().values()) {
            org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO plateformeVO = new org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO(plateforme.getPlateforme());
            ProjetSite projetSite = sondeMultiDatatypeManager.retrieveSondeMultiAvailablesProjetsSiteByProjetAndSite(projectsAvailables.get(idProjectSelected).getProjet().getId(), plateforme.getPlateforme().getSite().getId());
            ProjetSiteVO projetSiteVO = new ProjetSiteVO(projetSite);
            if (!projetSites.contains(projetSite)) {
                projetSites.add(projetSite);
            }

            plateformeVO.setProjet(projetSiteVO);
            metadatasPlateformesVos.add(plateformeVO);
        }

        metadatasMap.put(PlateformeVO.class.getSimpleName(), metadatasPlateformesVos);
        List<VariableVO> variablesVO = new LinkedList<VariableVO>();
        for (VOVariableJSF variable : parametersRequest.getListVariablesSelected()) {
            variablesVO.add(variable.getVariable());
        }
        metadatasMap.put(VariableVO.class.getSimpleName(), variablesVO);

        List<FileComp> listFiles = new LinkedList<FileComp>();
        try {
            if (!parametersRequest.plateformesSelected.isEmpty() && parametersRequest.getFormIsValid()) {
                IntervalDate intervalDate;

                intervalDate = new IntervalDate(parametersRequest.getFirstStartDate(), parametersRequest.getLastEndDate(), DateUtil.getSimpleDateFormatDateLocale());

                List<IntervalDate> intervalsDate = new LinkedList<>();
                intervalsDate.add(intervalDate);
                final List<ItemFile> files = fileCompManager.getFileCompFromPathesAndIntervalsDate(getPathes(projetSites, variablesVO), intervalsDate);
                for (final ItemFile file : files) {
                    if (file.getMandatory()) {
                        listFiles.add(file.getFileComp());
                    }
                }
            }
        } catch (BadExpectedValueException e) {
            throw new BusinessException("bad file", e);
        } catch (ParseException e) {
            throw new BusinessException("bad file name", e);
        }
        metadatasMap.put(FileComp.class.getSimpleName(), listFiles);

        metadatasMap.put(DatasRequestParamVO.class.getSimpleName(), parametersRequest.getDatasRequestParam().clone());
        metadatasMap.put(DepthRequestParamVO.class.getSimpleName(), parametersRequest.getDepthRequestParam().clone());

        getParametersRequest().getDatesRequestParam().getDatesYearsDiscretsFormParam().setPeriods(getParametersRequest().getDatesRequestParam().getNonPeriodsYearsContinuous());
        getParametersRequest().getDatesRequestParam().getDatesYearsRangeFormParam().setPeriods(getParametersRequest().getDatesRequestParam().getNonPeriodsYearsContinuous());

        metadatasMap.put(DatesRequestParamVO.class.getSimpleName(), parametersRequest.getDatesRequestParam().clone());
        metadatasMap.put(IExtractionManager.KEYMAP_COMMENTS, new String(parametersRequest.getCommentExtraction()));
        IParameter parameters = new SondeMultiParameters(metadatasMap);
        extractionManager.extract(parameters, 1);
        return null;
    }

    /**
     *
     */
    public class ProjectVO {

        private Projet projet;
        private String localizedProjetName;
        private Map<Long, PlateformeVO> plateformes = new HashMap<Long, PlateformeVO>();

        /**
         *
         * @param projet
         * @param plateformes
         */
        public ProjectVO(Projet projet, List<Plateforme> plateformes) {
            super();
            this.projet = projet;
            if (projet != null) {
                this.localizedProjetName = getPropertiesProjectsNames().getProperty(projet.getNom());
                if (Strings.isNullOrEmpty(this.localizedProjetName))
                    this.localizedProjetName = projet.getNom();
            }
            for (Plateforme plateforme : plateformes) {
                this.plateformes.put(plateforme.getId(), new PlateformeVO(plateforme));
            }

        }

        /**
         *
         * @return
         */
        public Projet getProjet() {
            return projet;
        }

        /**
         *
         * @return
         */
        public Map<Long, PlateformeVO> getPlateformes() {
            return plateformes;
        }

        /**
         *
         * @return
         */
        public String getLocalizedProjetName() {
            return localizedProjetName;
        }

        /**
         *
         * @param localizedProjetName
         */
        public void setLocalizedProjetName(String localizedProjetName) {
            this.localizedProjetName = localizedProjetName;
        }
    }

    /**
     *
     */
    public class PlateformeVO {

        private Plateforme plateforme;
        private String localizedPlateformeName;
        private String localizedSiteName;
        private Boolean selected = false;

        /**
         *
         * @param plateforme
         */
        public PlateformeVO(Plateforme plateforme) {
            super();
            this.plateforme = plateforme;
            if (plateforme != null) {
                this.localizedPlateformeName = getPropertiesPlatformNames().getProperty(plateforme.getNom());
                if (Strings.isNullOrEmpty(this.localizedPlateformeName))
                    this.localizedPlateformeName = plateforme.getNom();
                this.localizedSiteName = getPropertiesSiteNames().getProperty(plateforme.getSite().getNom());
                if (Strings.isNullOrEmpty(this.localizedSiteName))
                    this.localizedSiteName = plateforme.getSite().getNom();
            }
        }

        /**
         *
         * @return
         */
        public Plateforme getPlateforme() {
            return plateforme;
        }

        /**
         *
         * @return
         */
        public Boolean getSelected() {
            return selected;
        }

        /**
         *
         * @param selected
         */
        public void setSelected(Boolean selected) {
            this.selected = selected;
        }

        /**
         *
         * @return
         */
        public String getLocalizedPlateformeName() {
            return localizedPlateformeName;
        }

        /**
         *
         * @param localizedPlateformeName
         */
        public void setLocalizedPlateformeName(String localizedPlateformeName) {
            this.localizedPlateformeName = localizedPlateformeName;
        }

        /**
         *
         * @return
         */
        public String getLocalizedSiteName() {
            return localizedSiteName;
        }

        /**
         *
         * @param localizedSiteName
         */
        public void setLocalizedSiteName(String localizedSiteName) {
            this.localizedSiteName = localizedSiteName;
        }
    }

    /**
     *
     */
    public class VOVariableJSF {

        private VariableVO variable;
        private String localizedVariableName;
        private Boolean selected = false;

        /**
         *
         * @param variable
         */
        public VOVariableJSF(VariableVO variable) {
            super();
            this.variable = variable;
            if (variable != null) {
                this.localizedVariableName = getPropertiesVariableNames().getProperty(variable.getAffichage());
                if (Strings.isNullOrEmpty(this.localizedVariableName))
                    this.localizedVariableName = variable.getAffichage();
            }

        }

        /**
         *
         * @return
         */
        public VariableVO getVariable() {
            return variable;
        }

        /**
         *
         * @return
         */
        public Boolean getSelected() {
            return selected;
        }

        /**
         *
         * @param selected
         */
        public void setSelected(Boolean selected) {
            this.selected = selected;
        }

        /**
         *
         * @return
         */
        public String getLocalizedVariableName() {
            return localizedVariableName;
        }

        /**
         *
         * @param localizedVariableName
         */
        public void setLocalizedVariableName(String localizedVariableName) {
            this.localizedVariableName = localizedVariableName;
        }
    }

    /**
     *
     */
    public class ParametersRequest {

        private Map<Long, PlateformeVO> plateformesSelected = new HashMap<Long, PlateformeVO>();
        private Map<Long, VOVariableJSF> variablesSelected = new HashMap<Long, VOVariableJSF>();

        private DepthRequestParamVO depthRequestParam;
        private DatasRequestParamVO datasRequestParam;
        private DatesRequestParamVO datesRequestParam;
        private String commentExtraction;

        /**
         *
         * @return
         */
        public Boolean getPlateformeStepIsValid() {
            return !plateformesSelected.isEmpty();
        }

        /**
         *
         * @return
         */
        public Boolean getVariableStepIsValid() {
            return !variablesSelected.isEmpty() && getDateStepIsValid();
        }

        /**
         *
         * @return
         */
        public Boolean getDephtDatatypeStepIsValid() {
            if (getDepthRequestParam().isValidDepht() && (getDatasRequestParam().getDataBalancedByDepth() || getDatasRequestParam().getRawData()) && getVariableStepIsValid()) {
                return true;
            } else {
                return false;
            }
        }

        /**
         *
         * @return
         */
        public Boolean getDateStepIsValid() {
            if ((datesRequestParam.getSelectedFormSelection() != null) && datesRequestParam.getCurrentDatesFormParam().getIsValid() && getPlateformeStepIsValid()) {
                return true;
            } else {
                return false;
            }
        }

        /**
         *
         * @return
         * @throws ParseException
         */
        public Date getFirstStartDate() throws ParseException {
            if (!datesRequestParam.getCurrentDatesFormParam().getPeriods().isEmpty()) {
                Date firstDate = null;
                for (Date mapDate : datesRequestParam.getCurrentDatesFormParam().retrieveParametersMap().values()) {
                    if (firstDate == null) {
                        firstDate = mapDate;
                    } else if (firstDate.after(mapDate)) {
                        firstDate = mapDate;
                    }
                }
                return firstDate;
            }
            return null;
        }

        /**
         *
         * @return
         * @throws ParseException
         */
        public Date getLastEndDate() throws ParseException {
            if (!datesRequestParam.getCurrentDatesFormParam().getPeriods().isEmpty()) {
                Date lastDate = null;
                for (Date mapDate : datesRequestParam.getCurrentDatesFormParam().retrieveParametersMap().values()) {
                    if (lastDate == null) {
                        lastDate = mapDate;
                    } else if (lastDate.before(mapDate)) {
                        lastDate = mapDate;
                    }
                }
                return lastDate;
            }
            return null;
        }

        /**
         *
         * @return
         */
        public DatesRequestParamVO getDatesRequestParam() {
            if (datesRequestParam == null) {
                initDatesRequestParam();
            }
            return datesRequestParam;
        }

        private void initDatesRequestParam() {
            datesRequestParam = new DatesRequestParamVO(localizationManager);
        }

        /**
         *
         * @return
         */
        public Map<Long, PlateformeVO> getPlateformesSelected() {
            return plateformesSelected;
        }

        /**
         *
         * @return
         */
        public List<PlateformeVO> getListPlateformesSelected() {
            return new LinkedList<PlateformeVO>(plateformesSelected.values());
        }

        /**
         *
         * @return
         */
        public Map<Long, VOVariableJSF> getVariablesSelected() {
            return variablesSelected;
        }

        /**
         *
         * @return
         */
        public List<VOVariableJSF> getListVariablesSelected() {
            return new LinkedList<VOVariableJSF>(variablesSelected.values());
        }

        private void initDepthRequestParam() {
            depthRequestParam = new DepthRequestParamVO(localizationManager);
        }

        /**
         *
         * @return
         */
        public DepthRequestParamVO getDepthRequestParam() {
            if (depthRequestParam == null) {
                initDepthRequestParam();
            }
            return depthRequestParam;
        }

        private void initDatasRequestParam() {
            datasRequestParam = new DatasRequestParamVO(localizationManager);
        }

        /**
         *
         * @return
         */
        public DatasRequestParamVO getDatasRequestParam() {
            if (datasRequestParam == null) {
                initDatasRequestParam();
            }
            return datasRequestParam;
        }

        /**
         *
         * @return
         */
        public String getCommentExtraction() {
            return commentExtraction;
        }

        /**
         *
         * @param commentExtraction
         */
        public void setCommentExtraction(String commentExtraction) {
            this.commentExtraction = commentExtraction;
        }

        /**
         *
         * @return
         */
        public boolean getFormIsValid() {
            return getDephtDatatypeStepIsValid();
        }

    }

    // GETTERS SETTERS - BEANS

    /**
     *
     * @param notificationsManager
     */
    
    public void setNotificationsManager(INotificationsManager notificationsManager) {
        this.notificationsManager = notificationsManager;
    }

    /**
     *
     * @param sondeMultiDatatypeManager
     */
    public void setSondeMultiDatatypeManager(ISondeMultiDatatypeManager sondeMultiDatatypeManager) {
        this.sondeMultiDatatypeManager = sondeMultiDatatypeManager;
    }

    /**
     *
     * @param idPlateformeSelected
     */
    public void setIdPlateformeSelected(Long idPlateformeSelected) {
        this.idPlateformeSelected = idPlateformeSelected;
    }

    /**
     *
     * @param idProjectSelected
     */
    public void setIdProjectSelected(Long idProjectSelected) {
        this.idProjectSelected = idProjectSelected;
    }

    /**
     *
     * @return
     */
    public ParametersRequest getParametersRequest() {
        return parametersRequest;
    }

    /**
     *
     * @param idVariableSelected
     */
    public void setIdVariableSelected(Long idVariableSelected) {
        this.idVariableSelected = idVariableSelected;
    }

    /**
     *
     * @param currentParameterExtraction
     */
    public void setCurrentParameterExtraction(ExtractionParametersVO currentParameterExtraction) {
        this.currentParameterExtraction = currentParameterExtraction;
    }

    /**
     *
     * @param extractionManager
     */
    public void setExtractionManager(IExtractionManager extractionManager) {
        this.extractionManager = extractionManager;
    }

    /**
     *
     * @return
     */
    public Long getIdProjectSelected() {
        return idProjectSelected;
    }

    /**
     *
     * @return
     */
    @Override
    public boolean getIsStepValid() {
        if (getStep() == 1) {
            return getParametersRequest().getPlateformeStepIsValid();
        } else if (getStep() == 2) {
            return getParametersRequest().getDateStepIsValid();
        } else if (getStep() == 3) {
            return getParametersRequest().getVariableStepIsValid();
        }
        return false;
    }

    /**
     *
     * @param transactionManager
     */
    public void setTransactionManager(JpaTransactionManager transactionManager) {
        this.transactionManager = transactionManager;
    }

    /**
     *
     * @param securityContext
     */
    public void setSecurityContext(ISecurityContext securityContext) {
        this.securityContext = securityContext;
    }

    /**
     *
     * @return
     */
    @Override
    public String nextStep() {
        super.nextStep();
        switch (getStep()) {
            case 3 :
                try {
                    createOrUpdateListVariablesAvailables();
                } catch (BusinessException e) {
                    variablesAvailables.clear();
                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                break;
            default :
                break;
        }
        return null;
    }

    /**
     *
     * @return
     */
    public Properties getPropertiesProjectsNames() {
        if (this.propertiesProjectsNames == null)
            this.setPropertiesProjectsNames(localizationManager.newProperties(Site.NAME_ENTITY_JPA, "nom"));
        return propertiesProjectsNames;
    }

    /**
     *
     * @param propertiesProjectsNames
     */
    public void setPropertiesProjectsNames(Properties propertiesProjectsNames) {
        this.propertiesProjectsNames = propertiesProjectsNames;
    }

    /**
     *
     * @return
     */
    public Properties getPropertiesPlatformNames() {
        if (this.propertiesPlatformNames == null)
            this.setPropertiesPlatformNames(localizationManager.newProperties(Plateforme.TABLE_NAME, "nom"));
        return propertiesPlatformNames;
    }

    /**
     *
     * @param propertiesPlatformNames
     */
    public void setPropertiesPlatformNames(Properties propertiesPlatformNames) {
        this.propertiesPlatformNames = propertiesPlatformNames;
    }

    /**
     *
     * @param localizationManager
     */
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    /**
     *
     * @return
     */
    public Properties getPropertiesSiteNames() {
        if (this.propertiesSiteNames == null)
            this.setPropertiesSiteNames(localizationManager.newProperties(Site.NAME_ENTITY_JPA, "nom"));
        return propertiesSiteNames;
    }

    /**
     *
     * @param propertiesSiteNames
     */
    public void setPropertiesSiteNames(Properties propertiesSiteNames) {
        this.propertiesSiteNames = propertiesSiteNames;
    }

    /**
     *
     * @return
     */
    public Properties getPropertiesVariableNames() {
        if (this.propertiesVariableNames == null)
            this.setPropertiesVariableNames(localizationManager.newProperties(Variable.NAME_ENTITY_JPA, "affichage"));
        return propertiesVariableNames;
    }

    /**
     *
     * @param propertiesVariableNames
     */
    public void setPropertiesVariableNames(Properties propertiesVariableNames) {
        this.propertiesVariableNames = propertiesVariableNames;
    }

    /**
     *
     * @param fileCompManager
     */
    public void setFileCompManager(IFileCompManager fileCompManager) {
        this.fileCompManager = fileCompManager;
    }

}
