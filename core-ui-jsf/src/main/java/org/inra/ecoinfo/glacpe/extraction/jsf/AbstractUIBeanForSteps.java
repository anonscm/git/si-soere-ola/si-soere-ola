package org.inra.ecoinfo.glacpe.extraction.jsf;

import javax.faces.component.html.HtmlPanelGroup;

import org.richfaces.component.UIAccordion;
import org.richfaces.component.UITogglePanel;
import org.richfaces.component.UITogglePanelItem;

/**
 *
 * @author ptcherniati
 */
public abstract class AbstractUIBeanForSteps {

    private static final String TITLE_STEP = "%d : %s";

    /**
     *
     */
    public AbstractUIBeanForSteps() {
        super();
    }

    private int step = 1;
    private UITogglePanel togglePanel;
    private UIAccordion accordion;
    private HtmlPanelGroup panel;

    /**
     *
     * @return
     */
    public abstract boolean getIsStepValid();

    /**
     *
     * @return
     */
    public UITogglePanel getTogglePanel() {
        return togglePanel;
    }

    /**
     *
     * @param panel
     */
    public void setTogglePanel(UITogglePanel panel) {
        this.togglePanel = panel;
    }

    /**
     *
     * @param accordion
     */
    public void setAccordion(UIAccordion accordion) {
        this.accordion = accordion;
    }

    /**
     *
     * @return
     */
    public UIAccordion getAccordion() {
        return accordion;
    }

    /**
     *
     * @return
     */
    public UITogglePanelItem getCurrentToggleItem() {
        return (UITogglePanelItem) togglePanel.getItem(togglePanel.getActiveItem());
    }

    /**
     *
     * @param step
     */
    public void setStep(int step) {
        this.step = step;
    }

    /**
     *
     * @return
     */
    public int getStep() {
        return step;
    }

    /**
     *
     * @return
     */
    public String nextStep() {
        if (togglePanel.getNextItem() != null) {
            togglePanel.setActiveItem(togglePanel.getNextItem().getName());
            step++;
        }
        if (accordion.getNextItem() != null) {
            accordion.setActiveItem(accordion.getNextItem().getName());
        }
        return null;
    }

    /**
     *
     * @return
     */
    public boolean getIsNextStep() {
        if (step < togglePanel.getChildren().size()) {
            return true;
        }
        return false;
    }

    /**
     *
     * @return
     */
    public String prevStep() {
        if (togglePanel.getPrevItem() != null) {
            togglePanel.setActiveItem(togglePanel.getPrevItem().getName());
            step--;
        }
        if (accordion.getPrevItem() != null) {
            accordion.setActiveItem(accordion.getPrevItem().getName());
        }
        return null;
    }

    /**
     *
     * @param panel
     */
    public void setPanel(HtmlPanelGroup panel) {
        this.panel = panel;
    }

    /**
     *
     * @return
     */
    public HtmlPanelGroup getPanel() {
        return panel;
    }

    /**
     *
     * @return
     */
    public String getTitleForStep() {
        return String.format(TITLE_STEP, step, togglePanel.getItemByIndex(step - 1).getName());
    }

}