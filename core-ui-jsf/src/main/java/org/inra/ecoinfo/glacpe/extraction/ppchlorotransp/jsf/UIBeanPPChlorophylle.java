package org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.jsf;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.inra.ecoinfo.dataset.security.ISecurityDatasetManager;
import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.filecomp.IFileCompManager;
import org.inra.ecoinfo.filecomp.ItemFile;
import org.inra.ecoinfo.filecomp.entity.FileComp;
import org.inra.ecoinfo.glacpe.dataset.chloro.IChlorophylleDatatypeManager;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.IConditionGeneraleDatatypeManager;
import org.inra.ecoinfo.glacpe.dataset.productionprimaire.IPPDatatypeManager;
import org.inra.ecoinfo.glacpe.extraction.jsf.AbstractUIBeanForSteps;
import org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.impl.PPChloroTranspParameter;
import org.inra.ecoinfo.glacpe.extraction.vo.DatasRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.DepthRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.GroupeVariableVO;
import org.inra.ecoinfo.glacpe.extraction.vo.ProjetSiteVO;
import org.inra.ecoinfo.glacpe.extraction.vo.VariableVO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.projetsite.ProjetSite;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.notifications.INotificationsManager;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.security.ISecurityContext;
import org.inra.ecoinfo.security.entity.Role;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.richfaces.component.UITree;
import org.richfaces.component.UITreeNode;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;

/**
 *
 * @author ptcherniati
 */
@ManagedBean(name = "uiPPChlorophylle")
@ViewScoped
public class UIBeanPPChlorophylle extends AbstractUIBeanForSteps implements Serializable {

    /**
     *
     */
    public class ParametersRequest {

        private Map<Long, PlateformeVO> plateformesSelected = new HashMap<Long, PlateformeVO>();
        private Map<Long, VOVariableJSF> variablesSelected = new HashMap<Long, VOVariableJSF>();

        private DepthRequestParamVO depthRequestParam;
        private DatasRequestParamVO datasRequestParam;
        private DatesRequestParamVO datesRequestParam;
        private String commentExtraction;
        
        /**
         *
         * @return
         */
        public String getCommentExtraction() {
            return commentExtraction;
        }

        private void initDatasRequestParam() {
            datasRequestParam = new DatasRequestParamVO(localizationManager);
        }

        /**
         *
         * @return
         */
        public DatasRequestParamVO getDatasRequestParam() {
            if (datasRequestParam == null) {
                initDatasRequestParam();
            }
            return datasRequestParam;
        }

        /**
         *
         * @return
         */
        public DatesRequestParamVO getDatesRequestParam() {
            if (datesRequestParam == null) {
                initDatesRequestParam();
            }
            return datesRequestParam;
        }

        /**
         *
         * @return
         */
        public boolean getHasPPVariablesSelected() {
            for (Entry<Long, VOVariableJSF> variableEntry : getVariablesSelected().entrySet()) {
                if (PPChloroTranspParameter.PRODUCTION_PRIMAIRE.equals(variableEntry.getValue().getVariable().getGroup()))
                    return true;
            }
            return false;
        }

        /**
         *
         * @return
         */
        public boolean getHasChloroVariablesSelected() {
            for (Entry<Long, VOVariableJSF> variableEntry : getVariablesSelected().entrySet()) {
                if (PPChloroTranspParameter.CHLOROPHYLLE.equals(variableEntry.getValue().getVariable().getGroup()))
                    return true;
            }
            return false;
        }

        /**
         *
         * @return
         */
        public boolean getHasTranspVariablesSelected() {
            for (Entry<Long, VOVariableJSF> variableEntry : getVariablesSelected().entrySet()) {
                if (PPChloroTranspParameter.TRANSPARENCE.equals(variableEntry.getValue().getVariable().getGroup()))
                    return true;
            }
            return false;
        }

        /**
         *
         * @return
         */
        public Boolean getDateStepIsValid() {
            if ((datesRequestParam.getSelectedFormSelection() != null) && getPlateformeStepIsValid() && datesRequestParam.getCurrentDatesFormParam().getIsValid()) {
                return true;
            } else {
                return false;
            }
        }

        /**
         *
         * @return
         * @throws ParseException
         */
        public Date getFirstStartDate() throws ParseException {
            if (!datesRequestParam.getCurrentDatesFormParam().getPeriods().isEmpty()) {
                Date firstDate = null;
                for (Date mapDate : datesRequestParam.getCurrentDatesFormParam().retrieveParametersMap().values()) {
                    if (firstDate == null) {
                        firstDate = mapDate;
                    } else if (firstDate.after(mapDate)) {
                        firstDate = mapDate;
                    }
                }
                return firstDate;
            }
            return null;
        }

        /**
         *
         * @return
         * @throws ParseException
         */
        public Date getLastEndDate() throws ParseException {
            if (!datesRequestParam.getCurrentDatesFormParam().getPeriods().isEmpty()) {
                Date lastDate = null;
                for (Date mapDate : datesRequestParam.getCurrentDatesFormParam().retrieveParametersMap().values()) {
                    if (lastDate == null) {
                        lastDate = mapDate;
                    } else if (lastDate.before(mapDate)) {
                        lastDate = mapDate;
                    }
                }
                return lastDate;
            }
            return null;
        }

        /**
         *
         * @return
         */
        public Boolean getDephtDatatypeStepIsValid() {
            if (!getVariableStepIsValid())
                return false;
            if (getParametersRequest().getDepthRequestParam().isValidDepht()
                    && (getParametersRequest().getDatasRequestParam().getDataBalancedByDepth() || getParametersRequest().getDatasRequestParam().getRawData() || getParametersRequest().getDatasRequestParam().getMaxValueAndAssociatedDepth() || getParametersRequest()
                            .getDatasRequestParam().getMinValueAndAssociatedDepth()))
            {
                return true;
            }
            return false;
        }

        private void initDepthRequestParam() {
            depthRequestParam = new DepthRequestParamVO(localizationManager);
        }

        /**
         *
         * @return
         */
        public DepthRequestParamVO getDepthRequestParam() {
            if (depthRequestParam == null) {
                initDepthRequestParam();
            }
            return depthRequestParam;
        }

        /**
         *
         * @return
         */
        public List<PlateformeVO> getListPlateformesSelected() {
            return new LinkedList<PlateformeVO>(plateformesSelected.values());
        }

        /**
         *
         * @return
         */
        public List<VOVariableJSF> getListVariablesSelected() {
            return Lists.newArrayList(variablesSelected.values().iterator());
        }

        /**
         *
         * @return
         */
        public Map<String, List<VOVariableJSF>> getMapVariablesSelected() {
            Map<String, List<VOVariableJSF>> variables = new HashMap<String, List<VOVariableJSF>>();
            for (Entry<Long, VOVariableJSF> variableEntry : variablesSelected.entrySet()) {
                if (!variables.containsKey(variableEntry.getValue().getVariable().getGroup()))
                    variables.put(variableEntry.getValue().getVariable().getGroup(), new LinkedList<VOVariableJSF>());
                variables.get(variableEntry.getValue().getVariable().getGroup()).add(variableEntry.getValue());
            }
            return variables;
        }

        /**
         *
         * @return
         */
        public Map<Long, PlateformeVO> getPlateformesSelected() {
            return plateformesSelected;
        }

        /**
         *
         * @return
         */
        public Boolean getPlateformeStepIsValid() {
            return !plateformesSelected.isEmpty();
        }

        /**
         *
         * @return
         */
        public Map<Long, VOVariableJSF> getVariablesSelected() {
            return variablesSelected;
        }

        /**
         *
         * @return
         */
        public Boolean getVariableStepIsValid() {
            return !variablesSelected.isEmpty() && getDateStepIsValid();
        }

        private void initDatesRequestParam() {
            datesRequestParam = new DatesRequestParamVO(localizationManager);
        }

        /**
         *
         * @param commentExtraction
         */
        public void setCommentExtraction(String commentExtraction) {
            this.commentExtraction = commentExtraction;
        }

        /**
         *
         * @return
         */
        public boolean getFormIsValid() {
            return getDephtDatatypeStepIsValid() && affichage != null;
        }

    }

    /**
     *
     */
    public class PlateformeVO {

        private Plateforme plateforme;
        private String localizedPlateformeName;
        private String localizedSiteName;
        private Boolean selected = false;

        /**
         *
         * @param plateforme
         */
        public PlateformeVO(Plateforme plateforme) {
            super();
            this.plateforme = plateforme;
            if (plateforme != null) {
                this.localizedPlateformeName = getPropertiesPlatformNames().getProperty(plateforme.getNom());
                if (Strings.isNullOrEmpty(this.localizedPlateformeName))
                    this.localizedPlateformeName = plateforme.getNom();
                this.localizedSiteName = getPropertiesSiteNames().getProperty(plateforme.getSite().getNom());
                if (Strings.isNullOrEmpty(this.localizedSiteName))
                    this.localizedSiteName = plateforme.getSite().getNom();
            }
        }

        /**
         *
         * @return
         */
        public Plateforme getPlateforme() {
            return plateforme;
        }

        /**
         *
         * @return
         */
        public Boolean getSelected() {
            return selected;
        }

        /**
         *
         * @param selected
         */
        public void setSelected(Boolean selected) {
            this.selected = selected;
        }

        /**
         *
         * @return
         */
        public String getLocalizedPlateformeName() {
            return localizedPlateformeName;
        }

        /**
         *
         * @param localizedPlateformeName
         */
        public void setLocalizedPlateformeName(String localizedPlateformeName) {
            this.localizedPlateformeName = localizedPlateformeName;
        }

        /**
         *
         * @return
         */
        public String getLocalizedSiteName() {
            return localizedSiteName;
        }

        /**
         *
         * @param localizedSiteName
         */
        public void setLocalizedSiteName(String localizedSiteName) {
            this.localizedSiteName = localizedSiteName;
        }
    }

    /**
     *
     */
    public class ProjectVO {

        private Projet projet;
        private String localizedProjetName;
        private Map<Long, PlateformeVO> plateformes = new HashMap<Long, PlateformeVO>();

        /**
         *
         * @param projet
         * @param plateformes
         */
        public ProjectVO(Projet projet, List<Plateforme> plateformes) {
            super();
            this.projet = projet;
            if (projet != null) {
                this.localizedProjetName = getPropertiesProjectsNames().getProperty(projet.getNom());
                if (Strings.isNullOrEmpty(this.localizedProjetName))
                    this.localizedProjetName = projet.getNom();
            }
            for (Plateforme plateforme : plateformes) {
                this.plateformes.put(plateforme.getId(), new PlateformeVO(plateforme));
            }

        }

        /**
         *
         * @return
         */
        public Map<Long, PlateformeVO> getPlateformes() {
            return plateformes;
        }

        /**
         *
         * @return
         */
        public Projet getProjet() {
            return projet;
        }

        /**
         *
         * @return
         */
        public String getLocalizedProjetName() {
            return localizedProjetName;
        }

        /**
         *
         * @param localizedProjetName
         */
        public void setLocalizedProjetName(String localizedProjetName) {
            this.localizedProjetName = localizedProjetName;
        }
    }

    /**
     *
     */
    public class TreeNodeGroupeVariable {

        private GroupeVariableVO groupeVariable;
        private String localizedGroupeName;
        private List<TreeNodeGroupeVariable> groupesVariables = Lists.newArrayList();
        private Boolean expanded = false;

        private List<VOVariableJSF> variables = Lists.newArrayList();

        /**
         *
         * @param groupeVariable
         */
        public TreeNodeGroupeVariable(GroupeVariableVO groupeVariable) {
            super();
            this.groupeVariable = groupeVariable;
            this.localizedGroupeName = groupeVariable.getNom();
        }

        /**
         *
         * @return
         */
        public Boolean getExpanded() {
            return expanded;
        }

        /**
         *
         * @return
         */
        public List<TreeNodeGroupeVariable> getGroupesVariables() {
            if (groupesVariables.isEmpty()) {

                if (groupeVariable.getChildren() != null && !groupeVariable.getChildren().isEmpty()) {
                    for (GroupeVariableVO groupeVariableVO : groupeVariable.getChildren()) {
                        groupesVariables.add(new TreeNodeGroupeVariable(groupeVariableVO));
                    }
                }
            }
            return groupesVariables;
        }

        /**
         *
         * @return
         */
        public GroupeVariableVO getGroupeVariable() {
            return groupeVariable;
        }

        /**
         *
         * @return
         */
        public List<VOVariableJSF> getVariables() {
            if (variables.isEmpty()) {
                if (groupeVariable.getVariables() != null && !groupeVariable.getVariables().isEmpty() && !parametersRequest.plateformesSelected.isEmpty()) {
                    for (PlateformeVO plateforme : parametersRequest.plateformesSelected.values()) {
                        for (VariableVO variableVO : groupeVariable.getVariables()) {
                            if (securityContext.isRoot()
                                    || (groupeVariable.getNom().equals(localizationManager.getMessage(BUNDLE_JSF, "PROPERTY_MSG_PP")) && securityContext.matchPrivilege(
                                            String.format("*/%s/*/production_primaire/%s", plateforme.getPlateforme().getSite().getCode(), variableVO.getCode()), Role.ROLE_EXTRACTION, ISecurityDatasetManager.PRIVILEGE_TYPE_DATASET))
                                    || (groupeVariable.getNom().equals(localizationManager.getMessage(BUNDLE_JSF, "PROPERTY_MSG_CHLORO")) && securityContext.matchPrivilege(
                                            String.format("*/%s/*/chlorophylle/%s", plateforme.getPlateforme().getSite().getCode(), variableVO.getCode()), Role.ROLE_EXTRACTION, ISecurityDatasetManager.PRIVILEGE_TYPE_DATASET))
                                    || (groupeVariable.getNom().equals(localizationManager.getMessage(BUNDLE_JSF, "PROPERTY_MSG_TRANSP")) && securityContext.matchPrivilege(
                                            String.format("*/%s/*/conditions_prelevements/%s", plateforme.getPlateforme().getSite().getCode(), variableVO.getCode()), Role.ROLE_EXTRACTION, ISecurityDatasetManager.PRIVILEGE_TYPE_DATASET)))
                            {
                                VOVariableJSF voVariableJSF = new VOVariableJSF(variableVO);
                                Boolean isIn = false;
                                for (VOVariableJSF variable : variables) {
                                    if (voVariableJSF.getVariable().getCode().equals(variable.getVariable().getCode())) {
                                        isIn = true;
                                        break;
                                    }
                                }
                                if (!isIn) {
                                    variables.add(voVariableJSF);
                                }
                            }
                        }
                    }
                }
            }
            return variables;
        }

        /**
         *
         * @param expanded
         */
        public void setExpanded(Boolean expanded) {
            this.expanded = expanded;
        }

        /**
         *
         * @return
         */
        public String getLocalizedGroupeName() {
            return localizedGroupeName;
        }

        /**
         *
         * @param localizedGroupeName
         */
        public void setLocalizedGroupeName(String localizedGroupeName) {
            this.localizedGroupeName = localizedGroupeName;
        }
    }

    /**
     *
     */
    public class VOVariableJSF {

        private VariableVO variable;
        private String localizedVariableName;
        private Boolean selected = false;

        /**
         *
         * @param variable
         */
        public VOVariableJSF(VariableVO variable) {
            super();
            this.variable = variable;
            if (variable != null) {
                this.localizedVariableName = getPropertiesVariableNames().getProperty(variable.getNom());
                if (Strings.isNullOrEmpty(this.localizedVariableName))
                    this.localizedVariableName = variable.getNom();
            }

        }

        /**
         *
         * @return
         */
        public Boolean getSelected() {
            return selected;
        }

        /**
         *
         * @return
         */
        public VariableVO getVariable() {
            return variable;
        }

        /**
         *
         * @param selected
         */
        public void setSelected(Boolean selected) {
            this.selected = selected;
        }

        /**
         *
         * @param variable
         */
        public void setVariable(VariableVO variable) {
            this.variable = variable;
        }

        /**
         *
         * @return
         */
        public String getLocalizedVariableName() {
            return localizedVariableName;
        }

        /**
         *
         * @param localizedVariableName
         */
        public void setLocalizedVariableName(String localizedVariableName) {
            this.localizedVariableName = localizedVariableName;
        }
    }

    private static final String RULE_NAVIGATION_JSF = "ppChlorophylle";

    private static final long serialVersionUID = 1L;

    private static final String DATATYPE_TRANSP = "conditions_prelevements";
    private static final String THEME_PPCHLORO = "compartiments_biologiques";
    private static final String THEME_TRANSP = "conditions_generales";
    private static final String PATH_PATTERN = "%s/%s/%s/%s/%s";

    private Map<Long, ProjectVO> projectsAvailables = new HashMap<Long, ProjectVO>();

    /**
     *
     */
    @ManagedProperty(value = "#{ppDatatypeManager}")
    protected IPPDatatypeManager PPDatatypeManager;

    /**
     *
     */
    @ManagedProperty(value = "#{chlorophylleDatatypeManager}")
    protected IChlorophylleDatatypeManager chlorophylleDatatypeManager;

    /**
     *
     */
    @ManagedProperty(value = "#{conditionGeneraleDatatypeManager}")
    protected IConditionGeneraleDatatypeManager conditionGeneraleDatatypeManager;

    /**
     *
     */
    @ManagedProperty(value = "#{notificationsManager}")
    protected INotificationsManager notificationsManager;

    /**
     *
     */
    @ManagedProperty(value = "#{extractionManager}")
    protected IExtractionManager extractionManager;

    /**
     *
     */
    @ManagedProperty(value = "#{securityContext}")
    protected ISecurityContext securityContext;

    /**
     *
     */
    @ManagedProperty(value = "#{localizationManager}")
    protected ILocalizationManager localizationManager;

    /**
     *
     */
    @ManagedProperty(value = "#{fileCompManager}")
    protected IFileCompManager fileCompManager;

    private Properties propertiesProjectsNames;
    private Properties propertiesPlatformNames;
    private Properties propertiesSiteNames;
    private Properties propertiesVariableNames;

    private String BUNDLE_JSF = "org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.jsf.ppchlorotransp-jsf";

    private UITree treeListVariables;

    private UITreeNode treeNodeGroupeVariable;

    private Long idPlateformeSelected;
    private Long idProjectSelected;

    private Long idVariableSelected;

    /** The affichage @link(String). */
    private String affichage = null;

    private TreeNodeGroupeVariable treeNodeGroupeVariableSelected;

    private List<TreeNodeGroupeVariable> rootNodes = new ArrayList<TreeNodeGroupeVariable>();

    private VOVariableJSF variableSelected = new VOVariableJSF(null);

    List<VOVariableJSF> variablesSelected = new LinkedList<VOVariableJSF>();

    private ParametersRequest parametersRequest = new ParametersRequest();

    /**
     *
     */
    public UIBeanPPChlorophylle() {}

    private String addAllGroupVariables(TreeNodeGroupeVariable groupeVariable) {
        groupeVariable.setExpanded(true);
        if (groupeVariable.getGroupeVariable().getChildren().isEmpty()) {
            for (VOVariableJSF variable : groupeVariable.getVariables()) {
                getParametersRequest().getVariablesSelected().put(variable.getVariable().getId(), variable);
                variable.setSelected(true);

            }
        } else {
            for (TreeNodeGroupeVariable groupeVariables : groupeVariable.getGroupesVariables()) {

                addAllGroupVariables(groupeVariables);
            }
        }
        return null;
    }

    /**
     *
     * @return
     */
    public String addAllVariables() {
        Map<Long, VOVariableJSF> variablesSelected = getParametersRequest().getVariablesSelected();
        variablesSelected.clear();

        for (TreeNodeGroupeVariable groupeVariable : rootNodes) {
            addAllGroupVariables(groupeVariable);
        }
        return null;
    }

    /**
     *
     * @return
     * @throws BusinessException
     */
    public String changeProject() throws BusinessException {
        getParametersRequest().getListPlateformesSelected().clear();
        for (ProjectVO projectSelected : projectsAvailables.values()) {
            for (PlateformeVO plateformeSelected : projectSelected.getPlateformes().values()) {
                plateformeSelected.setSelected(false);
            }
        }
        return null;
    }

    /**
     *
     * @return
     * @throws BusinessException
     */
    public String getProjetSelected() throws BusinessException {
        String localizedProjetSelected = null;
        if (idProjectSelected != null) {
            String nom = projectsAvailables.get(idProjectSelected).getProjet().getNom();
            localizedProjetSelected = getPropertiesProjectsNames().getProperty(nom);
            if (Strings.isNullOrEmpty(localizedProjetSelected))
                localizedProjetSelected = nom;
            return localizedProjetSelected;
        }
        return localizedProjetSelected;
    }

    private void createOrUpdateGroupVariablesAvailables() throws BusinessException, ParseException {

        parametersRequest.getVariablesSelected().clear();
        rootNodes.clear();

        if (!parametersRequest.plateformesSelected.isEmpty() && parametersRequest.getDateStepIsValid()) {
            List<VariableGLACPE> variablesPP = PPDatatypeManager.retrievePPAvailablesVariables(new LinkedList<Long>(parametersRequest.plateformesSelected.keySet()), parametersRequest.getFirstStartDate(), parametersRequest.getLastEndDate());
            GroupeVariableVO groupevariablesPPVO = new GroupeVariableVO();
            groupevariablesPPVO.setNom(localizationManager.getMessage(BUNDLE_JSF, "PROPERTY_MSG_PP"));
            for (VariableGLACPE variable : variablesPP) {
                groupevariablesPPVO.getVariables().add(new VariableVO(variable, PPChloroTranspParameter.PRODUCTION_PRIMAIRE));
            }
            sortVariables(groupevariablesPPVO);
            TreeNodeGroupeVariable treeNodeGroupeVariablePP = new TreeNodeGroupeVariable(groupevariablesPPVO);
            if (!variablesPP.isEmpty())
                rootNodes.add(treeNodeGroupeVariablePP);

            List<VariableGLACPE> variablesChloro = chlorophylleDatatypeManager.retrieveChlorophylleAvailablesVariables(new LinkedList<Long>(parametersRequest.plateformesSelected.keySet()), parametersRequest.getFirstStartDate(),
                    parametersRequest.getLastEndDate());
            GroupeVariableVO groupevariablesChloroVO = new GroupeVariableVO();
            groupevariablesChloroVO.setNom(localizationManager.getMessage(BUNDLE_JSF, "PROPERTY_MSG_CHLORO"));
            for (VariableGLACPE variable : variablesChloro) {
                groupevariablesChloroVO.getVariables().add(new VariableVO(variable, PPChloroTranspParameter.CHLOROPHYLLE));
            }
            sortVariables(groupevariablesChloroVO);
            TreeNodeGroupeVariable treeNodeGroupeVariableChloro = new TreeNodeGroupeVariable(groupevariablesChloroVO);
            if (!variablesChloro.isEmpty())
                rootNodes.add(treeNodeGroupeVariableChloro);

            List<VariableGLACPE> variablesTransparence = conditionGeneraleDatatypeManager.retrieveTransparenceAvailablesVariables(new LinkedList<Long>(parametersRequest.plateformesSelected.keySet()), parametersRequest.getFirstStartDate(),
                    parametersRequest.getLastEndDate());
            GroupeVariableVO groupevariablesTransparenceVO = new GroupeVariableVO();
            groupevariablesTransparenceVO.setNom(localizationManager.getMessage(BUNDLE_JSF, "PROPERTY_MSG_TRANSP"));
            for (VariableGLACPE variable : variablesTransparence) {
                groupevariablesTransparenceVO.getVariables().add(new VariableVO(variable, PPChloroTranspParameter.TRANSPARENCE));
            }
            sortVariables(groupevariablesTransparenceVO);
            TreeNodeGroupeVariable treeNodeGroupeVariableTransparence = new TreeNodeGroupeVariable(groupevariablesTransparenceVO);
            if (!variablesTransparence.isEmpty())
                rootNodes.add(treeNodeGroupeVariableTransparence);
        }
    }

    private void sortVariables(GroupeVariableVO groupeVariableVO) {
        Collections.sort(groupeVariableVO.getVariables(), new Comparator<VariableVO>() {

            @Override
            public int compare(VariableVO o1, VariableVO o2) {
                if (o1.getOrdreAffichageGroupe() == null) {
                    o1.setOrdreAffichageGroupe(0);
                }
                if (o2.getOrdreAffichageGroupe() == null) {
                    o2.setOrdreAffichageGroupe(0);
                }
                return o1.getOrdreAffichageGroupe().toString().concat(o1.getCode()).compareTo(o2.getOrdreAffichageGroupe().toString().concat(o2.getCode()));
            }
        });
    }

    /**
     *
     * @return
     */
    public String deployNodeGroupeVariable() {

        expandAndSelectGroupeVariable(treeNodeGroupeVariableSelected);

        return null;
    }

    /**
     *
     * @return
     */
    public String deSelectVariable() {

        for (TreeNodeGroupeVariable groupeVariable : rootNodes) {
            deSelectVariable(groupeVariable);
        }
        return null;
    }

    private String deSelectVariable(TreeNodeGroupeVariable groupeVariable) {
        if (groupeVariable.getGroupeVariable().getChildren().isEmpty()) {
            for (VOVariableJSF variable : groupeVariable.getVariables()) {
                if (variable.getVariable().getId().equals(idVariableSelected)) {
                    variableSelected = variable;
                    selectVariable(variableSelected);
                    return null;
                }
            }
        } else {
            for (TreeNodeGroupeVariable groupeVariables : groupeVariable.getGroupesVariables()) {

                deSelectVariable(groupeVariables);
            }
        }
        return null;
    }

    private void enableOrDisableLeaf(Boolean conditionDisabling, VOVariableJSF variable) {
        Map<Long, VOVariableJSF> variablesSelecteds = parametersRequest.getVariablesSelected();

        if (conditionDisabling) {
            variablesSelecteds.remove(variable.getVariable().getId());
            variable.setSelected(false);
        } else {
            variablesSelecteds.put(variable.getVariable().getId(), variable);
            variable.setSelected(true);
        }
    }

    private void expandAndSelectGroupeVariable(TreeNodeGroupeVariable treeNodeGroupeVariable) {
        treeNodeGroupeVariable.setExpanded(true);
        if (treeNodeGroupeVariable.getGroupesVariables() == null || treeNodeGroupeVariable.getGroupesVariables().isEmpty()) {
            for (VOVariableJSF variable : treeNodeGroupeVariable.getVariables()) {
                enableOrDisableLeaf(variable.getSelected(), variable);
            }

        } else {
            for (TreeNodeGroupeVariable childTreeNodeGroupeVariable : treeNodeGroupeVariable.getGroupesVariables()) {
                expandAndSelectGroupeVariable(childTreeNodeGroupeVariable);
            }
        }
    }

    private List<String> getPathes(List<ProjetSite> projetSites, List<VariableVO> variables, String theme, String datatype) {
        String sitePath, projet, path;
        List<String> pathes = new LinkedList<>();
        for (ProjetSite projetSite : projetSites) {
            sitePath = projetSite.getSite().getPath();
            projet = projetSite.getProjet().getCode();
            for (VariableVO variable : variables) {
                path = String.format(PATH_PATTERN, projet, sitePath, theme, datatype, variable.getCode());
                if (!pathes.contains(path)) {
                    pathes.add(path);
                }
            }
        }
        return pathes;
    }

    /**
     *
     * @return
     * @throws BusinessException
     */
    public String extract() throws BusinessException {
        Map<String, Object> metadatasMap = new HashMap<String, Object>();
        List<org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO> metadatasPlateformesVos = new LinkedList<org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO>();
        List<Long> projectSiteIds = new LinkedList<Long>();
        List<ProjetSite> projetSites = new LinkedList<ProjetSite>();
        for (PlateformeVO plateforme : getParametersRequest().getPlateformesSelected().values()) {
            org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO plateformeVO = new org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO(plateforme.getPlateforme());
            for (ProjetSite projetSite : projectsAvailables.get(idProjectSelected).getProjet().getProjetsSites()) {
                if (projetSite.getSite().getId().equals(plateforme.getPlateforme().getSite().getId())) {
                    plateformeVO.setProjet(new ProjetSiteVO(projetSite));
                    projectSiteIds.add(projetSite.getId());
                    projetSites.add(projetSite);
                    break;
                }
            }
            metadatasPlateformesVos.add(plateformeVO);
        }

        metadatasMap.put(PlateformeVO.class.getSimpleName(), metadatasPlateformesVos);
        metadatasMap.put(ProjetSite.class.getSimpleName(), projectSiteIds);

        Map<String, List<VariableVO>> mapVO = new HashMap<String, List<VariableVO>>();
        List<String> pathes = new LinkedList<String>();
        for (String datatype : getParametersRequest().getMapVariablesSelected().keySet()) {
            List<VariableVO> variablesVO = new LinkedList<VariableVO>();
            for (VOVariableJSF variable : getParametersRequest().getMapVariablesSelected().get(datatype)) {
                variablesVO.add(variable.getVariable());
            }
            mapVO.put(datatype, variablesVO);
            if (!datatype.equals(PPChloroTranspParameter.TRANSPARENCE)) {
                pathes.addAll(getPathes(projetSites, variablesVO, THEME_PPCHLORO, Utils.createCodeFromString(datatype)));
            } else {
                pathes.addAll(getPathes(projetSites, variablesVO, THEME_TRANSP, DATATYPE_TRANSP));
            }
        }
        metadatasMap.put(VariableVO.class.getSimpleName(), mapVO);

        List<FileComp> listFiles = new LinkedList<FileComp>();
        try {
            if (!parametersRequest.plateformesSelected.isEmpty() && parametersRequest.getFormIsValid()) {
                IntervalDate intervalDate;
                intervalDate = new IntervalDate(parametersRequest.getFirstStartDate(), parametersRequest.getLastEndDate(), DateUtil.getSimpleDateFormatDateLocale());

                List<IntervalDate> intervalsDate = new LinkedList<>();
                intervalsDate.add(intervalDate);
                final List<ItemFile> files = fileCompManager.getFileCompFromPathesAndIntervalsDate(pathes, intervalsDate);
                for (final ItemFile file : files) {
                    if (file.getMandatory()) {
                        listFiles.add(file.getFileComp());
                    }
                }
            }
        } catch (BadExpectedValueException e) {
            throw new BusinessException("bad file", e);
        } catch (ParseException e) {
            throw new BusinessException("bad file name", e);
        }
        metadatasMap.put(FileComp.class.getSimpleName(), listFiles);

        metadatasMap.put(DatasRequestParamVO.class.getSimpleName(), getParametersRequest().getDatasRequestParam().clone());
        metadatasMap.put(DepthRequestParamVO.class.getSimpleName(), getParametersRequest().getDepthRequestParam().clone());

        getParametersRequest().getDatesRequestParam().getDatesYearsDiscretsFormParam().setPeriods(getParametersRequest().getDatesRequestParam().getNonPeriodsYearsContinuous());
        getParametersRequest().getDatesRequestParam().getDatesYearsRangeFormParam().setPeriods(getParametersRequest().getDatesRequestParam().getNonPeriodsYearsContinuous());

        metadatasMap.put(DatesRequestParamVO.class.getSimpleName(), getParametersRequest().getDatesRequestParam().clone());
        metadatasMap.put(IExtractionManager.KEYMAP_COMMENTS, new String(getParametersRequest().getCommentExtraction()));
        IParameter parameters = new PPChloroTranspParameter(metadatasMap);
        Integer aff = Integer.parseInt(affichage);
        extractionManager.extract(parameters, aff);
        return null;
    }

    /**
     *
     * @return
     */
    public Long getIdProjectSelected() {
        return idProjectSelected;
    }

    /**
     *
     * @return
     */
    public Long getIdVariableSelected() {
        return idVariableSelected;
    }

    /**
     *
     * @param variable
     * @return
     */
    public Boolean getIsSelected(VariableVO variable) {
        if (getParametersRequest().getVariablesSelected().containsValue(variable))
            return true;
        else
            return false;
    }

    /**
     *
     * @return
     */
    @Override
    public boolean getIsStepValid() {
        if (getStep() == 1) {
            return getParametersRequest().getPlateformeStepIsValid();
        } else if (getStep() == 2) {
            return getParametersRequest().getDateStepIsValid();
        } else if (getStep() == 3) {
            return getParametersRequest().getVariableStepIsValid();
        } else if (getStep() == 4) {
            return getParametersRequest().getDephtDatatypeStepIsValid();
        }
        return false;
    }

    /**
     *
     * @return
     */
    public ParametersRequest getParametersRequest() {
        return parametersRequest;
    }

    /**
     *
     * @return
     * @throws BusinessException
     */
    public Map<Long, ProjectVO> getProjectsAvailables() throws BusinessException {
        if (projectsAvailables.isEmpty()) {
            List<Projet> projets = PPDatatypeManager.retrievePPAvailablesProjets();
            for (Projet projet : projets) {
                for (ProjetSite projetSite : projet.getProjetsSites()) {
                    List<Plateforme> plateformes = Lists.newArrayList();
                    for (Plateforme plateforme : PPDatatypeManager.retrievePPAvailablesPlateformesByProjetSiteId(projetSite.getId())) {
                        if (!plateformes.contains(plateforme)
                                && (securityContext.isRoot() || securityContext.matchPrivilege(String.format("%s/%s/*/production_primaire/*", projetSite.getProjet().getCode(), projetSite.getSite().getCode()), Role.ROLE_EXTRACTION,
                                        ISecurityDatasetManager.PRIVILEGE_TYPE_DATASET)))
                        {
                            plateformes.add(plateforme);
                        }
                    }
                    if (!plateformes.isEmpty()) {
                        ProjectVO projectVO;
                        if (projectsAvailables.containsKey(projet.getId())) {
                            projectVO = projectsAvailables.get(projet.getId());
                            for (Plateforme plateforme : plateformes) {
                                if (!projectVO.getPlateformes().containsKey(plateforme.getId()))
                                    projectVO.getPlateformes().put(plateforme.getId(), new PlateformeVO(plateforme));
                            }
                        } else {
                            projectVO = new ProjectVO(projetSite.getProjet(), plateformes);
                            projectsAvailables.put(projet.getId(), projectVO);
                        }
                    }
                }
            }
            projets = chlorophylleDatatypeManager.retrieveChloroAvailablesProjets();
            for (Projet projet : projets) {
                for (ProjetSite projetSite : projet.getProjetsSites()) {
                    List<Plateforme> plateformes = Lists.newArrayList();
                    for (Plateforme plateforme : chlorophylleDatatypeManager.retrieveChloroAvailablesPlateformesByProjetSiteId(projetSite.getId())) {
                        if (!plateformes.contains(plateforme)
                                && (securityContext.isRoot() || securityContext.matchPrivilege(String.format("%s/%s/*/chlorophylle/*", projetSite.getProjet().getCode(), projetSite.getSite().getCode()), Role.ROLE_EXTRACTION,
                                        ISecurityDatasetManager.PRIVILEGE_TYPE_DATASET)))
                        {
                            plateformes.add(plateforme);
                        }
                    }
                    if (!plateformes.isEmpty()) {
                        ProjectVO projectVO;
                        if (projectsAvailables.containsKey(projet.getId())) {
                            projectVO = projectsAvailables.get(projet.getId());
                            for (Plateforme plateforme : plateformes) {
                                if (!projectVO.getPlateformes().containsKey(plateforme.getId()))
                                    projectVO.getPlateformes().put(plateforme.getId(), new PlateformeVO(plateforme));
                            }
                        } else {
                            projectVO = new ProjectVO(projetSite.getProjet(), plateformes);
                            projectsAvailables.put(projet.getId(), projectVO);
                        }
                    }
                }
            }
            projets = conditionGeneraleDatatypeManager.retrieveTransparenceAvailablesProjets();
            for (Projet projet : projets) {
                for (ProjetSite projetSite : projet.getProjetsSites()) {
                    List<Plateforme> plateformes = Lists.newArrayList();
                    for (Plateforme plateforme : conditionGeneraleDatatypeManager.retrieveConditionGeneraleAvailablesPlateformesByProjetSiteId(projetSite.getId())) {
                        if (!plateformes.contains(plateforme)
                                && (securityContext.isRoot() || securityContext.matchPrivilege(String.format("%s/%s/*/conditions_prelevements/*", projetSite.getProjet().getCode(), projetSite.getSite().getCode()), Role.ROLE_EXTRACTION,
                                        ISecurityDatasetManager.PRIVILEGE_TYPE_DATASET)))
                        {
                            plateformes.add(plateforme);
                        }
                    }
                    if (!plateformes.isEmpty()) {
                        ProjectVO projectVO;
                        if (projectsAvailables.containsKey(projet.getId())) {
                            projectVO = projectsAvailables.get(projet.getId());
                            for (Plateforme plateforme : plateformes) {
                                if (!projectVO.getPlateformes().containsKey(plateforme.getId()))
                                    projectVO.getPlateformes().put(plateforme.getId(), new PlateformeVO(plateforme));
                            }
                        } else {
                            projectVO = new ProjectVO(projetSite.getProjet(), plateformes);
                            projectsAvailables.put(projet.getId(), projectVO);
                        }
                    }
                }
            }
        }
        return projectsAvailables;

    }

    /**
     *
     * @return
     * @throws BusinessException
     * @throws ParseException
     */
    public List<TreeNodeGroupeVariable> getRootNodes() throws BusinessException, ParseException {
        if (rootNodes == null) {
            createOrUpdateGroupVariablesAvailables();
        }
        return rootNodes;
    }

    /**
     *
     * @return
     */
    public UITree getTreeListVariables() {
        return treeListVariables;
    }

    /**
     *
     * @return
     */
    public UITreeNode getTreeNodeGroupeVariable() {
        return treeNodeGroupeVariable;
    }

    /**
     *
     * @return
     */
    public VOVariableJSF getVariableSelected() {
        return variableSelected;
    }

    /**
     *
     * @return
     */
    public String navigate() {
        return RULE_NAVIGATION_JSF;
    }

    /**
     *
     * @return
     */
    @Override
    public String nextStep() {
        super.nextStep();
        switch (getStep()) {
            case 3 :
                try {
                    createOrUpdateGroupVariablesAvailables();
                } catch (BusinessException e) {
                    rootNodes.clear();
                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                break;
            default :
                break;
        }
        return null;
    }

    /**
     *
     * @return
     * @throws BusinessException
     * @throws ParseException
     */
    public String removeAllVariables() throws BusinessException, ParseException {
        createOrUpdateGroupVariablesAvailables();
        return null;
    }

    /**
     *
     * @return
     * @throws BusinessException
     */
    public String selectPlateforme() throws BusinessException {
        ProjectVO projectSelected = projectsAvailables.get(idProjectSelected);
        PlateformeVO plateformeSelected = projectSelected.getPlateformes().get(idPlateformeSelected);
        if (plateformeSelected.getSelected()) {
            getParametersRequest().getPlateformesSelected().remove(idPlateformeSelected);
            plateformeSelected.setSelected(false);
        } else {
            getParametersRequest().getPlateformesSelected().put(idPlateformeSelected, plateformeSelected);
            plateformeSelected.setSelected(true);
        }

        return null;
    }

    /**
     *
     * @return
     */
    public String selectVariable() {

        selectVariable(variableSelected);
        return null;
    }

    /**
     *
     * @param variable
     */
    public void selectVariable(VOVariableJSF variable) {
        Map<Long, VOVariableJSF> variablesSelecteds = getParametersRequest().getVariablesSelected();
        enableOrDisableLeaf(variablesSelecteds.containsKey(variable.getVariable().getId()), variable);
    }

    /**
     *
     * @param chlorophylleDatatypeManager
     */
    public void setChlorophylleDatatypeManager(IChlorophylleDatatypeManager chlorophylleDatatypeManager) {
        this.chlorophylleDatatypeManager = chlorophylleDatatypeManager;
    }

    /**
     *
     * @param extractionManager
     */
    public void setExtractionManager(IExtractionManager extractionManager) {
        this.extractionManager = extractionManager;
    }

    /**
     *
     * @param idPlateformeSelected
     */
    public void setIdPlateformeSelected(Long idPlateformeSelected) {
        this.idPlateformeSelected = idPlateformeSelected;
    }

    /**
     *
     * @param idProjectSelected
     */
    public void setIdProjectSelected(Long idProjectSelected) {
        this.idProjectSelected = idProjectSelected;
    }

    /**
     *
     * @param idVariableSelected
     */
    public void setIdVariableSelected(Long idVariableSelected) {
        this.idVariableSelected = idVariableSelected;
    }

    /**
     *
     * @param PPDatatypeManager
     */
    public void setPPDatatypeManager(IPPDatatypeManager PPDatatypeManager) {
        this.PPDatatypeManager = PPDatatypeManager;
    }

    /**
     *
     * @param conditionGeneraleDatatypeManager
     */
    public void setConditionGeneraleDatatypeManager(IConditionGeneraleDatatypeManager conditionGeneraleDatatypeManager) {
        this.conditionGeneraleDatatypeManager = conditionGeneraleDatatypeManager;
    }

    /**
     *
     * @param notificationsManager
     */
    public void setNotificationsManager(INotificationsManager notificationsManager) {
        this.notificationsManager = notificationsManager;
    }

    /**
     *
     * @param securityContext
     */
    public void setSecurityContext(ISecurityContext securityContext) {
        this.securityContext = securityContext;
    }

    /**
     *
     * @param treeListVariables
     */
    public void setTreeListVariables(UITree treeListVariables) {
        this.treeListVariables = treeListVariables;
    }

    /**
     *
     * @param treeNodeGroupeVariable
     */
    public void setTreeNodeGroupeVariable(UITreeNode treeNodeGroupeVariable) {
        this.treeNodeGroupeVariable = treeNodeGroupeVariable;
    }

    /**
     *
     * @param treeNodeGroupeVariableSelected
     */
    public void setTreeNodeGroupeVariableSelected(TreeNodeGroupeVariable treeNodeGroupeVariableSelected) {
        this.treeNodeGroupeVariableSelected = treeNodeGroupeVariableSelected;
    }

    /**
     *
     * @param variableSelected
     */
    public void setVariableSelected(VOVariableJSF variableSelected) {
        this.variableSelected = variableSelected;
    }

    /**
     *
     * @return
     */
    public Properties getPropertiesProjectsNames() {
        if (this.propertiesProjectsNames == null)
            this.setPropertiesProjectsNames(localizationManager.newProperties(Projet.NAME_TABLE, "nom"));
        return propertiesProjectsNames;
    }

    /**
     *
     * @param propertiesProjectsNames
     */
    public void setPropertiesProjectsNames(Properties propertiesProjectsNames) {
        this.propertiesProjectsNames = propertiesProjectsNames;
    }

    /**
     *
     * @return
     */
    public Properties getPropertiesPlatformNames() {
        if (this.propertiesPlatformNames == null)
            this.setPropertiesPlatformNames(localizationManager.newProperties(Plateforme.TABLE_NAME, "nom"));
        return propertiesPlatformNames;
    }

    /**
     *
     * @param propertiesPlatformNames
     */
    public void setPropertiesPlatformNames(Properties propertiesPlatformNames) {
        this.propertiesPlatformNames = propertiesPlatformNames;
    }

    /**
     *
     * @param localizationManager
     */
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    /**
     *
     * @return
     */
    public Properties getPropertiesSiteNames() {
        if (this.propertiesSiteNames == null)
            this.setPropertiesSiteNames(localizationManager.newProperties(Site.NAME_ENTITY_JPA, "nom"));
        return propertiesSiteNames;
    }

    /**
     *
     * @param propertiesSiteNames
     */
    public void setPropertiesSiteNames(Properties propertiesSiteNames) {
        this.propertiesSiteNames = propertiesSiteNames;
    }

    /**
     *
     * @return
     */
    public Properties getPropertiesVariableNames() {
        if (this.propertiesVariableNames == null)
            this.setPropertiesVariableNames(localizationManager.newProperties(Variable.NAME_ENTITY_JPA, "nom"));
        return propertiesVariableNames;
    }

    /**
     *
     * @param propertiesVariableNames
     */
    public void setPropertiesVariableNames(Properties propertiesVariableNames) {
        this.propertiesVariableNames = propertiesVariableNames;
    }

    /**
     *
     * @param fileCompManager
     */
    public void setFileCompManager(IFileCompManager fileCompManager) {
        this.fileCompManager = fileCompManager;
    }

    /**
     *
     * @return
     */
    public String getAffichage() {
        return affichage;
    }

    /**
     *
     * @param affichage
     */
    public void setAffichage(String affichage) {
        this.affichage = affichage;
    }
}
