package org.inra.ecoinfo.glacpe.identification.jsf;

import java.io.Serializable;
import java.util.Date;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlInputTextarea;

import org.inra.ecoinfo.glacpe.identification.entity.RightRequestGLACPE;
import org.inra.ecoinfo.identification.IUsersManager;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.security.ISecurityContext;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.richfaces.component.UIPanel;

/**
 *
 * @author ptcherniati
 */
@ManagedBean(name = "uiRequestRights")
@ViewScoped
public class UIBeanRequestRights implements Serializable {

    private static final long serialVersionUID = 1L;

    /** The Constant BUNDLE_SOURCE_PATH @link(String). */
    private static final String BUNDLE_SOURCE_PATH_RIGHTS = "org.inra.ecoinfo.identification.impl.messages";

    /** The Constant PROPERTY_MSG_NEW_RIGHT_REQUEST @link(String). */
    private static final String PROPERTY_MSG_NEW_RIGHT_REQUEST = "PROPERTY_MSG_NEW_RIGHT_REQUEST";

    /** The Constant RULE_NAVIGATION_REQUEST_RIGHTS_JSF @link(String). */
    private static final String RULE_NAVIGATION_REQUEST_RIGHTS_JSF = "requestRights";

    /** The Constant STRING_EMPTY @link(String). */
    private static final String STRING_EMPTY = "";

    /** The security context @link(ISecurityContext). */
    @ManagedProperty(value = "#{securityContext}")
    protected ISecurityContext securityContext;

    /** The users manager @link(IUsersManager). */
    @ManagedProperty(value = "#{usersManager}")
    protected IUsersManager usersManager;

    /** The localization manager @link(ILocalizationManager). */
    @ManagedProperty(value = "#{localizationManager}")
    protected ILocalizationManager localizationManager;

    /** The ui form @link(HtmlPanelGroup). */
    private UIPanel uiForm;

    /** The nom @link(String). */
    private String nom;

    /** The prenom @link(String). */
    private String prenom;

    /** The email @link(String). */
    private String email;

    /** The emploi @link(String). */
    private String emploi;

    /** The poste @link(String). */
    private String poste;

    /** The encadrant @link(String). */
    private String encadrant;

    /** The adresse @link(String). */
    private String adresse;

    /** The telephone @link(String). */
    private String telephone;

    /** The request @link(String). */
    private String request;

    /** The localScientist @link(String). */
    private String localScientist;

    /** The variables @link(String). */
    private String variables;

    /** The sites @link(String). */
    private String sites;

    /** The dates @link(String). */
    private String dates;

    private Boolean validateCharter = false;

    /** The save message @link(String). */
    private String saveMessage = UIBeanRequestRights.STRING_EMPTY;

    private Utilisateur currentUser;

    /**
     *
     * @return
     */
    public String navigate() {
        return UIBeanRequestRights.RULE_NAVIGATION_REQUEST_RIGHTS_JSF;
    }

    /**
     *
     * @return
     * @throws BusinessException
     */
    public String updateRights() throws BusinessException {
        Boolean modified = false;
        if (poste != getCurrentUser().getPoste()) {
            getCurrentUser().setPoste(poste);
            modified = true;
        }
        if (emploi != getCurrentUser().getEmploi()) {
            getCurrentUser().setEmploi(emploi);
            modified = true;
        }
        if (modified) {
            getCurrentUser().setPassword(null);
            usersManager.updateUserProfile(getCurrentUser());
        }
        RightRequestGLACPE rightRequest = new RightRequestGLACPE();
        rightRequest.setRequest(request);
        rightRequest.setMotivation(encadrant);
        rightRequest.setReference(localScientist);
        rightRequest.setInfos(adresse);
        rightRequest.setTelephone(telephone);
        rightRequest.setVariables(variables);
        rightRequest.setSites(sites);
        rightRequest.setDates(dates);
        rightRequest.setDateRequest(new Date());
//        usersManager.addNewRequest(rightRequest, getCurrentUser());
        setSaveMessage(localizationManager.getMessage(UIBeanRequestRights.BUNDLE_SOURCE_PATH_RIGHTS, UIBeanRequestRights.PROPERTY_MSG_NEW_RIGHT_REQUEST));
        return null;
    }

    /**
     *
     * @return
     */
    public Boolean getIsButtonValid() {
        Boolean isValid = false;

        if (UIBeanRequestRights.STRING_EMPTY.equals(getUiOrganisme().getValue()) || getUiOrganisme().getValue() == null || UIBeanRequestRights.STRING_EMPTY.equals(getUiFonction().getValue()) || getUiFonction().getValue() == null
                || UIBeanRequestRights.STRING_EMPTY.equals(getUiDemand().getValue()) || getUiDemand().getValue() == null || UIBeanRequestRights.STRING_EMPTY.equals(getUiVariables().getValue()) || getUiVariables().getValue() == null
                || UIBeanRequestRights.STRING_EMPTY.equals(getUiSites().getValue()) || getUiSites().getValue() == null || UIBeanRequestRights.STRING_EMPTY.equals(getUiDates().getValue()) || getUiDates().getValue() == null)
        {
            isValid = false;
        } else {
            isValid = true;
        }
        return isValid;
    }

    /**
     *
     * @return
     */
    public HtmlInputText getUiOrganisme() {
        return (HtmlInputText) uiForm.findComponent("organisme");
    }

    /**
     *
     * @return
     */
    public HtmlInputText getUiFonction() {
        return (HtmlInputText) uiForm.findComponent("fonction");
    }

    /**
     *
     * @return
     */
    public HtmlInputTextarea getUiDemand() {
        return (HtmlInputTextarea) uiForm.findComponent("demand");
    }

    /**
     *
     * @return
     */
    public HtmlInputTextarea getUiVariables() {
        return (HtmlInputTextarea) uiForm.findComponent("variables");
    }

    /**
     *
     * @return
     */
    public HtmlInputTextarea getUiSites() {
        return (HtmlInputTextarea) uiForm.findComponent("sites");
    }

    /**
     *
     * @return
     */
    public HtmlInputTextarea getUiDates() {
        return (HtmlInputTextarea) uiForm.findComponent("dates");
    }

    /**
     *
     * @param securityContext
     */
    public void setSecurityContext(ISecurityContext securityContext) {
        this.securityContext = securityContext;
    }

    /**
     *
     * @param usersManager
     */
    public void setUsersManager(IUsersManager usersManager) {
        this.usersManager = usersManager;
    }

    /**
     *
     * @param localizationManager
     */
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    /**
     *
     * @return
     */
    public String getNom() {
        return nom;
    }

    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     *
     * @return
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     *
     * @param prenom
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     *
     * @return
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return
     */
    public String getEmploi() {
        if (emploi == null) {
            emploi = getCurrentUser().getEmploi();
        }
        return emploi;
    }

    /**
     *
     * @param emploi
     */
    public void setEmploi(String emploi) {
        this.emploi = emploi;
    }

    /**
     *
     * @return
     */
    public String getPoste() {
        if (poste == null) {
            poste = getCurrentUser().getPoste();
        }
        return poste;
    }

    /**
     *
     * @param poste
     */
    public void setPoste(String poste) {
        this.poste = poste;
    }

    /**
     *
     * @return
     */
    public String getEncadrant() {
        return encadrant;
    }

    /**
     *
     * @param encadrant
     */
    public void setEncadrant(String encadrant) {
        this.encadrant = encadrant;
    }

    /**
     *
     * @return
     */
    public String getAdresse() {
        return adresse;
    }

    /**
     *
     * @param adresse
     */
    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    /**
     *
     * @return
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     *
     * @param telephone
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     *
     * @return
     */
    public String getRequest() {
        return request;
    }

    /**
     *
     * @param request
     */
    public void setRequest(String request) {
        this.request = request;
    }

    /**
     *
     * @return
     */
    public String getLocalScientist() {
        return localScientist;
    }

    /**
     *
     * @param localScientist
     */
    public void setLocalScientist(String localScientist) {
        this.localScientist = localScientist;
    }

    /**
     *
     * @return
     */
    public String getVariables() {
        return variables;
    }

    /**
     *
     * @param variables
     */
    public void setVariables(String variables) {
        this.variables = variables;
    }

    /**
     *
     * @return
     */
    public String getSites() {
        return sites;
    }

    /**
     *
     * @param sites
     */
    public void setSites(String sites) {
        this.sites = sites;
    }

    /**
     *
     * @return
     */
    public String getDates() {
        return dates;
    }

    /**
     *
     * @param dates
     */
    public void setDates(String dates) {
        this.dates = dates;
    }

    /**
     *
     * @return
     */
    public Boolean getValidateCharter() {
        return validateCharter;
    }

    /**
     *
     * @param validateCharter
     */
    public void setValidateCharter(Boolean validateCharter) {
        this.validateCharter = validateCharter;
    }

    /**
     *
     * @return
     */
    public String getSaveMessage() {
        return saveMessage;
    }

    /**
     *
     * @param saveMessage
     */
    public void setSaveMessage(String saveMessage) {
        this.saveMessage = saveMessage;
    }

    /**
     *
     * @return
     */
    public UIPanel getUiForm() {
        return uiForm;
    }

    /**
     *
     * @param uiForm
     */
    public void setUiForm(UIPanel uiForm) {
        this.uiForm = uiForm;
    }

    /**
     *
     * @return
     */
    public Utilisateur getCurrentUser() {
        if (currentUser == null) {
            currentUser = securityContext.getUtilisateur();
        }
        return currentUser;
    }

    /**
     *
     * @param currentUser
     */
    public void setCurrentUser(Utilisateur currentUser) {
        this.currentUser = currentUser;
    }
}
