package org.inra.ecoinfo.glacpe.logging.jsf;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author ptcherniati
 */
@ManagedBean(name = "uiLogs")
@ViewScoped
public class UIBeanLogs implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     * @return
     */
    public String navigate() {
        return "logs";
    }
}
