/**
 * OREILacs project - see LICENCE.txt for use created: 12 août 2009 11:23:00
 */
package org.inra.ecoinfo.dataset;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Transient;
import org.apache.log4j.Logger;
import org.inra.ecoinfo.dataset.config.IDatasetConfiguration;
import org.inra.ecoinfo.dataset.config.impl.DataTypeDescription;
import org.inra.ecoinfo.dataset.versioning.IVersionFileDAO;
import org.inra.ecoinfo.localization.IInternationalizable;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * The Class ConfigurationGeneratedRecorderDAO.
 *
 * @author "Antoine Schellenberger"
 */
public class ConfigurationGeneratedRecorderDAO implements IRecorderDAO, ApplicationContextAware, IInternationalizable {

    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.dataset.messages";
    /**
     * The Constant CST_VERSION_FILE_SETTER @link(String).
     */
    private static final String CST_VERSION_FILE_SETTER = "setVersionFileDAO";
    /**
     * The Constant PROPERTY_MSG_NO_SETTER @link(String).
     */
    private static final String PROPERTY_MSG_NO_SETTER = "PROPERTY_MSG_NO_SETTER";
    /**
     * The Constant PROPERTY_MSG_UNAVALAIBLE_RECORDER @link(String).
     */
    private static final String PROPERTY_MSG_UNAVALAIBLE_RECORDER = "PROPERTY_MSG_UNAVALAIBLE_RECORDER";
    /**
     * The LOGGER @link(Logger).
     */
    @Transient
    private static final Logger LOGGER = Logger.getLogger(ConfigurationGeneratedRecorderDAO.class.getName());
    /**
     * The application context @link(ApplicationContext).
     */
    private ApplicationContext applicationContext;
    /**
     * The localization manager @link(ILocalizationManager).
     */
    private ILocalizationManager localizationManager;
    /**
     * The configuration @link(Configuration).
     */
    protected IDatasetConfiguration configuration;
    /**
     * The datatypes recorders map @link(Map<String,IRecorder>).
     */
    protected Map<String, IRecorder> datatypesRecordersMap = new HashMap<>();

    /**
     * Gets the recorder by data type code.
     *
     * @param datatypeCode the datatype code
     * @return the recorder by data type code
     * @throws PersistenceException the persistence exception
     * @see
     * org.inra.ecoinfo.dataset.IRecorderDAO#getRecorderByDataTypeCode(java.lang.String)
     */
    @Override
    public IRecorder getRecorderByDataTypeCode(final String datatypeCode) throws PersistenceException {
        IRecorder datatypeRecorder = datatypesRecordersMap.get(datatypeCode);

        //If no recorder is assigned for the datatype code provided then use the Generic Recorder
        if (datatypeRecorder == null) {
            datatypeRecorder = applicationContext.getBean(GenericDatatypeRecorder.class);
        }
        return datatypeRecorder;

    }

    /**
     * Inits the.
     *
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    @SuppressWarnings("rawtypes")
    public void init() throws BusinessException {
        for (final DataTypeDescription datatype : configuration.getDataTypes()) {
            Object datatypeRecorderInstance;
            Class datatypeRecorderClass = null;
            try {
                datatypeRecorderClass = Class.forName(datatype.getRecorder());
                datatypeRecorderInstance = datatypeRecorderClass.newInstance();
            } catch (final ClassNotFoundException e) {
                LOGGER.info("class not found ", e);
                datatypeRecorderInstance = applicationContext.getBean(datatype.getRecorder());
                datatypeRecorderClass = datatypeRecorderInstance.getClass();
            } catch (InstantiationException | IllegalAccessException ex) {
                throw new BusinessException(ex);
            }
            final Method versionFileSetterMethod = retrieveMethodByName(ConfigurationGeneratedRecorderDAO.CST_VERSION_FILE_SETTER, datatypeRecorderClass);
            final Object versionFileDAO = applicationContext.getBean(IVersionFileDAO.ID_BEAN);
            try {
                versionFileSetterMethod.invoke(datatypeRecorderInstance, versionFileDAO);
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                throw new BusinessException(ex);
            }
            for (final String daoReference : datatype.getDaos()) {
                try {
                    final Object springDAO = applicationContext.getBean(daoReference);
                    final StringBuilder stringBuilder = new StringBuilder(daoReference);
                    final String daoName = stringBuilder.replace(0, 1, stringBuilder.substring(0, 1).toUpperCase()).toString();
                    final Method daoSetterMethod = retrieveMethodByName("set" + daoName, datatypeRecorderClass);
                    throwExceptionIfNull(daoSetterMethod, daoName, datatype);
                    daoSetterMethod.invoke(datatypeRecorderInstance, springDAO);
                } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                    throw new BusinessException(ex);
                }
            }
            final IRecorder datatypeRecorder = (IRecorder) datatypeRecorderInstance;
            datatypeRecorder.setLocalizationManager((ILocalizationManager) applicationContext.getBean("localizationManager"));
            datatypesRecordersMap.put(datatype.getCode(), datatypeRecorder);
        }
    }

    /**
     * Sets the application context.
     *
     * @param applicationContext the new application context
     * @see
     * org.inra.ecoinfo.dataset.ore.data.dao.IRecorderDAO#getRecorderByDataTypeName(java.lang.String)
     */
    @Override
    public void setApplicationContext(final ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
        setLocalizationManager((ILocalizationManager) applicationContext.getBean("localizationManager"));
    }

    /**
     * Sets the configuration.
     *
     * @param configuration the new configuration
     */
    public void setConfiguration(final IDatasetConfiguration configuration) {
        this.configuration = configuration;
    }

    /**
     * Sets the localization manager.
     *
     * @param localizationManager the new localization manager
     * @see
     * org.inra.ecoinfo.localization.IInternationalizable#setLocalizationManager(org.inra.ecoinfo.localization.ILocalizationManager)
     */
    @Override
    public void setLocalizationManager(final ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    private void throwExceptionIfNull(final Method daoSetterMethod, final String daoName, final DataTypeDescription datatype) throws BusinessException {
        if (daoSetterMethod == null) {
            try {
                throw new NoSuchMethodException(String.format(localizationManager.getMessage(ConfigurationGeneratedRecorderDAO.BUNDLE_SOURCE_PATH, ConfigurationGeneratedRecorderDAO.PROPERTY_MSG_NO_SETTER), daoName, datatype.getRecorder()));
            } catch (NoSuchMethodException ex) {
                throw new BusinessException(ex);
            }
        }
    }

    /**
     * Retrieve method by name.
     *
     * @param name the name
     * @param clazz the clazz
     * @return the method
     * @link(String) the name
     * @link(Class) the clazz
     */
    @SuppressWarnings("rawtypes")
    private Method retrieveMethodByName(final String name, final Class clazz) {
        for (final Method method : clazz.getMethods()) {
            if (method.getName().equals(name)) {
                return method;
            }
        }
        return null;
    }
}
