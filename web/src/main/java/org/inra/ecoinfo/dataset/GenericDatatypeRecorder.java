/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.dataset;

import static org.inra.ecoinfo.dataset.AbstractRecorder.LOGGER;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.dataset.versioning.IVersionFileDAO;

/**
 * The Class RecorderGLACPE.
 * <p>
 * This class is used to delete, test, upload and publish files
 *
 * @author Antoine Schellenberger
 */
public class GenericDatatypeRecorder extends AbstractRecorder {


    /**
     * The localization manager @link(ILocalizationManager).
     */
    private static volatile ILocalizationManager localizationManager;

    /**
     * Delete records.
     *
     * @param versionFile
     * @link(VersionFile) the version file
     * @throws BusinessException the business exception @see
     * org.inra.ecoinfo.dataset.IRecorder#deleteRecords(org.inra.ecoinfo.dataset
     * .versioning.entity.VersionFile)
     */
    @Override
    public void deleteRecords(final VersionFile versionFile) throws BusinessException {
        LOGGER.debug(String.format("Stub method at this time -> \"%s\" must be implemented", "deleteRecords()"));
    }

   

    /**
     * Process record.
     *
     * @param parser
     * @link(CSVParser) the parser
     * @param versionFile
     * @link(VersionFile) the version file
     * @param fileEncoding
     * @link(String) the file encoding
     * @throws BusinessException the business exception @see
     * org.inra.ecoinfo.dataset.impl.AbstractRecorder#processRecord(com.Ostermiller
     * .util.CSVParser, org.inra.ecoinfo.dataset.versioning.entity.VersionFile,
     * java.lang.String) does nothing because of overload record ()
     */
    @Override
    protected void processRecord(final CSVParser parser, final VersionFile versionFile, final String fileEncoding) throws BusinessException {
           LOGGER.debug(String.format("Stub method at this time -> \"%s\" must be implemented", "processRecord()"));
    }

    /**
     * Record.
     *
     * @param versionFile
     * @link(VersionFile) the version file
     * @param fileEncoding
     * @link(String) the file encoding
     * @throws BusinessException the business exception
     * 
     */
    @Override
    public void record(final VersionFile versionFile, final String fileEncoding) throws BusinessException {
        LOGGER.debug(String.format("Stub method at this time -> \"%s\" must be implemented", "record()"));
    }


    /**
     * Test format.
     *
     * @param versionFile
     * @link(VersionFile) the version file
     * @param encoding
     * @link(String) the encoding
     * @throws BusinessException the business exception
     *
     */
    @Override
    public void testFormat(final VersionFile versionFile, final String encoding) throws BusinessException {
        LOGGER.debug(String.format("Stub method at this time -> \"%s\" must be implemented", "testFormat()"));

    }

    /**
     * Test header.
     *
     * @param badsFormatsReport
     * @link(BadsFormatsReport) the bads formats report
     * @param parser
     * @link(CSVParser) the parser
     * @return the int
     * @throws BusinessException the business exception @see
     * org.inra.ecoinfo.dataset.impl.AbstractRecorder#testHeader(org.inra.ecoinfo
     * .dataset.BadsFormatsReport, com.Ostermiller.util.CSVParser) does nothing
     * because of overload record ()
     */
    @Override
    protected int testHeader(final BadsFormatsReport badsFormatsReport, final CSVParser parser) throws BusinessException {
        return 0;
    }

    @Override
    public void setVersionFileDAO(IVersionFileDAO versionFileDAO) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
