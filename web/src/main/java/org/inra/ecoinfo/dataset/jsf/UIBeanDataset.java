package org.inra.ecoinfo.dataset.jsf;

import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.persistence.Transient;
import org.apache.log4j.Logger;
import org.inra.ecoinfo.dataset.IDatasetManager;
import org.inra.ecoinfo.dataset.security.ISecurityDatasetManager;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.identification.IUsersManager;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.notifications.INotificationsManager;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.node.ILeafTreeNode;
import org.inra.ecoinfo.security.ISecurityContext;
import org.inra.ecoinfo.security.entity.Role;
import org.inra.ecoinfo.tree.TreeNodeItem;
import org.inra.ecoinfo.utils.UncatchedExceptionLogger;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.richfaces.application.push.MessageException;
import org.richfaces.application.push.TopicKey;
import org.richfaces.application.push.TopicsContext;
import org.richfaces.component.UIDataTable;
import org.richfaces.component.UITree;
import org.richfaces.event.FileUploadEvent;
import org.richfaces.event.TreeSelectionChangeEvent;
import org.richfaces.model.UploadedFile;
import org.springframework.orm.jpa.JpaTransactionManager;

/**
 * The Class UIBeanDataset.
 */
@ManagedBean(name = "uiDataset")
@ViewScoped
public class UIBeanDataset implements Serializable {

    /**
     * The Constant PATTERN_CONCAT @link(String).
     */
    private static final String PATTERN_CONCAT = "%s%s";
    /**
     * The Constant serialVersionUID @link(long).
     */
    private static final long serialVersionUID = 1L;
    /**
     * The current selection @link(TreeNodeDataset).
     */
    @Transient
    private TreeNodeDataset currentSelection;
    /**
     * The current selections @link(List<IUIBeanDatasetCurrentSelection>).
     */
    @Transient
    private final List<IUIBeanDatasetCurrentSelection> currentSelections = new LinkedList<>();
    /**
     * The tree length @link(int).
     */
    private int treeLength;
    /**
     * The datasets details datatable @link(UIDataTable).
     */
    @Transient
    private UIDataTable datasetsDetailsDatatable;
    /**
     * The datasets selection @link(List<Dataset>).
     */
    @Transient
    private List<Dataset> datasetsSelection = new LinkedList<>();
    /**
     * The display dataset panel @link(Boolean).
     */
    private Boolean displayDatasetPanel;
    /**
     * The selected dataset @link(Dataset).
     */
    private Dataset selectedDataset;
    /**
     * The selected dataset details datatable @link(UIDataTable).
     */
    @Transient
    private UIDataTable selectedDatasetDetailsDatatable;
    /**
     * The selected version @link(SelectedVersion). 
     */ 
    @Transient
    private SelectedVersion selectedVersion = new SelectedVersion();
    /**
     * The selection tree current @link(IUIBeanDatasetCurrentSelection).
     */
    private IUIBeanDatasetCurrentSelection selectionTreeCurrent = null;
    /**
     * The tree nodes datasets @link(List<TreeNodeDataset>).
     */
    @Transient
    private List<TreeNodeDataset> treeNodesDatasets = null;
    /**
     * The tree selection @link(UITree).
     */
    @Transient
    private UITree treeSelection;
    /**
     * The dataset current selection @link(IUIBeanDatasetCurrentSelection).
     */
    @ManagedProperty(value = "#{datasetCurrentSelection}")
    @Transient
    protected IUIBeanDatasetCurrentSelection datasetCurrentSelection;
    /**
     * The dataset manager @link(IDatasetManager).
     */
    @ManagedProperty(value = "#{datasetManager}")
    protected IDatasetManager datasetManager;   
    /**
     * The notifications manager @link(INotificationsManager).
     */
    @ManagedProperty(value = "#{notificationsManager}")
    protected INotificationsManager notificationsManager;
    /**
     * The security context @link(ISecurityContext).
     */
    @ManagedProperty(value = "#{securityContext}")
    protected ISecurityContext securityContext;
    /**
     * The transaction manager @link(JpaTransactionManager).
     */
    @ManagedProperty(value = "#{transactionManager}")
    protected JpaTransactionManager transactionManager;
    /**
     * The users manager @link(IUsersManager).
     */
    @ManagedProperty(value = "#{usersManager}")
    @Transient
    protected IUsersManager usersManager;
    /**
     * The localisation manager @link(ILocalizationManager).
     */
    @ManagedProperty(value = "#{localisationManager}")
    @Transient
    protected ILocalizationManager localisationManager;

    /**
     * Instantiates a new uI bean dataset.
     */
    public UIBeanDataset() {
    }

    public String getAcceptedTypesForSelection() {
        if (selectionTreeCurrent != null) {
            DataType datatype = ((ILeafTreeNode) selectionTreeCurrent.getCurrentTreeItemSelected().getValue()).getLeafNode().getDatatype();
            if (datatype.getIsGeneric()) {
                return "";
            } else {
                return "csv";
            }
        }
        return "";

    }

    /**
     * Builds the tree items.
     *
     * @param tis the tis
     * @param parent the parent
     * @return the list
     * @link(List<TreeNodeItem>) the tis
     * @link(TreeNodeDataset) the parent
     */
    public List<TreeNodeDataset> buildTreeItems(final List<TreeNodeItem> tis, final TreeNodeDataset parent) {
        final List<TreeNodeDataset> tnis = new LinkedList<>();
        for (final TreeNodeItem tni : tis) {
            final TreeNodeDataset ti = new TreeNodeDataset(tni, parent, this);
            tnis.add(ti);
            if (tni.isLeaf() && ti.getValue() instanceof ILeafTreeNode && !securityContext.getUtilisateur().getIsRoot()) {
                final String pathSelectionTree = ((ILeafTreeNode) ti.getValue()).getLeafNode().getPath();
                ti.setActionPublish(securityContext.hasPrivilege(pathSelectionTree, Role.ROLE_PUBLICATION, ISecurityDatasetManager.PRIVILEGE_TYPE_DATASET));
                ti.setActionDelete(securityContext.hasPrivilege(pathSelectionTree, Role.ROLE_SUPPRESSION, ISecurityDatasetManager.PRIVILEGE_TYPE_DATASET));
                ti.setActionUpload(securityContext.hasPrivilege(pathSelectionTree, Role.ROLE_DEPOT, ISecurityDatasetManager.PRIVILEGE_TYPE_DATASET));
            }
        }
        return tnis;
    }

    /**
     * Clicked row.
     *
     * @return the string
     */
    public String clickedRow() {
        selectedDataset = (Dataset) datasetsDetailsDatatable.getRowData();
        return null;
    }

    /**
     * Delete version.
     *
     * @return the string
     * @throws BusinessException the business exception
     */
    public String deleteVersion() throws BusinessException {
        final VersionFile versionFileSelected = selectedVersion.getVersionFile();
        datasetManager.deleteVersion(versionFileSelected);
        updateDatasetsSelection();
        if (selectedDataset.getVersions().size() > 1) {
            updateDatasetSelected();
        } else {
            selectedDataset = null;
        }
        return null;
    }

    /**
     * Gets the current selection.
     *
     * @return the current selection
     */
    public TreeNodeDataset getCurrentSelection() {
        return currentSelection;
    }

    /**
     * Sets the current selection.
     *
     * @param currentSelection the new current selection
     */
    public void setCurrentSelection(final TreeNodeDataset currentSelection) {
        this.currentSelection = currentSelection;
    }

    /**
     * Gets the current selections.
     *
     * @return the current selections
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    public List<IUIBeanDatasetCurrentSelection> getCurrentSelections() throws BusinessException {
        if (currentSelections.isEmpty()) {
            currentSelections.add(currentSelectionNewInstance());
        }
        return currentSelections;
    }

    /**
     * Gets the dataset current selection.
     *
     * @return the dataset current selection
     */
    public IUIBeanDatasetCurrentSelection getDatasetCurrentSelection() {
        return datasetCurrentSelection;
    }

    /**
     * Sets the dataset current selection.
     *
     * @param datasetCurrentSelection the new dataset current selection
     */
    public void setDatasetCurrentSelection(final IUIBeanDatasetCurrentSelection datasetCurrentSelection) {
        treeLength = datasetCurrentSelection.getTreeLength();
        this.datasetCurrentSelection = datasetCurrentSelection;
    }

    /**
     * Gets the datasets details datatable.
     *
     * @return the datasets details datatable
     */
    public UIDataTable getDatasetsDetailsDatatable() {
        return datasetsDetailsDatatable;
    }

    /**
     * Sets the datasets details datatable.
     *
     * @param datasetsDetailsDatatable the new datasets details datatable
     */
    public void setDatasetsDetailsDatatable(final UIDataTable datasetsDetailsDatatable) {
        this.datasetsDetailsDatatable = datasetsDetailsDatatable;
    }

    /**
     * Gets the datasets selection.
     *
     * @return the datasets selection
     */
    public List<Dataset> getDatasetsSelection() {
        return datasetsSelection;
    }

    /**
     * Gets the display dataset panel.
     *
     * @return the display dataset panel
     */
    public Boolean getDisplayDatasetPanel() {
        return displayDatasetPanel;
    }

    /**
     * Gets the selected dataset.
     *
     * @return the selected dataset
     */
    public Dataset getSelectedDataset() {
        return selectedDataset;
    }

    /**
     * Gets the selected dataset details datatable.
     *
     * @return the selected dataset details datatable
     */
    public UIDataTable getSelectedDatasetDetailsDatatable() {
        return selectedDatasetDetailsDatatable;
    }

    /**
     * Sets the selected dataset details datatable.
     *
     * @param selectedDatasetDetailsDatatable the new selected dataset details
     * datatable
     */
    public void setSelectedDatasetDetailsDatatable(final UIDataTable selectedDatasetDetailsDatatable) {
        this.selectedDatasetDetailsDatatable = selectedDatasetDetailsDatatable;
    }

    /**
     * Gets the selected version.
     *
     * @return the selected version
     */
    public SelectedVersion getSelectedVersion() {
        return selectedVersion;
    }

    /**
     * Sets the selected version.
     *
     * @param selectedVersion the new selected version
     */
    public void setSelectedVersion(final SelectedVersion selectedVersion) {
        this.selectedVersion = selectedVersion;
    }

    /**
     * Gets the selection tree current.
     *
     * @return the selection tree current
     */
    public IUIBeanDatasetCurrentSelection getSelectionTreeCurrent() {
        return selectionTreeCurrent;
    }

    /**
     * Gets the time zone.
     *
     * @return the time zone
     */
    public TimeZone getTimeZone() {
        return TimeZone.getDefault();
    }

    /**
     * Gets the tree nodes datasets.
     *
     * @return the tree nodes datasets
     */
    public List<TreeNodeDataset> getTreeNodesDatasets() {
        if (treeNodesDatasets == null) {
            try {
                final List<TreeNodeItem> treeNodesItem = datasetManager.retrieveRepositoryTree();
                treeNodesDatasets = buildTreeItems(treeNodesItem, null);
            } catch (final BusinessException e) {
                UncatchedExceptionLogger.logUncatchedException("uncatched exception in getTreeNodesDatasets", e);
            }
        }
        return treeNodesDatasets;
    }

    /**
     * Gets the tree selection.
     *
     * @return the tree selection
     */
    public UITree getTreeSelection() {
        return treeSelection;
    }

    /**
     * Sets the tree selection.
     *
     * @param treeSelection the new tree selection
     */
    public void setTreeSelection(final UITree treeSelection) {
        this.treeSelection = treeSelection;
    }

    /**
     * Inits the file upload.
     */
    public void initFileUpload() {
        Logger.getRootLogger().info("unused");
    }

    /**
     * Navigate.
     *
     * @return the string
     */
    
    public String navigate() {
        return "dataset";
    }

    /**
     * Publish checkbox.
     *
     * @return the string
     * @throws BusinessException the business exception
     */
    
    public String publishCheckbox() throws BusinessException {
        try {
            final VersionFile versionFileSelected = selectedVersion.getVersionFile();
            if (selectedVersion.getChecked()) {
                datasetManager.publishVersion(versionFileSelected);
            } else {
                datasetManager.unPublishVersion(versionFileSelected);
            }
            updateDatasetsSelection();
            updateDatasetSelected();
        } catch (final BusinessException e) {
            UncatchedExceptionLogger.log("error in uibenadataset", e);
            return null;
        }
        if (securityContext != null && securityContext.getUtilisateur() != null) {
            try {
                final TopicKey topicKey = new TopicKey(String.format(UIBeanDataset.PATTERN_CONCAT, "publishOccuredAdress", securityContext.getUtilisateur().getLogin()));
                final TopicsContext topicsContext = TopicsContext.lookup();
                topicsContext.publish(topicKey, null);
            } catch (final MessageException e) {
                UncatchedExceptionLogger.log("unvalid message", e);
                return null;
            }
        }
        return null;
    }

    /**
     * Row selected.
     */
    public void rowSelected() {
        selectedDataset = (Dataset) datasetsDetailsDatatable.getRowData();
    }

    /**
     * Selection changed.
     *
     * @param selectionChangeEvent the selection change event
     * @throws BusinessException the business exception
     * @link(TreeSelectionChangeEvent) the selection change event
     */
    public void selectionChanged(final TreeSelectionChangeEvent selectionChangeEvent) throws BusinessException {
        final List<Object> selection = new ArrayList<>(selectionChangeEvent.getNewSelection());
        final Object currentSelectionKey = selection.get(0);
        final UITree tree = (UITree) selectionChangeEvent.getSource();
        final Object storedKey = tree.getRowKey();
        tree.setRowKey(storedKey);
        tree.setRowKey(currentSelectionKey);
        currentSelection = (TreeNodeDataset) tree.getRowData();
        currentSelections.clear();
        selectionTreeCurrent = currentSelectionNewInstanceWithConstructor(currentSelection);
        currentSelections.add(selectionTreeCurrent);
        if (selectionTreeCurrent.isComplete()) {
            displayDatasetPanel = true;
            final ILeafTreeNode leafNode = (ILeafTreeNode) currentSelection.getValue();
            datasetsSelection = datasetManager.retrieveDatasets(leafNode);
        } else {
            displayDatasetPanel = false;
        }
        selectedDataset = null;
    }

    /**
     * Sets the dataset manager.
     *
     * @param datasetManager the new dataset manager
     */
    public void setDatasetManager(final IDatasetManager datasetManager) {
        this.datasetManager = datasetManager;
    }

    /**
     * Sets the localisation manager.
     *
     * @param localisationManager the new localisation manager
     */
    public void setLocalisationManager(final ILocalizationManager localisationManager) {
        this.localisationManager = localisationManager;
    }

    /**
     * Sets the notifications manager.
     *
     * @param notificationsManager the new notifications manager
     */
    public void setNotificationsManager(final INotificationsManager notificationsManager) {
        this.notificationsManager = notificationsManager;
    }

    /**
     * Sets the security context.
     *
     * @param securityContext the new security context
     */
    public void setSecurityContext(final ISecurityContext securityContext) {
        this.securityContext = securityContext;
    }

    /**
     * Sets the selected dataset id.
     *
     * @param selectedDatasetId the new selected dataset id
     */
    public void setSelectedDatasetId(final Long selectedDatasetId) {
        selectedDataset = retrieveDatasetById(selectedDatasetId);
    }

    /**
     * Sets the transaction manager.
     *
     * @param transactionManager the new transaction manager
     */
    public void setTransactionManager(final JpaTransactionManager transactionManager) {
        this.transactionManager = transactionManager;
    }

    /**
     * Sets the users manager.
     *
     * @param usersManager the new users manager
     */
    public void setUsersManager(final IUsersManager usersManager) {
        this.usersManager = usersManager;
    }

    /**
     * Upload check listener.
     *
     * @param event the event
     * @link(FileUploadEvent) the event
     */
    public void uploadCheckListener(final FileUploadEvent event) {
        try {
            final UploadedFile uploadedFile = event.getUploadedFile();

            if (!currentSelections.isEmpty()) {
                final VersionFile version = datasetManager.getVersionFromFileName(uploadedFile.getName(), (ILeafTreeNode) selectionTreeCurrent.getCurrentTreeItemSelected().getValue());
                if (version != null) {
                    version.setData(uploadedFile.getData());
                    datasetManager.checkVersion(version);
                }
            }
        } catch (final BusinessException e) {
            UncatchedExceptionLogger.log("error in uibenadataset", e);
        }
    }

    /**
     * Upload submit listener.
     *
     * @param event the event
     * @link(FileUploadEvent) the event
     */
    public void uploadSubmitListener(final FileUploadEvent event) {
        try {
            final UploadedFile uploadedFile = event.getUploadedFile();
            if (!currentSelections.isEmpty()) {
                final VersionFile version = datasetManager.getVersionFromFileName(uploadedFile.getName(), (ILeafTreeNode) selectionTreeCurrent.getCurrentTreeItemSelected().getValue());
                if (version != null) {
                    version.setData(uploadedFile.getData());
                    version.setExtension(uploadedFile.getFileExtension());
                    datasetManager.uploadVersion(version);
                    updateDatasetsSelection();
                    updateDatasetSelected();
                }
            }
        } catch (final BusinessException e) {
            UncatchedExceptionLogger.log("error in uibenadataset", e);
        }
    }

    /**
     * Current selection new instance.
     *
     * @return the iUI bean dataset current selection
     * @throws InstantiationException the instantiation exception
     * @throws IllegalAccessException the illegal access exception
     * @throws ClassNotFoundException the class not found exception
     * @throws NoSuchMethodException the no such method exception
     * @throws InvocationTargetException the invocation target exception
     */
    
    @SuppressWarnings("unchecked")
    private IUIBeanDatasetCurrentSelection currentSelectionNewInstance() throws BusinessException {
        try {
            Constructor<? extends TreeNodeDataset> cst = null;
            cst = tryGetConstructor(cst);
            cst.setAccessible(true);
            return (IUIBeanDatasetCurrentSelection) cst.newInstance(treeLength);
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            throw new BusinessException(ex);
        }
    }

    private <E extends TreeNodeDataset> Constructor<E> tryGetConstructor(Constructor<E> cst) {
        try {
            return (Constructor<E>) datasetCurrentSelection.getClass().getDeclaredConstructor(Integer.class);
        } catch (NoSuchMethodException | SecurityException ex) {
            java.util.logging.Logger.getLogger(UIBeanDataset.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cst;
    }

    /**
     * Current selection new instance with constructor.
     *
     * @param currentSelection the current selection
     * @return the iUI bean dataset current selection
     * @throws NoSuchMethodException the no such method exception
     * @throws InstantiationException the instantiation exception
     * @throws IllegalAccessException the illegal access exception
     * @throws InvocationTargetException the invocation target exception
     * @link(TreeNodeDataset) the current selection
     */
    private IUIBeanDatasetCurrentSelection currentSelectionNewInstanceWithConstructor(final TreeNodeDataset currentSelection) throws BusinessException {
        try {
            @SuppressWarnings("unchecked")
            final Constructor<? extends TreeNodeDataset> cst = (Constructor<? extends TreeNodeDataset>) datasetCurrentSelection.getClass().getDeclaredConstructor(TreeNodeDataset.class, Integer.class);
            cst.setAccessible(true);
            return (IUIBeanDatasetCurrentSelection) cst.newInstance(currentSelection, treeLength);
        } catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            throw new BusinessException(ex);
        }
    }

    /**
     * Retrieve dataset by id.
     *
     * @param id the id
     * @return the dataset
     * @link(Long) the id
     */
    private Dataset retrieveDatasetById(final Long id) {
        for (final Dataset dataset : datasetsSelection) {
            if (dataset.getId().equals(id)) {
                return dataset;
            }
        }
        return null;
    }

    /**
     * Retrieve version file by id.
     *
     * @param id the id
     * @param dataset the dataset
     * @return the version file
     * @link(Long) the id
     * @link(Dataset) the dataset
     */
    private VersionFile retrieveVersionFileById(final Long id, final Dataset dataset) {
        for (final VersionFile versionFile : dataset.getCollectionVersions()) {
            if (Long.valueOf(versionFile.getId()).equals(id)) {
                return versionFile;
            }
        }
        return null;
    }

    /**
     * Update dataset selected.
     */
    private void updateDatasetSelected() {
        if (selectedDataset != null) {
            selectedDataset = retrieveSelectedDatasetById(selectedDataset.getId(), datasetsSelection);
        }
    }

    /**
     * Update datasets selection.
     *
     * @throws BusinessException the business exception
     */
    private void updateDatasetsSelection() throws BusinessException {
        datasetsSelection = datasetManager.retrieveDatasets((ILeafTreeNode) currentSelection.getValue());        
    }

    /**
     * Retrieve selected dataset by id.
     *
     * @param id the id
     * @param datasets the datasets
     * @return the dataset
     * @link(Long) the id
     * @link(List<Dataset>) the datasets
     */
    Dataset retrieveSelectedDatasetById(final Long id, final List<Dataset> datasets) {
        for (final Dataset dataset : datasets) {
            if (dataset.getId().equals(id)) {
                return dataset;
            }
        }
        return null;
    }

    /**
     * The Class SelectedVersion.
     */
    public class SelectedVersion {

        /**
         * The Constant PROPERTY_CST_HYPHEN @link(String).
         */
        private static final String PROPERTY_CST_HYPHEN = "-";
        /**
         * The checked @link(Boolean).
         */
        private Boolean checked;
        /**
         * The id @link(Long).
         */
        private Long id;
        /**
         * The version file @link(VersionFile).
         */
        private VersionFile versionFile;

        /**
         * Gets the checked.
         *
         * @return the checked
         */
        public Boolean getChecked() {
            return checked;
        }

        /**
         * Gets the id.
         *
         * @return the id
         */
        public Long getId() {
            return id;
        }

        /**
         * Gets the version file.
         *
         * @return the version file
         */
        public VersionFile getVersionFile() {
            return versionFile;
        }

        /**
         * Sets the checked.
         *
         * @param checked the new checked
         */
        public void setChecked(final Boolean checked) {
            this.checked = checked;
        }

        /**
         * Sets the id.
         *
         * @param id the new id
         */
        public void setId(final Long id) {
            this.id = id;
            versionFile = retrieveVersionFileById(id, selectedDataset);
        }

        /**
         * To string.
         *
         * @return the string
         * @see java.lang.Object#toString()
         */
        @Override
        public String toString() {
            return id.toString().concat(SelectedVersion.PROPERTY_CST_HYPHEN).concat(checked.toString());
        }
    }
}
