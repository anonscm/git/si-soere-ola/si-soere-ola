package org.inra.ecoinfo.dataset.versioning;

import java.util.Date;
import java.util.List;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.refdata.node.entity.LeafNode;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * The Interface IDatasetDAO.
 */
public interface IDatasetDAO extends IDAO<Dataset> {

    /**
     * Adds the.
     *
     * @param version the version
     * @return the dataset
     * @throws PersistenceException the persistence exception
     * @link(VersionFile) the version
     */
    Dataset add(VersionFile version) throws PersistenceException;

    /**
     * Gets the dataset by natural key.
     *
     * @param leafNode the leaf node
     * @param startDate the start date
     * @param endDate the end date
     * @return the dataset by natural key
     * @throws PersistenceException the persistence exception
     * @link(LeafNode) the leaf node
     * @link(Date) the start date
     * @link(Date) the end date
     */
    Dataset getDatasetByNaturalKey(LeafNode leafNode, Date startDate, Date endDate) throws PersistenceException;

    /**
     * Checks for overlaping dates.
     *
     * @param version the version
     * @return true, if successful
     * @throws PersistenceException the persistence exception
     * @link(VersionFile) the version
     */
    boolean hasOverlapingDates(VersionFile version) throws PersistenceException;

    /**
     * Retrieve datasets.
     *
     * @param leafNode the leaf node
     * @return the list
     * @throws PersistenceException the persistence exception
     * @link(LeafNode) the leaf node
     */
    List<Dataset> retrieveDatasets(LeafNode leafNode) throws PersistenceException;
    
    /**
     * 
     * @return the published datasets 
     * @throws PersistenceException 
     */
     List<Dataset> retrievePublishedDatasets() throws PersistenceException ;
}
