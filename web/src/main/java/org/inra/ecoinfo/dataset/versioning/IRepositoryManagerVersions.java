package org.inra.ecoinfo.dataset.versioning;

import java.io.File;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * The Interface IRepositoryManagerVersions.
 */
public interface IRepositoryManagerVersions {

    /**
     * Gets the data.
     *
     * @param versionFile the version file
     * @return the data
     * @throws BusinessException the business exception
     * @link(VersionFile) the version file
     */
    byte[] getData(VersionFile versionFile) throws BusinessException;

    
     File getFile(final VersionFile versionFile) throws BusinessException;
    /**
     * Removes the version.
     *
     * @param versionFile the version file
     * @throws BusinessException the business exception
     * @link(VersionFile) the version file
     */
    void removeVersion(VersionFile versionFile) throws BusinessException;

    /**
     * Upload version.
     *
     * @param versionFile the version file
     * @throws BusinessException the business exception
     * @link(VersionFile) the version file
     */
    void uploadVersion(VersionFile versionFile) throws BusinessException;
}
