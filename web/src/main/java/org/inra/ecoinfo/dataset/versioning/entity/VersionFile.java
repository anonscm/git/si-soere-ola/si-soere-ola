package org.inra.ecoinfo.dataset.versioning.entity;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.BatchSize;
import org.inra.ecoinfo.identification.entity.Utilisateur;

/**
 * The Class VersionFile.
 *
 * @author philippe Entity implementation class for Entity: VersionFile
 */
@Entity
@Table(name = "insertion_version_file_ivf", uniqueConstraints = {
    @UniqueConstraint(columnNames = {VersionFile.VERSION_NUMBER, Dataset.KEY_ID})},
               indexes = {
                   @Index(name = "ivf_ids_idx", columnList = Dataset.KEY_ID)})
@BatchSize(size = VersionFile.BATCH_SIZE)
public class VersionFile implements Comparable<VersionFile>, Serializable {
    /**
     * The Constant ID_JPA @link(String).
     */
    public static final String ID_JPA = "ivf_id";
    /**
     * The Constant SIZE @link(String).
     */
    public static final String SIZE = "ivf_size";
    /**
     * The Constant VERSION_NUMBER @link(String).
     */
    public static final String VERSION_NUMBER = "ivf_version_number";

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    /**
     * The Constant MB.
     */
    private static final int MB = 1_048_576;
    /**
     * The Constant KB.
     */
    private static final int KB = 1_024;
    /**
     * The Constant BYTES.
     */
    private static final String BYTES = "bytes";
    /**
     * The Constant HEXA_20 @link(int).
     */
    private static final int HEXA_20 = 32;
    /**
     * The Constant BATCH_SIZE @link(int).
     */
    static final int BATCH_SIZE = 14;
    /**
     * The data @link(byte[]).
     */
    @Transient
    private byte[] data;
    /**
     * The dataset @link(Dataset).
     */
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.REFRESH, CascadeType.PERSIST}, optional = false)
    @JoinColumn(referencedColumnName = Dataset.KEY_ID, nullable = false, name = Dataset.KEY_ID)
    private Dataset dataset;
    /**
     * The id @link(long).
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = VersionFile.ID_JPA)
    private long id;
    /**
     * The size @link(int).
     */
    @Column(name = VersionFile.SIZE)
    private int size = 0;
    /**
     * The upload date @link(Date).
     */
    @Column(nullable = false, unique = false, name = "ivf_upload_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date uploadDate;
    /**
     * The upload user @link(Utilisateur).
     */
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.REFRESH, CascadeType.PERSIST}, optional = false)
    @JoinColumn(referencedColumnName = Utilisateur.UTILISATEUR_NAME_ID, nullable = false, name = "ivf_upload_user")
    private Utilisateur uploadUser;
    /**
     * The version number @link(long).
     */
    @Column(name = VersionFile.VERSION_NUMBER)
    private long versionNumber = -1;
    
    @Column(nullable = true)
    private String extension;

    

    /**
     * Compare to.
     *
     * @param o the o
     * @return the int
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(final VersionFile o) {
        if (o == null) {
            return -1;
        }
        if (getDataset().compareTo(o.getDataset()) != 0) {
            return getDataset().compareTo(o.getDataset());
        }
        return getVersionNumber() < o.getVersionNumber() ? 1 : -1;
    }

    /**
     * /* (non-Javadoc)
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if(obj instanceof VersionFile){
            VersionFile version = (VersionFile) obj;
            return Objects.equals(dataset, version.dataset) && 
            versionNumber== version.versionNumber ;
        }
        return false;
    }

    /**
     * Gets the data.
     *
     * @return the data
     */
    public byte[] getData() {
        return data == null ? null : Arrays.copyOf(data, data.length);
    }

    /**
     * Sets the data.
     *
     * @param d the new data
     */
    public void setData(final byte[] d) {
        if (d == null) {
            this.data = new byte[0];
        } else {
            this.data = Arrays.copyOf(d, d.length);
        }
    }

    /**
     * Gets the dataset.
     *
     * @return the dataset
     */
    public Dataset getDataset() {
        if (dataset == null) {
            dataset = new Dataset();
        }
        return dataset;
    }

    /**
     * Sets the dataset.
     *
     * @param dataset the new dataset
     */
    public void setDataset(final Dataset dataset) {
        this.dataset = dataset;
    }

    /**
     * Gets the file size.
     *
     * @return the file size
     */
    public int getFileSize() {
        return size;
    }

    /**
     * Sets the file size.
     *
     * @param size the new file size
     */
    public void setFileSize(final int size) {
        this.size = size;
    }

    /**
     * Gets the file size affichage.
     *
     * @return the file size affichage
     */
    public String getFileSizeAffichage() {
        String fileSize = "";
        if (size < VersionFile.KB) {
            fileSize = String.format("%s %s", size, VersionFile.BYTES);
        } else if (size < VersionFile.MB) {
            fileSize = String.format("%d %s", Math.round((double) size / VersionFile.KB), "KB");
        } else {
            fileSize = String.format("%d %s", Math.round((double) size / VersionFile.MB), "MB");
        }
        return fileSize;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final long id) {
        this.id = id;
    }

    /**
     * Gets the upload date.
     *
     * @return the upload date
     */
    public Date getUploadDate() {
        return uploadDate == null ? null : (Date) uploadDate.clone();
    }

    /**
     * Sets the upload date.
     *
     * @param uploadDate the new upload date
     */
    public void setUploadDate(final Date uploadDate) {
        this.uploadDate = uploadDate == null ? null : (Date) uploadDate.clone();
    }

    /**
     * Gets the upload user.
     *
     * @return the upload user
     */
    public Utilisateur getUploadUser() {
        return uploadUser;
    }

    /**
     * Sets the upload user.
     *
     * @param uploadUser the new upload user
     */
    public void setUploadUser(final Utilisateur uploadUser) {
        this.uploadUser = uploadUser;
    }

    /**
     * Gets the version number.
     *
     * @return the version number
     */
    public long getVersionNumber() {
        return versionNumber;
    }

    /**
     * Sets the version number.
     *
     * @param versionNumber the new version number
     */
    public void setVersionNumber(final long versionNumber) {
        this.versionNumber = versionNumber;
    }

    /**
     * Hash code.
     *
     * @return the int
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (dataset == null ? 0 : dataset.getId().hashCode());
        result = prime * result + (int) (versionNumber ^ versionNumber >>> VersionFile.HEXA_20);
        return result;
    }
    
    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }
    
}
