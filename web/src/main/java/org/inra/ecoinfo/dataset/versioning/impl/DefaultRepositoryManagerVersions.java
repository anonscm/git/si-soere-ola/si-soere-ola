package org.inra.ecoinfo.dataset.versioning.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.persistence.Transient;
import org.apache.log4j.Logger;
import org.inra.ecoinfo.MO;
import org.inra.ecoinfo.config.ICoreConfiguration;
import org.inra.ecoinfo.dataset.versioning.IRepositoryManagerVersions;
import org.inra.ecoinfo.dataset.versioning.IVersionFileHelper;
import org.inra.ecoinfo.dataset.versioning.IVersionFileHelperResolver;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.refdata.AbstractRecurentObject;
import org.inra.ecoinfo.utils.StreamCloser;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * The Class DefaultRepositoryManagerVersions.
 */
public class DefaultRepositoryManagerVersions extends MO implements IRepositoryManagerVersions {

    /**
     * The Constant PATH_SUFFIX_REPOSITORY_VERSION @link(String).
     */
    public static final String PATH_SUFFIX_REPOSITORY_VERSION = "repository";
    /**
     * The LOGGER @link(Logger).
     */
    @Transient
    private static final Logger LOGGER = Logger.getLogger(DefaultRepositoryManagerVersions.class.getName());
    /**
     * The configuration @link(Configuration).
     */
    protected ICoreConfiguration configuration;
    /**
     * The version file helper resolver @link(IVersionFileHelperResolver).
     */
    protected IVersionFileHelperResolver versionFileHelperResolver;

    /**
     * Instantiates a new default repository manager versions.
     */
    public DefaultRepositoryManagerVersions() {
        super();
    }

    /**
     * Gets the data.
     *
     * @param versionFile the version file
     * @return the data
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.dataset.versioning.IRepositoryManagerVersions#getData(org.inra.ecoinfo.dataset.versioning.entity.VersionFile)
     */
    @Override
    public byte[] getData(final VersionFile versionFile) throws BusinessException {
        final String filePath = buildFilePath(versionFile);
        final File file = new File(filePath);
        final byte[] datas = new byte[(int) file.length()];
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
            fis.read(datas, 0, (int) file.length());
        } catch (final FileNotFoundException e) {
            throw new BusinessException("file not found", e);
        } catch (final IOException e) {
            throw new BusinessException("ioexception", e);
        } finally {
            StreamCloser.closeStream(fis);
        }
        return datas;
    }

    /**
     * Gets the data.
     *
     * @param versionFile the version file
     * @return the data
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.dataset.versioning.IRepositoryManagerVersions#getData(org.inra.ecoinfo.dataset.versioning.entity.VersionFile)
     */
    @Override
    public File getFile(final VersionFile versionFile) throws BusinessException {
        final String filePath = buildFilePath(versionFile);
        return new File(filePath);
    }

    /**
     * Removes the version.
     *
     * @param versionFile the version file
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.dataset.versioning.IRepositoryManagerVersions#removeVersion(org.inra.ecoinfo.dataset.versioning.entity.VersionFile)
     */
    @Override
    public void removeVersion(final VersionFile versionFile) throws BusinessException {
        final String filePath = buildFilePath(versionFile);
        final File fileToRemove = new File(filePath);
        if (fileToRemove.exists() && fileToRemove.delete()) {
            LOGGER.info(fileToRemove.getName() + " deleted");
        }
    }

    /**
     * Sets the configuration.
     *
     * @param configuration the new configuration
     */
    public void setConfiguration(final ICoreConfiguration configuration) {
        this.configuration = configuration;
    }

    /**
     * Sets the version file helper resolver.
     *
     * @param versionFileHelperResolver the new version file helper resolver
     */
    public void setVersionFileHelperResolver(final IVersionFileHelperResolver versionFileHelperResolver) {
        this.versionFileHelperResolver = versionFileHelperResolver;
    }

    /**
     * Upload version.
     *
     * @param versionFile the version file
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.dataset.versioning.IRepositoryManagerVersions#uploadVersion(org.inra.ecoinfo.dataset.versioning.entity.VersionFile)
     */
    @Override
    public void uploadVersion(final VersionFile versionFile) throws BusinessException {
        Utils.testNotNullArguments(localizationManager, versionFile, versionFile.getData());
        FileOutputStream fos = null;
        try {
            byte[] data = versionFile.getData();

            if (!versionFile.getDataset().getLeafNode().getDatatype().getIsGeneric()) {
                data = Utils.convertEncoding(data, Utils.ENCODING_ISO_8859_1);
            }

            final String filePath = buildFilePath(versionFile);
            fos = new FileOutputStream(filePath);
            fos.write(data, 0, data.length);
            fos.flush();
            StreamCloser.closeStream(fos);
        } catch (final IOException e) {
            throw new BusinessException("ioexception", e);
        } finally {
            StreamCloser.closeStream(fos);
        }
    }

    /**
     * Builds the file path.
     *
     * @param versionFile the version file
     * @return the string
     * @throws BusinessException the business exception
     * @link(VersionFile) the version file
     */
    private String buildFilePath(final VersionFile versionFile) throws BusinessException {
        final String pathRootRepository = configuration.getRepositoryURI();
        final String datatypeCode = versionFile.getDataset().getLeafNode().getDatatype().getCode();
        final IVersionFileHelper versionFileHelper = versionFileHelperResolver.resolveByDatatype(datatypeCode);
        final String path = versionFile.getDataset().getLeafNode().getPath().replaceAll(AbstractRecurentObject.CST_PROPERTY_RECURENT_SEPARATOR_IN_SITE_NAME, Utils.SEPARATOR_URL);
        final String uploadDirectory = String.format("%s%s/%s", pathRootRepository, DefaultRepositoryManagerVersions.PATH_SUFFIX_REPOSITORY_VERSION, path);
        final File file = new File(uploadDirectory);
        if (!file.exists() && file.mkdirs()) {
            LOGGER.info(file.getName() + " created");
        }
        String extensionFile = "csv";
        if (versionFile.getDataset().getLeafNode().getDatatype().getIsGeneric() && versionFile.getExtension() != null && !versionFile.getExtension().isEmpty()) {
            extensionFile = versionFile.getExtension();
        }
        return String.format("%s/%s#_V%s#.%s", uploadDirectory, versionFileHelper.buildDownloadFilename(versionFile).split(".csv")[0], Long.toString(versionFile.getVersionNumber()), extensionFile);
    }
}
