package org.inra.ecoinfo.dataset.versioning.impl;

import com.google.common.collect.Lists;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import javax.persistence.Transient;
import org.apache.log4j.Logger;
import org.inra.ecoinfo.dataset.IRecorder;
import org.inra.ecoinfo.dataset.IRecorderDAO;
import org.inra.ecoinfo.dataset.config.IDatasetConfiguration;
import org.inra.ecoinfo.dataset.config.impl.DataTypeDescription;
import org.inra.ecoinfo.dataset.security.ISecurityDatasetManager;
import org.inra.ecoinfo.dataset.versioning.IDatasetDAO;
import org.inra.ecoinfo.dataset.versioning.IRepositoryManagerVersions;
import org.inra.ecoinfo.dataset.versioning.IVersionFileDAO;
import org.inra.ecoinfo.dataset.versioning.IVersionFileHelper;
import org.inra.ecoinfo.dataset.versioning.IVersionFileHelperResolver;
import org.inra.ecoinfo.dataset.versioning.IVersioningManager;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.identification.IUtilisateurDAO;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.notifications.entity.Notification;
import org.inra.ecoinfo.refdata.datatype.IDatatypeDAO;
import org.inra.ecoinfo.refdata.node.ILeafTreeNode;
import org.inra.ecoinfo.refdata.node.entity.LeafNode;
import org.inra.ecoinfo.refdata.site.ISiteDAO;
import org.inra.ecoinfo.refdata.theme.IThemeDAO;
import org.inra.ecoinfo.security.SecurityRole;
import org.inra.ecoinfo.security.entity.Role;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.ErrorsReport;
import org.inra.ecoinfo.utils.UncatchedExceptionLogger;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class DefaultVersioningManager.
 */
public class DefaultVersioningManager extends AbstractVersioningManager implements IVersioningManager {

    /**
     * The Constant MSG_BAD_FILE_FORMAT @link(String).
     */
    public static final String MSG_BAD_FILE_FORMAT = "PROPERTY_MSG_BAD_FILE_FORMAT";
    /**
     * The Constant MSG_GOOD_FILE_FORMAT @link(String).
     */
    public static final String MSG_GOOD_FILE_FORMAT = "PROPERTY_MSG_GOOD_FILE_FORMAT";

    /**
     * The Constant UNCATCHED_EXEPTION_IN_CHECK_VERSION @link(String).
     */
    private static final String UNCATCHED_EXEPTION_IN_CHECK_VERSION = "Uncatched exeption in check Version";
    /**
     * The Constant UNCATCHED_EXEPTION_IN_PUBLISH_VERSION @link(String).
     */
    private static final String UNCATCHED_EXEPTION_IN_PUBLISH_VERSION = "Uncatched exeption in publish Version";
    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.dataset.versioning.impl.messages";
    /**
     * The Constant MSG_ERREUR_PERSISTENCE2 @link(String).
     */
    private static final String MSG_ERREUR_PERSISTENCE2 = "PROPERTY_MSG_ERREUR_PERSISTENCE2";
    /**
     * The Constant MSG_ERROR_DURING_CHECKING @link(String).
     */
    private static final String MSG_ERROR_DURING_CHECKING = "PROPERTY_MSG_ERROR_DURING_CHECKING";
    /**
     * The Constant MSG_ERROR_DURING_PUBLICATION @link(String).
     */
    private static final String MSG_ERROR_DURING_PUBLICATION = "PROPERTY_MSG_ERROR_DURING_PUBLICATION";
    /**
     * The Constant MSG_PUBLISH_NOT_AVAILABLE @link(String).
     */
    private static final String MSG_PUBLISH_NOT_AVAILABLE = "PROPERTY_MSG_PUBLISH_NOT_AVAILABLE";
    private static final String MSG_PUBLISH_NOT_AVAILABLE_FOR_SAME_DATE_BEFORE = "PROPERTY_MSG_PUBLISH_NOT_AVAILABLE_FOR_SAME_DATE_BEFORE";
    private static final String MSG_PUBLISH_NOT_AVAILABLE_FOR_SAME_DATE_AFTER = "PROPERTY_MSG_PUBLISH_NOT_AVAILABLE_FOR_SAME_DATE_AFTER";
    /**
     * The Constant MSG_PUBLISH_NOT_AVAILABLE_FOR_OVERLAPING @link(String).
     */
    private static final String MSG_PUBLISH_NOT_AVAILABLE_FOR_OVERLAPING = "PROPERTY_MSG_PUBLISH_NOT_AVAILABLE_FOR_OVERLAPING";
    /**
     * The Constant MSG_PUBLISH_START @link(String).
     */
    private static final String MSG_PUBLISH_START = "PROPERTY_MSG_PUBLISH_START";
    /**
     * The Constant MSG_PUBLISH_SUCCESS @link(String).
     */
    private static final String MSG_PUBLISH_SUCCESS = "PROPERTY_MSG_PUBLISH_SUCCESS";
    /**
     * The Constant MSG_UNPUBLISH_FAILURE @link(String).
     */
    private static final String MSG_UNPUBLISH_FAILURE = "PROPERTY_MSG_UNPUBLISH_FAILURE";
    /**
     * The Constant MSG_UNPUBLISH_START @link(String).
     */
    private static final String MSG_UNPUBLISH_START = "PROPERTY_MSG_UNPUBLISH_START";
    /**
     * The Constant MSG_UNPUBLISH_SUCCESS @link(String).
     */
    private static final String MSG_UNPUBLISH_SUCCESS = "PROPERTY_MSG_UNPUBLISH_SUCCESS";
    /**
     * The Constant MSG_UPLOAD_SUCCESS @link(String).
     */
    private static final String MSG_UPLOAD_SUCCESS = "PROPERTY_MSG_UPLOAD_SUCCESS";
    /**
     * The Constant MSG_DELETION_SUCCESS @link(String).
     */
    private static final String MSG_DELETION_SUCCESS = "PROPERTY_MSG_DELETION_SUCCESS";
    private static final String MSG_NO_RIGHTS_FOR_UPLOAD = "PROPERTY_MSG_NO_RIGHTS_FOR_UPLOAD";
    private static final String MSG_NO_RIGHTS_FOR_PUBLISH = "PROPERTY_MSG_NO_RIGHTS_FOR_PUBLISH";
    private static final String MSG_NO_RIGHTS_FOR_DELETE = "PROPERTY_MSG_NO_RIGHTS_FOR_DELETE";
    private static final String MSG_NULL_VERSION = "PROPERTY_MSG_NULL_VERSION";
    /**
     * The LOGGER @link(Logger).
     */
    @Transient
    private static final Logger LOGGER = Logger.getLogger(DefaultVersioningManager.class.getName());
    /**
     * The dataset dao @link(IDatasetDAO).
     */
    protected IDatasetDAO datasetDAO;
    /**
     * The datatype dao @link(IDatatypeDAO).
     */
    protected IDatatypeDAO datatypeDAO;
    /**
     * The publishing version files @link(List<VersionFile>).
     */
    protected List<VersionFile> publishingVersionFiles = Lists.newArrayList();
    /**
     * The recorder dao @link(IRecorderDAO).
     */
    protected IRecorderDAO recorderDAO;
    /**
     * The repository manager versions @link(IRepositoryManagerVersions).
     */
    protected IRepositoryManagerVersions repositoryManagerVersions;
    /**
     * The site dao @link(ISiteDAO).
     */
    protected ISiteDAO siteDAO;
    /**
     * The theme dao @link(IThemeDAO).
     */
    protected IThemeDAO themeDAO;
    /**
     * The utilisateur dao @link(IUtilisateurDAO).
     */
    protected IUtilisateurDAO utilisateurDAO;
    /**
     * The version file dao @link(IVersionFileDAO).
     */
    protected IVersionFileDAO versionFileDAO;
    /**
     * The version file helper resolver @link(IVersionFileHelperResolver).
     */
    protected IVersionFileHelperResolver versionFileHelperResolver;

    @Override
    public String getDownloadFileName(VersionFile versionFile) throws BusinessException {
        final IVersionFileHelper versionFileHelper = versionFileHelperResolver.resolveByDatatype(versionFile.getDataset().getLeafNode().getDatatype().getCode());
        return versionFileHelper.buildDownloadFilename(versionFile);
    }

    /**
     * Check version.
     *
     * @param version the version
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.dataset.versioning.IVersioningManager#checkVersion(org.inra.ecoinfo.dataset.versioning.entity.VersionFile)
     */
    @Override
    @Transactional
    public void checkVersion(final VersionFile version) throws BusinessException {
        testIsNullVersion(version);
        checkVersion(version, true);
    }

    /**
     * Delete version.
     *
     * @param versionFile the version file
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.dataset.versioning.IVersioningManager#deleteVersion(org.inra.ecoinfo.dataset.versioning.entity.VersionFile)
     */
    @Override
    public void deleteVersion(VersionFile versionFile) throws BusinessException {
        testIsNullVersion(versionFile);
        testHasRights(versionFile, Role.ROLE_SUPPRESSION, DefaultVersioningManager.MSG_NO_RIGHTS_FOR_DELETE);
        VersionFile localVersionFile = versionFile;
        try {
            localVersionFile = versionFileDAO.merge(localVersionFile);
            final IVersionFileHelper versionFileHelper = versionFileHelperResolver.resolveByDatatype(localVersionFile.getDataset().getLeafNode().getDatatype().getCode());
            final String filename = versionFileHelper.buildDownloadFilename(localVersionFile);
            final String message = String.format(localizationManager.getMessage(DefaultVersioningManager.BUNDLE_SOURCE_PATH, DefaultVersioningManager.MSG_DELETION_SUCCESS), filename);
            if (localVersionFile.getDataset().getPublishVersion() != null && localVersionFile.getDataset().getPublishVersion().getId() == localVersionFile.getId()) {
                unPublish(localVersionFile);
            }
            final Dataset datasetDB = datasetDAO.merge(localVersionFile.getDataset());
            if (datasetDB.getVersions().size() == 1) {
                datasetDAO.remove(datasetDB);
            } else {
                datasetDB.getVersions().remove(localVersionFile.getVersionNumber());
                versionFileDAO.remove(localVersionFile);
            }
            repositoryManagerVersions.removeVersion(localVersionFile);
            notificationsManager.addNotification(buildNotification(DateUtil.calendarUTC().getTime(), Notification.INFO, message, null, null), securityContext.getUtilisateur().getLogin(), false);
        } catch (final PersistenceException e) {
            throw new BusinessException("can't remove version", e);
        }
    }

    /**
     * Download version by id.
     *
     * @param versionFileId the version file id
     * @return the byte[]
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.dataset.versioning.IVersioningManager#downloadVersionById(java.lang.Long)
     */
    @Override
    public byte[] downloadVersionById(final Long versionFileId) throws BusinessException {
        try {
            final VersionFile versionFile = versionFileDAO.getById(localizationManager, versionFileId);
            return repositoryManagerVersions.getData(versionFile);
        } catch (final PersistenceException e) {
            throw new BusinessException("can't download version", e);
        }
    }

    /**
     * Gets the version file by id.
     *
     * @param versionFileId the version file id
     * @return the version file by id
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.dataset.versioning.IVersioningManager#getVersionFileById(java.lang.Long)
     */
    @Override
    public VersionFile getVersionFileById(final Long versionFileId) throws BusinessException {
        VersionFile versionFile = null;
        try {
            versionFile = versionFileDAO.getById(localizationManager, versionFileId);
        } catch (final PersistenceException e) {
            throw new BusinessException("can't get version", e);
        }
        return versionFile;
    }

    /**
     * Gets the version file helper resolver.
     *
     * @return the version file helper resolver
     * @see
     * org.inra.ecoinfo.dataset.versioning.IVersioningManager#getVersionFileHelperResolver()
     */
    @Override
    public IVersionFileHelperResolver getVersionFileHelperResolver() {
        return versionFileHelperResolver;
    }

    /**
     * Sets the version file helper resolver.
     *
     * @param versionFileHelperResolver the new version file helper resolver
     */
    public void setVersionFileHelperResolver(final IVersionFileHelperResolver versionFileHelperResolver) {
        this.versionFileHelperResolver = versionFileHelperResolver;
    }

    /**
     * Publish version.
     *
     * @param versionFile the version file
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.dataset.versioning.IVersioningManager#publishVersion(org.inra.ecoinfo.dataset.versioning.entity.VersionFile)
     */
    @Override
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public void publishVersion(VersionFile versionFile) throws BusinessException {
        testIsNullVersion(versionFile);
        testHasRights(versionFile, Role.ROLE_PUBLICATION, DefaultVersioningManager.MSG_NO_RIGHTS_FOR_PUBLISH);
        testOverlapThenRecord(versionFile);
    }

    /**
     * Retrieve datasets.
     *
     * @param leafNode the leaf node
     * @return the list
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.dataset.versioning.IVersioningManager#retrieveDatasets(org.inra.ecoinfo.refdata.node.ILeafTreeNode)
     */
    @Override
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public List<Dataset> retrieveDatasets(final ILeafTreeNode leafNode) throws BusinessException {
        List<Dataset> datasets = null;
        try {
            datasets = datasetDAO.retrieveDatasets(leafNode.getLeafNode());
        } catch (final PersistenceException e) {
            throw new BusinessException("no dataset", e);
        }
    //data sets list sorted by start date period
       Collections.sort(datasets, new Comparator<Dataset>(){

            @Override
            public int compare(Dataset o1, Dataset o2) {                
                return o1.getDateDebutPeriode().compareTo(o2.getDateDebutPeriode());
            }

        });
        
        return datasets;
    }

    /**
     * Sets the configuration.
     *
     * @param configuration the new configuration
     */
    public void setConfiguration(final IDatasetConfiguration configuration) {
        this.configuration = configuration;
    }

    /**
     * Sets the dataset dao.
     *
     * @param datasetDAO the new dataset dao
     */
    public void setDatasetDAO(final IDatasetDAO datasetDAO) {
        this.datasetDAO = datasetDAO;
    }

    /**
     * Sets the datatype dao.
     *
     * @param datatypeDAO the new datatype dao
     */
    public void setDatatypeDAO(final IDatatypeDAO datatypeDAO) {
        this.datatypeDAO = datatypeDAO;
    }

    /**
     * Sets the recorder dao.
     *
     * @param recorderDAO the new recorder dao
     */
    public void setRecorderDAO(final IRecorderDAO recorderDAO) {
        this.recorderDAO = recorderDAO;
    }

    /**
     * Sets the repository manager versions.
     *
     * @param repositoryManagerVersions the new repository manager versions
     */
    public void setRepositoryManagerVersions(final IRepositoryManagerVersions repositoryManagerVersions) {
        this.repositoryManagerVersions = repositoryManagerVersions;
    }

    /**
     * Sets the site dao.
     *
     * @param siteDAO the new site dao
     */
    public void setSiteDAO(final ISiteDAO siteDAO) {
        this.siteDAO = siteDAO;
    }

    /**
     * Sets the theme dao.
     *
     * @param themeDAO the new theme dao
     */
    public void setThemeDAO(final IThemeDAO themeDAO) {
        this.themeDAO = themeDAO;
    }

    /**
     * Sets the utilisateur dao.
     *
     * @param utilisateurDAO the new utilisateur dao
     */
    public void setUtilisateurDAO(final IUtilisateurDAO utilisateurDAO) {
        this.utilisateurDAO = utilisateurDAO;
    }

    /**
     * Sets the version file dao.
     *
     * @param versionFileDAO the new version file dao
     */
    public void setVersionFileDAO(final IVersionFileDAO versionFileDAO) {
        this.versionFileDAO = versionFileDAO;
    }

    /**
     * Un publish.
     *
     * @param versionFile the version file
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.dataset.versioning.IVersioningManager#unPublish(org.inra.ecoinfo.dataset.versioning.entity.VersionFile)
     */
    @Override
    public void unPublish(VersionFile versionFile) throws BusinessException {
        testIsNullVersion(versionFile);
        testHasRights(versionFile, Role.ROLE_PUBLICATION, DefaultVersioningManager.MSG_NO_RIGHTS_FOR_PUBLISH);
        VersionFile localVersionFile = versionFile;
        IRecorder recorder = null;
        String message;
        final IVersionFileHelper versionFileHelper = versionFileHelperResolver.resolveByDatatype(localVersionFile.getDataset().getLeafNode().getDatatype().getCode());
        final String filename = versionFileHelper.buildDownloadFilename(localVersionFile);
        notificationsManager.addNotification(
                buildNotification(new Date(), Notification.INFO, String.format(localizationManager.getMessage(DefaultVersioningManager.BUNDLE_SOURCE_PATH, DefaultVersioningManager.MSG_UNPUBLISH_START), localVersionFile.getUploadDate(), filename),
                        null, null), securityContext.getUtilisateur().getLogin(), false);
        try {
            final Utilisateur utilisateur = utilisateurDAO.merge(securityContext.getUtilisateur());
            localVersionFile = versionFileDAO.getById(localizationManager, localVersionFile.getId());
            recorder = recorderDAO.getRecorderByDataTypeCode(localVersionFile.getDataset().getLeafNode().getDatatype().getCode());
            final Dataset datasetDB = localVersionFile.getDataset();
            datasetDB.setPublishDate(null);
            datasetDB.setPublishUser(null);
            datasetDB.setPublishVersion(null);
            recorder.deleteRecords(localVersionFile);
            message = String.format(localizationManager.getMessage(DefaultVersioningManager.BUNDLE_SOURCE_PATH, DefaultVersioningManager.MSG_UNPUBLISH_SUCCESS), localVersionFile.getUploadDate(), filename);
            notificationsManager.addNotification(buildNotification(DateUtil.calendarLocale().getTime(), Notification.INFO, message, null, null), utilisateur.getLogin(), false);
        } catch (final PersistenceException | BusinessException e) {
            message = String.format(localizationManager.getMessage(DefaultVersioningManager.BUNDLE_SOURCE_PATH, DefaultVersioningManager.MSG_UNPUBLISH_FAILURE), localVersionFile.getUploadDate(), filename);
            notificationsManager.addNotification(buildNotification(new Date(), Notification.ERROR, message, e.getMessage(), null), securityContext.getUtilisateur().getLogin(), false);
            throw new BusinessException(message, e);
        }
    }

    /**
     * Upload version.
     *
     * @param version the version
     * @return the dataset
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.dataset.versioning.IVersioningManager#uploadVersion(org.inra.ecoinfo.dataset.versioning.entity.VersionFile)
     */
    @Override
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    @SecurityRole(roles = {Role.ROLE_DEPOT, Role.ROLE_ADMINISTRATION})
    public Dataset uploadVersion(final VersionFile version) throws BusinessException {
        testIsNullVersion(version);
        testHasRights(version, Role.ROLE_DEPOT, DefaultVersioningManager.MSG_NO_RIGHTS_FOR_UPLOAD);
        checkVersion(version, false);
        final ErrorsReport errorsReport = new ErrorsReport(localizationManager.getMessage(ErrorsReport.BUNDLE_NAME, ErrorsReport.PROPERTY_MSG_ERROR));
        Dataset dataset = null;
        try {
            final Utilisateur utilisateur = utilisateurDAO.merge(securityContext.getUtilisateur());
            final Date uploadDate = DateUtil.calendarLocale().getTime();
            version.setUploadDate(uploadDate);
            version.setUploadUser(utilisateur);
            version.setFileSize(version.getData().length);
            final LeafNode leafNode = version.getDataset().getLeafNode();
            if (errorsReport.hasErrors()) {
                LOGGER.error(errorsReport.getErrorsMessages());
                notificationsManager.addNotification(buildNotification(DateUtil.calendarUTC().getTime(), Notification.INFO, errorsReport.buildHTMLMessages(), null, null), utilisateur.getLogin(), false);
                throw new BusinessException(errorsReport.getMessages());
            }
            dataset = datasetDAO.getDatasetByNaturalKey(leafNode, version.getDataset().getDateDebutPeriode(), version.getDataset().getDateFinPeriode());
            if (dataset == null) {
                dataset = version.getDataset();
                dataset.setLeafNode(leafNode);
                dataset.setCreateDate(uploadDate);
                dataset.setCreateUser(utilisateur);
                datasetDAO.saveOrUpdate(dataset);
            }
            dataset.addVersion(version);
            repositoryManagerVersions.uploadVersion(version);
            final IVersionFileHelper versionFileHelper = versionFileHelperResolver.resolveByDatatype(version.getDataset().getLeafNode().getDatatype().getCode());
            final String filename = versionFileHelper.buildDownloadFilename(version);
            notificationsManager
                    .addNotification(
                            buildNotification(DateUtil.calendarUTC().getTime(), Notification.INFO, String.format(localizationManager.getMessage(DefaultVersioningManager.BUNDLE_SOURCE_PATH, DefaultVersioningManager.MSG_UPLOAD_SUCCESS), filename),
                                    null, null), utilisateur.getLogin(), false);
        } catch (final PersistenceException e) {
            throw new BusinessException("can't upload version", e);
        }
        return dataset;
    }

    /**
     * Check version.
     *
     * @param version the version
     * @param enableNotifications the enable notifications
     * @return the i recorder
     * @throws BusinessException the business exception
     * @link(VersionFile) the version
     * @link(boolean) the enable notifications
     */
    private IRecorder checkVersion(final VersionFile version, final boolean enableNotifications) throws BusinessException {
        IRecorder recorder = null;
        final Utilisateur utilisateur = securityContext.getUtilisateur();

        try {
            recorder = recorderDAO.getRecorderByDataTypeCode(version.getDataset().getLeafNode().getDatatype().getCode());
            final IVersionFileHelper versionFileHelper = versionFileHelperResolver.resolveByDatatype(version.getDataset().getLeafNode().getDatatype().getCode());
            final String filename = versionFileHelper.buildDownloadFilename(version);
            trySanitize(version, utilisateur, filename);
            tryTestFormat(version, enableNotifications, recorder, utilisateur, filename);
        } catch (final PersistenceException e) {
            LOGGER.error("persitence exception", e);
            notificationsManager.addNotification(
                    buildNotification(DateUtil.calendarUTC().getTime(), Notification.ERROR, localizationManager.getMessage(DefaultVersioningManager.BUNDLE_SOURCE_PATH, DefaultVersioningManager.MSG_ERREUR_PERSISTENCE2), null, null),
                    utilisateur.getLogin(), false);
            throw new BusinessException("can't check version", e);
        }
        return recorder;
    }

    private Overlap getInstance(VersionFile versionFile) throws BusinessException, PersistenceException {
        final Overlap overlap = new Overlap(versionFile);
        overlap.initInstance();
        return overlap;
    }

    /**
     * Test right for publishing
     *
     * @param versionFile
     * @param role
     * @param keyMessage
     * @throws BusinessException
     */
    private void testHasRights(VersionFile versionFile, String role, String keyMessage) throws BusinessException {
        if (!securityContext.getUtilisateur().getIsRoot() && !securityContext.hasPrivilege(versionFile.getDataset().getLeafNode().getPath(), role, ISecurityDatasetManager.PRIVILEGE_TYPE_DATASET)) {
            String message = String.format(localizationManager.getMessage(DefaultVersioningManager.BUNDLE_SOURCE_PATH, keyMessage), versionFile.getDataset().getLeafNode().getPath());
            notificationsManager.addNotification(buildNotification(DateUtil.calendarUTC().getTime(), Notification.ERROR, message, null, null), securityContext.getUtilisateur().getLogin(), false);
            throw new BusinessException(message);
        }
    }

    private void testOverlapThenRecord(VersionFile versionFile) throws BusinessException {
        String message;
        Overlap overlap;
        try {
            overlap = getInstance(versionFile);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't read overlaping versions", ex);
        }
        if (overlap.canRecord()) {
            IRecorder recorder = null;
            final IVersionFileHelper versionFileHelper = versionFileHelperResolver.resolveByDatatype(versionFile.getDataset().getLeafNode().getDatatype().getCode());
            final String filename = versionFileHelper.buildDownloadFilename(versionFile);
            notificationsManager.addNotification(
                    buildNotification(new Date(), Notification.INFO, String.format(localizationManager.getMessage(DefaultVersioningManager.BUNDLE_SOURCE_PATH, DefaultVersioningManager.MSG_PUBLISH_START), versionFile.getUploadDate(), filename),
                            null, null), securityContext.getUtilisateur().getLogin(), false);
            tryRecord(versionFile, filename);
        } else if (overlap.overlapDB()) {
            final IVersionFileHelper versionFileHelper = versionFileHelperResolver.resolveByDatatype(versionFile.getDataset().getLeafNode().getDatatype().getCode());
            final String filename = versionFileHelper.buildDownloadFilename(versionFile);
            message = String.format(localizationManager.getMessage(DefaultVersioningManager.BUNDLE_SOURCE_PATH, DefaultVersioningManager.MSG_ERROR_DURING_PUBLICATION), filename);
            final String body = String.format(localizationManager.getMessage(DefaultVersioningManager.BUNDLE_SOURCE_PATH, DefaultVersioningManager.MSG_PUBLISH_NOT_AVAILABLE_FOR_OVERLAPING), versionFile.getDataset().getLeafNode().getPath());
            notificationsManager.addNotification(buildNotification(new Date(), Notification.ERROR, message, body, null), securityContext.getUtilisateur().getLogin(), false);
        } else if (overlap.notOverlappingWithSameDate != 0) {
            String bundleMessage = overlap.notOverlappingWithSameDate == Overlap.NOT_OVERLAPING_BUT_SAME_AT_BEGIN ? DefaultVersioningManager.MSG_PUBLISH_NOT_AVAILABLE_FOR_SAME_DATE_BEFORE : DefaultVersioningManager.MSG_PUBLISH_NOT_AVAILABLE_FOR_SAME_DATE_AFTER;
            message = String.format(localizationManager.getMessage(DefaultVersioningManager.BUNDLE_SOURCE_PATH, bundleMessage), versionFile.getDataset().getLeafNode().getPath(), versionFile.getDataset().getLeafNode().getPath());
            notificationsManager.addNotification(buildNotification(new Date(), Notification.ERROR, message, null, null), securityContext.getUtilisateur().getLogin(), false);
        } else {
            message = String.format(localizationManager.getMessage(DefaultVersioningManager.BUNDLE_SOURCE_PATH, DefaultVersioningManager.MSG_PUBLISH_NOT_AVAILABLE), versionFile.getDataset().getLeafNode().getPath());
            notificationsManager.addNotification(buildNotification(new Date(), Notification.ERROR, message, null, null), securityContext.getUtilisateur().getLogin(), false);
        }
    }

    private void tryRecord(VersionFile versionFile, final String filename) throws BusinessException {
        IRecorder recorder;
        String message;
        try {
            VersionFile localVersionFile = versionFileDAO.getById(localizationManager, versionFile.getId());
            publishingVersionFiles.add(localVersionFile);
            if (localVersionFile.getDataset().getPublishVersion() != null) {
                unPublish(localVersionFile.getDataset().getPublishVersion());
            }
            final Utilisateur utilisateur = utilisateurDAO.merge(securityContext.getUtilisateur());
            recorder = recorderDAO.getRecorderByDataTypeCode(localVersionFile.getDataset().getLeafNode().getDatatype().getCode());
            setAndSanitizeVersionFileData(localVersionFile);
            final Date startTime = new Date();
            final Dataset dataset = localVersionFile.getDataset();
            dataset.setPublishDate(DateUtil.calendarUTC().getTime());
            dataset.setPublishUser(utilisateur);
            dataset.setPublishVersion(localVersionFile);
            String encoding;
            encoding = Utils.detectStreamEncoding(localVersionFile.getData());
            recorder.record(localVersionFile, encoding);
            final Date endTime = new Date();
            final DateFormat dateFormat = DateUtil.getSimpleDateFormatTimeLocale();
            final String timePublish = dateFormat.format(new Date(endTime.getTime() - startTime.getTime()));
            LOGGER.debug(String.format(localizationManager.getMessage(DefaultVersioningManager.BUNDLE_SOURCE_PATH, DefaultVersioningManager.MSG_PUBLISH_SUCCESS), localVersionFile.getUploadDate(), filename, timePublish));
            message = String.format(localizationManager.getMessage(DefaultVersioningManager.BUNDLE_SOURCE_PATH, DefaultVersioningManager.MSG_PUBLISH_SUCCESS), localVersionFile.getUploadDate(), filename, timePublish);
            notificationsManager.addNotification(buildNotification(DateUtil.calendarUTC().getTime(), Notification.INFO, message, null, null), utilisateur.getLogin(), false);
        } catch (final BusinessException e) {
            message = String.format(localizationManager.getMessage(DefaultVersioningManager.BUNDLE_SOURCE_PATH, DefaultVersioningManager.MSG_ERROR_DURING_PUBLICATION), filename);
            notificationsManager.addNotification(buildNotification(new Date(), Notification.ERROR, message, e.getMessage(), null), securityContext.getUtilisateur().getLogin(), false);
            throw new BusinessException("business exception", e);
        } catch (final IOException | PersistenceException e) {
            UncatchedExceptionLogger.logUncatchedException(DefaultVersioningManager.UNCATCHED_EXEPTION_IN_PUBLISH_VERSION, e);
            message = String.format(localizationManager.getMessage(DefaultVersioningManager.BUNDLE_SOURCE_PATH, DefaultVersioningManager.MSG_ERROR_DURING_PUBLICATION), filename);
            notificationsManager.addNotification(buildNotification(new Date(), Notification.ERROR, message, e.getMessage(), null), securityContext.getUtilisateur().getLogin(), false);
            throw new BusinessException("can't publish version", e);
        } finally {
            publishingVersionFiles.remove(versionFile);
        }
    }

    private DataTypeDescription findDatatype(VersionFile versionFile) throws BusinessException {
        org.inra.ecoinfo.dataset.config.impl.DataTypeDescription datatype = null;
        try {
            for (final org.inra.ecoinfo.dataset.config.impl.DataTypeDescription dt : configuration.getDataTypes()) {
                if (dt.getCode().equals(versionFile.getDataset().getLeafNode().getDatatype().getCode())) {
                    datatype = dt;
                    break;
                }
            }
            if (datatype == null) {
                throw new PersistenceException("null datatype");
            }
        } catch (final PersistenceException e1) {
            LOGGER.info("no datatype", e1);
            notificationsManager.addNotification(buildNotification(new Date(), Notification.ERROR, "erreur d'accès à la base", "Une erreur s'est produite lors de l'accès à la base.", null), securityContext.getUtilisateur().getLogin(), false);
        }
        return datatype;
    }

    private void trySanitize(final VersionFile version, final Utilisateur utilisateur, final String filename) throws BusinessException {
        String message;
        try {
            if (version.getDataset().getLeafNode().getDatatype().getIsGeneric()) {
                Utils.sanitizeData(version.getData(), localizationManager, false);
            } else {
                Utils.sanitizeData(version.getData(), localizationManager);
            };
        } catch (final BusinessException e) {
            message = String.format(localizationManager.getMessage(DefaultVersioningManager.BUNDLE_SOURCE_PATH, DefaultVersioningManager.MSG_BAD_FILE_FORMAT), filename);
            notificationsManager.addNotification(buildNotification(DateUtil.calendarLocale().getTime(), Notification.ERROR, message, e.getMessage(), null), utilisateur.getLogin(), false);
            throw new BusinessException("message", e);
        }
    }

    private void tryTestFormat(final VersionFile version, final boolean enableNotifications, IRecorder recorder, final Utilisateur utilisateur, final String filename) throws BusinessException {
        String message;
        try {
            final String encoding = Utils.detectStreamEncoding(version.getData());
            recorder.testFormat(version, encoding);
            recorder.testInDeepFilename(filename);
            message = String.format(localizationManager.getMessage(DefaultVersioningManager.BUNDLE_SOURCE_PATH, DefaultVersioningManager.MSG_GOOD_FILE_FORMAT), filename);
            if (enableNotifications) {
                notificationsManager.addNotification(buildNotification(DateUtil.calendarUTC().getTime(), Notification.INFO, message, null, null), utilisateur.getLogin(), false);
            }
        } catch (final BusinessException e) {
            message = String.format(localizationManager.getMessage(DefaultVersioningManager.BUNDLE_SOURCE_PATH, DefaultVersioningManager.MSG_BAD_FILE_FORMAT), filename);
            notificationsManager.addNotification(buildNotification(DateUtil.calendarUTC().getTime(), Notification.ERROR, message, e.getMessage(), null), utilisateur.getLogin(), false);
            throw new BusinessException(message, e);
        } catch (final IOException e) {
            UncatchedExceptionLogger.logUncatchedException(DefaultVersioningManager.UNCATCHED_EXEPTION_IN_CHECK_VERSION, e);
            message = String.format(localizationManager.getMessage(DefaultVersioningManager.BUNDLE_SOURCE_PATH, DefaultVersioningManager.MSG_ERROR_DURING_CHECKING), filename);
            notificationsManager.addNotification(buildNotification(new Date(), Notification.ERROR, message, e.getMessage(), null), securityContext.getUtilisateur().getLogin(), false);
            throw new BusinessException("can't test format", e);
        }
    }

    /**
     * test the nullability of versionFile
     *
     * @param version
     * @throws BusinessException
     */
    private void testIsNullVersion(final VersionFile version) throws BusinessException {
        if (version == null) {
            String message = localizationManager.getMessage(DefaultVersioningManager.BUNDLE_SOURCE_PATH, DefaultVersioningManager.MSG_NULL_VERSION);
            notificationsManager.addNotification(buildNotification(DateUtil.calendarUTC().getTime(), Notification.WARN, message, null, null), securityContext.getUtilisateur().getLogin(), false);
            throw new BusinessException(message);
        }
    }

    /**
     *
     * @param localVersionFile
     * @throws BusinessException
     */
    protected void setAndSanitizeVersionFileData(VersionFile localVersionFile) throws BusinessException {
        localVersionFile.setData(repositoryManagerVersions.getData(localVersionFile));
        Boolean checkExtension = !localVersionFile.getDataset().getLeafNode().getDatatype().getIsGeneric();
        localVersionFile.setData(Utils.sanitizeData(localVersionFile.getData(), localizationManager, checkExtension));
    }

    class Overlap {

        protected static final int NOT_OVERLAPING = 0;
        protected static final int NOT_OVERLAPING_BUT_SAME_AT_END = -1;
        protected static final int NOT_OVERLAPING_BUT_SAME_AT_BEGIN = 1;

        /**
         * The version to publish
         */
        final VersionFile versionFile;
        /**
         * true if this version overlap another version in DB
         */
        boolean hasOverlapingDatesInDB = false;
        /**
         * sometimes version not overlap any version but the start date is the
         * same of the end date than an other publishing version or th end date
         * is the sams than the begin date of an other publishing version in
         * that case, wehave to wait theend of the publishing
         */
        int notOverlappingWithSameDate = NOT_OVERLAPING;
        /**
         * an other version from same dates and leafnode is trying publish
         */
        Boolean inList = false;
        /**
         * start date
         */
        final Date dateFinPeriode;
        /*
         end date
         */
        final Date dateDebutPeriode;
        final SimpleDateFormat simpleDateFormatDateLocale;
        /*
        the associate data type
         */
        org.inra.ecoinfo.dataset.config.impl.DataTypeDescription datatype;

        public Overlap(VersionFile versionFile) throws BusinessException {
            this.versionFile = versionFile;
            dateFinPeriode = versionFile.getDataset().getDateFinPeriode();
            dateDebutPeriode = versionFile.getDataset().getDateDebutPeriode();
            simpleDateFormatDateLocale = DateUtil.getSimpleDateFormatDateLocale();
            datatype = findDatatype(versionFile);
            hasOverlapingDatesInDB();
        }

        /**
         * find if the version - has eoverlaping version in db - has other
         * version in publishing continueous
         *
         * @throws PersistenceException
         */
        private void initInstance() throws PersistenceException {
            final Date dateFinPeriode = versionFile.getDataset().getDateFinPeriode();
            final Date dateDebutPeriode = versionFile.getDataset().getDateDebutPeriode();
            final SimpleDateFormat simpleDateFormatDateLocale = DateUtil.getSimpleDateFormatDateLocale();
            for (final VersionFile publishingVersionFile : publishingVersionFiles) {
                final Date publishingDateDebutPeriode = publishingVersionFile.getDataset().getDateDebutPeriode();
                final Date publishingDateFinPeriode = publishingVersionFile.getDataset().getDateFinPeriode();
                final String publishingPath = publishingVersionFile.getDataset().getLeafNode().getPath();
                final String path = versionFile.getDataset().getLeafNode().getPath();
                if (publishingPath.equals(path)
                        && publishingDateDebutPeriode.compareTo(dateFinPeriode) <= 0
                        && publishingDateFinPeriode.compareTo(dateDebutPeriode) >= 0) {
                    inList = true;
                    break;
                }
                if (publishingPath.equals(path)
                        && simpleDateFormatDateLocale.format(publishingDateFinPeriode).equals(simpleDateFormatDateLocale.format(dateDebutPeriode))) {
                    notOverlappingWithSameDate = NOT_OVERLAPING_BUT_SAME_AT_BEGIN;
                } else if (publishingPath.equals(path)
                        && simpleDateFormatDateLocale.format(publishingDateDebutPeriode).equals(simpleDateFormatDateLocale.format(dateFinPeriode))) {
                    notOverlappingWithSameDate = NOT_OVERLAPING_BUT_SAME_AT_END;
                }
            }
        }

        private void hasOverlapingDatesInDB() throws BusinessException {
            try {
                hasOverlapingDatesInDB = datasetDAO.hasOverlapingDates(versionFile);
            } catch (final PersistenceException e1) {
                LOGGER.info("overLapingDates version : " + versionFile.getId(), e1);
                notificationsManager.addNotification(buildNotification(new Date(), Notification.ERROR, "erreur d'accès à la base", "Une erreur s'est produite lors de l'accès à la base.", null), securityContext.getUtilisateur().getLogin(), false);
            }
        }

        /**
         * can record if there is not an other version with same dates and same
         * datatype trying publishing
         *
         * @return
         */
        private boolean canRecord() {
            return !inList && !overlapDB();
        }

        private boolean overlapDB() {
            return hasOverlapingDatesInDB && !datatype.canOverlapDates();
        }

    }
}
