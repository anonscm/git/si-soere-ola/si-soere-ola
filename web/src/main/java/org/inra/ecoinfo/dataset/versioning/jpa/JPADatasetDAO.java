package org.inra.ecoinfo.dataset.versioning.jpa;

import java.util.Date;
import java.util.List;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.dataset.versioning.IDatasetDAO;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.refdata.node.entity.LeafNode;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * The Class JPADatasetDAO.
 */
public class JPADatasetDAO extends AbstractJPADAO<Dataset> implements IDatasetDAO {

    /**
     * The Constant QUERY_GET_DATASET_BY_NATURAL_KEY @link(String).
     */
    private static final String QUERY_GET_DATASET_BY_NATURAL_KEY = "select distinct dsf from Dataset as dsf  left join fetch dsf.versions where dsf.leafNode = :leafNode and dsf.dateDebutPeriode= :dateDebut and dsf.dateFinPeriode= :dateFin";
    /**
     * The Constant QUERY_GET_OVERLAPING_DATASETS @link(String).
     */
    private static final String QUERY_GET_OVERLAPING_DATASETS = "select dsf from Dataset as dsf where " + "dsf.publishVersion is not null and " + "dsf !=:dataset and " + "dsf.leafNode = :leafNode and " + "dsf.dateDebutPeriode < :dateFin and "
            + "dsf.dateFinPeriode > :dateDebut";
    /**
     * The Constant QUERY_GET_DATASETS @link(String).
     */
    private static final String QUERY_GET_DATASETS = "select distinct dsf from Dataset as dsf where dsf.leafNode = :leafNode";
    private static final String QUERY_GET_PUBLISHED_DATASETS = "select distinct dsf from Dataset as dsf where dsf.publishDate is not null";

    /**
     * Adds the.
     *
     * @param version the version
     * @return the dataset
     * @throws PersistenceException the persistence exception
     * @see
     * org.inra.ecoinfo.dataset.versioning.IDatasetDAO#add(org.inra.ecoinfo.dataset.versioning.entity.VersionFile)
     */
    @Override
    public Dataset add(final VersionFile version) throws PersistenceException {
        return null;
    }

    /**
     * Gets the dataset by natural key.
     *
     * @param leafNode the leaf node
     * @param startDate the start date
     * @param endDate the end date
     * @return the dataset by natural key
     * @throws PersistenceException the persistence exception
     * @see
     * org.inra.ecoinfo.dataset.versioning.IDatasetDAO#getDatasetByNaturalKey(org.inra.ecoinfo.refdata.node.entity.LeafNode,
     * java.util.Date, java.util.Date)
     */
    @SuppressWarnings("unchecked")
    @Override
    public Dataset getDatasetByNaturalKey(final LeafNode leafNode, final Date startDate, final Date endDate) throws PersistenceException {
        Dataset dataset = null;
        try {
            final Query query = entityManager.createQuery(JPADatasetDAO.QUERY_GET_DATASET_BY_NATURAL_KEY);
            query.setParameter("leafNode", leafNode);
            query.setParameter("dateDebut", startDate);
            query.setParameter("dateFin", endDate);
            final List<Dataset> datasets = query.getResultList();
            dataset = !datasets.isEmpty() ? datasets.get(0) : null;
            return dataset;
        } catch (final NoResultException e) {
            LOGGER.info("no result", e);
            return null;
        } catch (final Exception e) {
            LOGGER.error(Utils.getLastCauseExceptionMessage(e));
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     * Checks for overlaping dates.
     *
     * @param version the version
     * @return true, if successful
     * @throws PersistenceException the persistence exception
     * @see
     * org.inra.ecoinfo.dataset.versioning.IDatasetDAO#hasOverlapingDates(org.inra.ecoinfo.dataset.versioning.entity.VersionFile)
     */
    @SuppressWarnings("unchecked")
    @Override
    public boolean hasOverlapingDates(final VersionFile version) throws PersistenceException {
        List<Dataset> datasets = null;
        try {
            final Query query = entityManager.createQuery(JPADatasetDAO.QUERY_GET_OVERLAPING_DATASETS);
            query.setParameter("dataset", version.getDataset());
            query.setParameter("leafNode", version.getDataset().getLeafNode());
            query.setParameter("dateDebut", version.getDataset().getDateDebutPeriode());
            query.setParameter("dateFin", version.getDataset().getDateFinPeriode());
            datasets = query.getResultList();
        } catch (final Exception e) {
            LOGGER.error(Utils.getLastCauseExceptionMessage(e));
            throw new PersistenceException(e.getMessage(), e);
        }
        if (datasets == null) {
            return false;
        }
        return !datasets.isEmpty();
    }

    /**
     * Retrieve datasets.
     *
     * @param leafNode the leaf node
     * @return the list
     * @throws PersistenceException the persistence exception
     * @see
     * org.inra.ecoinfo.dataset.versioning.IDatasetDAO#retrieveDatasets(org.inra.ecoinfo.refdata.node.entity.LeafNode)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Dataset> retrieveDatasets(final LeafNode leafNode) throws PersistenceException {
        try {
            final Query query = entityManager.createQuery(JPADatasetDAO.QUERY_GET_DATASETS);
            query.setParameter("leafNode", leafNode);
            return query.getResultList();
        } catch (final Exception e) {
            LOGGER.error(Utils.getLastCauseExceptionMessage(e));
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Dataset> retrievePublishedDatasets() throws PersistenceException {
        try {
            final Query query = entityManager.createQuery(JPADatasetDAO.QUERY_GET_PUBLISHED_DATASETS);
            return query.getResultList();
        } catch (final Exception e) {
            LOGGER.error(Utils.getLastCauseExceptionMessage(e));
            throw new PersistenceException(e.getMessage(), e);
        }
    }
}
