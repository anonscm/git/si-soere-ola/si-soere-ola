package org.inra.ecoinfo.genericfilesrepository.api;

import java.util.Date;
import java.util.List;
import org.inra.ecoinfo.glacpe.extraction.vo.DatatypeGenericVO;

import org.inra.ecoinfo.glacpe.extraction.vo.VariableVO;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author antoine Schellenberger
 */
public interface IGenericDatatypeManager {

    public List<DataType> retrieveAvailablesGenericDatatypes() throws BusinessException;

    /**
     *
     * @param plateformesIds
     * @param firstDate
     * @param endDate
     * @return
     * @throws BusinessException
     */
    List<VariableVO> retrieveListsVariables(List<Long> plateformesIds, Date firstDate, Date endDate) throws BusinessException;

    /**
     *
     * @return @throws BusinessException
     */
    List<Projet> retrieveAvailablesProjets() throws BusinessException;

    /**
     *
     * @param projetSiteId
     * @return
     * @throws BusinessException
     */
    List<Site> retrieveAvailablesSitesByProjetId(long projetSiteId) throws BusinessException;

    public List<DatatypeGenericVO> retrieveAvailablesGenericDatatypes(Long idProjectSelected, List<Long> idsSites, Boolean excludeAllVariable) throws BusinessException;

}
