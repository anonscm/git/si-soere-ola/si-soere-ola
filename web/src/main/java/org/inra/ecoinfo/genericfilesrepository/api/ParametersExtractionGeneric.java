/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.genericfilesrepository.api;

import java.util.Map;
import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;

/**
 *
 * @author antoine schellenberger
 */
public class ParametersExtractionGeneric extends DefaultParameter implements IParameter {

  
    public static final String KEY_MAP_SELECTION_SITES = "0";
    public static final String KEY_MAP_SELECTION_PROJECT = "1";
    public static final String KEY_MAP_SELECTION_DATE = "2";
    public static final String KEY_MAP_SELECTION_ID_PROJECT = "3";
    public static final String KEY_MAP_SELECTION_DATATYPE = "4";
    public static final String CODE_EXTRACTIONTYPE_GENERIC = "generic_datatypes";

    /**
     *
     * @param metadatasMap
     */
    public ParametersExtractionGeneric(Map<String, Object> metadatasMap) {
        setParameters(metadatasMap);
        setCommentaire((String) metadatasMap.get(IExtractionManager.KEYMAP_COMMENTS));
    }

    @Override
    public String getExtractionTypeCode() {
        return CODE_EXTRACTIONTYPE_GENERIC;
    }
    
}
