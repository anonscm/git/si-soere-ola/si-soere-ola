/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.genericfilesrepository.impl;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.apache.commons.io.FileUtils;
import org.inra.ecoinfo.config.impl.CoreConfiguration;
import org.inra.ecoinfo.extraction.IOutputBuilder;
import org.inra.ecoinfo.extraction.IOutputsBuildersResolver;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.filecomp.config.impl.IFileCompConfiguration;
import org.inra.ecoinfo.filecomp.entity.FileComp;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.FileWithFolderCreator;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author schellen
 */
public class CompositeGenericsDatatypesOutputBuilder implements IOutputsBuildersResolver, IOutputBuilder {

    private static final String EXTRACTION = "extraction";
    private static final String FILE_SEPARATOR = System.getProperty("file.separator");

    /**
     *
     */
    protected static final String SEPARATOR_TEXT = "_";

    protected static final String EXTENSION_ZIP = ".zip";

    protected IOutputBuilder genericsDatatypesOutputBuilder;

    protected IOutputBuilder requestReminderOutputBuilder;

    private CoreConfiguration configuration;
    /**
     * The configuration.
     */
    private IFileCompConfiguration fileCompConfiguration;

    public void setGenericsDatatypesOutputBuilder(IOutputBuilder genericsDatatypesOutputBuilder) {
        this.genericsDatatypesOutputBuilder = genericsDatatypesOutputBuilder;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<IOutputBuilder> resolveOutputsBuilders(Map<String, Object> metadatasMap) {
        List<IOutputBuilder> outputsBuilders = new LinkedList<IOutputBuilder>();
        outputsBuilders.add(genericsDatatypesOutputBuilder);
        outputsBuilders.add(requestReminderOutputBuilder);
        return outputsBuilders;
    }

    @SuppressWarnings("unchecked")
    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException, NoExtractionResultException {
        RObuildZipOutputStream rObuildZipOutputStream = buildZipOutputStream(parameters.getExtractionTypeCode());
        List<IOutputBuilder> roBuildZipOutputStream = resolveOutputsBuilders(parameters.getParameters());
        ZipOutputStream zipOutputStream = rObuildZipOutputStream.getZipOutputStream();
        NoExtractionResultException noDataToExtract = null;
        for (IOutputBuilder iOutputBuilder : roBuildZipOutputStream) {
            try {
                iOutputBuilder.buildOutput(parameters);
            } catch (NoExtractionResultException e) {
                noDataToExtract = e;
            }
        }
        for (Map<String, File> filesMap : ((DefaultParameter) parameters).getFilesMaps()) {
            try {
                embedInZip(zipOutputStream, filesMap);
            } catch (IOException e) {
                throw new BusinessException("file not found ", e);
            }
        }
        List<FileComp> fileComps = (List<FileComp>) parameters.getParameters().get(FileComp.class.getSimpleName());
        Map<String, File> filecompsToEmbed = new TreeMap<String, File>();
        if (fileComps != null && !fileComps.isEmpty()) {
            for (FileComp fileComp : fileComps) {
                String path = fileComp.getAbsoluteFilePath(fileCompConfiguration.getRepositoryFiles());
                File fileToEmbed = new File(path);
                if (fileToEmbed.exists() && fileToEmbed.canRead()) {
                    String fileName = "tmp/".concat(fileComp.getFileName());
                    int count = 1;
                    while (filecompsToEmbed.containsKey(fileName)) {
                        count++;
                        fileName = fileName.matches("(.*)( \\([0-9]*\\))$") ? fileName.replaceAll("(.*)(\\([0-9]*\\))$", String.format("$1 (%d)", count)) : fileName.concat(String.format(" (%d)", count));
                    }
                    File copyFile = new File(fileName);
                    try {
                        FileUtils.copyFile(fileToEmbed, copyFile);
                        fileToEmbed = copyFile;
                    } catch (IOException e) {
                        Logger.getAnonymousLogger().info("can't copy file " + path);
                        continue;
                    }
                    filecompsToEmbed.put(fileName, fileToEmbed);
                }
            }
            try {
                if (!filecompsToEmbed.isEmpty()) {
                    embedInZip(zipOutputStream, filecompsToEmbed);
                }
            } catch (final IOException e) {
                throw new BusinessException("file not found ", e);
            }
        }

        try {
            zipOutputStream.flush();
            zipOutputStream.close();
        } catch (final IOException e) {
            throw new BusinessException("io exception", e);
        }
        if (noDataToExtract != null) {
            throw noDataToExtract;
        }
        return rObuildZipOutputStream;
    }

    private RObuildZipOutputStream buildZipOutputStream(String datatypeCode) throws BusinessException {
        Date startDate = DateUtil.calendarUTC().getTime();

        String extractionPath = configuration.getRepositoryURI().concat(FILE_SEPARATOR).concat(EXTRACTION).concat(FILE_SEPARATOR);
        String fileRandomSuffix = new Long(startDate.getTime()).toString().concat("-").concat(new Double(Math.random() * 1000000000).toString().substring(0, 6));
        File resultFile = FileWithFolderCreator.createFile(extractionPath.concat(datatypeCode).concat(FILE_SEPARATOR).concat(datatypeCode).concat(SEPARATOR_TEXT).concat(fileRandomSuffix).concat(EXTENSION_ZIP));
        FileOutputStream outFile;
        try {
            outFile = new FileOutputStream(resultFile);
        } catch (final FileNotFoundException e) {
            throw new BusinessException("file not found", e);
        }
        ZipOutputStream zipOutputStream = new ZipOutputStream(new BufferedOutputStream(outFile));
        RObuildZipOutputStream roBuildZipOutputStream = new RObuildZipOutputStream(zipOutputStream, fileRandomSuffix, startDate);

        return roBuildZipOutputStream;
    }

    private void embedInZip(ZipOutputStream zipOutputStream, Map<String, File> filesMap) throws IOException {
        byte[] buf = new byte[1024];
        for (String fileName : filesMap.keySet()) {
            File file = filesMap.get(fileName);

            InputStream in = new BufferedInputStream(new FileInputStream(file));

            zipOutputStream.putNextEntry(new ZipEntry(file.getName()));
            int len;
            while ((len = in.read(buf, 0, 1024)) > 0) {
                zipOutputStream.write(buf, 0, len);
            }
            file.delete();
            zipOutputStream.closeEntry();
            in.close();
        }
    }

    /**
     *
     * @param requestReminderOutputBuilder
     */
    public void setRequestReminderOutputBuilder(IOutputBuilder requestReminderOutputBuilder) {
        this.requestReminderOutputBuilder = requestReminderOutputBuilder;
    }

    /**
     *
     * @param configuration
     */
    public void setConfiguration(CoreConfiguration configuration) {
        this.configuration = configuration;
    }

    /**
     *
     * @param fileCompConfiguration
     */
    public void setFileCompConfiguration(IFileCompConfiguration fileCompConfiguration) {
        this.fileCompConfiguration = fileCompConfiguration;
    }

}
