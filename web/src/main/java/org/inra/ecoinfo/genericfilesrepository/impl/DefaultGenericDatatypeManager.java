package org.inra.ecoinfo.genericfilesrepository.impl;

import org.inra.ecoinfo.genericfilesrepository.api.IGenericDatatypeManager;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.inra.ecoinfo.MO;
import org.inra.ecoinfo.dataset.versioning.IDatasetDAO;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.glacpe.extraction.vo.DatatypeGenericVO;
import org.inra.ecoinfo.glacpe.extraction.vo.VariableVO;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.projetsite.ProjetSite;
import org.inra.ecoinfo.glacpe.refdata.projetsitethemedatatype.IProjetSiteThemeDatatypeDAO;
import org.inra.ecoinfo.glacpe.refdata.projetsitethemedatatype.ProjetSiteThemeDatatype;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.datatype.IDatatypeDAO;
import org.inra.ecoinfo.refdata.datatypevariableunite.DatatypeVariableUnite;
import org.inra.ecoinfo.refdata.datatypevariableunite.IDatatypeVariableUniteDAO;
import org.inra.ecoinfo.refdata.node.ILeafTreeNode;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.inra.ecoinfo.zooplancton.services.IZooplanctonDatatypeManager;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author antoine Schellenberger
 */
public class DefaultGenericDatatypeManager extends MO implements IGenericDatatypeManager {

    protected IGenericDatatypeDAO genericDatatypeDAO;
    protected IDatasetDAO datasetDAO;
    protected IProjetSiteThemeDatatypeDAO projetSiteThemeDatatypeDAO;
    protected IDatatypeVariableUniteDAO datatypeVariableUniteDAO;

    public void setProjetSiteThemeDatatypeDAO(IProjetSiteThemeDatatypeDAO projetSiteThemeDatatypeDAO) {
        this.projetSiteThemeDatatypeDAO = projetSiteThemeDatatypeDAO;
    }

    /**
     *
     */
    protected IDatatypeDAO datatypeDAO;

    public void setGenericDatatypeDAO(IGenericDatatypeDAO genericDatatypeDAO) {
        this.genericDatatypeDAO = genericDatatypeDAO;
    }

    /**
     *
     * @return @throws BusinessException
     */
    @Transactional(readOnly = true, rollbackFor = Exception.class)
    @Override
    public List<Projet> retrieveAvailablesProjets() throws BusinessException {
        try {
            List<Dataset> datasets = datasetDAO.retrievePublishedDatasets();
            Set<Projet> projectsInners = new HashSet<>();
            for (Dataset dataset : datasets) {
                if (dataset.getLeafNode() instanceof ProjetSiteThemeDatatype) {
                    ProjetSiteThemeDatatype projetSiteThemeDatatype = (ProjetSiteThemeDatatype) dataset.getLeafNode();
                    projectsInners.add(projetSiteThemeDatatype.getProjetSiteTheme().getProjetSite().getProjet());
                } else {
                    throw new BusinessException(String.format("The leaf node must be of type ProjetSiteThemeDatatype but it's type is %s!", dataset.getLeafNode().getClass().getSimpleName()));
                }
            }
            return new LinkedList<>(projectsInners);
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    /**
     *
     * @return @throws BusinessException
     */
    @Transactional(readOnly = true, rollbackFor = Exception.class)
    @Override
    public List<DataType> retrieveAvailablesGenericDatatypes() throws BusinessException {
        try {
            List<Dataset> datasets = datasetDAO.retrievePublishedDatasets();
            Set<DataType> datatypesInner = new HashSet<>();
            for (Dataset dataset : datasets) {
                DataType datatype = dataset.getLeafNode().getDatatype();
                if (datatype.getIsGeneric()) {
                    datatypesInner.add(datatype);
                }
            }
            return new LinkedList<>(datatypesInner);
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    private List<VariableVO> retrieveListVariables() throws BusinessException {
        try {
            List<VariableGLACPE> variables = (List) ((DataType) datatypeDAO.getByCode(IZooplanctonDatatypeManager.CODE_DATATYPE)).getVariables();
            List<VariableVO> variablesVO = new ArrayList<VariableVO>();

            for (VariableGLACPE variable : variables) {
                VariableVO variableVO = new VariableVO(variable);
                variablesVO.add(variableVO);
            }
            return variablesVO;
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    private List<VariableVO> retrieveListAvailableVariables(List<Long> plateformesIds, Date firstDate, Date endDate) throws BusinessException {
        try {
            List<VariableGLACPE> variables = genericDatatypeDAO.retrieveAvailablesVariables(plateformesIds, firstDate, endDate);
            List<VariableVO> variablesVO = new ArrayList<VariableVO>();

            for (VariableGLACPE variable : variables) {
                VariableVO variableVO = new VariableVO(variable);
                variablesVO.add(variableVO);
            }
            return variablesVO;
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    /**
     *
     * @param plateformesIds
     * @param firstDate
     * @param lastDate
     * @return
     * @throws BusinessException
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public List<VariableVO> retrieveListsVariables(List<Long> plateformesIds, Date firstDate, Date lastDate) throws BusinessException {
        Map<String, VariableVO> rawVariablesMap = new HashMap<String, VariableVO>();

        for (VariableVO variable : retrieveListVariables()) {
            rawVariablesMap.put(variable.getCode(), variable);
        }

        for (VariableVO availableVariable : retrieveListAvailableVariables(plateformesIds, firstDate, lastDate)) {
            availableVariable.setIsDataAvailable(true);
            rawVariablesMap.put(availableVariable.getCode(), availableVariable);
        }

        return new LinkedList<VariableVO>(rawVariablesMap.values());
    }

    /**
     *
     * @param datatypeDAO
     */
    public void setDatatypeDAO(IDatatypeDAO datatypeDAO) {
        this.datatypeDAO = datatypeDAO;
    }

    /**
     *
     * @param projetId
     * @return
     * @throws BusinessException
     */
    @Override
    public List<Site> retrieveAvailablesSitesByProjetId(long projetId) throws BusinessException {
        try {
            List<Dataset> datasets = datasetDAO.retrievePublishedDatasets();
            Set<Site> sitesInners = new HashSet<>();
            for (Dataset dataset : datasets) {
                if (dataset.getLeafNode() instanceof ProjetSiteThemeDatatype) {
                    ProjetSiteThemeDatatype projetSiteThemeDatatype = (ProjetSiteThemeDatatype) dataset.getLeafNode();
                    ProjetSite projetSite = projetSiteThemeDatatype.getProjetSiteTheme().getProjetSite();
                    Projet projet = projetSite.getProjet();
                    if (projet.getId().equals(projetId)) {
                        sitesInners.add(projetSite.getSite());
                    }

                } else {
                    throw new BusinessException(String.format("The leaf node must be of type ProjetSiteThemeDatatype but it's type is !", dataset.getLeafNode().getClass().getSimpleName()));
                }
            }
            return new LinkedList<>(sitesInners);
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    public void setDatasetDAO(IDatasetDAO datasetDAO) {
        this.datasetDAO = datasetDAO;
    }

    @Override
    @Transactional(readOnly = true)
    public List<DatatypeGenericVO> retrieveAvailablesGenericDatatypes(Long idProjectSelected, List<Long> idsSites, Boolean excludeAllVariable) throws BusinessException {
        Set<DatatypeGenericVO> datatypeGenericVOs = new HashSet<>();

        try {
            List<Dataset> datasets = datasetDAO.getAll(Dataset.class);
            List<Long> datatypeIdsSelected = new LinkedList<>();

            for (Dataset dataset : datasets) {
                if (dataset.getLeafNode().getDatatype().getIsGeneric()) {
                    DataType datatype = dataset.getLeafNode().getDatatype();
                    if (dataset.getLeafNode() instanceof ProjetSiteThemeDatatype) {
                        ProjetSiteThemeDatatype projetSiteThemeDatatype = (ProjetSiteThemeDatatype) dataset.getLeafNode();
                        ProjetSite projetSite = projetSiteThemeDatatype.getProjetSiteTheme().getProjetSite();
                        Projet projet = projetSite.getProjet();
                        Site site = projetSite.getSite();
                        if (projet.getId().equals(idProjectSelected) && idsSites.contains(site.getId()) && dataset.getPublishDate() != null) {
                            List<Variable> variables = new LinkedList<>();
                            for (DatatypeVariableUnite datatypeVariableUnite : datatypeVariableUniteDAO.getByDatatype(datatype.getCode())) {
                                if (excludeAllVariable) {
                                    if (!datatypeVariableUnite.getVariable().getCode().equals("toutes_les_variables")) {
                                        variables.add(datatypeVariableUnite.getVariable());

                                    }
                                } else {
                                    variables.add(datatypeVariableUnite.getVariable());
                                }
                            }
                            final DatatypeGenericVO datatypeGenericVO = new DatatypeGenericVO(datatype, Boolean.TRUE, dataset);
                            datatypeGenericVO.setVariables(variables);
                            datatypeGenericVOs.add(datatypeGenericVO);
                            datatypeIdsSelected.add(datatype.getId());
                        }

                    } else {
                        throw new BusinessException(String.format("The leaf node must be of type ProjetSiteThemeDatatype but it's type is !", dataset.getLeafNode().getClass().getSimpleName()));
                    }
                }
            }
            List<ILeafTreeNode> projetSiteThemeDatatypes = projetSiteThemeDatatypeDAO.getAll();
            for (ILeafTreeNode pstd : projetSiteThemeDatatypes) {
                ProjetSiteThemeDatatype projetSiteThemeDatatype = (ProjetSiteThemeDatatype) pstd;
                ProjetSite projetSite = projetSiteThemeDatatype.getProjetSiteTheme().getProjetSite();
                Projet projet = projetSite.getProjet();
                Site site = projetSite.getSite();
                DataType datatype = pstd.getLeafNode().getDatatype();
                if (!datatypeIdsSelected.contains(datatype.getId()) && pstd.getLeafNode().getDatatype().getIsGeneric() && projet.getId().equals(idProjectSelected) && idsSites.contains(site.getId())) {
                    datatypeGenericVOs.add(new DatatypeGenericVO(datatype, Boolean.FALSE, null));
                }
            }

        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }

        LinkedList<DatatypeGenericVO> resultList = new LinkedList<>(datatypeGenericVOs);
        Collections.sort(resultList, new Comparator<DatatypeGenericVO>() {
            @Override
            public int compare(DatatypeGenericVO o1, DatatypeGenericVO o2) {
                return o1.getDatatype().getName().compareTo(o2.getDatatype().getName());
            }

        });
        return resultList;
    }

    public void setDatatypeVariableUniteDAO(IDatatypeVariableUniteDAO datatypeVariableUniteDAO) {
        this.datatypeVariableUniteDAO = datatypeVariableUniteDAO;
    }

}
