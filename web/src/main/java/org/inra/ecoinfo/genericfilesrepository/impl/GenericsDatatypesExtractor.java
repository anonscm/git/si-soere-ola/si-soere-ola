package org.inra.ecoinfo.genericfilesrepository.impl;

import org.inra.ecoinfo.extraction.IExtractor;

import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.config.impl.Extraction;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author Antoine Schellenberger
 */
public class GenericsDatatypesExtractor implements IExtractor {

    public static String CST_RESULT_EXTRACTION_CODE = "zooplanctonRawDatas";

    protected static final String MAP_INDEX_0 = "0";

    protected static final String PROPERTY_MSG_NO_EXTRACTION_RESULT = "MSG_NO_EXTRACTION_RESULT_RAW_DATAS";
    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.zooplancton.services.messages";

   

   

    @SuppressWarnings("rawtypes")
    @Override
    public void extract(IParameter parameters) throws BusinessException {
//        ((ZooplanctonParameters) parameters).getResults().put(CST_RESULT_EXTRACTION_CODE, filteredResultsDatasMap);
    }

    @Override
    public Extraction getExtraction() {
      return null;
    }

    @Override
    public void setExtraction(Extraction extraction) {
        
    }

    @Override
    public long getExtractionSize(IParameter parameters) {
        return 0;
    }

  

}
