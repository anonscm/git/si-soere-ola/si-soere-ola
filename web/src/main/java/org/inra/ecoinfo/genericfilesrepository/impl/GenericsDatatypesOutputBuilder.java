package org.inra.ecoinfo.genericfilesrepository.impl;

import com.google.common.io.Files;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.inra.ecoinfo.dataset.IDatasetManager;
import org.inra.ecoinfo.dataset.versioning.IDatasetDAO;
import org.inra.ecoinfo.dataset.versioning.IRepositoryManagerVersions;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;

import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.genericfilesrepository.api.ParametersExtractionGeneric;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.glacpe.extraction.vo.DatatypeGenericVO;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesRequestParamVO;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.projetsite.ProjetSite;
import org.inra.ecoinfo.glacpe.refdata.projetsitethemedatatype.ProjetSiteThemeDatatype;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.inra.ecoinfo.dataset.versioning.IVersionManager;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.glacpe.extraction.vo.AbstractDatesFormParam;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesYearsContinuousFormParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesYearsDiscretsFormParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesYearsRangeFormParamVO;
import org.inra.ecoinfo.security.entity.Role;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;

/**
 *
 * @author Antoine Schellenberger
 */
public class GenericsDatatypesOutputBuilder extends AbstractOutputBuilder {

    private IDatasetDAO datasetDAO;
    private IDatasetManager datasetManager;
    private IRepositoryManagerVersions repositoryManagerVersions;
    private IVersionManager versionManager;
    

    private static final String PROPERTY_MSG_HEADER_RAW = "MSG_HEADER_RAW_DATA_LINE";
    private static final String PROPERTY_MSG_ALL_STAGES_DEVELOPMENT = "MSG_ALL_STAGES_DEVELOPMENT";

    protected static final String BUNDLE_SOURCE_PATH_ZOOPLANKTON = "org.inra.ecoinfo.zooplancton.services.messages";

    protected static final String MAP_INDEX_0 = "0";

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException, NoExtractionResultException {

        List<Map<String, File>> listFilesMaps = ((DefaultParameter) parameters).getFilesMaps();
        Map<String, File> filesMap = new HashMap<>();
        
        
        
        try {
            listFilesMaps.add(filesMap);
            List<Site> sites = (List<Site>) parameters.getParameters().get(ParametersExtractionGeneric.KEY_MAP_SELECTION_SITES);
            List<Long> idsSites = new LinkedList<>();
            for (Site site : sites) {
                idsSites.add(site.getId());
            }
            DatesRequestParamVO datesRequestParam = (DatesRequestParamVO) parameters.getParameters().get(ParametersExtractionGeneric.KEY_MAP_SELECTION_DATE);
            Projet projetSelected = (Projet) parameters.getParameters().get(ParametersExtractionGeneric.KEY_MAP_SELECTION_PROJECT);
            List<DatatypeGenericVO> datatypes = (List<DatatypeGenericVO>) parameters.getParameters().get(ParametersExtractionGeneric.KEY_MAP_SELECTION_DATATYPE);
            List<Long> idsDatatypes = new LinkedList<>();

            for (DatatypeGenericVO datatype : datatypes) {
                idsDatatypes.add(datatype.getDatatype().getId());
            }

            List<Dataset> datasets = datasetDAO.getAll(Dataset.class);
            Set<Long> idsDatasetsProcessed = new HashSet<>();
            List<String> roles = new LinkedList<>();
            roles.add(Role.ROLE_EXTRACTION);

            for (Dataset dataset : datasets) {
                if (dataset.getLeafNode().getDatatype().getIsGeneric()) {
                    DataType datatype = dataset.getLeafNode().getDatatype();
                    if (dataset.getLeafNode() instanceof ProjetSiteThemeDatatype) {
                        if (idsDatatypes.contains(datatype.getId())) {
                            ProjetSiteThemeDatatype projetSiteThemeDatatype = (ProjetSiteThemeDatatype) dataset.getLeafNode();
                            ProjetSite projetSite = projetSiteThemeDatatype.getProjetSiteTheme().getProjetSite();
                            Projet projet = projetSite.getProjet();
                            Site site = projetSite.getSite();                            
                            if (projet.getId().equals(projet.getId()) && idsSites.contains(site.getId()) && dataset.getPublishDate() != null) {
                                AbstractDatesFormParam currentDatesFormParam = datesRequestParam.getCurrentDatesFormParam();
                                if (currentDatesFormParam instanceof DatesYearsDiscretsFormParamVO) {
                                    DatesYearsDiscretsFormParamVO datesYearsDiscretsFormParam = (DatesYearsDiscretsFormParamVO) currentDatesFormParam;
                                    String[] years = datesYearsDiscretsFormParam.getYears().trim().replaceAll(" ", "").split(",");
                                    for (String year : years) {
                                        if (!idsDatasetsProcessed.contains(dataset.getId())) {
                                            for (Map<String, String> period : datesYearsDiscretsFormParam.getPeriods()) {
                                                Date startDate = new SimpleDateFormat("dd/MM/yyyy").parse(String.format("%s/%s", period.get(DatesYearsDiscretsFormParamVO.START_INDEX), year));
                                                Date endDate = new SimpleDateFormat("dd/MM/yyyy").parse(String.format("%s/%s", period.get(DatesYearsDiscretsFormParamVO.END_INDEX), year));
                                                IntervalDate intervalDateRequest = new IntervalDate(startDate, endDate, new SimpleDateFormat("dd/MM/yyyy"));
                                                IntervalDate intervalDateDataset = new IntervalDate(dataset.getDateDebutPeriode(), dataset.getDateFinPeriode(), new SimpleDateFormat("dd/MM/yyyy"));
                                                if (intervalDateRequest.overlaps(intervalDateDataset)
                                                        || intervalDateDataset.isDateInInterval(startDate)
                                                        || intervalDateDataset.isDateInInterval(endDate)
                                                        || intervalDateDataset.overlaps(intervalDateRequest)) {
                                                    createGenericDatatypeEntryZip(idsDatasetsProcessed, dataset, filesMap);
                                                }

                                            }
                                        }
                                    }
                                }
                                if (currentDatesFormParam instanceof DatesYearsContinuousFormParamVO) {
                                    DatesYearsContinuousFormParamVO datesYearsContinuousFormParam = (DatesYearsContinuousFormParamVO) currentDatesFormParam;
                                    if (!idsDatasetsProcessed.contains(dataset.getId())) {
                                        for (Map<String, String> period : datesYearsContinuousFormParam.getPeriods()) {
                                            Date startDate = new SimpleDateFormat("dd/MM/yyyy").parse(period.get(datesYearsContinuousFormParam.START_INDEX));
                                            Date endDate = new SimpleDateFormat("dd/MM/yyyy").parse(period.get(datesYearsContinuousFormParam.END_INDEX));
                                            IntervalDate intervalDateRequest = new IntervalDate(startDate, endDate, new SimpleDateFormat("dd/MM/yyyy"));
                                            IntervalDate intervalDateDataset = new IntervalDate(dataset.getDateDebutPeriode(), dataset.getDateFinPeriode(), new SimpleDateFormat("dd/MM/yyyy"));
                                            if (intervalDateRequest.overlaps(intervalDateDataset)
                                                    || intervalDateDataset.isDateInInterval(startDate)
                                                    || intervalDateDataset.isDateInInterval(endDate)
                                                    || intervalDateDataset.overlaps(intervalDateRequest)) {
                                                createGenericDatatypeEntryZip(idsDatasetsProcessed, dataset, filesMap);
                                            }

                                        }
                                    }
                                }

                                if (currentDatesFormParam instanceof DatesYearsRangeFormParamVO) {
                                    DatesYearsRangeFormParamVO datesYearsRangeFormParam = (DatesYearsRangeFormParamVO) currentDatesFormParam;
                                    List<Map<String, String>> yearsLists = datesYearsRangeFormParam.getYears();
                                    for (Map<String, String> yearMap : yearsLists) {
                                        Integer yearStart = Integer.parseInt(yearMap.get(DatesYearsRangeFormParamVO.START_INDEX));
                                        Integer yearEnd = Integer.parseInt(yearMap.get(DatesYearsRangeFormParamVO.END_INDEX));

                                        if (!idsDatasetsProcessed.contains(dataset.getId())) {
                                            for (Map<String, String> period : datesYearsRangeFormParam.getPeriods()) {
                                                Date startDate = new SimpleDateFormat("dd/MM/yyyy").parse(String.format("%s/%d", period.get(DatesYearsDiscretsFormParamVO.START_INDEX), yearStart));
                                                Date endDate = new SimpleDateFormat("dd/MM/yyyy").parse(String.format("%s/%d", period.get(DatesYearsDiscretsFormParamVO.END_INDEX), yearEnd));
                                                IntervalDate intervalDateRequest = new IntervalDate(startDate, endDate, new SimpleDateFormat("dd/MM/yyyy"));
                                                IntervalDate intervalDateDataset = new IntervalDate(dataset.getDateDebutPeriode(), dataset.getDateFinPeriode(), new SimpleDateFormat("dd/MM/yyyy"));
                                                if (intervalDateRequest.overlaps(intervalDateDataset)
                                                        || intervalDateDataset.isDateInInterval(startDate)
                                                        || intervalDateDataset.isDateInInterval(endDate)
                                                        || intervalDateDataset.overlaps(intervalDateRequest)) {
                                                    createGenericDatatypeEntryZip(idsDatasetsProcessed, dataset, filesMap);
                                                }

                                            }
                                        }
                                    }

                                }
                            }
                        }
                    } else {
                        throw new BusinessException(String.format("The leaf node must be of type ProjetSiteThemeDatatype but it's type is !", dataset.getLeafNode().getClass().getSimpleName()));
                    }
                }
            }

//        add(buildOutput(parameters, ZooplanctonRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE));
            System.out.println(parameters.getParameters());
        } catch (PersistenceException persistenceException) {
            throw new BusinessException(persistenceException);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GenericsDatatypesOutputBuilder.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(GenericsDatatypesOutputBuilder.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(GenericsDatatypesOutputBuilder.class.getName()).log(Level.SEVERE, null, ex);
        } catch (BadExpectedValueException ex) {
            Logger.getLogger(GenericsDatatypesOutputBuilder.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    private void createGenericDatatypeEntryZip(Set<Long> idsDatasetsProcessed, Dataset dataset, Map<String, File> filesMap) throws BusinessException, FileNotFoundException, IOException {
        idsDatasetsProcessed.add(dataset.getId());
        final VersionFile publishVersion = dataset.getPublishVersion();
        byte[] data = repositoryManagerVersions.getData(publishVersion);
        File tempDirectory = Files.createTempDir();
        
        tempDirectory.deleteOnExit();
        
        File tempFile = new File(tempDirectory, versionManager.getDownloadFileName(publishVersion));
        OutputStream os = new FileOutputStream(tempFile);
        os.write(data);
        os.flush();
        os.close();
        filesMap.put(dataset.getId().toString(), tempFile);
    }

    @SuppressWarnings("rawtypes")
    @Override
    public Map<String, File> buildOutput(final IParameter parameters, final String cstResultExtractionCode) throws BusinessException, NoExtractionResultException {

//        final Map<String, List> results = parameters.getResults().get(cstResultExtractionCode);
//        if (results == null || (results != null && results.get(AbstractOutputBuilder.MAP_INDEX_0).isEmpty())) {
//            throw new NoExtractionResultException(localizationManager.getMessage(NoExtractionResultException.BUNDLE_SOURCE_PATH, NoExtractionResultException.ERROR));
//        }
//        final String resultsHeader = buildHeader(parameters.getParameters());
        return buildBody(null, null, parameters.getParameters());
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {
        // List<VariableVO> selectedVariables = (List<VariableVO>) requestMetadatasMap.get(ZooplanctonParameters.KEY_MAP_SELECTION_VARIABLES);
/*
        List<ResultExtractionZooplanctonRawsDatas> resultsExtraction = resultsDatasMap.get(MAP_INDEX_0);

        List<Taxon> selectedTaxons = (List<Taxon>) requestMetadatasMap.get(ZooplanctonParameters.KEY_MAP_SELECTION_TAXONS);
        Boolean isDetailTaxon = (Boolean) requestMetadatasMap.get(ZooplanctonParameters.KEY_MAP_DETAIL_TAXON);
        Boolean isDetailStageDevelopment = (Boolean) requestMetadatasMap.get(ZooplanctonParameters.KEY_MAP_DETAIL_DEVELOPMENT_STAGE);

        Properties propertiesNameProjet = localizationManager.newProperties(Projet.NAME_TABLE, "nom");
        Properties propertiesNamePlateforme = localizationManager.newProperties(Plateforme.TABLE_NAME, "nom");
        Properties propertiesNameSite = localizationManager.newProperties(Site.NAME_ENTITY_JPA, "nom");
        Properties propertiesNameOutil = localizationManager.newProperties(OutilsMesure.TABLE_NAME, "nom");
        String msgAllStages = getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_ZOOPLANKTON, PROPERTY_MSG_ALL_STAGES_DEVELOPMENT);

        List<PlateformeVO> selectedPlateformes = (List<PlateformeVO>) requestMetadatasMap.get(ZooplanctonParameters.KEY_MAP_SELECTION_PLATEFORMES);
        Set<String> sitesNames = retrieveSitesCodes(selectedPlateformes);

        Map<String, List<Taxon>> mapParenteTaxonsLeaves = Maps.newHashMap();

        buildAndSaveMapParenteTaxonsLeaves(selectedTaxons, selectedTaxons, mapParenteTaxonsLeaves);

        Map<String, List<Taxon>> mapParenteTaxonsSelected = buildMapParenteTaxonsSelected(selectedTaxons);

        Map<KeyMapExtractionL1Zooplancton, Map<KeyMapExtractionL2Zooplancton, Map<KeyMapExtractionL3Zooplancton, List<ResultExtractionZooplanctonRawsDatas>>>> resultsStructuredMap = buildResultsStructuredExtraction(resultsExtraction,
                mapParenteTaxonsLeaves);

        // Map<Date, Map<String, Map<Long, List<MesurePhytoplancton>>>> sumBiovolumeMap = new HashMap<Date, Map<String, Map<Long, List<MesurePhytoplancton>>>>();
        Map<String, File> filesMap = buildOutputsFiles(sitesNames, SUFFIX_FILENAME_DEFAULT);

        Map<String, PrintStream> outputPrintStreamMap = buildOutputPrintStreamMap(filesMap);
        for (String siteName : outputPrintStreamMap.keySet()) {
            outputPrintStreamMap.get(siteName).println(headers);
        }

        // On construit la liste et on la trie à partir des clés
        List<KeyMapExtractionL1Zooplancton> listKeyMapL1 = new LinkedList<>(resultsStructuredMap.keySet());
        Collections.sort(listKeyMapL1);

        for (KeyMapExtractionL1Zooplancton keyMapL1 : listKeyMapL1) {
            String localizedNameProjet = lookupLocalizedProperty(propertiesNameProjet, keyMapL1.getProjet());
            String localizedNameSite = lookupLocalizedProperty(propertiesNameSite, keyMapL1.getSite());
            String localizedNamePlateforme = lookupLocalizedProperty(propertiesNamePlateforme, keyMapL1.getPlateforme());
            String localizedNameOutilPrelevement = lookupLocalizedProperty(propertiesNameOutil, keyMapL1.getOutilPrelevement());
            String localizedNameOutilMesure = lookupLocalizedProperty(propertiesNameOutil, keyMapL1.getOutilMesure());

            Date date = keyMapL1.getDate();
            Map<KeyMapExtractionL2Zooplancton, Map<KeyMapExtractionL3Zooplancton, List<ResultExtractionZooplanctonRawsDatas>>> mapTaxons = resultsStructuredMap.get(keyMapL1);

            List<KeyMapExtractionL2Zooplancton> listKeyMapL2 = new LinkedList<>(mapTaxons.keySet());
            Collections.sort(listKeyMapL2);
            PrintStream rawDataPrintStream = outputPrintStreamMap.get(Utils.createCodeFromString(keyMapL1.getSite()));

            mainProcessOuput(isDetailTaxon, isDetailStageDevelopment, msgAllStages, mapParenteTaxonsSelected, keyMapL1, localizedNameProjet, localizedNameSite, localizedNamePlateforme, localizedNameOutilPrelevement, localizedNameOutilMesure, date,
                    mapTaxons, listKeyMapL2, rawDataPrintStream);

        }

        closeStreams(outputPrintStreamMap);
        return filesMap;
         */
        return null;
    }

    @Override
    protected String buildHeader(Map<String, Object> requestMetadatasMap) throws BusinessException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setDatasetManager(IDatasetManager datasetManager) {
        this.datasetManager = datasetManager;
    }

    public void setDatasetDAO(IDatasetDAO datasetDAO) {
        this.datasetDAO = datasetDAO;
    }

    public void setRepositoryManagerVersions(IRepositoryManagerVersions repositoryManagerVersions) {
        this.repositoryManagerVersions = repositoryManagerVersions;
    }

    public void setVersionManager(IVersionManager versionManager) {
        this.versionManager = versionManager;
    }

}
