package org.inra.ecoinfo.genericfilesrepository.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.genericfilesrepository.api.ParametersExtractionGeneric;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.glacpe.extraction.vo.DatatypeGenericVO;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesRequestParamVO;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public class GenericsDatatypesRequestReminderOutputBuilder extends AbstractOutputBuilder {

    private static final String DATATYPE_ZOOPLANKTON = "Zooplancton";

    /**
     *
     */
    public static String CST_RESULT_EXTRACTION_CODE = "ZooplanctonRequestReminder";

    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.genericfilesrepository.impl.messages";

    private static final String PROPERTY_MSG_EXTRACTION_COMMENTS = "MSG_EXTRACTION_COMMENTS";

    private static final String PROPERTY_MSG_HEADER = "MSG_HEADER";

    private static final String PATTERN_STRING_SITES_SUMMARY = "   * %s";
    private static final String PATTERN_STRING_DATATYPES_SUMMARY = "   * %s";
    private static final String PATTERN_STRING_COMMENTS_SUMMARY = "     %s";
    private static final String PATTERN_STRING_PROJECT_SUMMARY = "   * %s";

    private static final String PROPERTY_MSG_SELECTED_PERIODS = "MSG_SELECTED_PERIODS";
    private static final String PROPERTY_MSG_SELECTED_DATATYPES = "MSG_SELECTED_DATATYPES";
    private static final String PROPERTY_MSG_SITE = "MSG_SITE";
    private static final String PROPERTY_MSG_SELECTED_SITES = "MSG_SELECTED_SITES";
    private static final String PROPERTY_MSG_SELECTED_PROJECT = "MSG_SELECTED_PROJECT";

    private static final String KEYMAP_COMMENTS = "comments";

    @Override
    protected String buildHeader(Map<String, Object> requestMetadatas) throws BusinessException {
        return getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_HEADER);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {

        Map<String, File> reminderMap = new HashMap<String, File>();
        File reminderFile = buildOutputFile(FILENAME_REMINDER, EXTENSION_TXT);
        PrintStream reminderPrintStream;

        try {

            reminderPrintStream = new PrintStream(reminderFile, CHARACTER_ENCODING_ISO_8859_1);
            reminderPrintStream.println(headers);
            reminderPrintStream.println();

            printProjectSummary((Projet) requestMetadatasMap.get(ParametersExtractionGeneric.KEY_MAP_SELECTION_PROJECT), reminderPrintStream);
            printSitesSummary((List<Site>) requestMetadatasMap.get(ParametersExtractionGeneric.KEY_MAP_SELECTION_SITES), reminderPrintStream);
            printDatesSummary((DatesRequestParamVO) requestMetadatasMap.get(ParametersExtractionGeneric.KEY_MAP_SELECTION_DATE), reminderPrintStream);
            printDatatypesSummary((List<DatatypeGenericVO>) requestMetadatasMap.get(ParametersExtractionGeneric.KEY_MAP_SELECTION_DATATYPE), reminderPrintStream);
//            printTaxonsSummary((List<Taxon>) requestMetadatasMap.get(ZooplanctonParameters.KEY_MAP_SELECTION_TAXONS), reminderPrintStream);
//            printCheckBoxesDetails((Boolean) requestMetadatasMap.get(ZooplanctonParameters.KEY_MAP_DETAIL_DEVELOPMENT_STAGE), (Boolean) requestMetadatasMap.get(ZooplanctonParameters.KEY_MAP_DETAIL_TAXON), reminderPrintStream);
            reminderPrintStream.println(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_EXTRACTION_COMMENTS));
            reminderPrintStream.println(String.format(PATTERN_STRING_COMMENTS_SUMMARY, requestMetadatasMap.get(KEYMAP_COMMENTS)));

        } catch (FileNotFoundException e) {
            AbstractOutputBuilder.LOGGER.error(e);
            throw new BusinessException(e);
        } catch (UnsupportedEncodingException e) {
            AbstractOutputBuilder.LOGGER.error(e);
            throw new BusinessException(e);
        }
        reminderMap.put(FILENAME_REMINDER, reminderFile);

        reminderPrintStream.flush();
        reminderPrintStream.close();

        return reminderMap;
    }

    private void printDatesSummary(DatesRequestParamVO datesRequestParamVO, PrintStream reminderPrintStream) throws BusinessException {
        reminderPrintStream.println(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_SELECTED_PERIODS));
        try {
            datesRequestParamVO.getCurrentDatesFormParam().buildSummary(reminderPrintStream);
        } catch (ParseException e) {
            AbstractOutputBuilder.LOGGER.error(e);
            throw new BusinessException(e);
        }

        reminderPrintStream.println();
    }

    private void printSitesSummary(List<Site> list, PrintStream reminderPrintStream) {

        Properties propertiesNomSite = getLocalizationManager().newProperties(Site.NAME_ENTITY_JPA, "nom");
        reminderPrintStream.println(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_SELECTED_SITES));
        for (Site site : list) {
            reminderPrintStream.println(String.format(PATTERN_STRING_SITES_SUMMARY, propertiesNomSite.getProperty(site.getNom(), site.getNom()), getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_SITE)));
        }
        reminderPrintStream.println();
    }

    private void printProjectSummary(Projet project, PrintStream reminderPrintStream) {

        Properties propertiesNomProject = getLocalizationManager().newProperties(Projet.NAME_TABLE, "nom");
        reminderPrintStream.println(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_SELECTED_PROJECT));
        reminderPrintStream.println(String.format(PATTERN_STRING_PROJECT_SUMMARY, propertiesNomProject.getProperty(project.getNom(), project.getNom())));

        reminderPrintStream.println();
    }

    private void printDatatypesSummary(List<DatatypeGenericVO> list, PrintStream reminderPrintStream) {

        Properties propertiesNomDatatype = getLocalizationManager().newProperties(DataType.NAME_ENTITY_JPA, "name");
        reminderPrintStream.println(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_SELECTED_DATATYPES));

        for (DatatypeGenericVO datatype : list) {
            reminderPrintStream.println(String.format(PATTERN_STRING_DATATYPES_SUMMARY, propertiesNomDatatype.getProperty(datatype.getDatatype().getName(), datatype.getDatatype().getName())));
        }
        reminderPrintStream.println();
    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException, NoExtractionResultException {
        ((DefaultParameter) parameters).getFilesMaps().add(super.buildOutput(parameters, CST_RESULT_EXTRACTION_CODE));
        return null;
    }
}
