package org.inra.ecoinfo.genericfilesrepository.impl;

import java.util.Date;
import java.util.List;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author antoine Schellenberger
 */
public interface IGenericDatatypeDAO extends IDAO<Object> {

   
    /**
     *
     * @param plateformesIds
     * @param firstDate
     * @param endDate
     * @return
     * @throws PersistenceException
     */
    List<VariableGLACPE> retrieveAvailablesVariables(List<Long> plateformesIds, Date firstDate, Date endDate) throws PersistenceException;

    /**
     *
     * @param projetSiteId
     * @return
     * @throws PersistenceException
     */
    List<Plateforme> retrieveAvailablesPlateformesByProjetId(long projetSiteId) throws PersistenceException;

}
