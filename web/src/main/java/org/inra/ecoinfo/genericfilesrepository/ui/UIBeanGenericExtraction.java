package org.inra.ecoinfo.genericfilesrepository.ui;

import java.io.Serializable;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.inra.ecoinfo.dataset.security.ISecurityDatasetManager;
import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.filecomp.IFileCompManager;
import org.inra.ecoinfo.glacpe.extraction.jsf.AbstractUIBeanForSteps;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.VariableVO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.notifications.INotificationsManager;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.refdata.valeurqualitative.ValeurQualitative;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.security.ISecurityContext;
import org.inra.ecoinfo.security.entity.Role;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.genericfilesrepository.api.ParametersExtractionGeneric;
import org.inra.ecoinfo.genericfilesrepository.api.IGenericDatatypeManager;
import org.inra.ecoinfo.filecomp.ItemFile;
import org.inra.ecoinfo.filecomp.entity.FileComp;
import org.inra.ecoinfo.glacpe.extraction.vo.DatatypeGenericVO;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;

/**
 *
 * @author Antoine Schellenberger
 */
@ManagedBean(name = "uiGenericExtraction")
@ViewScoped
public class UIBeanGenericExtraction extends AbstractUIBeanForSteps implements Serializable {

    private static final String RULE_JSF_GENERIC_EXTRACTION = "genericExtraction";
    private static final long serialVersionUID = 1L;

    private int affichage = 1;

    @ManagedProperty(value = "#{extractionManager}")
    protected IExtractionManager extractionManager;

    private Boolean filter = false;

    private Long idSiteSelected;

    private Long idProjectSelected;

    private Long idDatatypeSelected;

    private String lengthOrder;

    private String lengthValue;

    @ManagedProperty(value = "#{localizationManager}")
    protected ILocalizationManager localizationManager;

    @ManagedProperty(value = "#{notificationsManager}")
    protected INotificationsManager notificationsManager;

    private ParametersRequest parametersRequest = new ParametersRequest();

    private Map<Long, ProjetVO> projectsAvailables = new HashMap<Long, UIBeanGenericExtraction.ProjetVO>();

    private Properties propertiesPlatformNames;

    private Properties propertiesProjectsNames;

    private Properties propertiesQualitativeNames;

    private Properties propertiesSiteNames;

    private Properties propertiesVariableNames;

    private Properties propertiesDatatypesNames;

    /**
     *
     */
    @ManagedProperty(value = "#{securityContext}")
    protected ISecurityContext securityContext;

    /**
     *
     */
    @ManagedProperty(value = "#{transactionManager}")
    protected JpaTransactionManager transactionManager;

    private Map<Long, VODatatypeJSF> datatypesAvailables = new HashMap<>();
    private Map<Long, VODatatypeJSF> datatypesAvailablesForExtraction = new HashMap<>();

    private Map<Long, VODatatypeJSF> datatypesNonStillAvailables = new HashMap<>();

    /**
     *
     */
    @ManagedProperty(value = "#{genericDatatypeManager}")
    protected IGenericDatatypeManager genericDatatypeManager;

    /**
     *
     */
    @ManagedProperty(value = "#{fileCompManager}")
    protected IFileCompManager fileCompManager;

    /**
     *
     */
    public UIBeanGenericExtraction() {
    }

    /**
     *
     * @return
     */
    public String addAllDatatypes() {
        Map<Long, VODatatypeJSF> datatypesSelected = parametersRequest.getDatatypeSelected();
        datatypesSelected.clear();

        for (VODatatypeJSF datatype : datatypesAvailables.values()) {
            if (datatype.getDatatype().getIsDataAvailable()) {
                datatypesSelected.put(datatype.getDatatype().getDatatype().getId(), datatype);
                datatype.setSelected(true);
            }

        }
        return null;
    }

    /**
     *
     * @return @throws BusinessException
     */
    public String changeProject() throws BusinessException {
        parametersRequest.getSitesSelected().clear();
        for (ProjetVO projectSelected : projectsAvailables.values()) {
            for (SiteVO siteSelected : projectSelected.getSites().values()) {
                siteSelected.setSelected(false);
            }
        }

        return null;
    }

    private void createOrUpdateListDatatypesAvailables() throws BusinessException, ParseException {
        datatypesAvailables.clear();
        parametersRequest.getDatatypeSelected().clear();

        if (!parametersRequest.sitesSelected.isEmpty()) {

//            List<VariableVO> variables = genericDatatypeManager.retrieveListsVariables(new LinkedList<Long>(parametersRequest.plateformesSelected.keySet()), parametersRequest.getFirstStartDate(), parametersRequest.getLastEndDate());
//            List<DataType> datatypes = genericDatatypeManager.retrieveAvailablesGenericDatatypes();
            List<Long> idsSites = new LinkedList<>();

            for (SiteVO site : parametersRequest.sitesSelected.values()) {
                idsSites.add(site.getSite().getId());
            }

            List<DatatypeGenericVO> datatypes = genericDatatypeManager.retrieveAvailablesGenericDatatypes(idProjectSelected, idsSites, Boolean.TRUE);
            for (SiteVO site : parametersRequest.sitesSelected.values()) {

                for (DatatypeGenericVO datatypeGenericVO : datatypes) {
                    DataType datatype = datatypeGenericVO.getDatatype();
                    if (securityContext.isRoot()
                            //                            || securityContext.matchPrivilege(String.format("*/%s/*/zooplancton/%s|*/%s/*/zooplancton", plateforme.getPlateforme().getSite().getCode(), variable.getCode(), plateforme.getPlateforme().getSite().getCode()),
                            || securityContext.matchPrivilege(String.format("*/%s/*/%s/*", site.getSite().getCode(), datatype.getCode()),
                                    Role.ROLE_EXTRACTION, ISecurityDatasetManager.PRIVILEGE_TYPE_DATASET)) {
                        if (!datatypesAvailables.keySet().contains(datatype.getId())) {
                            VODatatypeJSF datatypeVO = new VODatatypeJSF(datatypeGenericVO);
                            datatypesAvailables.put(datatype.getId(), datatypeVO);
                            if (datatypeGenericVO.getIsDataAvailable()) {
                                datatypesAvailablesForExtraction.put(datatype.getId(), datatypeVO);
                            } else {
                                datatypesNonStillAvailables.put(datatype.getId(), datatypeVO);
                            }
                        }
                    }
                }
            }
        }
    }

    private List<String> getPathes(String projet, List<Site> sites, List<DatatypeGenericVO> datatypes) {
        String sitePath, path;

        List<String> pathes = new LinkedList<>();
        for (Site site : sites) {
            sitePath = site.getPath();

            for (DatatypeGenericVO datatype : datatypes) {
                path = String.format("%s/%s/*/%s/*", projet, sitePath, datatype.getDatatype().getCode());
                if (!pathes.contains(path)) {
                    pathes.add(path);
                }
            }
        }
        return pathes;
    }

    /**
     *
     * @return @throws BusinessException
     */
    public String extract() throws BusinessException {

        Map<String, Object> metadatasMap = new HashMap<String, Object>();

        List<Site> sites = new LinkedList<Site>();
        for (SiteVO site : parametersRequest.getSitesSelected().values()) {
            if (!sites.contains(site.getSite())) {
                sites.add(site.getSite());
            }

        }
        metadatasMap.put(ParametersExtractionGeneric.KEY_MAP_SELECTION_SITES, sites);

        List<DatatypeGenericVO> datatypes = buildParameterExtractionDatatypes();

        List<FileComp> listFiles = new LinkedList<>();
        try {
            if (parametersRequest.getFormIsValid()) {
                IntervalDate intervalDate;

                intervalDate = new IntervalDate(parametersRequest.getFirstStartDate(), parametersRequest.getLastEndDate(), DateUtil.getSimpleDateFormatDateLocale());

                List<IntervalDate> intervalsDate = new LinkedList<>();
                intervalsDate.add(intervalDate);
                
                final List<ItemFile> files = fileCompManager.getFileCompFromRegExPathsAndIntervalsDate(getPathes(projectsAvailables.get(idProjectSelected).getProjet().getCode(), sites, datatypes), intervalsDate);
                for (final ItemFile file : files) {
                    if (file.getMandatory()) {
                        listFiles.add(file.getFileComp());
                    }
                }
            }
        } catch (BadExpectedValueException e) {
            throw new BusinessException("bad file", e);
        } catch (ParseException e) {
            throw new BusinessException("bad file name", e);
        }
        metadatasMap.put(FileComp.class.getSimpleName(), listFiles);

        parametersRequest.getDatesRequestParam().getDatesYearsDiscretsFormParam().setPeriods(parametersRequest.getDatesRequestParam().getNonPeriodsYearsContinuous());
        parametersRequest.getDatesRequestParam().getDatesYearsRangeFormParam().setPeriods(parametersRequest.getDatesRequestParam().getNonPeriodsYearsContinuous());

        metadatasMap.put(ParametersExtractionGeneric.KEY_MAP_SELECTION_DATE, parametersRequest.getDatesRequestParam());
        metadatasMap.put(ParametersExtractionGeneric.KEY_MAP_SELECTION_ID_PROJECT, projectsAvailables.get(idProjectSelected).getProjet().getId());
        metadatasMap.put(ParametersExtractionGeneric.KEY_MAP_SELECTION_PROJECT, projectsAvailables.get(idProjectSelected).getProjet());
        metadatasMap.put(ParametersExtractionGeneric.KEY_MAP_SELECTION_DATATYPE, datatypes);

        metadatasMap.put(IExtractionManager.KEYMAP_COMMENTS, parametersRequest.getCommentExtraction());

        IParameter parameters = new ParametersExtractionGeneric(metadatasMap);
        extractionManager.extract(parameters, getAffichage());

        return null;
    }

//    private List<VariableVO> buildParameterExtractionVariables() {
//        List<VariableVO> variablesVO = new LinkedList<VariableVO>();
//        for (VOVariableJSF variable : parametersRequest.getListDatatypesSelected()) {
//            variablesVO.add(variable.getVariable());
//        }
//        return variablesVO;
//    }
//    
    private List<DatatypeGenericVO> buildParameterExtractionDatatypes() {
        List<DatatypeGenericVO> datatypes = new LinkedList<>();
        for (VODatatypeJSF datatype : parametersRequest.getListDatatypesSelected()) {
            datatypes.add(datatype.getDatatype());
        }
        return datatypes;
    }

    /**
     *
     * @return
     */
    public int getAffichage() {
        return affichage;
    }

    /**
     *
     * @return
     */
    public Boolean getFilter() {
        return filter;
    }

    /**
     *
     * @return
     */
    public String getIdJSFForm() {
        return ("formZooplancton");
    }

    /**
     *
     * @return
     */
    public Long getIdProjectSelected() {
        return idProjectSelected;
    }

    /**
     *
     * @return
     */
    @Override
    public boolean getIsStepValid() {
        if (getStep() == 1) {
            return getParametersRequest().getSitesStepIsValid();
        } else if (getStep() == 2) {
            return getParametersRequest().getDateStepIsValid();
        }
        return false;
    }

    /**
     *
     * @return
     */
    public String getLengthOrder() {
        return lengthOrder;
    }

    /**
     *
     * @return
     */
    public String getLengthValue() {
        return lengthValue;
    }

    /**
     *
     * @return
     */
    public ParametersRequest getParametersRequest() {
        return parametersRequest;
    }

    /**
     *
     * @return @throws BusinessException
     */
    public Map<Long, ProjetVO> getProjectsAvailables() throws BusinessException {
        TransactionStatus status = transactionManager.getTransaction(new DefaultTransactionDefinition(TransactionDefinition.PROPAGATION_REQUIRES_NEW));
        if (projectsAvailables.isEmpty()) {

            List<Projet> projets = genericDatatypeManager.retrieveAvailablesProjets();
            List<DataType> genericDatatypes = genericDatatypeManager.retrieveAvailablesGenericDatatypes();
            for (Projet projet : projets) {
                List<Site> sites = Lists.newArrayList();

                for (Site site : genericDatatypeManager.retrieveAvailablesSitesByProjetId(projet.getId())) {
                    for (DataType datatype : genericDatatypes) {
                        if (!sites.contains(site)
                                && (securityContext.isRoot() || securityContext.matchPrivilege(
                                        //                                    String.format("%s/%s/*/zooplancton/*|%s/%s/*/zooplancton", projet.getCode(), plateforme.getSite().getCode(), projet.getCode(), plateforme.getSite().getCode()), Role.ROLE_EXTRACTION,
                                        String.format("%s/%s/*/%s/*", projet.getCode(), site.getCode(), datatype.getCode(), projet.getCode(), site.getCode(), datatype.getCode()), Role.ROLE_EXTRACTION,
                                        ISecurityDatasetManager.PRIVILEGE_TYPE_DATASET))) {
                            sites.add(site);
                        }
                    }

                }
                if (!sites.isEmpty()) {
                    ProjetVO projectVO = new ProjetVO(projet, sites);
                    projectsAvailables.put(projet.getId(), projectVO);
                }
            }

        }

        transactionManager.commit(status);
        return projectsAvailables;
    }

    // GETTERS SETTERS - BEANS
    /**
     *
     * @return @throws BusinessException
     */
    public String getProjetSelected() throws BusinessException {
        String localizedProjetSelected = null;
        if (idProjectSelected != null) {
            String nom = projectsAvailables.get(idProjectSelected).getProjet().getNom();
            localizedProjetSelected = getPropertiesProjectsNames().getProperty(nom);
            if (Strings.isNullOrEmpty(localizedProjetSelected)) {
                localizedProjetSelected = nom;
            }
            return localizedProjetSelected;
        }
        return localizedProjetSelected;
    }

    /**
     *
     * @return
     */
    public Properties getPropertiesPlatformNames() {
        if (this.propertiesPlatformNames == null) {
            this.setPropertiesPlatformNames(localizationManager.newProperties(Plateforme.TABLE_NAME, "nom"));
        }
        return propertiesPlatformNames;
    }

    /**
     *
     * @return
     */
    public Properties getPropertiesProjectsNames() {
        if (this.propertiesProjectsNames == null) {
            this.setPropertiesProjectsNames(localizationManager.newProperties(Projet.NAME_TABLE, "nom"));
        }
        return propertiesProjectsNames;
    }

    /**
     *
     * @return
     */
    public Properties getPropertiesQualitativeNames() {
        if (this.propertiesQualitativeNames == null) {
            this.setPropertiesQualitativeNames(localizationManager.newProperties(ValeurQualitative.NAME_ENTITY_JPA, "value"));
        }
        return propertiesQualitativeNames;
    }

    /**
     *
     * @return
     */
    public Properties getPropertiesSiteNames() {
        if (this.propertiesSiteNames == null) {
            this.setPropertiesSiteNames(localizationManager.newProperties(Site.NAME_ENTITY_JPA, "nom"));
        }
        return propertiesSiteNames;
    }

    /**
     *
     * @return
     */
    public Properties getPropertiesVariableNames() {
        if (this.propertiesVariableNames == null) {
            this.setPropertiesVariableNames(localizationManager.newProperties(Variable.NAME_ENTITY_JPA, "nom"));
        }
        return propertiesVariableNames;
    }

    /**
     *
     * @return
     */
    public Properties getPropertiesDatatypesNames() {
        if (this.propertiesDatatypesNames == null) {
            this.setPropertiesDatatypesNames(localizationManager.newProperties(DataType.NAME_ENTITY_JPA, "name"));
        }
        return propertiesDatatypesNames;
    }

    /**
     *
     * @return
     */
    public String getUpdatePropriete() {
        return null;
    }

//    /**
//     *
//     * @return @throws BusinessException
//     * @throws ParseException
//     */
//    public List<VOVariableJSF> getVariablesAvailables() throws BusinessException, ParseException {
//        if (variablesAvailables.isEmpty() && parametersRequest.getDateStepIsValid()) {
//            createOrUpdateListVariablesAvailables();
//        }
//        return new LinkedList<VOVariableJSF>(variablesAvailables.values());
//
//    }
    /**
     *
     * @return
     */
    public String navigate() {
        return RULE_JSF_GENERIC_EXTRACTION;
    }

    /**
     *
     * @return
     */
    @Override
    public String nextStep() {
        super.nextStep();
        switch (getStep()) {
            case 3:
                try {
                    createOrUpdateListDatatypesAvailables();
                } catch (BusinessException e) {
                    datatypesAvailables.clear();
                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                break;
            default:
                break;
        }
        return null;
    }

//    /**
//     *
//     * @return @throws BusinessException
//     * @throws ParseException
//     */
//    public String removeAllVariables() throws BusinessException, ParseException {
//        createOrUpdateListVariablesAvailables();
//        return null;
//    }
    /**
     *
     * @return @throws BusinessException
     * @throws ParseException
     */
    public String removeAllDatatypes() throws BusinessException, ParseException {
        createOrUpdateListDatatypesAvailables();
        return null;
    }

    public void setIdSiteSelected(Long idSiteSelected) {
        this.idSiteSelected = idSiteSelected;
    }

    /**
     *
     * @return @throws BusinessException
     */
    public String selectSite() throws BusinessException {
        ProjetVO projectSelected = projectsAvailables.get(idProjectSelected);
        SiteVO siteSelected = projectSelected.getSites().get(idSiteSelected);
        if (siteSelected.getSelected()) {
            parametersRequest.getSitesSelected().remove(idSiteSelected);
            siteSelected.setSelected(false);
        } else {
            parametersRequest.getSitesSelected().put(idSiteSelected, siteSelected);
            siteSelected.setSelected(true);
        }

        return null;
    }

    /**
     *
     * @return
     */
    public String selectDatatype() {
        VODatatypeJSF datatypeSelected = datatypesAvailables.get(idDatatypeSelected);

        if (datatypeSelected.getSelected()) {
            parametersRequest.getDatatypeSelected().remove(idDatatypeSelected);
        } else {
            parametersRequest.getDatatypeSelected().put(idDatatypeSelected, datatypeSelected);
        }
        datatypeSelected.setSelected(!datatypeSelected.getSelected());
        return null;
    }

    /**
     *
     * @param affichage
     */
    public void setAffichage(int affichage) {
        this.affichage = affichage;
    }

    /**
     *
     * @param extractionManager
     */
    public void setExtractionManager(IExtractionManager extractionManager) {
        this.extractionManager = extractionManager;
    }

    /**
     *
     * @param filter
     */
    public void setFilter(Boolean filter) {
        this.filter = filter;
    }

    /**
     *
     * @param idPlateformeSelected
     */
    public void setIdPlateformeSelected(Long idPlateformeSelected) {
        this.idSiteSelected = idPlateformeSelected;
    }

    /**
     *
     * @param idProjectSelected
     */
    public void setIdProjectSelected(Long idProjectSelected) {
        this.idProjectSelected = idProjectSelected;
    }

    /**
     *
     * @param idDatatypeSelected
     */
    public void setIdDatatypeSelected(Long idDatatypeSelected) {
        this.idDatatypeSelected = idDatatypeSelected;
    }

    /**
     *
     * @param lengthOrder
     */
    public void setLengthOrder(String lengthOrder) {
        this.lengthOrder = lengthOrder;
    }

    /**
     *
     * @param lengthValue
     */
    public void setLengthValue(String lengthValue) {
        this.lengthValue = lengthValue;
    }

    /**
     *
     * @param localizationManager
     */
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    /**
     *
     * @param notificationsManager
     */
    public void setNotificationsManager(INotificationsManager notificationsManager) {
        this.notificationsManager = notificationsManager;
    }

    /**
     *
     * @param propertiesPlatformNames
     */
    public void setPropertiesPlatformNames(Properties propertiesPlatformNames) {
        this.propertiesPlatformNames = propertiesPlatformNames;
    }

    /**
     *
     * @param propertiesProjectsNames
     */
    public void setPropertiesProjectsNames(Properties propertiesProjectsNames) {
        this.propertiesProjectsNames = propertiesProjectsNames;
    }

    /**
     *
     * @param propertiesQualitativeNames
     */
    public void setPropertiesQualitativeNames(Properties propertiesQualitativeNames) {
        this.propertiesQualitativeNames = propertiesQualitativeNames;
    }

    /**
     *
     * @param propertiesSiteNames
     */
    public void setPropertiesSiteNames(Properties propertiesSiteNames) {
        this.propertiesSiteNames = propertiesSiteNames;
    }

    /**
     *
     * @param propertiesVariableNames
     */
    public void setPropertiesVariableNames(Properties propertiesVariableNames) {
        this.propertiesVariableNames = propertiesVariableNames;
    }

    /**
     *
     * @param securityContext
     */
    public void setSecurityContext(ISecurityContext securityContext) {
        this.securityContext = securityContext;
    }

    /**
     *
     * @param transactionManager
     */
    public void setTransactionManager(JpaTransactionManager transactionManager) {
        this.transactionManager = transactionManager;
    }

    /**
     *
     */
    public class ParametersRequest {

        private String commentExtraction;
        private DatesRequestParamVO datesRequestParam;
        final private Map<Long, SiteVO> sitesSelected = new HashMap<>();

        final private Map<Long, VODatatypeJSF> datatypesSelected = new HashMap<>();

        public Map<Long, VODatatypeJSF> getDatatypesSelected() {
            return datatypesSelected;
        }

        /**
         *
         * @return
         */
        public String getCommentExtraction() {
            return commentExtraction;
        }

        /**
         *
         * @return
         */
        public DatesRequestParamVO getDatesRequestParam() {
            if (datesRequestParam == null) {
                initDatesRequestParam();
            }
            return datesRequestParam;
        }

        /**
         *
         * @return
         */
        public Boolean getDateStepIsValid() {
            if ((datesRequestParam.getSelectedFormSelection() != null) && getSitesStepIsValid() && datesRequestParam.getCurrentDatesFormParam().getIsValid()) {
                return true;
            } else {
                return false;
            }
        }

        /**
         *
         * @return @throws ParseException
         */
        public Date getFirstStartDate() throws ParseException {
            if (!datesRequestParam.getCurrentDatesFormParam().getPeriods().isEmpty()) {
                Date firstDate = null;
                for (Date mapDate : datesRequestParam.getCurrentDatesFormParam().retrieveParametersMap().values()) {
                    if (firstDate == null) {
                        firstDate = mapDate;
                    } else if (firstDate.after(mapDate)) {
                        firstDate = mapDate;
                    }
                }
                return firstDate;
            }
            return null;
        }

        /**
         *
         * @return
         */
        public boolean getFormIsValid() {
            if (getSitesStepIsValid() && getDateStepIsValid() && getDatatypeStepIsValid()) {
                return true;
            } else {
                return false;
            }

        }

        /**
         *
         * @return @throws ParseException
         */
        public Date getLastEndDate() throws ParseException {
            if (!datesRequestParam.getCurrentDatesFormParam().getPeriods().isEmpty()) {
                Date lastDate = null;
                for (Date mapDate : datesRequestParam.getCurrentDatesFormParam().retrieveParametersMap().values()) {
                    if (lastDate == null) {
                        lastDate = mapDate;
                    } else if (lastDate.before(mapDate)) {
                        lastDate = mapDate;
                    }
                }
                return lastDate;
            }
            return null;
        }

        /**
         *
         * @return
         */
        public List<SiteVO> getListSitesSelected() {
            return new LinkedList<>(sitesSelected.values());
        }

        /**
         *
         * @return
         */
        public List<VODatatypeJSF> getListDatatypesSelected() {
            return new LinkedList<>(datatypesSelected.values());
        }

        /**
         *
         * @return
         */
        public Map<Long, SiteVO> getSitesSelected() {
            return sitesSelected;
        }

        /**
         *
         * @return
         */
        public Boolean getSitesStepIsValid() {
            return !sitesSelected.isEmpty();
        }

        public Map<Long, VODatatypeJSF> getDatatypeSelected() {
            return datatypesSelected;
        }

        /**
         *
         * @return
         */
        public Boolean getDatatypeStepIsValid() {
            return (!datatypesSelected.isEmpty()) && getDateStepIsValid();
        }

        private void initDatesRequestParam() {
            datesRequestParam = new DatesRequestParamVO(localizationManager);
        }

        /**
         *
         * @param commentExtraction
         */
        public void setCommentExtraction(String commentExtraction) {
            this.commentExtraction = commentExtraction;
        }

    }

    /**
     *
     */
    public class SiteVO {

        private String localizedSiteName;
        private final Site site;
        private Boolean selected = false;

        /**
         *
         * @param site
         */
        public SiteVO(Site site) {
            super();
            this.site = site;
            if (site != null) {
                this.localizedSiteName = getPropertiesSiteNames().getProperty(site.getNom());
                if (Strings.isNullOrEmpty(this.localizedSiteName)) {
                    this.localizedSiteName = site.getNom();
                }

            }
        }

        /**
         *
         * @return
         */
        public String getLocalizedSiteName() {
            return localizedSiteName;
        }

        /**
         *
         * @return
         */
        public Site getSite() {
            return site;
        }

        /**
         *
         * @return
         */
        public Boolean getSelected() {
            return selected;
        }

        /**
         *
         * @param localizedSiteName
         */
        public void setLocalizedSiteName(String localizedSiteName) {
            this.localizedSiteName = localizedSiteName;
        }

        /**
         *
         * @param selected
         */
        public void setSelected(Boolean selected) {
            this.selected = selected;
        }
    }

    /**
     *
     */
    public class ProjetVO {

        private String localizedProjetName;
        private Map<Long, SiteVO> sites = new HashMap<Long, SiteVO>();
        private Projet projet;

        /**
         *
         * @param projet
         * @param sites
         */
        public ProjetVO(Projet projet, List<Site> sites) {
            super();
            this.projet = projet;
            if (projet != null) {
                this.localizedProjetName = getPropertiesProjectsNames().getProperty(projet.getNom());
                if (Strings.isNullOrEmpty(this.localizedProjetName)) {
                    this.localizedProjetName = projet.getNom();
                }
            }
            for (Site site : sites) {
                this.sites.put(site.getId(), new SiteVO(site));
            }

        }

        /**
         *
         * @return
         */
        public String getLocalizedProjetName() {
            return localizedProjetName;
        }

        /**
         *
         * @return
         */
        public Map<Long, SiteVO> getSites() {
            return sites;
        }

        /**
         *
         * @return
         */
        public Projet getProjet() {
            return projet;
        }

        /**
         *
         * @param localizedProjetName
         */
        public void setLocalizedProjetName(String localizedProjetName) {
            this.localizedProjetName = localizedProjetName;
        }
    }

    /**
     *
     */
    public class VODatatypeJSF {

        private String localizedDatatypeName;
        private Boolean selected = false;
        private DatatypeGenericVO datatype;

        /**
         *
         * @param datatype
         */
        public VODatatypeJSF(DatatypeGenericVO dataType) {
            super();
            this.datatype = dataType;
            if (dataType != null) {
                this.localizedDatatypeName = getPropertiesDatatypesNames().getProperty(datatype.getDatatype().getName());
                if (Strings.isNullOrEmpty(this.localizedDatatypeName)) {
                    this.localizedDatatypeName = datatype.getDatatype().getName();
                }
            }
        }

        /**
         *
         * @return
         */
        public String getLocalizedDatatypeName() {
            return localizedDatatypeName;
        }

        /**
         *
         * @return
         */
        public Boolean getSelected() {
            return selected;
        }

        public DatatypeGenericVO getDatatype() {
            return datatype;
        }

        /**
         *
         * @param localizedDatatypeName
         */
        public void setLocalizedVariableName(String localizedDatatypeName) {
            this.localizedDatatypeName = localizedDatatypeName;
        }

        /**
         *
         * @param selected
         */
        public void setSelected(Boolean selected) {
            this.selected = selected;
        }

    }

    /**
     *
     */
    public class VOVariableJSF {

        private String localizedVariableName;
        private Boolean selected = false;
        private VariableVO variable;

        /**
         *
         * @param variable
         */
        public VOVariableJSF(VariableVO variable) {
            super();
            this.variable = variable;
            if (variable != null) {
                this.localizedVariableName = getPropertiesVariableNames().getProperty(variable.getNom());
                if (Strings.isNullOrEmpty(this.localizedVariableName)) {
                    this.localizedVariableName = variable.getNom();
                }
            }
        }

        /**
         *
         * @return
         */
        public String getLocalizedVariableName() {
            return localizedVariableName;
        }

        /**
         *
         * @return
         */
        public Boolean getSelected() {
            return selected;
        }

        /**
         *
         * @return
         */
        public VariableVO getVariable() {
            return variable;
        }

        /**
         *
         * @param localizedVariableName
         */
        public void setLocalizedVariableName(String localizedVariableName) {
            this.localizedVariableName = localizedVariableName;
        }

        /**
         *
         * @param selected
         */
        public void setSelected(Boolean selected) {
            this.selected = selected;
        }
    }

    /**
     *
     * @param fileCompManager
     */
    public void setFileCompManager(IFileCompManager fileCompManager) {
        this.fileCompManager = fileCompManager;
    }

    public void setGenericDatatypeManager(IGenericDatatypeManager genericDatatypeManager) {
        this.genericDatatypeManager = genericDatatypeManager;
    }

    public void setPropertiesDatatypesNames(Properties propertiesDatatypesNames) {
        this.propertiesDatatypesNames = propertiesDatatypesNames;
    }

    public void setDatatypesNonStillAvailables(Map<Long, VODatatypeJSF> datatypesNonStillAvailables) {
        this.datatypesNonStillAvailables = datatypesNonStillAvailables;
    }

    public List<VODatatypeJSF> getDatatypesAvailablesForExtraction() throws BusinessException, ParseException {
        if (datatypesAvailables.isEmpty() && parametersRequest.getDateStepIsValid()) {
            createOrUpdateListDatatypesAvailables();
        }
        return new LinkedList<>(datatypesAvailablesForExtraction.values());
    }

    /**
     *
     * @return @throws BusinessException
     * @throws ParseException
     */
    public List<VODatatypeJSF> getDatatypesNonStillAvailables() throws BusinessException, ParseException {

        if (datatypesAvailables.isEmpty() && parametersRequest.getDateStepIsValid()) {
            createOrUpdateListDatatypesAvailables();
        }
        return new LinkedList<VODatatypeJSF>(datatypesNonStillAvailables.values());

    }

}
