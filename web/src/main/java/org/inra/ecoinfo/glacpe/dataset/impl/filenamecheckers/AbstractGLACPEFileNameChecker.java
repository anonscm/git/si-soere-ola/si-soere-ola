package org.inra.ecoinfo.glacpe.dataset.impl.filenamecheckers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.inra.ecoinfo.dataset.exception.InvalidFileNameException;
import org.inra.ecoinfo.dataset.impl.filenamecheckers.AbstractFileNameChecker;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.glacpe.refdata.projet.IProjetDAO;
import org.inra.ecoinfo.glacpe.refdata.projetsitethemedatatype.ProjetSiteThemeDatatype;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public abstract class AbstractGLACPEFileNameChecker extends AbstractFileNameChecker {

    /**
     *
     */
    protected static final String INVALID_FILE_NAME = "%s_%s_%s_%s_%s.csv";

    /**
     *
     */
    protected static final String PATTERN = "^(%s|.*?)_(%s|.*?)_(%s|.*?)_(.*)_(.*)\\.csv$";
     protected static final String PATTERN_GENERIC = "^(%s|.*?)_(%s|.*?)_(%s|.*?)_(.*)_(.*)\\.(.*)$";
    /** The pattern file name path @link(String). */
    protected static final String PATTERN_FILE_NAME_PATH = "%s_%s_%s_%s_%s#V%d#.csv";

    /** The Constant CST_STRING_EMPTY @link(String). */
    protected static final String CST_STRING_EMPTY = "";

    /**
     *
     */
    protected IProjetDAO projetDAO;

    @Override
    public boolean isValidFileName(String fileName, VersionFile version) throws InvalidFileNameException {
        fileName = cleanFileName(fileName);
        String currentProject = ((ProjetSiteThemeDatatype) version.getDataset().getLeafNode()).getProjetSiteTheme().getProjetSite().getProjet().getCode();
        String currentSite = reWriteSitePath(((org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE) ((ProjetSiteThemeDatatype) version.getDataset().getLeafNode()).getProjetSiteTheme().getProjetSite().getSite()).getCodeFromName());
        DataType datatype = version.getDataset().getLeafNode().getDatatype();
        String currentDatatype = datatype.getCode();
        
        String splitPattern = null;
        if (datatype.getIsGeneric()){
            splitPattern = PATTERN_GENERIC;
        } else {
            splitPattern = PATTERN;
        }
        
        Matcher splitFilename = Pattern.compile(String.format(splitPattern, currentProject, currentSite, currentDatatype)).matcher(fileName);
        testPath(currentProject, currentSite, currentDatatype, splitFilename);
        String projetName = Utils.createCodeFromString(splitFilename.group(1));
        testProject(currentProject, currentSite, currentDatatype, projetName);
        String siteName = reWriteSitePath(Utils.createCodeFromString(splitFilename.group(2)));
        testSite(version, currentProject, currentSite, currentDatatype, siteName);
        String datatypeName = Utils.createCodeFromString(splitFilename.group(3));
        testDatatype(currentProject, currentSite, currentDatatype, datatypeName);
        testDates(version, currentProject, currentSite, currentDatatype, splitFilename);
        return true;
    }

    /**
     *
     * @param version
     * @param currentProject
     * @param currentSite
     * @param currentDatatype
     * @param siteName
     * @throws InvalidFileNameException
     */
    protected void testSite(VersionFile version, String currentProject, String currentSite, String currentDatatype, String siteName) throws InvalidFileNameException {
        try {
            if (siteDAO.getByCode(Utils.createCodeFromString(siteName)) == null || !((SiteGLACPE) ((ProjetSiteThemeDatatype) version.getDataset().getLeafNode()).getProjetSiteTheme().getProjetSite().getSite()).getCodeFromName().equals(siteName)) {
                throw new InvalidFileNameException(String.format(INVALID_FILE_NAME, currentProject, currentSite, currentDatatype, getDatePattern(), getDatePattern(), FILE_FORMAT));
            }
        } catch (PersistenceException e) {
            e.printStackTrace();
            throw new InvalidFileNameException(String.format(INVALID_FILE_NAME, currentProject, currentSite, currentDatatype, getDatePattern(), getDatePattern(), FILE_FORMAT));
        }
    }

    /**
     *
     * @param currentProject
     * @param currentSite
     * @param currentDatatype
     * @param splitFilename
     * @throws InvalidFileNameException
     */
    protected void testPath(String currentProject, String currentSite, String currentDatatype, Matcher splitFilename) throws InvalidFileNameException {
        if (!splitFilename.find()) {
            throw new InvalidFileNameException(String.format(INVALID_FILE_NAME, currentProject, currentSite, currentDatatype, getDatePattern(), getDatePattern(), FILE_FORMAT));
        }
    }

    /**
     *
     * @param currentProject
     * @param currentSite
     * @param currentDatatype
     * @param projectName
     * @throws InvalidFileNameException
     */
    protected void testProject(String currentProject, String currentSite, String currentDatatype, String projectName) throws InvalidFileNameException {
        try {
            if (projetDAO.getByCode(Utils.createCodeFromString(projectName)) == null) {
                throw new InvalidFileNameException(String.format(INVALID_FILE_NAME, currentProject, currentSite, currentDatatype, getDatePattern(), getDatePattern(), FILE_FORMAT));
            }
        } catch (final PersistenceException e) {
            LOGGER.debug(e);
            throw new InvalidFileNameException(String.format(INVALID_FILE_NAME, currentProject, currentSite, currentDatatype, getDatePattern(), getDatePattern(), FILE_FORMAT), e);
        }
    }

    static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(AbstractFileNameChecker.class.getName());
    
    /**
     *
     * @param currentProject
     * @param currentSite
     * @param currentDatatype
     * @param datatypeName
     * @throws InvalidFileNameException
     */
    protected void testDatatype(String currentProject, String currentSite, String currentDatatype, String datatypeName) throws InvalidFileNameException {
        try {
            if (datatypeDAO.getByCode(Utils.createCodeFromString(datatypeName)) == null) {
                throw new InvalidFileNameException(String.format(INVALID_FILE_NAME, currentProject, currentSite, currentDatatype, getDatePattern(), getDatePattern(), FILE_FORMAT));
            }
        } catch (final PersistenceException e) {
            LOGGER.debug(e);
            throw new InvalidFileNameException(String.format(INVALID_FILE_NAME, currentProject, currentSite, currentDatatype, getDatePattern(), getDatePattern(), FILE_FORMAT), e);
        }
    }

    /**
     *
     * @param version
     * @param currentProject
     * @param currentSite
     * @param currentDatatype
     * @param splitFilename
     * @throws InvalidFileNameException
     */
    protected abstract void testDates(VersionFile version, String currentProject, String currentSite, String currentDatatype, Matcher splitFilename) throws InvalidFileNameException;

    /**
     *
     * @param projetDAO
     */
    public void setProjetDAO(IProjetDAO projetDAO) {
        this.projetDAO = projetDAO;
    }
}
