/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.glacpe.identification;

import java.util.LinkedList;
import java.util.List;
import javax.persistence.Query;
import org.apache.log4j.Logger;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.glacpe.identification.entity.RightsRequest;
import org.inra.ecoinfo.identification.IRightRequestDAO;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class JPARightRequestDAO extends AbstractJPADAO<RightsRequest> implements IRightRequestDAO<RightsRequest> {

    private static final Logger LOGGER = Logger.getLogger(JPARightRequestDAO.class);

    @Override
    public List<RightsRequest> getByUser(Utilisateur utilisateur) throws PersistenceException {
        List<RightsRequest> requests = null;
        Query query = entityManager.createQuery("from " + RightsRequest.NAME_ENTITY + " where utilisateur=:utilisateur");
        try {
            List<RightsRequest> req = query.getResultList();
        } catch (javax.persistence.PersistenceException e) {
            LOGGER.error("error finding requests", e);
        }
        return requests;
    }

    /**
     *
     * @return
     */
    @Override
    public List<RightsRequest> getAll() {
        try {
            return getAll(RightsRequest.class);
        } catch (PersistenceException ex) {
            LOGGER.error("error finding requests", ex);
            return new LinkedList();
        }
    }

}
