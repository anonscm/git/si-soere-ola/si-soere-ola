/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.glacpe.identification;

import java.util.Locale;
import java.util.ResourceBundle;
import org.apache.log4j.Logger;
import org.inra.ecoinfo.glacpe.identification.entity.RightsRequest;
import org.inra.ecoinfo.identification.IRequestManager;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.identification.impl.AbstractRequestManager;
import org.inra.ecoinfo.localization.entity.Localization;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class OLARequestManager extends AbstractRequestManager<RightsRequest> implements IRequestManager<RightsRequest> {

    /**
     * The Constant PROPERTY_MSG_RIGHT_DEMAND.
     */
    public static final String PROPERTY_MSG_RIGHT_DEMAND = "PROPERTY_MSG_RIGHT_DEMAND";

    /**
     *
     */
    public static final String BUNDLE_NAME = "org.inra.ecoinfo.identification.messages";

    /**
     *
     */
    public static final String LOCAL_BUNDLE_NAME = "org.inra.ecoinfo.glacpe.identification.message";
    /**
     * The Constant PROPERTY_MSG_NEW_RIGHT_REQUEST_MAIL_CONTENT.
     */
    public static final String PROPERTY_MSG_NEW_RIGHT_REQUEST_MAIL_CONTENT = "PROPERTY_MSG_NEW_RIGHT_REQUEST_MAIL_CONTENT";
    /**
     * The Constant PROPERTY_MSG_NEW_RIGHT_REQUEST
     */
    public static final String PROPERTY_MSG_NEW_RIGHT_REQUEST = "PROPERTY_MSG_NEW_RIGHT_REQUEST";
     /**
     * The Constant PROPERTY_MSG_NEW_RIGHT_REQUEST
     */
    public static final String PROPERTY_MSG_NEW_RIGHT_REQUEST_BODY = "PROPERTY_MSG_NEW_RIGHT_REQUEST_BODY";
     
    /**
     *
     */
    protected static final Logger LOGGER = Logger.getLogger(OLARequestManager.class);

    /**
     *
     * @param admin
     * @param utilisateur
     * @param rightRequest
     * @return
     */
    @Override
    public String getMailMessageForRequest(Utilisateur admin, Utilisateur utilisateur, RightsRequest rightRequest) {
        final ResourceBundle resourceBundle = ResourceBundle.getBundle(LOCAL_BUNDLE_NAME, utilisateur.getLanguage() == null ? new Locale(Localization.getDefaultLocalisation()) : new Locale(utilisateur.getLanguage()));
        String message = resourceBundle.getString(PROPERTY_MSG_NEW_RIGHT_REQUEST_MAIL_CONTENT);        
        
        message = String.format(message,
                                admin.getPrenom(), admin.getNom(),
                                utilisateur.getLogin(),rightRequest.toString());
        return message;

    }

    /**
     *
     * @param rightRequest
     * @param utilisateur
     */
    @Override
    protected void setRightRequestUser(RightsRequest rightRequest, Utilisateur utilisateur) {
        rightRequest.setUtilisateur(utilisateur);        
    }

    /**
     *
     * @param rightRequest
     * @return
     * @throws PersistenceException
     */
    @Override
    protected Utilisateur getUtilisateurForRequest(RightsRequest rightRequest) throws PersistenceException {
        return rightRequest.getUtilisateur();
    }

}
