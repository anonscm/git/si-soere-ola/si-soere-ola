/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.glacpe.identification;

import java.util.Date;
import org.inra.ecoinfo.glacpe.identification.entity.RightsRequest;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.identification.jsf.AbstractRightRequestVO;
import org.inra.ecoinfo.identification.jsf.UtilisateurVO;

/**
 *
 * @author ptcherniati
 */
public class RightRequestOLAVO extends AbstractRightRequestVO<RightRequestOLAVO, RightsRequest> {
    private static RightRequestOLAVO _getInstance() {
        return new RightRequestOLAVO();
    }

    RightsRequest _request;
    UtilisateurVO utilisateurVO;

    /**
     *
     * @param request
     */
    public RightRequestOLAVO(RightsRequest request) {
        super();
        this._request = request;
        this.utilisateurVO = new UtilisateurVO(request.getUtilisateur());
    }

    /**
     *
     */
    public RightRequestOLAVO() {
        super();
    }

    /**
     *
     * @param req
     * @return
     */
    @Override
    public RightRequestOLAVO getInstance(RightsRequest req) {
        RightRequestOLAVO instance = _getInstance();
        instance._request = req;
        return instance;
    }

    /**
     *
     * @return
     */
    @Override
    public RightRequestOLAVO getInstance() {
        this.utilisateurVO = utilisateurVO;
        RightRequestOLAVO instance = _getInstance();
        instance._request = new RightsRequest();
        return instance;
    }

    /**
     *
     * @param utilisateur
     */
    @Override
    public void setUtilisateur(Utilisateur utilisateur) {
        _request.setUtilisateur(utilisateur);       
    }

    /**
     *
     * @return
     */
    @Override
    public Utilisateur getUtilisateur() {
        return _request.getUtilisateur();
    }

    /**
     *
     * @return
     */
    @Override
    public RightsRequest getRightRequest() {
        return _request;
    }

    /**
     *
     * @return
     */
    @Override
    public Date getCreateDate() {
        return _request.getCreateDate();
    }

    /**
     *
     * @return
     */
    @Override
    public boolean isValidated() {
        return _request.isValidated();
    }

    /**
     *
     * @param validated
     */
    @Override
    public void setValidated(boolean validated) {
        _request.setValidated(validated);
    }
}
