package org.inra.ecoinfo.glacpe.identification;

import com.google.common.base.Strings;
import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.apache.log4j.Logger;
import org.inra.ecoinfo.glacpe.identification.entity.RightsRequest;

import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.identification.jsf.AbstractUIBeanRequestRights;

/**
 *
 * @author ptcherniati
 */
@ManagedBean(name = "uiRequestRightsOLA")
@ViewScoped
public class UIBeanRequestRightsOLA extends AbstractUIBeanRequestRights<RightRequestOLAVO, RightsRequest> implements Serializable {

    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.glacpe.identification.messages";

    /**
     *
     */
    protected static final Logger LOGGER = Logger.getLogger(UIBeanRequestRightsOLA.class);

    RightsRequest request = new RightsRequest();
    List<UtilisateurItem> requests;
    Map<Utilisateur, UtilisateurItem> items = new HashMap();

    /**
     *
     */
    public UIBeanRequestRightsOLA() {
    }

    /**
     *
     * @return
     */
    @Override
    public String navigate() {
        return "requestRights";
    }

    /**
     *
     * @return
     */
    @Override
    public String userNavigate() {
        return "requestUsersRights";
    }

 

    /**
     *
     * @param dbRequest
     * @return
     */
    @Override
    protected RightRequestOLAVO newRequestVO(RightsRequest dbRequest) {
        return new RightRequestOLAVO(dbRequest);
    }

    /**
     *
     * @return
     */
    @Override
    protected RightRequestOLAVO getRequestVOInstance() {
        return new RightRequestOLAVO().getInstance();
    }


}
