package org.inra.ecoinfo.glacpe.identification.entity;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import org.apache.log4j.Logger;
import org.hibernate.validator.constraints.NotEmpty;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.localization.entity.Localization;

/**
 *
 * @author ptcherniati
 */
@Entity(name = RightsRequest.NAME_ENTITY)
@Table(indexes = {
    @Index(name = "rightrequest_utilisateur_idx", columnList = RightsRequest.UTILISATEUR_ID_JPA)})
public class RightsRequest implements Serializable {

    /**
     *
     */
    public static final String ID_JPA = "rightsrequest";

    /**
     *
     */
    public static final String NAME_ENTITY = "ola_rightsrequest";

    /**
     *
     */
    public static final String UTILISATEUR_ID_JPA = "usr_id";

    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.glacpe.identification.message";
    private static final String PROPERTY_MSG_REQUEST_TO_STRING = "PROPERTY_MSG_REQUEST_TO_STRING";
    private static final long serialVersionUID = 1L;
    /**
     *
     */
    protected static final Logger LOGGER = Logger.getLogger(RightsRequest.class);

    @Id
    @Column(name = RightsRequest.ID_JPA)
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    private Date createDate = new Date();
    
    private boolean validated;

    @Column(nullable = false)
    @Size(min = 1)
    @NotEmpty
    private String poste;

    @Column(nullable = false)
    @Size(min = 1)
    @NotEmpty
    private String emploi;

    @Column(nullable = true)
    private String workAdress;

    @Column(nullable = true)
    private String phone;

    @Column(nullable = true)
    private String encadrant;

    @Column(nullable = false)
    @Size(min = 1)
    @NotEmpty
    private String request;

    @Column(nullable = false)
    private String sites;

    @Column(nullable = false)
    @Size(min = 1)
    @NotEmpty
    private String localScientist;

    @Column(nullable = false)
    @Size(min = 1)
    @NotEmpty
    private String variables;

    @Column(nullable = false)
    @Size(min = 1)
    @NotEmpty
    private String dates;

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH}, optional = false)
    @JoinColumn(name = UTILISATEUR_ID_JPA, referencedColumnName = Utilisateur.UTILISATEUR_NAME_ID, nullable = false)
    private Utilisateur utilisateur;

    /**
     *
     */
    public RightsRequest() {
    }

    public String getPoste() {
        return poste;
    }

    public void setPoste(String poste) {
        this.poste = poste;
    }

    public String getEmploi() {
        return emploi;
    }

    public void setEmploi(String emploi) {
        this.emploi = emploi;
    }

    public String getEncadrant() {
        return encadrant;
    }

    public void setEncadrant(String encadrant) {
        this.encadrant = encadrant;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getSites() {
        return sites;
    }

    public void setSites(String sites) {
        this.sites = sites;
    }

    public String getLocalScientist() {
        return localScientist;
    }

    public void setLocalScientist(String localScientist) {
        this.localScientist = localScientist;
    }

    public String getVariables() {
        return variables;
    }

    public void setVariables(String variables) {
        this.variables = variables;
    }

    public String getDates() {
        return dates;
    }

    public void setDates(String dates) {
        this.dates = dates;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     *
     * @param createDate
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     *
     * @return
     */
    public boolean isValidated() {
        return validated;
    }

    /**
     *
     * @param validated
     */
    public void setValidated(boolean validated) {
        this.validated = validated;
    }

    /**
     *
     * @return
     */
    public String getWorkAdress() {
        return workAdress;
    }

    /**
     *
     * @param workAdress
     */
    public void setWorkAdress(String workAdress) {
        this.workAdress = workAdress;
    }

    /**
     *
     * @return
     */
    public String getPhone() {
        return phone;
    }

    /**
     *
     * @param phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     *
     * @return
     */
    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    /**
     *
     * @param utilisateur
     */
    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    @Override
    public String toString() {
        final ResourceBundle resourceBundle = ResourceBundle.getBundle(BUNDLE_SOURCE_PATH, utilisateur.getLanguage() == null ? new Locale(Localization.getDefaultLocalisation()) : new Locale(utilisateur.getLanguage()));
        String message = resourceBundle.getString(PROPERTY_MSG_REQUEST_TO_STRING);
        message = String.format(message,
                getUtilisateur().getEmail(), emploi, poste, encadrant, workAdress, phone, request, localScientist, variables, sites, dates,new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
        return message;

    }

}
