package org.inra.ecoinfo.glacpe.security;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Query;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.security.IPathDAO;

/**
 *
 * @author ptcherniati
 */
public class PathDAO extends AbstractJPADAO<Object> implements IPathDAO, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    static String QUERY_IS_REAL_PATH = "from ProjetSiteThemeDatatype pstd where  concat(pstd.projetSiteTheme.projetSite.projet.code,'/',pstd.projetSiteTheme.projetSite.site.code,'/',pstd.projetSiteTheme.theme.code,'/',pstd.datatype.code) like :chaine||'%'";
    static String QUERY_IS_GET_PATHES = "select concat(pstd.projetSiteTheme.projetSite.projet.code,'/',pstd.projetSiteTheme.projetSite.site.code,'/',pstd.projetSiteTheme.theme.code,'/',pstd.datatype.code) from ProjetSiteThemeDatatype pstd";

    /**
     *
     * @param path
     * @return
     */
    @Override
    public boolean isRealPathNode(String path) {
        Query query = entityManager.createQuery(QUERY_IS_REAL_PATH);
        query.setParameter("chaine", path);
        return query.getResultList().size() > 0;
    }

    /**
     *
     * @return
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<String> getPathes() {
        return entityManager.createQuery(QUERY_IS_GET_PATHES).getResultList();
    }

}
