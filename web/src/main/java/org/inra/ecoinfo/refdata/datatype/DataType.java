package org.inra.ecoinfo.refdata.datatype;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.Type;
import org.inra.ecoinfo.refdata.datatypevariableunite.DatatypeVariableUnite;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.Utils;

import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;

/**
 * The Class DataType.
 *
 * @author "Antoine Schellenberger"
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name = DataType.NAME_ENTITY_JPA)
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@DiscriminatorColumn(discriminatorType = DiscriminatorType.STRING, name = "datatypeCDC")
public class DataType implements Serializable, Comparable<DataType>, Cloneable {

    /**
     * The Constant DATATYPE_NAME_ID @link(String).
     */
    public static final String DATATYPE_NAME_ID = "id";
    /**
     * The Constant ID_JPA @link(String).
     */
    public static final String ID_JPA = "dty_id";
    /**
     * The Constant NAME_ENTITY_JPA @link(String).
     */
    public static final String NAME_ENTITY_JPA = "datatype";
    /**
     * The Constant NAME_ATTRIBUTS_DESCRIPTION @link(String).
     */
    public static final String NAME_ATTRIBUTS_DESCRIPTION = "description";
    /**
     * The Constant NAME_ATTRIBUTS_NAME @link(String).
     */
    public static final String NAME_ATTRIBUTS_NAME = "name";
    /**
     * The Constant serialVersionUID @link(long).
     */
    private static final long serialVersionUID = 1L;
    /**
     * The code @link(String).
     */
    @XmlElement(name = "code")
    @Column(nullable = false, unique = true)
    private String code;

    @Column(nullable = false)
    private Boolean isGeneric = Boolean.FALSE;
    /**
     * The datatypes unites variables @link(List<DatatypeVariableUnite>).
     */
    @XmlTransient
    @OneToMany(mappedBy = "datatype", cascade = {MERGE, PERSIST, REFRESH})
    private List<DatatypeVariableUnite> datatypesUnitesVariables = new LinkedList<>();
    /**
     * The description @link(String).
     */
    @XmlElement(name = "description")
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = DataType.NAME_ATTRIBUTS_DESCRIPTION)
    private String description = "";
    /**
     * The name @link(String).
     */
    @XmlElement(name = "name")
    @Column(name = DataType.NAME_ATTRIBUTS_NAME, nullable = false, unique = true)
    private String name;
    /**
     * The id @link(Long).
     */
    @XmlAttribute
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    protected Long id;

    /**
     * Instantiates a new data type.
     */
    public DataType() {
    }

    /**
     * Instantiates a new data type.
     *
     * @param name the name
     * @link(String) the name
     */
    public DataType(final String name) {
        this.setName(name);
    }

    /**
     * Instantiates a new data type.
     *
     * @param name the name
     * @param description the description
     * @link(String) the name
     * @link(String) the description
     */
    public DataType(final String name, final String description) {
        super();
        setName(name);
        setDescription(description);
    }
    
    public DataType(final String name, final String description, Boolean isGeneric) {
        super();
        setName(name);
        setDescription(description);
        this.isGeneric = isGeneric;
    }

    /**
     * Clone.
     *
     * @return the data type
     * @throws CloneNotSupportedException the clone not supported exception
     * @see java.lang.Object#clone()
     */
    @Override
    public DataType clone() throws CloneNotSupportedException {
        super.clone();
        final DataType datatype = new DataType();
        datatype.setDescription(description);
        datatype.setId(id);
        datatype.setName(name);
        datatype.setDatatypesUnitesVariables(datatypesUnitesVariables);
        return datatype;
    }

    /**
     * Clone only attributes.
     *
     * @return the data type
     */
    public DataType cloneOnlyAttributes() {
        final DataType datatype = new DataType();
        datatype.setDescription(description);
        datatype.setId(id);
        datatype.setName(name);
        return datatype;
    }

    /**
     * Compare to.
     *
     * @param o the o
     * @return the int
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(final DataType o) {
        if (this.equals(o)) {
            assert this.equals(o) : "compareTo inconsistent with equals.";
            return 0;
        } else {
            return getCode() == null ? -1 : getCode().compareTo(o.getCode());
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DataType other = (DataType) obj;
        return Objects.equals(this.code, other.code);
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the code.
     *
     * @param code the new code
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * Gets the datatypes unites variables.
     *
     * @return the datatypes unites variables
     */
    public List<DatatypeVariableUnite> getDatatypesUnitesVariables() {
        return datatypesUnitesVariables;
    }

    /**
     * Sets the datatypes unites variables.
     *
     * @param datatypesUnitesVariables the new datatypes unites variables
     */
    public void setDatatypesUnitesVariables(final List<DatatypeVariableUnite> datatypesUnitesVariables) {
        this.datatypesUnitesVariables = datatypesUnitesVariables;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description.
     *
     * @param description the new description
     */
    public void setDescription(final String description) {
        this.description = description == null ? "" : description;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    public void setName(final String name) {
        this.name = name;
        this.code = Utils.createCodeFromString(name);
    }

    /**
     * Gets the variables.
     *
     * @return the variables
     */
    public List<Variable> getVariables() {
        final List<Variable> variables = new LinkedList<>();
        for (final DatatypeVariableUnite datatypeVariableUnite : datatypesUnitesVariables) {
            variables.add(datatypeVariableUnite.getVariable());
        }
        return variables;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (code == null ? 0 : code.hashCode());
        result = prime * result + (description == null ? 0 : description.hashCode());
        return result;
    }

    /**
     * To string.
     *
     * @return the string
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return this.name;
    }

    public Boolean getIsGeneric() {
        return isGeneric;
    }

    public void setIsGeneric(Boolean isGeneric) {
        this.isGeneric = isGeneric;
    }

}
