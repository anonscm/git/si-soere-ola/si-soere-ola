package org.inra.ecoinfo.refdata.datatype;

import java.util.List;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * The Interface IDatatypeDAO.
 */
public interface IDatatypeDAO extends IDAO<DataType> {

    /**
     * Gets the all.
     *
     * @return all the generics data types
     * @throws PersistenceException the persistence exception
     */
    public List<DataType> retrieveGenerics() throws PersistenceException;

    /**
     * Gets the all.
     *
     * @return the all
     * @throws PersistenceException the persistence exception
     */
    List<DataType> getAll() throws PersistenceException;

    /**
     * Gets the by code.
     *
     * @param code the code
     * @return the by code
     * @throws PersistenceException the persistence exception
     * @link(String) the code
     */
    DataType getByCode(String code) throws PersistenceException;
}
