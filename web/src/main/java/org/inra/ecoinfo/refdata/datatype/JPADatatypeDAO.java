/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:50:44
 */
package org.inra.ecoinfo.refdata.datatype;

import java.util.List;
import javax.persistence.Query;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * The Class JPADatatypeDAO.
 *
 * @author "Antoine Schellenberger"
 */
public class JPADatatypeDAO extends AbstractJPADAO<DataType> implements IDatatypeDAO {

    /**
     * The Constant QUERY_GET_ALL @link(String).
     */
    private static final String QUERY_GET_ALL = "from DataType s order by code";

    private static final String QUERY_GET_GENERICS = "from DataType s where s.isGeneric = true order by code";
    

    /**
     * The Constant QUERY_GET_BY_CODE @link(String).
     */
    private static final String QUERY_GET_BY_CODE = "from DataType d where d.code = :code";

    /**
     * Gets the all.
     *
     * @return the all
     * @throws PersistenceException the persistence exception
     * @see org.inra.ecoinfo.refdata.datatype.IDatatypeDAO#getAll()
     */
    @SuppressWarnings({"unchecked"})
    @Override
    public List<DataType> getAll() throws PersistenceException {
        try {
            final String queryString = JPADatatypeDAO.QUERY_GET_ALL;
            final Query query = entityManager.createQuery(queryString);
            return query.getResultList();
        } catch (final Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     * Gets the all.
     *
     * @return the all
     * @throws PersistenceException the persistence exception
     * @see org.inra.ecoinfo.refdata.datatype.IDatatypeDAO#getAll()
     */
    @SuppressWarnings({"unchecked"})
    @Override
    public List<DataType> retrieveGenerics() throws PersistenceException {
        try {
            final String queryString = JPADatatypeDAO.QUERY_GET_GENERICS;
            final Query query = entityManager.createQuery(queryString);
            return query.getResultList();
        } catch (final Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     * Gets the by code.
     *
     * @param code the code
     * @return the by code
     * @throws PersistenceException the persistence exception
     * @see
     * org.inra.ecoinfo.refdata.datatype.IDatatypeDAO#getByCode(java.lang.String)
     */
    @SuppressWarnings("rawtypes")
    @Override
    public DataType getByCode(final String code) throws PersistenceException {
        try {
            final String queryString = JPADatatypeDAO.QUERY_GET_BY_CODE;
            final Query query = entityManager.createQuery(queryString);
            query.setParameter("code", code);
            final List datatypes = query.getResultList();
            DataType datatype = null;
            if (datatypes != null && !datatypes.isEmpty()) {
                datatype = (DataType) datatypes.get(0);
            }
            return datatype;
        } catch (final Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }
}
