/**
 * OREILacs project - see LICENCE.txt for use created: 7 avr. 2009 16:17:33
 */
package org.inra.ecoinfo.refdata.datatype;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.groupevariable.IGroupeVariableDAO;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.glacpe.refdata.variablenorme.IVariableNormeDAO;
import org.inra.ecoinfo.glacpe.refdata.variablenorme.VariableNorme;
import org.inra.ecoinfo.localization.entity.Localization;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.refdata.datatypevariableunite.IDatatypeVariableUniteDAO;
import org.inra.ecoinfo.refdata.unite.IUniteDAO;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class Recorder.
 *
 * @author "Antoine Schellenberger"
 */
public class Recorder extends AbstractCSVMetadataRecorder<DataType> {

    /**
     * The properties description en @link(Properties).
     */
    private Properties propertiesDescriptionEN;
    /**
     * The properties nom en @link(Properties).
     */
    private Properties propertiesNomEN;
    /**
     * The datatype dao @link(IDatatypeDAO).
     */

    private static final String ALL_VARIABLE_NAME_FR = "Toutes les variables";
    private static final String ALL_VARIABLE_NAME_EN = "All variables";
    private static final String NO_UNIT_NAME_FR = "Pas d'unité";
    private static final String NO_UNIT_NAME_EN = "no unit";
    private static final String NO_VARIABLE_NORM_NAME = "Pas de norme de variable";

    protected IDatatypeDAO datatypeDAO;
    protected IUniteDAO uniteDAO;
    protected IVariableDAO variableDAO;
    protected IDatatypeVariableUniteDAO datatypeVariableUniteDAO;
    protected IVariableNormeDAO variableNormeDAO;
    protected IGroupeVariableDAO groupeVariableDAO;

    /**
     * Delete record.
     *
     * @param parser the parser
     * @param file the file
     * @param encoding the encoding
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#deleteRecord(com.Ostermiller.util.CSVParser,
     * java.io.File, java.lang.String)
     */
    @Override
    public void deleteRecord(final CSVParser parser, final File file, final String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String code = Utils.createCodeFromString(tokenizerValues.nextToken());
                datatypeDAO.remove(datatypeDAO.getByCode(code));
                values = parser.getLine();
            }
        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Gets the new line model grid metadata.
     *
     * @param datatype the datatype
     * @return the new line model grid metadata
     * @see
     * org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata(java.lang.Object)
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(final DataType datatype) {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(datatype == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : datatype.getName(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(datatype == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : propertiesNomEN.get(datatype.getName()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(datatype == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : datatype.getDescription(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(datatype == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : propertiesDescriptionEN.get(datatype.getDescription()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true));
        return lineModelGridMetadata;
    }

    /**
     * Process record.
     *
     * @param parser the parser
     * @param file the file
     * @param encoding the encoding
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#processRecord(com.Ostermiller.util.CSVParser,
     * java.io.File, java.lang.String)
     */
    @Override
    @Transactional
    public void processRecord(final CSVParser parser, final File file, final String encoding) throws BusinessException {
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, DataType.NAME_ENTITY_JPA);
                final String nom = tokenizerValues.nextToken();
                final String description = tokenizerValues.nextToken();
                final DataType dbDatatype = datatypeDAO.getByCode(Utils.createCodeFromString(nom));
                if (dbDatatype == null) {
                    final DataType datatype = new DataType(nom, description, Boolean.TRUE);
                    persistDatatype(datatype);
                } else {
                    dbDatatype.setDescription(description);
                }
                values = parser.getLine();
            }
        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    private VariableNorme retrieveDBVariableNorme(String variableNormeCode) throws PersistenceException {
        VariableNorme variableNorme = variableNormeDAO.getByCode(Utils.createCodeFromString(NO_VARIABLE_NORM_NAME));
        if (variableNorme == null) {
            variableNorme = new VariableNorme(NO_VARIABLE_NORM_NAME, NO_VARIABLE_NORM_NAME);
        }
        return variableNorme;
    }

    private void persistDatatype(DataType dbDatatype) throws PersistenceException {

        Variable dbAllVariable = retrieveDBAllVariable();
        Unite dbNoUnit = retrieveDBNoUnit();

        String datatypeCode = dbDatatype.getCode();

        if (datatypeVariableUniteDAO.getByNKey(datatypeCode, dbAllVariable.getCode(), dbNoUnit.getCode()) == null) {
            DatatypeVariableUniteGLACPE datatypeVariableUnite = new DatatypeVariableUniteGLACPE(dbDatatype, dbNoUnit, dbAllVariable);
            dbDatatype.getDatatypesUnitesVariables().add(datatypeVariableUnite);
            dbAllVariable.getDatatypesUnitesVariables().add(datatypeVariableUnite);
            datatypeVariableUniteDAO.saveOrUpdate(datatypeVariableUnite);
        }

    }

    private VariableGLACPE retrieveDBAllVariable() throws PersistenceException {
        VariableGLACPE variable = (VariableGLACPE) variableDAO.getByCode(Utils.createCodeFromString(ALL_VARIABLE_NAME_FR));

        if (variable == null) {
            VariableNorme dbVariableNorme = retrieveDBVariableNorme(Utils.createCodeFromString(NO_VARIABLE_NORM_NAME));

            saveOrUpdateLocalization("variable", "nom", ALL_VARIABLE_NAME_FR, ALL_VARIABLE_NAME_EN);
            saveOrUpdateLocalization("variable", "affichage", ALL_VARIABLE_NAME_FR, ALL_VARIABLE_NAME_EN);
            saveOrUpdateLocalization("variable", "definition", ALL_VARIABLE_NAME_FR, ALL_VARIABLE_NAME_EN);

            variable = new VariableGLACPE(ALL_VARIABLE_NAME_FR, ALL_VARIABLE_NAME_FR, ALL_VARIABLE_NAME_FR, 1, Boolean.FALSE);
            variable.setVariableNorme(dbVariableNorme);
        }
        return variable;
    }

    private void saveOrUpdateLocalization(String entity, String attribute, String valueFr, String valueEn) throws PersistenceException {
        Localization localization = localizationManager.getByNKey("en", entity, attribute, valueFr);
        if (localization == null) {
            localization = new Localization("en", entity, attribute, valueFr, valueEn);
            localizationManager.saveLocalization(new Localization("en", entity, attribute, valueFr, valueEn));
        }

    }

    private Unite retrieveDBNoUnit() throws PersistenceException {
        Unite unite = uniteDAO.getByCode(Utils.createCodeFromString(NO_UNIT_NAME_FR));
        if (unite == null) {
            saveOrUpdateLocalization("unite", "nom", NO_UNIT_NAME_FR, NO_UNIT_NAME_EN);
            unite = new Unite(Utils.createCodeFromString(NO_UNIT_NAME_FR), NO_UNIT_NAME_FR);

        }
        return unite;
    }

    /**
     * Sets the datatype dao.
     *
     * @param datatypeDAO the new datatype dao
     */
    public void setDatatypeDAO(final IDatatypeDAO datatypeDAO) {
        this.datatypeDAO = datatypeDAO;
    }

    /**
     * Gets the all elements.
     *
     * @return the all elements
     * @throws PersistenceException the persistence exception
     * @see
     * org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<DataType> getAllElements() throws PersistenceException {
        final List<DataType> all = datatypeDAO.getAll();
        Collections.sort(all, new Comparator<DataType>() {

            @Override
            public int compare(DataType o1, DataType o2) {
                if (o1 == null || o2 == null) {
                    return o1 == o2 ? 0 : -1;
                }
                return o1.getCode().compareTo(o2.getCode());
            }
        });
        return all;
    }

    /**
     * Inits the model grid metadata.
     *
     * @return the model grid metadata
     * @see
     * org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#initModelGridMetadata()
     */
    @Override
    protected ModelGridMetadata<DataType> initModelGridMetadata() {
        propertiesNomEN = localizationManager.newProperties(DataType.NAME_ENTITY_JPA, "name", Locale.ENGLISH);
        propertiesDescriptionEN = localizationManager.newProperties(DataType.NAME_ENTITY_JPA, "description", Locale.ENGLISH);
        return super.initModelGridMetadata();
    }

    public void setUniteDAO(IUniteDAO uniteDAO) {
        this.uniteDAO = uniteDAO;
    }

    public void setVariableDAO(IVariableDAO variableDAO) {
        this.variableDAO = variableDAO;
    }

    public void setDatatypeVariableUniteDAO(IDatatypeVariableUniteDAO datatypeVariableUniteDAO) {
        this.datatypeVariableUniteDAO = datatypeVariableUniteDAO;
    }

    public void setVariableNormeDAO(IVariableNormeDAO variableNormeDAO) {
        this.variableNormeDAO = variableNormeDAO;
    }

    public void setGroupeVariableDAO(IGroupeVariableDAO groupeVariableDAO) {
        this.groupeVariableDAO = groupeVariableDAO;
    }

}
