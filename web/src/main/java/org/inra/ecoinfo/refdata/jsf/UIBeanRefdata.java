package org.inra.ecoinfo.refdata.jsf;

import com.google.common.base.Strings;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.TreeSet;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.persistence.Transient;
import javax.validation.constraints.AssertFalse;
import org.apache.commons.lang.StringUtils;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.IMetadataManager;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.refdata.config.Metadata;
import org.inra.ecoinfo.refdata.security.ISecurityRefdataManager;
import org.inra.ecoinfo.security.ISecurityContext;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.StreamCloser;
import org.inra.ecoinfo.utils.UncatchedExceptionLogger;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BadValueTypeException;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.NullValueException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.primefaces.context.RequestContext;
import org.richfaces.component.UIDataTable;
import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;

/**
 * The Class UIBeanRefdata.
 */
@ManagedBean(name = "uiRefdata")
@ViewScoped
public class UIBeanRefdata implements Serializable {

    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.refdata.jsf.messages";
    /**
     * The Constant PROPERTY_MSG_DELETE_FAIL @link(String).
     */
    private static final String PROPERTY_MSG_DELETE_FAIL = "PROPERTY_MSG_DELETE_FAIL";
    /**
     * The Constant RULE_NAVIGATION_JSF @link(String).
     */
    private static final String RULE_NAVIGATION_JSF = "refdata";
    /**
     * The Constant serialVersionUID @link(long).
     */
    
    private static final long serialVersionUID = 1L;
    private static final String PROPERTY_MSG_DATE_VALUE_EXPECTED = "PROPERTY_MSG_DATE_VALUE_EXPECTED";
    private static final String PROPERTY_MSG_TIME_VALUE_EXPECTED = "PROPERTY_MSG_TIME_VALUE_EXPECTED";
    private static final String PROPERTY_MSG_MISSING_FLOAT = "PROPERTY_MSG_MISSING_FLOAT";
    private static final String PROPERTY_MSG_MISSING_INTEGER = "PROPERTY_MSG_MISSING_INTEGER";
    private static final String PROPERTY_MSG_MISSING_VALUE = "PROPERTY_MSG_MISSING_VALUE";
    private static final String PROPERTY_MSG_DUPLICATE_NKEY = "PROPERTY_MSG_DUPLICATE_NKEY";
    /**
     * The column edited @link(ColumnEdited).
     */
    @Transient
    private ColumnEdited columnEdited = new ColumnEdited();
    /**
     * The current selections @link(List<CurrentSelection>).
     */
    @Transient
    private final List<CurrentSelection> currentSelections = new LinkedList<>();
    /**
     * The datasets details datatable @link(UIDataTable).
     */
    @Transient
    private UIDataTable datasetsDetailsDatatable;
    /**
     * The key paths @link(Set<String>).
     */
    private final Set<String> keyPaths = new TreeSet<>();
    /**
     * The line model grid metadata edited @link(LineModelGridMetadata).
     */
    @Transient
    private LineModelGridMetadata lineModelGridMetadataEdited;
    /**
     * The lines model grid metadata edited @link(Set<LineModelGridMetadata>).
     */
    @Transient
    private Set<LineModelGridMetadata> linesModelGridMetadataEdited = new HashSet<>();
    /**
     * The list value @link(String).
     */
    private String listValue;
    /**
     * The metadata selection @link(MetadataSelection).
     */
    @Transient
    private MetadataSelection metadataSelection;
    /**
     * The metadatas vo @link(List<MetadataVO>).
     */
    private List<MetadataVO> metadatasVO;
    /**
     * The model grid metadata @link(ModelGridMetadata).
     */
    @SuppressWarnings("rawtypes")
    @Transient
    private ModelGridMetadata modelGridMetadata;
    private SortedMap<Integer, List<Integer>> invalidCells = new TreeMap<>();
    /**
     * The localization manager @link(ILocalizationManager).
     */
    @ManagedProperty(value = "#{localizationManager}")
    protected ILocalizationManager localizationManager;
    /**
     * The metadata manager @link(IMetadataManager).
     */
    @ManagedProperty(value = "#{metadataManager}")
    protected IMetadataManager metadataManager;
    /**
     * The security context @link(ISecurityContext).
     */
    @ManagedProperty(value = "#{securityContext}")
    protected ISecurityContext securityContext;

    /**
     * Instantiates a new uI bean refdata.
     */
    public UIBeanRefdata() {
    }

    /**
     * Action delete line.
     *
     * @return the string
     */
    public String actionDeleteLine() {
        final LineModelGridMetadata lineModelGridMetadata = (LineModelGridMetadata) modelGridMetadata.getLinesModelGridMetadatas().get(columnEdited.getRowIndex());
        lineModelGridMetadata.setToDelete(!lineModelGridMetadata.getToDelete());
        return null;
    }

    /**
     * Action delete new line.
     *
     * @return the string
     */
    public String actionDeleteNewLine() {
        final LineModelGridMetadata lineModelGridMetadata = (LineModelGridMetadata) modelGridMetadata.getLinesModelGridMetadatas().get(columnEdited.getRowIndex());
        linesModelGridMetadataEdited.remove(lineModelGridMetadata);
        modelGridMetadata.getLinesModelGridMetadatas().remove(lineModelGridMetadata);
        invalidCells.remove(0);
        return null;
    }

    /**
     * Action edit column.
     *
     * @return the string
     */
    public String actionEditColumn() {
        final Integer rowIndex = columnEdited.getRowIndex();
        final Integer columnIndex = columnEdited.getColumnIndex();
        final String columnValue = columnEdited.getValue();
        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("");
        if (!validColumn(badsFormatsReport, columnValue, rowIndex, columnIndex)) {
            RequestContext.getCurrentInstance().execute("alert('" + badsFormatsReport.getMessages() + "')");
            invalid(rowIndex, columnIndex);
        }
        valid(rowIndex, columnIndex);
        final LineModelGridMetadata localLineModelGridMetadataEdited = modelGridMetadata.getLineModelGridMetadataAt(rowIndex);
        localLineModelGridMetadataEdited.getColumnModelGridMetadataAt(columnIndex).setValue(columnValue);
        linesModelGridMetadataEdited.add(localLineModelGridMetadataEdited);
        return null;
    }

    /**
     *
     * @param badsFormatsReport
     * @param columnValue
     * @param rowIndex
     * @param columnIndex
     * @return
     */
    public boolean validColumn(BadsFormatsReport badsFormatsReport, String columnValue, Integer rowIndex, Integer columnIndex) {
        lineModelGridMetadataEdited = modelGridMetadata.getLineModelGridMetadataAt(rowIndex);
        if (columnIndex + 1 > modelGridMetadata.getColumns().size()) {
            return false;
        }
        Column column = (Column) modelGridMetadata.getColumns().get(columnIndex);
        if (lineModelGridMetadataEdited == null) {
            return false;
        }
        ColumnModelGridMetadata columnMetadata = lineModelGridMetadataEdited.getColumnModelGridMetadataAt(columnIndex);
        if (columnMetadata == null) {
            return false;
        }

        return validColumn(badsFormatsReport, column, columnValue, lineModelGridMetadataEdited, columnMetadata);
    }

    /**
     * Adds the refdata.
     *
     * @return the string
     * @throws PersistenceException the persistence exception
     */
    @SuppressWarnings("unchecked")
    public String addRefdata() throws PersistenceException {
        lineModelGridMetadataEdited = modelGridMetadata.buildEmptyLineModelGridMetadata();
        linesModelGridMetadataEdited.add(lineModelGridMetadataEdited);
        modelGridMetadata.getLinesModelGridMetadatas().add(0, lineModelGridMetadataEdited);
        testMandatoryForNewLine(lineModelGridMetadataEdited);
        return null;
    }

    /**
     * Gets the can save.
     *
     * @return the can save
     */
    public Boolean getCanSave() {
        if (metadataSelection == null || !getIsValidForm()) {
            return false;
        }
        boolean isAndCanModified = metadataSelection.edition && !linesModelGridMetadataEdited.isEmpty();
        boolean isAndCanDelete = metadataSelection.delete && getHasDeletedLines();
        return getIsGridOverallValid() && (isAndCanModified || isAndCanDelete);
    }

    /**
     * Gets the column edited.
     *
     * @return the column edited
     */
    public ColumnEdited getColumnEdited() {
        return columnEdited;
    }

    /**
     * Sets the column edited.
     *
     * @param columnEdited the new column edited
     */
    public void setColumnEdited(final ColumnEdited columnEdited) {
        this.columnEdited = columnEdited;
    }

    /**
     * Gets the count deleted lines.
     *
     * @return the count deleted lines
     */
    public Integer getCountDeletedLines() {
        if (modelGridMetadata == null || !metadataSelection.delete) {
            return 0;
        }
        int count = 0;
        for (final Object line : modelGridMetadata.getLinesModelGridMetadatas()) {
            if (((LineModelGridMetadata) line).getToDelete()) {
                count++;
            }
        }
        return count;
    }

    /**
     * Gets the current selections.
     *
     * @return the current selections
     */
    public List<CurrentSelection> getCurrentSelections() {
        if (currentSelections.isEmpty()) {
            currentSelections.add(new CurrentSelection());
        }
        return currentSelections;
    }

    /**
     * Gets the datasets details datatable.
     *
     * @return the datasets details datatable
     */
    public UIDataTable getDatasetsDetailsDatatable() {
        return datasetsDetailsDatatable;
    }

    /**
     * Sets the datasets details datatable.
     *
     * @param datasetsDetailsDatatable the new datasets details datatable
     */
    public void setDatasetsDetailsDatatable(final UIDataTable datasetsDetailsDatatable) {
        this.datasetsDetailsDatatable = datasetsDetailsDatatable;
    }

    /**
     * Gets the checks for deleted lines.
     *
     * @return the checks for deleted lines
     */
    public Boolean getHasDeletedLines() {
        if (modelGridMetadata.getLinesModelGridMetadatas().isEmpty()) {
            return false;
        }
        for (final Object lineModelGridMetadata : modelGridMetadata.getLinesModelGridMetadatas()) {
            if (((LineModelGridMetadata) lineModelGridMetadata).getToDelete()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Gets the checks if is grid overall valid.
     *
     * @return the checks if is grid overall valid
     */
    public Boolean getIsGridOverallValid() {
        for (final LineModelGridMetadata lineModelGridMetadata : linesModelGridMetadataEdited) {
            if (!lineModelGridMetadata.getIsValid()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Gets the checks if is in key path.
     *
     * @return the checks if is in key path
     */
    @AssertFalse(message = "Ligne en doublon!")
    public boolean getIsInKeyPath() {
        if (getLineModelGridMetadataEdited() == null) {
            return false;
        }
        return getKeyPaths().contains(getLineModelGridMetadataEdited().getKeyPath());
    }

    /**
     * Gets the key paths.
     *
     * @return the key paths
     */
    public Set<String> getKeyPaths() {
        return keyPaths;
    }

    /**
     * Gets the line model grid metadata edited.
     *
     * @return the line model grid metadata edited
     */
    public LineModelGridMetadata getLineModelGridMetadataEdited() {
        return lineModelGridMetadataEdited;
    }

    /**
     * Sets the line model grid metadata edited.
     *
     * @param lineModelGridMetadataEdited the new line model grid metadata
     * edited
     */
    public void setLineModelGridMetadataEdited(final LineModelGridMetadata lineModelGridMetadataEdited) {
        this.lineModelGridMetadataEdited = lineModelGridMetadataEdited;
    }

    /**
     * Gets the lines model grid metadata edited.
     *
     * @return the lines model grid metadata edited
     */
    public Set<LineModelGridMetadata> getLinesModelGridMetadataEdited() {
        return linesModelGridMetadataEdited;
    }

    /**
     * Gets the list value.
     *
     * @return the list value
     */
    public String getListValue() {
        return listValue;
    }

    /**
     * Sets the list value.
     *
     * @param listValue the new list value
     */
    public void setListValue(final String listValue) {
        this.listValue = listValue;
    }

    /**
     * Gets the metadatas.
     *
     * @return the metadatas
     */
    public List<MetadataVO> getMetadatas() {
        if (metadatasVO == null) {
            try {
                final List<Metadata> metadatas = metadataManager.retrieveRestrictedMetadatas();
                metadatasVO = new LinkedList<>();
                for (final Metadata metadata : metadatas) {
                    metadatasVO.add(new MetadataVO(metadata));
                }
            } catch (final BusinessException e) {
                UncatchedExceptionLogger.logUncatchedException("uncatched exception in getMetadatas", e);
            }
        }
        return metadatasVO;
    }

    /**
     * Gets the metadata selection.
     *
     * @return the metadata selection
     */
    public MetadataSelection getMetadataSelection() {
        return metadataSelection;
    }

    /**
     * Gets the model grid metadata.
     *
     * @return the model grid metadata
     */
    @SuppressWarnings("rawtypes")
    public ModelGridMetadata getModelGridMetadata() {
        return modelGridMetadata;
    }

    /**
     * Gets the selection tree current.
     *
     * @return the selection tree current
     */
    public CurrentSelection getSelectionTreeCurrent() {
        return null;
    }

    /**
     * Listener upload refdata.
     *
     * @param event the event
     * @throws BusinessException the business exception
     * @link(FileUploadEvent) the event
     */
    public void listenerUploadRefdata(final FileUploadEvent event) throws BusinessException {
        final UploadedFile uploadedFile = event.getUploadedFile();
        FileOutputStream fos = null;
        try {
            final File tmpFile = File.createTempFile("si_metadata-", null);
            fos = new FileOutputStream(tmpFile);
            fos.write(uploadedFile.getData(), 0, uploadedFile.getData().length);
            fos.flush();
            StreamCloser.closeStream(fos);
            metadataManager.uploadMetadatas(tmpFile, metadataSelection.getMetadata().getCode(), uploadedFile.getName().replace("/",""), securityContext.getUtilisateur().getLogin());
            modelGridMetadata = metadataManager.buildModelGridMetadata(metadataSelection.getMetadata().getCode());
        } catch (IOException ex) {
            throw new BusinessException(ex);
        } finally {
            StreamCloser.closeStream(fos);
        }
    }

    /**
     * Navigate.
     *
     * @return the string
     */
    public String navigate() {
        return UIBeanRefdata.RULE_NAVIGATION_JSF;
    }

    /**
     * Save refdatas.
     *
     * @return the string
     */
    public String saveRefdatas() {
        try {
            metadataManager.saveAndDeleteModelGridMetadata(modelGridMetadata, linesModelGridMetadataEdited, metadataSelection.getMetadata().getCode());
            modelGridMetadata = metadataManager.buildModelGridMetadata(metadataSelection.getMetadata().getCode());
            linesModelGridMetadataEdited = new HashSet<>();
        } catch (final BusinessException e) {
            UncatchedExceptionLogger.logUncatchedException("uncatched BusinessException in ".concat(localizationManager.getMessage(UIBeanRefdata.BUNDLE_SOURCE_PATH, UIBeanRefdata.PROPERTY_MSG_DELETE_FAIL)), e);
        } catch (final Exception e) {
            UncatchedExceptionLogger.logUncatchedException("uncatched exception in ".concat(localizationManager.getMessage(UIBeanRefdata.BUNDLE_SOURCE_PATH, UIBeanRefdata.PROPERTY_MSG_DELETE_FAIL)), e);
        }
        return null;
    }

    /**
     * Sets the localization manager.
     *
     * @param localizationManager the new localization manager
     */
    public void setLocalizationManager(final ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    /**
     * Sets the metadata manager.
     *
     * @param metadataManager the new metadata manager
     */
    public void setMetadataManager(final IMetadataManager metadataManager) {
        this.metadataManager = metadataManager;
    }

    /**
     * Sets the metadata selected.
     *
     * @param metadataSelected the new metadata selected
     * @throws BusinessException the business exception
     */
    public void setMetadataSelected(final MetadataVO metadataSelected) throws BusinessException {
        modelGridMetadata = metadataManager.buildModelGridMetadata(metadataSelected.getCode());
        metadataSelection = new MetadataSelection(metadataSelected);
        linesModelGridMetadataEdited = new HashSet<>();
        keyPaths.clear();
        invalidCells = new TreeMap<>();
        for (final Object line : modelGridMetadata.getLinesModelGridMetadatas()) {
            keyPaths.add(((LineModelGridMetadata) line).getKeyPath());
        }
    }

    /**
     * Sets the security context.
     *
     * @param securityContext the new security context
     */
    public void setSecurityContext(final ISecurityContext securityContext) {
        this.securityContext = securityContext;
    }

    /**
     * Validation new line.
     *
     * @param context the context
     * @param component the component
     * @param object the object
     * @link(FacesContext) the context
     * @link(UIComponent) the component
     * @link(Object) the object
     */
    public void validationNewLine(final FacesContext context, final UIComponent component, final Object object) {
        throw new ValidatorException(new FacesMessage("Error"));
    }

    /**
     *
     * @return
     */
    public boolean getIsValidForm() {
        return invalidCells.isEmpty();
    }

    /**
     *
     * @return
     */
    public boolean getIsValidNewForm() {
        if (invalidCells == null || invalidCells.isEmpty()) {
            return true;
        }
        return !invalidCells.containsKey(invalidCells.lastKey());
    }

    private void valid(Integer rowIndex, Integer columnIndex) {
        if (invalidCells.containsKey(rowIndex)) {
            invalidCells.get(rowIndex).remove(columnIndex);
            if (invalidCells.get(rowIndex).isEmpty()) {
                invalidCells.remove(rowIndex);
            }
        }
    }

    private void invalid(Integer rowIndex, Integer columnIndex) {
        if (!invalidCells.containsKey(rowIndex)) {
            invalidCells.put(rowIndex, new LinkedList<Integer>());
        } else {
            invalidCells.get(rowIndex).add(columnIndex);
        }
    }

    private boolean validColumn(BadsFormatsReport badsFormatsReport, Column column, String columnValue, LineModelGridMetadata lineMetadata, ColumnModelGridMetadata columnMetadata) {
        checkValue(badsFormatsReport, column, columnValue, lineMetadata, columnMetadata);
        return !badsFormatsReport.hasErrors();
    }

    private void checkDateValue(final BadsFormatsReport badsFormatsReport, final String value, final Column column) {
        if (AbstractCSVMetadataRecorder.DATE_TYPE.equalsIgnoreCase(column.getValueType().trim()) && value.length() > 0) {
            try {
                final DateFormat dateFormat = DateUtil.getDateFormatLocale(Strings.isNullOrEmpty(column.getFormatType()) ? DateUtil.DD_MM_YYYY : column.getFormatType());
                dateFormat.parse(value);
            } catch (final ParseException e) {
                badsFormatsReport.addException(new BadValueTypeException(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_DATE_VALUE_EXPECTED), column.getName(), column.getFormatType()), e));
            }
        }
    }

    private void checkFloatValue(final BadsFormatsReport badsFormatsReport, final String value, final Column column) {
        if (AbstractCSVMetadataRecorder.FLOAT_TYPE.equals(column.getValueType()) && value.length() > 0) {
            try {
                Float.parseFloat(value.replaceAll(AbstractCSVMetadataRecorder.COMMA, AbstractCSVMetadataRecorder.DOT));
            } catch (final NumberFormatException e) {
                badsFormatsReport.addException(new BadValueTypeException(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_MISSING_FLOAT), column.getName()), e));
            }
        }
    }

    private void checkIntegerValue(final BadsFormatsReport badsFormatsReport, final String value, final Column column) {
        if (AbstractCSVMetadataRecorder.INTEGER_TYPE.equals(column.getValueType()) && value.length() > 0) {
            try {
                Integer.parseInt(value);
            } catch (final NumberFormatException e) {
                badsFormatsReport.addException(new BadValueTypeException(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_MISSING_INTEGER), column.getName()), e));
            }
        }
    }

    private void checkNullableValue(final BadsFormatsReport badsFormatsReport, final String value, final Column column) {
        if (!column.isNullable() && StringUtils.isEmpty(value)) {
            badsFormatsReport.addException(new NullValueException(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_MISSING_VALUE), column.getName())));
        }
    }

    private void checkTimeValue(final BadsFormatsReport badsFormatsReport, final String value, final Column column) {
        if (AbstractCSVMetadataRecorder.TIME_TYPE.equalsIgnoreCase(column.getValueType().trim()) && value.length() > 0) {
            try {
                final DateFormat dateFormat = DateUtil.getDateFormatLocale(Strings.isNullOrEmpty(column.getFormatType()) ? DateUtil.HH_MM : column.getFormatType());
                dateFormat.parse(value);
            } catch (final ParseException e) {
                badsFormatsReport.addException(new BadValueTypeException(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_TIME_VALUE_EXPECTED), column.getName(), column.getFormatType()), e));
            }
        }
    }

    private void checkValue(final BadsFormatsReport badsFormatsReport, final Column column, final String value, LineModelGridMetadata lineMetadata, ColumnModelGridMetadata columnMetadata) {
        if (lineModelGridMetadataEdited.getIsNew()) {
            chekNKey(badsFormatsReport, lineMetadata, columnMetadata);
        }
        checkNullableValue(badsFormatsReport, value, column);
        checkValueByType(badsFormatsReport, value, column);
    }

    private void chekNKey(BadsFormatsReport badsFormatsReport, LineModelGridMetadata lineMetadata, ColumnModelGridMetadata columnMetadata) {
        if (columnMetadata != null && columnMetadata.getIsNKey()) {
            for (Object line : modelGridMetadata.getLinesModelGridMetadatas()) {
                LineModelGridMetadata lineToCompare = (LineModelGridMetadata) line;
                if (lineToCompare.getIsNew()) {
                    return;
                }
                if (lineToCompare.getKeyPath().equals(lineMetadata.getKeyPath())) {
                    badsFormatsReport.addException(new NullValueException(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_DUPLICATE_NKEY), lineMetadata.getKeyPath())));
                }
            }
        }
    }

    private void checkValueByType(final BadsFormatsReport badsFormatsReport, final String value, final Column column) {
        if (column.getValueType() != null) {
            checkDateValue(badsFormatsReport, value, column);
            checkTimeValue(badsFormatsReport, value, column);
            checkFloatValue(badsFormatsReport, value, column);
            checkIntegerValue(badsFormatsReport, value, column);
        }
    }

    private void testMandatoryForNewLine(LineModelGridMetadata lineMetadata) {
        int columnIndex = 0;
        invalidCells.put(0, new LinkedList());
        for (ColumnModelGridMetadata columnMetadata : lineMetadata.getColumnsModelGridMetadatas()) {
            if (columnMetadata.getIsNKey() || columnMetadata.getMandatory()) {
                invalidCells.get(0).add(columnIndex);
            }
            columnIndex++;
        }
        if (invalidCells.get(0).isEmpty()) {
            invalidCells.remove(0);
        }
    }

    /**
     * The Class ColumnEdited.
     */
    public class ColumnEdited {

        /**
         * The column index @link(Integer).
         */
        private Integer columnIndex;
        /**
         * The is long string @link(Boolean).
         */
        private Boolean isLongString;
        /**
         * The row index @link(Integer).
         */
        private Integer rowIndex;
        /**
         * The value @link(String).
         */
        private String value;

        /**
         * Gets the column index.
         *
         * @return the column index
         */
        public Integer getColumnIndex() {
            return columnIndex;
        }

        /**
         * Gets the checks if is long string.
         *
         * @return the checks if is long string
         */
        public Boolean getIsLongString() {
            return isLongString;
        }

        /**
         * Gets the row index.
         *
         * @return the row index
         */
        public Integer getRowIndex() {
            return rowIndex;
        }

        /**
         * Gets the value.
         *
         * @return the value
         */
        public String getValue() {
            return value;
        }

        /**
         * Sets the column index.
         *
         * @param columnIndex the new column index
         */
        public void setColumnIndex(final Integer columnIndex) {
            this.columnIndex = columnIndex;
        }

        /**
         * Sets the checks if is long string.
         *
         * @param isLongString the new checks if is long string
         */
        public void setIsLongString(final Boolean isLongString) {
            this.isLongString = isLongString;
        }

        /**
         * Sets the row index.
         *
         * @param rowIndex the new row index
         */
        public void setRowIndex(final Integer rowIndex) {
            this.rowIndex = rowIndex;
        }

        /**
         * Sets the value.
         *
         * @param value the new value
         */
        public void setValue(final String value) {
            this.value = value;
        }
    }

    /**
     * The Class CurrentSelection.
     */
    public class CurrentSelection {

        /**
         * Checks if is complete.
         *
         * @return the boolean
         */
        public Boolean isComplete() {
            return false;
        }
    }

    /**
     * The Class MetadataSelection.
     */
    public class MetadataSelection {

        /**
         * The delete @link(Boolean).
         */
        private Boolean delete = true;
        /**
         * The download @link(Boolean).
         */
        private Boolean download = true;
        /**
         * The edition @link(Boolean).
         */
        private Boolean edition = true;
        /**
         * The metadata @link(MetadataVO).
         */
        private MetadataVO metadata;

        /**
         * Instantiates a new metadata selection.
         *
         * @param metadataSelected the metadata selected
         * @link(MetadataVO) the metadata selected
         */
        public MetadataSelection(final MetadataVO metadataSelected) {
            metadata = metadataSelected;
            if (!securityContext.isRoot()) {
                download = securityContext.hasPrivilege(metadata.getCode(), ISecurityRefdataManager.ROLE_DOWNLOAD, ISecurityRefdataManager.PRIVILEGE_TYPE_REFDATA);
                edition = securityContext.hasPrivilege(metadata.getCode(), ISecurityRefdataManager.ROLE_EDITION, ISecurityRefdataManager.PRIVILEGE_TYPE_REFDATA);
                delete = securityContext.hasPrivilege(metadata.getCode(), ISecurityRefdataManager.ROLE_DELETE, ISecurityRefdataManager.PRIVILEGE_TYPE_REFDATA);
            }
            if ("types_de_donnees".equals(metadataSelected.getCode())) {
                delete = false;
            }
        }

        /**
         * Gets the delete.
         *
         * @return the delete
         */
        public Boolean getDelete() {
            return delete;
        }

        /**
         * Gets the download.
         *
         * @return the download
         */
        public Boolean getDownload() {
            return download;
        }

        /**
         * Gets the edition.
         *
         * @return the edition
         */
        public Boolean getEdition() {
            return edition;
        }

        /**
         * Gets the final metadata.
         *
         * @return the final metadata
         */
        public boolean getFinalMetadata() {
            return metadata.getFinalMetadata();
        }

        /**
         * Gets the metadata.
         *
         * @return the metadata
         */
        public MetadataVO getMetadata() {
            return metadata;
        }

        /**
         * Sets the delete.
         *
         * @param delete the new delete
         */
        public void setDelete(final Boolean delete) {
            this.delete = delete;
        }

        /**
         * Sets the download.
         *
         * @param download the new download
         */
        public void setDownload(final Boolean download) {
            this.download = download;
        }

        /**
         * Sets the edition.
         *
         * @param edition the new edition
         */
        public void setEdition(final Boolean edition) {
            this.edition = edition;
        }

        /**
         * Sets the metadata.
         *
         * @param metadata the new metadata
         */
        public void setMetadata(final MetadataVO metadata) {
            this.metadata = metadata;
        }
    }

    /**
     * The Class MetadataVO.
     */
    public class MetadataVO implements Serializable {

        /**
         * The Constant serialVersionUID @link(long).
         */
        private static final long serialVersionUID = 1L;
        /**
         * The code @link(String).
         */
        private final String code;
        /**
         * The final metadata @link(boolean).
         */
        private boolean finalMetadata = false;
        /**
         * The name @link(String).
         */
        private final String name;

        /**
         * Instantiates a new metadata vo.
         *
         * @param metadata the metadata
         * @link(Metadata) the metadata
         */
        public MetadataVO(final Metadata metadata) {
            final String localizedName = metadata.getInternationalizedNames().get(getLanguage());
            this.name = localizedName == null ? metadata.getName() : localizedName;
            this.code = Utils.createCodeFromString(metadata.getName());
            this.finalMetadata = metadata.getFinalMetadata();
        }

        /**
         * Gets the code.
         *
         * @return the code
         */
        public String getCode() {
            return code;
        }

        /**
         * Gets the final metadata.
         *
         * @return the final metadata
         */
        public boolean getFinalMetadata() {
            return finalMetadata;
        }

        /**
         * Gets the language.
         *
         * @return the language
         */
        protected String getLanguage() {
            return FacesContext.getCurrentInstance().getViewRoot().getLocale().getLanguage();
        }

        /**
         * Gets the name.
         *
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * Sets the final metadata.
         *
         * @param finalMetadata the new final metadata
         */
        public void setFinalMetadata(final String finalMetadata) {
            this.finalMetadata = Boolean.parseBoolean(finalMetadata);
        }
    }
}
