package org.inra.ecoinfo.zooplancton.services;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.inra.ecoinfo.glacpe.extraction.vo.VariableVO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.taxon.Taxon;
import org.inra.ecoinfo.glacpe.refdata.taxon.TaxonProprieteTaxon;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public interface IZooplanctonDatatypeManager {

    /**
     *
     */
    public static final String CODE_DATATYPE = "zooplancton";

    /**
     *
     * @param plateformesIds
     * @param firstDate
     * @param endDate
     * @return
     * @throws BusinessException
     */
    List<VariableVO> retrieveListsVariables(List<Long> plateformesIds, Date firstDate, Date endDate) throws BusinessException;

    /**
     *
     * @return
     * @throws BusinessException
     */
    List<Projet> retrieveAvailablesProjets() throws BusinessException;

    /**
     *
     * @return
     * @throws BusinessException
     */
    Map<Taxon, Boolean> retrieveZooplanctonAvailablesTaxons() throws BusinessException;

    /**
     *
     * @return
     * @throws BusinessException
     */
    List<Taxon> retrieveRootTaxon() throws BusinessException;

    /**
     *
     * @param projetSiteId
     * @return
     * @throws BusinessException
     */
    List<Plateforme> retrieveAvailablesPlateformesByProjetId(long projetSiteId) throws BusinessException;

    /**
     *
     * @param taxonId
     * @return
     * @throws BusinessException
     */
    List<TaxonProprieteTaxon> retrieveTaxonPropertyByTaxonId(long taxonId) throws BusinessException;

    /**
     *
     * @param taxonCode
     * @return
     * @throws BusinessException
     */
    Taxon retrieveTaxonByCode(String taxonCode) throws BusinessException;
}
