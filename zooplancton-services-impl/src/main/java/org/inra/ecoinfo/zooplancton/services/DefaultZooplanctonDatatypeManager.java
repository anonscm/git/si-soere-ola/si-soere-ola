package org.inra.ecoinfo.zooplancton.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.inra.ecoinfo.MO;
import org.inra.ecoinfo.glacpe.extraction.vo.VariableVO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.proprietetaxon.ProprieteTaxon;
import org.inra.ecoinfo.glacpe.refdata.taxon.ITaxonDAO;
import org.inra.ecoinfo.glacpe.refdata.taxon.Taxon;
import org.inra.ecoinfo.glacpe.refdata.taxon.TaxonProprieteTaxon;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.datatype.IDatatypeDAO;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ptcherniati
 */
public class DefaultZooplanctonDatatypeManager extends MO implements IZooplanctonDatatypeManager {

    /**
     *
     */
    protected IZooplanctonDAO zooplanctonDAO;

    /**
     *
     */
    protected IDatatypeDAO datatypeDAO;

    /**
     *
     */
    protected ITaxonDAO taxonDAO;

    /**
     *
     * @return
     * @throws BusinessException
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public List<Projet> retrieveAvailablesProjets() throws BusinessException {
        try {
            return zooplanctonDAO.retrieveAvailablesProjets();
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    private List<VariableVO> retrieveListVariables() throws BusinessException {
        try {
            List<VariableGLACPE> variables = (List) ((DataType) datatypeDAO.getByCode(IZooplanctonDatatypeManager.CODE_DATATYPE)).getVariables();
            List<VariableVO> variablesVO = new ArrayList<VariableVO>();

            for (VariableGLACPE variable : variables) {
                VariableVO variableVO = new VariableVO(variable);
                variablesVO.add(variableVO);
            }
            return variablesVO;
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    private List<VariableVO> retrieveListAvailableVariables(List<Long> plateformesIds, Date firstDate, Date endDate) throws BusinessException {
        try {
            List<VariableGLACPE> variables = zooplanctonDAO.retrieveAvailablesVariables(plateformesIds, firstDate, endDate);
            List<VariableVO> variablesVO = new ArrayList<VariableVO>();

            for (VariableGLACPE variable : variables) {
                VariableVO variableVO = new VariableVO(variable);
                variablesVO.add(variableVO);
            }
            return variablesVO;
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    /**
     *
     * @param plateformesIds
     * @param firstDate
     * @param lastDate
     * @return
     * @throws BusinessException
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public List<VariableVO> retrieveListsVariables(List<Long> plateformesIds, Date firstDate, Date lastDate) throws BusinessException {
        Map<String, VariableVO> rawVariablesMap = new HashMap<String, VariableVO>();

        for (VariableVO variable : retrieveListVariables()) {
            rawVariablesMap.put(variable.getCode(), variable);
        }

        for (VariableVO availableVariable : retrieveListAvailableVariables(plateformesIds, firstDate, lastDate)) {
            availableVariable.setIsDataAvailable(true);
            rawVariablesMap.put(availableVariable.getCode(), availableVariable);
        }

        return new LinkedList<VariableVO>(rawVariablesMap.values());
    }

    /**
     *
     * @return
     * @throws BusinessException
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public Map<Taxon, Boolean> retrieveZooplanctonAvailablesTaxons() throws BusinessException {

        try {
            List<Taxon> taxons = taxonDAO.getTaxonByTheme(Taxon.VALUES_ATTR_THEME.ZOOPLANCTON);
            Map<Taxon, Boolean> taxonsInfos = null;

            if (taxons != null && !taxons.isEmpty()) {
                taxonsInfos = new HashMap<>();

                for (Taxon taxon : (List<Taxon>) taxons) {
                    Boolean isPreselected = false;
                    for (TaxonProprieteTaxon tpt : taxon.getTaxonProprieteTaxon()) {
                        if (tpt.getProprieteTaxon().getCode().equals(ProprieteTaxon.VALUES_ATTR_CODE.PRESELECTED) && tpt.getValeurProprieteTaxon().getStringValue() != null && !tpt.getValeurProprieteTaxon().getStringValue().trim().isEmpty()) {
                            isPreselected = true;
                            break;
                        }
                    }

                    taxonsInfos.put(taxon, isPreselected);
                }

            }
            return taxonsInfos;
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }

    }

    /**
     *
     * @return
     * @throws BusinessException
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public List<Taxon> retrieveRootTaxon() throws BusinessException {

        try {
            return taxonDAO.getZooRootTaxa();
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }

    }

    /**
     *
     * @param datatypeDAO
     */
    public void setDatatypeDAO(IDatatypeDAO datatypeDAO) {
        this.datatypeDAO = datatypeDAO;
    }

    /**
     *
     * @param taxonDAO
     */
    public void setTaxonDAO(ITaxonDAO taxonDAO) {
        this.taxonDAO = taxonDAO;
    }

    /**
     *
     * @param projetId
     * @return
     * @throws BusinessException
     */
    @Override
    public List<Plateforme> retrieveAvailablesPlateformesByProjetId(long projetId) throws BusinessException {
        try {
            return zooplanctonDAO.retrieveAvailablesPlateformesByProjetId(projetId);
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    /**
     *
     * @param taxonId
     * @return
     * @throws BusinessException
     */
    @Override
    public List<TaxonProprieteTaxon> retrieveTaxonPropertyByTaxonId(long taxonId) throws BusinessException {
        try {
            return taxonDAO.getTaxonProprieteTaxonByTaxonId(taxonId);
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    /**
     *
     * @param taxonCode
     * @return
     * @throws BusinessException
     */
    @Override
    public Taxon retrieveTaxonByCode(String taxonCode) throws BusinessException {
        try {
            return taxonDAO.getByCode(taxonCode);
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    /**
     *
     * @param zooplanctonDAO
     */
    public void setZooplanctonDAO(IZooplanctonDAO zooplanctonDAO) {
        this.zooplanctonDAO = zooplanctonDAO;
    }
}
