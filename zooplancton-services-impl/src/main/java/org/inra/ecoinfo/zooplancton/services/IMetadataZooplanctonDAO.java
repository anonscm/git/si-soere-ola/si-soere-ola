package org.inra.ecoinfo.zooplancton.services;

import java.util.Date;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.inra.ecoinfo.zooplancton.entity.MetadataZooplancton;

/**
 *
 * @author ptcherniati
 */
public interface IMetadataZooplanctonDAO extends IDAO<MetadataZooplancton> {

    /**
     *
     * @param datePrelevement
     * @param keyProjet
     * @param keyPlateforme
     * @param keyOutilsPrelevement
     * @return
     * @throws PersistenceException
     */
    MetadataZooplancton getByNKey(Date datePrelevement, String keyProjet, String keyPlateforme, String keyOutilsPrelevement) throws PersistenceException;

    @Override
    void flush();
}
