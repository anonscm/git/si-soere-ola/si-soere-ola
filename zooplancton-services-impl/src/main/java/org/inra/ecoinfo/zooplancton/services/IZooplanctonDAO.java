package org.inra.ecoinfo.zooplancton.services;

import java.util.Date;
import java.util.List;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesRequestParamVO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public interface IZooplanctonDAO extends IDAO<Object> {

    /**
     *
     * @return
     * @throws PersistenceException
     */
    List<Projet> retrieveAvailablesProjets() throws PersistenceException;

    /**
     *
     * @param plateformesIds
     * @param firstDate
     * @param endDate
     * @return
     * @throws PersistenceException
     */
    List<VariableGLACPE> retrieveAvailablesVariables(List<Long> plateformesIds, Date firstDate, Date endDate) throws PersistenceException;

    /**
     *
     * @param projetSiteId
     * @return
     * @throws PersistenceException
     */
    List<Plateforme> retrieveAvailablesPlateformesByProjetId(long projetSiteId) throws PersistenceException;

    /**
     *
     * @param selectedPlateformesIds
     * @param projetId
     * @param datesRequestParamVO
     * @param selectedTaxonsIds
     * @param idsVariables
     * @return
     * @throws PersistenceException
     */
    List<ResultExtractionZooplanctonRawsDatas> extractDatas(List<Long> selectedPlateformesIds, Long projetId, DatesRequestParamVO datesRequestParamVO, List<Long> selectedTaxonsIds, List<Long> idsVariables) throws PersistenceException;

    /**
     *
     * @param plateformesIds
     * @param projetId
     * @param datesRequestParamVO
     * @return
     * @throws PersistenceException
     */
    List<ResultExtractionZooplanctonBiovolume> extractDatasBiovolume(List<Long> plateformesIds, Long projetId, DatesRequestParamVO datesRequestParamVO) throws PersistenceException;

}