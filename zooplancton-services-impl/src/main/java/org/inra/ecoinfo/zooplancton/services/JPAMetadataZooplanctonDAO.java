package org.inra.ecoinfo.zooplancton.services;

import java.util.Date;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.inra.ecoinfo.zooplancton.entity.MetadataZooplancton;

/**
 *
 * @author ptcherniati
 */
public class JPAMetadataZooplanctonDAO extends AbstractJPADAO<MetadataZooplancton> implements IMetadataZooplanctonDAO {

    /**
     *
     * @param datePrelevement
     * @param keyProjet
     * @param keyPlateforme
     * @return
     * @throws PersistenceException
     */
    @Override
    public MetadataZooplancton getByNKey(Date datePrelevement, String keyProjet, String keyPlateforme, String keyOutilsPrelevement) throws PersistenceException {
        try {
            Query query = null;

            String queryString = "from MetadataZooplancton mdtz where mdtz.date = :date and mdtz.projet.code = :keyProjet and mdtz.plateforme.code = :keyPlateforme and mdtz.outilsPrelevement.code = :keyOutilsPrelevement";

            query = entityManager.createQuery(queryString);
            query.setParameter("date", datePrelevement);
            query.setParameter("keyProjet", keyProjet);
            query.setParameter("keyPlateforme", keyPlateforme);
            query.setParameter("keyOutilsPrelevement", keyOutilsPrelevement);

            return (MetadataZooplancton) query.getSingleResult();

        } catch (NoResultException e) {
            return null;
        } catch (Exception e) {
            throw new PersistenceException(e);
        }

    }

}
