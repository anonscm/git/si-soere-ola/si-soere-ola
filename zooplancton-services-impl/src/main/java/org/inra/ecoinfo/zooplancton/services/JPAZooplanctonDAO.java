package org.inra.ecoinfo.zooplancton.services;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.glacpe.extraction.IDatatypeSpecifigProjectStatisticsDAO;
import org.inra.ecoinfo.glacpe.extraction.jpa.AbstractJPAProjectSpecificDatatypeStatisticsDAO;
import org.inra.ecoinfo.glacpe.extraction.vo.AbstractDatesFormParam;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesRequestParamVO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class JPAZooplanctonDAO extends AbstractJPADAO<Object> implements IZooplanctonDAO {

    private static final String REQUEST_ZOO_AVAILABLES_PROJETS = "select distinct m.projet from MetadataZooplancton m";
    private static final String REQUEST_ZOO_AVAILABLES_PLATEFORMES_BY_PROJET_ID = "select distinct mz.plateforme from MetadataZooplancton mz where mz.projet.id = :projetId";
    private static final String REQUEST_ZOO_AVAILABLES_VARIABLES_BY_PLATEFORMES_ID = "select distinct dz.variable from DataZooplancton dz where dz.metadataZooplancton.plateforme.id in (:plateformesIds) and dz.metadataZooplancton.date between :firstDate and :lastDate and dz.value is not null";

    private static final String REQUEST_EXTRACTION_RAWS_DATA = "select new org.inra.ecoinfo.zooplancton.services.ResultExtractionZooplanctonRawsDatas("
            + "dz.metadataZooplancton.projet.nom,"
            + "dz.metadataZooplancton.plateforme.site.nom,"
            + "dz.metadataZooplancton.plateforme.nom, "
            + "dz.metadataZooplancton.date,"
            + "dz.metadataZooplancton.outilsPrelevement.nom,"
            + " dz.metadataZooplancton.outilsMesure.nom, "
            + "dz.metadataZooplancton.profondeurMin, "
            + "dz.metadataZooplancton.profondeurMax, "
            + "dz.metadataZooplancton.nomDeterminateur,"
            + "dz.metadataZooplancton.volumeSedimente,"
            + "dz.taxon.nomLatin,"
            + "dz.stadeDeveloppement.nom, "
            + "dz.variable.nom, "
            + "dz.value,"
            + "dz.metadataZooplancton.versionFile.dataset.leafNode,"
            
            + "dz.variable.codeSandre,"
            + "dz.variable.contexte,"
            + "dz.metadataZooplancton.plateforme.site.codeSandrePe,"
            + "dz.metadataZooplancton.plateforme.site.codeSandreMe,"
            + "dz.metadataZooplancton.plateforme.codeSandre,"
            + "dz.metadataZooplancton.plateforme.contexte,"
            + "dz.metadataZooplancton.plateforme.latitude,"
            + "dz.metadataZooplancton.plateforme.longitude,"
            + "dz.metadataZooplancton.plateforme.altitude,"
            + "dz.taxon.taxonParent.nomLatin,"
            + "dz.taxon.codeSandreTax,"
            + "dz.taxon.taxonParent.codeSandreTax,"
            + "dz.taxon.taxonNiveau.nom,"
            + "dz.taxon.taxonNiveau.codeSandre,"
            + "dz.taxon.taxonNiveau.contexte,"
            + "dz.stadeDeveloppement.codeSandre,"
            + "dz.stadeDeveloppement.contexte"
            + ") "
            + "from DataZooplancton dz "
            + "where dz.metadataZooplancton.plateforme.id in (:selectedPlateformesIds) "
            + "and dz.metadataZooplancton.projet.id in (:selectedProjetId) "
            + "and dz.variable.id in (:idsVariablesSelected) "
            + "and dz.taxon.id in (:selectedTaxonsIds) %s ";
    

    
    
    
    private static final String REQUEST_EXTRACTION_BIOVOLUME = "select new org.inra.ecoinfo.zooplancton.services.ResultExtractionZooplanctonBiovolume(mtdz.projet.nom,mtdz.plateforme.site.nom,mtdz.plateforme.nom, mtdz.date, mtdz.outilsPrelevement.nom, mtdz.outilsMesure.nom, mtdz.profondeurMin, mtdz.profondeurMax, mtdz.nomDeterminateur,mtdz.volumeSedimente) from MetadataZooplancton mtdz where mtdz.plateforme.id in (:selectedPlateformesIds) and mtdz.projet.id in (:selectedProjetId) %s order by mtdz.projet.nom,mtdz.plateforme.site.nom,mtdz.plateforme.nom, mtdz.date";

    private static final String HQL_DATE_FIELD_2 = "mtdz.date";
    private static final String HQL_DATE_FIELD_1 = "dz.metadataZooplancton.date";

    private IDatatypeSpecifigProjectStatisticsDAO datatypeStatisticsDAO;

    /**
     *
     * @return
     * @throws PersistenceException
     */
    @Override
    public List<Projet> retrieveAvailablesProjets() throws PersistenceException {
        try {

            Query query = entityManager.createQuery(REQUEST_ZOO_AVAILABLES_PROJETS);
            @SuppressWarnings("unchecked")
            List<Projet> projets = query.getResultList();

            return projets;
        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param plateformesIds
     * @param firstDate
     * @param endDate
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<VariableGLACPE> retrieveAvailablesVariables(List<Long> plateformesIds, Date firstDate, Date endDate) throws PersistenceException {

        List<VariableGLACPE> variables;
        try {
            Query query = entityManager.createQuery(REQUEST_ZOO_AVAILABLES_VARIABLES_BY_PLATEFORMES_ID);

            query.setParameter("plateformesIds", plateformesIds);
            query.setParameter("firstDate", firstDate);
            query.setParameter("lastDate", endDate);
            variables = query.getResultList();
            return variables;
        } catch (Exception e) {
            AbstractJPADAO.LOGGER.error(e);
            throw new PersistenceException(e);
        }

    }

    /**
     *
     * @param selectedPlateformesIds
     * @param projetId
     * @param datesRequestParamVO
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<ResultExtractionZooplanctonBiovolume> extractDatasBiovolume(List<Long> selectedPlateformesIds, Long projetId, DatesRequestParamVO datesRequestParamVO) throws PersistenceException {
        try {
            AbstractDatesFormParam datesFormParam = datesRequestParamVO.getCurrentDatesFormParam();
            Query query = entityManager.createQuery(String.format(REQUEST_EXTRACTION_BIOVOLUME, datesFormParam.buildSQLCondition(HQL_DATE_FIELD_2)));
            Map<String, Date> datesMap = datesFormParam.retrieveParametersMap();

            for (String key : datesMap.keySet()) {
                query.setParameter(key, datesMap.get(key));
            }

            query.setParameter("selectedPlateformesIds", selectedPlateformesIds);
            query.setParameter("selectedProjetId", projetId);

            return (List<ResultExtractionZooplanctonBiovolume>) query.getResultList();

        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }

    }

    /**
     *
     * @param selectedPlateformesIds
     * @param projetId
     * @param datesRequestParamVO
     * @param selectedTaxonsIds
     * @param idsVariablesSelected
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<ResultExtractionZooplanctonRawsDatas> extractDatas(List<Long> selectedPlateformesIds, Long projetId, DatesRequestParamVO datesRequestParamVO, List<Long> selectedTaxonsIds, List<Long> idsVariablesSelected) throws PersistenceException {

        try {
            AbstractDatesFormParam datesFormParam = datesRequestParamVO.getCurrentDatesFormParam();
            Query query = entityManager.createQuery(String.format(REQUEST_EXTRACTION_RAWS_DATA, datesFormParam.buildSQLCondition(HQL_DATE_FIELD_1)));
            Map<String, Date> datesMap = datesFormParam.retrieveParametersMap();

            for (String key : datesMap.keySet()) {
                query.setParameter(key, datesMap.get(key));
            }

            query.setParameter("selectedPlateformesIds", selectedPlateformesIds);
            query.setParameter("selectedProjetId", projetId);
            query.setParameter("selectedTaxonsIds", selectedTaxonsIds);
            query.setParameter("idsVariablesSelected", idsVariablesSelected);

            return (List<ResultExtractionZooplanctonRawsDatas>) query.getResultList();

        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }

    }

    /**
     *
     * @param projetId
     * @return
     * @throws PersistenceException
     */
    @Override
    public List<Plateforme> retrieveAvailablesPlateformesByProjetId(long projetId) throws PersistenceException {

        try {

            Query query = entityManager.createQuery(REQUEST_ZOO_AVAILABLES_PLATEFORMES_BY_PROJET_ID);

            query.setParameter("projetId", projetId);
            @SuppressWarnings("unchecked")
            List<Plateforme> plateformes = query.getResultList();

            return plateformes;
        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }

    }

    /**
     *
     * @param datatypeStatisticsDAO
     */
    public void setDatatypeStatisticsDAO(IDatatypeSpecifigProjectStatisticsDAO datatypeStatisticsDAO) {
        this.datatypeStatisticsDAO = datatypeStatisticsDAO;
    }

    /**
     *
     */
    public static class JPADatatypeStatisticsDAO extends AbstractJPAProjectSpecificDatatypeStatisticsDAO {

        private static final String REQUEST_AllDepthAggregatedDatas = "select vm from ValeurMesurePhytoplancton vm where vm.mesure.sousSequence.plateforme.id in (:selectedPlateformesIds) and vm.mesure.sousSequence.sequence.projetSite.id in (:selectedProjetId) and vm.variable.id in (:selectedVariablesIds) %s  order by vm.mesure.sousSequence.sequence.projetSite.projet.nom,vm.mesure.sousSequence.plateforme.site.nom,vm.mesure.sousSequence.plateforme.nom,vm.mesure.sousSequence.sequence.datePrelevement";

        /**
         *
         * @return
         */
        @Override
        protected String getQLRequestAggregatedDatasForAllDepth() {
            return REQUEST_AllDepthAggregatedDatas;
        }

        /**
         *
         * @return
         */
        @Override
        protected String getQLRequestAggregatedDatasForRangeDepth() {
            return null;
        }

        @Override
        protected String getQLRequestDatasForAllDepth() {
            return null;
        }

        /**
         *
         * @return
         */
        @Override
        protected String getRequestDatasForRangeDepth() {
            return null;
        }
    }

}
