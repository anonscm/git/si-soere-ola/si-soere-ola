package org.inra.ecoinfo.zooplancton.services;

import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_ERROR;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_FLOAT_VALUE_EXPECTED;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_MISSING_OUTILS_MESURE_IN_REFERENCES_DATAS;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_MISSING_PROJET_SITE_IN_REFERENCES_DATAS;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.getGLACPEMessage;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.getGLACPEMessageWithBundle;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.inra.ecoinfo.dataset.exception.InsertionDatabaseException;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.glacpe.dataset.impl.AbstractProcessRecord;
import org.inra.ecoinfo.glacpe.dataset.impl.CleanerValues;
import org.inra.ecoinfo.glacpe.dataset.impl.VariableValue;
import org.inra.ecoinfo.glacpe.refdata.controlecoherence.ControleCoherence;
import org.inra.ecoinfo.glacpe.refdata.controlecoherence.IControleCoherenceDAO;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.IOutilsMesureDAO;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.OutilsMesure;
import org.inra.ecoinfo.glacpe.refdata.plateforme.IPlateformeDAO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.IProjetDAO;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.projetsitethemedatatype.ProjetSiteThemeDatatype;
import org.inra.ecoinfo.glacpe.refdata.taxon.ITaxonDAO;
import org.inra.ecoinfo.glacpe.refdata.taxon.Taxon;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.DatasetDescriptorBuilder;
import org.inra.ecoinfo.utils.ErrorsReport;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.inra.ecoinfo.zooplancton.entity.DataZooplancton;
import org.inra.ecoinfo.zooplancton.entity.MetadataZooplancton;
import org.inra.ecoinfo.zooplancton.entity.StadeDeveloppement;
import org.inra.ecoinfo.zooplancton.services.stadedeveloppement.IStadeDeveloppementDAO;
import org.xml.sax.SAXException;

import com.Ostermiller.util.CSVParser;
import java.util.HashSet;
import java.util.Set;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_TWICE_DATA;

/**
 *
 * @author ptcherniati
 */
public class ProcessRecord extends AbstractProcessRecord {

    private static final String STRING_EMPTY = "";

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    protected IMetadataZooplanctonDAO metadataZooplanctonDAO;

    /**
     *
     */
    protected IOutilsMesureDAO outilsMesureDAO;

    /**
     *
     */
    protected IControleCoherenceDAO controleCoherenceDAO;

    /**
     *
     */
    protected IPlateformeDAO plateformeDAO;

    /**
     *
     */
    protected ITaxonDAO taxonDAO;

    /**
     *
     */
    protected IProjetDAO projetDAO;
    private IStadeDeveloppementDAO stadeDeveloppementDAO;

    /**
     *
     * @param stadeDeveloppementDAO
     */
    public void setStadeDeveloppementDAO(IStadeDeveloppementDAO stadeDeveloppementDAO) {
        this.stadeDeveloppementDAO = stadeDeveloppementDAO;
    }

    /**
     *
     * @return @throws IOException
     * @throws SAXException
     */
    @Override
    public DatasetDescriptor holdDatasetDescriptor() throws IOException, SAXException {
        if (datasetDescriptor == null) {
            datasetDescriptor = DatasetDescriptorBuilder.buildDescriptor(this.getClass().getResource(DATASET_DESCRIPTOR_XML).openStream());
        }
        return datasetDescriptor;
    }

    private static final String PATH_BUNDLE_SOURCE = "org.inra.ecoinfo.zooplancton.services.messages";

    private static final String PROPERTY_MSG_MISSING_OUTILS_PRELEVEMENT_IN_REFERENCES_DATAS = "MSG_MISSING_OUTILS_PRELEVEMENT_IN_REFERENCES_DATAS";
    private static final String PROPERTY_MSG_MISSING_TAXON_IN_REFERENCES_DATAS = "MSG_MISSING_TAXON_IN_REFERENCES_DATAS";
    private static final String PROPERTY_MSG_MISSING_STADES_DEVELOPPEMENT_IN_REFERENCES_DATAS = "MSG_MISSING_STADES_DEVELOPPEMENT_IN_REFERENCES_DATAS";
    private static final String PROPERTY_MSG_MISSING_PLATEFORME_IN_REFERENCES_DATAS = "MSG_MISSING_PLATEFORME_IN_REFERENCES_DATAS";

    @Override
    public void processRecord(CSVParser parser, VersionFile versionFile, String fileEncoding, DatasetDescriptor datasetDescriptor) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport(getGLACPEMessage(PROPERTY_MSG_ERROR));
        Utils.testNotNullArguments(getLocalizationManager(), versionFile, fileEncoding);
        Utils.testCastedArguments(versionFile, VersionFile.class, getLocalizationManager());

        try {

            String[] values = null;
            long lineCount = 1;
            int variableHeaderIndex = datasetDescriptor.getUndefinedColumn();

            Map<String, ControleCoherence> controlesCoherenceMap = controleCoherenceDAO.getControleCoherenceBySitecodeAndDatatypecode(((ProjetSiteThemeDatatype) versionFile.getDataset().getLeafNode()).getProjetSiteTheme().getProjetSite().getSite()
                    .getCode(), versionFile.getDataset().getLeafNode().getDatatype().getCode());
            List<VariableGLACPE> dbVariables = buildVariableHeaderAndSkipHeader(variableHeaderIndex, parser, errorsReport);
            Map<String, List<LineRecord>> metadatasMapLines = new HashMap<String, List<LineRecord>>();

            // On parcourt chaque ligne du fichier
            Set<String> doublonSet = new HashSet();
            
            while ((values = parser.getLine()) != null) {
                
                CleanerValues cleanerValues = new CleanerValues(values);
                lineCount++;
                List<VariableValue> variablesValues = new LinkedList<VariableValue>();

                // On parcourt chaque colonne d'une ligne
                String projetCode = Utils.createCodeFromString(cleanerValues.nextToken());
                String nomSite = Utils.createCodeFromString(cleanerValues.nextToken());
                String plateformeCode = Utils.createCodeFromString(cleanerValues.nextToken());
                String datePrelevementString = cleanerValues.nextToken();
                Date datePrelevement = datePrelevementString.length() > 0 ? new SimpleDateFormat(datasetDescriptor.getColumns().get(cleanerValues.currentTokenIndex() - 1).getFormatType()).parse(datePrelevementString) : null;
                String outilMesureCode = Utils.createCodeFromString(cleanerValues.nextToken());
                String outilPrelevementCode = Utils.createCodeFromString(cleanerValues.nextToken());

                String profondeurMinString = cleanerValues.nextToken();
                Float profondeurMin = profondeurMinString.length() > 0 ? Float.parseFloat(profondeurMinString) : 0.0f;
                String profondeurMaxString = cleanerValues.nextToken();
                Float profondeurMax = profondeurMaxString.length() > 0 ? Float.parseFloat(profondeurMaxString) : 0.0f;

                String determinateur = cleanerValues.nextToken();
                String volumeSedimenteString = cleanerValues.nextToken();
                Float volumeSedimente = volumeSedimenteString.length() > 0 ? Float.parseFloat(volumeSedimenteString) : null;
                String nomTaxon = Utils.createCodeFromString(cleanerValues.nextToken());
                String stadeDeveloppement = Utils.createCodeFromString(cleanerValues.nextToken());

                for (int actualVariableArray = variableHeaderIndex; actualVariableArray < values.length; actualVariableArray++) {
                    String value = values[actualVariableArray].replaceAll(" ", STRING_EMPTY);
                    try {
                        VariableGLACPE variable = dbVariables.get(actualVariableArray - variableHeaderIndex);
                        if (controlesCoherenceMap.get(variable.getCode()) != null) {
                            testValueCoherence(Float.valueOf(value), controlesCoherenceMap.get(variable.getCode()).getValeurMin(), controlesCoherenceMap.get(variable.getCode()).getValeurMax(), lineCount, 11);
                        }
                        variablesValues.add(new VariableValue(dbVariables.get(actualVariableArray - variableHeaderIndex), values[actualVariableArray].replaceAll(" ", STRING_EMPTY)));
                    } catch (NumberFormatException e) {
                        errorsReport.addException(new BadExpectedValueException(String.format(getGLACPEMessage(PROPERTY_MSG_FLOAT_VALUE_EXPECTED), lineCount, 11, value)));
                    } catch (BadExpectedValueException e) {
                        errorsReport.addException(e);
                    }
                }

                LineRecord line = new LineRecord(nomSite, plateformeCode, datePrelevement, profondeurMin, profondeurMax, outilMesureCode, outilPrelevementCode, determinateur, volumeSedimente, stadeDeveloppement, nomTaxon, variablesValues, lineCount,
                        projetCode);
                
                if (!doublonSet.contains(line.toString())){
                    doublonSet.add(line.toString());
                } else {
                    errorsReport.addException(new PersistenceException(String.format(getGLACPEMessage(PROPERTY_MSG_TWICE_DATA),line.getOriginalLineNumber())));
                }
                
                String metadataKey = projetCode.concat(plateformeCode).concat(datePrelevementString).concat(Utils.createCodeFromString(nomSite)).concat(outilPrelevementCode);
                fillLinesMap(metadatasMapLines, line, metadataKey);

            }
            if (errorsReport.hasErrors()) {
                throw new PersistenceException(errorsReport.buildHTMLMessages());
            }
            int count = 0;
            doublonSet = null;
            List<String> listErrors = new LinkedList<>();
            for (String metadataKey : metadatasMapLines.keySet()) {

                List<LineRecord> metadataLines = metadatasMapLines.get(metadataKey);
                if (!metadataLines.isEmpty()) {
                    try {
                        Date datePrelevement = metadataLines.get(0).getDatePrelevement();
                        String codeProjet = metadataLines.get(0).getProjetCode();
                        String codePlateforme = metadataLines.get(0).getPlateformeCode();
                        String codeOutilsPrelevement = metadataLines.get(0).getOutilsPrelevementCode();
                        String codeSite = Utils.createCodeFromString(metadataLines.get(0).getNomSite());
                        buildMetadata(datePrelevement, codeProjet, codePlateforme, codeSite, codeOutilsPrelevement, metadataLines, versionFile, errorsReport, listErrors);
                        logger.debug(String.format("%d - %s", count++, datePrelevement));

                        // Très important à cause de problèmes de performances
                        metadataZooplanctonDAO.flush();
                        versionFile = (VersionFile) versionFileDAO.merge(versionFile);

                    } catch (InsertionDatabaseException e) {
                        errorsReport.addException(e);
                    }
                }
            }

            if (errorsReport.hasErrors()) {
                throw new PersistenceException(errorsReport.buildHTMLMessages());
            }

        } catch (Exception e) {

            throw new BusinessException(e);
        }
    }

    private void fillLinesMap(Map<String, List<LineRecord>> linesMap, LineRecord line, String keyLine) {
        List<LineRecord> datasLines = null;
        if (keyLine != null && keyLine.trim().length() > 0) {
            if (linesMap.containsKey(keyLine)) {
                linesMap.get(keyLine).add(line);
            } else {
                datasLines = new LinkedList<LineRecord>();
                datasLines.add(line);
                linesMap.put(keyLine, datasLines);
            }
        }
    }

    private List<VariableGLACPE> buildVariableHeaderAndSkipHeader(int variableHeaderIndex, CSVParser parser, ErrorsReport errorsReport) throws PersistenceException, IOException {
        List<String> variablesHeaders = skipHeaderAndRetrieveVariableName(variableHeaderIndex, parser);
        List<VariableGLACPE> dbVariables = new LinkedList<VariableGLACPE>();
        for (String variableHeader : variablesHeaders) {
            VariableGLACPE variable = (VariableGLACPE) variableDAO.getByCode(Utils.createCodeFromString(variableHeader));
            if (variable == null) {
                errorsReport.addException(new PersistenceException(String.format(getGLACPEMessage(PROPERTY_MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS), variableHeader)));
            }

            dbVariables.add(variable);
        }
        if (errorsReport.hasErrors()) {
            throw new PersistenceException(errorsReport.buildHTMLMessages());
        }
        return dbVariables;
    }

    private void buildMetadata(Date datePrelevement, String codeProjet, String codePlateforme, String codeSite, String codeOutilsPrelevement, List<LineRecord> metadatasLines, VersionFile versionFile, ErrorsReport errorsReport, List<String> listErrors)
            throws PersistenceException, InsertionDatabaseException {

        LineRecord firstLine = metadatasLines.get(0);

        MetadataZooplancton metadataZooplancton = metadataZooplanctonDAO.getByNKey(datePrelevement, codeProjet, codePlateforme, codeOutilsPrelevement);

        if (metadataZooplancton == null) {

            Projet projet = lookupProjet(codeProjet, errorsReport);
            OutilsMesure outilsMesure = lookupOutilMesure(errorsReport, firstLine);
            OutilsMesure outilsPrelevement = lookupOutilPrelevement(errorsReport, firstLine);
            Plateforme plateforme = lookupPlateforme(codePlateforme, codeSite, errorsReport);

            metadataZooplancton = new MetadataZooplancton();
            metadataZooplancton.setProjet(projet);
            metadataZooplancton.setDate(datePrelevement);
            metadataZooplancton.setNomDeterminateur(firstLine.getDeterminateur());
            metadataZooplancton.setVolumeSedimente(firstLine.getVolumeSedimente());
            metadataZooplancton.setOutilsPrelevement(outilsPrelevement);
            metadataZooplancton.setOutilsMesure(outilsMesure);
            metadataZooplancton.setVersionFile(versionFile);
            metadataZooplancton.setPlateforme(plateforme);
            metadataZooplancton.setProfondeurMax(firstLine.getProfondeurMax());
            metadataZooplancton.setProfondeurMin(firstLine.getProfondeurMin());

        }

        for (LineRecord metadataLine : metadatasLines) {
            for (VariableValue variableValue : metadataLine.getVariablesValues()) {

                Taxon taxon = lookupTaxon(errorsReport, metadataLine);
                StadeDeveloppement stadeDeveloppement = lookupStadeDeveloppement(errorsReport, metadataLine);

                DataZooplancton dataZooplancton = new DataZooplancton();
                VariableGLACPE variable = (VariableGLACPE) variableDAO.merge(variableValue.getVariableGLACPE());

                dataZooplancton.setVariable(variable);
                dataZooplancton.setTaxon(taxon);
                dataZooplancton.setStadeDeveloppement(stadeDeveloppement);

                if (variableValue.getValue() == null || variableValue.getValue().length() == 0) {
                    dataZooplancton.setValue(null);
                } else {
                    Float value = Float.parseFloat(variableValue.getValue());
                    dataZooplancton.setValue(value);
                }

                dataZooplancton.setMetadataZooplancton(metadataZooplancton);
                metadataZooplancton.getDatas().add(dataZooplancton);

            }
        }

        metadataZooplanctonDAO.saveOrUpdate(metadataZooplancton);
    }

    private OutilsMesure lookupOutilPrelevement(ErrorsReport errorsReport, LineRecord firstLine) throws PersistenceException {
        String codeOutilsPrelevement = firstLine.getOutilsPrelevementCode();
        OutilsMesure outilsPrelevement = null;
        if (codeOutilsPrelevement != null && !codeOutilsPrelevement.equals(STRING_EMPTY)) {
            outilsPrelevement = outilsMesureDAO.getByCode(firstLine.getOutilsPrelevementCode());
            if (outilsPrelevement == null) {
                errorsReport.addException(new PersistenceException(String.format(getGLACPEMessageWithBundle(PATH_BUNDLE_SOURCE, PROPERTY_MSG_MISSING_OUTILS_PRELEVEMENT_IN_REFERENCES_DATAS), codeOutilsPrelevement)));
                throw new PersistenceException(String.format(getGLACPEMessageWithBundle(PATH_BUNDLE_SOURCE, PROPERTY_MSG_MISSING_OUTILS_PRELEVEMENT_IN_REFERENCES_DATAS), codeOutilsPrelevement));
            }
        }
        return outilsPrelevement;
    }

    private OutilsMesure lookupOutilMesure(ErrorsReport errorsReport, LineRecord firstLine) throws PersistenceException {
        OutilsMesure outilsMesure = null;
        String codeOutilMesure = firstLine.getOutilsMesureCode();
        if (codeOutilMesure != null && !codeOutilMesure.equals(STRING_EMPTY)) {
            outilsMesure = outilsMesureDAO.getByCode(codeOutilMesure);
            if (outilsMesure == null) {
                errorsReport.addException(new PersistenceException(String.format(getGLACPEMessageWithBundle(PATH_BUNDLE_SOURCE, PROPERTY_MSG_MISSING_OUTILS_MESURE_IN_REFERENCES_DATAS), codeOutilMesure)));
                throw new PersistenceException(String.format(getGLACPEMessageWithBundle(PATH_BUNDLE_SOURCE, PROPERTY_MSG_MISSING_OUTILS_MESURE_IN_REFERENCES_DATAS), codeOutilMesure));
            }
        }
        return outilsMesure;
    }

    private Projet lookupProjet(String codeProjet, ErrorsReport errorsReport) throws PersistenceException {
        Projet projet = projetDAO.getByCode(codeProjet);
        if (projet == null) {
            errorsReport.addException(new PersistenceException(String.format(getGLACPEMessageWithBundle(PATH_BUNDLE_SOURCE, PROPERTY_MSG_MISSING_PROJET_SITE_IN_REFERENCES_DATAS), codeProjet)));
            throw new PersistenceException(String.format(getGLACPEMessageWithBundle(PATH_BUNDLE_SOURCE, PROPERTY_MSG_MISSING_PROJET_SITE_IN_REFERENCES_DATAS), codeProjet));
        }
        return projet;
    }

    private Plateforme lookupPlateforme(String codePlateforme, String codeSite, ErrorsReport errorsReport) throws PersistenceException {
        Plateforme plateforme = plateformeDAO.getByNKey(codePlateforme, codeSite);
        if (plateforme == null) {
            errorsReport.addException(new PersistenceException(String.format(getGLACPEMessageWithBundle(PATH_BUNDLE_SOURCE, PROPERTY_MSG_MISSING_PLATEFORME_IN_REFERENCES_DATAS), codePlateforme)));
            throw new PersistenceException(String.format(getGLACPEMessageWithBundle(PATH_BUNDLE_SOURCE, PROPERTY_MSG_MISSING_PLATEFORME_IN_REFERENCES_DATAS), codePlateforme));
        }
        return plateforme;
    }

    private StadeDeveloppement lookupStadeDeveloppement(ErrorsReport errorsReport, LineRecord metadataLine) throws PersistenceException {
        String codeStadeDeveloppement = metadataLine.getStadeDeveloppement();
        StadeDeveloppement stadeDeveloppement = stadeDeveloppementDAO.getByCode(codeStadeDeveloppement);
        if (stadeDeveloppement == null) {
            errorsReport.addException(new PersistenceException(String.format(getGLACPEMessageWithBundle(PATH_BUNDLE_SOURCE, PROPERTY_MSG_MISSING_STADES_DEVELOPPEMENT_IN_REFERENCES_DATAS), codeStadeDeveloppement)));
            throw new PersistenceException(String.format(getGLACPEMessageWithBundle(PATH_BUNDLE_SOURCE, PROPERTY_MSG_MISSING_STADES_DEVELOPPEMENT_IN_REFERENCES_DATAS), codeStadeDeveloppement));
        }
        return stadeDeveloppement;
    }

    private Taxon lookupTaxon(ErrorsReport errorsReport, LineRecord metadataLine) throws PersistenceException {
        String codeTaxon = metadataLine.getTaxonCode();
        Taxon taxon = taxonDAO.getByCode(codeTaxon);
        if (taxon == null) {
            errorsReport.addException(new PersistenceException(String.format(getGLACPEMessageWithBundle(PATH_BUNDLE_SOURCE, PROPERTY_MSG_MISSING_TAXON_IN_REFERENCES_DATAS), codeTaxon)));
            throw new PersistenceException(String.format(getGLACPEMessageWithBundle(PATH_BUNDLE_SOURCE, PROPERTY_MSG_MISSING_TAXON_IN_REFERENCES_DATAS), codeTaxon));
        }
        return taxon;
    }

    /**
     *
     * @param outilsMesureDAO
     */
    public void setOutilsMesureDAO(IOutilsMesureDAO outilsMesureDAO) {
        this.outilsMesureDAO = outilsMesureDAO;
    }

    /**
     *
     * @param controleCoherenceDAO
     */
    public void setControleCoherenceDAO(IControleCoherenceDAO controleCoherenceDAO) {
        this.controleCoherenceDAO = controleCoherenceDAO;
    }

    /**
     *
     * @param plateformeDAO
     */
    public void setPlateformeDAO(IPlateformeDAO plateformeDAO) {
        this.plateformeDAO = plateformeDAO;
    }

    /**
     *
     * @param taxonDAO
     */
    public void setTaxonDAO(ITaxonDAO taxonDAO) {
        this.taxonDAO = taxonDAO;
    }

    /**
     *
     * @param metadataZooplanctonDAO
     */
    public void setMetadataZooplanctonDAO(IMetadataZooplanctonDAO metadataZooplanctonDAO) {
        this.metadataZooplanctonDAO = metadataZooplanctonDAO;
    }

    /**
     *
     * @param projetDAO
     */
    public void setProjetDAO(IProjetDAO projetDAO) {
        this.projetDAO = projetDAO;
    }

}
