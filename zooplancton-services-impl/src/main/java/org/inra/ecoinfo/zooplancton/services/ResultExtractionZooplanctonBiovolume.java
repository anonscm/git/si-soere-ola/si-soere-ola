package org.inra.ecoinfo.zooplancton.services;

import java.util.Date;

/**
 *
 * @author ptcherniati
 */
public class ResultExtractionZooplanctonBiovolume {

    private String projet;
    private String site;
    private String plateforme;
    private Date date;
    private String outilPrelevement;
    private String outilMesure;
    private Float profondeurMin;
    private Float profondeurMax;
    private String nomDeterminateur;
    private Float volumeSedimente;

    /**
     *
     * @param projet
     * @param site
     * @param plateforme
     * @param date
     * @param outilPrelevement
     * @param outilMesure
     * @param profondeurMin
     * @param profondeurMax
     * @param nomDeterminateur
     * @param volumeSedimente
     */
    public ResultExtractionZooplanctonBiovolume(String projet, String site, String plateforme, Date date, String outilPrelevement, String outilMesure, Float profondeurMin, Float profondeurMax, String nomDeterminateur, Float volumeSedimente) {
        super();
        this.projet = projet;
        this.site = site;
        this.plateforme = plateforme;
        this.date = date;
        this.outilPrelevement = outilPrelevement;
        this.outilMesure = outilMesure;
        this.profondeurMin = profondeurMin;
        this.profondeurMax = profondeurMax;
        this.nomDeterminateur = nomDeterminateur;
        this.volumeSedimente = volumeSedimente;

    }

    /**
     *
     * @return
     */
    public String getProjet() {
        return projet;
    }

    /**
     *
     * @return
     */
    public String getPlateforme() {
        return plateforme;
    }

    /**
     *
     * @return
     */
    public Date getDate() {
        return date;
    }

    /**
     *
     * @param projet
     */
    public void setProjet(String projet) {
        this.projet = projet;
    }

    /**
     *
     * @param plateforme
     */
    public void setPlateforme(String plateforme) {
        this.plateforme = plateforme;
    }

    /**
     *
     * @param date
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     *
     * @return
     */
    public String getSite() {
        return site;
    }

    /**
     *
     * @return
     */
    public String getOutilPrelevement() {
        return outilPrelevement;
    }

    /**
     *
     * @return
     */
    public Float getProfondeurMin() {
        return profondeurMin;
    }

    /**
     *
     * @return
     */
    public Float getProfondeurMax() {
        return profondeurMax;
    }

    /**
     *
     * @return
     */
    public String getNomDeterminateur() {
        return nomDeterminateur;
    }

    /**
     *
     * @return
     */
    public Float getVolumeSedimente() {
        return volumeSedimente;
    }

    /**
     *
     * @param site
     */
    public void setSite(String site) {
        this.site = site;
    }

    /**
     *
     * @param outilPrelevement
     */
    public void setOutilPrelevement(String outilPrelevement) {
        this.outilPrelevement = outilPrelevement;
    }

    /**
     *
     * @param profondeurMin
     */
    public void setProfondeurMin(Float profondeurMin) {
        this.profondeurMin = profondeurMin;
    }

    /**
     *
     * @param profondeurMax
     */
    public void setProfondeurMax(Float profondeurMax) {
        this.profondeurMax = profondeurMax;
    }

    /**
     *
     * @param nomDeterminateur
     */
    public void setNomDeterminateur(String nomDeterminateur) {
        this.nomDeterminateur = nomDeterminateur;
    }

    /**
     *
     * @param volumeSedimente
     */
    public void setVolumeSedimente(Float volumeSedimente) {
        this.volumeSedimente = volumeSedimente;
    }

    /**
     *
     * @return
     */
    public String getOutilMesure() {
        return outilMesure;
    }

    /**
     *
     * @param outilMesure
     */
    public void setOutilMesure(String outilMesure) {
        this.outilMesure = outilMesure;
    }

}
