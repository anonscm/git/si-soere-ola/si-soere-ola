package org.inra.ecoinfo.zooplancton.services.extraction;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.AbstractExtractor;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.NotYetImplementedException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.inra.ecoinfo.zooplancton.services.IZooplanctonDAO;
import org.inra.ecoinfo.zooplancton.services.ResultExtractionZooplanctonBiovolume;

/**
 *
 * @author ptcherniati
 */
public class ExtractorZooBiovolume extends AbstractExtractor {

    /**
     *
     */
    public static String CST_RESULT_EXTRACTION_CODE = "zooplanctonBiovolume";

    /**
     *
     */
    protected static final String MAP_INDEX_0 = "0";

    /**
     *
     */
    protected static final String PROPERTY_MSG_NO_EXTRACTION_RESULT = "MSG_NO_EXTRACTION_RESULT_BIOVOLUMES";
    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.zooplancton.services.messages";

    /**
     *
     */
    protected IZooplanctonDAO zooplanctonDAO;

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, List> extractDatas(Map<String, Object> requestMetadatas) throws NoExtractionResultException, BusinessException {

        Map<String, List> extractedDatasMap = new HashMap<String, List>();
        List<PlateformeVO> selectedPlateformes = (List<PlateformeVO>) requestMetadatas.get(ZooplanctonParameters.KEY_MAP_SELECTION_PLATEFORMES);
        List<Long> plateformesIds = buildIdsPlateformes(selectedPlateformes);
        Long projetId = (Long) requestMetadatas.get(ZooplanctonParameters.KEY_MAP_SELECTION_ID_PROJECT);

        DatesRequestParamVO datesRequestParamVO = (DatesRequestParamVO) requestMetadatas.get(ZooplanctonParameters.KEY_MAP_SELECTION_DATE);
        List<ResultExtractionZooplanctonBiovolume> results = null;

        try {

            results = (List<ResultExtractionZooplanctonBiovolume>) zooplanctonDAO.extractDatasBiovolume(plateformesIds, projetId, datesRequestParamVO);

            if (results == null || results.size() == 0) {
                throw new NoExtractionResultException(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_NO_EXTRACTION_RESULT));
            }

            extractedDatasMap.put(MAP_INDEX_0, results);

        } catch (PersistenceException e) {
            AbstractExtractor.LOGGER.error(e.getMessage());
            throw new BusinessException(e);
        }
        return extractedDatasMap;
    }

    private List<Long> buildIdsPlateformes(List<PlateformeVO> selectedPlateformes) {
        List<Long> plateformesIds = new LinkedList<Long>();
        for (PlateformeVO plateforme : selectedPlateformes) {
            plateformesIds.add(plateforme.getId());
        }
        return plateformesIds;
    }

    /**
     *
     * @param selectedPlateformes
     * @return
     */
    protected Set<String> retrieveSitesCodes(List<PlateformeVO> selectedPlateformes) {
        Set<String> sitesNames = new HashSet<String>();
        for (PlateformeVO plateforme : selectedPlateformes) {
            sitesNames.add(Utils.createCodeFromString(plateforme.getNomSite()));
        }
        return sitesNames;
    }

    /**
     * @param resultsDatasMap
     *            the map with the extraction results Filter datas according to the user's permissions.
     * @return 
     */
    @SuppressWarnings("rawtypes")
    protected Map<String, List> filterExtractedDatas(Map<String, List> resultsDatasMap) throws BusinessException {
        filterExtractedDatas(resultsDatasMap, MAP_INDEX_0);
        return resultsDatasMap;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public void extract(IParameter parameters) throws BusinessException {
        Map<String, List> resultsDatasMap = extractDatas(parameters.getParameters());
        // Map<String, List> filteredResultsDatasMap = filterExtractedDatas(resultsDatasMap);
        ((ZooplanctonParameters) parameters).getResults().put(CST_RESULT_EXTRACTION_CODE, resultsDatasMap);
    }

    /**
     *
     * @param zooplanctonDAO
     */
    public void setZooplanctonDAO(IZooplanctonDAO zooplanctonDAO) {
        this.zooplanctonDAO = zooplanctonDAO;
    }

    @Override
    protected void prepareRequestMetadatas(Map<String, Object> requestMetadatasMap) throws BusinessException {
        throw new NotYetImplementedException();

    }

}
