package org.inra.ecoinfo.zooplancton.services.extraction;

import java.util.Objects;

class KeyMapExtractionL3Zooplancton implements Comparable<KeyMapExtractionL3Zooplancton> {

    private final String nomStadeDeveloppement;
    private final String nomLatin;

    public KeyMapExtractionL3Zooplancton(String nomStadeDeveloppement, String nomLatin) {
        super();
        this.nomStadeDeveloppement = nomStadeDeveloppement;
        this.nomLatin = nomLatin;
    }

    public String getNomLatinTaxon() {
        return nomLatin;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 23 * hash + Objects.hashCode(this.nomStadeDeveloppement);
        hash = 23 * hash + Objects.hashCode(this.nomLatin);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final KeyMapExtractionL3Zooplancton other = (KeyMapExtractionL3Zooplancton) obj;
        if (!Objects.equals(this.nomStadeDeveloppement, other.nomStadeDeveloppement)) {
            return false;
        }
        if (!Objects.equals(this.nomLatin, other.nomLatin)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.nomStadeDeveloppement;
    }

    @Override
    public int compareTo(KeyMapExtractionL3Zooplancton keyMapToCompare) {
        return toString().compareTo(keyMapToCompare.toString());
    }

}
