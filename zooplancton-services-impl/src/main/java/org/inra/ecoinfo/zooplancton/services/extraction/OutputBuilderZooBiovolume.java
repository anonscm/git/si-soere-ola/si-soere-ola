package org.inra.ecoinfo.zooplancton.services.extraction;

import java.io.File;
import java.io.PrintStream;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.IDatatypeVariableUniteGLACPEDAO;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.OutilsMesure;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.zooplancton.services.IZooplanctonDAO;
import org.inra.ecoinfo.zooplancton.services.ResultExtractionZooplanctonBiovolume;

import com.google.common.base.Strings;

/**
 *
 * @author ptcherniati
 */
public class OutputBuilderZooBiovolume extends AbstractOutputBuilder {

    private static final String PROPERTY_MSG_HEADER_BIOVOLUME = "MSG_HEADER_BIOVOLUME";

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH_ZOOPLANKTON = "org.inra.ecoinfo.zooplancton.services.messages";
    private String SUFFIX_FILENAME = "biovolumes";

    /**
     *
     */
    protected IZooplanctonDAO zooplanctonDAO;

    /**
     *
     */
    protected IVariableDAO variableDAO;

    /**
     *
     */
    protected IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO;

    /**
     *
     * @param variableDAO
     */
    public void setVariableDAO(IVariableDAO variableDAO) {
        this.variableDAO = variableDAO;
    }

    /**
     *
     * @param selectedPlateformes
     * @return
     */
    protected Set<String> retrieveSitesCodes(List<PlateformeVO> selectedPlateformes) {
        Set<String> sitesNames = new HashSet<String>();
        for (PlateformeVO plateforme : selectedPlateformes) {
            sitesNames.add(Utils.createCodeFromString(plateforme.getNomSite()));
        }
        return sitesNames;
    }

    /**
     *
     * @param datatypeVariableUniteDAO
     */
    public void setDatatypeVariableUniteDAO(IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO) {
        this.datatypeVariableUniteDAO = datatypeVariableUniteDAO;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public Map<String, File> buildOutput(final IParameter parameters, final String cstResultExtractionCode) throws BusinessException, NoExtractionResultException {

        final Map<String, List> results = parameters.getResults().get(cstResultExtractionCode);
        if (results == null || (results != null && results.get(AbstractOutputBuilder.MAP_INDEX_0).isEmpty())) {
            throw new NoExtractionResultException(localizationManager.getMessage(NoExtractionResultException.BUNDLE_SOURCE_PATH, NoExtractionResultException.ERROR));
        }
        final String resultsHeader = buildHeader(parameters.getParameters());
        return buildBody(resultsHeader, results, parameters.getParameters());
    }

    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException, NoExtractionResultException {
        ((DefaultParameter) parameters).getFilesMaps().add(buildOutput(parameters, ExtractorZooBiovolume.CST_RESULT_EXTRACTION_CODE));
 
        return null;
    }

    protected String buildHeader(Map<String, Object> requestMetadatas) throws BusinessException {

        StringBuilder stringBuilder = new StringBuilder();

        try {
            stringBuilder.append(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_ZOOPLANKTON, PROPERTY_MSG_HEADER_BIOVOLUME));
        } catch (Exception e) {
            throw new BusinessException(e);
        }
        return stringBuilder.toString();
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {
        // List<VariableVO> selectedVariables = (List<VariableVO>) requestMetadatasMap.get(ZooplanctonParameters.KEY_MAP_SELECTION_VARIABLES);

        List<ResultExtractionZooplanctonBiovolume> resultsExtraction = resultsDatasMap.get(MAP_INDEX_0);

        Properties propertiesNameProjet = localizationManager.newProperties(Projet.NAME_TABLE, "nom");
        Properties propertiesNamePlateforme = localizationManager.newProperties(Plateforme.TABLE_NAME, "nom");
        Properties propertiesNameSite = localizationManager.newProperties(Site.NAME_ENTITY_JPA, "nom");
        Properties propertiesNameOutil = localizationManager.newProperties(OutilsMesure.TABLE_NAME, "nom");

        List<PlateformeVO> selectedPlateformes = (List<PlateformeVO>) requestMetadatasMap.get(ZooplanctonParameters.KEY_MAP_SELECTION_PLATEFORMES);
        Set<String> sitesNames = retrieveSitesCodes(selectedPlateformes);

        // Map<Date, Map<String, Map<Long, List<MesurePhytoplancton>>>> sumBiovolumeMap = new HashMap<Date, Map<String, Map<Long, List<MesurePhytoplancton>>>>();
        Map<String, File> filesMap = buildOutputsFiles(sitesNames, SUFFIX_FILENAME);

        Map<String, PrintStream> outputPrintStreamMap = buildOutputPrintStreamMap(filesMap);
        for (String siteName : outputPrintStreamMap.keySet()) {
            outputPrintStreamMap.get(siteName).println(headers);
        }

        for (ResultExtractionZooplanctonBiovolume result : resultsExtraction) {
            String localizedNameProjet = lookupLocalizedProperty(propertiesNameProjet, result.getProjet());
            String localizedNameSite = lookupLocalizedProperty(propertiesNameSite, result.getSite());
            String localizedNamePlateforme = lookupLocalizedProperty(propertiesNamePlateforme, result.getPlateforme());
            String localizedNameOutilPrelevement = lookupLocalizedProperty(propertiesNameOutil, result.getOutilPrelevement());
            String localizedNameOutilMesure = lookupLocalizedProperty(propertiesNameOutil, result.getOutilMesure());

            Date date = result.getDate();
            String line = String.format("%s;%s;%s;%s;%s;%s;%s;%s;%s;%s", localizedNameProjet, localizedNameSite, localizedNamePlateforme, DateUtil.getSimpleDateFormatDateLocale().format(date), localizedNameOutilPrelevement, localizedNameOutilMesure,
                    result.getProfondeurMin(), result.getProfondeurMax(), result.getNomDeterminateur(), result.getVolumeSedimente()==null?"":result.getVolumeSedimente());
            outputPrintStreamMap.get(Utils.createCodeFromString(result.getSite())).println(line);

        }

        closeStreams(outputPrintStreamMap);
        return filesMap;
    }

    private String lookupLocalizedProperty(Properties properties, String defaultValue) {
        String localizedName = properties.getProperty(defaultValue);
        if (Strings.isNullOrEmpty(localizedName)) {
            return defaultValue;
        } else {
            return localizedName;
        }
    }

    /**
     *
     * @param zooplanctonDAO
     */
    public void setZooplanctonDAO(IZooplanctonDAO zooplanctonDAO) {
        this.zooplanctonDAO = zooplanctonDAO;
    }

}
