package org.inra.ecoinfo.zooplancton.services.extraction;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.AbstractExtractor;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO;
import org.inra.ecoinfo.glacpe.extraction.vo.VariableVO;
import org.inra.ecoinfo.glacpe.refdata.taxon.Taxon;
import org.inra.ecoinfo.security.entity.ISecurityPathWithVariable;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.inra.ecoinfo.zooplancton.services.IZooplanctonDAO;
import org.inra.ecoinfo.zooplancton.services.ResultExtractionZooplanctonRawsDatas;

import com.google.common.collect.Lists;

/**
 *
 * @author Antoine Schellenberger
 */
public class ZooplanctonRawsDatasExtractor extends AbstractExtractor {

    /**
     *
     */
    public static String CST_RESULT_EXTRACTION_CODE = "zooplanctonRawDatas";

    /**
     *
     */
    protected static final String MAP_INDEX_0 = "0";

    /**
     *
     */
    protected static final String PROPERTY_MSG_NO_EXTRACTION_RESULT = "MSG_NO_EXTRACTION_RESULT_RAW_DATAS";
    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.zooplancton.services.messages";

    /**
     *
     */
    protected IZooplanctonDAO zooplanctonDAO;

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, List> extractDatas(Map<String, Object> requestMetadatas) throws NoExtractionResultException, BusinessException {

        Map<String, List> extractedDatasMap = new HashMap<String, List>();
        List<PlateformeVO> selectedPlateformes = (List<PlateformeVO>) requestMetadatas.get(ZooplanctonParameters.KEY_MAP_SELECTION_PLATEFORMES);
        List<Long> plateformesIds = buildIdsPlateformes(selectedPlateformes);
        List<Long> idsVariables = buildIdsVariables((List<VariableVO>) requestMetadatas.get(ZooplanctonParameters.KEY_MAP_SELECTION_VARIABLES));
        Long projetId = (Long) requestMetadatas.get(ZooplanctonParameters.KEY_MAP_SELECTION_ID_PROJECT);
        List<Taxon> selectedTaxons = (List<Taxon>) requestMetadatas.get(ZooplanctonParameters.KEY_MAP_SELECTION_TAXONS);
        List<Long> idsTaxonsLeaves = buildIdsTaxons(selectedTaxons);

        DatesRequestParamVO datesRequestParamVO = (DatesRequestParamVO) requestMetadatas.get(ZooplanctonParameters.KEY_MAP_SELECTION_DATE);
        List<ResultExtractionZooplanctonRawsDatas> results = null;

        try {

            results = (List<ResultExtractionZooplanctonRawsDatas>) zooplanctonDAO.extractDatas(plateformesIds, projetId, datesRequestParamVO, idsTaxonsLeaves, idsVariables);

            if (results == null || results.size() == 0) {
                throw new NoExtractionResultException(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_NO_EXTRACTION_RESULT));
            }

            extractedDatasMap.put(MAP_INDEX_0, results);

        } catch (PersistenceException e) {
            AbstractExtractor.LOGGER.error(e.getMessage());
            throw new BusinessException(e);
        }
        return extractedDatasMap;
    }

    private List<Long> buildIdsPlateformes(List<PlateformeVO> selectedPlateformes) {
        List<Long> plateformesIds = new LinkedList<Long>();
        for (PlateformeVO plateforme : selectedPlateformes) {
            plateformesIds.add(plateforme.getId());
        }
        return plateformesIds;
    }

    private List<Long> buildIdsTaxons(List<Taxon> selectedTaxons) {
        List<Long> taxonsIds = new LinkedList<Long>();
        for (Taxon selectedTaxon : selectedTaxons) {
            if (selectedTaxon.getTaxonsEnfants() != null && !selectedTaxon.getTaxonsEnfants().isEmpty()) {
                taxonsIds.addAll(buildIdsTaxons(selectedTaxon.getTaxonsEnfants()));
            } else {
                taxonsIds.add(selectedTaxon.getId());

            }
        }
        return taxonsIds;
    }

    private List<Long> buildIdsVariables(List<VariableVO> variablesSelected) {
        List<Long> idsVariables = Lists.newLinkedList();
        for (VariableVO variable : variablesSelected) {
            idsVariables.add(variable.getId());
        }
        return idsVariables;
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void prepareRequestMetadatas(Map<String, Object> requestMetadatasMap) throws BusinessException {
        sortSelectedVariables((List<VariableVO>) requestMetadatasMap.get(ZooplanctonParameters.KEY_MAP_SELECTION_VARIABLES));
    }

    private void sortSelectedVariables(List<VariableVO> variablesSelected) {
        Collections.sort(variablesSelected, new Comparator<VariableVO>() {

            @Override
            public int compare(VariableVO o1, VariableVO o2) {
                if (o1.getOrdreAffichageGroupe() == null) {
                    o1.setOrdreAffichageGroupe(0);
                }
                if (o2.getOrdreAffichageGroupe() == null) {
                    o2.setOrdreAffichageGroupe(0);
                }
                return o1.getOrdreAffichageGroupe().toString().concat(o1.getCode()).compareTo(o2.getOrdreAffichageGroupe().toString().concat(o2.getCode()));
            }
        });

    }

    /**
     *
     * @param selectedPlateformes
     * @return
     */
    protected Set<String> retrieveSitesCodes(List<PlateformeVO> selectedPlateformes) {
        Set<String> sitesNames = new HashSet<String>();
        for (PlateformeVO plateforme : selectedPlateformes) {
            sitesNames.add(Utils.createCodeFromString(plateforme.getNomSite()));
        }
        return sitesNames;
    }

    /**
     * @param resultsDatasMap
     *            the map with the extraction results Filter datas according to the user's permissions.
     * @return 
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    protected Map<String, List> filterExtractedDatas(Map<String, List> resultsDatasMap) throws BusinessException {
        if (securityContext.getUtilisateur().getIsRoot()) {
            return resultsDatasMap;
        }
        List<ResultExtractionZooplanctonRawsDatas> results = resultsDatasMap.get(MAP_INDEX_0);
        List<ISecurityPathWithVariable> resultsFiltered = Lists.newLinkedList();
        for (ResultExtractionZooplanctonRawsDatas result : results) {
            filterSecurityPath(resultsFiltered, result);
        }
        resultsDatasMap.put(MAP_INDEX_0, resultsFiltered);
        return resultsDatasMap;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public void extract(IParameter parameters) throws BusinessException {
        prepareRequestMetadatas(parameters.getParameters());
        Map<String, List> resultsDatasMap = extractDatas(parameters.getParameters());
        Map<String, List> filteredResultsDatasMap = filterExtractedDatas(resultsDatasMap);
        ((ZooplanctonParameters) parameters).getResults().put(CST_RESULT_EXTRACTION_CODE, filteredResultsDatasMap);
    }

    /**
     *
     * @param zooplanctonDAO
     */
    public void setZooplanctonDAO(IZooplanctonDAO zooplanctonDAO) {
        this.zooplanctonDAO = zooplanctonDAO;
    }

}
