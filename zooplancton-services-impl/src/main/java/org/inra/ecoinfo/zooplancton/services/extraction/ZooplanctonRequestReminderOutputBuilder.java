package org.inra.ecoinfo.zooplancton.services.extraction;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.glacpe.dataset.phytoplancton.IPhytoplanctonDAO;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO;
import org.inra.ecoinfo.glacpe.extraction.vo.VariableVO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.taxon.Taxon;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public class ZooplanctonRequestReminderOutputBuilder extends AbstractOutputBuilder {

    private static final String DATATYPE_ZOOPLANKTON = "Zooplancton";

    /**
     *
     */
    public static String CST_RESULT_EXTRACTION_CODE = "ZooplanctonRequestReminder";

    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.zooplancton.services.messages";

    private static final String PROPERTY_MSG_EXTRACTION_COMMENTS = "MSG_EXTRACTION_COMMENTS";

    private static final String PROPERTY_MSG_HEADER = "MSG_HEADER";

    private static final String PATTERN_STRING_PLATEFORMS_SUMMARY = "   %s (%s: %s)";
    private static final String PATTERN_STRING_VARIABLES_SUMMARY = "   %s";
    private static final String PATTERN_STRING_COMMENTS_SUMMARY = "   %s";

    private static final String PROPERTY_MSG_SELECTED_PERIODS = "MSG_SELECTED_PERIODS";
    private static final String PROPERTY_MSG_SELECTED_VARIABLES = "MSG_SELECTED_VARIABLES";
    private static final String PROPERTY_MSG_SELECTED_TAXONS = "MSG_SELECTED_TAXONS";
    private static final String PROPERTY_MSG_SITE = "MSG_SITE";
    private static final String PROPERTY_MSG_SELECTED_PLATEFORMS = "MSG_SELECTED_PLATEFORMS";
    private static final String PROPERTY_MSG_SELECTED_BIOVOLUME = "MSG_SELECTED_BIOVOLUME";
    private static final String PROPERTY_MSG_DETAILS_DEVELOPMENT_STAGE = "MSG_DETAILS_DEVELOPMENT_STAGE";
    private static final String PROPERTY_MSG_DETAILS_TAXA = "MSG_DETAILS_TAXA";

    private static final String KEYMAP_COMMENTS = "comments";

    /**
     *
     */
    protected IPhytoplanctonDAO phytoplanctonDAO;

    @Override
    protected String buildHeader(Map<String, Object> requestMetadatas) throws BusinessException {
        return getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_HEADER);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {

        Map<String, File> reminderMap = new HashMap<String, File>();
        File reminderFile = buildOutputFile(FILENAME_REMINDER, EXTENSION_TXT);
        PrintStream reminderPrintStream;

        try {

            reminderPrintStream = new PrintStream(reminderFile, CHARACTER_ENCODING_ISO_8859_1);
            reminderPrintStream.println(headers);
            reminderPrintStream.println();

            printPlateformesSummary((List<PlateformeVO>) requestMetadatasMap.get(ZooplanctonParameters.KEY_MAP_SELECTION_PLATEFORMES), reminderPrintStream);

            printProjectSummary((Projet) requestMetadatasMap.get(ZooplanctonParameters.KEY_MAP_SELECTION_PROJECT), reminderPrintStream);
            printVariablesSummary((List<VariableVO>) requestMetadatasMap.get(ZooplanctonParameters.KEY_MAP_SELECTION_VARIABLES), (Boolean) requestMetadatasMap.get(ZooplanctonParameters.KEY_MAP_SELECTION_BIOVOLUME), reminderPrintStream);
            printTaxonsSummary((List<Taxon>) requestMetadatasMap.get(ZooplanctonParameters.KEY_MAP_SELECTION_TAXONS), reminderPrintStream);
            printDatesSummary((DatesRequestParamVO) requestMetadatasMap.get(ZooplanctonParameters.KEY_MAP_SELECTION_DATE), reminderPrintStream);
            printCheckBoxesDetails((Boolean) requestMetadatasMap.get(ZooplanctonParameters.KEY_MAP_DETAIL_DEVELOPMENT_STAGE), (Boolean) requestMetadatasMap.get(ZooplanctonParameters.KEY_MAP_DETAIL_TAXON), reminderPrintStream);
            reminderPrintStream.println(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_EXTRACTION_COMMENTS));
            reminderPrintStream.println(String.format(PATTERN_STRING_COMMENTS_SUMMARY, requestMetadatasMap.get(KEYMAP_COMMENTS)));

        } catch (FileNotFoundException e) {
            AbstractOutputBuilder.LOGGER.error(e);
            throw new BusinessException(e);
        } catch (UnsupportedEncodingException e) {
            AbstractOutputBuilder.LOGGER.error(e);
            throw new BusinessException(e);
        }
        reminderMap.put(FILENAME_REMINDER, reminderFile);

        reminderPrintStream.flush();
        reminderPrintStream.close();

        return reminderMap;
    }

    private void printDatesSummary(DatesRequestParamVO datesRequestParamVO, PrintStream reminderPrintStream) throws BusinessException {
        reminderPrintStream.println(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_SELECTED_PERIODS));
        try {
            datesRequestParamVO.getCurrentDatesFormParam().buildSummary(reminderPrintStream);
        } catch (ParseException e) {
            AbstractOutputBuilder.LOGGER.error(e);
            throw new BusinessException(e);
        }

        reminderPrintStream.println();
    }

    private void printPlateformesSummary(List<PlateformeVO> list, PrintStream reminderPrintStream) {

        Properties propertiesNomPlateforme = getLocalizationManager().newProperties(Plateforme.TABLE_NAME, "nom");
        Properties propertiesNomSite = getLocalizationManager().newProperties(Site.NAME_ENTITY_JPA, "nom");
        reminderPrintStream.println(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_SELECTED_PLATEFORMS));
        for (PlateformeVO plateforme : list) {
            reminderPrintStream.println(String.format(PATTERN_STRING_PLATEFORMS_SUMMARY, propertiesNomPlateforme.getProperty(plateforme.getNom(), plateforme.getNom()), getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_SITE),
                    propertiesNomSite.getProperty(plateforme.getNomSite(), plateforme.getNomSite())));
        }
        reminderPrintStream.println();
    }

    private void printProjectSummary(Projet project, PrintStream reminderPrintStream) {

        Properties propertiesNomProject = getLocalizationManager().newProperties(Plateforme.TABLE_NAME, "nom");
        reminderPrintStream.println(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_SELECTED_PLATEFORMS));
        reminderPrintStream.println(propertiesNomProject.getProperty(project.getNom(), project.getNom()));

        reminderPrintStream.println();
    }

    private void printCheckBoxesDetails(Boolean cbxDetailDevelopmentStage, Boolean cbxDetailTaxon, PrintStream reminderPrintStream) {
        if (cbxDetailDevelopmentStage) {
            reminderPrintStream.println(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_DETAILS_DEVELOPMENT_STAGE));
        }
        if (cbxDetailTaxon) {
            reminderPrintStream.println(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_DETAILS_TAXA));
        }

        reminderPrintStream.println();
    }

    private void printVariablesSummary(List<VariableVO> list, Boolean selectionBiovolume, PrintStream reminderPrintStream) {
        Properties propertiesNomVariable = getLocalizationManager().newProperties(Variable.NAME_ENTITY_JPA, "nom");
        Properties propertiesNomDatatype = getLocalizationManager().newProperties(DataType.NAME_ENTITY_JPA, "name");
        reminderPrintStream.println(String.format(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_SELECTED_VARIABLES), propertiesNomDatatype.getProperty(DATATYPE_ZOOPLANKTON, DATATYPE_ZOOPLANKTON)));
        if (selectionBiovolume) {
            reminderPrintStream.println(String.format(PATTERN_STRING_VARIABLES_SUMMARY, getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_SELECTED_BIOVOLUME)));
        }
        for (VariableVO variable : list) {
            reminderPrintStream.println(String.format(PATTERN_STRING_VARIABLES_SUMMARY, propertiesNomVariable.getProperty(variable.getNom(), variable.getNom())));
        }
        reminderPrintStream.println();
    }

    private void printTaxonsSummary(List<Taxon> list, PrintStream reminderPrintStream) {
        reminderPrintStream.println(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_SELECTED_TAXONS));
        for (Taxon taxon : list) {
            reminderPrintStream.println(String.format(PATTERN_STRING_VARIABLES_SUMMARY, taxon.getNomLatin()));
        }
        reminderPrintStream.println();
    }

    /**
     *
     * @param phytoplanctonDAO
     */
    public void setPhytoplanctonDAO(IPhytoplanctonDAO phytoplanctonDAO) {
        this.phytoplanctonDAO = phytoplanctonDAO;
    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException, NoExtractionResultException {
        ((DefaultParameter) parameters).getFilesMaps().add(super.buildOutput(parameters, CST_RESULT_EXTRACTION_CODE));
        return null;
    }
}
