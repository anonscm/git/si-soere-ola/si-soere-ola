/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:53:17
 */
package org.inra.ecoinfo.zooplancton.services.stadedeveloppement;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.inra.ecoinfo.zooplancton.entity.StadeDeveloppement;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public interface IStadeDeveloppementDAO extends IDAO<StadeDeveloppement> {

    /**
     *
     * @param code
     * @return
     * @throws PersistenceException
     */
    public StadeDeveloppement getByCode(String code) throws PersistenceException;
}
