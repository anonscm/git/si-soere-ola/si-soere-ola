/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:50:44
 */
package org.inra.ecoinfo.zooplancton.services.stadedeveloppement;

import java.util.List;

import javax.persistence.Query;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.inra.ecoinfo.zooplancton.entity.StadeDeveloppement;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public class JPAStadeDeveloppementDAO extends AbstractJPADAO<StadeDeveloppement> implements IStadeDeveloppementDAO {

    /**
     *
     * @param code
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("rawtypes")
    @Override
    public StadeDeveloppement getByCode(String code) throws PersistenceException {
        try {
            String queryString = "from StadeDeveloppement s where s.code = :code";
            Query query = entityManager.createQuery(queryString);
            query.setParameter("code", code);
            List stadesDeveloppements = query.getResultList();

            StadeDeveloppement stadeDeveloppement = null;
            if (stadesDeveloppements != null && !stadesDeveloppements.isEmpty())
                stadeDeveloppement = (StadeDeveloppement) stadesDeveloppements.get(0);

            return stadeDeveloppement;
        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

}
