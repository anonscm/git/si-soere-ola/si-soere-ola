package org.inra.ecoinfo.zooplancton.dataset;

import java.io.IOException;

import org.inra.ecoinfo.ConcordionSpringJunit4ClassRunner;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.notifications.exceptions.UpdateNotificationException;
import org.inra.ecoinfo.refdata.node.ILeafTreeNode;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.inra.ecoinfo.zooplancton.ZooplanctonTransactionalTestFixtureExecutionListener;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ptcherniati
 */
@RunWith(ConcordionSpringJunit4ClassRunner.class)
@ContextConfiguration(locations = {"/META-INF/spring/applicationContextTest.xml"})
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@Transactional(rollbackFor = Exception.class)
@TestExecutionListeners(listeners = {ZooplanctonTransactionalTestFixtureExecutionListener.class})
public class LoadDatasetFixture extends org.inra.ecoinfo.dataset.LoadDatasetFixture<TreeDescriptor> {

    /**
     *
     */
    public LoadDatasetFixture() {
        super();
        MockitoAnnotations.initMocks(this);
    }

    /**
     *
     * @param treeDescriptor
     * @return
     * @throws PersistenceException
     */
    @Override
    public ILeafTreeNode getLeafNode(TreeDescriptor treeDescriptor) throws PersistenceException {
        return ZooplanctonTransactionalTestFixtureExecutionListener.projetSiteThemeDatatypeDAO.getByPathProjetSiteThemeCodeAndDatatypeCode(treeDescriptor.getProjetCode(), treeDescriptor.getPathSite(), treeDescriptor.getThemeCode(),
                treeDescriptor.getDatatypeCode());
    }

    /**
     *
     * @param projetCode
     * @param pathSite
     * @param themeCode
     * @param datatypeCode
     * @param path
     * @return
     * @throws BusinessException
     * @throws IOException
     * @throws UpdateNotificationException
     */
    public VersionFile uploadDataset(String projetCode, String pathSite, String themeCode, String datatypeCode, String path) throws BusinessException, IOException, UpdateNotificationException {
        final TreeDescriptor treeDescriptor = new TreeDescriptor(projetCode, pathSite, themeCode, datatypeCode);
        return uploadDataset(treeDescriptor, path);
    }
}
