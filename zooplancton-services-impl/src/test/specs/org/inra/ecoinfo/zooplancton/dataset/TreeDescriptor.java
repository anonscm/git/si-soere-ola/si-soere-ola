package org.inra.ecoinfo.zooplancton.dataset;

/**
 *
 * @author ptcherniati
 */
public class TreeDescriptor {

    String projetCode;
    String pathSite;
    String themeCode;
    String datatypeCode;

    /**
     *
     * @param projetCode
     * @param pathSite
     * @param themeCode
     * @param datatypeCode
     */
    public TreeDescriptor(String projetCode, String pathSite, String themeCode, String datatypeCode) {
        super();
        this.projetCode = projetCode;
        this.pathSite = pathSite;
        this.themeCode = themeCode;
        this.datatypeCode = datatypeCode;
    }

    /**
     *
     * @return
     */
    public String getDatatypeCode() {
        return datatypeCode;
    }

    /**
     *
     * @return
     */
    public String getPathSite() {
        return pathSite;
    }

    /**
     *
     * @return
     */
    public String getProjetCode() {
        return projetCode;
    }

    /**
     *
     * @return
     */
    public String getThemeCode() {
        return themeCode;
    }
}
