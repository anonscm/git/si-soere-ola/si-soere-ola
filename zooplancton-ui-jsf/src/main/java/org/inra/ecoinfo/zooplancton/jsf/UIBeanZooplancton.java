package org.inra.ecoinfo.zooplancton.jsf;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.swing.tree.TreeNode;

import org.inra.ecoinfo.dataset.security.ISecurityDatasetManager;
import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.filecomp.IFileCompManager;
import org.inra.ecoinfo.filecomp.ItemFile;
import org.inra.ecoinfo.filecomp.entity.FileComp;
import org.inra.ecoinfo.glacpe.extraction.jsf.AbstractUIBeanForSteps;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.VariableVO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.taxon.Taxon;
import org.inra.ecoinfo.glacpe.refdata.taxon.TaxonProprieteTaxon;
import org.inra.ecoinfo.glacpe.refdata.taxonniveau.TaxonNiveau;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.notifications.INotificationsManager;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.refdata.valeurqualitative.ValeurQualitative;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.security.ISecurityContext;
import org.inra.ecoinfo.security.entity.Role;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.zooplancton.services.IZooplanctonDatatypeManager;
import org.inra.ecoinfo.zooplancton.services.extraction.ZooplanctonParameters;
import org.richfaces.component.UITree;
import org.richfaces.component.UITreeNode;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.google.common.base.Strings;
import com.google.common.collect.Iterators;
import com.google.common.collect.Lists;

/**
 *
 * @author Antoine Schellenberger
 */
@ManagedBean(name = "uiZooplancton")
@ViewScoped
public class UIBeanZooplancton extends AbstractUIBeanForSteps implements Serializable {

    private static final String RULE_JSF_ZOOPLANCTON = "zooplancton";
    private static final long serialVersionUID = 1L;
    private static final String DATATYPE = "zooplancton";
    private static final String THEME = "compartiments_biologiques";
    private static final String PATH_PATTERN = "%s/%s/%s/%s/%s";

    private int affichage = 1;

    /**
     *
     */
    @ManagedProperty(value = "#{extractionManager}")
    protected IExtractionManager extractionManager;

    private Boolean filter = false;

    private Boolean genre = false;

    private Boolean selectionBiovolume = false;

    private Long idPlateformeSelected;

    private Long idProjectSelected;

    private Long idTaxonVOSelected;

    private Long idVariableSelected;

    private String lengthOrder;

    private String lengthValue;

    /**
     *
     */
    @ManagedProperty(value = "#{localizationManager}")
    protected ILocalizationManager localizationManager;

    /**
     *
     */
    @ManagedProperty(value = "#{notificationsManager}")
    protected INotificationsManager notificationsManager;

    private ParametersRequest parametersRequest = new ParametersRequest();

    private List<VOTaxonJSF> taxaPreselectedAvailables = Lists.newLinkedList();

    private Map<Long, ProjetVO> projectsAvailables = new HashMap<Long, UIBeanZooplancton.ProjetVO>();

    private Properties propertiesPlatformNames;

    private Properties propertiesProjectsNames;

    private Properties propertiesQualitativeNames;

    private Properties propertiesSiteNames;

    private Properties propertiesTaxonLevelNames;

    private Properties propertiesVariableNames;

    private List<TreeNodeTaxon> rootNodes = new ArrayList<TreeNodeTaxon>();

    /**
     *
     */
    @ManagedProperty(value = "#{securityContext}")
    protected ISecurityContext securityContext;

    private Boolean detailTaxon = false;
    private Boolean detailStadeDeveloppement = false;

    private String taxonGroupSelected;

    private Map<Long, VOTaxonJSF> taxonsAvailables = new HashMap<Long, VOTaxonJSF>();

    private String taxonSearch = "";

    private List<VOTaxonJSF> taxonsLengthAvailables = Lists.newArrayList();

    private List<VOTaxonJSF> taxonsVOAvailables = Lists.newArrayList();

    private VOTaxonJSF taxonVOSelected;

    /**
     *
     */
    @ManagedProperty(value = "#{transactionManager}")
    protected JpaTransactionManager transactionManager;

    private UITree treeListTaxon;

    private UITreeNode treeNodePreselection;

    private UITreeNode treeNodePreselectionLeaf;

    private UITreeNode treeNodeTaxon;

    private TreeNodeTaxon treeNodeTaxonSelected;

    private UITree treePreselectionListTaxons;

    private Map<Long, VOVariableJSF> variablesAvailables = new HashMap<Long, VOVariableJSF>();

    /**
     *
     */
    @ManagedProperty(value = "#{zooplanctonDatatypeManager}")
    protected IZooplanctonDatatypeManager zooplanctonDatatypeManager;

    /**
     *
     */
    @ManagedProperty(value = "#{fileCompManager}")
    protected IFileCompManager fileCompManager;

    /**
     *
     */
    public UIBeanZooplancton() {
    }

    /**
     *
     * @return @throws BusinessException
     */
    public String addAllTaxons() throws BusinessException {
        removeAllTaxons();

        for (VOTaxonJSF taxon : taxonsVOAvailables) {
//            if (taxon.getTaxon().getTaxonNiveau().getCode().equals(TaxonNiveau.VALUES_ATTR_CODE.GENRE_ESPECE)) {
            if (taxon.getTaxon().isLeaf()) {
                parametersRequest.getTaxonsSelected().put(taxon.getTaxon().getId(), taxon);
                taxon.setSelected(true);
            }
        }
        return null;
    }

    /**
     *
     * @return
     */
    public String addAllVariables() {
        Map<Long, VOVariableJSF> variablesSelected = parametersRequest.getVariablesSelected();
        variablesSelected.clear();

        for (VOVariableJSF variable : variablesAvailables.values()) {
            if (variable.getVariable().getIsDataAvailable()) {
                variablesSelected.put(variable.getVariable().getId(), variable);
                variable.setSelected(true);
            }
        }
        selectionBiovolume = true;
        return null;
    }

    /**
     *
     * @return @throws BusinessException
     */
    public String addFreeTaxon() throws BusinessException {
        for (VOTaxonJSF taxonJSF : taxonsVOAvailables) {
            idTaxonVOSelected = taxonJSF.getTaxon().getId();
            selectTaxonVO();
        }
        return null;
    }

    /**
     *
     * @return @throws BusinessException
     */
    public String addListTaxon() throws BusinessException {
        for (VOTaxonJSF taxonJSF : taxonsLengthAvailables) {
            idTaxonVOSelected = taxonJSF.getTaxon().getId();
            selectTaxonVO();
        }
        return null;
    }

    /**
     *
     * @return @throws BusinessException
     */
    public String changeProject() throws BusinessException {
        parametersRequest.getPlateformesSelected().clear();
        for (ProjetVO projectSelected : projectsAvailables.values()) {
            for (PlateformeVO plateformeSelected : projectSelected.getPlateformes().values()) {
                plateformeSelected.setSelected(false);
            }
        }

        return null;
    }

    private void createOrUpdateListVariablesAvailables() throws BusinessException, ParseException {
        variablesAvailables.clear();
        parametersRequest.getVariablesSelected().clear();

        if (!parametersRequest.plateformesSelected.isEmpty()) {

            List<VariableVO> variables = zooplanctonDatatypeManager.retrieveListsVariables(new LinkedList<Long>(parametersRequest.plateformesSelected.keySet()), parametersRequest.getFirstStartDate(), parametersRequest.getLastEndDate());
            for (PlateformeVO plateforme : parametersRequest.plateformesSelected.values()) {
                for (VariableVO variable : variables) {
                    if (securityContext.isRoot()
                            || securityContext.matchPrivilege(String.format("*/%s/*/zooplancton/%s|*/%s/*/zooplancton", plateforme.getPlateforme().getSite().getCode(), variable.getCode(), plateforme.getPlateforme().getSite().getCode()),
                                    Role.ROLE_EXTRACTION, ISecurityDatasetManager.PRIVILEGE_TYPE_DATASET)) {
                        if (!variablesAvailables.keySet().contains(variable.getId())) {
                            VOVariableJSF variableVO = new VOVariableJSF(variable);
                            variablesAvailables.put(variable.getId(), variableVO);
                        }
                    }
                }
            }
        }
    }

    /**
     *
     * @return @throws BusinessException
     */
    public String deployNodeTaxon() throws BusinessException {

        if ((treeNodeTaxonSelected.getChildren() == null || treeNodeTaxonSelected.getChildren().isEmpty()) && (treeNodeTaxonSelected.getLeaves() == null || treeNodeTaxonSelected.getLeaves().isEmpty())) {
            enableOrDisableLeaf(treeNodeTaxonSelected.getTaxon().getSelected(), treeNodeTaxonSelected);
        } else {
            if (treeNodeTaxonSelected.getChildren() != null && !treeNodeTaxonSelected.getChildren().isEmpty() && treeNodeTaxonSelected.getTaxon() != null) {
                enableOrDisableAllLeaves(treeNodeTaxonSelected.getTaxon().getSelected(), treeNodeTaxonSelected);
            }

        }

        return null;
    }

    private void enableOrDisableAllLeaves(boolean selected, TreeNodeTaxon TNTaxon) throws BusinessException {
        if (!TNTaxon.getExpanded()) {
            TNTaxon.setExpanded(true);
        }
        enableOrDisableLeaf(selected, TNTaxon);
    }

    private void enableOrDisableLeaf(Boolean conditionDisabling, TreeNodeTaxon taxon) throws BusinessException {
        Map<Long, VOTaxonJSF> taxonsSelecteds = parametersRequest.getTaxonsSelected();

        taxon.getTaxon().setSelected(!conditionDisabling);
        taxonsAvailables.get(taxon.getTaxon().getTaxon().getId()).setSelected(!conditionDisabling);
        if (conditionDisabling) {
            taxonsSelecteds.remove(taxon.getTaxon().getTaxon().getId());
        } else {
            taxonsSelecteds.put(taxon.getTaxon().getTaxon().getId(), taxon.getTaxon());
        }
        // selectSynonyme(conditionDisabling, taxon.getTaxon());
    }

    private List<String> getPathes(String projet, List<Site> sites, List<VariableVO> variables) {
        String sitePath, theme, path;
        theme = THEME;
        List<String> pathes = new LinkedList<>();
        for (Site site : sites) {
            sitePath = site.getPath();
            for (VariableVO variable : variables) {
                path = String.format(PATH_PATTERN, projet, sitePath, theme, DATATYPE, variable.getCode());
                if (!pathes.contains(path)) {
                    pathes.add(path);
                }
            }
        }
        return pathes;
    }

    /**
     *
     * @return @throws BusinessException
     */
    public String extract() throws BusinessException {
        Map<String, Object> metadatasMap = new HashMap<String, Object>();

        List<org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO> metadatasPlateformesVos = new LinkedList<org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO>();
        List<Site> sites = new LinkedList<Site>();
        for (PlateformeVO plateforme : parametersRequest.getPlateformesSelected().values()) {
            org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO plateformeVO = new org.inra.ecoinfo.glacpe.extraction.vo.PlateformeVO(plateforme.getPlateforme());
            if (!sites.contains(plateforme.getPlateforme().getSite())) {
                sites.add(plateforme.getPlateforme().getSite());
            }
            metadatasPlateformesVos.add(plateformeVO);
        }

        List<VariableVO> variablesVO = buildParameterExtractionVariables();
        metadatasMap.put(ZooplanctonParameters.KEY_MAP_SELECTION_VARIABLES, variablesVO);

        List<FileComp> listFiles = new LinkedList<>();
        try {
            if (!parametersRequest.plateformesSelected.isEmpty() && parametersRequest.getFormIsValid()) {
                IntervalDate intervalDate;

                intervalDate = new IntervalDate(parametersRequest.getFirstStartDate(), parametersRequest.getLastEndDate(), DateUtil.getSimpleDateFormatDateLocale());

                List<IntervalDate> intervalsDate = new LinkedList<>();
                intervalsDate.add(intervalDate);
                
                final List<ItemFile> files = fileCompManager.getFileCompFromPathesAndIntervalsDate(getPathes(projectsAvailables.get(idProjectSelected).getProjet().getCode(), sites, variablesVO), intervalsDate);
                for (final ItemFile file : files) {
                    if (file.getMandatory()) {
                        listFiles.add(file.getFileComp());
                    }
                }
            }
        } catch (BadExpectedValueException e) {
            throw new BusinessException("bad file", e);
        } catch (ParseException e) {
            throw new BusinessException("bad file name", e);
        }
        metadatasMap.put(FileComp.class.getSimpleName(), listFiles);
        metadatasMap.put(ZooplanctonParameters.KEY_MAP_SELECTION_TAXONS, buildParameterExtractionTaxons());
        metadatasMap.put(ZooplanctonParameters.KEY_MAP_SELECTION_PLATEFORMES, metadatasPlateformesVos);
        metadatasMap.put(ZooplanctonParameters.KEY_MAP_DETAIL_DEVELOPMENT_STAGE, detailStadeDeveloppement);
        metadatasMap.put(ZooplanctonParameters.KEY_MAP_DETAIL_TAXON, detailTaxon);

        parametersRequest.getDatesRequestParam().getDatesYearsDiscretsFormParam().setPeriods(parametersRequest.getDatesRequestParam().getNonPeriodsYearsContinuous());
        parametersRequest.getDatesRequestParam().getDatesYearsRangeFormParam().setPeriods(parametersRequest.getDatesRequestParam().getNonPeriodsYearsContinuous());

        metadatasMap.put(ZooplanctonParameters.KEY_MAP_SELECTION_DATE, parametersRequest.getDatesRequestParam().clone());
        metadatasMap.put(ZooplanctonParameters.KEY_MAP_SELECTION_ID_PROJECT, projectsAvailables.get(idProjectSelected).getProjet().getId());
        metadatasMap.put(ZooplanctonParameters.KEY_MAP_SELECTION_PROJECT, projectsAvailables.get(idProjectSelected).getProjet());
        metadatasMap.put(ZooplanctonParameters.KEY_MAP_SELECTION_BIOVOLUME, selectionBiovolume);

        metadatasMap.put(IExtractionManager.KEYMAP_COMMENTS, parametersRequest.getCommentExtraction());
        IParameter parameters = new ZooplanctonParameters(metadatasMap);
        extractionManager.extract(parameters, getAffichage());

        return null;
    }

    private List<VariableVO> buildParameterExtractionVariables() {
        List<VariableVO> variablesVO = new LinkedList<VariableVO>();
        for (VOVariableJSF variable : parametersRequest.getListVariablesSelected()) {
            variablesVO.add(variable.getVariable());
        }
        return variablesVO;
    }

    private List<Taxon> buildParameterExtractionTaxons() {
        List<Taxon> taxons = Lists.newLinkedList();
        for (VOTaxonJSF taxon : parametersRequest.getListTaxonsSelected()) {
            taxons.add(taxon.getTaxon());
        }
        return taxons;
    }

    /**
     *
     * @throws BusinessException
     */
    public void filterLists() throws BusinessException {
        taxonsVOAvailables.clear();

        for (VOTaxonJSF taxon : taxonsAvailables.values()) {
            if (genre) {
//                if (taxon.getTaxon().getTaxonNiveau().getCode().equals("genre_espece")) {
                if (taxon.getTaxon().isLeaf()) {
                    taxonsVOAvailables.add(taxon);
                }
            } else {
                taxonsVOAvailables.add(taxon);
            }
        }
        updateTaxonList();

    }

    /**
     *
     * @return
     */
    public int getAffichage() {
        return affichage;
    }

    /**
     *
     * @return
     */
    public Boolean getFilter() {
        return filter;
    }

    /**
     *
     * @return
     */
    public Boolean getGenre() {
        return genre;
    }

    /**
     *
     * @return
     */
    public String getIdJSFForm() {
        return ("formZooplancton");
    }

    /**
     *
     * @return
     */
    public Long getIdProjectSelected() {
        return idProjectSelected;
    }

    /**
     *
     * @return
     */
    public Long getIdTaxonVOSelected() {
        return idTaxonVOSelected;
    }

    /**
     *
     * @return
     */
    @Override
    public boolean getIsStepValid() {
        if (getStep() == 1) {
            return getParametersRequest().getPlateformeStepIsValid();
        } else if (getStep() == 2) {
            return getParametersRequest().getDateStepIsValid();
        } else if (getStep() == 3) {
            return getParametersRequest().getVariableStepIsValid();
        }
        return false;
    }

    /**
     *
     * @return
     */
    public String getLengthOrder() {
        return lengthOrder;
    }

    /**
     *
     * @return
     */
    public String getLengthValue() {
        return lengthValue;
    }

    /**
     *
     * @return
     */
    public ParametersRequest getParametersRequest() {
        return parametersRequest;
    }

    private List<TreeNodeTaxon> preselectionRootNodes = new ArrayList<TreeNodeTaxon>();

    /**
     *
     * @return
     */
    public List<TreeNodeTaxon> getPreselectionRootNodes() {
        return preselectionRootNodes;
    }

    /**
     *
     * @return @throws BusinessException
     */
    public Map<Long, ProjetVO> getProjectsAvailables() throws BusinessException {
        TransactionStatus status = transactionManager.getTransaction(new DefaultTransactionDefinition(TransactionDefinition.PROPAGATION_REQUIRES_NEW));
        if (projectsAvailables.isEmpty()) {

            List<Projet> projets = zooplanctonDatatypeManager.retrieveAvailablesProjets();
            for (Projet projet : projets) {
                List<Plateforme> plateformes = Lists.newArrayList();
                for (Plateforme plateforme : zooplanctonDatatypeManager.retrieveAvailablesPlateformesByProjetId(projet.getId())) {
                    if (!plateformes.contains(plateforme)
                            && (securityContext.isRoot() || securityContext.matchPrivilege(
                                    String.format("%s/%s/*/zooplancton/*|%s/%s/*/zooplancton", projet.getCode(), plateforme.getSite().getCode(), projet.getCode(), plateforme.getSite().getCode()), Role.ROLE_EXTRACTION,
                                    ISecurityDatasetManager.PRIVILEGE_TYPE_DATASET))) {
                        plateformes.add(plateforme);
                    }

                }
                if (!plateformes.isEmpty()) {
                    ProjetVO projectVO = new ProjetVO(projet, plateformes);
                    projectsAvailables.put(projet.getId(), projectVO);
                }
            }

        }

        transactionManager.commit(status);
        return projectsAvailables;
    }

    // GETTERS SETTERS - BEANS
    /**
     *
     * @return @throws BusinessException
     */
    public String getProjetSelected() throws BusinessException {
        String localizedProjetSelected = null;
        if (idProjectSelected != null) {
            String nom = projectsAvailables.get(idProjectSelected).getProjet().getNom();
            localizedProjetSelected = getPropertiesProjectsNames().getProperty(nom);
            if (Strings.isNullOrEmpty(localizedProjetSelected)) {
                localizedProjetSelected = nom;
            }
            return localizedProjetSelected;
        }
        return localizedProjetSelected;
    }

    /**
     *
     * @return
     */
    public Properties getPropertiesPlatformNames() {
        if (this.propertiesPlatformNames == null) {
            this.setPropertiesPlatformNames(localizationManager.newProperties(Plateforme.TABLE_NAME, "nom"));
        }
        return propertiesPlatformNames;
    }

    /**
     *
     * @return
     */
    public Properties getPropertiesProjectsNames() {
        if (this.propertiesProjectsNames == null) {
            this.setPropertiesProjectsNames(localizationManager.newProperties(Projet.NAME_TABLE, "nom"));
        }
        return propertiesProjectsNames;
    }

    /**
     *
     * @return
     */
    public Properties getPropertiesQualitativeNames() {
        if (this.propertiesQualitativeNames == null) {
            this.setPropertiesQualitativeNames(localizationManager.newProperties(ValeurQualitative.NAME_ENTITY_JPA, "value"));
        }
        return propertiesQualitativeNames;
    }

    /**
     *
     * @return
     */
    public Properties getPropertiesSiteNames() {
        if (this.propertiesSiteNames == null) {
            this.setPropertiesSiteNames(localizationManager.newProperties(Site.NAME_ENTITY_JPA, "nom"));
        }
        return propertiesSiteNames;
    }

    /**
     *
     * @return
     */
    public Properties getPropertiesTaxonLevelNames() {
        if (this.propertiesTaxonLevelNames == null) {
            this.setPropertiesTaxonLevelNames(localizationManager.newProperties(TaxonNiveau.TABLE_NAME, "nom"));
        }
        return propertiesTaxonLevelNames;
    }

    /**
     *
     * @return
     */
    public Properties getPropertiesVariableNames() {
        if (this.propertiesVariableNames == null) {
            this.setPropertiesVariableNames(localizationManager.newProperties(Variable.NAME_ENTITY_JPA, "nom"));
        }
        return propertiesVariableNames;
    }

    /**
     *
     * @return @throws BusinessException
     */
    public List<TreeNodeTaxon> getRootNodes() throws BusinessException {
        return rootNodes;
    }

    /**
     *
     * @return
     */
    public List<VOTaxonJSF> getTaxaPreselectedAvailables() {
        return taxaPreselectedAvailables;
    }

    /**
     *
     * @return
     */
    public String getTaxonGroupSelected() {
        return taxonGroupSelected;
    }

    /**
     *
     * @return @throws BusinessException
     */
    public List<VOTaxonJSF> getTaxonsAvailables() throws BusinessException {
        return taxonsVOAvailables;
    }

    /**
     *
     * @return
     */
    public String getTaxonSearch() {
        return taxonSearch;
    }

    /**
     *
     * @return
     */
    public List<VOTaxonJSF> getTaxonsLengthAvailables() {
        return taxonsLengthAvailables;
    }

    /**
     *
     * @return
     */
    public List<VOTaxonJSF> getTaxonsVOAvailables() {
        return taxonsVOAvailables;
    }

    /**
     *
     * @return
     */
    public VOTaxonJSF getTaxonVOSelected() {
        return taxonVOSelected;
    }

    /**
     *
     * @return
     */
    public UITree getTreeListTaxon() {
        return treeListTaxon;
    }

    /**
     *
     * @return
     */
    public UITreeNode getTreeNodePreselection() {
        return treeNodePreselection;
    }

    /**
     *
     * @return
     */
    public UITreeNode getTreeNodePreselectionLeaf() {
        return treeNodePreselectionLeaf;
    }

    /**
     *
     * @return
     */
    public UITreeNode getTreeNodeTaxon() {
        return treeNodeTaxon;
    }

    /**
     *
     * @return
     */
    public TreeNodeTaxon getTreeNodeTaxonSelected() {
        return treeNodeTaxonSelected;
    }

    /**
     *
     * @return
     */
    public UITree getTreePreselectionListTaxons() {
        return treePreselectionListTaxons;
    }

    /**
     *
     * @return
     */
    public String getUpdatePropriete() {
        return null;
    }

    /**
     *
     * @return @throws BusinessException
     * @throws ParseException
     */
    public List<VOVariableJSF> getVariablesAvailables() throws BusinessException, ParseException {
        if (variablesAvailables.isEmpty() && parametersRequest.getDateStepIsValid()) {
            createOrUpdateListVariablesAvailables();
        }
        return new LinkedList<VOVariableJSF>(variablesAvailables.values());

    }

    /**
     *
     * @throws BusinessException
     */
    public void initTaxons() throws BusinessException {

        if (taxonsAvailables.isEmpty()) {

            Map<Taxon, Boolean> taxons = zooplanctonDatatypeManager.retrieveZooplanctonAvailablesTaxons();

            for (Taxon taxon : taxons.keySet()) {
                VOTaxonJSF taxonVO = new VOTaxonJSF(taxon);
                taxonsVOAvailables.add(taxonVO);
                taxonsAvailables.put(taxon.getId(), taxonVO);
//                if (taxon.getTaxonNiveau().getCode().equals(TaxonNiveau.VALUES_ATTR_CODE.GENRE_ESPECE)) {
                if (taxon.isLeaf()) {
                    taxonsLengthAvailables.add(taxonVO);
                }
                if (taxons.get(taxon)) {
                    taxaPreselectedAvailables.add(taxonVO);
                }
            }
        }

        if (rootNodes == null || rootNodes.isEmpty()) {
            List<Taxon> rootTaxon = zooplanctonDatatypeManager.retrieveRootTaxon();
            List<TreeNodeTaxon> rootChildNode = Lists.newLinkedList();
            List<TreeNodeTaxon> rootLeavesNode = Lists.newLinkedList();
            for (Taxon taxon : rootTaxon) {
                VOTaxonJSF voTaxon = taxonsAvailables.get(taxon.getId());
                TreeNodeTaxon treeNodeRoot = new TreeNodeTaxon(voTaxon, taxonsAvailables);
                taxonsAvailables.put(taxon.getId(), voTaxon);
                if (taxon.getTaxonsEnfants().isEmpty()) {
                    rootLeavesNode.add(treeNodeRoot);
                } else {
                    rootChildNode.add(treeNodeRoot);
                }
            }
            TreeNodeTaxon rootNode = new TreeNodeTaxon(rootChildNode, rootLeavesNode);
            rootNodes = Lists.newArrayList(rootNode);
        }

    }

    /**
     *
     * @return
     */
    public String navigate() {
        return RULE_JSF_ZOOPLANCTON;
    }

    /**
     *
     * @return
     */
    @Override
    public String nextStep() {
        super.nextStep();
        switch (getStep()) {
            case 3:
                try {
                    createOrUpdateListVariablesAvailables();
                } catch (BusinessException e) {
                    variablesAvailables.clear();
                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                break;
            case 4:
                if (!getParametersRequest().getVariablesSelected().isEmpty()) {
                    try {
                        initTaxons();
                    } catch (BusinessException e) {
                        taxonsAvailables.clear();
                        taxonsVOAvailables.clear();
                        taxaPreselectedAvailables.clear();
                        rootNodes.clear();
                        preselectionRootNodes.clear();
                    }
                }
                break;
            default:
                break;
        }
        return null;
    }

    /**
     *
     * @param selected
     * @param taxonCurrent
     */
    public void reccursiveTaxonAdd(Boolean selected, VOTaxonJSF taxonCurrent) {
        for (Taxon taxon : taxonCurrent.getTaxon().getTaxonsEnfants()) {
            if (parametersRequest.getTaxonsSelected().keySet().contains(taxon.getId())) {
                if (selected) {
                    parametersRequest.getTaxonsSelected().remove(taxon.getId());
                } else {
                    parametersRequest.getTaxonsSelected().put(taxon.getId(), taxonsAvailables.get(taxon.getId()));
                }
            }
            reccursiveTaxonAdd(selected, taxonsAvailables.get(taxon.getId()));
        }
    }

    /**
     *
     * @return @throws BusinessException
     */
    public String removeAllTaxons() throws BusinessException {
        parametersRequest.getTaxonsSelected().clear();

        for (VOTaxonJSF taxon : taxonsVOAvailables) {
            taxon.setSelected(false);
        }

        for (TreeNodeTaxon tnt : rootNodes.get(0).getChildren()) {
            enableOrDisableAllLeaves(true, tnt);
        }

        return null;
    }

    /**
     *
     * @return @throws BusinessException
     * @throws ParseException
     */
    public String removeAllVariables() throws BusinessException, ParseException {
        createOrUpdateListVariablesAvailables();
        selectionBiovolume = false;
        return null;
    }

    /**
     *
     * @return @throws BusinessException
     */
    public String selectPlateforme() throws BusinessException {
        ProjetVO projectSelected = projectsAvailables.get(idProjectSelected);
        PlateformeVO plateformeSelected = projectSelected.getPlateformes().get(idPlateformeSelected);
        if (plateformeSelected.getSelected()) {
            parametersRequest.getPlateformesSelected().remove(idPlateformeSelected);
            plateformeSelected.setSelected(false);
        } else {
            parametersRequest.getPlateformesSelected().put(idPlateformeSelected, plateformeSelected);
            plateformeSelected.setSelected(true);
        }

        return null;
    }

    /**
     *
     * @param selected
     * @param taxonSelected
     * @throws BusinessException
     */
    public void selectSynonyme(Boolean selected, VOTaxonJSF taxonSelected) throws BusinessException {
        TransactionStatus status = transactionManager.getTransaction(new DefaultTransactionDefinition(TransactionDefinition.PROPAGATION_REQUIRES_NEW));

        List<TaxonProprieteTaxon> taxonProprietesTaxon = zooplanctonDatatypeManager.retrieveTaxonPropertyByTaxonId(taxonSelected.getTaxon().getId());

        if (taxonProprietesTaxon != null && !taxonProprietesTaxon.isEmpty()) {
            String synonymes = "";
            for (TaxonProprieteTaxon taprota : taxonProprietesTaxon) {
                if (taprota.getProprieteTaxon().getCode().equals("synonyme_ancien") || taprota.getProprieteTaxon().getCode().equals("synonyme_recent")) {
                    synonymes = synonymes.concat(taprota.getValeurProprieteTaxon().getStringValue()).concat(",");
                }
            }
            if (synonymes.length() != 0) {
                for (String synonyme : synonymes.split(",")) {
                    if (synonyme.length() > 0) {
                        Taxon taxon = zooplanctonDatatypeManager.retrieveTaxonByCode(Utils.createCodeFromString(synonyme));
                        if (taxon != null) {
                            VOTaxonJSF taxonJSF = taxonsAvailables.get(taxon.getId());
                            taxonJSF.setSelected(!selected);
                            if (selected) {
                                parametersRequest.getTaxonsSelected().remove(taxon.getId());
                            } else {
                                parametersRequest.getTaxonsSelected().put(taxon.getId(), taxonJSF);
                            }
                        }
                    }
                }
            }
        }

        transactionManager.commit(status);
    }

    /**
     *
     * @return @throws BusinessException
     */
    public String selectTaxonVO() throws BusinessException {
        VOTaxonJSF taxonSelected = taxonsAvailables.get(idTaxonVOSelected);

        if (taxonSelected.getSelected()) {
            parametersRequest.getTaxonsSelected().remove(taxonSelected.getTaxon().getId());
        } else {
            parametersRequest.getTaxonsSelected().put(taxonSelected.getTaxon().getId(), taxonSelected);
        }

        taxonSelected.setSelected(!taxonSelected.getSelected());
        selectSynonyme(taxonSelected.getSelected(), taxonSelected);

        return null;
    }

    /**
     *
     * @return
     */
    public String removeVariableBiovolume() {
        selectionBiovolume = false;
        return null;
    }

    /**
     *
     * @return
     */
    public String selectVariable() {
        VOVariableJSF variableSelected = variablesAvailables.get(idVariableSelected);

        if (variableSelected.getSelected()) {
            parametersRequest.getVariablesSelected().remove(idVariableSelected);
        } else {
            parametersRequest.getVariablesSelected().put(idVariableSelected, variableSelected);
        }
        variableSelected.setSelected(!variableSelected.getSelected());
        return null;
    }

    /**
     *
     * @param affichage
     */
    public void setAffichage(int affichage) {
        this.affichage = affichage;
    }

    /**
     *
     * @param extractionManager
     */
    public void setExtractionManager(IExtractionManager extractionManager) {
        this.extractionManager = extractionManager;
    }

    /**
     *
     * @param filter
     */
    public void setFilter(Boolean filter) {
        this.filter = filter;
    }

    /**
     *
     * @param genre
     */
    public void setGenre(Boolean genre) {
        this.genre = genre;
        try {
            filterLists();
        } catch (BusinessException e) {
            e.printStackTrace();
        }

    }

    /**
     *
     * @param idPlateformeSelected
     */
    public void setIdPlateformeSelected(Long idPlateformeSelected) {
        this.idPlateformeSelected = idPlateformeSelected;
    }

    /**
     *
     * @param idProjectSelected
     */
    public void setIdProjectSelected(Long idProjectSelected) {
        this.idProjectSelected = idProjectSelected;
    }

    /**
     *
     * @param idTaxonVOSelected
     */
    public void setIdTaxonVOSelected(Long idTaxonVOSelected) {
        this.idTaxonVOSelected = idTaxonVOSelected;
    }

    /**
     *
     * @param idVariableSelected
     */
    public void setIdVariableSelected(Long idVariableSelected) {
        this.idVariableSelected = idVariableSelected;
    }

    /**
     *
     * @param lengthOrder
     */
    public void setLengthOrder(String lengthOrder) {
        this.lengthOrder = lengthOrder;
    }

    /**
     *
     * @param lengthValue
     */
    public void setLengthValue(String lengthValue) {
        this.lengthValue = lengthValue;
    }

    /**
     *
     * @param localizationManager
     */
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    /**
     *
     * @param notificationsManager
     */
    public void setNotificationsManager(INotificationsManager notificationsManager) {
        this.notificationsManager = notificationsManager;
    }

    /**
     *
     * @param propertiesPlatformNames
     */
    public void setPropertiesPlatformNames(Properties propertiesPlatformNames) {
        this.propertiesPlatformNames = propertiesPlatformNames;
    }

    /**
     *
     * @param propertiesProjectsNames
     */
    public void setPropertiesProjectsNames(Properties propertiesProjectsNames) {
        this.propertiesProjectsNames = propertiesProjectsNames;
    }

    /**
     *
     * @param propertiesQualitativeNames
     */
    public void setPropertiesQualitativeNames(Properties propertiesQualitativeNames) {
        this.propertiesQualitativeNames = propertiesQualitativeNames;
    }

    /**
     *
     * @param propertiesSiteNames
     */
    public void setPropertiesSiteNames(Properties propertiesSiteNames) {
        this.propertiesSiteNames = propertiesSiteNames;
    }

    /**
     *
     * @param propertiesTaxonLevelNames
     */
    public void setPropertiesTaxonLevelNames(Properties propertiesTaxonLevelNames) {
        this.propertiesTaxonLevelNames = propertiesTaxonLevelNames;
    }

    /**
     *
     * @param propertiesVariableNames
     */
    public void setPropertiesVariableNames(Properties propertiesVariableNames) {
        this.propertiesVariableNames = propertiesVariableNames;
    }

    /**
     *
     * @param rootNodes
     */
    public void setRootNodes(List<TreeNodeTaxon> rootNodes) {
        this.rootNodes = rootNodes;
    }

    /**
     *
     * @param securityContext
     */
    public void setSecurityContext(ISecurityContext securityContext) {
        this.securityContext = securityContext;
    }

    /**
     *
     * @param taxaPreselectedAvailables
     */
    public void setTaxaPreselectedAvailables(List<VOTaxonJSF> taxaPreselectedAvailables) {
        this.taxaPreselectedAvailables = taxaPreselectedAvailables;
    }

    /**
     *
     * @param taxonGroupSelected
     */
    public void setTaxonGroupSelected(String taxonGroupSelected) {
        this.taxonGroupSelected = taxonGroupSelected;
    }

    /**
     *
     * @param taxonSearch
     */
    public void setTaxonSearch(String taxonSearch) {
        this.taxonSearch = taxonSearch;
    }

    /**
     *
     * @param taxonsLengthAvailables
     */
    public void setTaxonsLengthAvailables(List<VOTaxonJSF> taxonsLengthAvailables) {
        this.taxonsLengthAvailables = taxonsLengthAvailables;
    }

    /**
     *
     * @param taxonsVOAvailables
     */
    public void setTaxonsVOAvailables(List<VOTaxonJSF> taxonsVOAvailables) {
        this.taxonsVOAvailables = taxonsVOAvailables;
    }

    /**
     *
     * @param taxonVOSelected
     */
    public void setTaxonVOSelected(VOTaxonJSF taxonVOSelected) {
        this.taxonVOSelected = taxonVOSelected;
    }

    /**
     *
     * @param transactionManager
     */
    public void setTransactionManager(JpaTransactionManager transactionManager) {
        this.transactionManager = transactionManager;
    }

    /**
     *
     * @param treeListTaxon
     */
    public void setTreeListTaxon(UITree treeListTaxon) {
        this.treeListTaxon = treeListTaxon;
    }

    /**
     *
     * @param treeNodePreselection
     */
    public void setTreeNodePreselection(UITreeNode treeNodePreselection) {
        this.treeNodePreselection = treeNodePreselection;
    }

    /**
     *
     * @param treeNodePreselectionLeaf
     */
    public void setTreeNodePreselectionLeaf(UITreeNode treeNodePreselectionLeaf) {
        this.treeNodePreselectionLeaf = treeNodePreselectionLeaf;
    }

    /**
     *
     * @param treeNodeTaxon
     */
    public void setTreeNodeTaxon(UITreeNode treeNodeTaxon) {
        this.treeNodeTaxon = treeNodeTaxon;
    }

    /**
     *
     * @param treeNodeTaxonSelected
     */
    public void setTreeNodeTaxonSelected(TreeNodeTaxon treeNodeTaxonSelected) {
        this.treeNodeTaxonSelected = treeNodeTaxonSelected;
    }

    /**
     *
     * @param treePreselectionListTaxons
     */
    public void setTreePreselectionListTaxons(UITree treePreselectionListTaxons) {
        this.treePreselectionListTaxons = treePreselectionListTaxons;
    }

    /**
     *
     * @param zooplanctonDatatypeManager
     */
    public void setZooplanctonDatatypeManager(IZooplanctonDatatypeManager zooplanctonDatatypeManager) {
        this.zooplanctonDatatypeManager = zooplanctonDatatypeManager;
    }

    /**
     *
     * @return
     */
    public String updateCheckDetailsTaxon() {

        setDetailTaxon(true);
        return null;
    }

    /**
     *
     * @return
     */
    public String updateCheckDetailsStadeDevelopment() {

        if (!detailTaxon) {
            setDetailStadeDeveloppement(false);
        }
        return null;
    }

    /**
     *
     * @return
     */
    public String updateTaxonList() {
        taxonsVOAvailables.clear();
        for (Long taxonID : taxonsAvailables.keySet()) {
            if (taxonSearch.trim().length() > 0) {
                String taxonVOSearch = Utils.createCodeFromString(taxonSearch);
                if (Utils.createCodeFromString(taxonsAvailables.get(taxonID).getTaxon().getNomLatin()).contains(taxonVOSearch)) {
                    if (genre) {
//                        if (taxonsAvailables.get(taxonID).getTaxon().getTaxonNiveau().getCode().equals("genre_espece")) {
                        if (taxonsAvailables.get(taxonID).getTaxon().isLeaf()) {
                            taxonsVOAvailables.add(taxonsAvailables.get(taxonID));
                        }
                    } else {
                        taxonsVOAvailables.add(taxonsAvailables.get(taxonID));
                    }
                }
            } else {
                if (genre) {
//                    if (taxonsAvailables.get(taxonID).getTaxon().getTaxonNiveau().getCode().equals("genre_espece")) {
                    if (taxonsAvailables.get(taxonID).getTaxon().isLeaf()) {
                        taxonsVOAvailables.add(taxonsAvailables.get(taxonID));
                    }
                } else {
                    taxonsVOAvailables.add(taxonsAvailables.get(taxonID));
                }
            }
        }
        return null;
    }

    /**
     *
     */
    public class ParametersRequest {

        private String commentExtraction;
        private DatesRequestParamVO datesRequestParam;
        private Map<Long, PlateformeVO> plateformesSelected = new HashMap<Long, PlateformeVO>();

        private Map<Long, VOTaxonJSF> taxonsSelected = new HashMap<Long, VOTaxonJSF>();
        private Map<Long, VOVariableJSF> variablesSelected = new HashMap<Long, VOVariableJSF>();

        /**
         *
         * @return
         */
        public String getCommentExtraction() {
            return commentExtraction;
        }

        /**
         *
         * @return
         */
        public DatesRequestParamVO getDatesRequestParam() {
            if (datesRequestParam == null) {
                initDatesRequestParam();
            }
            return datesRequestParam;
        }

        /**
         *
         * @return
         */
        public Boolean getDateStepIsValid() {
            if ((datesRequestParam.getSelectedFormSelection() != null) && getPlateformeStepIsValid() && datesRequestParam.getCurrentDatesFormParam().getIsValid()) {
                return true;
            } else {
                return false;
            }
        }

        /**
         *
         * @return @throws ParseException
         */
        public Date getFirstStartDate() throws ParseException {
            if (!datesRequestParam.getCurrentDatesFormParam().getPeriods().isEmpty()) {
                Date firstDate = null;
                for (Date mapDate : datesRequestParam.getCurrentDatesFormParam().retrieveParametersMap().values()) {
                    if (firstDate == null) {
                        firstDate = mapDate;
                    } else if (firstDate.after(mapDate)) {
                        firstDate = mapDate;
                    }
                }
                return firstDate;
            }
            return null;
        }

        /**
         *
         * @return
         */
        public boolean getFormIsValid() {
            if (getDateStepIsValid() && selectionBiovolume && !getVariableStepIsValid()) {
                return true;
            }
            return getTaxonStepIsValid();
        }

        /**
         *
         * @return @throws ParseException
         */
        public Date getLastEndDate() throws ParseException {
            if (!datesRequestParam.getCurrentDatesFormParam().getPeriods().isEmpty()) {
                Date lastDate = null;
                for (Date mapDate : datesRequestParam.getCurrentDatesFormParam().retrieveParametersMap().values()) {
                    if (lastDate == null) {
                        lastDate = mapDate;
                    } else if (lastDate.before(mapDate)) {
                        lastDate = mapDate;
                    }
                }
                return lastDate;
            }
            return null;
        }

        /**
         *
         * @return
         */
        public List<PlateformeVO> getListPlateformesSelected() {
            return new LinkedList<PlateformeVO>(plateformesSelected.values());
        }

        /**
         *
         * @return
         */
        public List<VOTaxonJSF> getListTaxonsSelected() {
            return new LinkedList<VOTaxonJSF>(taxonsSelected.values());
        }

        /**
         *
         * @return
         */
        public List<VOVariableJSF> getListVariablesSelected() {
            return new LinkedList<VOVariableJSF>(variablesSelected.values());
        }

        /**
         *
         * @return
         */
        public Map<Long, PlateformeVO> getPlateformesSelected() {
            return plateformesSelected;
        }

        /**
         *
         * @return
         */
        public Boolean getPlateformeStepIsValid() {
            return !plateformesSelected.isEmpty();
        }

        /**
         *
         * @return
         */
        public Map<Long, VOTaxonJSF> getTaxonsSelected() {
            return taxonsSelected;
        }

        /**
         *
         * @return
         */
        public Boolean getTaxonStepIsValid() {
            if (getVariableStepIsValid() && !taxonsSelected.isEmpty()) {
                return true;
            }
            if (variablesSelected.isEmpty() && getVariableStepIsValid()) {
                return true;
            }

            return false;
        }

        /**
         *
         * @return
         */
        public Map<Long, VOVariableJSF> getVariablesSelected() {
            return variablesSelected;
        }

        /**
         *
         * @return
         */
        public Boolean getVariableStepIsValid() {
            return (!variablesSelected.isEmpty()) && getDateStepIsValid();
        }

        private void initDatesRequestParam() {
            datesRequestParam = new DatesRequestParamVO(localizationManager);
        }

        /**
         *
         * @param commentExtraction
         */
        public void setCommentExtraction(String commentExtraction) {
            this.commentExtraction = commentExtraction;
        }

        /**
         *
         * @param taxonsSelected
         */
        public void setTaxonsSelected(Map<Long, VOTaxonJSF> taxonsSelected) {
            this.taxonsSelected = taxonsSelected;
        }

    }

    /**
     *
     */
    public class PlateformeVO {

        private String localizedPlateformeName;
        private String localizedSiteName;
        private Plateforme plateforme;
        private Boolean selected = false;

        /**
         *
         * @param plateforme
         */
        public PlateformeVO(Plateforme plateforme) {
            super();
            this.plateforme = plateforme;
            if (plateforme != null) {
                this.localizedPlateformeName = getPropertiesPlatformNames().getProperty(plateforme.getNom());
                if (Strings.isNullOrEmpty(this.localizedPlateformeName)) {
                    this.localizedPlateformeName = plateforme.getNom();
                }
                this.localizedSiteName = getPropertiesSiteNames().getProperty(plateforme.getSite().getNom());
                if (Strings.isNullOrEmpty(this.localizedSiteName)) {
                    this.localizedSiteName = plateforme.getSite().getNom();
                }
            }
        }

        /**
         *
         * @return
         */
        public String getLocalizedPlateformeName() {
            return localizedPlateformeName;
        }

        /**
         *
         * @return
         */
        public String getLocalizedSiteName() {
            return localizedSiteName;
        }

        /**
         *
         * @return
         */
        public Plateforme getPlateforme() {
            return plateforme;
        }

        /**
         *
         * @return
         */
        public Boolean getSelected() {
            return selected;
        }

        /**
         *
         * @param localizedPlateformeName
         */
        public void setLocalizedPlateformeName(String localizedPlateformeName) {
            this.localizedPlateformeName = localizedPlateformeName;
        }

        /**
         *
         * @param localizedSiteName
         */
        public void setLocalizedSiteName(String localizedSiteName) {
            this.localizedSiteName = localizedSiteName;
        }

        /**
         *
         * @param selected
         */
        public void setSelected(Boolean selected) {
            this.selected = selected;
        }
    }

    /**
     *
     */
    public class ProjetVO {

        private String localizedProjetName;
        private Map<Long, PlateformeVO> plateformes = new HashMap<Long, PlateformeVO>();
        private Projet projet;

        /**
         *
         * @param projet
         * @param plateformes
         */
        public ProjetVO(Projet projet, List<Plateforme> plateformes) {
            super();
            this.projet = projet;
            if (projet != null) {
                this.localizedProjetName = getPropertiesProjectsNames().getProperty(projet.getNom());
                if (Strings.isNullOrEmpty(this.localizedProjetName)) {
                    this.localizedProjetName = projet.getNom();
                }
            }
            for (Plateforme plateforme : plateformes) {
                this.plateformes.put(plateforme.getId(), new PlateformeVO(plateforme));
            }

        }

        /**
         *
         * @return
         */
        public String getLocalizedProjetName() {
            return localizedProjetName;
        }

        /**
         *
         * @return
         */
        public Map<Long, PlateformeVO> getPlateformes() {
            return plateformes;
        }

        /**
         *
         * @return
         */
        public Projet getProjet() {
            return projet;
        }

        /**
         *
         * @param localizedProjetName
         */
        public void setLocalizedProjetName(String localizedProjetName) {
            this.localizedProjetName = localizedProjetName;
        }
    }

    /**
     *
     */
    public class TreeNodeTaxon implements TreeNode {

        private List<TreeNodeTaxon> children;
        private Boolean expanded = false;
        private List<TreeNodeTaxon> leaves;
        private VOTaxonJSF taxon;
        Map<Long, VOTaxonJSF> taxonsAvailables = null;

        /**
         *
         * @param children
         * @param leaves
         */
        public TreeNodeTaxon(List<TreeNodeTaxon> children, List<TreeNodeTaxon> leaves) {
            super();
            this.taxon = null;
            this.children = Lists.newArrayList();
            for (TreeNodeTaxon child : children) {
                this.children.add(child);
            }
            this.leaves = Lists.newArrayList();
            for (TreeNodeTaxon leaf : leaves) {
                this.leaves.add(leaf);
            }
        }

        /**
         *
         * @param taxon
         * @param taxonsAvailables
         */
        public TreeNodeTaxon(VOTaxonJSF taxon, Map<Long, VOTaxonJSF> taxonsAvailables) {
            super();
            this.taxon = taxon;
            this.taxonsAvailables = taxonsAvailables;

        }

        @SuppressWarnings("rawtypes")
        @Override
        public Enumeration children() {
            if (isLeaf()) {
                return new Enumeration<TreeNode>() {

                    public boolean hasMoreElements() {
                        return false;
                    }

                    public TreeNode nextElement() {
                        return null;
                    }
                };
            } else {
                return Iterators.asEnumeration(children.iterator());
            }
        }

        @Override
        public boolean getAllowsChildren() {
            return false;
        }

        @Override
        public TreeNode getChildAt(int childIndex) {
            if (children != null) {
                return children.get(childIndex);
            }
            return null;
        }

        @Override
        public int getChildCount() {
            return children.size();
        }

        /**
         *
         * @return
         */
        public List<TreeNodeTaxon> getChildren() {
            if (children == null || children.isEmpty()) {
                children = null;
                if (!taxon.getTaxon().getTaxonsEnfants().isEmpty() && (leaves == null || leaves.isEmpty())) {
                    children = Lists.newArrayList();
                    leaves = getLeaves();
                    for (Taxon taxonEnfant : taxon.getTaxon().getTaxonsEnfants()) {
                        if (taxonEnfant.getTaxonsEnfants().isEmpty()) {
                            leaves.add(new TreeNodeTaxon(taxonsAvailables.get(taxonEnfant.getId()), taxonsAvailables));
                        } else {
                            children.add(new TreeNodeTaxon(taxonsAvailables.get(taxonEnfant.getId()), taxonsAvailables));
                        }
                    }
                }
            }
            return children;
        }

        /**
         *
         * @return
         */
        public Boolean getExpanded() {
            return expanded;
        }

        @Override
        public int getIndex(TreeNode node) {
            return children.indexOf(node);
        }

        /**
         *
         * @return
         */
        public List<TreeNodeTaxon> getLeaves() {
            if (leaves == null) {
                leaves = Lists.newArrayList();
            }
            return leaves;
        }

        @Override
        public TreeNode getParent() {
            return null;
        }

        /**
         *
         * @return
         */
        public VOTaxonJSF getTaxon() {
            return taxon;
        }

        @Override
        public boolean isLeaf() {
            if (children == null || children.isEmpty()) {
                return true;
            }
            return false;
        }

        /**
         *
         * @param expanded
         */
        public void setExpanded(Boolean expanded) {
            this.expanded = expanded;
        }

        /**
         *
         * @param leaves
         */
        public void setLeaves(List<TreeNodeTaxon> leaves) {
            this.leaves = leaves;
        }
    }

    /**
     *
     */
    public class VOTaxonJSF {

        private Float biovolume;
        private Float cellLength;
        private String localizedLevelName;
        private String localizedName;
        private Boolean selected;
        private Taxon taxon;

        /**
         *
         * @param taxon
         */
        public VOTaxonJSF(Taxon taxon) {
            super();
            this.taxon = taxon;
            this.selected = false;
            if (taxon != null) {
                this.localizedName = taxon.getNomLatin();
                this.localizedLevelName = getPropertiesTaxonLevelNames().getProperty(taxon.getTaxonNiveau().getNom());
                if (Strings.isNullOrEmpty(this.localizedLevelName)) {
                    this.localizedLevelName = taxon.getTaxonNiveau().getNom();
                }
            }
        }

        /**
         *
         * @return
         */
        public Float getBiovolume() {
            return biovolume;
        }

        /**
         *
         * @return
         */
        public Float getCellLength() {
            return cellLength;
        }

        /**
         *
         * @return
         */
        public String getLocalizedLevelName() {
            return localizedLevelName;
        }

        /**
         *
         * @return
         */
        public String getLocalizedName() {
            return localizedName;
        }

        /**
         *
         * @return
         */
        public Boolean getSelected() {
            return selected;
        }

        /**
         *
         * @return
         */
        public Taxon getTaxon() {
            return taxon;
        }

        /**
         *
         * @param biovolume
         */
        public void setBiovolume(Float biovolume) {
            this.biovolume = biovolume;
        }

        /**
         *
         * @param cellLength
         */
        public void setCellLength(Float cellLength) {
            this.cellLength = cellLength;
        }

        /**
         *
         * @param localizedLevelName
         */
        public void setLocalizedLevelName(String localizedLevelName) {
            this.localizedLevelName = localizedLevelName;
        }

        /**
         *
         * @param localizedName
         */
        public void setLocalizedName(String localizedName) {
            this.localizedName = localizedName;
        }

        /**
         *
         * @param selected
         */
        public void setSelected(Boolean selected) {
            this.selected = selected;
        }

    }

    /**
     *
     */
    public class VOVariableJSF {

        private String localizedVariableName;
        private Boolean selected = false;
        private VariableVO variable;

        /**
         *
         * @param variable
         */
        public VOVariableJSF(VariableVO variable) {
            super();
            this.variable = variable;
            if (variable != null) {
                this.localizedVariableName = getPropertiesVariableNames().getProperty(variable.getNom());
                if (Strings.isNullOrEmpty(this.localizedVariableName)) {
                    this.localizedVariableName = variable.getNom();
                }
            }
        }

        /**
         *
         * @return
         */
        public String getLocalizedVariableName() {
            return localizedVariableName;
        }

        /**
         *
         * @return
         */
        public Boolean getSelected() {
            return selected;
        }

        /**
         *
         * @return
         */
        public VariableVO getVariable() {
            return variable;
        }

        /**
         *
         * @param localizedVariableName
         */
        public void setLocalizedVariableName(String localizedVariableName) {
            this.localizedVariableName = localizedVariableName;
        }

        /**
         *
         * @param selected
         */
        public void setSelected(Boolean selected) {
            this.selected = selected;
        }
    }

    /**
     *
     * @return
     */
    public Boolean getDetailTaxon() {
        return detailTaxon;
    }

    /**
     *
     * @return
     */
    public Boolean getDetailStadeDeveloppement() {
        return detailStadeDeveloppement;
    }

    /**
     *
     * @param detailTaxon
     */
    public void setDetailTaxon(Boolean detailTaxon) {
        this.detailTaxon = detailTaxon;
    }

    /**
     *
     * @param detailStadeDeveloppement
     */
    public void setDetailStadeDeveloppement(Boolean detailStadeDeveloppement) {
        this.detailStadeDeveloppement = detailStadeDeveloppement;
    }

    /**
     *
     * @return
     */
    public Boolean getSelectionBiovolume() {
        return selectionBiovolume;
    }

    /**
     *
     * @param selectionBiovolume
     */
    public void setSelectionBiovolume(Boolean selectionBiovolume) {
        this.selectionBiovolume = selectionBiovolume;
    }

    /**
     *
     * @param fileCompManager
     */
    public void setFileCompManager(IFileCompManager fileCompManager) {
        this.fileCompManager = fileCompManager;
    }

}
